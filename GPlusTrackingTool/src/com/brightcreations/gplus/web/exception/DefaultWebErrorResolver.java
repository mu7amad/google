/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.web.exception;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.mvc.multiaction.NoSuchRequestHandlingMethodException;

/**
 * 
 * @author Bright Creations
 * 
 */

public class DefaultWebErrorResolver implements WebErrorResolver, MessageSourceAware, InitializingBean {
	private Logger logger = LoggerFactory.getLogger(getClass());

	public static final String DEFAULT_EXCEPTION_MESSAGE_VALUE = "_exmsg";
	public static final String DEFAULT_MESSAGE_VALUE = "_msg";

	private static final Logger log = LoggerFactory.getLogger(DefaultWebErrorResolver.class);

	private Map<String, WebError> exceptionMappings = Collections.emptyMap();

	private Map<String, String> exceptionMappingDefinitions = Collections.emptyMap();

	private MessageSource messageSource;
	private LocaleResolver localeResolver;

	private String defaultMoreInfoUrl;
	private boolean defaultEmptyCodeToStatus;
	private String defaultDeveloperMessage;

	public DefaultWebErrorResolver() {
		this.defaultEmptyCodeToStatus = true;
		this.defaultDeveloperMessage = DEFAULT_EXCEPTION_MESSAGE_VALUE;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public void setLocaleResolver(LocaleResolver resolver) {
		this.localeResolver = resolver;
	}

	public void setExceptionMappingDefinitions(Map<String, String> exceptionMappingDefinitions) {
		this.exceptionMappingDefinitions = exceptionMappingDefinitions;
	}

	public void setDefaultMoreInfoUrl(String defaultMoreInfoUrl) {
		this.defaultMoreInfoUrl = defaultMoreInfoUrl;
	}

	public void setDefaultEmptyCodeToStatus(boolean defaultEmptyCodeToStatus) {
		this.defaultEmptyCodeToStatus = defaultEmptyCodeToStatus;
	}

	public void setDefaultDeveloperMessage(String defaultDeveloperMessage) {
		this.defaultDeveloperMessage = defaultDeveloperMessage;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	public void afterPropertiesSet() throws Exception {
		Map<String, String> definitions = createDefaultExceptionMappingDefinitions();

		if (this.exceptionMappingDefinitions != null && !this.exceptionMappingDefinitions.isEmpty()) {
			definitions.putAll(this.exceptionMappingDefinitions);
		}

		this.exceptionMappings = toRestErrors(definitions);
	}

	protected final Map<String, String> createDefaultExceptionMappingDefinitions() {

		Map<String, String> m = new LinkedHashMap<String, String>();

		// 400
		applyDef(m, HttpMessageNotReadableException.class, HttpStatus.BAD_REQUEST);
		applyDef(m, MissingServletRequestParameterException.class, HttpStatus.BAD_REQUEST);
		applyDef(m, TypeMismatchException.class, HttpStatus.BAD_REQUEST);
		applyDef(m, "javax.validation.ValidationException", HttpStatus.BAD_REQUEST);

		// 404
		applyDef(m, NoSuchRequestHandlingMethodException.class, HttpStatus.NOT_FOUND);
		applyDef(m, "org.hibernate.ObjectNotFoundException", HttpStatus.NOT_FOUND);

		// 405
		applyDef(m, HttpRequestMethodNotSupportedException.class, HttpStatus.METHOD_NOT_ALLOWED);

		// 406
		applyDef(m, HttpMediaTypeNotAcceptableException.class, HttpStatus.NOT_ACCEPTABLE);

		// 409
		// can't use the class directly here as it may not be an available
		// dependency:
		applyDef(m, "org.springframework.dao.DataIntegrityViolationException", HttpStatus.CONFLICT);

		// 415
		applyDef(m, HttpMediaTypeNotSupportedException.class, HttpStatus.UNSUPPORTED_MEDIA_TYPE);

		return m;
	}

	private void applyDef(Map<String, String> m, Class<?> clazz, HttpStatus status) {
		applyDef(m, clazz.getName(), status);
	}

	private void applyDef(Map<String, String> m, String key, HttpStatus status) {
		m.put(key, definitionFor(status));
	}

	private String definitionFor(HttpStatus status) {
		return status.value() + ", " + DEFAULT_EXCEPTION_MESSAGE_VALUE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.brightcreations.gplus.web.exception.WebErrorResolver#resolveError
	 * (org.springframework.web.context.request.ServletWebRequest,
	 * java.lang.Object, java.lang.Exception)
	 */
	public WebError resolveError(ServletWebRequest request, Object handler, Exception ex) {
		WebError template = getRestErrorTemplate(ex);
		if (template == null) {
			return null;
		}

		WebError.Builder builder = new WebError.Builder();
		builder.setStatus(getStatusValue(template, request, ex));
		builder.setCode(getCode(template, request, ex));
		builder.setMoreInfoUrl(getMoreInfoUrl(template, request, ex));
		builder.setThrowable(ex);

		if (ex.getClass().equals(CustomException.class)) {

			String msg = getDeveloperMessage(template, request, ex);
			logger.info("CUSTOM EXCEPTION: " + msg);
			if (msg != null) {
				builder.setMessage(msg);
			}
			builder.setMessage(msg);
			builder.setDeveloperMessage("");
		} else {

			String msg = getDeveloperMessage(template, request, ex);

			logger.info("Default EXCEPTION: " + msg);

			builder.setMessage("");
			builder.setDeveloperMessage("");
		}

		return builder.build();
	}

	protected int getStatusValue(WebError template, ServletWebRequest request, Exception ex) {
		return template.getStatus().value();
	}

	protected int getCode(WebError template, ServletWebRequest request, Exception ex) {
		int code = template.getCode();
		if (code <= 0 && defaultEmptyCodeToStatus) {
			code = getStatusValue(template, request, ex);
		}
		return code;
	}

	protected String getMoreInfoUrl(WebError template, ServletWebRequest request, Exception ex) {
		String moreInfoUrl = template.getMoreInfoUrl();
		if (moreInfoUrl == null) {
			moreInfoUrl = this.defaultMoreInfoUrl;
		}
		return moreInfoUrl;
	}

	protected String getMessage(WebError template, ServletWebRequest request, Exception ex) {
		return getMessage(template.getMessage(), request, ex);
	}

	protected String getDeveloperMessage(WebError template, ServletWebRequest request, Exception ex) {
		String devMsg = template.getDeveloperMessage();
		if (devMsg == null && defaultDeveloperMessage != null) {
			devMsg = defaultDeveloperMessage;
		}
		if (DEFAULT_MESSAGE_VALUE.equals(devMsg)) {
			devMsg = template.getMessage();
		}
		return getMessage(devMsg, request, ex);
	}

	/**
	 * Returns the response status message to return to the client, or
	 * {@code null} if no status message should be returned.
	 * 
	 * @return the response status message to return to the client, or
	 *         {@code null} if no status message should be returned.
	 */
	protected String getMessage(String msg, ServletWebRequest webRequest, Exception ex) {

		if (msg != null) {
			if (msg.equalsIgnoreCase("null") || msg.equalsIgnoreCase("off")) {
				return null;
			}
			if (msg.equalsIgnoreCase(DEFAULT_EXCEPTION_MESSAGE_VALUE)) {
				msg = ex.getMessage();
			}
			if (messageSource != null) {
				Locale locale = null;
				if (localeResolver != null) {
					locale = localeResolver.resolveLocale(webRequest.getRequest());
				}
				msg = messageSource.getMessage(msg, null, msg, locale);
			}
		}

		return msg;
	}

	/**
	 * Returns the config-time 'template' WebError instance configured for the
	 * specified Exception, or {@code null} if a match was not found.
	 * <p/>
	 * The config-time template is used as the basis for the WebError
	 * constructed at runtime.
	 * 
	 * @param ex
	 * @return the template to use for the WebError instance to be constructed.
	 */
	private WebError getRestErrorTemplate(Exception ex) {
		Map<String, WebError> mappings = this.exceptionMappings;
		if (CollectionUtils.isEmpty(mappings)) {
			return null;
		}
		WebError template = null;
		String dominantMapping = null;
		int deepest = Integer.MAX_VALUE;
		for (Map.Entry<String, WebError> entry : mappings.entrySet()) {
			String key = entry.getKey();
			int depth = getDepth(key, ex);
			if (depth >= 0 && depth < deepest) {
				deepest = depth;
				dominantMapping = key;
				template = entry.getValue();
			}
		}
		if (template != null && log.isDebugEnabled()) {
			log.debug("Resolving to WebError template '" + template + "' for exception of type ["
					+ ex.getClass().getName() + "], based on exception mapping [" + dominantMapping + "]");
		}
		return template;
	}

	/**
	 * Return the depth to the superclass matching.
	 * <p>
	 * 0 means ex matches exactly. Returns -1 if there's no match. Otherwise,
	 * returns depth. Lowest depth wins.
	 */
	protected int getDepth(String exceptionMapping, Exception ex) {
		return getDepth(exceptionMapping, ex.getClass(), 0);
	}

	private int getDepth(String exceptionMapping, Class<?> exceptionClass, int depth) {
		if (exceptionClass.getName().contains(exceptionMapping)) {
			// Found it!
			return depth;
		}
		// If we've gone as far as we can go and haven't found it...
		if (exceptionClass.equals(Throwable.class)) {
			return -1;
		}
		return getDepth(exceptionMapping, exceptionClass.getSuperclass(), depth + 1);
	}

	protected Map<String, WebError> toRestErrors(Map<String, String> smap) {
		if (CollectionUtils.isEmpty(smap)) {
			return Collections.emptyMap();
		}

		Map<String, WebError> map = new LinkedHashMap<String, WebError>(smap.size());

		for (Map.Entry<String, String> entry : smap.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			WebError template = toRestError(value);
			map.put(key, template);
		}

		return map;
	}

	protected WebError toRestError(String exceptionConfig) {
		String[] values = StringUtils.commaDelimitedListToStringArray(exceptionConfig);
		if (values == null || values.length == 0) {
			throw new IllegalStateException(
					"Invalid config mapping.  Exception names must map to a string configuration.");
		}

		WebError.Builder builder = new WebError.Builder();

		boolean statusSet = false;
		boolean codeSet = false;
		boolean msgSet = false;
		boolean devMsgSet = false;
		boolean moreInfoSet = false;

		for (String value : values) {

			String trimmedVal = StringUtils.trimWhitespace(value);

			// check to see if the value is an explicitly named key/value pair:
			String[] pair = StringUtils.split(trimmedVal, "=");
			if (pair != null) {
				// explicit attribute set:
				String pairKey = StringUtils.trimWhitespace(pair[0]);
				if (!StringUtils.hasText(pairKey)) {
					pairKey = null;
				}
				String pairValue = StringUtils.trimWhitespace(pair[1]);
				if (!StringUtils.hasText(pairValue)) {
					pairValue = null;
				}
				if ("status".equalsIgnoreCase(pairKey)) {
					int statusCode = getRequiredInt(pairKey, pairValue);
					builder.setStatus(statusCode);
					statusSet = true;
				} else if ("code".equalsIgnoreCase(pairKey)) {
					int code = getRequiredInt(pairKey, pairValue);
					builder.setCode(code);
					codeSet = true;
				} else if ("msg".equalsIgnoreCase(pairKey)) {
					builder.setMessage(pairValue);
					msgSet = true;
				} else if ("devMsg".equalsIgnoreCase(pairKey)) {
					builder.setDeveloperMessage(pairValue);
					devMsgSet = true;
				} else if ("infoUrl".equalsIgnoreCase(pairKey)) {
					builder.setMoreInfoUrl(pairValue);
					moreInfoSet = true;
				}
			} else {
				// not a key/value pair - use heuristics to determine what value
				// is being set:
				int val;
				if (!statusSet) {
					val = getInt("status", trimmedVal);
					if (val > 0) {
						builder.setStatus(val);
						statusSet = true;
						continue;
					}
				}
				if (!codeSet) {
					val = getInt("code", trimmedVal);
					if (val > 0) {
						builder.setCode(val);
						codeSet = true;
						continue;
					}
				}
				if (!moreInfoSet && trimmedVal.toLowerCase().startsWith("http")) {
					builder.setMoreInfoUrl(trimmedVal);
					moreInfoSet = true;
					continue;
				}
				if (!msgSet) {
					builder.setMessage(trimmedVal);
					msgSet = true;
					continue;
				}
				if (!devMsgSet) {
					builder.setDeveloperMessage(trimmedVal);
					devMsgSet = true;
					continue;
				}
				if (!moreInfoSet) {
					builder.setMoreInfoUrl(trimmedVal);
					moreInfoSet = true;
					// noinspection UnnecessaryContinue
					continue;
				}
			}
		}

		return builder.build();
	}

	private static int getRequiredInt(String key, String value) {
		try {
			int anInt = Integer.valueOf(value);
			return Math.max(-1, anInt);
		} catch (NumberFormatException e) {
			String msg = "Configuration element '" + key + "' requires an integer value.  The value " + "specified: "
					+ value;
			throw new IllegalArgumentException(msg, e);
		}
	}

	private static int getInt(String key, String value) {
		try {
			return getRequiredInt(key, value);
		} catch (IllegalArgumentException iae) {
			return 0;
		}
	}
}

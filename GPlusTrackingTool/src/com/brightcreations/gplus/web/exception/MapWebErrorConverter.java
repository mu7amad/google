/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.web.exception;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;

/**
 * 
 * @author Bright Creations
 * 
 */

@SuppressWarnings("rawtypes")
public class MapWebErrorConverter implements WebErrorConverter<Map> {

	private static final String DEFAULT_STATUS_KEY = "status";
	private static final String DEFAULT_CODE_KEY = "code";
	private static final String DEFAULT_MESSAGE_KEY = "message";
	private static final String DEFAULT_DEVELOPER_MESSAGE_KEY = "developerMessage";
	private static final String DEFAULT_MORE_INFO_URL_KEY = "moreInfoUrl";

	private String statusKey = DEFAULT_STATUS_KEY;
	private String codeKey = DEFAULT_CODE_KEY;
	private String messageKey = DEFAULT_MESSAGE_KEY;
	private String developerMessageKey = DEFAULT_DEVELOPER_MESSAGE_KEY;
	private String moreInfoUrlKey = DEFAULT_MORE_INFO_URL_KEY;

	protected Map<String, Object> createMap() {
		return new LinkedHashMap<String, Object>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.brightcreations.gplus.web.exception.WebErrorConverter#convert(com
	 * .brightcreations.gplus.web.exception.WebError)
	 */
	public Map convert(WebError re) {
		Map<String, Object> m = createMap();
		HttpStatus status = re.getStatus();
		m.put(getStatusKey(), status.value());

		int code = re.getCode();
		if (code > 0) {
			m.put(getCodeKey(), code);
		}

		String message = re.getMessage();
		if (message != null) {
			m.put(getMessageKey(), message);
		}

		String devMsg = re.getDeveloperMessage();
		if (devMsg != null) {
			m.put(getDeveloperMessageKey(), devMsg);
		}

		String moreInfoUrl = re.getMoreInfoUrl();
		if (moreInfoUrl != null) {
			m.put(getMoreInfoUrlKey(), moreInfoUrl);
		}

		return m;
	}

	/**
	 * @return the statusKey
	 */
	public String getStatusKey() {
		return statusKey;
	}

	/**
	 * @param statusKey
	 *            the statusKey to set
	 */
	public void setStatusKey(String statusKey) {
		this.statusKey = statusKey;
	}

	/**
	 * @return the codeKey
	 */
	public String getCodeKey() {
		return codeKey;
	}

	/**
	 * @param codeKey
	 *            the codeKey to set
	 */
	public void setCodeKey(String codeKey) {
		this.codeKey = codeKey;
	}

	/**
	 * @return the messageKey
	 */
	public String getMessageKey() {
		return messageKey;
	}

	/**
	 * @param messageKey
	 *            the messageKey to set
	 */
	public void setMessageKey(String messageKey) {
		this.messageKey = messageKey;
	}

	/**
	 * @return the developerMessageKey
	 */
	public String getDeveloperMessageKey() {
		return developerMessageKey;
	}

	/**
	 * @param developerMessageKey
	 *            the developerMessageKey to set
	 */
	public void setDeveloperMessageKey(String developerMessageKey) {
		this.developerMessageKey = developerMessageKey;
	}

	/**
	 * @return the moreInfoUrlKey
	 */
	public String getMoreInfoUrlKey() {
		return moreInfoUrlKey;
	}

	/**
	 * @param moreInfoUrlKey
	 *            the moreInfoUrlKey to set
	 */
	public void setMoreInfoUrlKey(String moreInfoUrlKey) {
		this.moreInfoUrlKey = moreInfoUrlKey;
	}
}

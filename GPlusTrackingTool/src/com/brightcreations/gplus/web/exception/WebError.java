/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.web.exception;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.springframework.http.HttpStatus;

/**
 * @author ahmed.fawzy
 * 
 */
@XmlRootElement(name = "webError")
public class WebError {

	private HttpStatus status;
	private int code;
	private String message;
	private String developerMessage;
	private String moreInfoUrl;
	private Throwable throwable;

	public WebError() {

	}

	public WebError(HttpStatus status, int code, String message, String developerMessage, String moreInfoUrl,
			Throwable throwable) {
		if (status == null) {
			throw new NullPointerException("HttpStatus argument cannot be null.");
		}

		this.status = status;
		this.code = code;
		this.message = message;
		this.developerMessage = developerMessage;
		this.moreInfoUrl = moreInfoUrl;
		this.throwable = throwable;
	}

	/**
	 * @return the status
	 */
	public HttpStatus getStatus() {
		return status;
	}

	/**
	 * @return the code
	 */
	public int getCode() {
		return code;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @return the developerMessage
	 */
	public String getDeveloperMessage() {
		return developerMessage;
	}

	/**
	 * @return the moreInfoUrl
	 */
	public String getMoreInfoUrl() {
		return moreInfoUrl;
	}

	/**
	 * @return the throwable
	 */
	@XmlTransient
	public Throwable getThrowable() {
		return throwable;
	}

	public static class Builder {

		private HttpStatus status;
		private int code;
		private String message;
		private String developerMessage;
		private String moreInfoUrl;
		private Throwable throwable;

		public Builder() {
		}

		public Builder setStatus(int statusCode) {
			this.status = HttpStatus.valueOf(statusCode);
			return this;
		}

		public Builder setStatus(HttpStatus status) {
			this.status = status;
			return this;
		}

		public Builder setCode(int code) {
			this.code = code;
			return this;
		}

		public Builder setMessage(String message) {
			this.message = message;
			return this;
		}

		public Builder setDeveloperMessage(String developerMessage) {
			this.developerMessage = developerMessage;
			return this;
		}

		public Builder setMoreInfoUrl(String moreInfoUrl) {
			this.moreInfoUrl = moreInfoUrl;
			return this;
		}

		public Builder setThrowable(Throwable throwable) {
			this.throwable = throwable;
			return this;
		}

		public WebError build() {
			if (this.status == null) {
				this.status = HttpStatus.INTERNAL_SERVER_ERROR;
			}
			return new WebError(this.status, this.code, this.message, this.developerMessage, this.moreInfoUrl,
					this.throwable);
		}
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setDeveloperMessage(String developerMessage) {
		this.developerMessage = developerMessage;
	}

	public void setMoreInfoUrl(String moreInfoUrl) {
		this.moreInfoUrl = moreInfoUrl;
	}
}

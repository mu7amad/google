/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.web.exception;

import org.springframework.core.convert.converter.Converter;

/**
 * 
 * @author Bright Creations
 * 
 */
public interface WebErrorConverter<T> extends Converter<WebError, T> {

	T convert(WebError re);
}

/*******************************************************
 * Copyright (c) 2016 Bright Creations , All Rights Reserved. NOTICE: All
 * information contained herein is, and remains the property of Bright
 * Creations. Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained Access to
 * the source code contained herein is hereby forbidden to anyone except current
 * Bright Creations employees, managers or parties given license to view and
 * create derivative works for the source code and who have executed
 * Confidentiality and Non-disclosure agreements explicitly covering such
 * access.
 *******************************************************/

package com.brightcreations.gplus.web.exception;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.util.CollectionUtils;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.handler.AbstractHandlerExceptionResolver;
import org.springframework.web.util.WebUtils;

/**
 * 
 * @author Bright Creations
 * 
 */
public class WebHandlerExceptionResolver extends AbstractHandlerExceptionResolver implements InitializingBean {

	private static final Logger logger = LoggerFactory.getLogger(WebHandlerExceptionResolver.class);

	private HttpMessageConverter<?>[] messageConverters = null;

	private List<HttpMessageConverter<?>> allMessageConverters = null;

	private WebErrorResolver errorResolver;

	private WebErrorConverter<?> errorConverter;

	/**
	 * @return the messageConverters
	 */
	public HttpMessageConverter<?>[] getMessageConverters() {
		return messageConverters;
	}

	/**
	 * @param messageConverters
	 *            the messageConverters to set
	 */
	public void setMessageConverters(HttpMessageConverter<?>[] messageConverters) {
		this.messageConverters = messageConverters;
	}

	/**
	 * @return the allMessageConverters
	 */
	public List<HttpMessageConverter<?>> getAllMessageConverters() {
		return allMessageConverters;
	}

	/**
	 * @param allMessageConverters
	 *            the allMessageConverters to set
	 */
	public void setAllMessageConverters(List<HttpMessageConverter<?>> allMessageConverters) {
		this.allMessageConverters = allMessageConverters;
	}

	/**
	 * @return the errorResolver
	 */
	public WebErrorResolver getErrorResolver() {
		return errorResolver;
	}

	/**
	 * @param errorResolver
	 *            the errorResolver to set
	 */
	public void setErrorResolver(WebErrorResolver errorResolver) {
		this.errorResolver = errorResolver;
	}

	/**
	 * @return the errorConverter
	 */
	public WebErrorConverter<?> getErrorConverter() {
		return errorConverter;
	}

	/**
	 * @param errorConverter
	 *            the errorConverter to set
	 */
	public void setErrorConverter(WebErrorConverter<?> errorConverter) {
		this.errorConverter = errorConverter;
	}

	/**
	 * 
	 */
	public WebHandlerExceptionResolver() {
		this.errorResolver = new DefaultWebErrorResolver();
		this.errorConverter = new MapWebErrorConverter();
	}

	public void afterPropertiesSet() throws Exception {
		ensureMessageConverters();
	}

	@SuppressWarnings("unchecked")
	private void ensureMessageConverters() {

		List<HttpMessageConverter<?>> converters = new ArrayList<HttpMessageConverter<?>>();

		if (this.messageConverters != null && this.messageConverters.length > 0) {
			converters.addAll(CollectionUtils.arrayToList(this.messageConverters));
		}

		new HttpMessageConverterHelper().addDefaults(converters);

		this.allMessageConverters = converters;
	}

	private static final class HttpMessageConverterHelper extends WebMvcConfigurationSupport {
		public void addDefaults(List<HttpMessageConverter<?>> converters) {
			addDefaultHttpMessageConverters(converters);
		}
	}

	protected ModelAndView doResolveException(HttpServletRequest request, HttpServletResponse response, Object handler,
			Exception ex) {
		logger.error("WebHandlerExceptionResolver Caught an Exception:", ex);
		ServletWebRequest webRequest = new ServletWebRequest(request, response);

		WebErrorResolver resolver = this.getErrorResolver();

		WebError error = resolver.resolveError(webRequest, handler, ex);
		if (error == null) {
			return null;
		}

		ModelAndView mav = null;

		try {
			response.setStatus(error.getCode());
			mav = new ModelAndView("exception_page", "exceptionCaught", error); // getModelAndView(webRequest,
																				// handler,
																				// error);
		} catch (Exception invocationEx) {
			logger.error("Acquiring ModelAndView for Exception [" + ex + "] resulted in an exception.", invocationEx);
		}

		return mav;
	}

	protected ModelAndView getModelAndView(ServletWebRequest webRequest, Object handler, WebError error)
			throws Exception {

		applyStatusIfPossible(webRequest, error);

		Object body = error;

		@SuppressWarnings("rawtypes")
		WebErrorConverter converter = getErrorConverter();
		if (converter != null) {
			body = converter.convert(error);
		}

		return handleResponseBody(body, webRequest, error);
		// final Map<String, Object> model = new HashMap<String, Object>();
		// model.put("exceptionCaught", error);
		// return new ModelAndView("exception_page",model);
	}

	private void applyStatusIfPossible(ServletWebRequest webRequest, WebError error) {
		if (!WebUtils.isIncludeRequest(webRequest.getRequest())) {
			webRequest.getResponse().setStatus(error.getStatus().value());
		}
		// TODO support response.sendError ?
	}

	@SuppressWarnings("unchecked")
	private ModelAndView handleResponseBody(Object body, ServletWebRequest webRequest, WebError error)
			throws ServletException, IOException {

		HttpInputMessage inputMessage = new ServletServerHttpRequest(webRequest.getRequest());

		List<MediaType> acceptedMediaTypes = inputMessage.getHeaders().getAccept();
		if (acceptedMediaTypes.isEmpty()) {
			acceptedMediaTypes = Collections.singletonList(MediaType.ALL);
		}

		MediaType.sortByQualityValue(acceptedMediaTypes);

		HttpOutputMessage outputMessage = new ServletServerHttpResponse(webRequest.getResponse());

		Class<?> bodyType = body.getClass();

		List<HttpMessageConverter<?>> converters = this.allMessageConverters;

		if (converters != null) {
			for (MediaType acceptedMediaType : acceptedMediaTypes) {
				for (@SuppressWarnings("rawtypes")
				HttpMessageConverter messageConverter : converters) {
					if (messageConverter.canWrite(bodyType, acceptedMediaType)) {
						messageConverter.write(body, acceptedMediaType, outputMessage);

						return new ModelAndView();
					}
				}
			}
		}

		if (logger.isWarnEnabled()) {
			logger.warn("Could not find HttpMessageConverter that supports return type [" + bodyType + "] and "
					+ acceptedMediaTypes);
		}
		return new ModelAndView("exception_page", "exceptionCaught", error);
	}
}

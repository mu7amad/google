/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.web;

import java.util.List;

import org.codehaus.jackson.map.AnnotationIntrospector;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.module.SimpleModule;
import org.codehaus.jackson.xc.JaxbAnnotationIntrospector;

/**
 * 
 * @author Bright Creations
 * 
 */
public class JaxbJacksonObjectMapper extends ObjectMapper {

	public JaxbJacksonObjectMapper() {
		final AnnotationIntrospector introspector = new JaxbAnnotationIntrospector();
		super.setDeserializationConfig(super.getDeserializationConfig().withAnnotationIntrospector(introspector));
		super.setSerializationConfig(super.getSerializationConfig().withAnnotationIntrospector(introspector));
	}

	public JaxbJacksonObjectMapper(List<SimpleModule> simpleModules) {
		final AnnotationIntrospector introspector = new JaxbAnnotationIntrospector();
		super.setDeserializationConfig(super.getDeserializationConfig().withAnnotationIntrospector(introspector));
		super.setSerializationConfig(super.getSerializationConfig().withAnnotationIntrospector(introspector));

		if (simpleModules != null) {
			for (SimpleModule simpleModule : simpleModules) {
				super.registerModule(simpleModule);
			}
		}
	}
}
/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Bright Creations
 * 
 */
public class WarmingServlet extends HttpServlet {
	private static final long serialVersionUID = -7723200954338873293L;

	Logger logger = LoggerFactory.getLogger(getClass());

	public void init() throws ServletException {
		logger.info("initializing warmup servlet...");
	}

	public void service(ServletRequest servletRequest, ServletResponse servletResponse)
			throws ServletException, IOException {
		logger.info("warming up...");
	}
}

/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.web.security;

import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.servlet.RequestDispatcher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.authentication.ProviderNotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import com.brightcreations.gplus.module.controllers.OfyService;
import com.brightcreations.gplus.module.controllers.MIDataExplorer.CmsMIDataExplorer;
import com.brightcreations.gplus.module.model.ApplicationUser;
import com.brightcreations.gplus.module.model.ApplicationUserStatusEnum;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.oauth2.Oauth2;
import com.google.api.services.oauth2.model.Userinfo;
import com.google.api.services.plus.model.Person;
import com.google.appengine.api.users.User;
import com.googlecode.objectify.Ref;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.CredentialRefreshListener;
import com.google.api.client.auth.oauth2.TokenErrorResponse;
import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.services.plusDomains.PlusDomains;

import java.util.Arrays;
import java.io.BufferedReader;

/**
 * @author Ramy Mahmoudi
 */
public class GaeAuthenticationProvider implements AuthenticationProvider {

	private Logger logger = LoggerFactory.getLogger(getClass());

	public Authentication authenticate(final Authentication authentication) throws AuthenticationException {

		logger.info("trying to authenticate user");

		System.out.println("GaeAuthenticationProvider");

		final User googleUser = (User) authentication.getPrincipal();

		System.out.println(googleUser.getUserId());

		final String email = googleUser.getEmail().toLowerCase();

		// Ref<ApplicationUser> userKey =
		// OfyService.ofy().load().type(ApplicationUser.class).filter("email",
		// email)
		// .filter("status",
		// ApplicationUserStatusEnum.ACTIVE.toString()).first();
		Ref<ApplicationUser> userKey = OfyService.ofy().load().type(ApplicationUser.class).filter("email", email)
				.first();

		ApplicationUser user = userKey != null ? userKey.get() : null;

		final GaeUserAuthentication gaeUserAuthentication;

		if (user == null) {
			user = new ApplicationUser();
			user.setEmail(email);
			String name = email.split("@")[0];

			final Set<String> authorities = new HashSet<String>();

			logger.error("unauthenticated user");

			if (OfyService.ofy().load().type(ApplicationUser.class).count() == 0) {
				logger.error("no users found");
				logger.error("Creating Super Admin.");

				authorities.add(ApplicationUserRole.ROLE_ADMIN.getAuthority());
				authorities.add(ApplicationUserRole.ROLE_RESEARCHER.getAuthority());
				authorities.add(ApplicationUserRole.ROLE_USER.getAuthority());
				user.setOnlyUser(false);
				user.setSuperUser(true);
				user.setType(ApplicationUserType.ADMINISTRATOR.getType());
				user.setAuthorities(authorities);
				SimpleDateFormat dt = new SimpleDateFormat("MMMM dd, yyyy", Locale.US);
				String date = dt.format(new Date());
				user.setCreationDate(date);
				user.setAddedBy("System");
				user.setFirstName(name);
				user.setLastName(name);
				user.setStatus(ApplicationUserStatusEnum.ACTIVE.toString());
				OfyService.ofy().save().entity(user).now();
				logger.error("first user saved");

				gaeUserAuthentication = new GaeUserAuthentication(user, authentication.getDetails());
				gaeUserAuthentication.setAuthenticated(true);

			} else {

				gaeUserAuthentication = new GaeUserAuthentication(user, authentication.getDetails());
				gaeUserAuthentication.setAuthenticated(false);

				throw new InsufficientAuthenticationException("Not Authenticated.");
			}

		} else {
			if (user.getStatus().equalsIgnoreCase(ApplicationUserStatusEnum.ACTIVE.toString())) {
				logger.info("User Found.");
				return new GaeUserAuthentication(user, authentication.getDetails());
			} else {

				gaeUserAuthentication = new GaeUserAuthentication(user, authentication.getDetails());
				gaeUserAuthentication.setAuthenticated(false);

				throw new ProviderNotFoundException("Not Authorized. InActive Account.");
			}
		}

		return new GaeUserAuthentication(user, authentication.getDetails());
	}

	public final boolean supports(final Class<?> authentication) {
		return PreAuthenticatedAuthenticationToken.class.isAssignableFrom(authentication);
	}
}

/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.web.security;

import org.springframework.security.core.GrantedAuthority;

/**
 * 
 * @author Bright Creations
 * 
 */
public enum ApplicationUserRole implements GrantedAuthority {

	ROLE_ADMIN(0), ROLE_RESEARCHER(1), ROLE_USER(2), ROLE_ANONYMOUS(3);

	private int bit;

	ApplicationUserRole(int bit) {
		this.setBit(bit);
	}

	public String getAuthority() {
		return toString();
	}

	/**
	 * @param bit
	 *            the bit to set
	 */
	public void setBit(int bit) {
		this.bit = bit;
	}

	/**
	 * @return the bit
	 */
	public int getBit() {
		return bit;
	}
}

/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.web.security;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import com.brightcreations.gplus.module.model.ApplicationUser;

/**
 * Authentication object representing a fully-authenticated user.
 * 
 * @author Ramy Mahmoudi
 */
public class GaeUserAuthentication implements Authentication {
	/**
	 * 
	 */
	private static final long serialVersionUID = -697414735567465674L;
	private final ApplicationUser principal;
	private final Object details;
	private final Set<String> authorities;
	private boolean authenticated;

	public GaeUserAuthentication(final ApplicationUser principal, final Object details) {
		this.principal = principal;
		this.details = details;
		authorities = principal == null ? new HashSet<String>() : principal.getAuthorities();
		authenticated = true;
	}

	public Collection<GrantedAuthority> getAuthorities() {
		final Set<GrantedAuthority> auth = new HashSet<GrantedAuthority>();
		for (final String a : authorities) {
			auth.add(new GrantedAuthority() {
				private static final long serialVersionUID = -4823153114970079988L;

				public String getAuthority() {
					return a;
				}
			});
		}
		return auth;
	}

	public Object getCredentials() {
		throw new UnsupportedOperationException();
	}

	public Object getDetails() {
		return null;
	}

	public String getName() {
		return principal.getEmail().toLowerCase();
	}

	public Object getPrincipal() {
		return principal;
	}

	public boolean isAuthenticated() {
		return authenticated;
	}

	public void setAuthenticated(final boolean isAuthenticated) {
		authenticated = isAuthenticated;
	}

	public String toString() {
		return "GaeUserAuthentication{" + "principal=" + principal + ", details=" + details + ", authenticated="
				+ authenticated + '}';
	}
}

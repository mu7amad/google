/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.web.security;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.filter.GenericFilterBean;

import com.google.appengine.api.taskqueue.TaskQueuePb.TaskQueueQueryTasksResponse.Task.RequestMethod;

/**
 * @author Ramy Mahmoudi
 */
public class PostAuthenticationFilter extends GenericFilterBean {

	private Logger logger = LoggerFactory.getLogger(getClass());

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		logger.info("PostAuthenticationFilter");
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;

		if (httpRequest.getMethod().equals(RequestMethod.GET.toString())) {
			chain.doFilter(request, response);
		} else {

			URI requestUrl = null;
			try {

				logger.info("Request URL: " + httpRequest.getRequestURL());
				logger.info("Request method: " + httpRequest.getMethod());

				requestUrl = new URI(httpRequest.getRequestURI());

			} catch (URISyntaxException e) {
				e.printStackTrace();
			}

			logger.info(
					"Exclude from forging filter match: " + requestUrl.getPath().matches("\\/cms/process/extract.*"));

			/**
			 * Exclude post requests created for the tasks
			 */
			if (requestUrl.getPath().matches("/cms/process/extract/.*") || requestUrl.getPath().matches("/cms/.*")
					|| requestUrl.getPath().matches("/emailmessage/.*") || requestUrl.getPath().matches("/site/.*")) {
				logger.info("Exclude from forging filter");
				chain.doFilter(request, response);
			} else {

				URI url = null;
				try {
					// System.out.println("Request URL: "
					// + httpRequest.getRequestURL());
					// System.out.println(httpRequest.getMethod());
					//
					// System.out.println(httpRequest.getHeader("referer"));

					url = new URI(httpRequest.getHeader("referer"));
				} catch (URISyntaxException e) {
					e.printStackTrace();
				}

				HttpSession session = httpRequest.getSession();
				Cookie[] cookies = httpRequest.getCookies();
				Cookie cookie = null;
				for (int i = 0; i < cookies.length; i++) {
					if (cookies[i].getName().equals("stk")) {
						cookie = cookies[i];
					}
				}

				@SuppressWarnings("unchecked")
				Map<String, String> sessionTokens = (Map<String, String>) session.getAttribute("tkm");

				String globalSessionToken = (String) session.getAttribute("globalToken");

				String requestToken = httpRequest.getParameter("tk");

				// System.out.println("Session: " + sessionTokens);
				// System.out.println(httpRequest.getRequestURI());
				// System.out.println(httpRequest.getHeader("referer"));
				// System.out.println("Cookie: " + cookie.getValue());
				// System.out.println("Request: " + requestToken);
				// / System.out.printlnglobalSessionTokenen: "
				// globalSessionTokenen);

				// System.out.println("PATH: " + url.getPath());

				if (sessionTokens != null && globalSessionToken != null && sessionTokens.get(url.getPath()) != null
						&& cookie != null && requestToken != null) {

					String cookieToken = cookie.getValue();

					// Page token
					String pageToken = sessionTokens.get(url.getPath());

					if (pageToken.equals(requestToken) && globalSessionToken.equals(cookieToken)) {
						chain.doFilter(request, httpResponse);
					} else {
						logger.error("REQUEST FORGING DETECTED!");
						logger.info("sessionTokens: " + sessionTokens);
						logger.info("globalSessionToken: " + globalSessionToken);
						logger.info("cookieToken: " + cookieToken);
						logger.info("sessionTokens.get(url.getPath()): " + sessionTokens.get(url.getPath()));
						logger.info("requestToken: " + requestToken);
						// httpResponse
						// .sendError(400, "Request forging detected!");
						httpResponse.sendError(400, "Session expired!");
					}
				} else {
					logger.error("REQUEST FORGING DETECTED!");
					logger.info("sessionTokens: " + sessionTokens);
					logger.info("globalSessionToken: " + globalSessionToken);
					logger.info("sessionTokens.get(url.getPath()): " + sessionTokens.get(url.getPath()));
					logger.info("cookie: " + cookie);
					logger.info("requestToken: " + requestToken);
					logger.error("REQUEST FORGING DETECTED!");
					// httpResponse.sendError(400, "Request forging detected!");
					httpResponse.sendError(400, "Session expired!");
				}

			}

		}

	}
}

/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/
package com.brightcreations.gplus.web.security;

/**
 * @author Ramy Mahmoudi
 */
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

public class MyAccessDeniedHandler implements AccessDeniedHandler {

	Logger logger = LoggerFactory.getLogger(MyAccessDeniedHandler.class);

	private String accessDeniedUrl;

	public MyAccessDeniedHandler() {
	}

	public MyAccessDeniedHandler(String accessDeniedUrl) {
		this.accessDeniedUrl = accessDeniedUrl;
	}

	public void handle(HttpServletRequest request, HttpServletResponse response,
			AccessDeniedException accessDeniedException) throws IOException, ServletException {

		logger.info("ACCESS DENIED !!");
		// response.sendRedirect(accessDeniedUrl);
		response.setStatus(403);

	}

	public String getAccessDeniedUrl() {
		return accessDeniedUrl;
	}

	public void setAccessDeniedUrl(String accessDeniedUrl) {
		this.accessDeniedUrl = accessDeniedUrl;
	}
}
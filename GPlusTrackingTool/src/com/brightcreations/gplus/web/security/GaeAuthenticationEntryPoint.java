/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.web.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

/**
 * 
 * @author Bright Creations
 * 
 */
public class GaeAuthenticationEntryPoint implements AuthenticationEntryPoint {

	public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			AuthenticationException authException) throws IOException, ServletException {

		System.out.println("+++++++ GaeAuthenticationEntryPoint (commence)  +++++++");

		UserService userService = UserServiceFactory.getUserService();
		String createLoginURL = userService.createLoginURL(httpServletRequest.getRequestURI());
		System.out.println("Redirect Url: " + createLoginURL);
		createLoginURL = createLoginURL.replaceFirst("https", "http");
		System.out.println("Redirect Url: " + createLoginURL);

		httpServletResponse.sendRedirect(createLoginURL);
	}
}

/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.web.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

/**
 * @author Ahmed Fawzy
 */
public class GaeLogoutFilter extends GenericFilterBean {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
			throws IOException, ServletException {
		System.out.println("GaeLogoutFilter");
		logger.debug("calling GaeLogoutFilter...");

		SecurityContextHolder.getContext().setAuthentication(null);

		if (SecurityContextHolder.getContext().getAuthentication() == null) {
			logger.debug("null authentication object");
		}

		chain.doFilter(request, response);
	}
}

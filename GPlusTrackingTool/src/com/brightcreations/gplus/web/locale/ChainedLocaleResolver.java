/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.web.locale;

import java.util.List;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.AbstractLocaleResolver;

/**
 * 
 * @author Bright Creations
 * 
 */

public class ChainedLocaleResolver extends AbstractLocaleResolver {
	private List<Locale> supportedLocales;

	private Locale currentLocale;

	private List<LocaleResolver> localeResolverChain;

	public Locale getCurrentLocale() {
		if (currentLocale == null) {
			currentLocale = super.getDefaultLocale();
		}
		return currentLocale;
	}

	/**
	 * @return the defaultLocale
	 */

	public Locale getDefaultLocale() {
		return super.getDefaultLocale();
	}

	/**
	 * @return the localeResolverChain
	 */
	public List<LocaleResolver> getLocaleResolverChain() {
		return localeResolverChain;
	}

	/**
	 * @return the supportedLocales
	 */
	public List<Locale> getSupportedLocales() {
		return supportedLocales;
	}

	public Locale resolveLocale(final HttpServletRequest request) {

		Locale resolved = null;

		for (final LocaleResolver locale : localeResolverChain) {
			if ((resolved = locale.resolveLocale(request)) != null) {
				break;
			}
		}

		boolean supported = false;

		for (final Locale locale : supportedLocales) {
			if (resolved.equals(locale)) {
				supported = true;
				break;
			}
		}

		if (!supported) {
			return super.getDefaultLocale();
		}

		currentLocale = resolved;

		return resolved;
	}

	/**
	 * @param defaultLocale
	 *            the defaultLocale to set
	 */

	public void setDefaultLocale(final Locale defaultLocale) {
		super.setDefaultLocale(defaultLocale);
	}

	public void setLocale(final HttpServletRequest request, final HttpServletResponse response, final Locale locale) {
		boolean supported = false;

		for (final Locale loc : supportedLocales) {
			if (loc.equals(locale)) {
				supported = true;
				break;
			}
		}

		if (!supported) {
			currentLocale = super.getDefaultLocale();
		} else {
			currentLocale = locale;
		}

		for (final LocaleResolver localeResolver : localeResolverChain) {
			try {
				localeResolver.setLocale(request, response, currentLocale);
			} catch (final Exception ex) {

			}
		}
	}

	/**
	 * @param localeResolverChain
	 *            the localeResolverChain to set
	 */
	public void setLocaleResolverChain(final List<LocaleResolver> localeResolverChain) {
		this.localeResolverChain = localeResolverChain;
	}

	/**
	 * @param supportedLocales
	 *            the supportedLocales to set
	 */
	public void setSupportedLocales(final List<Locale> supportedLocales) {
		this.supportedLocales = supportedLocales;
	}
}

/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.web;

import java.util.Map;

import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.module.SimpleSerializers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.brightcreations.gplus.module.model.AbstractResource;

/**
 *
 * @author Ahmed Fawzi
 *
 */
public class GPlusSimpleSerializers extends SimpleSerializers {

	private Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	public GPlusSimpleSerializers(Map<String, JsonSerializer<AbstractResource<?>>> jsonSerializers) {
		if (jsonSerializers != null) {
			for (Map.Entry<String, JsonSerializer<AbstractResource<?>>> jsonSerializerEntry : jsonSerializers
					.entrySet()) {
				Class<AbstractResource<?>> clazz = null;

				try {
					clazz = (Class<AbstractResource<?>>) Class.forName(jsonSerializerEntry.getKey());
				} catch (ClassNotFoundException e) {
					logger.error("cannot add serializer for class " + jsonSerializerEntry.getKey(), e);
					continue;
				}

				this.addSerializer(clazz, jsonSerializerEntry.getValue());
			}
		}
	}
}

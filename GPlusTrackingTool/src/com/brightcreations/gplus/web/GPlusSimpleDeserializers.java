/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.web;

import java.util.Map;

import org.codehaus.jackson.map.module.SimpleDeserializers;
import org.codehaus.jackson.map.JsonDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.brightcreations.gplus.module.model.AbstractResource;

/**
 * 
 * @author Ahmed Fawzi
 * 
 */
public class GPlusSimpleDeserializers extends SimpleDeserializers {

	private Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	public GPlusSimpleDeserializers(Map<String, JsonDeserializer<? extends AbstractResource<?>>> jsonDeserializers) {
		if (jsonDeserializers != null) {
			for (Map.Entry<String, JsonDeserializer<? extends AbstractResource<?>>> jsonDeserializerEntry : jsonDeserializers
					.entrySet()) {
				Class<AbstractResource<?>> clazz = null;

				try {
					clazz = (Class<AbstractResource<?>>) Class.forName(jsonDeserializerEntry.getKey());
				} catch (ClassNotFoundException e) {
					logger.error("cannot add deserializer for class " + jsonDeserializerEntry.getKey(), e);
					continue;
				}

				this.addDeserializer(clazz, jsonDeserializerEntry.getValue());
			}
		}
	}
}

/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/
package com.brightcreations.gplus.web.converter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Bright Creations
 * 
 */

@XmlRootElement(name = "list")
public class MarshallableList<T> {

	private Collection<T> list;

	public MarshallableList() {
		this.list = new ArrayList<T>();
	}

	public MarshallableList(final Collection<T> list) {
		this.list = list;
	}

	public MarshallableList(final Set<T> list) {
		this.list = list;
	}

	public MarshallableList(final List<T> list) {
		this.list = list;
	}

	@XmlElement(name = "list")
	public Collection<T> getList() {
		return this.list;
	}

	public void setList(final Collection<T> list) {
		this.list = list;
	}
}

package com.brightcreations.gplus.web.converter;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "map")
public class MarshallableMap<T> {

	private Map<T, T> map;

	public MarshallableMap() {
		this.map = new HashMap<>();
	}

	public MarshallableMap(final Map<T, T> map) {
		this.map = map;
	}

	public Map<T, T> getMap() {
		return map;
	}

	public void setMap(final Map<T, T> map) {
		this.map = map;
	}

}

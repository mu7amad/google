package com.brightcreations.gplus.module.model;
/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;

/**
 * @author Mohammed.Eladly
 */
@Entity
@Cache
@XmlRootElement(name = "userStudy")
public class UserStudy extends LongIdResource implements Comparable<UserStudy> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2717536118966860964L;

	@Index
	// @JsonBackReference
	private Ref<Study> studyRef;

	@Index
	// @JsonBackReference
	private Ref<ApplicationUser> appUserRef;

	@Index
	// @JsonBackReference
	private boolean uploader;

	@Index
	// @JsonBackReference
	private boolean publisher;

	@Index 
	private boolean isOwner;
	
	public Ref<Study> getStudyRef() {
		return studyRef;
	}

	public void setStudyRef(Ref<Study> studyRef) {
		this.studyRef = studyRef;
	}

	public Ref<ApplicationUser> getAppUserRef() {
		return appUserRef;
	}

	public void setAppUserRef(Ref<ApplicationUser> appUserRef) {
		this.appUserRef = appUserRef;
	}

	public boolean isUploader() {
		return uploader;
	}

	public void setUploader(boolean uploader) {
		this.uploader = uploader;
	}

	public boolean isPublisher() {
		return publisher;
	}

	public void setPublisher(boolean publisher) {
		this.publisher = publisher;
	}

	public boolean isOwner() {
		return isOwner;
	}

	public void setOwner(boolean isOwner) {
		this.isOwner = isOwner;
	}

	@Override
	public int compareTo(UserStudy o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserStudy other = (UserStudy) obj;
		if (appUserRef != other.appUserRef)
			return false;
		return true;
	}

}

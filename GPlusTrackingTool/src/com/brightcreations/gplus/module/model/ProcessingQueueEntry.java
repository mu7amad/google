/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.model;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;

/**
 * 
 * @author Bright Creations
 * 
 */

@Entity
public class ProcessingQueueEntry extends LongIdResource {

	private static final long serialVersionUID = 1377970567688867428L;

	private Key<DataSheet> dataSheetKey;

	/**
	 * @return the dataSheetKey
	 */
	public Key<DataSheet> getDataSheetKey() {
		return dataSheetKey;
	}

	/**
	 * @param dataSheetKey
	 *            the dataSheetKey to set
	 */
	public void setDataSheetKey(Key<DataSheet> dataSheetKey) {
		this.dataSheetKey = dataSheetKey;
	}

}

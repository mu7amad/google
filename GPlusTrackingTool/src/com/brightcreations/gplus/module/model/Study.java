package com.brightcreations.gplus.module.model;

/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

/**
 * @author Mohammed.Eladly
 */

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.appengine.api.blobstore.BlobKey;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;

@Entity
@Cache
@XmlRootElement(name = "study")
public class Study extends LongIdResource implements Comparable<Study> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2050023139036720890L;
	@Index
	private String studyName;
	@Index
	private String studyID;
	private String description;
	@Index
	private String creationDate;
	private boolean published = false;
	@Index
	private String ownerEmail;
	private int totalInterviewed;
	private int pendingFiles;
	@Index
	private boolean publicStudy;
	@javax.persistence.Transient
	private String uploadUrl;

	@Transient
	private Set<ApplicationUser> publishers;
	@Transient
	private Set<ApplicationUser> uploaders;

	private Set<String> sharedWithExternalUsers;

	@javax.persistence.Transient
	private List<DataSheetsZipEntry> zipFiles;

	@Index
	// // @JsonBackReference(value = "study-questions")
	private List<Key<Question>> questions = new ArrayList<Key<Question>>();

	@Index
	// @JsonBackReference(value = "study-answers")
	private List<Key<Answer>> answers = new ArrayList<Key<Answer>>();

	@Index
	// @JsonBackReference(value = "study-brands")
	private List<Key<Brand>> brands = new ArrayList<Key<Brand>>();

	@Index
	// @JsonBackReference(value = "study-filters")
	private List<Key<Filter>> filters = new ArrayList<Key<Filter>>();

	@Index
	// @JsonBackReference(value = "study-divisions")
	private List<Key<Division>> divisions = new ArrayList<Key<Division>>();

	@Index
	// @JsonBackReference(value = "study-dashboards")
	private List<Key<Dashboard>> dashboards = new ArrayList<Key<Dashboard>>();

	@Index
	// // @JsonBackReference(value = "study-applicationUsers")
	private Set<Key<ApplicationUser>> sharedWithApplicationUsers;

	@Index
	private String logoUrl;

	@Index
	private String logoBlobKey;

	@Index
	private boolean deleted;

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public String getStudyName() {
		return studyName;
	}

	public void setStudyName(String studyName) {
		this.studyName = studyName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public List<Key<Question>> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Key<Question>> questions) {
		this.questions = questions;
	}

	public List<Key<Answer>> getAnswers() {
		return answers;
	}

	public void setAnswers(List<Key<Answer>> answers) {
		this.answers = answers;
	}

	public List<Key<Brand>> getBrands() {
		return brands;
	}

	public void setBrands(List<Key<Brand>> brands) {
		this.brands = brands;
	}

	public List<Key<Filter>> getFilters() {
		return filters;
	}

	public void setFilters(List<Key<Filter>> filters) {
		this.filters = filters;
	}

	public List<Key<Division>> getDivisions() {
		return divisions;
	}

	public void setDivisions(List<Key<Division>> divisions) {
		this.divisions = divisions;
	}

	public String getStudyID() {
		return studyID;
	}

	public void setStudyID(String studyID) {
		this.studyID = studyID;
	}

	public boolean isPublished() {
		return published;
	}

	public void setPublished(boolean published) {
		this.published = published;
	}

	public List<Key<Dashboard>> getDashboards() {
		return dashboards;
	}

	public void setDashboards(List<Key<Dashboard>> dashboards) {
		this.dashboards = dashboards;
	}

	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}

	public String getLogoBlobKey() {
		return logoBlobKey;
	}

	public void setLogoBlobKey(String logoBlobKey) {
		this.logoBlobKey = logoBlobKey;
	}

	public Set<Key<ApplicationUser>> getSharedWithApplicationUsers() {
		return sharedWithApplicationUsers;
	}

	public void setSharedWithApplicationUsers(Set<Key<ApplicationUser>> applicationUsers) {
		this.sharedWithApplicationUsers = applicationUsers;
	}

	public int getTotalInterviewed() {
		return totalInterviewed;
	}

	public void setTotalInterviewed(int totalInterviewed) {
		this.totalInterviewed = totalInterviewed;
	}

	public Set<ApplicationUser> getPublishers() {
		return publishers;
	}

	public void setPublishers(Set<ApplicationUser> publishers) {
		this.publishers = publishers;
	}

	public Set<ApplicationUser> getUploaders() {
		return uploaders;
	}

	public void setUploaders(Set<ApplicationUser> uploaders) {
		this.uploaders = uploaders;
	}

	public Set<String> getSharedWithExternalUsers() {
		return sharedWithExternalUsers;
	}

	public void setSharedWithExternalUsers(Set<String> sharedWithExternalUsers) {
		this.sharedWithExternalUsers = sharedWithExternalUsers;
	}

	public String getOwnerEmail() {
		return ownerEmail;
	}

	public void setOwnerEmail(String ownerEmail) {
		this.ownerEmail = ownerEmail;
	}

	public List<DataSheetsZipEntry> getZipFiles() {
		return zipFiles;
	}

	public void setZipFiles(List<DataSheetsZipEntry> zipFiles) {
		this.zipFiles = zipFiles;
	}

	public int getPendingFiles() {
		return pendingFiles;
	}

	public void setPendingFiles(int pendingFiles) {
		this.pendingFiles = pendingFiles;
	}

	public String getUploadUrl() {
		return uploadUrl;
	}

	public void setUploadUrl(String uploadUrl) {
		this.uploadUrl = uploadUrl;
	}

	public boolean isPublicStudy() {
		return publicStudy;
	}

	public void setPublicStudy(boolean publicStudy) {
		this.publicStudy = publicStudy;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Study o) {
		if (getCreationDate() == null || o.getCreationDate() == null)
			return 0;
		return getCreationDate().compareTo(o.getCreationDate());
		// return 0;
	}

}

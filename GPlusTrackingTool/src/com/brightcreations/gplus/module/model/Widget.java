/*******************************************************
 * Copyright (c) 2016 Bright Creations , All Rights Reserved. NOTICE: All
 * information contained herein is, and remains the property of Bright
 * Creations. Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained Access to
 * the source code contained herein is hereby forbidden to anyone except current
 * Bright Creations employees, managers or parties given license to view and
 * create derivative works for the source code and who have executed
 * Confidentiality and Non-disclosure agreements explicitly covering such
 * access.
 *******************************************************/

package com.brightcreations.gplus.module.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;

/**
 * 
 * @author Bright Creations
 * 
 */

@Entity
@Cache
public class Widget extends LongIdResource implements Comparable<Widget> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5468259430416853013L;

	// @Parent
	// private Key<Dashboard> dashboard;

	// @Index
	// @JsonBackReference
	// private Ref<Dashboard> dashboard;
	@Index
	private String title;

	@Index
	private String contentURL;

	private String table;

	private String trend;

	@Index
	private Key<Study> studyKey;

	private String studyID;
	@Index
	private Key<ApplicationUser> appUserKey;

	private Set<String> questions;

	private Set<String> brands;

	private Set<String> filtersAndDivisions;

	private Set<String> countries;

	private Set<String> waves;

	private Date lastModificationDate;

	private Integer order = 0;
	private String type;

	@Index
	private String description;

	@Index
	private boolean deleted;

	@Index
	private List<Key<PointNotes>> pointNotes = new ArrayList<Key<PointNotes>>();

	public boolean isDeleted() {
		return deleted;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStudyID() {
		return studyID;
	}

	public void setStudyID(String studyID) {
		this.studyID = studyID;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	/**
	 * @param dashboard
	 *            the ParentDashboardRef to set
	 */
	// public void setDashboard(Dashboard value) {
	// dashboard = Key.create(value);
	// }

	/**
	 * @param dashboardId
	 *            the ParentDashboardRef to set
	 */
	// public void setDashboardId(Long dashboardId) {
	// Key<Dashboard> key = Key.create(Dashboard.class, dashboardId);
	// dashboard = key;
	// }

	/**
	 * @return the ParentDashboardRef
	 */
	// @XmlTransient
	// public Key<Dashboard> getDashboard() {
	// return dashboard;
	// }

	// public Ref<Dashboard> getDashboard() {
	// return dashboard;
	// }
	//
	// public void setDashboard(Ref<Dashboard> dashboard) {
	// this.dashboard = dashboard;
	// }

	/**
	 * @return the uRL
	 */
	public String getContentURL() {
		return contentURL;
	}

	public Key<Study> getStudyKey() {
		return studyKey;
	}

	public void setStudyKey(Key<Study> studyKey) {
		this.studyKey = studyKey;
	}

	/**
	 * @param uRL
	 *            the uRL to set
	 */
	public void setContentURL(String uRL) {
		contentURL = uRL;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	public String getTable() {
		return table;
	}

	public void setTable(String table) {
		this.table = table;
	}

	public String getTrend() {
		return trend;
	}

	public void setTrend(String trend) {
		this.trend = trend;
	}

	public Set<String> getQuestions() {
		return questions;
	}

	public void setQuestions(Set<String> questions) {
		this.questions = questions;
	}

	public Set<String> getBrands() {
		return brands;
	}

	public void setBrands(Set<String> brands) {
		this.brands = brands;
	}

	public Set<String> getFiltersAndDivisions() {
		return filtersAndDivisions;
	}

	public void setFiltersAndDivisions(Set<String> filtersAndDivisions) {
		this.filtersAndDivisions = filtersAndDivisions;
	}

	public Set<String> getCountries() {
		return countries;
	}

	public void setCountries(Set<String> countries) {
		this.countries = countries;
	}

	public Set<String> getWaves() {
		return waves;
	}

	public void setWaves(Set<String> waves) {
		this.waves = waves;
	}

	/**
	 * @return the lastModificationDate
	 */
	public Date getLastModificationDate() {
		return lastModificationDate;
	}

	/**
	 * @param lastModificationDate
	 *            the lastModificationDate to set
	 */
	public void setLastModificationDate(Date lastModificationDate) {
		this.lastModificationDate = lastModificationDate;
	}

	/**
	 * @param dashboard
	 *            the dashboard to set
	 */
	// public void setDashboard(Key<Dashboard> dashboard) {
	// this.dashboard = dashboard;
	// }

	/**
	 * @return the order
	 */
	public Integer getOrder() {
		return order;
	}

	/**
	 * @param order
	 *            the order to set
	 */
	public void setOrder(Integer order) {
		this.order = order;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Key<ApplicationUser> getAppUserKey() {
		return appUserKey;
	}

	public void setAppUserKey(Key<ApplicationUser> appUserKey) {
		this.appUserKey = appUserKey;
	}

	public List<Key<PointNotes>> getPointNotes() {
		return pointNotes;
	}

	public void setPointNotes(List<Key<PointNotes>> pointNotes) {
		this.pointNotes = pointNotes;
	}

	public int compareTo(Widget w) {
		return order.compareTo(w.getOrder());
	}

}

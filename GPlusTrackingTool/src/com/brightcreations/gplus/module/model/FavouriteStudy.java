package com.brightcreations.gplus.module.model;

import javax.xml.bind.annotation.XmlRootElement;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;

@Entity
@Cache
@XmlRootElement(name = "study")
public class FavouriteStudy extends LongIdResource implements Comparable<Study> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5160670093532113752L;

	@Index
	private Ref<Study> studyRef;
	@Index
	private Ref<ApplicationUser> appUserRef;
	@Index
	private boolean deleted;

	public Ref<Study> getStudyRef() {
		return studyRef;
	}

	public void setStudyRef(Ref<Study> studyRef) {
		this.studyRef = studyRef;
	}

	public Ref<ApplicationUser> getAppUserRef() {
		return appUserRef;
	}

	public void setAppUserRef(Ref<ApplicationUser> appUserRef) {
		this.appUserRef = appUserRef;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	@Override
	public int compareTo(Study o) {
		// TODO Auto-generated method stub
		return 0;
	}

}

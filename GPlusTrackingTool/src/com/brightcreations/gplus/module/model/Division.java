/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.model;

import static com.brightcreations.gplus.module.controllers.OfyService.ofy;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Parent;

/**
 * @author Ramy Mahmoudi
 * @author Ismail Marmoush
 * @author mohamed.salaheldin
 */
@XmlRootElement(name = "division")
@Entity
@Cache
public class Division extends LongIdResource implements Comparable<Division> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3823345181009826286L;

	@Index
	private String name;

	@Parent
	@JsonBackReference
	private Ref<Filter> filter;

	@Index
	private Key<Filter> filterKey;
	@Index
	@JsonBackReference
	private List<Key<DataSheet>> datasheets = new ArrayList<Key<DataSheet>>();
	@Index
	@JsonBackReference
	private List<Key<DataSet>> dataSets = new ArrayList<Key<DataSet>>();

	@Index
	@JsonBackReference
	private Key<Study> studyKey;

	public Key<Filter> getFilterKey() {
		return filterKey;
	}

	public void setFilterKey(Key<Filter> filterKey) {
		this.filterKey = filterKey;
	}

	public Key<Study> getStudyKey() {
		return studyKey;
	}

	public void setStudyKey(Key<Study> studyKey) {
		this.studyKey = studyKey;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the filter
	 */
	@XmlTransient
	public Ref<Filter> getFilter() {
		return filter;
	}

	/**
	 * @param filter
	 *            the filter to set
	 */
	public void setFilter(Ref<Filter> filter) {
		this.filter = filter;
	}

	/**
	 * @param filter
	 *            the filter to set
	 */
	public void setFilter(Filter filter) {
		this.filter = Ref.create(filter);
	}

	/**
	 * @return the datasheets
	 */
	@XmlTransient
	public List<Key<DataSheet>> getDatasheets() {
		return datasheets;
	}

	/**
	 * @param datasheets
	 *            the datasheets to set
	 */
	public void setDatasheets(List<Key<DataSheet>> datasheets) {
		this.datasheets = datasheets;
	}

	/**
	 * @return the dataSets
	 */
	@XmlTransient
	public List<Key<DataSet>> getDataSets() {
		return dataSets;
	}

	/**
	 * @param dataSets
	 *            the dataSets to set
	 */
	public void setDataSets(List<Key<DataSet>> dataSets) {
		this.dataSets = dataSets;
	}

	/**
	 */
	public Long getFilterId() {
		return filter.getKey().getId();
	}

	public static List<Division> getDivisionList() {
		return ofy().load().type(Division.class).list();
	}

	public static List<Division> getDivisionList(String sortBy, String order, Long from, Long to) {
		Long limit = to - from;
		String sort = (order.equals("desc")) ? "-" + sortBy : sortBy;
		List<Division> list = ofy().load().type(Division.class).limit(limit.intValue()).order(sort)
				.offset(from.intValue()).list();
		return list;
	}

	public static Division getDivision(Long id) {
		return ofy().load().type(Division.class).id(id).get();
	}

	public void save() {
		ofy().save().entity(this);
	}

	public int compareTo(Division d) {
		return name.compareTo(d.getName());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */

	public int hashCode() {
		final int prime = 31;
		int result = prime + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (getClass() != obj.getClass())
			return false;
		Division other = (Division) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}

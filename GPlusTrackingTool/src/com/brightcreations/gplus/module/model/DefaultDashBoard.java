/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.model;

import java.util.Date;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.IgnoreLoad;
import com.googlecode.objectify.annotation.IgnoreSave;
import com.googlecode.objectify.annotation.Index;

/**
 * 
 * @author Mohamed Salah El-Din
 * 
 */
@Entity
public class DefaultDashBoard extends LongIdResource {

	private static final long serialVersionUID = 868545027499705078L;
	@Index
	private Key<Dashboard> dashboard;

	@IgnoreSave
	@IgnoreLoad
	private Location locationObject;
	@IgnoreSave
	@IgnoreLoad
	private Dashboard dashboardObject;

	@Index
	private Date lastModificationDate;

	@Index
	private boolean deleted;

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public Key<Dashboard> getDashboard() {
		return dashboard;
	}

	public void setDashboard(Key<Dashboard> dashboard) {
		this.dashboard = dashboard;
	}

	public Date getLastModificationDate() {
		return lastModificationDate;
	}

	public void setLastModificationDate(Date lastModificationDate) {
		this.lastModificationDate = lastModificationDate;
	}

	/**
	 * @return the locationObject
	 */
	public Location getLocationObject() {
		return locationObject;
	}

	/**
	 * @param locationObject
	 *            the locationObject to set
	 */
	public void setLocationObject(Location locationObject) {
		this.locationObject = locationObject;
	}

	/**
	 * @return the dashboardObject
	 */
	public Dashboard getDashboardObject() {
		return dashboardObject;
	}

	/**
	 * @param dashboardObject
	 *            the dashboardObject to set
	 */
	public void setDashboardObject(Dashboard dashboardObject) {
		this.dashboardObject = dashboardObject;
	}
}

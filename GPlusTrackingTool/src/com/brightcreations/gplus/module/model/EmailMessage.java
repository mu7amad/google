/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.model;

import com.googlecode.objectify.annotation.Entity;

/**
 * 
 * @author IsmailMarmoush
 * 
 */
@Entity
public class EmailMessage extends LongIdResource {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3376291507491245062L;
	public static final String SENT = "sent";
	public static final String NOT_SENT = "notsent";
	public static final String INVALID_ADDRESS = "invalidaddress";
	public static final String INVALID_MESSAGE = "invalidmessage";

	// Default status
	private String status = NOT_SENT;
	private String fromAddress;
	private String toAddress;
	private String ccAddress;
	private String bccAddress;
	private String subject;
	private String content;

	public String getBccAddress() {
		return bccAddress;
	}

	public String getCcAddress() {
		return ccAddress;
	}

	public String getContent() {
		return content;
	}

	public String getFromAddress() {
		return fromAddress;
	}

	public String getSubject() {
		return subject;
	}

	public String getStatus() {
		return status;
	}

	public String getToAddress() {
		return toAddress;
	}

	public void setBccAddress(String bccAddresse) {
		this.bccAddress = bccAddresse;
	}

	public void setCcAddress(String ccAddresse) {
		this.ccAddress = ccAddresse;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	public void setSubject(String messageBody) {
		this.subject = messageBody;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setToAddress(String toAddresse) {
		this.toAddress = toAddresse;
	}
}

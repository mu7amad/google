/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.model;

import static com.brightcreations.gplus.module.controllers.OfyService.ofy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;

/**
 * @author Ramy Mahmoudi
 * @author Ismail Marmoush
 * @author mohamed.salaheldin
 * @author mohammed.eladly
 */
@XmlRootElement(name = "question")
@Entity
@Cache
public class Question extends LongIdResource implements Comparable<Question> {

	private static final long serialVersionUID = -9131295957439966071L;

	/* ______________________________________________________________________________________ */
	/*************************************** Vars ********************************************/
	/* ____________________________________________________________________________________ */
	@Index
	private String questionText;
	// @JsonBackReference
	private List<Key<Answer>> answers;
	// @JsonBackReference
	private List<Key<DataSheet>> datasheets;
	@Index
	// @JsonBackReference
	private List<Key<DataSet>> dataSets;
	// @JsonBackReference
	private List<Key<Brand>> brands;
	private String description;
	private List<String> answerGroups;
	private String base;
	@Index
	// @JsonBackReference
	private Key<Category> category;

	@Transient
	private List<Answer> transientAnswers = new ArrayList<Answer>();
	@Transient
	private Map<String, ArrayList<String>> transientFilters = new HashMap<String, ArrayList<String>>();
	private Integer listOrder = 0;
	String buzzWord;
	@Index
	// @JsonBackReference
	private Key<Study> studyKey;

	@Index
	// @JsonBackReference
	private String questionIndex;

	@Index
	private boolean deleted;

	/* ______________________________________________________________________________________ */
	/*************************************** Functions ***************************************/
	/* ____________________________________________________________________________________ */

	public boolean isDeleted() {
		return deleted;
	}

	public Map<String, ArrayList<String>> getTransientFilters() {
		return transientFilters;
	}

	public void setTransientFilters(Map<String, ArrayList<String>> transientFilters) {
		this.transientFilters = transientFilters;
	}

	public List<Answer> getTransientAnswers() {
		return transientAnswers;
	}

	public void setTransientAnswers(List<Answer> transientAnswers) {
		this.transientAnswers = transientAnswers;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public String getQuestionText() {
		return questionText;
	}

	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}

	/**
	 * @return the answers
	 */

	@XmlTransient
	public List<Key<Answer>> getAnswers() {
		return answers;
	}

	public void setAnswers(List<Key<Answer>> answers) {
		this.answers = answers;
	}

	@XmlTransient
	public List<Key<DataSheet>> getDatasheets() {
		return datasheets;
	}

	public void setDatasheets(List<Key<DataSheet>> datasheets) {
		this.datasheets = datasheets;
	}

	@XmlTransient
	public List<Key<DataSet>> getDataSets() {
		return dataSets;
	}

	public void setDataSets(List<Key<DataSet>> dataSets) {
		this.dataSets = dataSets;
	}

	@XmlTransient
	public List<Key<Brand>> getBrands() {
		return brands;
	}

	public List<Long> getBrandsIds() {
		ArrayList<Long> brandsIds = new ArrayList<Long>();
		for (Key<Brand> brand : brands) {
			brandsIds.add(brand.getId());

		}
		return brandsIds;
	}

	public void setBrands(List<Key<Brand>> brands) {
		this.brands = brands;
	}

	public static List<Question> getQuestionList() {
		return ofy().load().type(Question.class).list();
	}

	public static List<Question> getQuestionList(String sortBy, String order, Long from, Long to) {
		Long limit = to - from;
		String sort = (order.equals("desc")) ? "-" + sortBy : sortBy;
		List<Question> list = ofy().load().type(Question.class).limit(limit.intValue()).order(sort)
				.offset(from.intValue()).list();
		return list;
	}

	public static Question getQuestion(Long id) {
		return ofy().load().type(Question.class).id(id).get();
	}

	/**
	 * @return the buzzWord
	 */
	public String getBuzzWord() {
		return buzzWord;
	}

	/**
	 * @param buzzWord
	 *            the buzzWord to set
	 */
	public void setBuzzWord(String buzzWord) {
		this.buzzWord = buzzWord;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the answerGroups
	 */
	public List<String> getAnswerGroups() {
		return answerGroups;
	}

	/**
	 * @param answerGroups
	 *            the answerGroups to set
	 */
	public void setAnswerGroups(List<String> answerGroups) {
		this.answerGroups = answerGroups;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */

	// public void save() {
	// ofy().save().entity(this);
	// }

	public int compareTo(Question q) {
		return questionText.compareTo(q.getQuestionText());
	}

	/**
	 * @return the base
	 */
	public String getBase() {
		return base;
	}

	/**
	 * @param base
	 *            the base to set
	 */
	public void setBase(String base) {
		this.base = base;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */

	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime + ((questionText == null) ? 0 : questionText.hashCode());// prime
																				// *
		// result
		// + ((name
		// == null) ? 0 // name.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	//
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (getClass() != obj.getClass())
			return false;
		Question other = (Question) obj;
		if (questionText == null) {
			if (other.questionText != null)
				return false;
		} else if (!questionText.equals(other.questionText))
			return false;
		return true;
	}

	/**
	 * @return the category
	 */
	@XmlTransient
	public Key<Category> getCategory() {
		return category;
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public void setCategory(Key<Category> category) {
		this.category = category;
	}

	/**
	 * @return the listOrder
	 */
	public Integer getListOrder() {
		return listOrder;
	}

	/**
	 * @param listOrder
	 *            the listOrder to set
	 */
	public void setListOrder(Integer listOrder) {
		this.listOrder = listOrder;
	}

	public Key<Study> getStudyKey() {
		return studyKey;
	}

	public void setStudyKey(Key<Study> studyKey) {
		this.studyKey = studyKey;
	}

	public String getQuestionIndex() {
		return questionIndex;
	}

	public void setQuestionIndex(String questionIndex) {
		this.questionIndex = questionIndex;
	}

}

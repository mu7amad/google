/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.IgnoreLoad;
import com.googlecode.objectify.annotation.IgnoreSave;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Parent;

/**
 * 
 * @author Mohamed Salah El-Din
 * 
 */
@Entity
public class Dashboard extends LongIdResource {
	/**
	 * 
	 */
	private static final long serialVersionUID = 103287187959869623L;

	@Index
	@JsonBackReference
	private Ref<ApplicationUser> applicationUsers;

	// @Parent
	// @JsonBackReference
	// private Ref<ApplicationUser> dashboardUser;

	private Boolean published = false;

	private String name;
	@Index
	@JsonBackReference
	private Boolean defaultDashBoard = false;

	@JsonBackReference
	private Set<String> usersSharedWith = new HashSet<String>();

	@IgnoreSave
	@IgnoreLoad
	private Long userId;
	@Index
	@JsonBackReference
	private List<Key<Study>> studies = new ArrayList<>();

	@Index
	private boolean deleted;

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	/**
	 * @param dashboardUser
	 *            the dashboardUser to set
	 */
	// public void setDashboardUser(ApplicationUser value) {
	// dashboardUser = Ref.create(value);
	// }

	/**
	 * @return the dashboardUser
	 */
	// public ApplicationUser getDashboardUser() {
	// if (dashboardUser != null) {
	// return dashboardUser.getValue();
	// } else {
	// return null;
	// }
	//
	// }

	public Ref<ApplicationUser> getApplicationUsers() {
		return applicationUsers;
	}

	public void setApplicationUsers(Ref<ApplicationUser> applicationUsers) {
		this.applicationUsers = applicationUsers;
	}

	/**
	 * @return the published
	 */
	public Boolean getPublished() {
		return published;
	}

	/**
	 * @param published
	 *            the published to set
	 */
	public void setPublished(Boolean published) {
		this.published = published;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	public Boolean getDefaultDashBoard() {
		return defaultDashBoard;
	}

	public void setDefaultDashBoard(Boolean defaultDashBoard) {
		this.defaultDashBoard = defaultDashBoard;
	}

	/**
	 * @return the dashboardUser
	 */
	// @XmlTransient
	// public Ref<ApplicationUser> getDashboardUserRef() {
	// return dashboardUser;
	// }

	/**
	 * @param dashboardUser
	 *            the dashboardUser to set
	 */
	// public void setDashboardUser(Ref<ApplicationUser> dashboardUser) {
	// this.dashboardUser = dashboardUser;
	// }

	/**
	 * @return the usersSharedWith
	 */
	public Set<String> getUsersSharedWith() {
		return usersSharedWith;
	}

	/**
	 * @param usersSharedWith
	 *            the usersSharedWith to set
	 */
	public void setUsersSharedWith(Set<String> usersSharedWith) {
		this.usersSharedWith = usersSharedWith;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public List<Key<Study>> getStudies() {
		return studies;
	}

	public void setStudies(List<Key<Study>> studies) {
		this.studies = studies;
	}

}

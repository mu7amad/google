/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.model;

import static com.brightcreations.gplus.module.controllers.OfyService.ofy;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;

/**
 * @author Ramy Mahmoudi
 * @author Ismail Marmoush
 * @author mohammed.eladly
 * 
 */
@XmlRootElement(name = "brand")
@Entity
@Cache
public class Brand extends LongIdResource implements Comparable<Brand> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2076723930505029796L;

	@Index
	private String name;
//	@JsonBackReference
	private List<Key<DataSheet>> datasheets = new ArrayList<Key<DataSheet>>();
//	@JsonBackReference
	private List<Key<DataSet>> dataSets = new ArrayList<Key<DataSet>>();

	@Index
//	@JsonBackReference
	private List<Key<Study>> studies = new ArrayList<>();

	@Index
	private boolean deleted;

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the datasheets
	 */
	@XmlTransient
	public List<Key<DataSheet>> getDatasheets() {
		return datasheets;
	}

	/**
	 * @param datasheets
	 *            the datasheets to set
	 */
	public void setDatasheets(List<Key<DataSheet>> datasheets) {
		this.datasheets = datasheets;
	}

	/**
	 * @return the dataSets
	 */
	@XmlTransient
	public List<Key<DataSet>> getDataSets() {
		return dataSets;
	}

	/**
	 * @param dataSets
	 *            the dataSets to set
	 */
	public void setDataSets(List<Key<DataSet>> dataSets) {
		this.dataSets = dataSets;
	}

	public static List<Brand> getLocationList() {
		return ofy().load().type(Brand.class).list();
	}

	public static List<Brand> getBrandList(String sortBy, String order, Long from, Long to) {
		Long limit = to - from;
		String sort = (order.equals("desc")) ? "-" + sortBy : sortBy;
		List<Brand> list = ofy().load().type(Brand.class).limit(limit.intValue()).order(sort).offset(from.intValue())
				.list();
		return list;
	}

	public static Brand getBrand(Long id) {
		return ofy().load().type(Brand.class).id(id).get();
	}

	public void save() {
		ofy().save().entity(this);
	}

	public int compareTo(Brand b) {
		return name.compareTo(b.getName());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */

	public int hashCode() {
		final int prime = 31;
		int result = prime + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (getClass() != obj.getClass())
			return false;
		Brand other = (Brand) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public List<Key<Study>> getStudies() {
		return studies;
	}

	public void setStudies(List<Key<Study>> studies) {
		this.studies = studies;
	}

}

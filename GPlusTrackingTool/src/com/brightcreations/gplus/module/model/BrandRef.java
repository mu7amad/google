/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.model;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Parent;

/**
 * @author Ramy Mahmoudi
 */

@Entity
public class BrandRef extends LongIdResource {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8117093418956605394L;

	@Parent
	Ref<LocationRef> locationRef;

	/**
	 * @return the locationRef
	 */
	public Ref<LocationRef> getLocationRef() {
		return locationRef;
	}

	/**
	 * @param locationRef
	 *            the locationRef to set
	 */
	public void setLocationRef(Ref<LocationRef> locationRef) {
		this.locationRef = locationRef;
	}

	/**
	 * @param locationRef
	 *            the locationRef to set
	 */
	public void setLocationRef(LocationRef locationRef) {
		this.locationRef = Ref.create(locationRef);
	}

}

package com.brightcreations.gplus.module.model;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;

@Entity
@Cache
@XmlRootElement(name = "dataRow")
public class DataRow extends LongIdResource implements Comparable<DataRow> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6207759200254365380L;

	@Index
	private RowType rowType;

	private String rowValue;

	@Index
	private Key<DataSheet> datasheetKey;

	@Index
	private int rowNumber;

	public RowType getRowType() {
		return rowType;
	}

	public void setRowType(RowType rowType) {
		this.rowType = rowType;
	}

	public String getRowValue() {
		return rowValue;
	}

	public void setRowValue(String rowValue) {
		this.rowValue = rowValue;
	}

	public Key<DataSheet> getDatasheetKey() {
		return datasheetKey;
	}

	public void setDatasheetKey(Key<DataSheet> datasheetKey) {
		this.datasheetKey = datasheetKey;
	}

	public int getRowNumber() {
		return rowNumber;
	}

	public void setRowNumber(int rowNumber) {
		this.rowNumber = rowNumber;
	}

	@Override
	public int compareTo(DataRow o) {
		// TODO Auto-generated method stub
		return 0;
	}

}

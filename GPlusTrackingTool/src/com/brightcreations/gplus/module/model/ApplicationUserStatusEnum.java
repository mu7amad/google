package com.brightcreations.gplus.module.model;

public enum ApplicationUserStatusEnum {
	ACTIVE, INACTIVE
}

package com.brightcreations.gplus.module.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;

@Entity
@Cache
@XmlRootElement(name = "adminToken")
public class AdminToken extends LongIdResource implements Comparable<AdminToken> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8456573568740440911L;

	@Index
	private String token;

	@Index
	@JsonBackReference
	private Key<ApplicationUser> appUserKey;

	@Index
	private boolean valid;
	@Index
	private Date creationDate;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Key<ApplicationUser> getAppUserKey() {
		return appUserKey;
	}

	public void setAppUserKey(Key<ApplicationUser> appUserKey) {
		this.appUserKey = appUserKey;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public int compareTo(AdminToken o) {
		// TODO Auto-generated method stub
		return 0;
	}

}

package com.brightcreations.gplus.module.model;

import javax.xml.bind.annotation.XmlRootElement;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;

@Entity
@XmlRootElement(name = "unregistereduser")
public class UnRegisteredUser extends LongIdResource implements Comparable<Study> {

	private static final long serialVersionUID = 2829107086429641724L;

	@Index
	private String email;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public int compareTo(Study arg0) {
		return 0;
	}

}

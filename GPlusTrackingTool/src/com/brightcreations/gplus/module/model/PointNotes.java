/*******************************************************
 * Copyright (c) 2016 Bright Creations , All Rights Reserved. NOTICE: All
 * information contained herein is, and remains the property of Bright
 * Creations. Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained Access to
 * the source code contained herein is hereby forbidden to anyone except current
 * Bright Creations employees, managers or parties given license to view and
 * create derivative works for the source code and who have executed
 * Confidentiality and Non-disclosure agreements explicitly covering such
 * access.
 *******************************************************/

package com.brightcreations.gplus.module.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;

/**
 * 
 * @author Mohammed ElAdly
 * 
 */

@Entity
@Cache
@XmlRootElement(name = "pointNotes")
public class PointNotes extends LongIdResource implements Comparable<PointNotes> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2915389041336154324L;

	@Index
	@JsonBackReference
	private Ref<FilteredAnswerValue> filterdAnswerValue;

	@Index
	@JsonBackReference
	private Ref<ApplicationUser> applicationUser;
	@Index
	private String comment;
	@Index
	private Date creationDate;
	@Index
	private String title;
	@Index
	private long filterAnswerID;
	@Index
	private boolean deleted;

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public Ref<FilteredAnswerValue> getFilterdAnswerValue() {
		return filterdAnswerValue;
	}

	public void setFilterdAnswerValue(Ref<FilteredAnswerValue> filterdAnswerValue) {
		this.filterdAnswerValue = filterdAnswerValue;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public long getFilterAnswerID() {
		return filterAnswerID;
	}

	public void setFilterAnswerID(long filterAnswerID) {
		this.filterAnswerID = filterAnswerID;
	}

	public Ref<ApplicationUser> getApplicationUser() {
		return applicationUser;
	}

	public void setApplicationUser(Ref<ApplicationUser> applicationUser) {
		this.applicationUser = applicationUser;
	}

	@Override
	public int compareTo(PointNotes o) {
		// TODO Auto-generated method stub
		return 0;
	}
}

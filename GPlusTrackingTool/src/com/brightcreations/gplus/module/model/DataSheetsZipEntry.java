/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.appengine.api.blobstore.BlobKey;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;

/**
 * 
 * @author Bright Creations
 * 
 */

@Entity
public class DataSheetsZipEntry extends LongIdResource implements Comparable<DataSheetsZipEntry> {

	private static final long serialVersionUID = -4568424923474641732L;

	@Index
	private String name;
	@Index
	private List<Key<DataSheet>> datasheets = new ArrayList<Key<DataSheet>>();

	@Index
	private Date uploadDate;
	@Index
	private BlobKey blobstoreKey;
	@Index
	private Ref<Study> studyRef;
	@Index
	private String uploadedBy;
	@Index
	private boolean deleted;
	private long zipFileSize;

	public long getZipFileSize() {
		return zipFileSize;
	}

	public void setZipFileSize(long zipFileSize) {
		this.zipFileSize = zipFileSize;
	}

	public String getUploadedBy() {
		return uploadedBy;
	}

	public void setUploadedBy(String uploadedBy) {
		this.uploadedBy = uploadedBy;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the datasheets
	 */
	public List<Key<DataSheet>> getDatasheets() {
		return datasheets;
	}

	/**
	 * @param datasheets
	 *            the datasheets to set
	 */
	public void setDatasheets(List<Key<DataSheet>> datasheets) {
		this.datasheets = datasheets;
	}

	/**
	 * @return the uploadDate
	 */
	public Date getUploadDate() {
		return uploadDate;
	}

	/**
	 * @param uploadDate
	 *            the uploadDate to set
	 */
	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}

	public BlobKey getBlobstoreKey() {
		return blobstoreKey;
	}

	public void setBlobstoreKey(BlobKey blobstoreKey) {
		this.blobstoreKey = blobstoreKey;
	}

	public Ref<Study> getStudyRef() {
		return studyRef;
	}

	public void setStudyRef(Ref<Study> studyRef) {
		this.studyRef = studyRef;
	}

	@Override
	public int compareTo(DataSheetsZipEntry o) {
		return getUploadDate().compareTo(o.getUploadDate());
	}

}

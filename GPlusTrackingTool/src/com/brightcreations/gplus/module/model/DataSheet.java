/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.model;

import static com.brightcreations.gplus.module.controllers.OfyService.ofy;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.google.appengine.api.blobstore.BlobKey;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;

/**
 * 
 * @author Mohamed Salah El-Din
 * @author Ismail Marmoush
 */

@Entity
@Cache
@XmlRootElement(name = "dataSheet")
public class DataSheet extends LongIdResource {

	@Index
	// @JsonBackReference
	private boolean owner;
	@Index
	// @JsonBackReference
	private Ref<DataSet> dataSet;
	@Index
	// @JsonBackReference
	private Ref<Study> study;
	@Index
	private String name;
	@Index
	private String status;
	@Index
	private Date uploadDate;
	@Index
	private BlobKey blobstoreKey;
	@Index
	private String uploadedBy;
	@Index
	private Long size;
	private String format;
	@Index
	private String createdByEmail;
	@Index
	// @JsonBackReference
	private List<Key<Question>> questions = new ArrayList<Key<Question>>();
	// @JsonBackReference
	private List<Key<Answer>> answers = new ArrayList<Key<Answer>>();
	@Index
	// @JsonBackReference
	private List<Key<Brand>> brands = new ArrayList<Key<Brand>>();
	@Index
	// @JsonBackReference
	private List<Key<Filter>> filters = new ArrayList<Key<Filter>>();
	// @JsonBackReference
	private List<Key<Division>> divisions = new ArrayList<Key<Division>>();

	@Index
	// @JsonBackReference
	private Key<Location> location;
	@Index
	private boolean deleted;

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public boolean isOwner() {
		return owner;
	}

	public void setOwner(boolean owner) {
		this.owner = owner;
	}

	public enum dataSheetStatus {

		UPLOADED(1), VALIDATED(2), INVALID(3), PROCESSING(4), PROCESSED(5), ROLLINGBACK(6);

		private int bit;

		dataSheetStatus(int bit) {
			this.setBit(bit);
		}

		public String getStatus() {
			return toString();
		}

		/**
		 * @param bit
		 *            the bit to set
		 */
		public void setBit(int bit) {
			this.bit = bit;
		}

		/**
		 * @return the bit
		 */
		public int getBit() {
			return bit;
		}
	}

	/**
	 * @return the answers
	 */
	public List<Key<Answer>> getAnswers() {
		return answers;
	}

	/**
	 * @return the blobstoreKey
	 */
	public BlobKey getBlobstoreKey() {
		return blobstoreKey;
	}

	/**
	 * @return the brands
	 */
	public List<Key<Brand>> getBrands() {
		return brands;
	}

	public String getCreatedByEmail() {
		return createdByEmail;
	}

	/**
	 * @return the dataset
	 */
	public Ref<DataSet> getDatasetRef() {
		return dataSet;
	}

	/**
	 * @return the divisions
	 */
	public List<Key<Division>> getDivisions() {
		return divisions;
	}

	/**
	 * @return the filters
	 */
	public List<Key<Filter>> getFilters() {
		return filters;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the questions
	 */
	public List<Key<Question>> getQuestions() {
		return questions;
	}

	/**
	 * @return the staus
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * 
	 * @param staus
	 *            the staus to set
	 */
	public void setStatus(String staus) {
		this.status = staus;
	}

	/**
	 * 
	 * @return the uploadDate
	 */
	public Date getUploadDate() {
		return uploadDate;
	}

	public void save() {
		ofy().save().entity(this).now();
	}

	/**
	 * @param answers
	 *            the answers to set
	 */
	public void setAnswers(List<Key<Answer>> answers) {
		this.answers = answers;
	}

	/**
	 * @param blobstoreKey
	 *            the blobstoreKey to set
	 */
	public void setBlobstoreKey(BlobKey blobstoreKey) {
		this.blobstoreKey = blobstoreKey;
	}

	/**
	 * @param brands
	 *            the brands to set
	 */
	public void setBrands(List<Key<Brand>> brands) {
		this.brands = brands;
	}

	public void setCreatedByEmail(String createdByEmail) {
		this.createdByEmail = createdByEmail;
	}

	/**
	 * @param dataset
	 *            the dataset to set
	 */
	public void setDatasetRef(Ref<DataSet> dataset) {
		this.dataSet = dataset;
	}

	/**
	 * @param divisions
	 *            the divisions to set
	 */
	public void setDivisions(List<Key<Division>> divisions) {
		this.divisions = divisions;
	}

	/**
	 * @param filters
	 *            the filters to set
	 */
	public void setFilters(List<Key<Filter>> filters) {
		this.filters = filters;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param questions
	 *            the questions to set
	 */
	public void setQuestions(List<Key<Question>> questions) {
		this.questions = questions;
	}

	/**
	 * @param uploadDate
	 *            the uploadDate to set
	 */
	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}

	/**
	 * @return the uploadedBy
	 */
	public String getUploadedBy() {
		return uploadedBy;
	}

	/**
	 * @param uploadedBy
	 *            the uploadedBy to set
	 */
	public void setUploadedBy(String uploadedBy) {
		this.uploadedBy = uploadedBy;
	}

	/**
	 * @return the size
	 */
	public Long getSize() {
		return size;
	}

	/**
	 * @param size
	 *            the size to set
	 */
	public void setSize(Long size) {
		this.size = size;
	}

	/**
	 * @return the format
	 */
	public String getFormat() {
		return format;
	}

	/**
	 * @param format
	 *            the format to set
	 */
	public void setFormat(String format) {
		this.format = format;
	}

	private static final long serialVersionUID = -2220926053525560285L;

	public static DataSheet getDataSheet(Long id) {
		return ofy().load().type(DataSheet.class).id(id).get();
	}

	public static List<DataSheet> getDataSheetList() {
		return ofy().load().type(DataSheet.class).list();
	}

	/**
	 * @return the location
	 */
	public Key<Location> getLocation() {
		return location;
	}

	/**
	 * @param location
	 *            the location to set
	 */
	public void setLocation(Key<Location> location) {
		this.location = location;
	}

	public Ref<Study> getStudy() {
		return study;
	}

	public void setStudy(Ref<Study> study) {
		this.study = study;
	}

	// public static List<DataSheet> getDataSheetList(String sortBy, String
	// order,
	// Integer from, Integer to) {
	// Integer limit = to - from;
	// if (sortBy == null)
	// sortBy = "name";
	// if (order == null)
	// order = "asc";
	// String sortOrder = (order.equals("asc")) ? sortBy : "-" + sortBy;
	// List<DataSheet> list = ofy().load().type(DataSheet.class).limit(limit)
	// .order(sortOrder).offset(from).list();
	// return list;
	// }
}

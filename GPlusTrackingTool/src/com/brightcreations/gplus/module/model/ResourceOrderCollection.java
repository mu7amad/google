/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.model;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Ramy Mahmoudi
 */
public class ResourceOrderCollection {
	Map<Long, Integer> rom = new HashMap<Long, Integer>();

	/**
	 * @return the resourcesOrderMapById
	 */
	public Map<Long, Integer> getRom() {
		return rom;
	}

	/**
	 * @param resourcesOrderMapById
	 *            the filtersOrderMapById to set
	 */
	public void setFom(Map<Long, Integer> rom) {
		this.rom = rom;
	}

}

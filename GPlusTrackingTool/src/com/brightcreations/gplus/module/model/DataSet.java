/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.model;

import static com.brightcreations.gplus.module.controllers.OfyService.ofy;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.IgnoreSave;
import com.googlecode.objectify.annotation.Index;

/**
 * 
 * @author Mohamed Salah El-Din
 * @author Ismail Marmoush
 * @author mohammed.eladly
 */
@XmlRootElement(name = "dataset")
@Entity
@Cache
public class DataSet extends LongIdResource implements Comparable<DataSet> {
	private static final long serialVersionUID = -4427450666498114208L;

	@Index
	private boolean owner;
	@Index
//	@JsonBackReference
	private Ref<Study> study;
	@Index
	private Date waveDate;
	@Index
	private Date creationDate;
	@Index
	private Date lastModificationDate;
	@Index
	private String name;

	@Index
	private String nameNorm;

	@Index
	@IgnoreSave
	private Integer numberOfFiles;

	private Text description;

//	@JsonBackReference
	private List<Key<Location>> locations = new ArrayList<Key<Location>>();

	@Index
//	@JsonBackReference
	private List<Key<Question>> questions = new ArrayList<Key<Question>>();

//	@JsonBackReference
	private List<Key<Answer>> answers = new ArrayList<Key<Answer>>();
	@Index
//	@JsonBackReference
	private List<Key<Brand>> brands = new ArrayList<Key<Brand>>();

	@Index
//	@JsonBackReference
	private List<Key<Filter>> filters = new ArrayList<Key<Filter>>();

//	@JsonBackReference
	private List<Key<Division>> divisions = new ArrayList<Key<Division>>();

	@Index
//	@JsonBackReference
	private boolean published = false;

	@Index
//	@JsonBackReference
	private boolean showenInSnapshotOnly = false;
	@Index
	private boolean deleted;

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public void create() {
		this.creationDate = new Date();
		ofy().save().entity(this).now();
	}

	public boolean isOwner() {
		return owner;
	}

	public void setOwner(boolean isOwner) {
		this.owner = isOwner;
	}

	/**
	 * @return the answers
	 */
	@XmlTransient
	public List<Key<Answer>> getAnswers() {
		return answers;
	}

	/**
	 * @return the brands
	 */
	@XmlTransient
	public List<Key<Brand>> getBrands() {
		return brands;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public Text getDescription() {
		return description;
	}

	/**
	 * @return the divisions
	 */
	@XmlTransient
	public List<Key<Division>> getDivisions() {
		return divisions;
	}

	/**
	 * @return the filters
	 */
	@XmlTransient
	public List<Key<Filter>> getFilters() {
		return filters;
	}

	/**
	 * @return the lastModificationDate
	 */
	public Date getLastModificationDate() {
		return lastModificationDate;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param questions
	 *            the questions to set
	 */
	/*
	 * public void setQuestions(List<Question> questions) {
	 * ArrayList<Key<Question>> questionsKeys = new ArrayList<Key<Question>>();
	 * for (Question question : questions) {
	 * questionsKeys.add(Key.create(Question.class, question.getId())); }
	 * this.questions = questionsKeys ; }
	 */

	public String getNameNorm() {
		return nameNorm;
	}

	public Integer getNumberOfFiles() {
		return numberOfFiles;
	}

	/**
	 * @return the questions
	 */
	public List<Key<Question>> getQuestions() {
		return questions;
	}

	/**
	 * @return the date
	 */
	public Date getWaveDate() {
		return waveDate;
	}

	/**
	 * @return the published
	 */
	public boolean isPublished() {
		return published;
	}

	public void save() {
		this.lastModificationDate = new Date();
		ofy().save().entity(this).now();
	}

	/**
	 * @param answers
	 *            the answers to set
	 */
	public void setAnswers(List<Key<Answer>> answers) {
		this.answers = answers;
	}

	/**
	 * @param brands
	 *            the brands to set
	 */
	public void setBrands(List<Key<Brand>> brands) {
		this.brands = brands;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public void setDescription(Text description) {
		this.description = description;
	}

	/**
	 * @param divisions
	 *            the divisions to set
	 */
	public void setDivisions(List<Key<Division>> divisions) {
		this.divisions = divisions;
	}

	/**
	 * @param filters
	 *            the filters to set
	 */
	public void setFilters(List<Key<Filter>> filters) {
		this.filters = filters;
	}

	/**
	 * @param lastModificationDate
	 *            the lastModificationDate to set
	 */
	public void setLastModificationDate(Date lastModificationDate) {
		this.lastModificationDate = lastModificationDate;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	public void setNameNorm(String nameNorm) {
		this.nameNorm = nameNorm;
	}

	public void setNumberOfFiles(Integer numberOfFiles) {
		this.numberOfFiles = numberOfFiles;
	}

	/**
	 * @param published
	 *            the published to set
	 */
	public void setPublished(boolean published) {
		this.published = published;
	}

	/**
	 * @param questions
	 *            the questions to set
	 */
	public void setQuestions(List<Key<Question>> questions) {
		this.questions = questions;
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public void setWaveDate(Date date) {
		this.waveDate = date;
	}

	public void setWaveDateString(String date) throws ParseException {
		// System.out.println(date);
		if (!date.isEmpty()) {
			try {
				this.waveDate = new SimpleDateFormat(DATE_PATTERN).parse(date);
			} catch (final ParseException e) {
				e.printStackTrace();
			}
		}
	}

	public static DataSet getDataSet(Long id) {
		return ofy().load().type(DataSet.class).id(id).get();
	}

	public static List<DataSet> getDataSetList() {
		return ofy().load().type(DataSet.class).list();
	}

	public static void Delete(Long id) {
		ofy().delete().key(Key.create(DataSet.class, id)).now();
	}

	/**
	 * @return the locations
	 */
	public List<Key<Location>> getLocations() {
		return locations;
	}

	/**
	 * @param locations
	 *            the locations to set
	 */
	public void setLocations(List<Key<Location>> locations) {
		this.locations = locations;
	}

	public int compareTo(DataSet d) {
		return waveDate.compareTo(d.getWaveDate());
	}

	/**
	 * @return the showenInSnapshotOnly
	 */
	public boolean isShowenInSnapshotOnly() {
		return showenInSnapshotOnly;
	}

	/**
	 * @param showenInSnapshotOnly
	 *            the showenInSnapshotOnly to set
	 */
	public void setShowenInSnapshotOnly(boolean showenInSnapshotOnly) {
		this.showenInSnapshotOnly = showenInSnapshotOnly;
	}

	public Ref<Study> getStudy() {
		return study;
	}

	public void setStudy(Ref<Study> study) {
		this.study = study;
	}

}

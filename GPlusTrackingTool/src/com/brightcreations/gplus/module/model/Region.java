/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.model;

import static com.brightcreations.gplus.module.controllers.OfyService.ofy;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;

/**
 * @author Mohamed Salah El-Din
 */

@Entity
@Cache
public class Region extends LongIdResource {

	private static final long serialVersionUID = -6816134905613330646L;
	@Index
	private String name;

	@Index
	private List<Key<Location>> locations = new ArrayList<Key<Location>>();

	@Index
	private boolean deleted;

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the locations
	 */
	public List<Key<Location>> getLocations() {
		return locations;
	}

	/**
	 * @param locations
	 *            the locations to set
	 */
	public void setLocations(List<Key<Location>> locations) {
		this.locations = locations;
	}

	public static List<Region> getRegionList() {
		return ofy().load().type(Region.class).list();
	}

	public static List<Region> getRegionList(String sortBy, String order, Long from, Long to) {
		Long limit = to - from;
		String sort = (order.equals("desc")) ? "-" + sortBy : sortBy;
		List<Region> list = ofy().load().type(Region.class).limit(limit.intValue()).order(sort).offset(from.intValue())
				.list();
		return list;
	}

	public static Region getRegion(Long id) {
		return ofy().load().type(Region.class).id(id).get();
	}

	public void save() {
		ofy().save().entity(this).now();
	}
}

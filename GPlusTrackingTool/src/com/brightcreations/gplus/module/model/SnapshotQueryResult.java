/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.brightcreations.gplus.module.controllers.OfyService;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;

/**
 * 
 * @author Mohamed Salah El-Din
 * 
 */
public class SnapshotQueryResult {

	private List<Brand> brands = new ArrayList<Brand>();
	private Map<Filter, List<Division>> filtersAndDivisionsByUser = new HashMap<Filter, List<Division>>();
	private List<String> tableDivisions = new ArrayList<String>();
	private Map<Filter, List<Division>> filtersAndDivisions = new HashMap<Filter, List<Division>>();
	private List<Question> questions = new ArrayList<Question>();
	private List<Filter> filters = new ArrayList<Filter>();
	private Map<Key<Division>, Division> allDivisions = new HashMap<Key<Division>, Division>();
	private Map<Key<Filter>, List<Key<Division>>> filtersAndDivisionsKeysByUser = new HashMap<Key<Filter>, List<Key<Division>>>();
	private Key<DataSet> waveKey = null;
	private List<Long> locationIds = null;
	private List<List<Double>> values;
	private List<String> QuestionAndAnswer;
	private List<String> brandsAndFiltersNames;
	private List<Integer> noOfanswersPerQuestion = new ArrayList<Integer>();
	private List<String> allAnswers = new ArrayList<String>();
	private List<String> questionNames = new ArrayList<String>();
	private List<String> brandsNames = new ArrayList<String>();
	private List<String> locationsNames = new ArrayList<String>();
	private String filtersString = "";
	private Map<Long, String> groupsByQuestion = new HashMap<Long, String>();
	private Map<Key<FilteredAnswerValue>, FilteredAnswerValue> resultsMap;
	private List<PointNotes> pointNotes = new ArrayList<>();

	public Map<Key<FilteredAnswerValue>, FilteredAnswerValue> getResultsMap() {
		return resultsMap;
	}

	public void setResultsMap(Map<Key<FilteredAnswerValue>, FilteredAnswerValue> resultsMap) {
		this.resultsMap = resultsMap;
	}

	public List<PointNotes> getPointNotes() {
		return pointNotes;
	}

	public void setPointNotes(List<PointNotes> pointNotes) {
		this.pointNotes = pointNotes;
	}

	public SnapshotQueryResult(Long dataSetID, List<Long> brandsIDs, List<Long> questionsID,
			Map<Long, List<Long>> filtersAndDivisionsIDs, List<Long> locationIdParam,
			Map<Long, String> groupsByQuestion) {

		this.groupsByQuestion = groupsByQuestion;
		locationIds = locationIdParam;
		// Ref<Location> loc = OfyService.ofy().load()
		// .key(Key.create(Location.class, locationIds));
		// locationsNames.add(loc.getValue().getName());
		waveKey = Key.create(DataSet.class, dataSetID);
		List<Key<Question>> questionsKeys = new ArrayList<Key<Question>>();
		List<Key<Brand>> brandsKeys = new ArrayList<Key<Brand>>();

		for (Long questionId : questionsID) {
			questionsKeys.add(Key.create(Question.class, questionId));
		}
		questions = new ArrayList<Question>(OfyService.ofy().load().keys(questionsKeys).values());

		for (Long brandId : brandsIDs) {
			brandsKeys.add(Key.create(Brand.class, brandId));
		}
		brands = new ArrayList<Brand>(OfyService.ofy().load().keys(brandsKeys).values());

		for (Long filterID : filtersAndDivisionsIDs.keySet()) {
			Key<Filter> filterKey = Key.create(Filter.class, filterID);
			filtersAndDivisionsKeysByUser.put(filterKey, new ArrayList<Key<Division>>());
			for (Long divisionID : filtersAndDivisionsIDs.get(filterID)) {
				filtersAndDivisionsKeysByUser.get(filterKey).add(Key.create(filterKey, Division.class, divisionID));
			}
		}

		filters = new ArrayList<Filter>(OfyService.ofy().load().keys(filtersAndDivisionsKeysByUser.keySet()).values());

		List<Key<Division>> allDivisionsKeys = new ArrayList<Key<Division>>();
		for (Filter filter : filters) {
			allDivisionsKeys.addAll(filter.getDivisions());
		}

		allDivisions = OfyService.ofy().load().keys(allDivisionsKeys);

	}

	public void process() {
		// create a map of all filters and divisions for the combination list
		for (Filter filter : filters) {
			filtersAndDivisions.put(filter, new ArrayList<Division>());
			for (Division division : allDivisions.values()) {
				if (division.getFilter().getKey().getId() == filter.getId()
						&& division.getDataSets().contains(waveKey)) {
					filtersAndDivisions.get(filter).add(division);
				}
			}
		}

		/*
		 * each selected division is now converted to an index in the
		 * combination list eg. if for each filter the sum is selected then the
		 * result would be only one string with one integer value. and if
		 * compare is selected in one of the filters the the result would be an
		 * entry for each division in that filter
		 */
		LinkedHashMap<String, Integer> indexMapByFilter = convertFiltersAndDivisionsByUserToIndexList();

		// for each brand add all selected filters
		Map<String, Map<String, Long>> BrandsAndFilters = new LinkedHashMap<String, Map<String, Long>>();
		boolean divisionNamesAdded = false;
		for (Brand brand : brands) {
			for (String divisionsName : indexMapByFilter.keySet()) {
				String key = brand.getName() + "   " + divisionsName;
				if (!divisionNamesAdded)
					tableDivisions.add(divisionsName);
				BrandsAndFilters.put(brand.getName() + "   " + divisionsName, new LinkedHashMap<String, Long>());
				brandsNames.add(brand.getName());
				BrandsAndFilters.get(key).put("Brand", brand.getId());
				BrandsAndFilters.get(key).put("Combination", new Long(indexMapByFilter.get(divisionsName)));
			}
			divisionNamesAdded = true;
		}

		List<Key<Answer>> allAnswersKeys = new ArrayList<Key<Answer>>();
		// for optimization load all at once.
		for (Question question : questions) {
			allAnswersKeys.addAll(question.getAnswers());
		}

		Map<Key<Answer>, Answer> allAnswersMap = OfyService.ofy().load().keys(allAnswersKeys);

		Map<Question, List<Answer>> questionsAndAnswers = new LinkedHashMap<Question, List<Answer>>();
		Map<String, List<String>> questionsAndAnswersValues = new LinkedHashMap<String, List<String>>();

		for (Question question : questions) {
			questionsAndAnswers.put(question, new ArrayList<Answer>());
			for (Key<Answer> answerKey : question.getAnswers()) {

				if ((/* check if answer in the user sent wave */allAnswersMap.get(answerKey).getDataSets()
						.contains(waveKey))
						&& (/* check if answer should appear in snapshot */allAnswersMap.get(answerKey).getOnSnapshot())
						&& (((/*
								 * check if client choose group for this
								 * question
								 */groupsByQuestion.get(question.getId()) != null)
								&& (/*
									 * check if answerKey is in client chosen
									 * group
									 */allAnswersMap.get(answerKey).getAnswerGroup()
										.equals(groupsByQuestion.get(question.getId()))))

								||

								((/* check if client did not choose group */groupsByQuestion
										.get(question.getId()) == null)
										&& ((/*
												 * check if client did not
												 * choose group and answer does
												 * not have group or on default
												 * group
												 */
										allAnswersMap.get(answerKey).getAnswerGroup() == null) || allAnswersMap.get(answerKey).getAnswerGroup().equals("Default"))))) {

					questionsAndAnswers.get(question).add(allAnswersMap.get(answerKey));
				}
			}
		}

		List<Key<FilteredAnswerValue>> allFilteredAnswersValuesIDs = new ArrayList<Key<FilteredAnswerValue>>();

		List<Key<Location>> locationKeys = new ArrayList<Key<Location>>();
		for (Long id : locationIds) {
			locationKeys.add(Key.create(Location.class, id));
		}

		Map<Key<Location>, Location> locations = OfyService.ofy().load().keys(locationKeys);
		for (Long location : locationIds) {
			locationsNames.add(locations.get(Key.create(Location.class, location)).getName());
		}
		for (Question question : questions) {
			if (question.getBuzzWord() != null) {
				questionNames.add(question.getBuzzWord());

			} else {
				questionNames.add(question.getQuestionText());
			}
		}
		for (Entry<Question, List<Answer>> entry : questionsAndAnswers.entrySet()) {
			int counter = 0;

			boolean added = false;
			for (Long location : locationIds) {
				for (Answer answer : entry.getValue()) {
					counter++;
					String key = "";
					if (questions.size() > 1) {
						if (entry.getKey().getBuzzWord() != null) {
							key = entry.getKey().getBuzzWord() + "  " + answer.getAnswerText() + ".";
							// if (added)
							// questionNames.add(entry.getKey().getBuzzWord());

						} else {
							key = entry.getKey().getQuestionText() + "  " + answer.getAnswerText() + ".";
							// if (added)
							// questionNames.add(entry.getKey().getName());

						}
					} else if (locationIds.size() > 1) {
						if (!added)
							key = answer.getAnswerText() + " ("
									+ locations.get(Key.create(Location.class, location)).getName() + ").";
					} else {
						// if (entry.getKey().getBuzzWord() != null) {
						// if (added)
						// questionNames.add(entry.getKey().getBuzzWord());
						//
						// } else {
						// if (added)
						// questionNames.add(entry.getKey().getName());
						// }
						key = answer.getAnswerText() + ".";
					}
					allAnswers.add(answer.getAnswerText());
					questionsAndAnswersValues.put(key, new ArrayList<String>());
					for (Entry<String, Map<String, Long>> brandsEntry : BrandsAndFilters.entrySet()) {
						long studyId = OfyService.ofy().load().key(waveKey).getValue().getStudy().getKey().getId();
						String ID = ("S" + studyId + "L" + location + "W" + waveKey.getId() + "Q"
								+ entry.getKey().getId() + "A" + answer.getId() + "B"
								+ brandsEntry.getValue().get("Brand") + "C"
								+ brandsEntry.getValue().get("Combination"));
						// System.out.println("id " + ID);
						allFilteredAnswersValuesIDs.add(Key.create(FilteredAnswerValue.class, ID));
						questionsAndAnswersValues.get(key).add(ID);

						//////////// TEMP/////////
						PointNotes note = OfyService.ofy().load().type(PointNotes.class)
								.filter("filterdAnswerValue", Ref.create(Key.create(FilteredAnswerValue.class, ID)))
								.first().getValue();
						if (note != null) {
							pointNotes.add(note);
						}
					}
				}

				noOfanswersPerQuestion.add(counter);
				counter = 0;
			}
		}
		setResultsMap(OfyService.ofy().load().keys(allFilteredAnswersValuesIDs));

		values = new ArrayList<List<Double>>();
		QuestionAndAnswer = new ArrayList<String>();

		for (String key : questionsAndAnswersValues.keySet()) {
			QuestionAndAnswer.add(key);
			List<Double> temp = new ArrayList<Double>();

			for (String id : questionsAndAnswersValues.get(key)) {
				try {
					Double result = resultsMap.get(Key.create(FilteredAnswerValue.class, id)).getValue();
					if (result != null)
						temp.add(result);
					else {
						temp.add(new Double(-1000));
					}

				} catch (Exception e) {
					// insufficient data
					temp.add(new Double(-1000));
				}

			}
			values.add(temp);
		}

		brandsAndFiltersNames = new ArrayList<String>();
		for (String name : BrandsAndFilters.keySet()) {
			brandsAndFiltersNames.add(name);
		}
	}

	public LinkedHashMap<String, Integer> convertFiltersAndDivisionsByUserToIndexList() {
		LinkedHashMap<String, Integer> indexMap = new LinkedHashMap<String, Integer>();
		Key<Filter> compareFilterKey = null;
		// Generate the combinations list from the ordered filters and divisions
		List<List<Key<Division>>> combinationsList = generateCombinationsList(
				orderFiltersAndDivisions(filtersAndDivisions), 0);

		List<Key<Division>> divisionsKeys = new ArrayList<Key<Division>>();

		for (Entry<Key<Filter>, List<Key<Division>>> entry : filtersAndDivisionsKeysByUser.entrySet()) {

			if (entry.getValue().size() > 1) {
				compareFilterKey = entry.getKey();
			} else {
				divisionsKeys.add(entry.getValue().get(0));
			}
		}
		Collections.sort(divisionsKeys, new Comparator<Key<Division>>() {

			public int compare(Key<Division> k1, Key<Division> k2) {
				Division division1 = allDivisions.get(k1);
				Division division2 = allDivisions.get(k2);
				if (division1 != null && division2 != null) {
					return division1.compareTo(division2);
				}
				return 0;
			}

		});
		if (compareFilterKey == null || compareFilterKey != null) {

			int rowDivisionsIndex = compareLists(combinationsList, divisionsKeys);
			String divisionsName = "";
			int counter = 0;
			for (Key<Division> key : divisionsKeys) {
				if (allDivisions.get(key) != null) {
					if (counter == 0) {
						if (!allDivisions.get(key).getName().equalsIgnoreCase("all")
								&& !allDivisions.get(key).getName().toLowerCase().startsWith("all ")) {
							divisionsName += allDivisions.get(key).getName();
						}
					}

					else if (counter == divisionsKeys.size() - 1) {
						if (!allDivisions.get(key).getName().equalsIgnoreCase("all")
								&& !allDivisions.get(key).getName().toLowerCase().startsWith("all ")) {
							divisionsName += ", " + allDivisions.get(key).getName() + ".";
						}
					} else {
						if (!allDivisions.get(key).getName().equalsIgnoreCase("all")
								&& !allDivisions.get(key).getName().toLowerCase().startsWith("all ")) {
							divisionsName += ", " + allDivisions.get(key).getName();
						}
					}
					counter++;
				}
			}
			if (divisionsName.length() != 0) {
				if (divisionsName.charAt(0) == ',')
					filtersString += divisionsName.substring(1, divisionsName.length());
				else
					filtersString += divisionsName;
			}
			if (compareFilterKey == null)
				indexMap.put(divisionsName, rowDivisionsIndex);

		}
		if (compareFilterKey != null) {
			filtersString += " All sub-filters of "
					+ OfyService.ofy().load().key(compareFilterKey).getValue().getName();
			for (Key<Division> divisionKey : filtersAndDivisionsKeysByUser.get(compareFilterKey)) {
				divisionsKeys.add(divisionKey);
				indexMap.put(allDivisions.get(divisionKey).getName(), compareLists(combinationsList, divisionsKeys));
				divisionsKeys.remove(divisionsKeys.indexOf(divisionKey));
			}
		}

		return indexMap;
	}

	public List<List<Key<Division>>> orderFiltersAndDivisions(Map<Filter, List<Division>> divisionByFilter) {
		List<Filter> tempFilters = new ArrayList<Filter>();
		List<List<Key<Division>>> divisions = new ArrayList<List<Key<Division>>>();
		for (Filter filter : divisionByFilter.keySet()) {
			tempFilters.add(filter);
		}
		Collections.sort(tempFilters, new Comparator<Filter>() {

			public int compare(Filter o1, Filter o2) {
				return o1.getId().compareTo(o2.getId());

			}
		});

		for (Filter filter : tempFilters) {
			List<Division> tempList = divisionByFilter.get(filter);
			List<Key<Division>> divisionsKeys = new ArrayList<Key<Division>>();
			for (Division division : tempList) {
				divisionsKeys.add(Key.create(division));
			}
			Collections.sort(divisionsKeys, new Comparator<Key<Division>>() {

				public int compare(Key<Division> o1, Key<Division> o2) {
					return (new Long(o1.getId())).compareTo(new Long(o2.getId()));

				}
			});
			divisions.add(divisionsKeys);
		}
		return divisions;
	}

	private int compareLists(List<List<Key<Division>>> combinations, List<Key<Division>> choice) {

		int index = -1;
		boolean found = false;
		for (int i = 0; i < combinations.size(); i++) {
			if (combinations.get(i).size() == choice.size()) {
				for (int k = 0; k < combinations.get(i).size(); k++) {
					if (!combinations.get(i).contains(choice.get(k))) {
						break;
					}
					if (k == combinations.get(i).size() - 1 && combinations.get(i).contains(choice.get(k))) {

						found = true;
						break;
					}
				}
				if (found == true) {
					index = i;
					break;
				}
			}
		}
		return index;
	}

	private List<List<Key<Division>>> generateCombinationsList(List<List<Key<Division>>> filters, int i) {

		// stop condition
		if (i == filters.size()) {
			// return a list with an empty list
			List<List<Key<Division>>> result = new ArrayList<List<Key<Division>>>();
			result.add(new ArrayList<Key<Division>>());
			return result;
		}

		List<List<Key<Division>>> result = new ArrayList<List<Key<Division>>>();
		List<List<Key<Division>>> recursive = generateCombinationsList(filters, i + 1); // recursive
																						// call

		// for each element of the first list of input
		for (int j = 0; j < filters.get(i).size(); j++) {
			// add the element to all combinations obtained for the rest of the
			// lists
			for (int k = 0; k < recursive.size(); k++) {
				// copy a combination from recursive
				List<Key<Division>> newList = new ArrayList<Key<Division>>();
				for (Key<Division> key : recursive.get(k)) {
					newList.add(key);
				}
				// add element of the first list
				newList.add(filters.get(i).get(j));
				// add new combination to result
				result.add(newList);
			}
		}
		return result;
	}

	/**
	 * @return the values
	 */
	public List<List<Double>> getValues() {
		return values;
	}

	/**
	 * @param values
	 *            the values to set
	 */
	public void setValues(List<List<Double>> values) {
		this.values = values;
	}

	/**
	 * @return the questionAndAnswer
	 */
	public List<String> getQuestionAndAnswer() {
		return QuestionAndAnswer;
	}

	/**
	 * @param questionAndAnswer
	 *            the questionAndAnswer to set
	 */
	public void setQuestionAndAnswer(List<String> questionAndAnswer) {
		QuestionAndAnswer = questionAndAnswer;
	}

	/**
	 * @return the brandsAndFiltersNames
	 */
	public List<String> getBrandsAndFiltersNames() {
		return brandsAndFiltersNames;
	}

	/**
	 * @param brandsAndFiltersNames
	 *            the brandsAndFiltersNames to set
	 */
	public void setBrandsAndFiltersNames(List<String> brandsAndFiltersNames) {
		this.brandsAndFiltersNames = brandsAndFiltersNames;
	}

	/**
	 * @return the noOfanswersPerQuestion
	 */
	public List<Integer> getNoOfanswersPerQuestion() {
		return noOfanswersPerQuestion;
	}

	/**
	 * @param noOfanswersPerQuestion
	 *            the noOfanswersPerQuestion to set
	 */
	public void setNoOfanswersPerQuestion(List<Integer> noOfanswersPerQuestion) {
		this.noOfanswersPerQuestion = noOfanswersPerQuestion;
	}

	/**
	 * @return the allAnswers
	 */
	public List<String> getAllAnswers() {
		return allAnswers;
	}

	/**
	 * @param allAnswers
	 *            the allAnswers to set
	 */
	public void setAllAnswers(List<String> allAnswers) {
		this.allAnswers = allAnswers;
	}

	public List<String> getTableDivisions() {
		return tableDivisions;
	}

	public void setTableDivisions(ArrayList<String> tableDivisions) {
		this.tableDivisions = tableDivisions;
	}

	/**
	 * @return the questionNames
	 */
	public List<String> getQuestionNames() {
		return questionNames;
	}

	/**
	 * @param questionNames
	 *            the questionNames to set
	 */
	public void setQuestionNames(List<String> questionNames) {
		this.questionNames = questionNames;
	}

	public List<String> getBrandsNames() {
		return brandsNames;
	}

	public void setBrandsNames(List<String> brandsNames) {
		this.brandsNames = brandsNames;
	}

	public List<String> getLocationsNames() {
		return locationsNames;
	}

	public void setLocationsNames(List<String> locationsNames) {
		this.locationsNames = locationsNames;
	}

	/**
	 * @return the filtersString for widget
	 */
	public String getFiltersString() {
		if (filtersString == "")
			filtersString = "No Filters";
		return filtersString;
	}

	/**
	 * @param filterString
	 *            the filtersString for widget to be set
	 */
	public void setFiltersString(String filtersString) {
		this.filtersString = filtersString;
	}

}
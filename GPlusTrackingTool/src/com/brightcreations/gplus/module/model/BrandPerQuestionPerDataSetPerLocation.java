/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.model;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;

/**
 * @author mohamed.salaheldin
 */

@Entity
//@Cache
public class BrandPerQuestionPerDataSetPerLocation extends StringIdResource
		 {
	private static final long serialVersionUID = -85897131756452273L;

	private List<Key<Brand>> brands = new ArrayList<Key<Brand>>();

	/**
	 * @return the brands
	 */
	public List<Key<Brand>> getBrands() {
		return brands;
	}

	/**
	 * @param brands the brands to set
	 */
	public void setBrands(List<Key<Brand>> brands) {
		this.brands = brands;
	}
	
}

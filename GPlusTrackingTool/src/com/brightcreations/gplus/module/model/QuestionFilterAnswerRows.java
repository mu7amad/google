package com.brightcreations.gplus.module.model;

import javax.xml.bind.annotation.XmlRootElement;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;

@Entity
@XmlRootElement(name = "questionFilterAnswerRows")
public class QuestionFilterAnswerRows extends LongIdResource implements Comparable<QuestionFilterAnswerRows> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3565465675881592186L;

	private String questionRow;
	private String filterAnswerRow;
	@Index
	private Key<Study> studyKey;

	public String getQuestionRow() {
		return questionRow;
	}

	public void setQuestionRow(String questionRow) {
		this.questionRow = questionRow;
	}

	public String getFilterAnswerRow() {
		return filterAnswerRow;
	}

	public void setFilterAnswerRow(String filterAnswerRow) {
		this.filterAnswerRow = filterAnswerRow;
	}

	public Key<Study> getStudyKey() {
		return studyKey;
	}

	public void setStudyKey(Key<Study> studyKey) {
		this.studyKey = studyKey;
	}

	@Override
	public int compareTo(QuestionFilterAnswerRows o) {
		// TODO Auto-generated method stub
		return 0;
	}

}

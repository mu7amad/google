package com.brightcreations.gplus.module.model;

public enum RowType {
	QUESTION_INDEX, QUESTION_TEXT, BASE_DATA, BRAND, FILTER_AND_ANSWER, DIVISION_AND_DATA, UNKNOWN
}

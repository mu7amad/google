/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.brightcreations.gplus.module.controllers.OfyService;
import com.google.api.client.util.Data;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;

import util.AuthorizationValidatior;

/**
 * 
 * @author Mohamed Salah El-Din
 * 
 */
public class TrendQueryResult {

	private List<Brand> brands = new ArrayList<Brand>();
	private List<Map<Filter, List<Division>>> filtersAndDivisions = new ArrayList<Map<Filter, List<Division>>>();
	private List<String> tableDivisions = new ArrayList<String>();
	private List<Question> questions = new ArrayList<Question>();
	private List<Filter> filters = new ArrayList<Filter>();
	private Map<Key<Division>, Division> allDivisions = new HashMap<Key<Division>, Division>();
	private Map<Key<Filter>, List<Key<Division>>> filtersAndDivisionsKeysByUser = new HashMap<Key<Filter>, List<Key<Division>>>();
	private List<Key<DataSet>> waveKeys = null;
	private List<Long> locationIds = null;
	private List<List<Double>> values;
	private List<String> allHeaders;
	private List<String> brandsAndFiltersNames;
	private List<Integer> noOfanswersPerQuestion = new ArrayList<Integer>();
	private List<String> allAnswers = new ArrayList<String>();
	private List<String> divisionsToBeSent = new ArrayList<String>();
	private List<String> questionNames = new ArrayList<String>();
	private List<String> brandsNames = new ArrayList<String>();
	private List<String> locationsNames = new ArrayList<String>();
	private String filtersString = "";
	private Map<Long, String> groupsByQuestion = new HashMap<Long, String>();
	private List<PointNotes> pointNotes = new ArrayList<>();
	private Map<Key<FilteredAnswerValue>, FilteredAnswerValue> resultsMap;

	public Map<Key<FilteredAnswerValue>, FilteredAnswerValue> getResultsMap() {
		return resultsMap;
	}

	public void setResultsMap(Map<Key<FilteredAnswerValue>, FilteredAnswerValue> resultsMap) {
		this.resultsMap = resultsMap;
	}

	public List<PointNotes> getPointNotes() {
		return pointNotes;
	}

	public void setPointNotes(List<PointNotes> pointNotes) {
		this.pointNotes = pointNotes;
	}

	public TrendQueryResult(List<Key<DataSet>> dataSetIDs, List<Long> brandsIDs, List<Long> questionsID,
			Map<Long, List<Long>> filtersAndDivisionsIDs, List<Long> locationIdParam,
			Map<Long, String> groupsByQuestion) {

		this.groupsByQuestion = groupsByQuestion;
		locationIds = locationIdParam;
		waveKeys = dataSetIDs;
		List<Key<Question>> questionsKeys = new ArrayList<Key<Question>>();
		List<Key<Brand>> brandsKeys = new ArrayList<Key<Brand>>();

		// create keys for questions IDs
		for (Long questionId : questionsID) {
			questionsKeys.add(Key.create(Question.class, questionId));
		}
		// load questions Objects
		questions = new ArrayList<Question>(OfyService.ofy().load().keys(questionsKeys).values());

		// create keys for Brands IDs
		// for (Long brandId : brandsIDs) {
		// brandsKeys.add(Key.create(Brand.class, brandId));
		// }
		// load Brands objects
		// brands = new
		// ArrayList<Brand>(OfyService.ofy().load().keys(brandsKeys).values());

		// Create a Map of <Filter, Division(s)>
		for (Long filterID : filtersAndDivisionsIDs.keySet()) {
			Key<Filter> filterKey = Key.create(Filter.class, filterID);
			filtersAndDivisionsKeysByUser.put(filterKey, new ArrayList<Key<Division>>());
			for (Long divisionID : filtersAndDivisionsIDs.get(filterID)) {
				filtersAndDivisionsKeysByUser.get(filterKey).add(Key.create(filterKey, Division.class, divisionID));
			}
		}

		// Load all filters Objects
		filters = new ArrayList<Filter>(OfyService.ofy().load().keys(filtersAndDivisionsKeysByUser.keySet()).values());

		// List all Division for current filters
		List<Key<Division>> allDivisionsKeys = new ArrayList<Key<Division>>();
		for (Filter filter : filters) {
			allDivisionsKeys.addAll(filter.getDivisions());
		}

		// Load All divisions Objects
		allDivisions = OfyService.ofy().load().keys(allDivisionsKeys);

	}

	public void process() {

		// create a map of all filters and divisions for the combination list
		int i = 0;
		for (Key<DataSet> dataSetKey : waveKeys) {
			filtersAndDivisions.add(new HashMap<Filter, List<Division>>());
			for (Filter filter : filters) {
				filtersAndDivisions.get(i).put(filter, new ArrayList<Division>());
				for (Division division : allDivisions.values()) {
					if (division.getFilter().getKey().getId() == filter.getId()) {
						if (division.getDataSets().contains(dataSetKey)) {
							filtersAndDivisions.get(i).get(filter).add(division);
						}
					}
				}
			}
			i++;
		}

		/*
		 * each selected division is now converted to an index in the
		 * combination list eg. if for each filter the sum is selected then the
		 * result would be only one string with one integer value. and if
		 * compare is selected in one of the filters the the result would be an
		 * entry for each division in that filter
		 */
		List<Map<String, Integer>> allIndexMapByFilter = new ArrayList<Map<String, Integer>>();
		for (Map<Filter, List<Division>> filtersAndDivisionsFordDataSet : filtersAndDivisions) {
			allIndexMapByFilter.add(convertFiltersAndDivisionsByUserToIndexList(filtersAndDivisionsFordDataSet));
		}

		// for each brand add all selected filters
		// List<Map<String, Map<String, Long>>> BrandsAndFilters = new
		// ArrayList<Map<String, Map<String, Long>>>();

		// i = 0;
		// for (Key<DataSet> wave : waveKeys) {
		//// BrandsAndFilters.add(new LinkedHashMap<String, Map<String,
		// Long>>());
		// for (Brand brand : brands) {
		// for (String divisionsName : allIndexMapByFilter.get(i).keySet()) {
		// String key = "";
		// if (divisionsName.length() == 0) {
		// key = brand.getName() + ", " + divisionsName;
		// } else {
		// key = brand.getName() + ", " + divisionsName;
		// }
		//
		// tableDivisions.add(divisionsName);
		// BrandsAndFilters.get(i).put(key, new LinkedHashMap<String, Long>());
		// BrandsAndFilters.get(i).get(key).put("Brand", brand.getId());
		// BrandsAndFilters.get(i).get(key).put("Combination",
		// new Long(allIndexMapByFilter.get(i).get(divisionsName)));
		// }
		//
		// }
		// i++;
		// }
		// for (Brand brand : brands) {
		// brandsNames.add(brand.getName());
		// }
		List<Key<Answer>> allAnswersKeys = new ArrayList<Key<Answer>>();
		for (Question question : questions) {
			allAnswersKeys.addAll(question.getAnswers());
		}

		Map<Key<Answer>, Answer> allAnswersMap = OfyService.ofy().load().keys(allAnswersKeys);

		Map<Question, List<Answer>> questionsAndAnswers = new LinkedHashMap<Question, List<Answer>>();

		for (Question question : questions) {
			questionsAndAnswers.put(question, new ArrayList<Answer>());
			for (Key<Answer> answerKey : question.getAnswers()) {
				boolean found = false;
				for (Key<DataSet> dataSetKey : waveKeys) {
					if (allAnswersMap.get(answerKey).getDataSets().contains(dataSetKey)
							&& ((groupsByQuestion.get(question.getId()) != null
									&& allAnswersMap.get(answerKey).getAnswerGroup()
											.equals(groupsByQuestion.get(question.getId())))
									|| (groupsByQuestion.get(question.getId()) == null
											&& (allAnswersMap.get(answerKey).getAnswerGroup() == null || allAnswersMap
													.get(answerKey).getAnswerGroup().equals("Default"))))) {
						found = true;
						break;
					}
				}
				if (found && allAnswersMap.get(answerKey).getOnTrend()) {
					questionsAndAnswers.get(question).add(allAnswersMap.get(answerKey));
				}
			}
		}

		List<Key<FilteredAnswerValue>> allFilteredAnswersValuesIDs = new ArrayList<Key<FilteredAnswerValue>>();
		List<Map<String, List<String>>> allAnswersbyDatasetIndex = new ArrayList<Map<String, List<String>>>();
		List<Key<Location>> locationKeys = new ArrayList<Key<Location>>();
		for (Long id : locationIds) {
			locationKeys.add(Key.create(Location.class, id));
		}

		Map<Key<Location>, Location> locations = OfyService.ofy().load().keys(locationKeys);

		i = 0;
		for (Key<DataSet> dataSetKey : waveKeys) {
			Map<String, List<String>> questionsAndAnswersValues = new LinkedHashMap<String, List<String>>();
			for (Entry<Question, List<Answer>> entry : questionsAndAnswers.entrySet()) {
				boolean added = false;
				for (Long location : locationIds) {
					for (Answer answer : entry.getValue()) {
						String key = "";
						if (questions.size() > 1) {
							if (entry.getKey().getBuzzWord() != null) {
								key = entry.getKey().getBuzzWord() + "  " + answer.getAnswerText() + ".";
							} else {
								key = entry.getKey().getQuestionText() + "  " + answer.getAnswerText() + ".";
							}
						} else if (locationIds.size() > 1) {
							if (!added)
								key = answer.getAnswerText() + " ("
										+ locations.get(Key.create(Location.class, location)).getName() + ").";
						} else {
							key = answer.getAnswerText() + ".";
						}
						questionsAndAnswersValues.put(key, new ArrayList<String>());
						// for (Entry<String, Map<String, Long>> brandsEntry :
						// BrandsAndFilters.get(i).entrySet()) {
						long studyId = OfyService.ofy().load().key(dataSetKey).getValue().getStudy().getKey().getId();

						// String ID = ("S" + studyId + "L" + location + "W" +
						// dataSetKey.getId() + "Q"
						// + entry.getKey().getId() + "A" + answer.getId() + "B"
						// + brandsEntry.getValue().get("Brand") + "C"
						// + brandsEntry.getValue().get("Combination"));
						String ID = ("S" + studyId + "L" + location + "W" + dataSetKey.getId() + "Q"
								+ entry.getKey().getId() + "A" + answer.getId());
						allFilteredAnswersValuesIDs.add(Key.create(FilteredAnswerValue.class, ID));
						questionsAndAnswersValues.get(key).add(ID);

						//////////// TEMP/////////
						String currentAdmin = AuthorizationValidatior.getCurrentUser().getEmail();

						Key<ApplicationUser> appUserKey = OfyService.ofy().load().type(ApplicationUser.class)
								.filter("email", currentAdmin).first().getKey();
						PointNotes note = OfyService.ofy().load().type(PointNotes.class)
								.filter("filterdAnswerValue", Ref.create(Key.create(FilteredAnswerValue.class, ID)))
								.filter("applicationUser", appUserKey).first().getValue();
						pointNotes.add(note);
						// }

					}
				}
				added = true;
			}
			allAnswersbyDatasetIndex.add(questionsAndAnswersValues);
			i++;
		}

		for (Long location : locationIds) {
			locationsNames.add(locations.get(Key.create(Location.class, location)).getName());
		}
		for (Entry<Question, List<Answer>> entry : questionsAndAnswers.entrySet()) {
			noOfanswersPerQuestion.add(entry.getValue().size());
			for (Answer answer : entry.getValue()) {
				allAnswers.add(answer.getAnswerText());
			}
			if (entry.getKey().getBuzzWord() == null)
				questionNames.add(entry.getKey().getQuestionText());
			else
				questionNames.add(entry.getKey().getQuestionText());

		}

		setResultsMap(OfyService.ofy().load().keys(allFilteredAnswersValuesIDs));

		values = new ArrayList<List<Double>>();
		allHeaders = new ArrayList<String>();

		for (int x = 0; x < waveKeys.size(); x++) {
			int questionIndex = 0;
			for (String key : allAnswersbyDatasetIndex.get(x).keySet()) {

				int answerIndex = 0;
//				for (String name : BrandsAndFilters.get(x).keySet()) {
//
//					if (x == 0) {
//						allHeaders.add(key + "  " + name);
//						values.add(new ArrayList<Double>());
//					}
//
//					String id = allAnswersbyDatasetIndex.get(x).get(key).get(answerIndex);
//					try {
//						Double result = resultsMap.get(Key.create(FilteredAnswerValue.class, id)).getValue();
//						if (result != null)
//							values.get(questionIndex).add(result);
//						else {
//							values.get(questionIndex).add(new Double(-1000));
//						}
//
//					} catch (Exception e) {
//						// insufficient data
//						values.get(questionIndex).add(new Double(-1000));
//					}
//
//					answerIndex++;
//					questionIndex++;
//				}

			}
		}

		brandsAndFiltersNames = new ArrayList<String>();
//		for (String name : BrandsAndFilters.get(0).keySet()) {
//			brandsAndFiltersNames.add(name);
//		}

	}

	public Map<String, Integer> convertFiltersAndDivisionsByUserToIndexList(
			Map<Filter, List<Division>> allFiltersAndDivisions) {
		Map<String, Integer> indexMap = new HashMap<String, Integer>();
		Key<Filter> compareFilterKey = null;
		List<List<Key<Division>>> combinationsList = generateCombinationsList(
				orderFiltersAndDivisions(allFiltersAndDivisions), 0);

		List<Key<Division>> divisionsKeys = new ArrayList<Key<Division>>();

		for (Entry<Key<Filter>, List<Key<Division>>> entry : filtersAndDivisionsKeysByUser.entrySet()) {

			if (entry.getValue().size() > 1) {
				compareFilterKey = entry.getKey();
			} else {
				divisionsKeys.add(entry.getValue().get(0));
			}
		}

		if (compareFilterKey == null || compareFilterKey != null) {
			int rowDivisionsIndex = compareLists(combinationsList, divisionsKeys);
			String divisionsName = "";
			int counter = 0;
			boolean firstDiv = true;
			for (Key<Division> key : divisionsKeys) {
				if (allDivisions.get(key) != null) {
					if (firstDiv) {
						if (!allDivisions.get(key).getName().equalsIgnoreCase("all")
								&& !allDivisions.get(key).getName().toLowerCase().startsWith("all ")) {
							divisionsName += allDivisions.get(key).getName();
							firstDiv = false;
						}
					}

					else if (counter == divisionsKeys.size() - 1) {
						if (!allDivisions.get(key).getName().equalsIgnoreCase("all")
								&& !allDivisions.get(key).getName().toLowerCase().startsWith("all ")) {
							divisionsName += ", " + allDivisions.get(key).getName() + ".";
						}
					} else {
						if (!allDivisions.get(key).getName().equalsIgnoreCase("all")
								&& !allDivisions.get(key).getName().toLowerCase().startsWith("all ")) {
							divisionsName += ", " + allDivisions.get(key).getName();
						}
					}
					counter++;
				}
			} // end for
			if (divisionsName.length() != 0) {
				if (divisionsName.charAt(0) == ',')
					filtersString += divisionsName.substring(1, divisionsName.length());
				else
					filtersString += divisionsName;
			}
			if (compareFilterKey == null)
				indexMap.put(divisionsName, rowDivisionsIndex);

		}
		if (compareFilterKey != null) {
			filtersString += " All sub-filters of "
					+ OfyService.ofy().load().key(compareFilterKey).getValue().getName();
			for (Key<Division> divisionKey : filtersAndDivisionsKeysByUser.get(compareFilterKey)) {
				divisionsKeys.add(divisionKey);
				indexMap.put(allDivisions.get(divisionKey).getName(), compareLists(combinationsList, divisionsKeys));
				divisionsKeys.remove(divisionsKeys.indexOf(divisionKey));
			}
		}
		return indexMap;
	}

	public List<List<Key<Division>>> orderFiltersAndDivisions(Map<Filter, List<Division>> divisionByFilter) {
		List<Filter> tempFilters = new ArrayList<Filter>();
		List<List<Key<Division>>> divisions = new ArrayList<List<Key<Division>>>();
		for (Filter filter : divisionByFilter.keySet()) {
			tempFilters.add(filter);
		}
		Collections.sort(tempFilters, new Comparator<Filter>() {

			public int compare(Filter o1, Filter o2) {
				return o1.getId().compareTo(o2.getId());

			}

		});

		for (Filter filter : tempFilters) {
			List<Division> tempList = divisionByFilter.get(filter);
			List<Key<Division>> divisionsKeys = new ArrayList<Key<Division>>();
			for (Division division : tempList) {
				divisionsKeys.add(Key.create(division));
			}
			Collections.sort(divisionsKeys, new Comparator<Key<Division>>() {

				public int compare(Key<Division> o1, Key<Division> o2) {
					return (new Long(o1.getId())).compareTo(new Long(o2.getId()));

				}

			});
			divisions.add(divisionsKeys);
		}
		return divisions;
	}

	private int compareLists(List<List<Key<Division>>> combinations, List<Key<Division>> choice) {

		int index = -1;
		boolean found = false;
		for (int i = 0; i < combinations.size(); i++) {
			if (combinations.get(i).size() == choice.size()) {
				for (int k = 0; k < combinations.get(i).size(); k++) {
					if (!combinations.get(i).contains(choice.get(k))) {
						break;
					}
					if (k == combinations.get(i).size() - 1 && combinations.get(i).contains(choice.get(k))) {

						found = true;
						break;
					}
				}
				if (found == true) {
					index = i;
					break;
				}
			}
		}
		return index;
	}

	private List<List<Key<Division>>> generateCombinationsList(List<List<Key<Division>>> filters, int i) {

		// stop condition
		if (i == filters.size()) {
			// return a list with an empty list
			List<List<Key<Division>>> result = new ArrayList<List<Key<Division>>>();
			result.add(new ArrayList<Key<Division>>());
			return result;
		}

		List<List<Key<Division>>> result = new ArrayList<List<Key<Division>>>();
		List<List<Key<Division>>> recursive = generateCombinationsList(filters, i + 1); // recursive
																						// call

		// for each element of the first list of input
		for (int j = 0; j < filters.get(i).size(); j++) {
			// add the element to all combinations obtained for the rest of the
			// lists
			for (int k = 0; k < recursive.size(); k++) {
				// copy a combination from recursive
				List<Key<Division>> newList = new ArrayList<Key<Division>>();
				for (Key<Division> key : recursive.get(k)) {
					newList.add(key);
				}
				// add element of the first list
				newList.add(filters.get(i).get(j));
				// add new combination to result
				result.add(newList);
			}
		}
		return result;
	}

	/**
	 * @return the values
	 */
	public List<List<Double>> getValues() {
		return values;
	}

	/**
	 * @param values
	 *            the values to set
	 */
	public void setValues(List<List<Double>> values) {
		this.values = values;
	}

	/**
	 * @return the questionAndAnswer
	 */
	public List<String> getAllHeaders() {
		return allHeaders;
	}

	/**
	 * @param questionAndAnswer
	 *            the questionAndAnswer to set
	 */
	public void setAllHeaders(List<String> questionAndAnswer) {
		allHeaders = questionAndAnswer;
	}

	/**
	 * @return the brandsAndFiltersNames
	 */
	public List<String> getBrandsAndFiltersNames() {
		return brandsAndFiltersNames;
	}

	/**
	 * @param brandsAndFiltersNames
	 *            the brandsAndFiltersNames to set
	 */
	public void setBrandsAndFiltersNames(List<String> brandsAndFiltersNames) {
		this.brandsAndFiltersNames = brandsAndFiltersNames;
	}

	public List<Integer> getNoOfanswersPerQuestion() {
		return noOfanswersPerQuestion;
	}

	public void setNoOfanswersPerQuestion(List<Integer> noOfanswersPerQuestion) {
		this.noOfanswersPerQuestion = noOfanswersPerQuestion;
	}

	public List<String> getAllAnswers() {
		return allAnswers;
	}

	public void setAllAnswers(List<String> allAnswers) {
		this.allAnswers = allAnswers;
	}

	public List<String> getTableDivisions() {
		return tableDivisions;
	}

	public void setTableDivisions(ArrayList<String> tableDivisions) {
		this.tableDivisions = tableDivisions;
	}

	public List<String> getQuestionNames() {
		return questionNames;
	}

	public void setQuestionNames(List<String> questionNames) {
		this.questionNames = questionNames;
	}

	/**
	 * @return the locationsNames
	 */
	public List<String> getLocationsNames() {
		return locationsNames;
	}

	/**
	 * @param locationsNames
	 *            the locationsNames to set
	 */
	public void setLocationsNames(List<String> locationsNames) {
		this.locationsNames = locationsNames;
	}

	/**
	 * @return the brandsNames
	 */
	public List<String> getBrandsNames() {
		return brandsNames;
	}

	/**
	 * @param brandsNames
	 *            the brandsNames to set
	 */
	public void setBrandsNames(List<String> brandsNames) {
		this.brandsNames = brandsNames;
	}

	/**
	 * @return the divisionsToBeSent
	 */
	public List<String> getDivisionsToBeSent() {
		return divisionsToBeSent;
	}

	/**
	 * @param divisionsToBeSent
	 *            the divisionsToBeSent to set
	 */
	public void setDivisionsToBeSent(List<String> divisionsToBeSent) {
		this.divisionsToBeSent = divisionsToBeSent;
	}

	/**
	 * @return the filtersString for widget
	 */
	public String getFiltersString() {
		if (filtersString == "")
			filtersString = "No Filters";
		return filtersString;
	}

	/**
	 * @param filterString
	 *            the filtersString for widget to be set
	 */
	public void setFiltersString(String filtersString) {
		this.filtersString = filtersString;
	}

	/**
	 * @return the groupsByQuestion
	 */
	public Map<Long, String> getGroupsByQuestion() {
		return groupsByQuestion;
	}

	/**
	 * @param groupsByQuestion
	 *            the groupsByQuestion to set
	 */
	public void setGroupsByQuestion(Map<Long, String> groupsByQuestion) {
		this.groupsByQuestion = groupsByQuestion;
	}
}

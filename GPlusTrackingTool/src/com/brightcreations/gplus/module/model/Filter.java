/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.model;

import static com.brightcreations.gplus.module.controllers.OfyService.ofy;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Transient;

import com.brightcreations.gplus.module.model.DataSet;
import com.brightcreations.gplus.module.model.DataSheet;
import com.brightcreations.gplus.module.model.Division;
import com.brightcreations.gplus.module.model.LongIdResource;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;

/**
 * @author Ramy Mahmoudi
 * @author Ismail Marmoush
 */
@Entity
@Cache
public class Filter extends LongIdResource implements Comparable<Filter> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1970023294609024223L;
	@Index
	private String name;
	@JsonBackReference
	private List<Key<Division>> divisions = new ArrayList<Key<Division>>();
	@Index
	@JsonBackReference
	private List<Key<DataSheet>> datasheets = new ArrayList<Key<DataSheet>>();
	@JsonBackReference
	private List<Key<DataSet>> dataSets = new ArrayList<Key<DataSet>>();
	@Index
	private Integer order = 0;

	@Transient
	private List<Division> transientDivision = new ArrayList<>();

	@Index
	@JsonBackReference
	private Key<Study> studyKey;

	public List<Division> getTransientDivision() {
		return transientDivision;
	}

	public void setTransientDivision(List<Division> transientDivision) {
		this.transientDivision = transientDivision;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the datasheets
	 */
	public List<Key<DataSheet>> getDatasheets() {
		return datasheets;
	}

	/**
	 * @param datasheets
	 *            the datasheets to set
	 */
	public void setDatasheets(List<Key<DataSheet>> datasheets) {
		this.datasheets = datasheets;
	}

	/**
	 * @return the dataSets
	 */
	public List<Key<DataSet>> getDataSets() {
		return dataSets;
	}

	/**
	 * @param dataSets
	 *            the dataSets to set
	 */
	public void setDataSets(List<Key<DataSet>> dataSets) {
		this.dataSets = dataSets;
	}

	/**
	 * @return the divisions
	 */
	public List<Key<Division>> getDivisions() {
		return divisions;
	}

	/**
	 * @param divisions
	 *            the divisions to set
	 */
	public void setDivisions(List<Key<Division>> divisions) {
		this.divisions = divisions;
	}

	/**
	 * @return the order
	 */
	public Integer getOrder() {
		return order;
	}

	/**
	 * @param order
	 *            the order to set
	 */
	public void setOrder(Integer order) {
		this.order = order;
	}

	public static List<Filter> getFilterList() {
		return ofy().load().type(Filter.class).list();
	}

	public static List<Filter> getFilterList(String sortBy, String order, Long from, Long to) {
		Long limit = to - from;
		String sort = (order.equals("desc")) ? "-" + sortBy : sortBy;
		List<Filter> list = ofy().load().type(Filter.class).limit(limit.intValue()).order(sort).offset(from.intValue())
				.list();
		return list;
	}

	public static Filter getFilter(Long id) {
		return ofy().load().type(Filter.class).id(id).get();
	}

	public void save() {
		ofy().save().entity(this);
	}

	public int compareTo(Filter f) {
		return order.compareTo(f.getOrder());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */

	public int hashCode() {
		final int prime = 31;
		int result = prime + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (getClass() != obj.getClass())
			return false;
		Filter other = (Filter) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public Key<Study> getStudyKey() {
		return studyKey;
	}

	public void setStudyKey(Key<Study> studyKey) {
		this.studyKey = studyKey;
	}

}

package com.brightcreations.gplus.module.model;

import java.util.ArrayList;
import java.util.List;

public class SelectorBean {

	private ArrayList<Question> questions;
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(ArrayList<Question> questions) {
		this.questions = questions;
	}

}

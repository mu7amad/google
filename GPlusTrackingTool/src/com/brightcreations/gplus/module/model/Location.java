/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.model;

import static com.brightcreations.gplus.module.controllers.OfyService.ofy;

import java.util.List;

import com.google.gwt.editor.client.Editor.Ignore;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;

/**
 * 
 * @author Mohamed Salah El-Din
 * @author Ismail Marmoush
 * 
 */

@Entity
public class Location extends LongIdResource implements Comparable<Location> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6816134905613330646L;
	@Index
	private String name;

	@Index
	private Boolean isRegion;

	@Index
	private Key<Region> region;

	@Ignore
	private Region regionObj;

	@Index
	private boolean deleted;

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the isRegion
	 */
	public Boolean getIsRegion() {
		return isRegion;
	}

	/**
	 * @param isRegion
	 *            the isRegion to set
	 */
	public void setIsRegion(Boolean isRegion) {
		this.isRegion = isRegion;
	}

	public static List<Location> getLocationList() {
		return ofy().load().type(Location.class).list();
	}

	public static List<Location> getLocationList(String sortBy, String order, Long from, Long to) {
		Long limit = to - from;
		String sort = (order.equals("desc")) ? "-" + sortBy : sortBy;
		List<Location> list = ofy().load().type(Location.class).limit(limit.intValue()).order(sort)
				.offset(from.intValue()).list();
		return list;
	}

	public static Location getLocation(Long id) {
		return ofy().load().type(Location.class).id(id).get();
	}

	public void save() {
		ofy().save().entity(this).now();
	}

	/**
	 * @return the region
	 */
	public Key<Region> getRegion() {
		return region;
	}

	/**
	 * @param region
	 *            the region to set
	 */
	public void setRegion(Key<Region> region) {
		this.region = region;
	}

	public int compareTo(Location o) {
		return this.name.trim().toLowerCase().compareTo(o.getName().trim().toLowerCase());
	}

	public Region getRegionObj() {
		return regionObj;
	}

	public void setRegionObj(Region regionObj) {
		this.regionObj = regionObj;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */

	public int hashCode() {
		final int prime = 31;
		int result = prime + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (getClass() != obj.getClass())
			return false;
		Location other = (Location) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}

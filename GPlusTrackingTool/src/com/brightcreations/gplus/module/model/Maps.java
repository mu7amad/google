/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.model;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.IgnoreSave;

/**
 * @author Mohamed Salah El-Din
 */
@Entity
@Cache
public class Maps extends StringIdResource {

	private static final long serialVersionUID = -105462891567185124L;
	private String answersMapByQuestion;
	private String questionsMap;
	private String answersMapById;

	@IgnoreSave
	private ObjectMapper mapper;

	/**
	 * @return the mapper
	 */
	public ObjectMapper getMapper() {
		return mapper;
	}

	/**
	 * @param mapper
	 *            the mapper to set
	 */
	public void setMapper(ObjectMapper mapper) {
		this.mapper = mapper;
	}

	public Maps(ObjectMapper mapper) {
		this.mapper = mapper;
	}

	public Maps() {

	}

	/**
	 * @return the answersMapByQuestion
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */

	public Map<Question, Map<String, Answer>> getAnswersMapByQuestion()
			throws JsonParseException, JsonMappingException, IOException {

		Gson gson = new Gson();
		Map<Question, Map<String, Answer>> answersMapByQuestionMAP = new HashMap<Question, Map<String, Answer>>();
		Map<String, Map<String, String>> answersMapByQuestionGSON = mapper.readValue(answersMapByQuestion,
				new TypeReference<HashMap<String, HashMap<String, String>>>() {
				});

		for (String question : answersMapByQuestionGSON.keySet()) {
			Question ques = gson.fromJson(question, Question.class);
			answersMapByQuestionMAP.put(ques, new HashMap<String, Answer>());
			for (String key : answersMapByQuestionGSON.get(question).keySet()) {
				Answer ans = gson.fromJson(answersMapByQuestionGSON.get(question).get(key), Answer.class);
				answersMapByQuestionMAP.get(ques).put(gson.fromJson(key, String.class), ans);
			}
		}

		return answersMapByQuestionMAP;
	}

	public void setAnswersMapByQuestion(Map<Question, Map<String, Answer>> answersMapByQuestion)
			throws JsonGenerationException, JsonMappingException, IOException {

		Map<String, Map<String, String>> answersMapByQuestionGSON = new HashMap<String, Map<String, String>>();
		Gson gson = new Gson();
		for (Question question : answersMapByQuestion.keySet()) {

			String quesJson = gson.toJson(question);
			answersMapByQuestionGSON.put(quesJson, new HashMap<String, String>());
			for (String key : answersMapByQuestion.get(question).keySet()) {
				answersMapByQuestionGSON.get(quesJson).put(gson.toJson(key),
						mapper.writeValueAsString(answersMapByQuestion.get(question).get(key)));
			}
		}
		// System.out.println(gson.toJson(answersMapByQuestionGSON));
		this.answersMapByQuestion = gson.toJson(answersMapByQuestionGSON);
	}

	/**
	 * @return the questionsMap
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public Map<Integer, Question> getQuestionsMap() throws JsonParseException, JsonMappingException, IOException {
		Gson gson = new Gson();
		Map<Integer, Question> newQuestionsMap = gson.fromJson(questionsMap,
				new TypeToken<HashMap<Integer, Question>>() {
				}.getType());

		return newQuestionsMap;
	}

	/**
	 * @param questionsMap
	 *            the questionsMap to set
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonGenerationException
	 */
	public void setQuestionsMap(Map<Integer, Question> newQuestionsMap)
			throws JsonGenerationException, JsonMappingException, IOException {
		String questionsMapString = "";
		Gson gson = new Gson();
		questionsMapString = gson.toJson(newQuestionsMap);
		this.questionsMap = questionsMapString;
	}

	public Map<Integer, Answer> getAnswersMapById() throws JsonParseException, JsonMappingException, IOException {
		Gson gson = new Gson();
		Map<Integer, Answer> newAnswersMap = mapper.readValue(answersMapById,
				new TypeReference<HashMap<Integer, Answer>>() {
				});
		return newAnswersMap;
	}

	/**
	 * @param answersMapById
	 *            the answersMapById to set
	 */
	public void setAnswersMapById(Map<Integer, Answer> newAnswersMapById) {
		String answersMapString = "";
		Gson gson = new Gson();
		answersMapString = gson.toJson(newAnswersMapById);

		this.answersMapById = answersMapString;
	}
}
/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.model;

import static com.brightcreations.gplus.module.controllers.OfyService.ofy;

import java.util.Date;
import java.util.List;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Parent;

/**
 * 
 * @author Mohamed Salah El-Din
 * @author Ismail Marmoush
 */
@Entity
public class LogFile extends LongIdResource {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1430335387732595722L;
	@Parent
	private Ref<DataSheet> dataSheet;
	private Date processStartTime;
	private Date ProcessEndTime;
	private String waveStatus;
	private String studyStatus;
	private String questionsStatus;
	private String filtersStatus;
	private String brandsStatus;
	private boolean processed = false;
	private boolean reported = false;
	private String subject = "";
	private String content = "";

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the dataSheet
	 */
	public Ref<DataSheet> getDataSheet() {
		return dataSheet;
	}

	/**
	 * @return the filtersStatus
	 */
	public String getFiltersStatus() {
		return filtersStatus;
	}

	/**
	 * @return the processEndTime
	 */
	public Date getProcessEndTime() {
		return ProcessEndTime;
	}

	/**
	 * @return the processStartTime
	 */
	public Date getProcessStartTime() {
		return processStartTime;
	}

	/**
	 * @return the questionsStatus
	 */
	public String getQuestionsStatus() {
		return questionsStatus;
	}

	/**
	 * @return the waveStatus
	 */
	public String getWaveStatus() {
		return waveStatus;
	}

	public boolean isProcessed() {
		return processed;
	}

	public boolean isReported() {
		return reported;
	}

	/**
	 * @param dataSheet
	 *            the dataSheet to set
	 */
	public void setDataSheet(Ref<DataSheet> dataSheet) {
		this.dataSheet = dataSheet;
	}

	/**
	 * @param filtersStatus
	 *            the filtersStatus to set
	 */
	public void setFiltersStatus(String filtersStatus) {
		this.filtersStatus = filtersStatus;
	}

	public void setProcessed(boolean processed) {
		this.processed = processed;
	}

	/**
	 * @param processEndTime
	 *            the processEndTime to set
	 */
	public void setProcessEndTime(Date processEndTime) {
		ProcessEndTime = processEndTime;
	}

	/**
	 * @param processStartTime
	 *            the processStartTime to set
	 */
	public void setProcessStartTime(Date processStartTime) {
		this.processStartTime = processStartTime;
	}

	/**
	 * @param questionsStatus
	 *            the questionsStatus to set
	 */
	public void setQuestionsStatus(String questionsStatus) {
		this.questionsStatus = questionsStatus;
	}

	public void setReported(boolean reported) {
		this.reported = reported;
	}

	/**
	 * @param waveStatus
	 *            the waveStatus to set
	 */
	public void setWaveStatus(String waveStatus) {
		this.waveStatus = waveStatus;
	}

	public static List<LogFile> getLogFileList() {
		return ofy().load().type(LogFile.class).list();
	}

	public static List<LogFile> getLogFileList(String sortBy, String order,
			Long from, Long to) {
		Long limit = to - from;
		String sort = (order.equals("desc")) ? "-" + sortBy : sortBy;
		List<LogFile> list = ofy().load().type(LogFile.class)
				.limit(limit.intValue()).order(sort).offset(from.intValue())
				.list();
		return list;
	}

	public static LogFile getLogFile(Long id) {
		return ofy().load().type(LogFile.class).id(id).get();
	}

	public void save() {
		ofy().save().entity(this);
	}

	/**
	 * @return the brandsStatus
	 */
	public String getBrandsStatus() {
		return brandsStatus;
	}

	/**
	 * @param brandsStatus
	 *            the brandsStatus to set
	 */
	public void setBrandsStatus(String brandsStatus) {
		this.brandsStatus = brandsStatus;
	}

	public String getStudyStatus() {
		return studyStatus;
	}

	public void setStudyStatus(String studyStatus) {
		this.studyStatus = studyStatus;
	}

}

/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.model;

import static com.brightcreations.gplus.module.controllers.OfyService.ofy;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.brightcreations.gplus.web.security.ApplicationUserRole;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * 
 * @author Mohamed Salah El-Din
 * @author Ismail Marmoush
 * 
 */
@Cache
@XmlRootElement(name = "applicationUser")
@Entity
public class ApplicationUser extends LongIdResource {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5639324375372349393L;
	@Index
	private String firstName;
	@Index
	private String lastName;
	@Index
	private String email;
	@Index
	private String fullName;
	@Index
	private String addedBy;
	@Index
	private Set<String> authorities = new HashSet<String>();
	@Index
	private boolean onlyUser = true;
	@Index
	private String type;
	@Index
	// @JsonBackReference(value = "user-location")
	private Key<Location> defaultCountry;
	@Index
	private boolean superUser = false;

	@Index
	// @JsonBackReference(value = "user-shared-studies")
	private List<Key<Study>> sharedStudies;
	@Index
	// @JsonBackReference(value = "user-status")
	private String status;
	@Index
	private String imgURL;
	@Index
	private String googleUserId;

	@Index
	private String creationDate;

	@Index
	private boolean deleted;

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	/**
	 * @return the superUser
	 */
	public boolean isSuperUser() {
		return superUser;
	}

	/**
	 * @param superUser
	 *            the superUser to set
	 */
	public void setSuperUser(boolean superUser) {
		this.superUser = superUser;
	}

	private boolean showVideo = true;

	@Index
	// @JsonBackReference(value = "user-shared-dashboards")
	private Set<Key<Dashboard>> sharedDashboards = new HashSet<Key<Dashboard>>();

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @param authorities
	 *            the authorities to set
	 */
	public void setAuthorities(Set<String> authorities) {
		this.authorities = authorities;
	}

	/**
	 * @return the authorities
	 */
	public Set<String> getAuthorities() {
		return authorities;
	}

	/**
	 * @return the onlyUser
	 */
	public boolean isOnlyUser() {
		return onlyUser;
	}

	public boolean isAdmin() {
		return this.authorities.contains(ApplicationUserRole.ROLE_ADMIN.toString());
	}

	public boolean isResearcher() {
		return this.authorities.contains(ApplicationUserRole.ROLE_RESEARCHER.toString());
	}

	/**
	 * @param onlyUser
	 *            the onlyUser to set
	 */
	public void setOnlyUser(boolean onlyUser) {
		this.onlyUser = onlyUser;
	}

	public static List<ApplicationUser> getApplicationUserList() {
		return ofy().load().type(ApplicationUser.class).list();
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @return the defaultCountry
	 */
	@XmlTransient
	public Key<Location> getDefaultCountry() {
		return defaultCountry;
	}

	/**
	 * @param defaultCountry
	 *            the defaultCountry to set
	 */
	public void setDefaultCountry(Key<Location> defaultCountry) {
		this.defaultCountry = defaultCountry;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	public static List<ApplicationUser> getApplicationUserList(String sortBy, String order, Long from, Long to) {
		Long limit = to - from;
		String sort = (order.equals("desc")) ? "-" + sortBy : sortBy;
		List<ApplicationUser> list = ofy().load().type(ApplicationUser.class).limit(limit.intValue()).order(sort)
				.offset(from.intValue()).list();
		return list;
	}

	public static ApplicationUser getApplicationUser(Long id) {
		return ofy().load().type(ApplicationUser.class).id(id).get();
	}

	public void save() {
		ofy().save().entity(this).now();
	}

	public boolean isShowVideo() {
		return showVideo;
	}

	public void setShowVideo(boolean showVideo) {
		this.showVideo = showVideo;
	}

	/**
	 * @return the sharedDashboards
	 */
	@XmlTransient
	public Set<Key<Dashboard>> getSharedDashboards() {
		return sharedDashboards;
	}

	public List<Key<Study>> getSharedStudies() {
		return sharedStudies;
	}

	public void setSharedStudies(List<Key<Study>> sharedStudies) {
		this.sharedStudies = sharedStudies;
	}

	/**
	 * @param sharedDashboards
	 *            the sharedDashboards to set
	 */
	public void setSharedDashboards(Set<Key<Dashboard>> sharedDashboards) {
		this.sharedDashboards = sharedDashboards;
	}

	public String getGoogleUserId() {
		return googleUserId;
	}

	public void setGoogleUserId(String googleUserId) {
		this.googleUserId = googleUserId;
	}

	public String getAddedBy() {
		return addedBy;
	}

	public void setAddedBy(String addedBy) {
		this.addedBy = addedBy;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getImgURL() {
		return imgURL;
	}

	public void setImgURL(String imgURL) {
		this.imgURL = imgURL;
	}

	@Override
	public int hashCode() {
		return this.getId().intValue();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ApplicationUser other = (ApplicationUser) obj;
		if (this.getId() == null) {
			if (other.getId() != null)
				return false;
		} else if (!this.getId().equals(other.getId()))
			return false;
		return true;
	}
}

/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.model.collection;

import java.util.ArrayList;
import java.util.Collection;

import com.brightcreations.gplus.module.model.AbstractResource;
import com.googlecode.objectify.Key;

/**
 * @author Ramy Mahmoudi
 * @param <R>
 */
public class ResourceList<R extends AbstractResource<?>> extends ArrayList<R> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5926047585209687640L;

	public ResourceList(Collection<R> list) {
		super(list);
	}

	public ArrayList<Key<R>> getKeyList() {

		ArrayList<Key<R>> keyList = new ArrayList<Key<R>>();
		for (R resource : this) {
			keyList.add(Key.create(resource));
		}

		return keyList;

	}
}

package com.brightcreations.gplus.module.model;

import javax.xml.bind.annotation.XmlRootElement;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;

@Entity
@Cache
@XmlRootElement(name = "config")
public class ConfigEntity extends LongIdResource implements Comparable<ConfigEntity> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8706559305880356334L;

	@Index
	private String key;
	@Index
	private String value;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public int compareTo(ConfigEntity o) {
		// TODO Auto-generated method stub
		return 0;
	}

}

/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.model;

import static com.brightcreations.gplus.module.controllers.OfyService.ofy;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Parent;

/**
 * @author Ramy Mahmoudi
 * @author Ismail Marmoush
 * @author mohamed.salaheldin
 * @author mohammed.eladly
 */
@Entity
@XmlRootElement(name = "answer")
@Cache
public class Answer extends LongIdResource {

	private static final long serialVersionUID = -2163879325660164969L;

	@Index
	private String answerText;
	@Index
	private Key<Question> questionKey;
	// @Parent
	// @JsonBackReference
	// private Ref<Question> question;
	@Index
	// @JsonBackReference
	private List<Key<DataSheet>> datasheets = new ArrayList<Key<DataSheet>>();
	@Index
	// @JsonBackReference
	private List<Key<DataSet>> dataSets = new ArrayList<Key<DataSet>>();
	private Boolean onTrend = Boolean.TRUE;
	private Boolean onSnapshot = Boolean.TRUE;
	private String answerGroup;
	@Index
	private String questionAnswerKey;
	@Index
	// @JsonBackReference
	private Key<Study> studyKey;
	@Transient
	private Double transientValue;

	public Double getTransientValue() {
		return transientValue;
	}

	public void setTransientValue(Double transientValue) {
		this.transientValue = transientValue;
	}

	public Key<Question> getQuestionKey() {
		return questionKey;
	}

	public void setQuestionKey(Key<Question> questionKey) {
		this.questionKey = questionKey;
	}

	public String getQuestionAnswerKey() {
		return questionAnswerKey;
	}

	public void setQuestionAnswerKey(String questionAnswerKey) {
		this.questionAnswerKey = questionAnswerKey;
	}

	@Index
	private boolean deleted;

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	/**
	 * @return the name
	 */
	public String getAnswerText() {
		return answerText;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setAnswerText(String answerText) {
		this.answerText = answerText;
	}

	/**
	 * @return the question
	 */

	// @XmlTransient
	// public Ref<Question> getQuestion() {
	// return question;
	// }

	/**
	 * @param question
	 *            the question to set
	 */

	/*
	 * public void setQuestion(Ref<Question> question) { this.question =
	 * question; }
	 */

	/**
	 * @param question
	 *            the question to set
	 */
	// public void setQuestion(Question question) {
	// if (question != null) {
	// this.question = Ref.create(question);
	// }
	// }

	/**
	 * @return the datasheets
	 */
	@XmlTransient
	public List<Key<DataSheet>> getDatasheets() {
		return datasheets;
	}

	/**
	 * @param datasheets
	 *            the datasheets to set
	 */
	public void setDatasheets(List<Key<DataSheet>> datasheets) {
		this.datasheets = datasheets;
	}

	/**
	 * @return the dataSets
	 */
	@XmlTransient
	public List<Key<DataSet>> getDataSets() {
		return dataSets;
	}

	/**
	 * @param dataSets
	 *            the dataSets to set
	 */
	public void setDataSets(List<Key<DataSet>> dataSets) {
		this.dataSets = dataSets;
	}

	public static List<Answer> getAnswerList() {
		return ofy().load().type(Answer.class).list();
	}

	public static List<Answer> getAnswerList(String sortBy, String order, Long from, Long to) {
		Long limit = to - from;
		String sort = (order.equals("desc")) ? "-" + sortBy : sortBy;
		List<Answer> list = ofy().load().type(Answer.class).limit(limit.intValue()).order(sort).offset(from.intValue())
				.list();
		return list;
	}

	public static Answer getAnswer(Long id) {
		return ofy().load().type(Answer.class).id(id).get();
	}

	public void save() {
		ofy().save().entity(this);
	}

	/**
	 * @return the onTrend
	 */
	public Boolean getOnTrend() {
		return onTrend;
	}

	/**
	 * @param onTrend
	 *            the onTrend to set
	 */
	public void setOnTrend(Boolean onTrend) {
		this.onTrend = onTrend;
	}

	/**
	 * @return the onSnapshot
	 */
	public Boolean getOnSnapshot() {
		return onSnapshot;
	}

	/**
	 * @param onSnapshot
	 *            the onSnapshot to set
	 */
	public void setOnSnapshot(Boolean onSnapshot) {
		this.onSnapshot = onSnapshot;
	}

	/**
	 * @return the answerGroup
	 */
	public String getAnswerGroup() {
		return answerGroup;
	}

	/**
	 * @param answerGroup
	 *            the answerGroup to set
	 */
	public void setAnswerGroup(String answerGroup) {
		this.answerGroup = answerGroup;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */

	public int hashCode() {
		final int prime = 31;
		int result = prime + ((answerText == null) ? 0 : answerText.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (getClass() != obj.getClass())
			return false;
		Answer other = (Answer) obj;
		if (answerText == null) {
			if (other.answerText != null)
				return false;
		} else if (!answerText.equals(other.answerText))
			return false;
		return true;
	}

	public Key<Study> getStudyKey() {
		return studyKey;
	}

	public void setStudyKey(Key<Study> studyKey) {
		this.studyKey = studyKey;
	}

	// public void setQuestion(Ref<Question> question) {
	// this.question = question;
	// }

}

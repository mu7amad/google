/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.multipleuploader;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.brightcreations.gplus.module.model.DataSheet;

/**
 * 
 * @author Mohamed Salah El-Din
 * 
 */
@XmlRootElement(name = "file")
public class FileMeta implements Serializable {
	private static final long serialVersionUID = 1L;
	private String name;
	private long size;
	private String url;
	private String delete_url;
	private String delete_type;
	private String thumbnail_url;
	private List<DataSheet> dataSheets = new ArrayList<DataSheet>();

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the size
	 */
	public long getSize() {
		return size;
	}

	/**
	 * @param size
	 *            the size to set
	 */
	public void setSize(long size) {
		this.size = size;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url
	 *            the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the delete_url
	 */
	public String getDelete_url() {
		return delete_url;
	}

	/**
	 * @param delete_url
	 *            the delete_url to set
	 */
	public void setDelete_url(String delete_url) {
		this.delete_url = delete_url;
	}

	/**
	 * @return the delete_type
	 */
	public String getDelete_type() {
		return delete_type;
	}

	/**
	 * @param delete_type
	 *            the delete_type to set
	 */
	public void setDelete_type(String delete_type) {
		this.delete_type = delete_type;
	}

	public FileMeta(String filename, long size, String url, String thumbnail) {
		this.name = filename;
		this.size = size;
		this.url = url;
		this.delete_url = url;
		this.delete_type = "DELETE";
		this.thumbnail_url = thumbnail;
	}

	public FileMeta() {
	}

	/**
	 * @return the thumbnail_url
	 */
	public String getThumbnail_url() {
		return thumbnail_url;
	}

	/**
	 * @param thumbnail_url
	 *            the thumbnail_url to set
	 */
	public void setThumbnail_url(String thumbnail_url) {
		this.thumbnail_url = thumbnail_url;
	}

	/**
	 * @return the dataSheets
	 */
	public List<DataSheet> getDataSheets() {
		return dataSheets;
	}

	/**
	 * @param dataSheets
	 *            the dataSheets to set
	 */
	public void setDataSheets(List<DataSheet> dataSheets) {
		this.dataSheets = dataSheets;
	}

}
/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.controllers;

import java.io.ByteArrayOutputStream;

/**
*
* @author Mohammed ElAdly
*
*/

import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.brightcreations.gplus.module.controllers.MIDataExplorer.AdminTokenGenerator;
import com.brightcreations.gplus.module.controllers.MIDataExplorer.StudyBackEnd;
import com.brightcreations.gplus.module.controllers.MIDataExplorer.Util;
import com.brightcreations.gplus.module.controllers.MIDataExplorer.customClasses.ModuleType;
import com.brightcreations.gplus.module.model.DataSheet;
import com.brightcreations.gplus.web.security.GaeAuthenticationProvider;
import com.brightcreations.gplus.web.security.GaeUserAuthentication;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.util.Base64;
import com.google.appengine.api.blobstore.BlobInfo;
import com.google.appengine.api.blobstore.BlobInfoFactory;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.modules.ModulesServiceFactory;
import com.google.appengine.api.urlfetch.FetchOptions;
import com.google.appengine.api.urlfetch.HTTPHeader;
import com.google.appengine.api.urlfetch.HTTPMethod;
import com.google.appengine.api.urlfetch.HTTPRequest;
import com.google.appengine.api.urlfetch.URLFetchService;
import com.google.appengine.api.urlfetch.URLFetchServiceFactory;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.appengine.api.utils.SystemProperty;
import com.google.common.base.Splitter;

@Controller
@RequestMapping(value = "dataUploadController")
public class DataUploadController {

	private AuthenticationDetailsSource<HttpServletRequest, ?> authenticationDetailsSource = new WebAuthenticationDetailsSource();
	private GaeAuthenticationProvider authenticationProvider = new GaeAuthenticationProvider();

	private Logger logger = LoggerFactory.getLogger(getClass());
	private Util util;
	private StudyBackEnd studyBackEnd;

	public DataUploadController() {
		util = new Util();
		studyBackEnd = new StudyBackEnd();
	}

	private BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();

	@RequestMapping(value = "upload/{studyId}", method = RequestMethod.POST)
	public ModelAndView upload(@PathVariable("studyId") String studyId, HttpServletRequest req, HttpServletResponse res)
			throws IOException {
		Map<String, List<BlobKey>> blobs = blobstoreService.getUploads(req);
		// List<BlobKey> blobKeys = blobs.get("myFile");
		List<BlobKey> blobKeys = blobs.get("files[]");

		if (blobKeys == null || blobKeys.isEmpty()) {
			res.sendRedirect("/");
			System.out.println("BolbKey Null or Empty");
			logger.error("BolbKey Null or Empty");

		} else {
			for (BlobKey key : blobKeys) {
				// BlobKey key = blobKeys.get(0);
				BlobInfoFactory blobInfoFactory = new BlobInfoFactory();
				BlobInfo blobInfo = blobInfoFactory.loadBlobInfo(key);
				String fileName = blobInfo.getFilename();
				// long fileSize = blobInfo.getSize();

				logger.info("File Uploaded to BlobStore.");

				List<DataSheet> dataSheets = extractSheets(key, fileName, studyId);
			}
		}

		return new ModelAndView("redirect:/cms/get_import");

	}

	@RequestMapping(value = "extractDataSheet", method = RequestMethod.GET)
	public void extarctDataSheet(@RequestParam("id") Long id, HttpServletRequest req, HttpServletResponse res) {
		logger.info("\n++++ Importing Data ++++\n");

		String currentAdmin = UserServiceFactory.getUserService().getCurrentUser().getEmail();
		String appId = SystemProperty.applicationId.get();
		String moduleName = "import-data";

		AdminTokenGenerator adminTokenGenerator = new AdminTokenGenerator();
		String tokenUrl = util.createUrl("/adminTokenGenerator/generateAdminToken", moduleName) + "?email="
				+ currentAdmin + "&_appId=" + appId;

		String adminToken = adminTokenGenerator.callURL(tokenUrl, req, res);

		if (adminToken != null) {
			// String url = createUrl("/dataImporter/extractandvalidate") +
			// "?id=" + id + "&_admin_token=" + adminToken
			// + "&_appId=" + appId + "&_user=" + currentAdmin;

			String url = util.createUrl("/sheetImporter/extractandvalidate", moduleName) + "?id=" + id
					+ "&_admin_token=" + adminToken + "&_appId=" + appId + "&_user=" + currentAdmin;

			logger.info("URL : " + url);

			util.fetchAsyncURL(url, req, res);
		}

		logger.info("\n++++ Importing Data ++++\n");

	}

	@RequestMapping(value = "uncomressZipFile", method = RequestMethod.GET)
	public void uncomressZipFile(@RequestParam("blobKeyString") String blobKeyString,
			@RequestParam("studyId") long studyId, HttpServletRequest req, HttpServletResponse res) {

		logger.info("\n++++ uncomressZipFile ++++\n");

		System.out.println("BLOBKEY: " + blobKeyString);
		System.out.println("STUDYID: " + studyId);
		try {
			if (util.validTokens(req)) {
				studyBackEnd.saveZipContentsAsDataSheets(blobKeyString, studyId);
			} else {
				System.err.println("Tokens InValid.");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@RequestMapping(value = "rollBackDataSheet", method = RequestMethod.GET)
	public void rollBackDataSheet(@RequestParam("sheetId") long sheetId, HttpServletRequest req,
			HttpServletResponse res) throws IOException {

		logger.info("\n++++ rollBackDataSheet ++++\n");

		System.out.println("SheetId: " + sheetId);

		if (util.validTokens(req)) {
			//TODO: put rollBack Logic
			studyBackEnd.rollBackSheet(sheetId);
		} else {
			System.err.println("Tokens InValid.");
		}

	}

	public GaeUserAuthentication createAuthorizedToken(HttpServletRequest request, HttpServletResponse response) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		if (authentication == null) {

			User googleUser = UserServiceFactory.getUserService().getCurrentUser();

			if (googleUser != null) {

				PreAuthenticatedAuthenticationToken token = new PreAuthenticatedAuthenticationToken(googleUser, null);
				token.setDetails(authenticationDetailsSource.buildDetails((HttpServletRequest) request));

				authentication = authenticationProvider.authenticate(token);
				SecurityContextHolder.getContext().setAuthentication(authentication);
			}
		}
		return (GaeUserAuthentication) SecurityContextHolder.getContext().getAuthentication();
	}

	public byte[] convertObjectToBytes(Object obj) {

		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutput out = null;
		byte[] result = null;
		try {
			out = new ObjectOutputStream(bos);
			out.writeObject(obj);
			out.flush();
			result = bos.toByteArray();

			System.out.print("");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				bos.close();
			} catch (IOException ex) {
				// ignore close exception
			}
		}
		return result;
	}

	@SuppressWarnings("unused")
	private Map<String, String> splitToMap(String in) {
		return Splitter.on(" ").withKeyValueSeparator("=").split(in);
	}

	public boolean extarctDataSheets(List<DataSheet> dataSheets, HttpServletResponse res) throws IOException {
		try {

			for (DataSheet dataSheet : dataSheets) {
				System.out.println("\n++++ Uploading " + dataSheet.getName() + " ++++\n");
				// dataExtractionController.extractAndValidate(res,
				// dataSheet.getId());
				System.out.println("\n++++ " + dataSheet.getName() + " Uploaded And Extracted. ++++\n");
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			return false;
		}
		return true;
	}

	public List<DataSheet> extractSheets(BlobKey blobKey, String fileName, String studyId) {
		logger.info("Extracting Sheets");
		UploadController uploadController = new UploadController();
		try {
			return uploadController.saveZipContentsAsDataSheets(blobstoreService, blobKey, fileName, studyId);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		return null;
	}

	@RequestMapping(value = "/hello_get", method = RequestMethod.GET)
	public void hello() {
		logger.info("hello_get");

		System.out.println("dasdasdasdasda==================================");

	}

	@RequestMapping(value = "/setDefaultCountry", method = RequestMethod.POST)
	public void setDefaultCountry() {
		System.out.println("\nadadasd+++====\n");
	}

	@RequestMapping(value = "uploadFile", method = RequestMethod.POST)
	public void uploadFile(HttpServletRequest req, HttpServletResponse res) throws IOException {
		Map<String, List<BlobKey>> blobs = blobstoreService.getUploads(req);
		List<BlobKey> blobKeys = blobs.get("myFile");

		if (blobKeys == null || blobKeys.isEmpty()) {
			res.sendRedirect("/");
			System.out.println("BolbKey Null or Empty");
			logger.error("BolbKey Null or Empty");

		} else {
			for (BlobKey key : blobKeys) {
				BlobInfoFactory blobInfoFactory = new BlobInfoFactory();
				BlobInfo blobInfo = blobInfoFactory.loadBlobInfo(key);
				String fileName = blobInfo.getFilename();
				String path = "/serve?blob_key=" + key.getKeyString();

				// FileService fileService =
				// FileServiceFactory.getFileService();
				// ServingUrlOptions servingOptions =
				// ServingUrlOptions.Builder.withBlobKey(key);

				try {
					RandomAccessFile raf = new RandomAccessFile(path, "rw");

					System.out.println("" + raf.readLine());
					raf.seek(0);

					System.out.println("" + raf.readLine());

				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
		}

	}
}

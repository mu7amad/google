/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/
package com.brightcreations.gplus.module.controllers;

import com.brightcreations.gplus.module.model.AdminToken;
import com.brightcreations.gplus.module.model.Answer;
import com.brightcreations.gplus.module.model.ApplicationUser;
import com.brightcreations.gplus.module.model.Brand;
import com.brightcreations.gplus.module.model.BrandPerQuestionPerDataSetPerLocation;
import com.brightcreations.gplus.module.model.Category;
import com.brightcreations.gplus.module.model.CmsDashBoard;
import com.brightcreations.gplus.module.model.ConfigEntity;
import com.brightcreations.gplus.module.model.CopyOfFilteredAnswerValue;
import com.brightcreations.gplus.module.model.Dashboard;
import com.brightcreations.gplus.module.model.DataOperation;
import com.brightcreations.gplus.module.model.DataRow;
import com.brightcreations.gplus.module.model.DataSet;
import com.brightcreations.gplus.module.model.DataSheet;
import com.brightcreations.gplus.module.model.DataSheetsZipEntry;
import com.brightcreations.gplus.module.model.DefaultDashBoard;
import com.brightcreations.gplus.module.model.Division;
import com.brightcreations.gplus.module.model.EmailMessage;
import com.brightcreations.gplus.module.model.FavouriteStudy;
import com.brightcreations.gplus.module.model.Filter;
import com.brightcreations.gplus.module.model.FilteredAnswerValue;
import com.brightcreations.gplus.module.model.Location;
import com.brightcreations.gplus.module.model.LogFile;
import com.brightcreations.gplus.module.model.Page;
import com.brightcreations.gplus.module.model.PointNotes;
import com.brightcreations.gplus.module.model.ProcessingQueueEntry;
import com.brightcreations.gplus.module.model.Question;
import com.brightcreations.gplus.module.model.QuestionFilterAnswerRows;
import com.brightcreations.gplus.module.model.Region;
import com.brightcreations.gplus.module.model.Settings;
import com.brightcreations.gplus.module.model.Study;
import com.brightcreations.gplus.module.model.TempFilteredAnswersValues;
import com.brightcreations.gplus.module.model.UnRegisteredUser;
import com.brightcreations.gplus.module.model.UserStudy;
import com.brightcreations.gplus.module.model.Widget;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;

public class OfyService {
	static {

		factory().register(Question.class);
		factory().register(Brand.class);
		factory().register(Filter.class);
		factory().register(Division.class);
		factory().register(Answer.class);
		factory().register(FilteredAnswerValue.class);
		factory().register(Location.class);
		factory().register(Widget.class);
		factory().register(DataSet.class);
		factory().register(DataSheet.class);
		factory().register(ApplicationUser.class);
		factory().register(EmailMessage.class);
		factory().register(LogFile.class);
		factory().register(TempFilteredAnswersValues.class);
		factory().register(CopyOfFilteredAnswerValue.class);
		factory().register(Region.class);
		// factory().register(Maps.class);
		factory().register(Settings.class);
		factory().register(BrandPerQuestionPerDataSetPerLocation.class);
		factory().register(DataOperation.class);
		factory().register(Dashboard.class);
		factory().register(DefaultDashBoard.class);
		factory().register(Page.class);
		factory().register(Category.class);
		factory().register(DataSheetsZipEntry.class);
		factory().register(ProcessingQueueEntry.class);
		factory().register(Study.class);
		factory().register(PointNotes.class);
		factory().register(DataRow.class);
		factory().register(AdminToken.class);
		factory().register(UserStudy.class);
		factory().register(CmsDashBoard.class);
		factory().register(FavouriteStudy.class);
		factory().register(ConfigEntity.class);
		factory().register(UnRegisteredUser.class);
		factory().register(QuestionFilterAnswerRows.class);
	}

	public static Objectify ofy() {
		ObjectifyService.ofy().clear();
		return ObjectifyService.ofy();
	}

	public static ObjectifyFactory factory() {
		return ObjectifyService.factory();
	}
}

/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.controllers;

/**
 *
 * @author Mohamed Salah El-Din
 *
 */
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.brightcreations.gplus.module.controllers.MIDataExplorer.StudyBackEnd;
import com.brightcreations.gplus.module.model.DataOperation;
import com.brightcreations.gplus.module.model.DataSheet;
import com.brightcreations.gplus.module.model.DataSheetsZipEntry;
import com.brightcreations.gplus.module.model.Study;
import com.brightcreations.gplus.module.multipleuploader.FileMeta;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.files.AppEngineFile;
import com.google.appengine.api.files.FileService;
import com.google.appengine.api.files.FileServiceFactory;
import com.google.appengine.api.files.FileWriteChannel;
import com.google.appengine.api.users.UserServiceFactory;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.cmd.Query;

@SuppressWarnings("deprecation")
@RequestMapping("cms/import")
public class UploadController {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@RequestMapping("/{studyID}")
	public ModelAndView form(@PathVariable(value = "studyID") String studyId,
			@RequestParam(value = "orderBy", defaultValue = "name", required = false) String orderBy,
			@RequestParam(value = "orderDir", defaultValue = "asc", required = false) String orderDir,
			@RequestParam(value = "offset", defaultValue = "0", required = false) Integer offset,
			@RequestParam(value = "count", defaultValue = "20", required = false) Integer count) {
		Map<String, Object> model = new HashMap<String, Object>();

		Study studyRef = OfyService.ofy().load().type(Study.class).filter("studyID", studyId).first().getValue();
		if (studyRef != null) {
			Query<DataSheet> query = OfyService.ofy().load().type(DataSheet.class).filter("study", studyRef);

			int originalCount = count;
			int totalEntries = query.count();
			if (offset < 0) {
				offset = 0;
			}
			if (count == 0) {
				count = totalEntries;
			}
			if (count >= totalEntries) {
				offset = 0;
				count = totalEntries;
			}
			if (offset + count > totalEntries) {
				count = totalEntries - offset;
			}
			if (offset > 0) {
				query = query.offset(offset);
			}
			if (count > 0) {
				query = query.limit(count);
			}
			if (orderBy != null && orderDir != null) {
				if (orderDir.equals("asc")) {
					query = query.order(orderBy);
				} else {
					query = query.order("-" + orderBy);
				}
			}

			List<DataSheet> dataSheets = query.list();
			if (dataSheets.size() == 0 && offset == 0 && Math.min(count + offset, totalEntries) == 0) {
				offset = -1;
				count += 1;
			}

			String currentAdmin = UserServiceFactory.getUserService().getCurrentUser().getEmail();
			for (DataSheet dataSheet : dataSheets) {
				if (dataSheet.getUploadedBy().equals(currentAdmin)) {
					dataSheet.setOwner(true);
				} else {
					dataSheet.setOwner(false);
				}
			}

			BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
			model.put("resources", dataSheets);
			model.put("total", totalEntries);
			model.put("from", offset);
			model.put("to", Math.min(count + offset, totalEntries));
			model.put("count", originalCount);
			model.put("orderDir", orderDir);
			model.put("orderBy", orderBy);

			StudyBackEnd studyController = new StudyBackEnd();
			// List<Study> userStudies = studyController.getUserStudies();
			// model.put("studies", userStudies);
			model.put("studyId", studyId);
			model.put("action", blobstoreService.createUploadUrl("/dataUploadController/upload/" + studyId));

			checkOperationLock(model);
			return new ModelAndView("cms/get_import", model);
		}
		return new ModelAndView("/", model);
		// return new ModelAndView("cms/upload_data", model);
	}

	@RequestMapping(value = "filterStudyFiles")
	public ModelAndView filterStudyFiles(@RequestParam("studyId") String studyId) {
		List<DataSheet> list = OfyService.ofy().load().type(DataSheet.class)
				.filter("study", Ref.create(Key.create(studyId))).list();
		Map<String, Object> model = new HashMap<String, Object>();

		return new ModelAndView("cms/get_import", model);
	}

	@RequestMapping("/entry")
	public ModelAndView getImportEntry(
			@RequestParam(value = "orderBy", defaultValue = "name", required = false) String orderBy,
			@RequestParam(value = "orderDir", defaultValue = "asc", required = false) String orderDir,
			@RequestParam(value = "offset", defaultValue = "0", required = false) Integer offset,
			@RequestParam(value = "count", defaultValue = "20", required = false) Integer count) {
		Map<String, Object> model = new HashMap<String, Object>();

		Query<DataSheetsZipEntry> query = OfyService.ofy().load().type(DataSheetsZipEntry.class);

		int originalCount = count;
		int totalEntries = query.count();
		if (offset < 0) {
			offset = 0;
		}
		if (count == 0) {
			count = totalEntries;
		}
		if (count >= totalEntries) {
			offset = 0;
			count = totalEntries;
		}
		if (offset + count > totalEntries) {
			count = totalEntries - offset;
		}
		if (offset > 0) {
			query = query.offset(offset);
		}
		if (count > 0) {
			query = query.limit(count);
		}
		if (orderBy != null && orderDir != null) {
			if (orderDir.equals("asc")) {
				query = query.order(orderBy);
			} else {
				query = query.order("-" + orderBy);
			}
		}

		List<DataSheetsZipEntry> dataSheetsZipEntries = query.list();
		List<Key<DataSheet>> dataSheetsKeys = new ArrayList<Key<DataSheet>>();
		for (DataSheetsZipEntry dataSheetsZipEntry : dataSheetsZipEntries) {
			dataSheetsKeys.addAll(dataSheetsZipEntry.getDatasheets());
		}
		Map<Key<DataSheet>, DataSheet> allDatasheets = OfyService.ofy().load().keys(dataSheetsKeys);

		if (dataSheetsZipEntries.size() == 0 && offset == 0 && Math.min(count + offset, totalEntries) == 0) {
			offset = -1;
			count += 1;
		}

		List<List<DataSheet>> dataSheets = new ArrayList<List<DataSheet>>();
		List<String> status = new ArrayList<String>();

		for (DataSheetsZipEntry dataSheetsZipEntry : dataSheetsZipEntries) {
			List<DataSheet> tempDataSheets = new ArrayList<DataSheet>();
			boolean processed = false;
			boolean uploaded = false;
			for (Key<DataSheet> dataSheetKey : dataSheetsZipEntry.getDatasheets()) {
				if (allDatasheets.get(dataSheetKey) != null) {
					if (allDatasheets.get(dataSheetKey).getStatus()
							.equals(DataSheet.dataSheetStatus.PROCESSED.getStatus())) {
						processed = true;
					} else if (allDatasheets.get(dataSheetKey).getStatus()
							.equals(DataSheet.dataSheetStatus.UPLOADED.getStatus())) {
						uploaded = true;
					}
					tempDataSheets.add(allDatasheets.get(dataSheetKey));
				}
			}
			if (processed && uploaded) {
				status.add("PU");
			} else if (processed) {
				status.add("P");
			} else if (uploaded) {
				status.add("U");
			}
			dataSheets.add(tempDataSheets);
		}

		model.put("resources", dataSheetsZipEntries);
		model.put("dataSheets", dataSheets);
		model.put("status", status);
		model.put("total", totalEntries);
		model.put("from", offset);
		model.put("to", Math.min(count + offset, totalEntries));
		model.put("count", originalCount);
		model.put("orderDir", orderDir);
		model.put("orderBy", orderBy);

		checkOperationLock(model);
		return new ModelAndView("cms/get_import_zip", model);
	}

	@RequestMapping(value = "upload", method = RequestMethod.GET)
	public void addFilee(HttpServletRequest request, HttpServletResponse response) throws IOException {
	}

	@RequestMapping(value = "upload", method = RequestMethod.POST)
	public Map<String, Object> doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		Map<String, Object> files = new HashMap<String, Object>();
		try {
			ServletFileUpload upload = new ServletFileUpload();
			res.setContentType("text/plain");

			FileItemIterator iterator = upload.getItemIterator(req);
			while (iterator.hasNext()) {
				FileItemStream item = iterator.next();
				InputStream stream = item.openStream();
				long totalLength = 0;
				if (item.isFormField()) {
					logger.error("Got a form field: " + item.getFieldName());
				} else {
					logger.error("Got an uploaded file: " + item.getFieldName() + ", name = " + item.getName());

					// You now have the filename (item.getName() and the
					// contents (which you can read from stream). Here we just
					// print them back out to the servlet output stream, but you
					// will probably want to do something more interesting (for
					// example, wrap them in a Blob and commit them to the
					// datastore).

					// Get a file service
					FileService fileService = FileServiceFactory.getFileService();

					// Create a new Blob file with mime-type "text/plain"
					AppEngineFile file = fileService.createNewBlobFile("application/zip", item.getName());

					// Open a channel to write to it
					boolean lock = false;
					FileWriteChannel writeChannel = fileService.openWriteChannel(file, lock);

					// Different standard Java ways of writing to the channel
					// are possible. Here we use a PrintWriter:
					PrintWriter out = new PrintWriter(Channels.newWriter(writeChannel, "UTF8"));

					// Close without finalizing and save the file path for
					// writing later
					out.close();

					// This time lock because we intend to finalize
					lock = true;
					writeChannel = fileService.openWriteChannel(file, lock);

					int len;

					byte[] buffer = new byte[8192];
					while ((len = stream.read(buffer, 0, buffer.length)) != -1) {
						totalLength += len;
						writeChannel.write(ByteBuffer.wrap(buffer, 0, len), null);
					}
					// Now finalize
					writeChannel.closeFinally();

					// Now read from the file using the Blobstore API
					BlobKey blobKey = fileService.getBlobKey(file);

					logger.error("File \"" + item.getName() + "\" saved to BlobStore");

					BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
					List<DataSheet> dataSheets = saveZipContentsAsDataSheets(blobstoreService, blobKey, item.getName(),
							"");

					if (!item.isFormField()) {

						FileMeta fileMeta = new FileMeta(item.getName(), totalLength,
								("/serve?blob-key=" + blobKey.getKeyString()),
								"http://files.softicons.com/download/system-icons/lozengue-filetype-icons-by-gurato/png/32/ZIP.png");

						List<FileMeta> list = new ArrayList<FileMeta>();
						fileMeta.setDataSheets(dataSheets);
						list.add(fileMeta);
						files.put("files", list);
					}
				}
			}
		} catch (Exception ex) {
			throw new ServletException(ex);
		}
		res.setStatus(HttpServletResponse.SC_ACCEPTED);
		return files;
	}

	/**
	 * @param blobstoreService
	 * @param blobKey
	 * @throws IOException
	 */
	public List<DataSheet> saveZipContentsAsDataSheets(BlobstoreService blobstoreService, BlobKey blobKey, String name,
			String studyId) throws IOException {

		Ref<Study> studyRef = OfyService.ofy().load().type(Study.class).filter("studyID", studyId).first();
		int readLimit = 5120;

		int start = 0;
		byte[] buff = null;
		buff = blobstoreService.fetchData(blobKey, start, (start + readLimit));
		start += readLimit + 1;

		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		buffer.write(buff);

		while (buff.length != 0) {

			buff = blobstoreService.fetchData(blobKey, start, (start + readLimit));
			buffer.write(buff);
			start += readLimit + 1;
		}

		ZipInputStream zipInputStream = new ZipInputStream(new ByteArrayInputStream(buffer.toByteArray()));
		ZipEntry zipEntry = zipInputStream.getNextEntry();

		List<DataSheet> dataSheets = new ArrayList<DataSheet>();

		while (zipInputStream.available() != 0) {

			DataSheet datasheet = new DataSheet();
			datasheet.setName(zipEntry.getName());
			datasheet.setBlobstoreKey(blobKey);
			datasheet.setUploadDate(new Date());
			datasheet.setStatus(DataSheet.dataSheetStatus.UPLOADED.getStatus());
			datasheet.setSize(zipEntry.getSize());
			datasheet.setUploadedBy(UserServiceFactory.getUserService().getCurrentUser().getEmail());
			datasheet.setStudy(studyRef);
			dataSheets.add(datasheet);
			zipEntry = zipInputStream.getNextEntry();

		}
		Map<Key<DataSheet>, DataSheet> datasheetsKeys = OfyService.ofy().save().entities(dataSheets).now();

		DataSheetsZipEntry dataSheetsZipEntry = new DataSheetsZipEntry();
		dataSheetsZipEntry.setStudyRef(studyRef);
		dataSheetsZipEntry.setDatasheets(new ArrayList<Key<DataSheet>>(datasheetsKeys.keySet()));
		dataSheetsZipEntry.setName(name);
		dataSheetsZipEntry.setUploadDate(new Date());
		dataSheetsZipEntry.setBlobstoreKey(blobKey);
		OfyService.ofy().save().entity(dataSheetsZipEntry).now();

		Study study = studyRef.getValue();
		study.setPendingFiles(dataSheets.size());
		OfyService.ofy().save().entity(study).now();

		return dataSheets;
	}

	public void checkOperationLock(Map<String, Object> model) {

		DataOperation dataOperation = OfyService.ofy().load().type(DataOperation.class).first().getValue();

		if (dataOperation == null) {
			model.put("lock", false);
		} else {
			model.put("lock", true);
		}
		model.put("dataOperation", dataOperation);
	}

}
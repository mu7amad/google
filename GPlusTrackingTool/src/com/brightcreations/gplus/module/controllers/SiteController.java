/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.controllers;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.client.HttpResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.brightcreations.gplus.module.model.Answer;
import com.brightcreations.gplus.module.model.ApplicationUser;
import com.brightcreations.gplus.module.model.Brand;
import com.brightcreations.gplus.module.model.BrandPerQuestionPerDataSetPerLocation;
import com.brightcreations.gplus.module.model.BrandRef;
import com.brightcreations.gplus.module.model.Category;
import com.brightcreations.gplus.module.model.Dashboard;
import com.brightcreations.gplus.module.model.DataSet;
import com.brightcreations.gplus.module.model.DefaultDashBoard;
import com.brightcreations.gplus.module.model.Division;
import com.brightcreations.gplus.module.model.Filter;
import com.brightcreations.gplus.module.model.FilteredAnswerValue;
import com.brightcreations.gplus.module.model.Location;
import com.brightcreations.gplus.module.model.LocationRef;
import com.brightcreations.gplus.module.model.Page;
import com.brightcreations.gplus.module.model.PointNotes;
import com.brightcreations.gplus.module.model.Question;
import com.brightcreations.gplus.module.model.QuestionRef;
import com.brightcreations.gplus.module.model.ResourceOrderCollection;
import com.brightcreations.gplus.module.model.SnapshotQueryResult;
import com.brightcreations.gplus.module.model.Study;
import com.brightcreations.gplus.module.model.TempFilteredAnswersValues;
import com.brightcreations.gplus.module.model.TrendQueryResult;
import com.brightcreations.gplus.module.model.Widget;
import com.brightcreations.gplus.module.model.collection.ResourceList;
import com.google.api.client.auth.oauth2.BearerToken;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.ByteArrayContent;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.Drive.Files;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.ParentReference;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.images.Composite;
import com.google.appengine.api.images.Image;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.common.base.Splitter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.cmd.Query;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import util.AuthorizationValidatior;

/**
 * 
 * @author Mohamed Salah El-Din
 * 
 */
@RequestMapping("/site")
public class SiteController {
	private static final Class<? extends Object> QuestionRef = null;
	private static final int HashMap = 0;
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private static List<Double> values = new ArrayList<Double>();

	/* ______________________________________________________________________________________ */
	/************************************* Navigation ****************************************/
	/* ____________________________________________________________________________________ */
	@RequestMapping("navigateToGraphBuilder")
	public ModelAndView navigateToGraphBuilder() {
		return new ModelAndView("site/graph_builder");
	}

	@RequestMapping("navigateToHome")
	public ModelAndView navigateToHome() {
		return new ModelAndView("site/home");
	}

	@RequestMapping(value = "js_test", method = RequestMethod.GET)
	public ModelAndView js_test() {
		Map<String, Object> model = new HashMap<String, Object>();
		return new ModelAndView("site/js_test", model);
	}

	@RequestMapping(value = "hello", method = RequestMethod.GET)
	public ModelAndView hello() {
		logger.info("hello_get");

		System.out.println("dasdasdasdasda==================================");

		Map<String, Object> model = new HashMap<String, Object>();
		return new ModelAndView("", model);

	}

	@RequestMapping(value = "testing", method = RequestMethod.GET)
	public ModelAndView test() {
		Map<String, Object> model = new HashMap<String, Object>();
		// List<Division> divisionsList = new ArrayList<Division>();

		// Query<FilteredAnswerValue> filteredAnswerValueQuery =
		// OfyService.ofy()
		// .load().type(FilteredAnswerValue.class).filter("dataSet",
		// Key<DataSet>.create(raw))
		// .filter("location", "").filter("question", "")
		// .filter("brand", "");

		// [Filter(87)/Division(98), Filter(83)/Division(90),
		// Filter(86)/Division(95), Filter(85)/Division(92),
		// Filter(88)/Division(104), Filter(89)/Division(107),
		// Filter(84)/Division(91)]

		List<FilteredAnswerValue> filteredAnswerValueQuery = OfyService.ofy().load().type(FilteredAnswerValue.class)
				.ancestor(Key.create(Key.create(Question.class, 42), Answer.class, 47))
				.filter("dataSet", Key.create(DataSet.class, 3)).filter("brand", Key.create(Brand.class, 6))
				.filter("location", Key.create(Location.class, 111))

				.filter("divisions", Key.create(Key.create(Filter.class, 87), Division.class, 98))
				.filter("divisions", Key.create(Key.create(Filter.class, 83), Division.class, 90))
				.filter("divisions", Key.create(Key.create(Filter.class, 86), Division.class, 95))
				.filter("divisions", Key.create(Key.create(Filter.class, 85), Division.class, 92))
				.filter("divisions", Key.create(Key.create(Filter.class, 88), Division.class, 104))
				.filter("divisions", Key.create(Key.create(Filter.class, 89), Division.class, 107))
				.filter("divisions", Key.create(Key.create(Filter.class, 84), Division.class, 91)).list();

		logger.error("Query Result Size: " + filteredAnswerValueQuery.size());

		for (FilteredAnswerValue filteredAnswerValue : filteredAnswerValueQuery) {
			logger.error("Query Result: " + filteredAnswerValue.getValue());
		}

		// Query<FilteredAnswerValue> filteredAnswerValueQuery =
		// OfyService.ofy()
		// .load().type(FilteredAnswerValue.class).filter("wave", "")
		// .filter("location", "")
		// .filter("brand", "");
		// for (Division division : divisionsList) {
		// filteredAnswerValueQuery.filter("divisions", division);
		// for (Division division : divisionsList) {
		// filteredAnswerValueQuery.filter("divisions", division);
		// }
		// List<FilteredAnswerValue> result = filteredAnswerValueQuery.list();
		// System.out.println("in " + result);
		return new ModelAndView("test", model);
	}

	// @RequestMapping(value = "testing", method = RequestMethod.GET)
	// public ModelAndView checkIfUserIsAdmin() {
	//
	// return null;
	// }
	//
	// @RequestMapping(value = "printlog", method = RequestMethod.GET)
	// public ModelAndView testLog(@RequestParam("key") Long key,
	// @RequestParam("parent") Long parentKey) {
	// Map<String, Object> model = new HashMap<String, Object>();
	//
	// // Key<LogFile> logKey = Key.create(LogFile.class, key);
	// LogFile logFile = OfyService
	// .ofy()
	// .load()
	// .key(Key.create(Key.create(DataSheet.class, parentKey),
	// LogFile.class, key)).getValue();
	// if (logFile != null) {
	// System.out.println(logFile.getQuestionsStatus());
	// System.out.println(logFile.getFiltersStatus());
	// } else {
	// System.out.println("found Nothin");
	// }
	// return new ModelAndView("test", model);
	// }

	/**
	 * 
	 * @param datasetId
	 * @return the entities used to update the snapshot view after changing the
	 *         wave
	 */
	@RequestMapping(value = "snapshot/dataset/{id}/related", method = RequestMethod.GET)
	public ModelAndView getSnapshotDataSetObjects(@PathVariable("id") Long datasetId) {
		Map<String, Object> model = new HashMap<String, Object>();
		DataSet dataset = OfyService.ofy().load().key(Key.create(DataSet.class, datasetId)).getValue();

		Map<Key<Question>, Question> questionsMap = OfyService.ofy().load().keys(dataset.getQuestions());
		Map<Key<Division>, Division> divisionsMap = OfyService.ofy().load().keys(dataset.getDivisions());

		Map<Key<Location>, Location> locationsMap = OfyService.ofy().load().keys(dataset.getLocations());

		ArrayList<Question> questions = null;
		ArrayList<Division> divisions = null;
		// List<Brand> brands = null;
		ArrayList<Location> locations = null;

		if (questionsMap != null) {
			questions = new ArrayList<Question>(questionsMap.values());
			if (questions != null) {
				// Collections.sort(questions, new Comparator<Question>() {
				//
				//
				// public int compare(Question q1, Question q2) {
				// if (q1.getBuzzWord() != null
				// && q2.getBuzzWord() != null) {
				// return q1.getBuzzWord().compareTo(q2.getBuzzWord());
				// } else if (q1.getBuzzWord() == null
				// && q2.getBuzzWord() != null) {
				// return -1;
				// } else if (q1.getBuzzWord() != null
				// && q2.getBuzzWord() == null) {
				// return 1;
				// } else {
				// return 0;
				// }
				// }
				// });
				questions = new ArrayList<Question>(orderQuestions(questions));
			}
		}

		Map<String, Question> questionsResultMap = new LinkedHashMap<String, Question>(questions.size());
		for (Question question : questions) {
			questionsResultMap.put(question.getId().toString(), question);
			Collections.sort(question.getAnswerGroups());

			List<Key<Answer>> answersKeys = question.getAnswers();
			ArrayList<Answer> answers = new ArrayList<Answer>(OfyService.ofy().load().keys(answersKeys).values());
			Set<String> answerGroups = new HashSet<String>();
			for (Answer answer : answers) {
				if (answer.getAnswerGroup() != null && !answer.getAnswerGroup().isEmpty()
						&& !answer.getAnswerGroup().equals("Default")) {
					answerGroups.add(answer.getAnswerGroup());
				}
			}
			question.setAnswerGroups(new ArrayList<String>(answerGroups));

		}

		if (divisionsMap != null) {
			divisions = new ArrayList<Division>(divisionsMap.values());
			if (divisions != null) {
				Collections.sort(divisions);
			}
		}

		if (locationsMap != null) {
			locations = new ArrayList<Location>(locationsMap.values());
			if (locations != null) {
				Collections.sort(locations);
			}
		}

		// List<Long> questionsIds = new ArrayList<Long>();
		List<Long> dataSetIds = new ArrayList<Long>();
		List<Long> locationIds = new ArrayList<Long>();

		for (Location location : locations) {
			locationIds.add(location.getId());
		}

		dataSetIds.add(datasetId);

		// for (Question question : questions) {
		// questionsIds.add(question.getId());
		// }

		// brands = getBrandsForDataSetandQuestionAndLocation(questionsIds,
		// dataSetIds, locationIds);
		//
		// if (brands == null || brands.isEmpty()) {
		// Map<Key<Brand>, Brand> brandsMap = OfyService.ofy().load()
		// .keys(dataset.getBrands());
		// if (brandsMap != null) {
		// brands = new ArrayList<Brand>(brandsMap.values());
		// if (brands != null) {
		// Collections.sort(brands);
		// }
		// }
		// } else {
		// Collections.sort(brands);
		// }

		// model.put("brands", brands);
		model.put("divisions", divisions);
		model.put("questions", questionsResultMap);
		model.put("questionsList", questions);
		model.put("locations", locations);
		model.put("showInSnapshotOnly", dataset.isShowenInSnapshotOnly());

		return new ModelAndView("", model);

	}

	/**
	 * 
	 * @param waveIdParam
	 * @param secondWaveIdParam
	 * @return the entities used to update the trends view after changing the
	 *         wave
	 */
	@RequestMapping(value = "trend/dataset/{id}/{sId}/related", method = RequestMethod.GET)
	public ModelAndView getTrendDataSetObjects(@PathVariable("id") Long waveIdParam,
			@PathVariable("sId") Long secondWaveIdParam) {

		Map<String, Object> model = new HashMap<String, Object>();

		List<String> dataSetsNames = new ArrayList<String>();

		List<DataSet> dataSets = getDataSetsInterval(waveIdParam, secondWaveIdParam, dataSetsNames, null, null);

		Set<Key<Question>> questionsKeys = new HashSet<Key<Question>>();
		Set<Key<Division>> divisionsKeys = new HashSet<Key<Division>>();
		Set<Key<Location>> locationKeys = new HashSet<Key<Location>>();

		Set<Key<Brand>> brandsKeys = new HashSet<Key<Brand>>();

		for (DataSet dataSet : dataSets) {
			questionsKeys.addAll(dataSet.getQuestions());
			brandsKeys.addAll(dataSet.getBrands());
			divisionsKeys.addAll(dataSet.getDivisions());
			locationKeys.addAll(dataSet.getLocations());
			brandsKeys.addAll(dataSet.getBrands());
			logger.info("LOCATIONS " + dataSet.getName() + " " + dataSet.getLocations());
		}

		Map<Key<Question>, Question> questionsMap = OfyService.ofy().load().keys(questionsKeys);
		Map<Key<Division>, Division> divisionsMap = OfyService.ofy().load().keys(divisionsKeys);
		Map<Key<Location>, Location> locationsMap = OfyService.ofy().load().keys(locationKeys);

		ArrayList<Question> questions = null;
		ArrayList<Division> divisions = null;
		List<Brand> brands = null;
		ArrayList<Location> locations = null;

		if (questionsMap != null) {
			questions = new ArrayList<Question>(questionsMap.values());
			if (questions != null) {
				// Collections.sort(questions, new Comparator<Question>() {
				//
				//
				// public int compare(Question q1, Question q2) {
				// if (q1.getBuzzWord() != null
				// && q2.getBuzzWord() != null) {
				// return q1.getBuzzWord().compareTo(q2.getBuzzWord());
				// } else if (q1.getBuzzWord() == null
				// && q2.getBuzzWord() != null) {
				// return -1;
				// } else if (q1.getBuzzWord() != null
				// && q2.getBuzzWord() == null) {
				// return 1;
				// } else {
				// return 0;
				// }
				// }
				// });
				// System.out.println("ordering");
				questions = new ArrayList<Question>(orderQuestions(questions));
			}
		}
		// for (Question question : questions) {
		// System.out.println(question.getName() + question.getId());
		// }
		Map<String, Question> questionsResultMap = new LinkedHashMap<String, Question>();
		for (Question question : questions) {
			// int ran = (int) (Math.random() * 10);
			questionsResultMap.put((question.getId()) + "", question);
			Collections.sort(question.getAnswerGroups());

			List<Key<Answer>> answersKeys = question.getAnswers();
			ArrayList<Answer> answers = new ArrayList<Answer>(OfyService.ofy().load().keys(answersKeys).values());
			Set<String> answerGroups = new HashSet<String>();
			for (Answer answer : answers) {
				if (answer.getAnswerGroup() != null && !answer.getAnswerGroup().isEmpty()
						&& !answer.getAnswerGroup().equals("Default")) {
					answerGroups.add(answer.getAnswerGroup());
				}
			}
			question.setAnswerGroups(new ArrayList<String>(answerGroups));
		}

		if (divisionsMap != null) {
			divisions = new ArrayList<Division>(divisionsMap.values());
			if (divisions != null) {
				Collections.sort(divisions);
			}
		}

		if (locationsMap != null) {
			locations = new ArrayList<Location>(locationsMap.values());
			if (locations != null) {
				Collections.sort(locations);
			}
		}

		List<Long> questionsIds = new ArrayList<Long>();
		List<Long> locationIds = new ArrayList<Long>();

		for (Location location : locations) {
			locationIds.add(location.getId());
		}

		for (Question question : questions) {
			questionsIds.add(question.getId());
		}

		brands = getBrandsForDataSetandQuestionAndLocation(questionsIds, waveIdParam, secondWaveIdParam, locationIds);

		if (brands == null || brands.isEmpty()) {
			Map<Key<Brand>, Brand> brandsMap = OfyService.ofy().load().keys(brandsKeys);
			if (brandsMap != null) {
				brands = new ArrayList<Brand>(brandsMap.values());
				if (brands != null) {
					Collections.sort(brands);
				}
			}
		} else {
			Collections.sort(brands);
		}

		model.put("brands", brands);

		model.put("divisions", divisions);
		model.put("questions", questionsResultMap);
		model.put("questionsList", questions);
		model.put("locations", locations);
		// System.out.println("trend Map " + questionsResultMap);
		return new ModelAndView("", model);

	}

	@RequestMapping("getbrands")
	private ModelAndView getBrands(@RequestParam("q") List<Long> questionIds, @RequestParam("w") Long firstDataSetId,
			@RequestParam(value = "ws", required = false) Long secondDataSetId,
			@RequestParam("l") List<Long> locationIds) {

		HashMap<String, Object> model = new HashMap<String, Object>();
		List<Brand> brands = getBrandsForDataSetandQuestionAndLocation(questionIds, firstDataSetId, secondDataSetId,
				locationIds);

		model.put("brands", brands);
		return new ModelAndView("", model);
	}

	private List<Brand> getBrandsForDataSetandQuestionAndLocation(List<Long> questionsIds, Long firstDataSetId,
			Long secondDataSetId, List<Long> locationIds) {
		// "W" + waveId + "Q" + questionId + "L" +locationId
		List<BrandPerQuestionPerDataSetPerLocation> brandPerQuestionPerDataSetPerLocationList = new ArrayList<BrandPerQuestionPerDataSetPerLocation>();
		List<Key<BrandPerQuestionPerDataSetPerLocation>> brandPerQuestionPerDataSetPerLocationKeysList = new ArrayList<Key<BrandPerQuestionPerDataSetPerLocation>>();
		List<Brand> brandsList = new ArrayList<Brand>();
		Set<Key<Brand>> brandsKeysSet = new HashSet<Key<Brand>>();

		List<DataSet> dataSets = null;
		if (secondDataSetId != null) {
			dataSets = getDataSetsInterval(firstDataSetId, secondDataSetId, new ArrayList<String>(), null, null);

			for (DataSet dataSet : dataSets) {
				for (Long locationId : locationIds) {
					for (Long questionId : questionsIds) {
						brandPerQuestionPerDataSetPerLocationKeysList
								.add(Key.create(BrandPerQuestionPerDataSetPerLocation.class,
										"W" + dataSet.getId() + "Q" + questionId + "L" + locationId));
					}
				}
			}
		} else {
			for (Long locationId : locationIds) {
				for (Long questionId : questionsIds) {
					brandPerQuestionPerDataSetPerLocationKeysList
							.add(Key.create(BrandPerQuestionPerDataSetPerLocation.class,
									"W" + firstDataSetId + "Q" + questionId + "L" + locationId));
				}
			}

		}

		brandPerQuestionPerDataSetPerLocationList = new ArrayList<BrandPerQuestionPerDataSetPerLocation>(
				OfyService.ofy().load().keys(brandPerQuestionPerDataSetPerLocationKeysList).values());
		for (BrandPerQuestionPerDataSetPerLocation lsitOfBrands : brandPerQuestionPerDataSetPerLocationList) {
			brandsKeysSet.addAll(lsitOfBrands.getBrands());
		}
		brandsList = new ArrayList<Brand>(OfyService.ofy().load().keys(brandsKeysSet).values());

		// return new ModelAndView("", model);
		return brandsList;
	}

	/**
	 * 
	 * @param trend
	 *            if the list of questions links to the snapshot or trends page
	 * @return The list of questions for view all questions page
	 *         (snapshot/trends)
	 */
	@RequestMapping(value = "allquestions/{studyId}", method = RequestMethod.GET)
	public ModelAndView getAllQuestions(@RequestParam("trend") boolean trend, @PathVariable("studyId") String studyId) {
		Map<String, Object> model = new HashMap<String, Object>();
		List<Question> questions = getPublishedQuestions(studyId);

		List<List<Question>> categorizedQuestions = new ArrayList<List<Question>>();
		Map<Key<Category>, List<Question>> categoriesMap = new HashMap<Key<Category>, List<Question>>();
		List<Category> categories = OfyService.ofy().load().type(Category.class).list();
		List<Key<Category>> categoriesKeys = new ArrayList<Key<Category>>();
		for (Category category : categories) {
			categoriesKeys.add(Key.create(category));
		}

		for (Question question : questions) {
			if (question.getCategory() != null && categoriesKeys.contains(question.getCategory())) {
				if (categoriesMap.keySet().contains(question.getCategory())) {
					categoriesMap.get(question.getCategory()).add(question);
				} else {
					categoriesMap.put(question.getCategory(), new ArrayList<Question>());
					categoriesMap.get(question.getCategory()).add(question);
				}
			}
		}

		Collections.sort(categories, new Comparator<Category>() {

			public int compare(Category c1, Category c2) {

				if (c1.getOrder() >= c2.getOrder()) {
					return 1;
				} else if (c1.getOrder() == c2.getOrder()) {
					return 0;
				} else {
					return -1;
				}
			}
		});

		List<Category> categoriesToSend = new ArrayList<Category>();

		for (Category category : categories) {
			if (categoriesMap.keySet().contains(Key.create(category))) {
				categorizedQuestions.add(categoriesMap.get(Key.create(category)));
				categoriesToSend.add(category);
			}
		}
		model.put("categories", categoriesToSend);
		model.put("questions", questions);
		model.put("dataArray", categorizedQuestions);
		Long userId = null;
		model.put("isTrend", trend);
		model.put("admin", isAdmin(userId));
		model.put("userId", userId);
		model.put("studyId", studyId);
		return new ModelAndView("site/get_questions_page", model);
	}

	public String getUrlParamtersMap(HttpServletRequest request) {
		String uri = request.getHeader("referer");
		String query = uri.split("\\?")[1];
		final Map<String, String> map = Splitter.on('&').trimResults().withKeyValueSeparator("=").split(query);
		// return map;

		String[] split = uri.split("/");
		return split[split.length - 1];
	}

	@RequestMapping(value = "snapshot/{studyId}", method = RequestMethod.GET)
	public ModelAndView getSnapshot(@RequestParam(value = "q", required = false) Long questionIdParam,
			@PathVariable(value = "studyId") String studyId,
			@RequestParam(value = "qs", required = false) List<Long> questionsIdsParam,
			@RequestParam(value = "w", required = false) Long waveIdParam,
			@RequestParam(value = "l", required = false) List<Long> countriesIdsParam,
			@RequestParam(value = "b", required = false) List<Long> brandsIdsParam,
			@RequestParam(value = "fd", required = false) List<String> filtersParam, HttpServletResponse response,
			HttpServletRequest request) throws UnsupportedEncodingException {

		Key<Study> studyKey = OfyService.ofy().load().type(Study.class).filter("studyID", studyId).first().getKey();

		long startDate = System.currentTimeMillis();
		System.err.println("mahmoud zaki logssssssssssssss ==            == " + startDate);
		Map<String, Object> model = new HashMap<String, Object>();

		if (questionIdParam == null && waveIdParam == null && countriesIdsParam == null && brandsIdsParam == null
				&& filtersParam == null) {
			Cookie[] cookies = request.getCookies();
			if (cookies != null) {

				for (int i = 0; i < cookies.length; i++) {
					if (cookies[i].getName().equals(
							"questionIDSnap_" + UserServiceFactory.getUserService().getCurrentUser().getUserId())) {
						questionIdParam = Long.parseLong(cookies[i].getValue());
						String url = "redirect:/site/snapshot/" + studyId + "?q=" + questionIdParam.toString();

						// Question question =
						// OfyService.ofy().load().key(Key.create(Question.class,
						// questionIdParam))
						// .getValue();

						Question question = OfyService.ofy().load().type(Question.class).filter("id", questionIdParam)
								.filter("studies", studyKey).first().getValue();
						if (question != null) {
							List<DataSet> datasets = new ArrayList<DataSet>(
									OfyService.ofy().load().keys(question.getDataSets()).values());

							if (datasets != null) {
								DataSet publishedDataSet = null;
								for (int j = datasets.size() - 1; j >= 0; j--) {
									DataSet dataset = datasets.get(j);
									if (dataset.isPublished()) {
										publishedDataSet = dataset;
										break;
									}
								}

								if (OfyService.ofy().load().key(Key.create(Question.class, questionIdParam))
										.getValue() != null && publishedDataSet != null)
									return new ModelAndView(url, model);
								else {
									break;
								}
							}
						}
					}
				}
			}

			// View All Questions page if cookie == null

			List<Question> questions = getPublishedQuestions(studyId);

			List<List<Question>> categorizedQuestions = new ArrayList<List<Question>>();
			Map<Key<Category>, List<Question>> categoriesMap = new HashMap<Key<Category>, List<Question>>();
			List<Category> categories = OfyService.ofy().load().type(Category.class).list();
			List<Key<Category>> categoriesKeys = new ArrayList<Key<Category>>();
			for (Category category : categories) {
				categoriesKeys.add(Key.create(category));
			}

			for (Question question : questions) {
				if (question.getCategory() != null && categoriesKeys.contains(question.getCategory())) {
					if (categoriesMap.keySet().contains(question.getCategory())) {
						categoriesMap.get(question.getCategory()).add(question);
					} else {
						categoriesMap.put(question.getCategory(), new ArrayList<Question>());
						categoriesMap.get(question.getCategory()).add(question);
					}
				}
			}

			Collections.sort(categories, new Comparator<Category>() {

				public int compare(Category c1, Category c2) {

					if (c1.getOrder() >= c2.getOrder()) {
						return 1;
					} else if (c1.getOrder() == c2.getOrder()) {
						return 0;
					} else {
						return -1;
					}
				}
			});

			for (Category category : categories) {
				if (categoriesMap.keySet().contains(Key.create(category))) {
					categorizedQuestions.add(categoriesMap.get(Key.create(category)));
				}
			}
			model.put("categories", categories);
			model.put("questions", questions);
			model.put("dataArray", categorizedQuestions);
			Long userId = null;
			model.put("admin", isAdmin(userId));
			model.put("isTrend", false);
			model.put("userId", userId);
			model.put("studyId", studyId);
			System.err.println(
					"mahmoud zaki logssssssssssssss ==            == " + (System.currentTimeMillis() - startDate));
			return new ModelAndView("site/get_questions_page", model);
		} else if (questionIdParam != null && waveIdParam == null && countriesIdsParam == null && brandsIdsParam == null
				&& filtersParam == null) {
			Cookie questionCookie = new Cookie(
					"questionIDSnap_" + UserServiceFactory.getUserService().getCurrentUser().getUserId(),
					questionIdParam.toString());
			response.addCookie(questionCookie);
			// Question question =
			// OfyService.ofy().load().key(Key.create(Question.class,
			// questionIdParam)).getValue();
			Question question = OfyService.ofy().load().type(Question.class).filter("id", questionIdParam)
					.filter("studies", studyKey).first().getValue();

			List<DataSet> datasets = new ArrayList<DataSet>(
					OfyService.ofy().load().keys(question.getDataSets()).values());
			Collections.sort(datasets);

			DataSet latestDataSetForSelectedQuestion = null;
			for (int i = datasets.size() - 1; i >= 0; i--) {
				DataSet dataset = datasets.get(i);
				if (dataset.isPublished()) {
					latestDataSetForSelectedQuestion = dataset;
					break;
				}
			}

			StringBuffer urlString = new StringBuffer(studyId);
			if (latestDataSetForSelectedQuestion != null) {

				// Long waveIdInit = OfyService.ofy().load().type(DataSet.class)
				// .first().getKey().getId();
				// Long countryIdInit =
				// OfyService.ofy().load().type(Location.class)
				// .first().getKey().getId();

				Long waveIdInit = latestDataSetForSelectedQuestion.getId();

				// Long countryIdInit = latestDataSetForSelectedQuestion
				// .getLocations()
				// .get(latestDataSetForSelectedQuestion.getLocations()
				// .size() - 1).getId();

				Long brandId = null;
				// Ref<Brand> googleBrand =
				// OfyService.ofy().load().type(Brand.class).filter("name",
				// "Google+").first();
				Ref<Brand> googleBrand = OfyService.ofy().load().type(Brand.class).filter("name", "Google+")
						.filter("studies", studyKey).first();
				if (googleBrand == null) {
					brandId = OfyService.ofy().load().type(Brand.class).first().getKey().getId();
				} else {
					brandId = googleBrand.key().getId();
				}
				String userEmail = UserServiceFactory.getUserService().getCurrentUser().getEmail().toLowerCase();
				ApplicationUser currUser = OfyService.ofy().load().type(ApplicationUser.class)
						.filter("email", userEmail).first().getValue();
				String defaultCountry = "";
				if (currUser != null) {
					if (currUser.getDefaultCountry() != null) {
						Location defaultCountryKey = OfyService.ofy().load().key(currUser.getDefaultCountry())
								.getValue();
						if (defaultCountryKey != null)
							defaultCountry = currUser.getDefaultCountry().getId() + "";
					}
				}

				// List<Filter> filtersInit =
				// OfyService.ofy().load().type(Filter.class).list();
				List<Filter> filtersInit = OfyService.ofy().load().type(Filter.class).filter("studies", studyKey)
						.list();

				urlString.append("?q=" + questionIdParam);
				urlString.append("&");
				urlString.append("w=" + waveIdInit);
				if (defaultCountry != "") {
					urlString.append("&");
					urlString.append("l=" + defaultCountry);
				}
				urlString.append("&");
				urlString.append("b=" + brandId);
				// urlString.append("&");
				// urlString.append("studyId=YouTube");
				for (Filter filter : filtersInit) {
					urlString.append("&");
					urlString.append("fd=" + filter.getId() + "_" + "s");
				}
			}
			System.err.println(
					"mahmoud zaki logssssssssssssss ==            == " + (System.currentTimeMillis() - startDate));
			return new ModelAndView("redirect:/site/snapshot/" + urlString.toString());
		}

		// Question question =
		// OfyService.ofy().load().key(Key.create(Question.class,
		// questionIdParam)).getValue();
		Question question = OfyService.ofy().load().type(Question.class).filter("id", questionIdParam)
				.filter("studies", studyKey).first().getValue();

		List<DataSet> datasets = new ArrayList<DataSet>(OfyService.ofy().load().keys(question.getDataSets()).values());
		Collections.sort(datasets);

		DataSet latestDataSetForSelectedQuestion = null;
		for (int j = datasets.size() - 1; j >= 0; j--) {
			DataSet dataset = datasets.get(j);
			if (dataset.isPublished()) {
				latestDataSetForSelectedQuestion = dataset;
				break;
			}
		}

		if (latestDataSetForSelectedQuestion != null) {

			List<Question> questions = new ArrayList<Question>(
					OfyService.ofy().load().keys(latestDataSetForSelectedQuestion.getQuestions()).values());

			Collections.sort(questions);

			// Map<Long, List<Answer>> answersByQuestionId = new HashMap<Long,
			// List<Answer>>();
			//
			// for (Question question : questions) {
			// answersByQuestionId
			// .put(question.getId(),
			// OfyService.ofy().load().type(Answer.class)
			// .ancestor(question).order("name").list());
			//
			// }

			List<Filter> filters = new ArrayList<Filter>(
					OfyService.ofy().load().keys(latestDataSetForSelectedQuestion.getFilters()).values());

			if (filters != null) {
				Collections.sort(filters);
			}

			// List<Division> divisions = new
			// ArrayList<Division>(OfyService.ofy()
			// .load()
			// .keys(latestDataSetForSelectedQuestion.getDivisions())
			// .values());
			//
			// Map<Long, List<Division>> divisionsByFilterId = new HashMap<Long,
			// List<Division>>();
			//
			// for (Division division : divisions) {
			// List<Division> filtersDivisionsList = divisionsByFilterId
			// .get(division.getFilterId());
			// if (filtersDivisionsList == null) {
			// filtersDivisionsList = new ArrayList<Division>();
			// divisionsByFilterId.put(division.getFilterId(),
			// filtersDivisionsList);
			// }
			// filtersDivisionsList.add(division);
			//
			// }

			List<DataSet> dataSets = OfyService.ofy().load().type(DataSet.class).filter("published", Boolean.TRUE)
					.filter("study", Ref.create(studyKey)).list();
			// List<DataSet> dataSets = new ArrayList<DataSet>();

			// for (DataSet dataSet : dataSetsRaw) {
			// if (dataSet.isPublished()) {
			// dataSets.add(dataSet);
			// }
			// }

			// List<Location> locationsRaw = OfyService.ofy().load()
			// .type(Location.class).list();

			// List<Location> locations = new
			// ArrayList<Location>(OfyService.ofy()
			// .load()
			// .keys(latestDataSetForSelectedQuestion.getLocations())
			// .values());
			//
			// Collections.sort(locations);

			// for (Location location : locationsRaw) {
			// boolean found = true;
			// for (DataSet dataSet : dataSets) {
			// if (!dataSet.getLocations().contains(Key.create(location))) {
			// found = false;
			// }
			// }
			// if (found) {
			// locations.add(location);
			// }
			// }

			Set<Key<Brand>> brandkeys = new HashSet<Key<Brand>>();

			for (DataSet dataSet : dataSets) {
				brandkeys.addAll(dataSet.getBrands());
			}

			ArrayList<Brand> brands = new ArrayList<Brand>(OfyService.ofy().load().keys(brandkeys).values());

			Collections.sort(brands);

			// model.put("selectedQuestionId", questionIdParam);
			// model.put("questions", questions);
//			 model.put("answers", answersByQuestionId);
			Long userId = null;
			model.put("admin", isAdmin(userId));
			model.put("filters", filters);
			// model.put("divisions", divisionsByFilterId);
			model.put("dataSets", dataSets);
			model.put("selectedDataSets", waveIdParam);
			// model.put("locations", locations);
			model.put("brands", brands);
			model.put("studyId", studyId);
		}
		System.err
				.println("mahmoud zaki logssssssssssssss ==            == " + (System.currentTimeMillis() - startDate));
		return new ModelAndView("site/get_snapshot", model);
	}

	private List<Question> getPublishedQuestions(String studyId) {

		List<Question> questions = new ArrayList<Question>();

		Key<Study> studyKey = OfyService.ofy().load().type(Study.class).filter("studyID", studyId).first().getKey();

		List<Key<DataSet>> publishedDataSets = OfyService.ofy().load().type(DataSet.class)
				.filter("published", Boolean.TRUE).filter("study", Ref.create(studyKey)).keys().list();

		List<Key<Study>> studies = new ArrayList<>();
		studies.add(studyKey);

		List<Question> allQuestions = OfyService.ofy().load().type(Question.class).filter("studies", studyKey).list();

		for (Question question : allQuestions) {
			boolean published = false;
			for (Key<DataSet> dataSetKey : publishedDataSets) {
				if (question.getDataSets().contains(dataSetKey)) {
					published = true;
					break;
				}
			}
			if (published) {
				questions.add(question);
			}
		}

		return orderQuestions(questions);
	}

	public List<Question> orderQuestions(List<Question> questions) {

		// List<Question> orderedQuestions = new ArrayList<Question>();
		// List<String> orderBy = new ArrayList<String>();
		// List<String> oldOrder = new ArrayList<String>();
		//
		// for (Question question : questions) {
		// if (question.getBuzzWord() == null
		// || question.getBuzzWord().isEmpty()) {
		// orderBy.add(question.getName());
		// oldOrder.add(question.getName());
		// } else {
		// orderBy.add(question.getBuzzWord());
		// oldOrder.add(question.getBuzzWord());
		// }
		//
		// }
		// Collections.sort(orderBy);
		//
		// for (String string : orderBy) {
		// int index = oldOrder.indexOf(string);
		// orderedQuestions.add(questions.get(index));
		// }

		// return orderedQuestions;
		Collections.sort(questions, new Comparator<Question>() {

			public int compare(Question q1, Question q2) {
				return q1.getListOrder().compareTo(q2.getListOrder());
			}
		});
		return questions;
	}

	@RequestMapping(value = "trend/{studyId}", method = RequestMethod.GET)
	public ModelAndView getTrend(@PathVariable(value = "studyId") String studyId,
			@RequestParam(value = "q", required = false) Long questionIdParam,
			@RequestParam(value = "qs", required = false) List<Long> questionsIdsParam,
			@RequestParam(value = "w", required = false) Long waveIdParam,
			@RequestParam(value = "ws", required = false) Long secondWaveIdParam,
			@RequestParam(value = "l", required = false) List<Long> countriesIdsParam,
			@RequestParam(value = "b", required = false) List<Long> brandsIdsParam,
			@RequestParam(value = "fd", required = false) List<String> filtersParam, HttpServletResponse response,
			HttpServletRequest request) throws UnsupportedEncodingException {

		Key<Study> studyKey = OfyService.ofy().load().type(Study.class).filter("studyID", studyId).first().getKey();

		Map<String, Object> model = new HashMap<String, Object>();

		if (questionIdParam == null && waveIdParam == null && countriesIdsParam == null && brandsIdsParam == null
				&& filtersParam == null) {

			Cookie[] cookies = request.getCookies();
			if (cookies != null) {

				for (int i = 0; i < cookies.length; i++) {
					if (cookies[i].getName().equals(
							"questionIDTrend_" + UserServiceFactory.getUserService().getCurrentUser().getUserId())) {
						questionIdParam = Long.parseLong(cookies[i].getValue());
						// String url = "redirect:/site/trend?q=" +
						// questionIdParam.toString();
						String url = "redirect:/site/trend/" + studyId + "?q=" + questionIdParam.toString();
						// Question question =
						// OfyService.ofy().load().key(Key.create(Question.class,
						// questionIdParam))
						// .getValue();
						Question question = OfyService.ofy().load().type(Question.class).filter("id", questionIdParam)
								.filter("studies", studyKey).first().getValue();
						if (question != null) {
							List<DataSet> datasets = new ArrayList<DataSet>(
									OfyService.ofy().load().keys(question.getDataSets()).values());
							if (datasets != null) {
								DataSet publishedDataSet = null;
								for (int j = datasets.size() - 1; j >= 0; j--) {
									DataSet dataset = datasets.get(j);
									if (dataset.isPublished()) {
										publishedDataSet = dataset;
										break;
									}
								}

								if (OfyService.ofy().load().key(Key.create(Question.class, questionIdParam))
										.getValue() != null && publishedDataSet != null)
									return new ModelAndView(url, model);
								else
									break;
							}
						}
					}
				}
			}
			List<Question> questions = getPublishedQuestions(studyId);

			List<List<Question>> categorizedQuestions = new ArrayList<List<Question>>();
			Map<Key<Category>, List<Question>> categoriesMap = new HashMap<Key<Category>, List<Question>>();
			List<Category> categories = OfyService.ofy().load().type(Category.class).list();
			List<Key<Category>> categoriesKeys = new ArrayList<Key<Category>>();
			for (Category category : categories) {
				categoriesKeys.add(Key.create(category));
			}

			for (Question question : questions) {
				if (question.getCategory() != null && categoriesKeys.contains(question.getCategory())) {
					if (categoriesMap.keySet().contains(question.getCategory())) {
						categoriesMap.get(question.getCategory()).add(question);
					} else {
						categoriesMap.put(question.getCategory(), new ArrayList<Question>());
						categoriesMap.get(question.getCategory()).add(question);
					}
				}
			}

			Collections.sort(categories, new Comparator<Category>() {

				public int compare(Category c1, Category c2) {

					if (c1.getOrder() >= c2.getOrder()) {
						return 1;
					} else if (c1.getOrder() == c2.getOrder()) {
						return 0;
					} else {
						return -1;
					}
				}
			});

			for (Category category : categories) {
				if (categoriesMap.keySet().contains(Key.create(category))) {
					categorizedQuestions.add(categoriesMap.get(Key.create(category)));
				}
			}
			model.put("categories", categories);
			model.put("questions", questions);
			model.put("dataArray", categorizedQuestions);
			Long userId = null;
			model.put("admin", isAdmin(userId));
			model.put("isTrend", true);
			model.put("userId", userId);
			model.put("studyId", studyId);
			return new ModelAndView("site/get_questions_page", model);

		} else if (questionIdParam != null && waveIdParam == null && countriesIdsParam == null && brandsIdsParam == null
				&& filtersParam == null) {
			Cookie questionCookie = new Cookie(
					"questionIDTrend_" + UserServiceFactory.getUserService().getCurrentUser().getUserId(),
					questionIdParam.toString());
			response.addCookie(questionCookie);
			Long waveIdInit = null;
			DataSet dataSetInit = null;
			// Question question =
			// OfyService.ofy().load().key(Key.create(Question.class,
			// questionIdParam)).getValue();
			Question question = OfyService.ofy().load().type(Question.class).filter("id", questionIdParam)
					.filter("studies", studyKey).first().getValue();
			List<DataSet> dataSets = OfyService.ofy().load().type(DataSet.class).filter("published", Boolean.TRUE)
					.filter("study", Ref.create(studyKey)).filter("showenInSnapshotOnly", Boolean.FALSE).list();
			Collections.sort(dataSets);

			List<DataSet> dataSetsForTheInitDataset = new ArrayList<DataSet>();

			for (DataSet dataSet : dataSets) {
				if (dataSet.getQuestions().contains(Key.create(Question.class, questionIdParam))) {
					dataSetsForTheInitDataset.add(dataSet);
					secondWaveIdParam = dataSet.getId();
					dataSetInit = dataSet;
				}
			}
			int counter = 0;
			for (int i = dataSetsForTheInitDataset.size() - 1; i >= 0; i--) {
				counter++;
				waveIdInit = dataSetsForTheInitDataset.get(i).getId();
				if (counter == 4) {
					break;
				}
			}

			String countryIdInit = "";
			// if (dataSetInit != null) {
			// countryIdInit = dataSetInit.getLocations().get(0).getId();
			// }

			String userEmail = UserServiceFactory.getUserService().getCurrentUser().getEmail().toLowerCase();
			ApplicationUser currUser = OfyService.ofy().load().type(ApplicationUser.class).filter("email", userEmail)
					.first().getValue();
			if (currUser != null) {
				if (currUser.getDefaultCountry() != null) {
					Location defaultCountryKey = OfyService.ofy().load().key(currUser.getDefaultCountry()).getValue();
					if (defaultCountryKey != null)
						countryIdInit = currUser.getDefaultCountry().getId() + "";
				}
			}

			// Long brandId = null;
			//
			// if (question != null) {
			// brandId = question.getBrands().get(0).getId();
			// }

			// List<Filter> filtersInit =
			// OfyService.ofy().load().type(Filter.class).list();
			List<Filter> filtersInit = OfyService.ofy().load().type(Filter.class).filter("studies", studyKey).list();
			StringBuffer urlString = new StringBuffer(studyId);
			urlString.append("?q=" + questionIdParam);
			urlString.append("&");
			urlString.append("w=" + waveIdInit);
			urlString.append("&");
			urlString.append("ws=" + secondWaveIdParam);
			if (countryIdInit != "") {
				urlString.append("&");
				urlString.append("l=" + countryIdInit);
			}
			// urlString.append("&");
			// urlString.append("b=" + brandId);

			for (Filter filter : filtersInit) {
				urlString.append("&");
				urlString.append("fd=" + filter.getId() + "_" + "s");
			}

			return new ModelAndView("redirect:/site/trend/" + urlString.toString());
		}
		List<DataSet> dataSets = OfyService.ofy().load().type(DataSet.class).filter("published", Boolean.TRUE)
				.filter("showenInSnapshotOnly", Boolean.FALSE).filter("study", Ref.create(studyKey)).list();

		List<Location> locationsRaw = OfyService.ofy().load().type(Location.class).list();

		List<Location> locations = new ArrayList<Location>();
		for (Location location : locationsRaw) {
			boolean found = true;
			for (DataSet dataSet : dataSets) {
				if (!dataSet.getLocations().contains(Key.create(location))) {
					found = false;
					break;
				}
			}
			if (found) {
				locations.add(location);
			}
		}

		if (secondWaveIdParam == null) {
			List<DataSet> dataSetsForSnap = new ArrayList<DataSet>();
			for (DataSet dataSet2 : dataSets) {
				for (Long country : countriesIdsParam) {

					if (dataSet2.getLocations().contains(Key.create(Location.class, country))) {
						dataSetsForSnap.add(dataSet2);
					}
				}
			}
			Collections.sort(dataSetsForSnap);

			long tempStartId = 0;
			for (int i = 0; i < dataSetsForSnap.size(); i++) {
				if (dataSetsForSnap.get(i).getId().equals(waveIdParam)) {
					int WaveIdParamIndex = i;
					for (int j = i; j > 0 && j >= i - 5; j--) {

						WaveIdParamIndex--;
					}
					tempStartId = dataSetsForSnap.get(WaveIdParamIndex).getId();
					WaveIdParamIndex = i;
					for (int j = i; j < dataSetsForSnap.size() - 1 && j <= i + 5; j++) {
						WaveIdParamIndex++;
					}
					secondWaveIdParam = dataSetsForSnap.get(WaveIdParamIndex).getId();
					break;
				}
			}

			// secondWaveIdParam = waveIdParam;

			waveIdParam = tempStartId;
			// secondWaveIdParam = waveIdParam;
			StringBuffer urlString = new StringBuffer("");
			urlString.append("q=" + questionIdParam);
			urlString.append("&");
			urlString.append("w=" + waveIdParam);
			urlString.append("&");
			urlString.append("ws=" + secondWaveIdParam);
			urlString.append("&");
			for (Long country : countriesIdsParam) {
				urlString.append("&");
				urlString.append("l=" + country);
			}
			for (Long brand : brandsIdsParam) {
				urlString.append("&");
				urlString.append("b=" + brand);
			}

			for (String filter : filtersParam) {
				urlString.append("&");
				urlString.append("fd=" + filter);
			}

			return new ModelAndView("redirect:/site/trend?" + urlString.toString());
		}

		List<Question> questions = OfyService.ofy().load().type(Question.class).filter("studies", studyKey).list();

		Map<Long, List<Answer>> answersByQuestionId = new HashMap<Long, List<Answer>>();

		for (Question question : questions) {
			answersByQuestionId.put(question.getId(),
					OfyService.ofy().load().type(Answer.class).ancestor(question).order("name").list());

		}

		List<Filter> filters = OfyService.ofy().load().type(Filter.class).order("name").filter("studies", studyKey)
				.list();

		if (filters != null) {
			Collections.sort(filters);
		}

		Map<Long, List<Division>> divisionsByFilterId = new HashMap<Long, List<Division>>();

		for (Filter filter : filters) {
			divisionsByFilterId.put(filter.getId(), OfyService.ofy().load().type(Division.class).ancestor(filter)
					.order("name").filter("studies", studyKey).list());

		}

		Set<Key<Brand>> brandkeys = new HashSet<Key<Brand>>();

		for (DataSet dataSet : dataSets) {
			brandkeys.addAll(dataSet.getBrands());
		}

		ArrayList<Brand> brands = new ArrayList<Brand>(OfyService.ofy().load().keys(brandkeys).values());

		Collections.sort(brands);

		model.put("selectedQuestionId", questionIdParam);
		model.put("questions", questions);
		model.put("answers", answersByQuestionId);
		// Long userId = null;
		// model.put("admin", isAdmin(userId));
		// model.put("userId", userId);
		model.put("filters", filters);
		model.put("divisions", divisionsByFilterId);
		model.put("dataSets", dataSets);
		model.put("locations", locations);
		model.put("brands", brands);
		model.put("studyId", studyId);

		return new ModelAndView("site/get_trend", model);
	}

	@RequestMapping("/location")
	public ModelAndView getLocation(@RequestParam(value = "sortby", defaultValue = "name") String sortBy,
			@RequestParam(value = "order", defaultValue = "asc") String order,
			@RequestParam(value = "from", defaultValue = "0") Long from,
			@RequestParam(value = "to", defaultValue = "100000") Long to) {
		Map<String, Object> model = new HashMap<String, Object>();
		List<Location> list = Location.getLocationList(sortBy, order, from, to);
		model.put("datalist", list);
		return new ModelAndView("datajsp", model);
	}

	@RequestMapping("/brand")
	public ModelAndView getBrand(@RequestParam(value = "sortby", defaultValue = "name") String sortBy,
			@RequestParam(value = "order", defaultValue = "asc") String order,
			@RequestParam(value = "from", defaultValue = "0") Long from,
			@RequestParam(value = "to", defaultValue = "100000") Long to) {
		Map<String, Object> model = new HashMap<String, Object>();
		List<Brand> list = Brand.getBrandList(sortBy, order, from, to);
		model.put("datalist", list);
		return new ModelAndView("datajsp", model);
	}

	@RequestMapping("/question")
	public ModelAndView getQuestion(@RequestParam(value = "sortby", defaultValue = "name") String sortBy,
			@RequestParam(value = "order", defaultValue = "asc") String order,
			@RequestParam(value = "from", defaultValue = "0") Long from,
			@RequestParam(value = "to", defaultValue = "100000") Long to) {
		Map<String, Object> model = new HashMap<String, Object>();
		List<Question> list = Question.getQuestionList(sortBy, order, from, to);
		model.put("datalist", list);
		return new ModelAndView("datajsp", model);
	}

	@RequestMapping("/filter")
	public ModelAndView getFilter(@RequestParam(value = "sortby", defaultValue = "name") String sortBy,
			@RequestParam(value = "order", defaultValue = "asc") String order,
			@RequestParam(value = "from", defaultValue = "0") Long from,
			@RequestParam(value = "to", defaultValue = "100000") Long to) {
		Map<String, Object> model = new HashMap<String, Object>();
		List<Filter> list = Filter.getFilterList(sortBy, order, from, to);
		model.put("datalist", list);
		return new ModelAndView("datajsp", model);
	}

	@RequestMapping("/answer")
	public ModelAndView getAnswer(@RequestParam(value = "sortby", defaultValue = "name") String sortBy,
			@RequestParam(value = "order", defaultValue = "asc") String order,
			@RequestParam(value = "from", defaultValue = "0") Long from,
			@RequestParam(value = "to", defaultValue = "100000") Long to) {
		Map<String, Object> model = new HashMap<String, Object>();
		List<Answer> list = Answer.getAnswerList(sortBy, order, from, to);
		model.put("datalist", list);
		return new ModelAndView("datajsp", model);
	}

	@RequestMapping("/division")
	public ModelAndView getDivision(@RequestParam(value = "sortby", defaultValue = "name") String sortBy,
			@RequestParam(value = "order", defaultValue = "asc") String order,
			@RequestParam(value = "from", defaultValue = "0") Long from,
			@RequestParam(value = "to", defaultValue = "100000") Long to) {
		Map<String, Object> model = new HashMap<String, Object>();
		List<Division> list = Division.getDivisionList(sortBy, order, from, to);
		model.put("datalist", list);
		return new ModelAndView("datajsp", model);
	}

	@RequestMapping(value = "/query", method = RequestMethod.GET)
	public ModelAndView queryForAnswersPage() {

		List<Question> questions = OfyService.ofy().load().type(Question.class).list();

		Map<Long, List<Answer>> answersByQuestionId = new HashMap<Long, List<Answer>>();

		for (Question question : questions) {
			answersByQuestionId.put(question.getId(),
					OfyService.ofy().load().type(Answer.class).ancestor(question).order("name").list());

		}

		List<Filter> filters = OfyService.ofy().load().type(Filter.class).order("name").list();

		Map<Long, List<Division>> divisionsByFilterId = new HashMap<Long, List<Division>>();

		for (Filter filter : filters) {
			divisionsByFilterId.put(filter.getId(),
					OfyService.ofy().load().type(Division.class).ancestor(filter).order("name").list());

		}

		List<DataSet> dataSets = OfyService.ofy().load().type(DataSet.class).list();

		List<Location> locations = OfyService.ofy().load().type(Location.class).list();

		List<Brand> brands = OfyService.ofy().load().type(Brand.class).order("name").list();

		Map<String, Object> model = new HashMap<String, Object>();

		model.put("questions", questions);
		model.put("answers", answersByQuestionId);
		model.put("filters", filters);
		model.put("divisions", divisionsByFilterId);
		model.put("dataSets", dataSets);
		model.put("locations", locations);
		model.put("brands", brands);

		return new ModelAndView("query", model);
	}

	@RequestMapping(value = "/querypage", method = RequestMethod.POST)
	public ModelAndView queryForAnswersResult(@RequestParam("ans") Long answerId, @RequestParam("ques") Long questionId,
			@RequestParam("ds") Long dataSetId, @RequestParam("loc") Long locationId, @RequestParam("br") Long brandId,
			@RequestParam("fil") Long[] filtersIds, @RequestParam("div") Long[] divisionsIds) {

		Query<FilteredAnswerValue> query = OfyService.ofy().load().type(FilteredAnswerValue.class)
				.ancestor(Key.create(Key.create(Question.class, questionId), Answer.class, answerId))
				.filter("dataSet", Key.create(DataSet.class, dataSetId))
				.filter("brand", Key.create(Brand.class, brandId))
				.filter("location", Key.create(Location.class, locationId));

		List<Key<Division>> divisionsKeys = new ArrayList<Key<Division>>();
		for (int i = 0; i < filtersIds.length; i++) {
			// divisionsKeys.add(Key.create(
			// Key.create(Filter.class, filtersIds[i]), Division.class,
			// divisionsIds[i]));
			Key<Division> key = Key.create(Key.create(Filter.class, filtersIds[i]), Division.class, divisionsIds[i]);
			divisionsKeys.add(key);
			query = query.filter("divisions", key);
		}
		// query.filter("divisions", divisionsKeys);
		FilteredAnswerValue filteredAnswerValueQuery = query.first().getValue();
		Map<String, Object> model = new HashMap<String, Object>();

		if (filteredAnswerValueQuery != null) {
			model.put("result", filteredAnswerValueQuery.getValue());
		}
		return new ModelAndView("", model);
	}

	@RequestMapping(value = "/newquery", method = RequestMethod.POST)
	public ModelAndView newQueryForAnswersResult(@RequestParam("ques") Long questionId,
			@RequestParam("ds") Long dataSetId, @RequestParam("loc") Long locationId, @RequestParam("br") Long brandId,
			@RequestParam("fil") Long[] filtersIds, @RequestParam("div") Long[] divisionsIds) {

		Key<DataSet> dataSetKey = Key.create(DataSet.class, dataSetId);
		Key<LocationRef> locationKey = Key.create(dataSetKey, LocationRef.class, locationId);
		Key<BrandRef> brandKey = Key.create(locationKey, BrandRef.class, brandId);
		Key<QuestionRef> questionKey = Key.create(brandKey, QuestionRef.class, questionId);
		Query<FilteredAnswerValue> query = OfyService.ofy().load().type(FilteredAnswerValue.class)
				.ancestor(questionKey);

		List<Key<Division>> divisionsKeys = new ArrayList<Key<Division>>();
		for (int i = 0; i < filtersIds.length; i++) {
			// divisionsKeys.add(Key.create(
			// Key.create(Filter.class, filtersIds[i]), Division.class,
			// divisionsIds[i]));
			Key<Division> key = Key.create(Key.create(Filter.class, filtersIds[i]), Division.class, divisionsIds[i]);
			divisionsKeys.add(key);
			query = query.filter("divisions", key);
		}
		// query.filter("divisions", divisionsKeys);
		List<FilteredAnswerValue> filteredAnswerValueQuery = query.list();
		Map<String, Object> model = new HashMap<String, Object>();
		logger.info("Result Size: " + filteredAnswerValueQuery.size());
		if (filteredAnswerValueQuery != null) {
			for (int i = 0; i < filteredAnswerValueQuery.size(); i++) {

				model.put("result" + i, filteredAnswerValueQuery.get(i).getValue());
				logger.info("Result: " + filteredAnswerValueQuery.get(i).getValue());
			}
		}
		return new ModelAndView("", model);
	}

	private List<List<Key<Division>>> generateCombinationsList(List<List<Key<Division>>> filters, int i) {

		// stop condition
		if (i == filters.size()) {
			// return a list with an empty list
			List<List<Key<Division>>> result = new ArrayList<List<Key<Division>>>();
			result.add(new ArrayList<Key<Division>>());
			return result;
		}

		List<List<Key<Division>>> result = new ArrayList<List<Key<Division>>>();
		List<List<Key<Division>>> recursive = generateCombinationsList(filters, i + 1); // recursive
																						// call

		// for each element of the first list of input
		for (int j = 0; j < filters.get(i).size(); j++) {
			// add the element to all combinations obtained for the rest of the
			// lists
			for (int k = 0; k < recursive.size(); k++) {
				// copy a combination from recursive
				List<Key<Division>> newList = new ArrayList<Key<Division>>();
				for (Key<Division> key : recursive.get(k)) {
					newList.add(key);
				}
				// add element of the first list
				newList.add(filters.get(i).get(j));
				// add new combination to result
				result.add(newList);
			}
		}

		return result;
	}

	public List<List<Key<Division>>> orderFiltersAndDivisions(Map<Filter, List<Division>> divisionByFilter) {
		List<Filter> tempFilters = new ArrayList<Filter>();
		List<List<Key<Division>>> divisions = new ArrayList<List<Key<Division>>>();
		for (Filter filter : divisionByFilter.keySet()) {
			tempFilters.add(filter);
		}
		Collections.sort(tempFilters, new Comparator<Filter>() {

			public int compare(Filter o1, Filter o2) {
				return o1.getId().compareTo(o2.getId());

			}

		});

		for (Filter filter : tempFilters) {
			List<Division> tempList = divisionByFilter.get(filter);
			List<Key<Division>> divisionsKeys = new ArrayList<Key<Division>>();
			for (Division division : tempList) {
				divisionsKeys.add(Key.create(division));
			}
			Collections.sort(divisionsKeys, new Comparator<Key<Division>>() {

				public int compare(Key<Division> o1, Key<Division> o2) {
					return (new Long(o1.getId())).compareTo(new Long(o2.getId()));

				}

			});
			divisions.add(divisionsKeys);
		}
		return divisions;
	}

	private int compareLists(List<List<Key<Division>>> combinations, List<Key<Division>> choice) {

		int index = -1;
		boolean found = false;
		for (int i = 0; i < combinations.size(); i++) {
			if (combinations.get(i).size() == choice.size()) {
				for (int k = 0; k < combinations.get(i).size(); k++) {
					if (!combinations.get(i).contains(choice.get(k))) {
						break;
					}
					if (k == combinations.get(i).size() - 1 && combinations.get(i).contains(choice.get(k))) {

						found = true;
						break;
					}
				}
				if (found == true) {
					index = i;
					break;
				}
			}
		}
		return index;
	}

	@RequestMapping(value = "/combquery", method = RequestMethod.POST)
	public ModelAndView newQuery(@RequestParam("ques") Long questionId, @RequestParam("ans") Long answerId,
			@RequestParam("ds") Long dataSetId, @RequestParam("loc") Long locationId, @RequestParam("br") Long brandId,
			@RequestParam("fil") Long[] filtersIds, @RequestParam("div") Long[] divisionsIds) {

		List<Key<Division>> divisionsKeys = new ArrayList<Key<Division>>();
		Map<Filter, List<Division>> divisionByFilter = new HashMap<Filter, List<Division>>();

		for (int i = 0; i < filtersIds.length; i++) {
			Filter filter = OfyService.ofy().load().key(Key.create(Filter.class, filtersIds[i])).getValue();
			List<Key<Division>> divisionsKey = filter.getDivisions();
			List<Division> divisions = new ArrayList<Division>(OfyService.ofy().load().keys(divisionsKey).values());
			divisionByFilter.put(filter, divisions);
		}

		for (int i = 0; i < filtersIds.length; i++) {
			Key<Division> key = Key.create(Key.create(Filter.class, filtersIds[i]), Division.class, divisionsIds[i]);
			divisionsKeys.add(key);
		}

		List<List<Key<Division>>> combinations = generateCombinationsList(orderFiltersAndDivisions(divisionByFilter),
				0);

		int rowDivisionsIndex = compareLists(combinations, divisionsKeys);

		FilteredAnswerValue FilteredAnswerValue = new FilteredAnswerValue();
		String ID = ("L" + locationId + "W" + dataSetId + "Q" + questionId + "A" + answerId + "B" + brandId + "C"
				+ rowDivisionsIndex);

		double result = OfyService.ofy().load().key(Key.create(FilteredAnswerValue.class, ID)).getValue().getValue();

		Map<String, Object> model = new HashMap<String, Object>();

		// System.out.println("Result: " + result);
		model.put("result", result);

		return new ModelAndView("", model);
	}

	@RequestMapping(value = "/queryquery", method = RequestMethod.GET)
	public ModelAndView allQuery(HttpServletResponse response,
			@RequestParam(value = "offset", defaultValue = "0", required = false) Integer offset,
			@RequestParam(value = "count", defaultValue = "0", required = false) Integer count) {

		Map<String, Object> model = new HashMap<String, Object>();

		if (values.isEmpty()) {
			logger.error("Creating List");
			int i = 0;
			for (;;) {
				values.add(new Double(64.5362088798));
				values.add(new Double(38.5577804219));
				values.add(new Double(57.5854944707));
				values.add(new Double(57.8785240729));
				values.add(new Double(28.8935099648));
				values.add(new Double(48.8702206549));
				values.add(new Double(24.0108796768));
				i++;
				if (i == 200)
					break;
			}
		}

		List<TempFilteredAnswersValues> result = OfyService.ofy().load().type(TempFilteredAnswersValues.class)
				.order("value").filter("value in", values).limit(count).offset(offset).list();

		// System.out.println("size " + result.size());
		model.put("result", result);
		return new ModelAndView("test", model);
	}

	@RequestMapping(value = "/allquery", method = RequestMethod.POST)
	public ModelAndView allQuery(@RequestParam("ques") Long questionId, @RequestParam("ans") Long answerId,
			@RequestParam("ds") Long dataSetId, @RequestParam("loc") Long locationId, @RequestParam("br") Long brandId,
			@RequestParam("fil") Long[] filtersIds, @RequestParam("div") Long[] divisionsIds) {

		List<Key<Division>> divisionsKeys = new ArrayList<Key<Division>>();
		Map<Filter, List<Division>> divisionByFilter = new HashMap<Filter, List<Division>>();

		for (int i = 0; i < filtersIds.length; i++) {
			Filter filter = OfyService.ofy().load().key(Key.create(Filter.class, filtersIds[i])).getValue();
			List<Key<Division>> divisionsKey = filter.getDivisions();
			List<Division> divisions = new ArrayList<Division>(OfyService.ofy().load().keys(divisionsKey).values());
			divisionByFilter.put(filter, divisions);
		}

		for (int i = 0; i < filtersIds.length; i++) {
			Key<Division> key = Key.create(Key.create(Filter.class, filtersIds[i]), Division.class, divisionsIds[i]);
			divisionsKeys.add(key);
		}

		List<List<Key<Division>>> combinations = generateCombinationsList(orderFiltersAndDivisions(divisionByFilter),
				0);

		List<Key<FilteredAnswerValue>> ids = new ArrayList<Key<FilteredAnswerValue>>();

		for (int i = 0; i < combinations.size(); i++) {
			String ID = ("L" + locationId + "W" + dataSetId + "Q" + questionId + "A" + answerId + "B" + brandId + "C"
					+ i);
			ids.add(Key.create(FilteredAnswerValue.class, ID));
		}
		Map<Key<FilteredAnswerValue>, FilteredAnswerValue> result = OfyService.ofy().load().keys(ids);

		int error = 0;
		int success = 0;

		for (Key<FilteredAnswerValue> key : ids) {
			if (result.get(key) == null) {
				error++;
			} else {
				success++;
			}
		}

		// System.out.println("size: " + result.size());
		Map<String, Object> model = new HashMap<String, Object>();

		// System.out.println("Result: " + result);

		model.put("error", error);
		model.put("success", success);

		return new ModelAndView("", model);
	}

	@RequestMapping(value = "querySnapshot", method = RequestMethod.GET)
	public ModelAndView query(@RequestParam(value = "qs", required = false) List<Long> questionsIdsParam,
			@RequestParam(value = "w", required = false) Long waveIdParam,
			@RequestParam(value = "l", required = false) List<Long> locationsIdsParam,
			@RequestParam(value = "b", required = false) List<Long> brandsIdsParam,
			@RequestParam(value = "fd", required = false) List<String> filtersDivParam,
			@RequestParam(value = "forDashBoard", required = false) Boolean forDashBoard,
			@RequestParam(value = "ag", required = false) List<String> answerGroupsByQuestionList,
			@RequestParam(value = "stacked", defaultValue = "true", required = false) boolean stacked,
			HttpServletRequest request, HttpServletResponse response) {

		// System.out.println(stacked);

		// System.out.println("in Post");
		// waveIdParam = new Long(4);
		// questionsIdsParam = new ArrayList<Long>();
		// questionsIdsParam.add(new Long(43));
		// brandsIdsParam = new ArrayList<Long>();
		// brandsIdsParam.add(new Long(5));
		// brandsIdsParam.add(new Long(6));
		// brandsIdsParam.add(new Long(7));
		// filtersDivParam = new ArrayList<String>();
		// filtersDivParam.add("46_53");
		// filtersDivParam.add("47_54");
		// filtersDivParam.add("48_55");
		// // filtersDivParam.add("48_56");
		// filtersDivParam.add("49_58");
		// filtersDivParam.add("50_69");
		// filtersDivParam.add("51_72");
		// filtersDivParam.add("52_75");
		// locationIdParam = new Long(76);

		Map<String, Object> model = new HashMap<String, Object>();

		Map<Long, List<Long>> filtersAndDivisionsIDs = new HashMap<Long, List<Long>>();

		for (String filterDiv : filtersDivParam) {
			String[] filterDivArray = filterDiv.split("_");
			Long filterID = new Long(filterDivArray[0]);
			Long divisionID = new Long(filterDivArray[1]);
			if (!filtersAndDivisionsIDs.containsKey(filterID)) {
				filtersAndDivisionsIDs.put(filterID, new ArrayList<Long>());
				filtersAndDivisionsIDs.get(filterID).add(divisionID);
			} else {
				filtersAndDivisionsIDs.get(filterID).add(divisionID);
			}
		}

		Map<Long, String> groupsByQuestionMap = new HashMap<Long, String>();

		if (answerGroupsByQuestionList != null) {

			for (String answerGroupsByQuestionItem : answerGroupsByQuestionList) {
				String[] ansGroupsSplit = answerGroupsByQuestionItem.split("_");
				Long questionId = new Long(ansGroupsSplit[0]);
				String answerGroup = ansGroupsSplit[1];
				groupsByQuestionMap.put(questionId, answerGroup);

			}
		}

		SnapshotQueryResult snapshotQueryResult = new SnapshotQueryResult(waveIdParam, brandsIdsParam,
				questionsIdsParam, filtersAndDivisionsIDs, locationsIdsParam, groupsByQuestionMap);

		snapshotQueryResult.process();
		int i = 0;
		List<List<Double>> valuesFormatted = new ArrayList<List<Double>>();
		DecimalFormat twoDForm = new DecimalFormat("#.##");
		List<List<Double>> valuesForChart = new ArrayList<List<Double>>();

		for (List<Double> subValues : snapshotQueryResult.getValues()) {
			valuesFormatted.add(new ArrayList<Double>());
			valuesForChart.add(new ArrayList<Double>());
			for (Double double1 : subValues) {
				valuesFormatted.get(i).add(Double.valueOf(twoDForm.format(double1)));
				if (double1.equals(new Double(-1000))) {
					valuesForChart.get(i).add(Double.valueOf(twoDForm.format(new Double(0))));
				} else {
					valuesForChart.get(i).add(Double.valueOf(twoDForm.format(double1)));
				}
			}
			i++;
		}

		model.put("valuesForChart", valuesForChart);
		model.put("brandsAndFilters", snapshotQueryResult.getBrandsAndFiltersNames());
		model.put("questionsAndAnswers", snapshotQueryResult.getQuestionAndAnswer());
		model.put("answers", snapshotQueryResult.getAllAnswers());
		model.put("values", valuesFormatted);
		model.put("counts", snapshotQueryResult.getNoOfanswersPerQuestion());
		model.put("divisions", snapshotQueryResult.getTableDivisions());
		if (forDashBoard != null && forDashBoard) {
			ArrayList<String> dataSetsNames = new ArrayList<String>();
			ArrayList<String> dataSetsDates = new ArrayList<String>();
			Ref<DataSet> wave = OfyService.ofy().load().key(Key.create(DataSet.class, waveIdParam));
			dataSetsNames.add(wave.getValue().getName());
			String date = new SimpleDateFormat("dd MMM yyyy").format(wave.getValue().getWaveDate());
			String[] dateArray = date.split(" ");
			date = dateArray[0];
			if (dateArray[0].substring(0).equals("0")) {
				if (dateArray[0].substring(1).equals("1"))
					date += "st";
				else if (dateArray[0].substring(1).equals("2"))
					date += "nd";
				else if (dateArray[0].substring(1).equals("3"))
					date += "rd";
			} else
				date += "th";
			for (int j = 1; j < dateArray.length; j++)
				date += " " + dateArray[j];
			dataSetsDates.add(date);

			model.put("brandsNames", snapshotQueryResult.getBrandsNames());
			model.put("questions", snapshotQueryResult.getQuestionNames());
			model.put("locations", snapshotQueryResult.getLocationsNames());
			model.put("dataSetsNames", dataSetsNames);
			model.put("dataSetsDates", dataSetsDates);
			model.put("filterString", snapshotQueryResult.getFiltersString());
			model.put("stacked", stacked);

		}
		List<FilteredAnswerValue> ids = new ArrayList<>();
		DecimalFormat df = new DecimalFormat("#.00");

		for (Map.Entry<Key<FilteredAnswerValue>, FilteredAnswerValue> entry : snapshotQueryResult.getResultsMap()
				.entrySet()) {
			Key<FilteredAnswerValue> key = entry.getKey();
			FilteredAnswerValue value = entry.getValue();
			String format = df.format(value.getValue());
			double d = Double.parseDouble(format);
			value.setValue(d);
			ids.add(value);

		}

		model.put("pointNotes", snapshotQueryResult.getPointNotes());
		model.put("filterAnswerIDs", ids);
		Cookie questionCookie = new Cookie(
				"questionIDSnap_" + UserServiceFactory.getUserService().getCurrentUser().getUserId(),
				questionsIdsParam.get(0).toString());
		response.addCookie(questionCookie);
		return new ModelAndView("", model);

	}

	@RequestMapping(value = "trendquery", method = RequestMethod.GET)
	public ModelAndView trendQuery(@RequestParam(value = "qs", required = false) List<Long> questionsIdsParam,
			@RequestParam(value = "w", required = false) Long waveIdParam,
			@RequestParam(value = "ws", required = false) Long secondWaveIdParam,
			@RequestParam(value = "l", required = false) List<Long> locationIdParam,
			@RequestParam(value = "b", required = false) List<Long> brandsIdsParam,
			@RequestParam(value = "fd", required = false) List<String> filtersDivParam,
			@RequestParam(value = "forDashBoard", required = false) Boolean forDashBoard,
			@RequestParam(value = "ag", required = false) List<String> answerGroupsByQuestionList,
			// @RequestParam(value = "forDash", required = false) Boolean
			// forDash,
			HttpServletResponse response) {
		Map<String, Object> model = new HashMap<String, Object>();

		Map<Long, List<Long>> filtersAndDivisionsIDs = new HashMap<Long, List<Long>>();

		for (String filterDiv : filtersDivParam) {
			String[] filterDivArray = filterDiv.split("_");
			Long filterID = new Long(filterDivArray[0]);
			Long divisionID = new Long(filterDivArray[1]);
			if (!filtersAndDivisionsIDs.containsKey(filterID)) {
				filtersAndDivisionsIDs.put(filterID, new ArrayList<Long>());
				filtersAndDivisionsIDs.get(filterID).add(divisionID);
			} else {
				filtersAndDivisionsIDs.get(filterID).add(divisionID);
			}
		}

		List<String> dataSetsNames = new ArrayList<String>();
		List<String> dataSetsDates = new ArrayList<String>();

		List<DataSet> dataSetsInRange = getDataSetsInterval(waveIdParam, secondWaveIdParam, dataSetsNames,
				dataSetsDates, forDashBoard);

		// List<DataSet> dataSetsInRange = new ArrayList<DataSet>();

		// for (DataSet dataSet : rawDataSetsInRange) {
		// if (!dataSet.isShowenInSnapshotOnly()) {
		// dataSetsInRange.add(dataSet);
		// }
		// }

		ResourceList<DataSet> dataSetsResourceList = new ResourceList<DataSet>(dataSetsInRange);

		Map<Long, String> groupsByQuestionMap = new HashMap<Long, String>();

		if (answerGroupsByQuestionList != null) {

			for (String answerGroupsByQuestionItem : answerGroupsByQuestionList) {
				String[] ansGroupsSplit = answerGroupsByQuestionItem.split("_");
				Long questionId = new Long(ansGroupsSplit[0]);
				String answerGroup = ansGroupsSplit[1];
				groupsByQuestionMap.put(questionId, answerGroup);

			}
		}

		List<Key<DataSet>> dataSetsKeys = dataSetsResourceList.getKeyList();
		TrendQueryResult trendQueryResult = new TrendQueryResult(dataSetsKeys, brandsIdsParam, questionsIdsParam,
				filtersAndDivisionsIDs, locationIdParam, groupsByQuestionMap);

		trendQueryResult.process();
		List<PointNotes> pointNotes = trendQueryResult.getPointNotes();

		int i = 0;
		List<List<Double>> valuesFormatted = new ArrayList<List<Double>>();
		DecimalFormat twoDForm = new DecimalFormat("#.##");
		List<List<Double>> valuesForChart = new ArrayList<List<Double>>();

		for (List<Double> subValues : trendQueryResult.getValues()) {
			valuesFormatted.add(new ArrayList<Double>());
			valuesForChart.add(new ArrayList<Double>());
			for (Double double1 : subValues) {
				valuesFormatted.get(i).add(Double.valueOf(twoDForm.format(double1)));
				if (double1.equals(new Double(-1000))) {
					valuesForChart.get(i).add(Double.valueOf(twoDForm.format(new Double(0))));
				} else {
					valuesForChart.get(i).add(Double.valueOf(twoDForm.format(double1)));
				}

			}
			i++;
		}
		// System.out.println(trendQueryResult.getValues());
		// System.out.println(trendQueryResult.getQuestionAndAnswer());
		// System.out.println(trendQueryResult.getBrandsAndFiltersNames());
		// if (forDashBoard != null && !forDashBoard) {
		// dataSetsNames = new ArrayList<String>();
		// for (DataSet dataSet : dataSetsInRange) {
		// dataSetsNames.add(dataSet.getName());
		// }
		// }

		List<FilteredAnswerValue> ids = new ArrayList<>();
		DecimalFormat df = new DecimalFormat("#.00");

		for (Map.Entry<Key<FilteredAnswerValue>, FilteredAnswerValue> entry : trendQueryResult.getResultsMap()
				.entrySet()) {
			Key<FilteredAnswerValue> key = entry.getKey();
			FilteredAnswerValue value = entry.getValue();
			String format = df.format(value.getValue());
			double d = Double.parseDouble(format);
			value.setValue(d);
			ids.add(value);

		}
		model.put("answers", trendQueryResult.getAllAnswers());
		model.put("counts", trendQueryResult.getNoOfanswersPerQuestion());
		model.put("dataSetsNames", dataSetsNames);
		model.put("all", trendQueryResult.getAllHeaders());
		model.put("values", valuesFormatted);
		model.put("valuesForChart", valuesForChart);
		model.put("divisions", trendQueryResult.getTableDivisions());
		model.put("pointNotes", pointNotes);
		model.put("filterAnswerIDs", ids);
		// if(forDash!=null && forDash)
		// {
		// model.put("questions", trendQueryResult.getQuestionNames());
		// model.put("filtersAndDivisions",
		// trendQueryResult.getBrandsAndFiltersNames());
		//
		// }
		if (forDashBoard != null && forDashBoard) {
			model.put("brandsNames", trendQueryResult.getBrandsNames());
			model.put("questions", trendQueryResult.getQuestionNames());
			model.put("locations", trendQueryResult.getLocationsNames());
			model.put("dataSetsDates", dataSetsDates);
			model.put("filterString", trendQueryResult.getFiltersString());
		}
		Cookie questionCookie = new Cookie(
				"questionIDTrend_" + UserServiceFactory.getUserService().getCurrentUser().getUserId(),
				questionsIdsParam.get(0).toString());
		response.addCookie(questionCookie);
		return new ModelAndView("", model);

	}

	@RequestMapping(value = "country", method = RequestMethod.GET)
	public ModelAndView getPublishedLocations() {
		List<Location> publishedLocations = new ArrayList<Location>();
		List<DataSet> AllDataSets = OfyService.ofy().load().type(DataSet.class).filter("published", Boolean.TRUE)
				.list();
		Set<Key<Location>> locationSet = new HashSet<Key<Location>>();
		Map<String, Object> model = new HashMap<String, Object>();
		for (DataSet dataSet : AllDataSets) {
			for (Key<Location> location : dataSet.getLocations()) {
				locationSet.add(location);
			}
		}
		publishedLocations = new ArrayList<Location>(OfyService.ofy().load().keys(locationSet).values());
		Collections.sort(publishedLocations);
		model.put("publishedLocations", publishedLocations);

		return new ModelAndView("", model);
	}

	@RequestMapping(value = "/setDefaultCountry", method = RequestMethod.POST)
	public void setDefaultCountry(@RequestParam("country") Long countryId, HttpServletRequest request,
			HttpServletResponse response) {
		logger.info("setDeafultCountry Method");
		System.out.println("\n++++++++++setDeafultCountry ++++++++\n");
		String userEmail = UserServiceFactory.getUserService().getCurrentUser().getEmail().toLowerCase();
		ApplicationUser currUser = OfyService.ofy().load().type(ApplicationUser.class).filter("email", userEmail)
				.first().getValue();
		Key<Location> defaultCountryKey = Key.create(Location.class, countryId);
		Location defaultCountry = OfyService.ofy().load().key(defaultCountryKey).getValue();
		// if (currUser.getDefaultCountry() == null) {
		if (defaultCountryKey != null) {

			DefaultDashBoard defaultCountryDefaultDashboard = OfyService.ofy().load()
					.key(Key.create(DefaultDashBoard.class, countryId)).get();

			if (defaultCountryDefaultDashboard != null) {
				logger.info("Default Country Dashboard: " + defaultCountryDefaultDashboard.getId());
				logger.info("Default Country Dashboard ID: " + countryId);

				Dashboard defaultCountryDashboard = OfyService.ofy().load()
						.key(defaultCountryDefaultDashboard.getDashboard()).get();

				logger.info("Default Country Dashboard dash: " + defaultCountryDashboard.getName() + "  --  "
						+ defaultCountryDashboard.getId());
				if (defaultCountryDashboard != null) {
					List<Dashboard> dashboardsForUser = OfyService.ofy().load().type(Dashboard.class)
							.ancestor(Key.create(currUser)).list();
					for (Dashboard dashboard : dashboardsForUser) {
						if (dashboard.getDefaultDashBoard() == true) {
							dashboard.setDefaultDashBoard(false);
							OfyService.ofy().save().entity(dashboard).now();
							// break;
						}
					}
					Dashboard defaultDashboard = new Dashboard();
					// defaultDashboard.setDashboardUser(currUser);
					defaultDashboard.setName(defaultCountryDashboard.getName());
					defaultDashboard.setDefaultDashBoard(true);
					Key<Dashboard> keyDashBoard = OfyService.ofy().save().entity(defaultDashboard).now();
					List<Widget> widgetsForCountry = OfyService.ofy().load().type(Widget.class)
							.ancestor(Key.create(defaultCountryDashboard)).list();
					for (Widget widget : widgetsForCountry) {
						widget.setId(null);
						// widget.setDashboard(keyDashBoard);
						Ref<Dashboard> dashboardRef = Ref.create(keyDashBoard);
						// widget.setDashboard(dashboardRef);

					}
					OfyService.ofy().save().entities(widgetsForCountry).now();
				}
			}
		}

		// }
		currUser.setDefaultCountry(defaultCountryKey);
		OfyService.ofy().save().entity(currUser).now();
		Cookie locationCookie = new Cookie(
				"location_" + UserServiceFactory.getUserService().getCurrentUser().getUserId(), countryId.toString());
		response.addCookie(locationCookie);

		// return null;

	}

	/**
	 * Used by the main.js in the site_ui in the AutoLaunch function to check
	 * for the default country and if the video boolean is true or false
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "checkDefaultCountry", method = RequestMethod.GET)
	public ModelAndView checkForDefaultCountry(HttpServletRequest request, HttpServletResponse response) {
		String userEmail = UserServiceFactory.getUserService().getCurrentUser().getEmail().toLowerCase();
		ApplicationUser currUser = OfyService.ofy().load().type(ApplicationUser.class).filter("email", userEmail)
				.first().getValue();
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("showVideo", currUser.isShowVideo());
		Cookie videoCookie = new Cookie("showVideo_" + UserServiceFactory.getUserService().getCurrentUser().getUserId(),
				currUser.isShowVideo() + "");
		response.addCookie(videoCookie);
		if (currUser.getDefaultCountry() != null) {
			Long countryId = currUser.getDefaultCountry().getId();
			Cookie locationCookie = new Cookie(
					"location_" + UserServiceFactory.getUserService().getCurrentUser().getUserId(),
					countryId.toString());
			response.addCookie(locationCookie);
			model.put("country", countryId);
		}
		return new ModelAndView("", model);
	}

	// @RequestMapping(value = "home", method = RequestMethod.GET)
	// public ModelAndView tempHome() {
	// Map<String, Object> model = new HashMap<String, Object>();
	// return new ModelAndView("site/home", model);
	// }

	/**
	 * @param waveIdParam
	 * @param secondWaveIdParam
	 * @param dataSetsNames
	 * @return
	 */
	private List<DataSet> getDataSetsInterval(Long waveIdParam, Long secondWaveIdParam, List<String> dataSetsNames,
			List<String> dataSetsDates, Boolean forDashBoard) {
		Date startDate = null;
		Date endDate = null;
		List<DataSet> dataSets = OfyService.ofy().load().type(DataSet.class).list();

		Collections.sort(dataSets);

		for (DataSet dataSet : dataSets) {
			if (dataSet.getId().compareTo(waveIdParam) == 0) {
				startDate = dataSet.getWaveDate();
			}
			if (dataSet.getId().compareTo(secondWaveIdParam) == 0) {
				endDate = dataSet.getWaveDate();
			}
		}

		List<DataSet> dataSetsInRange = new ArrayList<DataSet>();
		for (DataSet dataSet : dataSets) {
			if (dataSet.getWaveDate() != null && dataSet.isPublished()) {
				if (dataSet.getWaveDate().compareTo(startDate) > -1 && dataSet.getWaveDate().compareTo(endDate) < 1
						&& !dataSet.isShowenInSnapshotOnly()) {
					dataSetsInRange.add(dataSet);
					if (forDashBoard != null && forDashBoard) {
						dataSetsNames.add(dataSet.getName());
					} else {
						dataSetsNames.add(dataSet.getName() + " - "
								+ new SimpleDateFormat("MMM yyyy").format(dataSet.getWaveDate()));
					}
					if (dataSetsDates != null) {

						String date = new SimpleDateFormat("dd MMM yyyy").format(dataSet.getWaveDate());
						String[] dateArray = date.split(" ");
						date = dateArray[0];
						if (dateArray[0].substring(0).equals("0")) {
							if (dateArray[0].substring(1).equals("1"))
								date += "st";
							else if (dateArray[0].substring(1).equals("2"))
								date += "nd";
							else if (dateArray[0].substring(1).equals("3"))
								date += "rd";
						} else
							date += "th";
						for (int j = 1; j < dateArray.length; j++)
							date += " " + dateArray[j];
						dataSetsDates.add(date);
					}
				}
			} else {
			}
		}
		return dataSetsInRange;
	}

	@RequestMapping(value = "export", method = RequestMethod.POST)
	public ModelAndView exportTrend(HttpServletResponse response, HttpServletRequest request,
			@RequestParam("tablesNames") String tablesNames) {
		String json = request.getParameter("json");

		XlsExport xlsExport = new XlsExport();
		Type table = new TypeToken<LinkedHashMap<String, List<List<String>>>>() {
		}.getType();
		Gson gson = new Gson();
		LinkedHashMap<String, List<List<String>>> tables = gson.fromJson(json, table);

		Type tablesNamesType = new TypeToken<LinkedHashMap<String, String>>() {
		}.getType();

		LinkedHashMap<String, String> tablesNamesMapByClass = gson.fromJson(tablesNames, tablesNamesType);

		try {

			xlsExport.exporterXLS(response, request, tables, tablesNamesMapByClass, "export");
		} catch (RowsExceededException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WriteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "exportpng", method = RequestMethod.POST)
	private void getPngExport(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("fn") String fileName) throws IOException, DecoderException {

		response.setContentType("image/png");
		response.setHeader("Content-Disposition",
				"attachment; filename=\"" + fileName.replace("\"", "") + ".png" + "\"");

		OutputStream output = response.getOutputStream();

		String svgImageData = request.getParameter("svg");
		svgImageData = svgImageData.replace("data:image/png;base64,", "");

		Base64 b = new Base64();
		byte[] imageArray = b.decode(svgImageData.getBytes());

		Collection<Composite> composites = new ArrayList<Composite>();
		ImagesService imagesService = ImagesServiceFactory.getImagesService();

		Image graphImage = ImagesServiceFactory.makeImage(imageArray);

		Composite graphComposite = ImagesServiceFactory.makeComposite(graphImage, 0, 0, 1f, Composite.Anchor.TOP_LEFT);

		composites.add(graphComposite);

		output.write(imageArray);

	}

	private boolean isAdmin(Long userId) {
		boolean admin = false;
		if (UserServiceFactory.getUserService().getCurrentUser() != null) {
			String email = UserServiceFactory.getUserService().getCurrentUser().getEmail().toLowerCase().trim();
			ApplicationUser user = null;
			try {
				user = OfyService.ofy().load().type(ApplicationUser.class).filter("email", email).first().getValue();
				userId = user.getId();
				if (!user.isOnlyUser()) {
					admin = true;
				}
			} catch (Exception e) {
			}
		}
		return admin;
		// if (user != null) {
		// if(!user.isOnlyUser())return true;
		// }
		// return false;

	}

	/*************************************************************************************************/
	/***************************************
	 * video Notification
	 ***********************************************/
	@RequestMapping(value = "setDefaultVideoNotification", method = RequestMethod.POST)
	public ModelAndView setVideoNotification(HttpServletRequest request, HttpServletResponse response) {
		String userEmail = UserServiceFactory.getUserService().getCurrentUser().getEmail().toLowerCase();
		ApplicationUser currUser = OfyService.ofy().load().type(ApplicationUser.class).filter("email", userEmail)
				.first().getValue();
		currUser.setShowVideo(Boolean.FALSE);
		OfyService.ofy().save().entity(currUser).now();
		Cookie videoCookie = new Cookie("showVideo_" + UserServiceFactory.getUserService().getCurrentUser().getUserId(),
				"false");
		response.addCookie(videoCookie);
		return null;
	}

	/*************************************************************************************************/

	/************************************** Dashboard ***********************************************/

	@RequestMapping(value = "studyDashboard/{studyId}", method = RequestMethod.GET)
	public ModelAndView getStudyDashboard(@PathVariable("studyId") String studyId) {
		Study study = OfyService.ofy().load().type(Study.class).filter("studyID", studyId).first().getValue();

		Map<String, Object> model = new HashMap<String, Object>();
		// ModelAndView returnValues = getAllDashboardsForUser();
		ModelAndView returnValues = getAllStudyDashboardsForUser(study);
		@SuppressWarnings("unchecked")
		List<Dashboard> dashboards = (List<Dashboard>) returnValues.getModel().get("dashboards");
		@SuppressWarnings("unchecked")
		List<Dashboard> defaultDashboards = (List<Dashboard>) returnValues.getModel().get("defaultDashboards");
		study = (Study) returnValues.getModel().get("study");

		model.put("defaultDashboards", defaultDashboards);

		ApplicationUser applicationUser = (ApplicationUser) returnValues.getModel().get("appUser");
		Long userID = applicationUser.getId();

		// Long userId = null;
		boolean admin = isAdmin(userID);
		model.put("admin", admin);
		String userEmail = applicationUser.getEmail();

		// userId = OfyService.ofy().load().type(ApplicationUser.class)
		// .filter("email", userEmail).first().getValue().getId();

		// userId = applicationUser.getId();

		List<List<Widget>> widgets = new ArrayList<List<Widget>>();
		List<List<Widget>> defaultDashboardsWidgets = new ArrayList<List<Widget>>();
		List<List<Widget>> sharedDashboardsWidgets = new ArrayList<List<Widget>>();
		boolean userHasDefault = false;
		Ref<ApplicationUser> appUserRef = Ref.create(Key.create(applicationUser));

		List<Dashboard> sharedDashboards = null;
		if (dashboards != null) {
			for (Dashboard dashboard : dashboards) {
				// dashboard.setUserId(
				// dashboard.getDashboardUserRef().getKey().getId());
				dashboard.setApplicationUsers(appUserRef);
				if (dashboard.getDefaultDashBoard() == true) {
					userHasDefault = true;
				}
				// dashboard.setUserId(
				// dashboard.getDashboardUserRef().getKey().getId());
				// List<Widget> tempWidgets = OfyService.ofy().load()
				// .type(Widget.class)
				// .ancestor(Key.create(
				// Key.create(ApplicationUser.class, userID),
				// Dashboard.class, dashboard.getId()))
				// .list();
				List<Widget> tempWidgets = OfyService.ofy().load().type(Widget.class)
						.filter("dashboard", Key.create(dashboard)).list();
				Collections.sort(tempWidgets);
				widgets.add(tempWidgets);
			}
			// String userEmail = UserServiceFactory.getUserService()
			// .getCurrentUser().getEmail();

			if (dashboards.size() == 0) {
				Dashboard defaultDashboard = saveDefaultDashBoard(appUserRef, study);

				dashboards.add(defaultDashboard);

				study = linkDefaultDashBoardToStudy(defaultDashboard, study);

			}
			Set<Key<Dashboard>> sharedDashboardsKeys = applicationUser.getSharedDashboards();

			sharedDashboards = new ArrayList<Dashboard>(OfyService.ofy().load().keys(sharedDashboardsKeys).values());
		}

		if (defaultDashboards != null) {
			for (Dashboard dashboard : defaultDashboards) {
				// dashboard.setUserId(
				// dashboard.getDashboardUserRef().getKey().getId());
				dashboard.setApplicationUsers(appUserRef);

				List<Widget> tempWidgets = OfyService.ofy().load().type(Widget.class).ancestor(Key.create(dashboard))
						.list();

				Collections.sort(tempWidgets);
				defaultDashboardsWidgets.add(tempWidgets);
			}
		}
		if (sharedDashboards != null) {
			for (Dashboard dashboard : sharedDashboards) {
				// dashboard.setUserId(
				// dashboard.getDashboardUserRef().getKey().getId());
				dashboard.setApplicationUsers(appUserRef);

				List<Widget> tempWidgets = OfyService.ofy().load().type(Widget.class).ancestor(Key.create(dashboard))
						.list();
				sharedDashboardsWidgets.add(tempWidgets);
				Collections.sort(tempWidgets);
			}
		}

		model.put("defaultDashboardsWidgets", defaultDashboardsWidgets);
		model.put("sharedDashboardsWidgets", sharedDashboardsWidgets);
		model.put("dashboards", dashboards);
		model.put("sharedDashboards", sharedDashboards);
		model.put("widgets", widgets);
		model.put("defaultCheck", userHasDefault);
		model.put("study", study);
		model.put("studyId", studyId);
		// System.out.println(appUserId);
		// System.out.println(dashboards.get(0).getId());
		// System.out.println(widgets);
		return new ModelAndView("site/get_dashboard", model);
	}

	private Study linkDefaultDashBoardToStudy(Dashboard defaultDashboard, Study study) {
		Key<Dashboard> dashBoardKey = Key.create(defaultDashboard);
		List<Key<Dashboard>> dashBoardKeys = new ArrayList<>();
		dashBoardKeys.add(dashBoardKey);
		study.setDashboards(dashBoardKeys);
		OfyService.ofy().save().entity(study).now();

		return study;
	}

	private Dashboard saveDefaultDashBoard(Ref<ApplicationUser> appUserRef, Study study) {
		Dashboard defaultDashboard = new Dashboard();
		// defaultDashboard.setDashboardUser(appUser);
		defaultDashboard.setApplicationUsers(appUserRef);
		defaultDashboard.setName("Default Dashboard");
		defaultDashboard.setDefaultDashBoard(true);

		List<Key<Study>> studies = new ArrayList<>();
		Key<Study> studyKey = Key.create(study);
		studies.add(studyKey);
		defaultDashboard.setStudies(studies);

		OfyService.ofy().save().entity(defaultDashboard).now();

		return defaultDashboard;
	}

	@RequestMapping(value = "dashboard/{studyId}", method = RequestMethod.GET)
	public ModelAndView getDashboard(@PathVariable(value = "studyId") String studyId) {
		// String studyId = getUrlParamtersMap(request);

		Map<String, Object> model = new HashMap<String, Object>();
		ModelAndView returnValues = getAllDashboardsForUser(studyId);
		@SuppressWarnings("unchecked")
		List<Dashboard> dashboards = (List<Dashboard>) returnValues.getModel().get("dashboards");
		@SuppressWarnings("unchecked")
		List<Dashboard> defaultDashboards = (List<Dashboard>) returnValues.getModel().get("defaultDashboards");

		model.put("defaultDashboards", defaultDashboards);

		ApplicationUser applicationUser = (ApplicationUser) returnValues.getModel().get("appUser");
		Long userID = applicationUser.getId();

		// Long userId = null;
		boolean admin = isAdmin(userID);
		model.put("admin", admin);
		String userEmail = applicationUser.getEmail();

		// userId = OfyService.ofy().load().type(ApplicationUser.class)
		// .filter("email", userEmail).first().getValue().getId();

		// userId = applicationUser.getId();

		List<List<Widget>> widgets = new ArrayList<List<Widget>>();
		List<List<Widget>> defaultDashboardsWidgets = new ArrayList<List<Widget>>();
		List<List<Widget>> sharedDashboardsWidgets = new ArrayList<List<Widget>>();
		boolean userHasDefault = false;
		Ref<ApplicationUser> appUserRef = Ref.create(Key.create(applicationUser));

		List<Dashboard> sharedDashboards = null;
		if (dashboards != null) {
			for (Dashboard dashboard : dashboards) {
				// dashboard.setUserId(
				// dashboard.getDashboardUserRef().getKey().getId());
				dashboard.setApplicationUsers(appUserRef);
				if (dashboard.getDefaultDashBoard() == true) {
					userHasDefault = true;
				}
				// dashboard.setUserId(
				// dashboard.getDashboardUserRef().getKey().getId());
				// List<Widget> tempWidgets = OfyService.ofy().load()
				// .type(Widget.class)
				// .ancestor(Key.create(
				// Key.create(ApplicationUser.class, userID),
				// Dashboard.class, dashboard.getId()))
				// .list();
				List<Widget> tempWidgets = OfyService.ofy().load().type(Widget.class)
						.filter("dashboard", Key.create(dashboard)).list();
				Collections.sort(tempWidgets);
				widgets.add(tempWidgets);
			}
			// String userEmail = UserServiceFactory.getUserService()
			// .getCurrentUser().getEmail();

			if (dashboards.size() == 0) {

				Dashboard defaultDashboard = new Dashboard();
				// defaultDashboard.setDashboardUser(appUser);
				defaultDashboard.setApplicationUsers(appUserRef);
				defaultDashboard.setName("Default Dashboard");
				defaultDashboard.setDefaultDashBoard(true);
				OfyService.ofy().save().entity(defaultDashboard).now();
				dashboards.add(defaultDashboard);

			}
			Set<Key<Dashboard>> sharedDashboardsKeys = applicationUser.getSharedDashboards();

			sharedDashboards = new ArrayList<Dashboard>(OfyService.ofy().load().keys(sharedDashboardsKeys).values());
		}

		if (defaultDashboards != null) {
			for (Dashboard dashboard : defaultDashboards) {
				// dashboard.setUserId(
				// dashboard.getDashboardUserRef().getKey().getId());
				dashboard.setApplicationUsers(appUserRef);

				List<Widget> tempWidgets = OfyService.ofy().load().type(Widget.class).ancestor(Key.create(dashboard))
						.list();

				Collections.sort(tempWidgets);
				defaultDashboardsWidgets.add(tempWidgets);
			}
		}
		if (sharedDashboards != null) {
			for (Dashboard dashboard : sharedDashboards) {
				// dashboard.setUserId(
				// dashboard.getDashboardUserRef().getKey().getId());
				dashboard.setApplicationUsers(appUserRef);

				List<Widget> tempWidgets = OfyService.ofy().load().type(Widget.class).ancestor(Key.create(dashboard))
						.list();
				sharedDashboardsWidgets.add(tempWidgets);
				Collections.sort(tempWidgets);
			}
		}

		model.put("defaultDashboardsWidgets", defaultDashboardsWidgets);
		model.put("sharedDashboardsWidgets", sharedDashboardsWidgets);
		model.put("dashboards", dashboards);
		model.put("sharedDashboards", sharedDashboards);
		model.put("widgets", widgets);
		model.put("defaultCheck", userHasDefault);
		model.put("studyId", studyId);
		// System.out.println(appUserId);
		// System.out.println(dashboards.get(0).getId());
		// System.out.println(widgets);
		return new ModelAndView("site/get_dashboard", model);
	}

	@RequestMapping(value = "dashboard/addnew", method = RequestMethod.POST)
	public ModelAndView getAddNewDashboard(Dashboard dashboard, HttpServletResponse response) {

		Map<String, Object> model = new HashMap<String, Object>();

		String userEmail = UserServiceFactory.getUserService().getCurrentUser().getEmail().toLowerCase();
		ApplicationUser appUser = null;
		Ref<ApplicationUser> appUserRef = null;
		if (userEmail != null) {
			appUserRef = OfyService.ofy().load().type(ApplicationUser.class).filter("email", userEmail).first();
			if (appUserRef != null) {
				appUser = appUserRef.getValue();
			}
		}
		if (appUser != null) {
			// dashboard.setDashboardUser(appUser);
			dashboard.setApplicationUsers(appUserRef);
			Key<Dashboard> dashboardKey = OfyService.ofy().save().entity(dashboard).now();

			model.put("userId", appUser.getId());
			model.put("dashboardId", dashboardKey.getId());
			model.put("dashboardName", dashboard.getName());
		}
		response.setStatus(HttpServletResponse.SC_ACCEPTED);
		return new ModelAndView("", model);
	}

	@RequestMapping(value = "studyDashBoard/addnew", method = RequestMethod.POST)
	public ModelAndView addNewStudyDashBoard(Dashboard dashboard, @RequestParam("studyId") String studyId,
			HttpServletResponse response) {
		logger.info("Add New DashBoard to Study");

		Map<String, Object> model = new HashMap<String, Object>();

		String userEmail = UserServiceFactory.getUserService().getCurrentUser().getEmail().toLowerCase();
		ApplicationUser appUser = null;
		Ref<ApplicationUser> appUserRef = null;
		if (userEmail != null) {
			appUserRef = OfyService.ofy().load().type(ApplicationUser.class).filter("email", userEmail).first();
			if (appUserRef != null) {
				appUser = appUserRef.getValue();
			}
		}
		if (appUser != null) {
			Study study = OfyService.ofy().load().type(Study.class).filter("studyID", studyId).first().getValue();
			List<Key<Study>> studies = new ArrayList<>();
			Key<Study> studyKey = Key.create(study);
			studies.add(studyKey);

			dashboard.setApplicationUsers(appUserRef);
			dashboard.setStudies(studies);
			Key<Dashboard> dashboardKey = OfyService.ofy().save().entity(dashboard).now();

			List<Key<Dashboard>> dashBoardKeys = study.getDashboards();
			dashBoardKeys.add(dashboardKey);
			study.setDashboards(dashBoardKeys);
			OfyService.ofy().save().entity(study).now();

			model.put("userId", appUser.getId());
			model.put("dashboardId", dashboardKey.getId());
			model.put("dashboardName", dashboard.getName());
			model.put("study", study);
		}
		response.setStatus(HttpServletResponse.SC_ACCEPTED);
		return new ModelAndView("", model);
	}
	// public ModelAndView addNewStudyDashboard(Dashboard dashboard,
	// HttpServletResponse response, Study study) {
	//
	// Map<String, Object> model = new HashMap<String, Object>();
	//
	// String userEmail =
	// UserServiceFactory.getUserService().getCurrentUser().getEmail().toLowerCase();
	// ApplicationUser appUser = null;
	// Ref<ApplicationUser> appUserRef = null;
	// if (userEmail != null) {
	// appUserRef =
	// OfyService.ofy().load().type(ApplicationUser.class).filter("email",
	// userEmail).first();
	// if (appUserRef != null) {
	// appUser = appUserRef.getValue();
	// }
	// }
	// if (appUser != null) {
	// // dashboard.setDashboardUser(appUser);
	// dashboard.setApplicationUsers(appUserRef);
	// Key<Dashboard> dashboardKey =
	// OfyService.ofy().save().entity(dashboard).now();
	//
	// model.put("userId", appUser.getId());
	// model.put("dashboardId", dashboardKey.getId());
	// model.put("dashboardName", dashboard.getName());
	// }
	// response.setStatus(HttpServletResponse.SC_ACCEPTED);
	// return new ModelAndView("", model);
	// }

	/**
	 * 
	 * @param dashboardId
	 * @return the users who have access to the dashboard with the provided id
	 */
	@RequestMapping(value = "dashboard/{dashboardId}/access", method = RequestMethod.GET)
	public ModelAndView getDashboardAccess(@PathVariable("dashboardId") Long dashboardId) {
		String userEmail = UserServiceFactory.getUserService().getCurrentUser().getEmail().toLowerCase();
		ApplicationUser user = OfyService.ofy().load().type(ApplicationUser.class).filter("email", userEmail).first()
				.getValue();
		// Dashboard dashboard = OfyService.ofy().load()
		// .key(Key.create(Key.create(user), Dashboard.class, dashboardId))
		// .getValue();
		Dashboard dashboard = OfyService.ofy().load().type(Dashboard.class).filter("id", dashboardId).first()
				.getValue();
		Set<String> usersSharedWith = dashboard.getUsersSharedWith();

		Map<String, Object> model = new HashMap<String, Object>();

		List<String> usersSharedWithList = new ArrayList<String>(usersSharedWith);
		Collections.sort(usersSharedWithList);
		model.put("users", usersSharedWithList);

		return new ModelAndView("", model);
	}

	@RequestMapping(value = "studyDashboard/{dashboardId}/access", method = RequestMethod.GET)
	public ModelAndView getStudyDashboardAccess(@PathVariable("dashboardId") Long dashboardId) {
		logger.info("getStudyDashboardAccess");
		String userEmail = UserServiceFactory.getUserService().getCurrentUser().getEmail().toLowerCase();
		ApplicationUser user = OfyService.ofy().load().type(ApplicationUser.class).filter("email", userEmail).first()
				.getValue();
		// Dashboard dashboard = OfyService.ofy().load()
		// .key(Key.create(Key.create(user), Dashboard.class, dashboardId))
		// .getValue();
		Dashboard dashboard = OfyService.ofy().load().type(Dashboard.class).filter("id", dashboardId).first()
				.getValue();
		Set<String> usersSharedWith = dashboard.getUsersSharedWith();

		Map<String, Object> model = new HashMap<String, Object>();

		List<String> usersSharedWithList = new ArrayList<String>(usersSharedWith);
		Collections.sort(usersSharedWithList);
		model.put("users", usersSharedWithList);

		return new ModelAndView("", model);
	}

	/**
	 * remove access to a shared dashboard from the user and the dashboard
	 * 
	 * @param dashboardId
	 * @param email
	 * @param userId
	 * @return
	 */
	@RequestMapping(value = "dashboard/{dashboardId}/remove", method = RequestMethod.POST)
	public ModelAndView removeDashboardAccess(@PathVariable("dashboardId") Long dashboardId,
			@RequestParam("email") String email, @RequestParam("uid") Long userId) {

		ApplicationUser user = OfyService.ofy().load().key(Key.create(ApplicationUser.class, userId)).getValue();

		if (user != null) {
			Dashboard dashboard = OfyService.ofy().load()
					.key(Key.create(Key.create(user), Dashboard.class, dashboardId)).getValue();

			Set<String> usersSharedWith = dashboard.getUsersSharedWith();

			usersSharedWith.remove(email);

			OfyService.ofy().save().entity(dashboard).now();
		}

		ApplicationUser userSharedWith = OfyService.ofy().load().type(ApplicationUser.class).filter("email", email)
				.first().getValue();

		if (userSharedWith != null) {
			userSharedWith.getSharedDashboards().remove(Key.create(Key.create(user), Dashboard.class, dashboardId));

			OfyService.ofy().save().entity(userSharedWith).now();
		}

		Map<String, Object> model = new HashMap<String, Object>();
		return new ModelAndView("", model);
	}

	@RequestMapping(value = "dashboard/setDefaultDashBoard/{dashboardId}", method = RequestMethod.POST)
	public void setDefaultDashBoard(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("dashboardId") Long dashboardId) {
		String userEmail = UserServiceFactory.getUserService().getCurrentUser().getEmail();
		ApplicationUser appUser = null;
		Ref<ApplicationUser> appUserRef = null;
		if (userEmail != null) {
			appUserRef = OfyService.ofy().load().type(ApplicationUser.class).filter("email", userEmail.toLowerCase())
					.first();
			if (appUserRef != null) {
				appUser = appUserRef.getValue();
			}
		}
		if (appUser != null && dashboardId != null) {
			// Dashboard dashboard = OfyService.ofy().load()
			// .key(Key.create(Key.create(appUser), Dashboard.class,
			// dashboardId)).getValue();
			Dashboard dashboard = OfyService.ofy().load().type(Dashboard.class).filter("id", dashboardId).first()
					.getValue();
			// Dashboard dashboardNotDefault =
			// OfyService.ofy().load().type(Dashboard.class).ancestor(Key.create(appUser))
			// .filter("defaultDashBoard", Boolean.TRUE).first().getValue();
			Dashboard deafultDashBoard = OfyService.ofy().load().type(Dashboard.class).filter("defaultDashBoard", true)
					.filter("applicationUsers", appUserRef).first().getValue();
			if (deafultDashBoard != null) {
				deafultDashBoard.setDefaultDashBoard(Boolean.FALSE);
				// System.out.println(dashboardNotDefault.getDefaultDashBoard()
				// + " " + dashboardNotDefault.getName());
				OfyService.ofy().save().entity(deafultDashBoard).now();
			}
			dashboard.setDefaultDashBoard(Boolean.TRUE);
			OfyService.ofy().save().entity(dashboard).now();
		}
		response.setStatus(HttpServletResponse.SC_ACCEPTED);
	}

	@RequestMapping(value = "dashboard/edit", method = RequestMethod.POST)
	public void editDashboard(HttpServletRequest request, HttpServletResponse response) {
		String userEmail = UserServiceFactory.getUserService().getCurrentUser().getEmail();
		ApplicationUser appUser = null;

		String name = "";
		Long id = null;
		Long userId = null;

		try {
			name = request.getParameter("name");
			id = Long.parseLong(request.getParameter("dashboard-hidden-id"));
			userId = Long.parseLong(request.getParameter("hidden-user-id"));
		} catch (Exception e) {

		}

		if (userEmail != null) {
			Ref<ApplicationUser> result = OfyService.ofy().load().type(ApplicationUser.class)
					.filter("email", userEmail.toLowerCase()).first();
			if (result != null) {
				appUser = result.getValue();
			}
		}
		if (appUser != null && id != null) {
			if (!appUser.isOnlyUser() && userId != null) {
				Dashboard dashboard = OfyService.ofy().load()
						.key(Key.create(Key.create(ApplicationUser.class, userId), Dashboard.class, id)).getValue();
				dashboard.setName(name);
				OfyService.ofy().save().entity(dashboard).now();
			} else {
				Dashboard dashboard = OfyService.ofy().load().key(Key.create(Key.create(appUser), Dashboard.class, id))
						.getValue();
				dashboard.setName(name);
				OfyService.ofy().save().entity(dashboard).now();
			}
		}
		response.setStatus(HttpServletResponse.SC_ACCEPTED);
	}

	@RequestMapping(value = "dashboard/widgetedit", method = RequestMethod.POST)
	public void editWidget(HttpServletRequest request, HttpServletResponse response) {
		String userEmail = UserServiceFactory.getUserService().getCurrentUser().getEmail();
		ApplicationUser appUser = null;

		String name = request.getParameter("name");
		Long id = null;
		Long widgetId = null;
		Long userId = null;
		try {
			id = Long.parseLong(request.getParameter("dashboard-hidden-id"));
			widgetId = Long.parseLong(request.getParameter("widget-hidden-id"));
			userId = Long.parseLong(request.getParameter("hidden-user-id"));

		} catch (Exception e) {

		}

		if (userEmail != null) {
			Ref<ApplicationUser> result = OfyService.ofy().load().type(ApplicationUser.class)
					.filter("email", userEmail.toLowerCase()).first();
			if (result != null) {
				appUser = result.getValue();
			}
		}
		if (appUser != null && id != null && widgetId != null) {
			if (!appUser.isOnlyUser() && userId != null) {
				Key<Dashboard> dashboardKey = Key.create(Key.create(ApplicationUser.class, userId), Dashboard.class,
						id);
				Key<Widget> widgetKey = Key.create(dashboardKey, Widget.class, widgetId);
				Widget widget = OfyService.ofy().load().key(widgetKey).getValue();
				widget.setTitle(name);
				OfyService.ofy().save().entity(widget).now();
			} else {

				Key<Dashboard> dashboardKey = Key.create(Key.create(appUser), Dashboard.class, id);
				Key<Widget> widgetKey = Key.create(dashboardKey, Widget.class, widgetId);
				Widget widget = OfyService.ofy().load().key(widgetKey).getValue();
				widget.setTitle(name);
				OfyService.ofy().save().entity(widget).now();
			}
		}
		response.setStatus(HttpServletResponse.SC_ACCEPTED);
	}

	@RequestMapping(value = "dashboard/deletewidget/{dashboardId}/{id}", method = RequestMethod.POST)
	public void deleteWidget(@PathVariable("dashboardId") Long dashboardId, @PathVariable("id") Long id,
			HttpServletResponse response, @RequestParam(value = "userId", required = false) Long userId) {
		String userEmail = UserServiceFactory.getUserService().getCurrentUser().getEmail();
		ApplicationUser appUser = null;

		if (userEmail != null) {
			Ref<ApplicationUser> result = OfyService.ofy().load().type(ApplicationUser.class)
					.filter("email", userEmail.toLowerCase()).first();
			if (result != null) {
				appUser = result.getValue();
			}
		}
		if (appUser != null && dashboardId != null && id != null) {
			if (userId != null && !appUser.isOnlyUser()) {

				Key<ApplicationUser> userKey = Key.create(ApplicationUser.class, userId);
				Key<Dashboard> dashboard = Key.create(userKey, Dashboard.class, dashboardId);
				Key<Widget> widgetKey = Key.create(dashboard, Widget.class, id);

				OfyService.ofy().delete().key(widgetKey).now();

			} else {
				Key<ApplicationUser> userKey = Key.create(appUser);
				Key<Dashboard> dashboard = Key.create(userKey, Dashboard.class, dashboardId);
				Key<Widget> widgetKey = Key.create(dashboard, Widget.class, id);

				OfyService.ofy().delete().key(widgetKey).now();
			}
		}
		response.setStatus(HttpServletResponse.SC_ACCEPTED);
	}

	@RequestMapping(value = "studyDashBoard/delete/{dashboardId}", method = RequestMethod.POST)
	public void deleteStudyDashBoard(@PathVariable("dashboardId") Long dashboardId,
			@RequestParam(value = "userId", required = false) Long userId,
			@RequestParam(value = "shared", defaultValue = "false", required = false) boolean isSharedDashboard,
			HttpServletResponse response) {

		logger.info("Delete Study DashBoard");

		Dashboard dashBoardToDelete = OfyService.ofy().load().type(Dashboard.class).filter("id", dashboardId).first()
				.getValue();
		String currentAdmin = UserServiceFactory.getUserService().getCurrentUser().getEmail();

		ApplicationUser applicationUser = OfyService.ofy().load().type(ApplicationUser.class)
				.filter("email", currentAdmin).first().getValue();
		if (applicationUser.equals(dashBoardToDelete.getApplicationUsers())) {
			if (dashBoardToDelete != null) {
				deleteStudyDashBoardRef(dashBoardToDelete);
				deleteStudyDashBoardWidget(dashBoardToDelete);
				deleteAppUserRef(dashBoardToDelete);
				deleteDashBoard(dashBoardToDelete);

			}
		} else {
			deleteAppUserRef(dashBoardToDelete);
			Set<String> usersSharedWith = dashBoardToDelete.getUsersSharedWith();
			if (usersSharedWith != null && !usersSharedWith.isEmpty() && usersSharedWith.contains(currentAdmin)) {
				usersSharedWith.remove(currentAdmin);
				OfyService.ofy().save().entity(dashBoardToDelete).now();
			}
		}

	}

	public void deleteStudyDashBoardRef(Dashboard dashboard) {
		logger.info("deleteStudyDashBoardRef");

		Key<Dashboard> dashboardKey = Key.create(dashboard);
		List<Key<Study>> studies = dashboard.getStudies();

		for (int i = 0; i < studies.size(); i++) {
			Long studyId = studies.get(i).getId();
			Study study = OfyService.ofy().load().type(Study.class).filter("id", studyId).first().getValue();

			if (study != null) {
				List<Key<Dashboard>> dashBoardsList = study.getDashboards();
				if (dashBoardsList.contains(dashboardKey)) {
					dashBoardsList.remove(dashboardKey);
					study.setDashboards(dashBoardsList);
					OfyService.ofy().save().entity(study).now();
				}
			}

			System.out.println();
		}

	}

	public void deleteStudyDashBoardWidget(Dashboard dashboard) {
		logger.info("deleteStudyDashBoardWidget");

		Widget widget = OfyService.ofy().load().type(Widget.class).filter("dashboard", Ref.create(dashboard)).first()
				.getValue();
		if (widget != null) {
			OfyService.ofy().delete().entity(widget).now();
		}

	}

	public void deleteAppUserRef(Dashboard dashboard) {
		logger.info("deleteLinkWithAppUsers");

		Set<Key<Dashboard>> sharedDashboards = new HashSet<Key<Dashboard>>();
		Key<Dashboard> create = Key.create(dashboard);
		sharedDashboards.add(create);

		List<ApplicationUser> applicationUsers = OfyService.ofy().load().type(ApplicationUser.class)
				.filter("sharedDashboards", create).list();

		for (ApplicationUser user : applicationUsers) {
			Set<Key<Dashboard>> userSharedDashboards = user.getSharedDashboards();
			userSharedDashboards.remove(Key.create(dashboard));
			user.setSharedDashboards(userSharedDashboards);
			OfyService.ofy().save().entity(user).now();
		}

	}

	public void deleteDashBoard(Dashboard dashboard) {
		logger.info("deleteDashBoard");
		OfyService.ofy().delete().entity(dashboard).now();

	}

	@RequestMapping(value = "dashboard/delete/{dashboardId}", method = RequestMethod.POST)
	public void deleteWidget(@PathVariable("dashboardId") Long dashboardId,
			@RequestParam(value = "userId", required = false) Long userId,
			@RequestParam(value = "shared", defaultValue = "false", required = false) boolean isSharedDashboard,
			HttpServletResponse response) {
		String userEmail = UserServiceFactory.getUserService().getCurrentUser().getEmail();
		ApplicationUser appUser = null;

		if (userEmail != null) {
			Ref<ApplicationUser> result = OfyService.ofy().load().type(ApplicationUser.class)
					.filter("email", userEmail.toLowerCase()).first();
			if (result != null) {
				appUser = result.getValue();
			}
		}

		if (appUser != null && isSharedDashboard) {

			Key<ApplicationUser> userKey = Key.create(ApplicationUser.class, userId);
			Key<Dashboard> dashboardKey = Key.create(userKey, Dashboard.class, dashboardId);
			appUser.getSharedDashboards().remove(dashboardKey);

			Dashboard dashboard = OfyService.ofy().load().key(dashboardKey).getValue();

			dashboard.getUsersSharedWith().remove(userEmail.toLowerCase());

			OfyService.ofy().save().entity(appUser).now();
			OfyService.ofy().save().entity(dashboard).now();

		} else {
			if (appUser != null && dashboardId != null) {

				if (userId != null && !appUser.isOnlyUser()) {
					Key<ApplicationUser> userKey = Key.create(ApplicationUser.class, userId);
					Key<Dashboard> dashboardKey = Key.create(userKey, Dashboard.class, dashboardId);
					//
					Dashboard dashboard = OfyService.ofy().load().key(dashboardKey).getValue();

					Set<String> usersEmails = dashboard.getUsersSharedWith();

					if (!usersEmails.isEmpty()) {

						List<ApplicationUser> applicationUsers = OfyService.ofy().load().type(ApplicationUser.class)
								.filter("email in", usersEmails).list();

						for (ApplicationUser applicationUser : applicationUsers) {
							applicationUser.getSharedDashboards().remove(dashboardKey);
						}

						if (!applicationUsers.isEmpty()) {
							OfyService.ofy().save().entities(applicationUsers).now();
						}
					}
					//

					OfyService.ofy().delete().key(dashboardKey).now();

				} else {
					Key<ApplicationUser> userKey = Key.create(appUser);
					Key<Dashboard> dashboardKey = Key.create(userKey, Dashboard.class, dashboardId);

					// Dashboard dashboard =
					// OfyService.ofy().load().key(dashboardKey).getValue();
					Dashboard dashboard = OfyService.ofy().load().type(Dashboard.class).filter("id", dashboardId)
							.filter("applicationUsers", userKey).first().getValue();

					Set<String> usersEmails = dashboard.getUsersSharedWith();

					if (!usersEmails.isEmpty()) {

						List<ApplicationUser> applicationUsers = OfyService.ofy().load().type(ApplicationUser.class)
								.filter("email in", usersEmails).list();

						for (ApplicationUser applicationUser : applicationUsers) {
							applicationUser.getSharedDashboards().remove(dashboardKey);
						}

						OfyService.ofy().delete().key(dashboardKey).now();
						if (!applicationUsers.isEmpty()) {
							OfyService.ofy().save().entities(applicationUsers).now();
						}
					}
				}
			}
		}
		response.setStatus(HttpServletResponse.SC_ACCEPTED);
	}

	@RequestMapping(value = "dashboard/addnewwidget", method = RequestMethod.POST)
	public ModelAndView addNewWidget(@RequestParam("newDashboardName") String newDashboardName, Widget widget,
			HttpServletRequest request) {
		Map<String, Object> model = new HashMap<String, Object>();
		String userEmail = UserServiceFactory.getUserService().getCurrentUser().getEmail();
		ApplicationUser appUser = null;
		if (userEmail != null) {
			Ref<ApplicationUser> result = OfyService.ofy().load().type(ApplicationUser.class)
					.filter("email", userEmail.toLowerCase()).first();
			if (result != null) {
				appUser = result.getValue();
			}
		}
		if (appUser != null) {

			if (newDashboardName != null && !newDashboardName.trim().isEmpty()) {
				Dashboard newDashboard = new Dashboard();
				newDashboard.setName(newDashboardName);
				// newDashboard.setDashboardUser(appUser);

				Key<Dashboard> dashboard = OfyService.ofy().save().entity(newDashboard).now();

				// widget.setDashboard(Ref.create(dashboard));
				widget.setLastModificationDate(new Date());

			} else {
				String rawDashboardId = request.getParameter("dashboard_id");

				Long dashboardId = null;
				Long userId = null;

				if (rawDashboardId != null && rawDashboardId.contains("_")) {
					String[] dashAndUser = rawDashboardId.split("_");
					dashboardId = new Long(dashAndUser[0]);
					userId = new Long(dashAndUser[1]);
				} else {
					dashboardId = new Long(rawDashboardId);
				}

				if (!appUser.isOnlyUser() && userId != null) {
					// Dashboard dashboard = OfyService.ofy().load()
					// .key(Key.create(
					// Key.create(ApplicationUser.class, userId),
					// Dashboard.class, dashboardId))
					// .getValue();
					Dashboard dashboard = OfyService.ofy().load().type(Dashboard.class).filter("id", dashboardId)
							.first().getValue();
					// widget.setDashboard(dashboard);
					Ref<Dashboard> dashboardRef = Ref.create(Key.create(dashboard));
					// widget.setDashboard(dashboardRef);

					widget.setLastModificationDate(new Date());
				} else {
					// Dashboard dashboard = OfyService.ofy().load()
					// .key(Key.create(Key.create(appUser),
					// Dashboard.class, dashboardId))
					// .getValue();
					Dashboard dashboard = OfyService.ofy().load().type(Dashboard.class).filter("id", dashboardId)
							.first().getValue();
					// widget.setDashboard(dashboard);
					Ref<Dashboard> dashboardRef = Ref.create(Key.create(dashboard));
					// widget.setDashboard(dashboardRef);
					widget.setLastModificationDate(new Date());
				}
			}
		}
		OfyService.ofy().save().entity(widget).now();
		return new ModelAndView("", model);
	}

	@RequestMapping(value = "dashboard/getall/{studyId}", method = RequestMethod.GET)
	public ModelAndView getAllDashboardsForUser(@PathVariable(value = "studyId") String studyId) {

		Map<String, Object> model = new HashMap<String, Object>();
		String userEmail = UserServiceFactory.getUserService().getCurrentUser().getEmail();
		ApplicationUser appUser = null;
		List<Dashboard> dashboards = null;
		Ref<ApplicationUser> applicationUserRef = null;
		if (userEmail != null) {
			applicationUserRef = OfyService.ofy().load().type(ApplicationUser.class)
					.filter("email", userEmail.toLowerCase()).first();
			if (applicationUserRef != null) {
				appUser = applicationUserRef.getValue();
			}
		}
		if (appUser != null) {
			List<Dashboard> defaultDashboards = new ArrayList<Dashboard>();
			List<Dashboard> tempDashboards = new ArrayList<Dashboard>();

			// dashboards = OfyService.ofy().load().type(Dashboard.class)
			// .ancestor(appUser).list();
			Key<Study> studyKey = OfyService.ofy().load().type(Study.class).filter("studyID", studyId).first().getKey();

			dashboards = OfyService.ofy().load().type(Dashboard.class).filter("applicationUsers", applicationUserRef)
					.filter("studies", studyKey).list();

			if (!appUser.isOnlyUser()) {
				defaultDashboards = getDefaultDashboards();
				for (Dashboard dashboard : dashboards) {
					boolean found = false;
					for (Dashboard defaultDashboard : defaultDashboards) {
						if (defaultDashboard.getId().equals(dashboard.getId())) {
							found = true;
						}
					}
					if (!found) {
						tempDashboards.add(dashboard);
					}
				}

				if (tempDashboards.isEmpty()) {

					Dashboard defaultDashboard = new Dashboard();
					// defaultDashboard.setDashboardUser(appUser);
					defaultDashboard.setApplicationUsers(applicationUserRef);
					defaultDashboard.setName("Default Dashboard");
					defaultDashboard.setDefaultDashBoard(true);
					OfyService.ofy().save().entity(defaultDashboard).now();
					tempDashboards.add(defaultDashboard);
				}
				dashboards = tempDashboards;
			}
			model.put("defaultDashboards", defaultDashboards);
		}
		model.put("appUser", appUser);
		model.put("dashboards", dashboards);
		return new ModelAndView("", model);
	}

	@RequestMapping(value = "studyDashboard/getall", method = RequestMethod.GET)
	public ModelAndView getAllStudyDashboardsForUser(Study study) {
		Map<String, Object> model = new HashMap<String, Object>();
		String userEmail = UserServiceFactory.getUserService().getCurrentUser().getEmail();
		ApplicationUser appUser = null;
		List<Dashboard> dashboards = null;
		Ref<ApplicationUser> applicationUserRef = null;
		if (userEmail != null) {
			applicationUserRef = OfyService.ofy().load().type(ApplicationUser.class)
					.filter("email", userEmail.toLowerCase()).first();
			if (applicationUserRef != null) {
				appUser = applicationUserRef.getValue();
			}
		}
		if (appUser != null) {
			List<Dashboard> defaultDashboards = new ArrayList<Dashboard>();
			List<Dashboard> tempDashboards = new ArrayList<Dashboard>();

			// dashboards = OfyService.ofy().load().type(Dashboard.class)
			// .ancestor(appUser).list();
			dashboards = OfyService.ofy().load().type(Dashboard.class).filter("applicationUsers", applicationUserRef)
					.filter("studies", study.getStudyID()).list();

			if (!appUser.isOnlyUser()) {
				defaultDashboards = getDefaultDashboards();
				for (Dashboard dashboard : dashboards) {
					boolean found = false;
					for (Dashboard defaultDashboard : defaultDashboards) {
						if (defaultDashboard.getId().equals(dashboard.getId())) {
							found = true;
						}
					}
					if (!found) {
						tempDashboards.add(dashboard);
					}
				}

				if (tempDashboards.isEmpty()) {

					Dashboard defaultDashboard = saveDefaultDashBoard(applicationUserRef, study);
					tempDashboards.add(defaultDashboard);
					study = linkDefaultDashBoardToStudy(defaultDashboard, study);
				}
				dashboards = tempDashboards;
			}
			model.put("defaultDashboards", defaultDashboards);
		}
		model.put("appUser", appUser);
		model.put("dashboards", dashboards);
		model.put("study", study);
		return new ModelAndView("", model);
	}

	@RequestMapping(value = "dashboard/checkforupdate", method = RequestMethod.GET)
	public ModelAndView checkForWidgetUpdate(@RequestParam(value = "widgetid", required = true) Long widgetId,
			@RequestParam(value = "dashboardid", required = true) Long dashboardId,
			@RequestParam(value = "qs", required = false) List<Long> questionsIdsParam,
			@RequestParam(value = "w", required = false) Long waveIdParam,
			@RequestParam(value = "b", required = false) List<Long> brandsIdsParam,
			@RequestParam(value = "fd", required = false) List<String> filtersDivParam,
			@RequestParam(value = "ag", required = false) List<String> answerGroupsByQuestionList,
			@RequestParam(value = "ws", required = false) Long secondWaveIdParam,
			@RequestParam(value = "l", required = false) List<Long> locationIdParam,
			@RequestParam(value = "s_or_t", required = false) String snapshotOrTrend,
			@RequestParam(value = "userId", required = false) Long userId,
			@RequestParam(value = "stacked", defaultValue = "true", required = false) boolean stacked,
			HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> model = new HashMap<String, Object>();
		Map<String, Map<String, Object>> mapOfModels = null;
		List<Date> lastModificationDates = new ArrayList<Date>();
		String userEmail = UserServiceFactory.getUserService().getCurrentUser().getEmail();
		ApplicationUser appUser = null;
		if (userEmail != null) {
			Ref<ApplicationUser> result = OfyService.ofy().load().type(ApplicationUser.class)
					.filter("email", userEmail.toLowerCase()).first();
			if (result != null) {
				appUser = result.getValue();
			}
		}
		if (appUser != null) {
			Widget widget = null;

			if (userId != null && !appUser.isOnlyUser()) {
				Key<ApplicationUser> userKey = Key.create(ApplicationUser.class, userId);
				Key<Dashboard> dashboardKey = Key.create(userKey, Dashboard.class, dashboardId);
				widget = OfyService.ofy().load().key(Key.create(dashboardKey, Widget.class, widgetId)).getValue();
			} else {
				widget = OfyService.ofy().load().key(Key
						.create(Key.create(Key.create(appUser), Dashboard.class, dashboardId), Widget.class, widgetId))
						.getValue();
			}
			List<DataSet> candidateDataSets = OfyService.ofy().load().type(DataSet.class)
					.filter("published", Boolean.TRUE)
					.filter("lastModificationDate >=", widget.getLastModificationDate()).list();

			mapOfModels = new LinkedHashMap<String, Map<String, Object>>();
			if (snapshotOrTrend.equals("snapshot")) {
				for (DataSet dataSet : candidateDataSets) {
					if (!dataSet.getId().equals(waveIdParam)) {
						// System.out.println("candidate");
						try {
							ModelAndView queryResult = query(questionsIdsParam, dataSet.getId(), locationIdParam,
									brandsIdsParam, filtersDivParam, true, answerGroupsByQuestionList, stacked, request,
									response);

							@SuppressWarnings("unchecked")
							List<List<Double>> list = (ArrayList<List<Double>>) queryResult.getModel().get("values");

							String url = recreateURL(questionsIdsParam, dataSet.getId(), null, brandsIdsParam,
									filtersDivParam, answerGroupsByQuestionList, locationIdParam, stacked);

							boolean hasValues = false;
							for (List<Double> values : list) {
								for (Double double1 : values) {
									if (!double1.equals(new Double(-1000))) {
										hasValues = true;
									}
								}
							}
							if (!list.isEmpty() && !hasValues == false) {
								mapOfModels.put(url, queryResult.getModel());
								lastModificationDates.add(dataSet.getLastModificationDate());
							}

						} catch (Exception e) {
							// System.out.println("error");
							e.printStackTrace();
						}
					}
				}
			} else {

				List<DataSet> dataSets = OfyService.ofy().load().type(DataSet.class).list();

				for (DataSet dataSet : candidateDataSets) {
					if (!dataSet.getId().equals(secondWaveIdParam)) {
						try {
							List<DataSet> dataSetsInRange = getDataSetsIntervalsForUpdate(waveIdParam, dataSet.getId(),
									dataSets);
							Long urlFirstWaveIdParam = waveIdParam;

							if (dataSetsInRange.size() > 10) {
								int start = dataSetsInRange.size() - 10;
								urlFirstWaveIdParam = dataSetsInRange.get(start).getId();
							}

							ModelAndView tendQueryResult = trendQuery(questionsIdsParam, urlFirstWaveIdParam,
									dataSet.getId(), locationIdParam, brandsIdsParam, filtersDivParam, true,
									answerGroupsByQuestionList, response);

							@SuppressWarnings("unchecked")
							List<List<Double>> list = (ArrayList<List<Double>>) tendQueryResult.getModel()
									.get("values");

							String url = recreateURL(questionsIdsParam, urlFirstWaveIdParam, dataSet.getId(),
									brandsIdsParam, filtersDivParam, answerGroupsByQuestionList, locationIdParam,
									stacked);

							if (!list.isEmpty()) {
								mapOfModels.put(url, tendQueryResult.getModel());
								lastModificationDates.add(dataSet.getLastModificationDate());
							}
						} catch (Exception e) {

						}
					}
				}
			}
		}
		response.setStatus(HttpServletResponse.SC_ACCEPTED);
		model.put("lastModificationDates", lastModificationDates);
		model.put("options", mapOfModels);
		return new ModelAndView("", model);
	}

	/**
	 * @param questionsIdsParam
	 * @param waveIdParam
	 * @param brandsIdsParam
	 * @param filtersDivParam
	 * @param answerGroupsByQuestionList
	 * @param locationIdParam
	 * @return
	 */
	private String recreateURL(List<Long> questionsIdsParam, Long waveIdParam, Long secondWaveIdParam,
			List<Long> brandsIdsParam, List<String> filtersDivParam, List<String> answerGroupsByQuestionList,
			List<Long> locationIdParam, boolean stacked) {
		String questions = "";

		int counter = 0;
		for (Long questionId : questionsIdsParam) {
			questions += "qs=" + questionId + "&";
			counter++;
		}

		String waves = "w=" + waveIdParam + "&";

		if (secondWaveIdParam != null) {
			waves += "w=" + waveIdParam + "&";
		}

		String filtersAndDivisions = "";
		counter = 0;
		for (String waveId : filtersDivParam) {
			filtersAndDivisions += "fd=" + waveId + "&";
			counter++;
		}

		String brands = "";
		counter = 0;
		for (Long brandId : brandsIdsParam) {
			brands += "b=" + brandId + "&";
			counter++;
		}

		String locations = "";
		counter = 0;
		for (Long locationId : locationIdParam) {
			locations += "l=" + locationId + "&";
			counter++;
		}

		String answerGroups = "";
		counter = 0;
		if (answerGroupsByQuestionList != null) {
			for (String answerGrp : answerGroupsByQuestionList) {
				answerGroups += "ag=" + answerGrp + "&";
				counter++;
			}
		}

		String url = questions + waves + brands + locations + answerGroups + filtersAndDivisions;
		// url = url.substring(0, url.length() - 1);
		url += "stacked=" + stacked;
		return url;
	}

	@RequestMapping(value = "dashboard/updatewidget", method = RequestMethod.POST)
	public void updateWidget(HttpServletRequest request, HttpServletResponse response) {
		String userEmail = UserServiceFactory.getUserService().getCurrentUser().getEmail();
		ApplicationUser appUser = null;

		String url = request.getParameter("selected-option");
		Long id = null;
		Long widgetId = null;
		try {
			id = Long.parseLong(request.getParameter("dashboard-hidden-id"));
			widgetId = Long.parseLong(request.getParameter("widget-hidden-id"));
		} catch (Exception e) {

		}

		if (userEmail != null) {
			Ref<ApplicationUser> result = OfyService.ofy().load().type(ApplicationUser.class)
					.filter("email", userEmail.toLowerCase()).first();
			if (result != null) {
				appUser = result.getValue();
			}
		}
		if (appUser != null && id != null && widgetId != null) {
			Key<Dashboard> dashboardKey = Key.create(Key.create(appUser), Dashboard.class, id);
			Key<Widget> widgetKey = Key.create(dashboardKey, Widget.class, widgetId);
			Widget widget = OfyService.ofy().load().key(widgetKey).getValue();
			widget.setContentURL(url);
			widget.setLastModificationDate(new Date());
			OfyService.ofy().save().entity(widget).now();
		}
		response.setStatus(HttpServletResponse.SC_ACCEPTED);
	}

	@RequestMapping(value = "dashboard/sharedashboard", method = RequestMethod.POST)
	public void shareDashboard(@RequestParam("emails") String emails,
			@RequestParam("dashboard-hidden-id") Long dashboardId, HttpServletResponse response) {
		String[] emailsSplit = emails.split(",");

		List<String> emailsSplitList = new ArrayList<String>();

		for (int i = 0; i < emailsSplit.length; i++) {

			emailsSplit[i] = emailsSplit[i].toLowerCase().trim();

			if (!emailsSplit[i].isEmpty()) {
				emailsSplitList.add(emailsSplit[i]);
			}

		}

		if (!emailsSplitList.isEmpty()) {
			// List<String> emailsSplit = Arrays.asList(emailsSplitArray);

			Key<ApplicationUser> user = OfyService.ofy().load().type(ApplicationUser.class)
					.filter("email", UserServiceFactory.getUserService().getCurrentUser().getEmail().toLowerCase())
					.first().getKey();

			// Dashboard dashboard = OfyService.ofy().load()
			// .key(Key.create(user, Dashboard.class, dashboardId))
			// .getValue();
			Dashboard dashboard = OfyService.ofy().load().type(Dashboard.class).filter("id", dashboardId).first()
					.getValue();

			List<ApplicationUser> users = new ArrayList<ApplicationUser>();

			users = OfyService.ofy().load().type(ApplicationUser.class).filter("email in", emailsSplitList).list();

			if (dashboard != null) {

				for (ApplicationUser applicationUser : users) {
					applicationUser.getSharedDashboards().add(Key.create(dashboard));
					dashboard.getUsersSharedWith().add(applicationUser.getEmail());
				}
				OfyService.ofy().save().entity(dashboard).now();

				OfyService.ofy().save().entities(users).now();
			}

		}
		response.setStatus(HttpServletResponse.SC_ACCEPTED);
	}

	@RequestMapping("dashboard/getlocations")
	public ModelAndView getLocations() {
		// get the locations from the datasheets
		// Map<String, Object> model = new HashMap<String, Object>();
		// Query<Location> query = OfyService.ofy().load().type(Location.class)
		// .filter("isRegion", Boolean.FALSE);
		// query = query.order("name");
		// List<Location> resources = query.list();

		List<Location> publishedLocations = new ArrayList<Location>();
		List<DataSet> AllDataSets = OfyService.ofy().load().type(DataSet.class).filter("published", Boolean.TRUE)
				.list();
		Set<Key<Location>> locationSet = new HashSet<Key<Location>>();
		Map<String, Object> model = new HashMap<String, Object>();
		for (DataSet dataSet : AllDataSets) {
			for (Key<Location> location : dataSet.getLocations()) {
				locationSet.add(location);
			}
		}
		publishedLocations = new ArrayList<Location>(OfyService.ofy().load().keys(locationSet).values());

		for (Iterator<Location> iterator = publishedLocations.iterator(); iterator.hasNext();) {

			Location location = (Location) iterator.next();
			if (location.getIsRegion()) {
				publishedLocations.remove(location);
			}

		}

		Collections.sort(publishedLocations);

		model.put("locations", publishedLocations);
		return new ModelAndView("", model);
	}

	public List<DataSet> getDataSetsIntervalsForUpdate(Long waveIdParam, Long secondWaveIdParam,
			List<DataSet> dataSets) {

		Date startDate = null;
		Date endDate = null;
		Collections.sort(dataSets);

		for (DataSet dataSet : dataSets) {
			if (dataSet.getId().compareTo(waveIdParam) == 0) {
				startDate = dataSet.getWaveDate();
			}
			if (dataSet.getId().compareTo(secondWaveIdParam) == 0) {
				endDate = dataSet.getWaveDate();
			}
		}

		List<DataSet> dataSetsInRange = new ArrayList<DataSet>();

		for (DataSet dataSet : dataSets) {
			if (dataSet.getWaveDate() != null && dataSet.isPublished()) {
				if (dataSet.getWaveDate().compareTo(startDate) > -1 && dataSet.getWaveDate().compareTo(endDate) < 1) {
					dataSetsInRange.add(dataSet);

				}
			}
		}
		return dataSetsInRange;
	}

	public List<Dashboard> getDefaultDashboards() {

		List<DefaultDashBoard> defaultDashboardsList = OfyService.ofy().load().type(DefaultDashBoard.class).list();
		List<Key<Dashboard>> dashboardsKeys = new ArrayList<Key<Dashboard>>();
		List<Key<Location>> locationsKeys = new ArrayList<Key<Location>>();

		for (DefaultDashBoard defaultDashBoard : defaultDashboardsList) {
			Long locationId = defaultDashBoard.getId();
			locationsKeys.add(Key.create(Location.class, locationId));
			Key<Dashboard> dashboardKey = defaultDashBoard.getDashboard();
			dashboardsKeys.add(dashboardKey);
		}
		Map<Key<Dashboard>, Dashboard> dashboards = OfyService.ofy().load().keys(dashboardsKeys);

		Map<Key<Location>, Location> locations = OfyService.ofy().load().keys(locationsKeys);

		for (DefaultDashBoard defaultDashBoard : defaultDashboardsList) {
			Long locationId = defaultDashBoard.getId();
			defaultDashBoard.setLocationObject(locations.get(Key.create(Location.class, locationId)));
			Key<Dashboard> dashboardKey = defaultDashBoard.getDashboard();
			defaultDashBoard.setDashboardObject(dashboards.get(dashboardKey));
		}
		for (Dashboard dashboard : dashboards.values()) {
			// dashboard.setUserId(
			// dashboard.getDashboardUserRef().getKey().getId());
		}
		return new ArrayList<Dashboard>(dashboards.values());
	}

	/*************************** Static Pages ****************************/

	@RequestMapping("getPagesForSite")
	public ModelAndView getPagesForSite() {
		System.out.print("+++++++++--------===========");
		Map<String, Object> model = new HashMap<String, Object>();
		Query<Page> query = OfyService.ofy().load().type(Page.class).order("order");
		List<Page> pages = query.list();
		List<String> names = new ArrayList<String>();
		for (Page page : pages) {
			names.add(page.getName());
		}
		model.put("names", names);
		return new ModelAndView("", model);
	}

	@RequestMapping("/pages/{name}")
	public ModelAndView getPage(@PathVariable("name") String name) {
		Map<String, Object> model = new HashMap<String, Object>();
		Query<Page> query = OfyService.ofy().load().type(Page.class).filter("name", name);
		Page page = query.first().getValue();
		model.put("title", page.getTitle());
		model.put("content", page.getContent());
		model.put("description", page.getDescription());
		model.put("keywords", page.getKeywords());
		return new ModelAndView("site/get_custom", model);
	}

	/****************************** Video *************************************/

	@RequestMapping("dashboard/video")
	public ModelAndView getVideo() {
		// get the locations from the datasheets
		Map<String, Object> model = new HashMap<String, Object>();

		return new ModelAndView("site/get_video", model);
	}

	@RequestMapping("testques")
	public ModelAndView getques() {
		// get the locations from the datasheets
		Map<String, Object> model = new HashMap<String, Object>();

		return new ModelAndView("site/get_questions_page", model);
	}

	/*******************************************************************/

	/******************************
	 * Drive Export
	 *************************************/

	// @RequestMapping("drive")
	// public void initDoc(HttpServletRequest req) throws Exception {
	// User user = UserServiceFactory.getUserService().getCurrentUser();
	// Credential credential = null;
	// // Credential credential = getStoredCredential(user.getUserId(),
	// // (CredentialStore) req.getSession().getServletContext()
	// // .getAttribute(OAuth2Constant.GOOG_CREDENTIAL_STORE));
	// //
	// // if (credential != null) {
	// // logger.info("Using access token: " + credential.getAccessToken());
	//
	// try {
	// GoogleCredential googleCredential = buildGoogleCredential(credential);
	//
	// Drive service = buildService(googleCredential);
	// if (service == null) {
	// logger.info("very bad!");
	// }
	//
	// File body = new File();
	// body.setTitle("My document");
	// body.setDescription("A test document");
	// body.setMimeType("text/plain");
	//
	// // java.io.File fileContent = new java.io.File("document.txt");
	// byte[] fileBytes = "test".getBytes();
	// ByteArrayContent mediaContent = new ByteArrayContent("text/plain",
	// fileBytes);
	//
	// service.files().insert(body, mediaContent).execute();
	// File file = service.files().insert(body, mediaContent).execute();
	// System.out.println("File ID: " + file.getId());
	//
	// } catch (HttpResponseException e) {
	// if (e.getStatusCode() == 401) {
	// logger.info(e.getMessage());
	// // Credentials have been revoked.
	// // TODO: Redirect the user to the authorization URL.
	// throw new UnsupportedOperationException();
	// }
	// } catch (IOException e) {
	// System.out.println("An error occurred: " + e);
	// }
	// // }
	//
	// }

	private GoogleCredential buildGoogleCredential(Credential credential) {
		try {
			// logger.warning(oauth2Service.getClientCredential().toString());

			String secret = "{\"web\":{\"auth_uri\":\"https://accounts.google.com/o/oauth2/auth\",\"token_uri\":\"https://accounts.google.com/o/oauth2/token\",\"client_email\":\"240644897271-c2dq00acunhb7s8rfu4dfofu39m53j2m@developer.gserviceaccount.com\",\"client_x509_cert_url\":\"https://www.googleapis.com/robot/v1/metadata/x509/240644897271-c2dq00acunhb7s8rfu4dfofu39m53j2m@developer.gserviceaccount.com\",\"client_id\":\"240644897271-c2dq00acunhb7s8rfu4dfofu39m53j2m.apps.googleusercontent.com\",\"auth_provider_x509_cert_url\":\"https://www.googleapis.com/oauth2/v1/certs\"}}";
			GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(new JacksonFactory(),
					new StringReader(secret));

			GoogleCredential googleCredential = new GoogleCredential.Builder().setClientSecrets(clientSecrets)
					.setTransport(new NetHttpTransport()).setJsonFactory(new JacksonFactory()).build();

			googleCredential.setAccessToken(credential.getAccessToken());
			googleCredential.setRefreshToken(credential.getRefreshToken());

			return googleCredential;

		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	private Drive buildService(GoogleCredential credential) {

		return new Drive.Builder(new NetHttpTransport(), new JacksonFactory(), credential).build();
	}

	public void initDoc(HttpServletRequest req) throws Exception {
		User user = UserServiceFactory.getUserService().getCurrentUser();

		Credential credential = null;
		// oauth2Service.getStoredCredential(
		// user.getUserId(),
		// (CredentialStore) req.getSession().getServletContext()
		// .getAttribute(OAuth2Constant.GOOG_CREDENTIAL_STORE));

		if (credential != null) {
			logger.info("Using access token: " + credential.getAccessToken());

			try {
				GoogleCredential googleCredential = buildGoogleCredential(credential);

				Drive service = buildService(googleCredential);

				if (service == null) {
					logger.info("very bad!");
				}

				File body = new File();
				body.setTitle("My document");
				body.setDescription("A test document");
				body.setMimeType("text/plain");

				byte[] fileBytes = "test".getBytes();
				ByteArrayContent mediaContent = new ByteArrayContent("text/plain", fileBytes);

				service.files().insert(body, mediaContent).execute();
				// File file = service.files().insert(body,
				// mediaContent).execute();
				// System.out.println("File ID: " + file.getId());

			} catch (HttpResponseException e) {
				if (e.getStatusCode() == 401) {
					logger.info(e.getMessage());
					// Credentials have been revoked.
					// TODO: Redirect the user to the authorization URL.
					throw new UnsupportedOperationException();
				}
			} catch (IOException e) {
				System.out.println("An error occurred: " + e);
			}
		}
	}

	private static String CLIENT_ID = "YOUR_CLIENT_ID";
	private static String CLIENT_SECRET = "YOUR_CLIENT_SECRET";

	private static String REDIRECT_URI = "urn:ietf:wg:oauth:2.0:oob";

	// @RequestMapping("drivet2")
	// public void testDrive() throws IOException {
	// HttpTransport httpTransport = new NetHttpTransport();
	// JsonFactory jsonFactory = new JacksonFactory();
	//
	// GoogleAuthorizationCodeFlow flow = new
	// GoogleAuthorizationCodeFlow.Builder(
	// httpTransport, jsonFactory, CLIENT_ID, CLIENT_SECRET,
	// Arrays.asList(DriveScopes.DRIVE)).setAccessType("online")
	// .setApprovalPrompt("auto").build();
	//
	// String url = flow.newAuthorizationUrl().setRedirectUri(REDIRECT_URI)
	// .build();
	// System.out
	// .println("Please open the following URL in your browser then type the
	// authorization code:");
	// System.out.println(" " + url);
	// BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	// String code = br.readLine();
	//
	// GoogleTokenResponse response = flow.newTokenRequest(code)
	// .setRedirectUri(REDIRECT_URI).execute();
	// GoogleCredential credential = new GoogleCredential()
	// .setFromTokenResponse(response);
	//
	// // Create a new authorized API client
	// Drive service = new Drive.Builder(httpTransport, jsonFactory,
	// credential).build();
	//
	// // Insert a file
	// File body = new File();
	// body.setTitle("My document");
	// body.setDescription("A test document");
	// body.setMimeType("text/plain");
	//
	// java.io.File fileContent = new java.io.File("document.txt");
	// FileContent mediaContent = new FileContent("text/plain", fileContent);
	//
	// File file = service.files().insert(body, mediaContent).execute();
	// System.out.println("File ID: " + file.getId());
	// }

	@RequestMapping("mydrive")
	public void myDrive(@RequestParam("code") String code) throws IOException, GeneralSecurityException {
		// String auth = MyDrive.getAuthorizationUrl(
		// "bright.creations.egypt@gmail.com", "/");
		// System.out.println(auth);
		// MyDrive.test();

		// AppIdentityCredential credential = new AppIdentityCredential(
		// Arrays.asList("https://www.googleapis.com/auth/drive.file"));

		GoogleAuthorizationCodeFlow cred = new GoogleAuthorizationCodeFlow.Builder(new NetHttpTransport(),
				new JacksonFactory(), "240644897271-8bbl4fvq3ptffnd01p57v3sh751f6qqa.apps.googleusercontent.com",
				"Dy9MtS6tDM7VCoPS714iJcsa", Collections.singleton(DriveScopes.DRIVE_FILE)).build();
		GoogleTokenResponse response = cred.newTokenRequest(code).setRedirectUri("http://localhost:8080/site/mydrive")
				.execute();

		Credential credential = cred.createAndStoreCredential(response, null);

		// final HttpRequestFactory requestFactory = new NetHttpTransport()
		// .createRequestFactory(credential);
		// Make an authenticated request
		// final GenericUrl url = new GenericUrl(
		// "https://www.googleapis.com/oauth2/v1/userinfo");
		// final HttpRequest request = requestFactory.buildGetRequest(url);
		// request.getHeaders().setContentType("application/json");
		// final String jsonIdentity = request.execute().parseAsString();

		// System.out.println("IDENTITY: " + jsonIdentity);

		Drive drive = new Drive.Builder(new NetHttpTransport(), new JacksonFactory(), credential).build();
		byte[] fileBytes = "test".getBytes();
		ByteArrayContent mediaContent = new ByteArrayContent("text/plain", fileBytes);
		File body = new File();
		body.setTitle("My document");
		body.setDescription("A test document");
		body.setMimeType("text/plain");
		logger.info("file size: " + drive.files().insert(body, mediaContent).execute());
		logger.info("app name: " + drive.getApplicationName());
		logger.info("File ID: " + body.getId());

		// String authorizeUrl = new GoogleAuthorizationRequestUrl(CLIENT_ID,
		// CALLBACK_URL, SCOPE).build();
		// System.out.println("Paste this url in your browser: " +
		// authorizeUrl);
		//
		// GoogleTokenResponse response = new
		// GoogleAuthorizationCodeTokenRequest(
		// new NetHttpTransport(),
		// new JacksonFactory(),
		// "240644897271-8bbl4fvq3ptffnd01p57v3sh751f6qqa.apps.googleusercontent.com",
		// "Dy9MtS6tDM7VCoPS714iJcsa", code,
		// "http://localhost:8080/site/dashboard").execute();
		//
		// CredentialManager.retri
		// buildEmpty().setAccessToken(response.getAccessToken();
	}

	// TODO Open a new window and perform all the redirects
	@RequestMapping(value = "oauthcallback", method = RequestMethod.GET)
	public ModelAndView ModelAndView(@RequestParam("code") String code, HttpServletRequest request) throws IOException {

		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(new NetHttpTransport(),
				new JacksonFactory(), "240644897271-8bbl4fvq3ptffnd01p57v3sh751f6qqa.apps.googleusercontent.com",
				"Dy9MtS6tDM7VCoPS714iJcsa", Collections.singleton(DriveScopes.DRIVE_FILE)).build();

		// System.out.println("Server Path redirect: " + request.getScheme()
		// + "://" + request.getServerName() + ":"
		// + request.getServerPort() + "/site/oauthcallback");

		GoogleTokenResponse driveResponse = flow.newTokenRequest(code).setRedirectUri(request.getScheme() + "://"
				+ request.getServerName() + ":" + request.getServerPort() + "/site/oauthcallback").execute();
		Credential credential = flow.createAndStoreCredential(driveResponse, null);

		HttpSession session = request.getSession();

		session.setAttribute("driveToken", credential.getAccessToken());
		// return new ModelAndView("redirect: " + state + "&code=" + code,
		// null);
		return new ModelAndView("site/get_oauth_callback", null);
	}

	@RequestMapping(value = "exportdrive", method = RequestMethod.POST)
	public ModelAndView exportToDrive(HttpServletResponse response, HttpServletRequest request,
			@RequestParam("tablesNames") String tablesNames
	// @RequestParam(value = "code", required = false) String code
	// @RequestParam(value = "link", required = false) String link) {
	) {
		Map<String, String> model = new HashMap<String, String>();
		String json = request.getParameter("json");

		XlsExport xlsExport = new XlsExport();
		Type table = new TypeToken<LinkedHashMap<String, List<List<String>>>>() {
		}.getType();
		Type tablesNamesType = new TypeToken<LinkedHashMap<String, String>>() {
		}.getType();
		Gson gson = new Gson();
		LinkedHashMap<String, List<List<String>>> tables = gson.fromJson(json, table);

		LinkedHashMap<String, String> tablesNamesMapByClass = gson.fromJson(tablesNames, tablesNamesType);
		HttpSession session = request.getSession();
		String accessToken = (String) session.getAttribute("driveToken");

		try {

			logger.info("Drive Access Token: " + accessToken);
			if (accessToken == null) {
				// logger.info("OAuth state: " + link);
				// logger.info("OAUTH Redirect: "
				// +
				// "https://accounts.google.com/o/oauth2/auth?response_type=code&client_id=240644897271-8bbl4fvq3ptffnd01p57v3sh751f6qqa.apps.googleusercontent.com&redirect_uri=/site/oauthcallback&access_type=online&scope=https://www.googleapis.com/auth/drive.file");
				model.put("redirect",
						"https://accounts.google.com/o/oauth2/auth?response_type=code&client_id=240644897271-8bbl4fvq3ptffnd01p57v3sh751f6qqa.apps.googleusercontent.com&redirect_uri="
								+ request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
								+ "/site/oauthcallback&access_type=online&scope=https://www.googleapis.com/auth/drive.file");
				// + URLEncoder.encode(link, "UTF-8"));
				return new ModelAndView("", model);
			}

			// System.out.println("Code resp: " + codeRequestResopnse);

			Credential credential = new Credential(BearerToken.authorizationHeaderAccessMethod())
					.setAccessToken(accessToken);
			Drive drive = new Drive.Builder(new NetHttpTransport(), new JacksonFactory(), credential).build();

			Files.List fileSearch = drive.files().list()
					.setQ("title = 'Google+ Brand Tracker Export' and trashed = false").setMaxResults(1);

			File folder = null;
			try {
				FileList files = fileSearch.execute();

				List<File> resultFiles = files.getItems();

				if (!resultFiles.isEmpty()) {
					folder = resultFiles.get(0);
				}

				if (folder == null) {

					File body = new File();
					body.setTitle("Google+ Brand Tracker Export");
					body.setMimeType("application/vnd.google-apps.folder");
					folder = drive.files().insert(body).execute();

				}
				logger.info("folderLink" + folder);
				model.put("folderLink", folder.getAlternateLink());

				xlsExport.exporterXLSToDrive(drive, folder.getId(), request, tables, tablesNamesMapByClass,
						"Google + brand tracker table");

				driveImageExport(drive, folder.getId(), request, "Google + brand tracker chart");
			} catch (GoogleJsonResponseException e) {
				e.printStackTrace();
				model.put("redirect",
						"https://accounts.google.com/o/oauth2/auth?response_type=code&client_id=240644897271-8bbl4fvq3ptffnd01p57v3sh751f6qqa.apps.googleusercontent.com&redirect_uri="
								+ request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
								+ "/site/oauthcallback&access_type=online&scope=https://www.googleapis.com/auth/drive.file");
				// + URLEncoder.encode(link, "UTF-8"));
				return new ModelAndView("", model);
			}

		} catch (RowsExceededException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WriteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ModelAndView("", model);
	}

	private String driveImageExport(Drive drive, String folderId, HttpServletRequest request, String fileName)
			throws IOException {
		ByteArrayOutputStream output = new ByteArrayOutputStream();

		String svgImageData = request.getParameter("svg");
		svgImageData = svgImageData.replace("data:image/png;base64,", "");

		Base64 b = new Base64();
		byte[] imageArray = b.decode(svgImageData.getBytes());

		Collection<Composite> composites = new ArrayList<Composite>();
		ImagesService imagesService = ImagesServiceFactory.getImagesService();

		Image graphImage = ImagesServiceFactory.makeImage(imageArray);

		Composite graphComposite = ImagesServiceFactory.makeComposite(graphImage, 0, 0, 1f, Composite.Anchor.TOP_LEFT);

		composites.add(graphComposite);

		output.write(imageArray);
		output.flush();
		ByteArrayContent mediaContent = new ByteArrayContent("image/png", output.toByteArray());
		File body = new File();

		ArrayList<ParentReference> parentReferenceList = new ArrayList<ParentReference>();
		ParentReference folderReference = new ParentReference();
		folderReference.setId(folderId);
		folderReference.setKind("drive#fileLink");

		parentReferenceList.add(folderReference);

		body.setParents(parentReferenceList);
		body.setTitle(fileName.replace("\"", ""));
		body.setDescription("Chart exported from GPBTT");
		body.setMimeType("image/png;base64");
		// System.out.println("file size: "
		// +
		drive.files().insert(body, mediaContent).execute();
		// );

		return body.getAlternateLink();
	}

	public static String excuteGet(String targetURL, String urlParameters) {
		URL url;
		HttpURLConnection connection = null;
		try {
			// Create connection
			url = new URL(targetURL);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

			connection.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));
			connection.setRequestProperty("Content-Language", "en-US");

			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);

			// Send request
			DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();

			// Get Response
			InputStream is = connection.getInputStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is));
			String line;
			StringBuffer response = new StringBuffer();
			while ((line = rd.readLine()) != null) {
				response.append(line);
				response.append('\r');
			}
			rd.close();
			return response.toString();

		} catch (Exception e) {

			e.printStackTrace();
			return null;

		} finally {

			if (connection != null) {
				connection.disconnect();
			}
		}
	}

	@RequestMapping(value = "dashboard/{id}/orderwidgets", method = RequestMethod.POST)
	public ModelAndView postWidgetsOrder(@PathVariable("id") Long dashboardId,
			@RequestParam("hidden-user-id") Long userId, ResourceOrderCollection widgetsOrder) {

		Key<ApplicationUser> parentUserKey = Key.create(ApplicationUser.class, userId);

		List<Widget> dashBoardWidgets = OfyService.ofy().load().type(Widget.class)
				.ancestor(Key.create(parentUserKey, Dashboard.class, dashboardId)).list();

		for (Widget widget : dashBoardWidgets) {
			Integer newWidgetOrder = widgetsOrder.getRom().get(widget.getId());
			if (newWidgetOrder == null) {
				newWidgetOrder = 0;
			}

			widget.setOrder(newWidgetOrder);
		}

		OfyService.ofy().save().entities(dashBoardWidgets).now();

		return new ModelAndView("redirect:/site/dashboard");
	}

	@RequestMapping(value = "delDash", method = RequestMethod.GET)
	public void deleteNonDefaultCountryDashboard() {
		List<DefaultDashBoard> defaultDashboards = OfyService.ofy().load().type(DefaultDashBoard.class).list();

		List<Key<Dashboard>> dashBoardsToDelete = new ArrayList<Key<Dashboard>>();
		List<Key<Dashboard>> defDashboards = new ArrayList<Key<Dashboard>>();
		for (DefaultDashBoard defaultDashBoard : defaultDashboards) {
			Key<Dashboard> dashboard = defaultDashBoard.getDashboard();
			if (dashboard != null) {
				defDashboards.add(dashboard);
			}
		}

		List<Dashboard> allDashBoards = OfyService.ofy().load().type(Dashboard.class).list();

		for (Dashboard dashboard : allDashBoards) {
			boolean contains = false;
			for (Key<Dashboard> defDashboard : defDashboards) {
				if (Key.create(dashboard).equals(defDashboard)) {
					contains = true;
					break;
				}

			}
			if (!contains) {
				// dashBoardsToDelete.add(
				// Key.create(dashboard.getDashboardUserRef().getKey(),
				// Dashboard.class, dashboard.getId()));
			}
			logger.info("DashBoard: " + dashboard.getName() + "__ Contains: " + contains);
		}

		List<Dashboard> dashboardsToDelObj = new ArrayList<Dashboard>(
				OfyService.ofy().load().keys(dashBoardsToDelete).values());

		logger.info("DASHBOARD-OBJ Size: " + dashboardsToDelObj.size());
		logger.info("DASHBOARD Size: " + dashBoardsToDelete.size());
		// for (Key<Dashboard> dashBoardToDelete : dashBoardsToDelete) {
		// logger.info("DASHBOARD KEY: " + dashBoardToDelete);
		// }
		for (Dashboard dashBoard : dashboardsToDelObj) {
			logger.info("DASHBOARD NAME: " + dashBoard.getName());
		}

		List<Widget> widgets = OfyService.ofy().load().type(Widget.class).list();
		Map<Key<Dashboard>, List<Widget>> widgetsByDashboardId = new HashMap<Key<Dashboard>, List<Widget>>();

		for (Widget widget : widgets) {
			System.out.println("");
			// List<Widget> entry = widgetsByDashboardId
			// .get(widget.getDashboard()));
			// if (entry == null) {
			// entry = new ArrayList<Widget>();
			// widgetsByDashboardId.put(widget.getDashboard(), entry);
			// }
			// entry.add(widget);
			// widgetsByDashboardId.put(widget.getDashboard(), entry);
		}

		Map<Key<Dashboard>, Dashboard> loadedDashboards = OfyService.ofy().load().keys(widgetsByDashboardId.keySet());

		ArrayList<Widget> widgetsToDelete = new ArrayList<Widget>();
		for (Key<Dashboard> dashboardKey : widgetsByDashboardId.keySet()) {
			if (loadedDashboards.get(dashboardKey) == null) {
				widgetsToDelete.addAll(widgetsByDashboardId.get(dashboardKey));
				logger.info("DASHBOARD-NULL: " + dashboardKey);
			}
		}

		logger.info("ALL-WIGETS Size: " + widgets.size());
		logger.info("WIGETS-TO-DELET Size: " + widgetsToDelete.size());

		OfyService.ofy().delete().entities(dashboardsToDelObj).now();
		OfyService.ofy().delete().entities(widgetsToDelete).now();

		List<ApplicationUser> applicationUsers = OfyService.ofy().load().type(ApplicationUser.class).list();

		for (ApplicationUser applicationUser : applicationUsers) {
			applicationUser.setDefaultCountry(null);
		}

		OfyService.ofy().save().entities(applicationUsers).now();

	}

}

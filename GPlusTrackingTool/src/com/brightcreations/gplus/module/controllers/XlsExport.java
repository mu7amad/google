/*******************************************************
 * Copyright (c) 2016 Bright Creations , All Rights Reserved. NOTICE: All
 * information contained herein is, and remains the property of Bright
 * Creations. Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained Access to
 * the source code contained herein is hereby forbidden to anyone except current
 * Bright Creations employees, managers or parties given license to view and
 * create derivative works for the source code and who have executed
 * Confidentiality and Non-disclosure agreements explicitly covering such
 * access.
 *******************************************************/

package com.brightcreations.gplus.module.controllers;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.CellView;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.client.http.ByteArrayContent;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.ParentReference;

/**
 * 
 * @author Bright Creations
 * 
 */

public class XlsExport {

	private WritableSheet sheet;
	WritableWorkbook workbook;
	private CellView autoSizeCellView;
	private WritableCellFormat titleRed;
	private WritableCellFormat titleGreen;
	private WritableCellFormat titleBlue;
	private WritableCellFormat head;
	private WritableCellFormat content;
	private WritableCellFormat contentBlue;
	private WritableCellFormat contentGreen;
	private WritableCellFormat contentRed;
	private Map<String, WritableCellFormat> titles = new HashMap<String, WritableCellFormat>();

	private final Logger logger = LoggerFactory.getLogger(getClass());

	public XlsExport() {

	}

	public void initSingleSheet(OutputStream outputStream) {
		try {
			workbook = Workbook.createWorkbook(outputStream);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		workbook.createSheet("Exported Data", 0);
	}

	public void initMultipleSheets(OutputStream outputStream) {
		try {
			workbook = Workbook.createWorkbook(outputStream);

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public void init(int sheetNumber) {
		try {
			sheet = workbook.getSheet(sheetNumber);

			WritableFont headFont = new WritableFont(WritableFont.ARIAL, 14, WritableFont.BOLD, false,
					UnderlineStyle.NO_UNDERLINE);

			head = new WritableCellFormat(headFont);
			head.setWrap(false);
			head.setShrinkToFit(true);
			head.setBorder(Border.ALL, BorderLineStyle.THIN);
			head.setBackground(Colour.GRAY_25);
			head.setVerticalAlignment(VerticalAlignment.CENTRE);
			head.setAlignment(Alignment.CENTRE);

			WritableFont titleFont = new WritableFont(WritableFont.ARIAL, 12, WritableFont.BOLD, false,
					UnderlineStyle.NO_UNDERLINE);
			titleFont.setColour(Colour.WHITE);

			titleBlue = new WritableCellFormat(titleFont);
			titleGreen = new WritableCellFormat(titleFont);
			titleRed = new WritableCellFormat(titleFont);

			titles.put("blue", titleBlue);
			titles.put("green", titleGreen);
			titles.put("red", titleRed);

			for (WritableCellFormat title : titles.values()) {

				title.setWrap(true);
				title.setShrinkToFit(true);
				title.setBorder(Border.ALL, BorderLineStyle.THIN);

				title.setVerticalAlignment(VerticalAlignment.CENTRE);
				title.setAlignment(Alignment.CENTRE);
			}
			workbook.setColourRGB(Colour.BLUE, 69, 114, 167);
			workbook.setColourRGB(Colour.GREEN, 35, 174, 43);
			workbook.setColourRGB(Colour.RED, 225, 7, 4);

			titleBlue.setBackground(Colour.BLUE);
			titleRed.setBackground(Colour.RED);
			titleGreen.setBackground(Colour.GREEN);

			WritableFont contentFontRed = new WritableFont(WritableFont.ARIAL, 12, WritableFont.BOLD, false,
					UnderlineStyle.NO_UNDERLINE, Colour.RED);

			WritableFont contentFontGreen = new WritableFont(WritableFont.ARIAL, 12, WritableFont.BOLD, false,
					UnderlineStyle.NO_UNDERLINE, Colour.GREEN);

			WritableFont contentFontBlue = new WritableFont(WritableFont.ARIAL, 12, WritableFont.BOLD, false,
					UnderlineStyle.NO_UNDERLINE, Colour.BLUE);

			WritableFont contentFont = new WritableFont(WritableFont.ARIAL, 12, WritableFont.NO_BOLD, false,
					UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
			content = new WritableCellFormat(contentFont);

			content.setWrap(false);
			content.setShrinkToFit(true);
			content.setBorder(Border.ALL, BorderLineStyle.THIN);
			content.setVerticalAlignment(VerticalAlignment.CENTRE);

			contentGreen = new WritableCellFormat(contentFontGreen);

			contentGreen.setWrap(false);
			contentGreen.setShrinkToFit(true);
			contentGreen.setBorder(Border.ALL, BorderLineStyle.THIN);
			contentGreen.setVerticalAlignment(VerticalAlignment.CENTRE);

			contentBlue = new WritableCellFormat(contentFontBlue);

			contentBlue.setWrap(false);
			contentBlue.setShrinkToFit(true);
			contentBlue.setBorder(Border.ALL, BorderLineStyle.THIN);
			contentBlue.setVerticalAlignment(VerticalAlignment.CENTRE);

			contentRed = new WritableCellFormat(contentFontRed);

			contentRed.setWrap(false);
			contentRed.setShrinkToFit(true);
			contentRed.setBorder(Border.ALL, BorderLineStyle.THIN);
			contentRed.setVerticalAlignment(VerticalAlignment.CENTRE);

			autoSizeCellView = new CellView();
			autoSizeCellView.setAutosize(true);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public WritableCellFormat getColor(String cssClass) {
		if (cssClass.contains("green")) {
			return titles.get("green");
		} else if (cssClass.contains("blue")) {
			return titles.get("blue");
		} else if (cssClass.contains("red")) {
			return titles.get("red");
		}
		return null;
	}

	public void exporterXLS(HttpServletResponse response, HttpServletRequest request,
			LinkedHashMap<String, List<List<String>>> tables, Map<String, String> tablesNamesMapByClass,
			String fileName) throws IOException, RowsExceededException, WriteException {

		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName.replace("\"", "") + ".xls\"");

		// File imageFile = new File(GIF_OR_JPG_IMAGE_FILE);
		// BufferedImage input = ImageIO.read(imageFile);
		// ByteArrayOutputStream baos = new ByteArrayOutputStream();
		// ImageIO.write(input, "PNG", baos);
		// sheet.addImage(new WritableImage(1,1,input.getWidth() /
		// CELL_DEFAULT_WIDTH,
		// input.getHeight() / CELL_DEFAULT_HEIGHT,baos.toByteArray()));

		OutputStream output = response.getOutputStream();

		// String svgImageData = request.getParameter("svg");
		// svgImageData = svgImageData.replace("data:image/png;base64,", "");
		//
		// Base64 b = new Base64();
		// byte[] imageArray = b.decode(svgImageData.getBytes());
		//
		// ImagesService imagesService =
		// ImagesServiceFactory.getImagesService();
		//
		// Image graphImage = ImagesServiceFactory.makeImage(imageArray);
		//
		// WritableImage img = new WritableImage(1, 1, 1, 1, imageArray);
		//
		// System.out.println(img.getDrawingGroup());
		//
		// sheet.addImage(

		// output.write(imageArray);

		// OutputStream output = response.getOutputStream();

		initSingleSheet(output);
		init(0);
		int heightInPoints = 26 * 20;

		Label label;
		int tableIndex = 0;
		int firstIndex = 0;
		for (String tableCssClass : tables.keySet()) {
			int i = tableIndex;
			firstIndex = tableIndex;
			int headersCount = 0;
			label = new Label(0, i, tablesNamesMapByClass.get(tableCssClass));
			i++;
			firstIndex++;
			sheet.addCell(label);
			for (List<String> row : tables.get(tableCssClass)) {
				int j = 0;
				if (i == firstIndex) {
					headersCount = row.size();
					sheet.setRowView(i, heightInPoints);
					for (String cell : row) {
						label = new Label(j, i, cell, getColor(tableCssClass));
						sheet.addCell(label);
						j++;

					}
				} else {
					if (row.size() < headersCount) {
						for (int x = 0; x < headersCount - row.size(); x++) {
							label = new Label(j, i, "", content);
							sheet.addCell(label);
							j++;
						}
					}
					for (String cell : row) {
						label = new Label(j, i, cell, content);
						sheet.addCell(label);
						j++;
					}
				}
				i++;
				tableIndex++;
			}
			tableIndex += 2;
			for (int j = 0; j < sheet.getColumns(); j++) {
				sheet.setColumnView(j, autoSizeCellView);
			}
		}

		workbook.write();
		workbook.close();

	}

	public String exporterXLSToDrive(Drive drive, String folderId, HttpServletRequest request,
			LinkedHashMap<String, List<List<String>>> tables, Map<String, String> tablesNamesMapByClass,
			String fileName) throws IOException, RowsExceededException, WriteException {

		ByteArrayOutputStream output = new ByteArrayOutputStream();

		// OutputStream output = response.getOutputStream();

		// String svgImageData = request.getParameter("svg");
		// svgImageData = svgImageData.replace("data:image/png;base64,", "");
		//
		// Base64 b = new Base64();
		// byte[] imageArray = b.decode(svgImageData.getBytes());
		//
		// ImagesService imagesService =
		// ImagesServiceFactory.getImagesService();
		//
		// Image graphImage = ImagesServiceFactory.makeImage(imageArray);
		//
		// WritableImage img = new WritableImage(1, 1, 1, 1, imageArray);
		//
		// System.out.println(img.getDrawingGroup());
		//
		// sheet.addImage(

		// output.write(imageArray);

		// OutputStream output = response.getOutputStream();

		initSingleSheet(output);
		init(0);
		int heightInPoints = 26 * 20;

		Label label;
		int tableIndex = 0;
		int firstIndex = 0;
		for (String tableCssClass : tables.keySet()) {
			int i = tableIndex;
			firstIndex = tableIndex;
			int headersCount = 0;
			label = new Label(0, i, tablesNamesMapByClass.get(tableCssClass));
			i++;
			firstIndex++;
			sheet.addCell(label);
			for (List<String> row : tables.get(tableCssClass)) {
				int j = 0;
				if (i == firstIndex) {
					headersCount = row.size();
					sheet.setRowView(i, heightInPoints);
					for (String cell : row) {
						label = new Label(j, i, cell, getColor(tableCssClass));
						sheet.addCell(label);
						j++;

					}
				} else {
					if (row.size() < headersCount) {
						for (int x = 0; x < headersCount - row.size(); x++) {
							label = new Label(j, i, "", content);
							sheet.addCell(label);
							j++;
						}
					}
					for (String cell : row) {
						label = new Label(j, i, cell, content);
						sheet.addCell(label);
						j++;
					}
				}
				i++;
				tableIndex++;
			}
			tableIndex += 2;
			for (int j = 0; j < sheet.getColumns(); j++) {
				sheet.setColumnView(j, autoSizeCellView);
			}
		}

		workbook.write();
		workbook.close();

		ByteArrayContent mediaContent = new ByteArrayContent("application/vnd.ms-excel", output.toByteArray());
		File body = new File();

		ArrayList<ParentReference> parentReferenceList = new ArrayList<ParentReference>();
		ParentReference folderReference = new ParentReference();
		folderReference.setId(folderId);
		folderReference.setKind("drive#fileLink");

		parentReferenceList.add(folderReference);

		body.setParents(parentReferenceList);
		body.setTitle(fileName);
		body.setDescription("Data exported from GPBTT");
		body.setMimeType("application/vnd.ms-excel");
		logger.info("file size: " + drive.files().insert(body, mediaContent).setConvert(true).execute());

		return body.getAlternateLink();

	}
}

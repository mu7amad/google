/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.controllers;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.brightcreations.gplus.module.model.DataSheetsZipEntry;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;

/**
 * @author Mohammed.Eladly
 */

@Controller
@RequestMapping("downloadController")
public class DownloadController {
	private BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();

	@RequestMapping(value = "download/{zipName}")
	public void download(@PathVariable("zipName") String zipName, HttpServletRequest req, HttpServletResponse res)
			throws IOException {

		DataSheetsZipEntry zipEntry = OfyService.ofy().load().type(DataSheetsZipEntry.class)
				.filter("name", zipName.trim()).first().getValue();

		BlobKey blobKey = zipEntry.getBlobstoreKey();
		String filename = zipEntry.getName();
		res.setHeader("Content-Disposition", "attachment; filename=\"" + filename);

		blobstoreService.serve(blobKey, res);
	}
}

/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.controllers;

import static com.brightcreations.gplus.module.controllers.OfyService.ofy;

import java.io.IOException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.brightcreations.gplus.module.controllers.EmailMessageController.EmailTypes;
import com.brightcreations.gplus.module.model.Answer;
import com.brightcreations.gplus.module.model.ApplicationUser;
import com.brightcreations.gplus.module.model.Brand;
import com.brightcreations.gplus.module.model.BrandPerQuestionPerDataSetPerLocation;
import com.brightcreations.gplus.module.model.Category;
import com.brightcreations.gplus.module.model.Dashboard;
import com.brightcreations.gplus.module.model.DataOperation;
import com.brightcreations.gplus.module.model.DataSet;
import com.brightcreations.gplus.module.model.DataSheet;
import com.brightcreations.gplus.module.model.DataSheet.dataSheetStatus;
import com.brightcreations.gplus.module.model.DefaultDashBoard;
import com.brightcreations.gplus.module.model.Division;
import com.brightcreations.gplus.module.model.Filter;
import com.brightcreations.gplus.module.model.FilteredAnswerValue;
import com.brightcreations.gplus.module.model.Location;
import com.brightcreations.gplus.module.model.Page;
import com.brightcreations.gplus.module.model.Question;
import com.brightcreations.gplus.module.model.Region;
import com.brightcreations.gplus.module.model.ResourceOrderCollection;
import com.brightcreations.gplus.module.model.Settings;
import com.brightcreations.gplus.module.model.collection.ResourceList;
import com.brightcreations.gplus.web.converter.MarshallableList;
import com.brightcreations.gplus.web.exception.CustomException;
import com.brightcreations.gplus.web.security.ApplicationUserRole;
import com.brightcreations.gplus.web.security.ApplicationUserType;
import com.google.appengine.api.backends.BackendServiceFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.RetryOptions;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import com.google.appengine.api.users.UserServiceFactory;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.cmd.Query;

import util.AuthorizationValidatior;

/**
 * 
 * @author Mohamed Salah El-Din
 * @author Ismail Marmoush
 * @author Ramy Mahmoudi
 * @author mohammed eladly
 */
@Controller
@RequestMapping("/cms")
public class CMSController {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	protected EmailMessageController emailController;

	/* ______________________________________________________________________________________ */
	/************************************* Navigation ****************************************/
	/* ____________________________________________________________________________________ */
	@RequestMapping("")
	public ModelAndView navigateToDashBoard() {
		return new ModelAndView("cms/dashboard");

	}

	@RequestMapping(value = "studies", method = RequestMethod.GET)
	public ModelAndView navigateToStudiesPage(HttpServletRequest request, HttpServletResponse response) {
		return new ModelAndView("cms/get_study");
	}

	@RequestMapping(value = "navigateToAdminsPage", method = RequestMethod.GET)
	public ModelAndView navigateToAdminsPage(HttpServletRequest request, HttpServletResponse response) {
		return new ModelAndView("cms/get_admins");
	}

	@RequestMapping(value = "navigateToViewStudyData", method = RequestMethod.GET)
	public ModelAndView navigateToViewStudyData(HttpServletRequest request, HttpServletResponse response) {
		return new ModelAndView("cms/view_study_data");
	}

	public EmailMessageController getEmailMessageController() {
		return emailController;
	}

	public void setEmailMessageController(EmailMessageController emailMessageController) {
		this.emailController = emailMessageController;
	}

	@RequestMapping("/{studyId}")
	public ModelAndView getDashboard(@PathVariable("studyId") String studyId) {
		Map<String, Object> model = new HashMap<String, Object>();
		// BlobstoreService blobstoreService = BlobstoreServiceFactory
		// .getBlobstoreService();
		// model.put("uploadUrl", blobstoreService.createUploadUrl("/upload"));
		model.put("studyId", studyId);
		return new ModelAndView("cms/dashboard", model);

	}

	/* ______________________________________________________________________________________ */
	/*************************************** Data ********************************************/
	/* ____________________________________________________________________________________ */

	/* Assign a string that contains the set of characters you allow. */
	private static final String symbols = "ABCDEFGJKLMNPRSTUVWXYZ-abcdefghijklmnopqrstuvwxyz_0123456789";

	private final Random random = new SecureRandom();

	private char[] buf;

	public String nextString() {
		int length = 50;
		if (length < 1)
			throw new IllegalArgumentException("length < 1: " + length);
		buf = new char[length];
		for (int idx = 0; idx < buf.length; ++idx)
			buf[idx] = symbols.charAt(random.nextInt(symbols.length()));
		return new String(buf);
	}

	@RequestMapping("/data")
	public ModelAndView getDatasetsMain() {

		return new ModelAndView("redirect:/cms/data/dataset");

	}

	// @RequestMapping("/dataa")
	// public ModelAndView getDatasetsMainn() {
	// List<ApplicationUser> users = OfyService.ofy().load()
	// .type(ApplicationUser.class).list();
	// for (ApplicationUser applicationUser : users) {
	// if (applicationUser.getAuthorities().contains(
	// ApplicationUserRole.ROLE_ADMIN.getAuthority())
	// && applicationUser.getAuthorities().contains(
	// ApplicationUserRole.ROLE_RESEARCHER.getAuthority())
	// && applicationUser.getAuthorities().contains(
	// ApplicationUserRole.ROLE_USER.getAuthority())) {
	// applicationUser.setSuperUser(Boolean.TRUE);
	// } else {
	// applicationUser.setSuperUser(Boolean.FALSE);
	// }
	// }
	// OfyService.ofy().save().entities(users).now();
	// return new ModelAndView("redirect:/cms/data/dataset");
	// }

	@RequestMapping("/dummy")
	public ModelAndView createDummyDataOperation() {
		DataOperation dataOperation = new DataOperation();
		dataOperation.setMessage("The CMS is locked now !");
		OfyService.ofy().save().entity(dataOperation).now();

		return new ModelAndView("");
	}

	public void checkOperationLock(Map<String, Object> model) {

		DataOperation dataOperation = OfyService.ofy().load().type(DataOperation.class).first().getValue();

		if (dataOperation == null) {
			model.put("lock", false);
		} else {
			model.put("lock", true);
		}
		model.put("dataOperation", dataOperation);
	}

	public ModelAndView blockIfOperationLock() throws IOException {

		DataOperation dataOperation = OfyService.ofy().load().type(DataOperation.class).first().getValue();

		if (dataOperation != null) {
			logger.error("BLOCKED OPERATION! LOCK!");
			return new ModelAndView("/error/exception?msg=" + "INVALID OPERATION !");
		}
		logger.info("Lock check passed.");
		return null;
	}

	/********************************/
	/* ___________Questions_______ */
	/******************************/

	@RequestMapping("/data/question")
	public ModelAndView getQuestions(
			@RequestParam(value = "orderBy", defaultValue = "name", required = false) String orderBy,
			@RequestParam(value = "orderDir", defaultValue = "asc", required = false) String orderDir,
			@RequestParam(value = "offset", defaultValue = "0", required = false) Integer offset,
			@RequestParam(value = "count", defaultValue = "20", required = false) Integer count) {

		Query<Question> query = OfyService.ofy().load().type(Question.class);
		int totalQuestionCount = query.count();

		int originalCount = count;
		if (offset < 0) {
			offset = 0;
		}
		if (offset > 0) {
			query = query.offset(offset);
		}
		if (count == 0) {
			count = totalQuestionCount;
		}
		if (count >= totalQuestionCount) {
			offset = 0;
			count = totalQuestionCount;
		}
		if (offset + count > totalQuestionCount) {
			count = totalQuestionCount - offset;
		}
		if (count > 0) {
			query = query.limit(count);
		}
		if (orderBy != null && orderDir != null) {
			if (orderDir.equals("asc")) {
				query = query.order(orderBy);
			} else {
				query = query.order("-" + orderBy);
			}
		}
		List<Question> questionsList = query.list();
		if (questionsList.size() == 0 && offset == 0 && Math.min(count + offset, totalQuestionCount) == 0) {
			offset = -1;
			count += 1;
		}
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("resources", questionsList);
		model.put("total", totalQuestionCount);
		model.put("from", offset);
		model.put("to", Math.min(count + offset, totalQuestionCount));

		model.put("count", originalCount);
		checkOperationLock(model);

		return new ModelAndView("cms/get_questions", model);
	}

	@RequestMapping("/data/question/list_order")
	public ModelAndView getQuestionsOrder() {

		Query<Question> query = OfyService.ofy().load().type(Question.class);

		List<Question> questionsList = query.list();
		Collections.sort(questionsList, new Comparator<Question>() {

			public int compare(Question q1, Question q2) {
				return q1.getListOrder().compareTo(q2.getListOrder());
			}
		});
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("resources", questionsList);
		// model.put("total", totalQuestionCount);
		checkOperationLock(model);

		return new ModelAndView("cms/get_questions_list_order", model);
	}

	@RequestMapping(value = "/data/question/list_order", method = RequestMethod.POST)
	public ModelAndView postQuestionsListOrder(ResourceOrderCollection questionsOrder) throws IOException {

		ModelAndView blockOperation = blockIfOperationLock();
		if (blockOperation != null) {
			return blockOperation;
		}

		// logger.info(questionsOrder.toString());

		Query<Question> query = OfyService.ofy().load().type(Question.class);

		List<Question> questionsList = query.list();

		for (Question question : questionsList) {
			question.setListOrder(questionsOrder.getRom().get(question.getId()));
		}

		OfyService.ofy().save().entities(questionsList).now();

		return new ModelAndView("redirect:/cms/data/question");
	}

	@RequestMapping(value = "/data/edit/question/{id}", method = RequestMethod.GET)
	public ModelAndView editQuestion(@PathVariable("id") Long id) {
		Question question = OfyService.ofy().load().key(Key.create(Question.class, id)).getValue();

		List<Key<Answer>> answersKeys = question.getAnswers();
		ArrayList<Answer> answers = new ArrayList<Answer>(OfyService.ofy().load().keys(answersKeys).values());

		List<Key<DataSet>> dataSetsKeys = question.getDataSets();
		List<DataSet> dataSets = new ArrayList<DataSet>(OfyService.ofy().load().keys(dataSetsKeys).values());

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("resource", question);
		model.put("dataSets", dataSets);
		model.put("answers", answers);

		checkOperationLock(model);
		return new ModelAndView("cms/edit_question", model);
	}

	@RequestMapping(value = "/data/save/question/{id}", method = RequestMethod.POST)
	public ModelAndView saveQuestion(@PathVariable("id") Long id, HttpServletRequest request) throws IOException {

		ModelAndView blockOperation = blockIfOperationLock();
		if (blockOperation != null) {
			return blockOperation;
		}

		Question question = OfyService.ofy().load().key(Key.create(Question.class, id)).getValue();
		Map<Key<Answer>, Answer> answers = OfyService.ofy().load().keys(question.getAnswers());

		Enumeration parameters = request.getParameterNames();
		List<String> groups = new ArrayList<String>();
		while (parameters.hasMoreElements()) {
			String paramName = parameters.nextElement().toString();
			if (paramName.contains("grp-")) {
				String groupName = request.getParameter(paramName);
				if (groupName != null) {
					groups.add(groupName);
				}
			}
		}
		question.setAnswerGroups(groups);

		if (request.getParameter("q_name") != null) {
			question.setQuestionText(request.getParameter("q_name"));
		}
		if (request.getParameter("q_buzz") != null && !request.getParameter("q_buzz").isEmpty()) {
			question.setBuzzWord(request.getParameter("q_buzz"));
		}
		if (request.getParameter("q_description") != null && !request.getParameter("q_description").isEmpty()) {
			question.setDescription(request.getParameter("q_description"));
		}
		List<Key<Answer>> questionsKeys = question.getAnswers();
		for (Key<Answer> key : questionsKeys) {
			Answer answer = answers.get(key);
			String onSnapshot = request.getParameter("snap_" + key.getId());
			String onTrend = request.getParameter("trend_" + key.getId());
			String group = request.getParameter("group_" + key.getId());

			if (onSnapshot != null && onSnapshot.equals("on")) {
				answer.setOnSnapshot(Boolean.TRUE);
			} else {
				answer.setOnSnapshot(Boolean.FALSE);
			}

			if (onTrend != null && onTrend.equals("on")) {
				answer.setOnTrend(Boolean.TRUE);
			} else {
				answer.setOnTrend(Boolean.FALSE);
			}
			if (group != null) {
				answer.setAnswerGroup(group);
			}
		}
		OfyService.ofy().save().entity(question).now();
		OfyService.ofy().save().entities(answers.values()).now();
		Map<String, Object> model = new HashMap<String, Object>();

		return new ModelAndView("redirect:/cms/data/question");
	}

	/********************************/
	/* ___________Categories_______ */
	/******************************/

	@RequestMapping("/data/category")
	public ModelAndView getCategories(
			@RequestParam(value = "orderBy", defaultValue = "name", required = false) String orderBy,
			@RequestParam(value = "orderDir", defaultValue = "asc", required = false) String orderDir,
			@RequestParam(value = "offset", defaultValue = "0", required = false) Integer offset,
			@RequestParam(value = "count", defaultValue = "20", required = false) Integer count) {

		Query<Category> query = OfyService.ofy().load().type(Category.class);

		List<Category> categoriesList = query.list();

		Collections.sort(categoriesList, new Comparator<Category>() {

			public int compare(Category c1, Category c2) {

				if (c1.getOrder() >= c2.getOrder()) {
					return 1;
				} else if (c1.getOrder() == c2.getOrder()) {
					return 0;
				} else {
					return -1;
				}
			}
		});

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("resources", categoriesList);

		checkOperationLock(model);

		return new ModelAndView("cms/get_categories", model);
	}

	@RequestMapping("/data/category/create")
	public ModelAndView createCategory() {

		Map<String, Object> model = new HashMap<String, Object>();

		checkOperationLock(model);

		return new ModelAndView("cms/create_category", model);
	}

	@RequestMapping("/data/question/categorize")
	public ModelAndView categorizeQuestions(
			@RequestParam(value = "orderBy", defaultValue = "name", required = false) String orderBy,
			@RequestParam(value = "orderDir", defaultValue = "asc", required = false) String orderDir,
			@RequestParam(value = "offset", defaultValue = "0", required = false) Integer offset,
			@RequestParam(value = "count", defaultValue = "20", required = false) Integer count) {

		Map<String, Object> model = new HashMap<String, Object>();

		Query<Question> query = OfyService.ofy().load().type(Question.class);
		// int totalQuestionCount = query.count();
		//
		// int originalCount = count;
		// if (offset < 0) {
		// offset = 0;
		// }
		// if (offset > 0) {
		// query = query.offset(offset);
		// }
		// if (count == 0) {
		// count = totalQuestionCount;
		// }
		// if (count >= totalQuestionCount) {
		// offset = 0;
		// count = totalQuestionCount;
		// }
		// if (offset + count > totalQuestionCount) {
		// count = totalQuestionCount - offset;
		// }
		// if (count > 0) {
		// query = query.limit(count);
		// }
		// if (orderBy != null && orderDir != null) {
		// if (orderDir.equals("asc")) {
		// query = query.order(orderBy);
		// } else {
		// query = query.order("-" + orderBy);
		// }
		// }
		List<Question> questionsList = query.list();
		// if (questionsList.size() == 0 && offset == 0
		// && Math.min(count + offset, totalQuestionCount) == 0) {
		// offset = -1;
		// count += 1;
		// }
		model.put("resources", questionsList);
		// model.put("total", totalQuestionCount);
		// model.put("from", offset);
		// model.put("to", Math.min(count + offset, totalQuestionCount));

		// model.put("count", originalCount);
		List<Category> categories = OfyService.ofy().load().type(Category.class).list();

		Collections.sort(categories, new Comparator<Category>() {

			public int compare(Category c1, Category c2) {

				if (c1.getOrder() >= c2.getOrder()) {
					return 1;
				} else if (c1.getOrder() == c2.getOrder()) {
					return 0;
				} else {
					return -1;
				}
			}
		});
		model.put("categories", categories);
		checkOperationLock(model);

		return new ModelAndView("cms/get_categorize_questions", model);
	}

	@RequestMapping(value = "/data/category/create", method = RequestMethod.POST)
	public ModelAndView saveCreatedCategory(Category category) {
		int count = OfyService.ofy().load().type(Category.class).count();
		if (count < 5) {
			if (category.getName() != null && !category.getName().isEmpty()) {
				OfyService.ofy().save().entity(category).now();
			}
		}
		return new ModelAndView("redirect:/cms/data/category");

	}

	@RequestMapping(value = "/data/category/edit/{id}", method = RequestMethod.POST)
	public ModelAndView saveEditedCategory(@PathVariable("id") Long id, Category category) {
		Category oldCategory = OfyService.ofy().load().key(Key.create(Category.class, id)).getValue();
		if (oldCategory != null) {
			if (category.getName() != null && !category.getName().isEmpty()) {
				oldCategory.setName(category.getName());
				oldCategory.setGooglePlusOnly(category.isGooglePlusOnly());
				OfyService.ofy().save().entity(oldCategory).now();
			}
		}
		return new ModelAndView("redirect:/cms/data/category");
	}

	@RequestMapping(value = "/data/category/delete/{id}", method = RequestMethod.POST)
	public ModelAndView deleteCategory(@PathVariable("id") Long id) {
		Category oldCategory = OfyService.ofy().load().key(Key.create(Category.class, id)).getValue();

		if (oldCategory != null) {

			List<Question> questions = OfyService.ofy().load().type(Question.class)
					.filter("category", Key.create(oldCategory)).list();
			for (Question question : questions) {
				question.setCategory(null);
			}
			OfyService.ofy().save().entities(questions).now();
			OfyService.ofy().delete().entity(oldCategory).now();
		}
		return new ModelAndView("redirect:/cms/data/category");
	}

	@RequestMapping(value = "/data/category/edit/{id}", method = RequestMethod.GET)
	public ModelAndView getEditCategory(@PathVariable("id") Long id) {
		Category oldCategory = OfyService.ofy().load().key(Key.create(Category.class, id)).getValue();
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("resource", oldCategory);
		return new ModelAndView("cms/edit_category", model);
	}

	@RequestMapping(value = "/data/question/categorize", method = RequestMethod.POST)
	public ModelAndView submitQuestionsCategories(HttpServletRequest request) {

		List<Question> questions = OfyService.ofy().load().type(Question.class).list();

		Collections.sort(questions, new Comparator<Question>() {

			public int compare(Question q1, Question q2) {
				return q1.getQuestionText().compareTo(q2.getQuestionText());
			}
		});

		List<Category> categories = OfyService.ofy().load().type(Category.class).list();
		List<Long> categoriesIds = new ArrayList<Long>();

		for (Category category : categories) {
			categoriesIds.add(category.getId());
		}
		for (Question question : questions) {
			String param = request.getParameter("" + question.getId());
			if (param != null && !param.isEmpty()) {
				try {
					Long id = Long.parseLong(param);
					if (categoriesIds.contains(id)) {
						question.setCategory(Key.create(Category.class, id));
					} else {
						question.setCategory(null);
					}
				} catch (Exception e) {

				}
			}
		}

		OfyService.ofy().save().entities(questions).now();

		return new ModelAndView("redirect:/cms/data/question");
	}

	@RequestMapping(value = "/data/category/order", method = RequestMethod.GET)
	public ModelAndView getCategoryOrder() {

		Query<Category> query = OfyService.ofy().load().type(Category.class);

		List<Category> categoriesList = query.list();

		Collections.sort(categoriesList, new Comparator<Category>() {

			public int compare(Category c1, Category c2) {

				if (c1.getOrder() >= c2.getOrder()) {
					return 1;
				} else if (c1.getOrder() == c2.getOrder()) {
					return 0;
				} else {
					return -1;
				}
			}
		});

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("resources", categoriesList);

		checkOperationLock(model);
		return new ModelAndView("cms/get_categories_order", model);
	}

	@RequestMapping(value = "/data/category/order", method = RequestMethod.POST)
	public ModelAndView postCategoryOrder(HttpServletRequest request) throws IOException {

		ModelAndView blockOperation = blockIfOperationLock();
		if (blockOperation != null) {
			return blockOperation;
		}

		List<Category> Categoris = OfyService.ofy().load().type(Category.class).list();
		Map<Long, Category> categoryById = new HashMap<Long, Category>();
		for (Category category : Categoris) {
			categoryById.put(category.getId(), category);
		}
		for (Long categoryId : categoryById.keySet()) {
			String requestParam = request.getParameter(categoryId + "");
			if (requestParam != null && !requestParam.isEmpty()) {
				try {
					int order = Integer.parseInt(requestParam);
					categoryById.get(categoryId).setOrder(order);

				} catch (Exception e) {

				}
			}
		}
		OfyService.ofy().save().entities(categoryById.values()).now();
		return new ModelAndView("redirect:/cms/data/category");
	}

	/********************************/
	/* ___________Brands__________ */
	/******************************/

	@RequestMapping("/data/brand")
	public ModelAndView getBrands(
			@RequestParam(value = "orderBy", required = false, defaultValue = "name") String orderBy,
			@RequestParam(value = "orderDir", required = false, defaultValue = "asc") String orderDir,
			@RequestParam(value = "offset", defaultValue = "0", required = false) Integer offset,
			@RequestParam(value = "count", defaultValue = "20", required = false) Integer count) {

		Query<Brand> query = OfyService.ofy().load().type(Brand.class);
		int totalBrandCount = query.count();

		int originalCount = count;
		if (offset < 0) {
			offset = 0;
		}
		if (count == 0) {
			count = totalBrandCount;

		}
		if (count >= totalBrandCount) {
			offset = 0;
			count = totalBrandCount;
		}
		if (offset + count > totalBrandCount) {
			count = totalBrandCount - offset;
		}
		if (offset > 0) {
			query = query.offset(offset);
		}

		if (count > 0) {
			query = query.limit(count);
		}

		if (orderBy != null && orderDir != null) {
			if (orderDir.equals("asc")) {
				query = query.order(orderBy);
			} else {
				query = query.order("-" + orderBy);
			}

		}
		List<Brand> brandsList = query.list();

		if (brandsList.size() == 0 && offset == 0 && Math.min(count + offset, totalBrandCount) == 0) {
			offset = -1;
			count += 1;
		}
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("resources", brandsList);
		model.put("total", totalBrandCount);
		model.put("from", offset);
		model.put("count", originalCount);
		model.put("to", Math.min(count + offset, totalBrandCount));

		checkOperationLock(model);
		return new ModelAndView("cms/get_brands", model);
	}

	/********************************/
	/* ___________Filters_________ */
	/******************************/

	@RequestMapping("/data/filter")
	public ModelAndView getFilters(
			@RequestParam(value = "orderBy", defaultValue = "order", required = false) String orderBy,
			@RequestParam(value = "orderDir", required = false, defaultValue = "asc") String orderDir,
			@RequestParam(value = "offset", defaultValue = "0", required = false) Integer offset,
			@RequestParam(value = "count", defaultValue = "20", required = false) Integer count) {
		Query<Filter> query = OfyService.ofy().load().type(Filter.class);
		int totalFilterCount = query.count();
		int originalCount = count;
		if (offset < 0) {
			offset = 0;
		}
		if (count == 0) {
			count = totalFilterCount;
		}
		if (count >= totalFilterCount) {
			offset = 0;
			count = totalFilterCount;
		}
		if (offset + count > totalFilterCount) {
			count = totalFilterCount - offset;
		}
		if (offset > 0) {
			query = query.offset(offset);
		}

		if (count > 0) {
			query = query.limit(count);
		}

		if (orderBy != null && orderDir != null) {
			if (orderDir.equals("asc")) {
				query = query.order(orderBy);
			} else {
				query = query.order("-" + orderBy);
			}

		}
		List<Filter> filtersList = query.list();
		Collections.sort(filtersList);
		if (filtersList.size() == 0 && offset == 0 && Math.min(count + offset, totalFilterCount) == 0) {
			offset = -1;
			count += 1;
		}
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("resources", filtersList);
		model.put("total", totalFilterCount);
		model.put("from", offset);
		model.put("to", Math.min(count + offset, totalFilterCount));
		model.put("count", originalCount);

		checkOperationLock(model);
		return new ModelAndView("cms/get_filters", model);
	}

	@RequestMapping(value = "/data/filter/order", method = RequestMethod.GET)
	public ModelAndView getFiltersOrder() {

		Query<Filter> query = OfyService.ofy().load().type(Filter.class);

		List<Filter> filtersList = query.list();

		Collections.sort(filtersList, new Comparator<Filter>() {

			public int compare(Filter f1, Filter f2) {
				return f1.getOrder().compareTo(f2.getOrder());
			}
		});

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("resources", filtersList);

		checkOperationLock(model);
		return new ModelAndView("cms/get_filters_order", model);
	}

	@RequestMapping(value = "/data/filter/order", method = RequestMethod.POST)
	public ModelAndView postFiltersOrder(ResourceOrderCollection filtersOrder) throws IOException {

		ModelAndView blockOperation = blockIfOperationLock();
		if (blockOperation != null) {
			return blockOperation;
		}

		logger.info(filtersOrder.toString());

		Query<Filter> query = OfyService.ofy().load().type(Filter.class);

		List<Filter> filtersList = query.list();

		for (Filter filter : filtersList) {
			filter.setOrder(filtersOrder.getRom().get(filter.getId()));
		}

		OfyService.ofy().save().entities(filtersList).now();

		return new ModelAndView("redirect:/cms/data/filter");
	}

	@RequestMapping(value = "/data/filter/edit/{id}", method = RequestMethod.GET)
	public ModelAndView editFilter(@PathVariable("id") Long id) {
		Filter oldFilter = OfyService.ofy().load().key(Key.create(Filter.class, id)).getValue();

		List<Key<Division>> divisionsKeys = oldFilter.getDivisions();

		ArrayList<Division> divisions = new ArrayList<Division>(OfyService.ofy().load().keys(divisionsKeys).values());

		List<Key<DataSet>> dataSetsKeys = oldFilter.getDataSets();
		List<DataSet> dataSets = new ArrayList<DataSet>(OfyService.ofy().load().keys(dataSetsKeys).values());

		Collections.sort(dataSets, new Comparator<DataSet>() {

			public int compare(DataSet d1, DataSet d2) {
				return d1.compareTo(d2);
			}
		});

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("resource", oldFilter);
		model.put("dataSets", dataSets);
		model.put("divisions", divisions);

		checkOperationLock(model);
		return new ModelAndView("cms/edit_filter", model);
	}

	@RequestMapping(value = "/data/filter/edit/{id}", method = RequestMethod.POST)
	public ModelAndView postEditFilter(@PathVariable("id") Long id, Filter filter) {
		Filter oldFilter = OfyService.ofy().load().key(Key.create(Filter.class, id)).getValue();

		if (filter.getName() != null && filter.getName().trim() != "") {
			oldFilter.setName(filter.getName());
			OfyService.ofy().save().entity(oldFilter).now();
		}

		return new ModelAndView("redirect:/cms/data/filter");
	}

	/*********************************/
	/* ___________DataSets_________ */
	/*******************************/

	@RequestMapping("/data/dataset")
	public ModelAndView getDatasets(
			@RequestParam(value = "orderBy", defaultValue = "name", required = false) String orderBy,
			@RequestParam(value = "orderDir", defaultValue = "asc", required = false) String orderDir,
			@RequestParam(value = "offset", defaultValue = "0", required = false) Integer offset,
			@RequestParam(value = "count", defaultValue = "20", required = false) Integer count) {
		Query<DataSet> query = OfyService.ofy().cache(false).load().type(DataSet.class);
		int totalDatasetCount = query.count();

		int originalCount = count;
		if (offset < 0) {
			offset = 0;
		}
		if (count == 0) {
			count = totalDatasetCount;
		}
		if (count >= totalDatasetCount) {
			offset = 0;
			count = totalDatasetCount;
		}
		if (offset + count > totalDatasetCount) {
			count = totalDatasetCount - offset;
		}
		if (offset > 0) {
			query = query.offset(offset);
		}

		if (count > 0) {
			query = query.limit(count);
		}

		if (orderBy != null && orderDir != null) {
			if (orderDir.equals("asc")) {
				query = query.order(orderBy);
			} else {
				query = query.order("-" + orderBy);
			}

		}
		List<DataSet> dataSetList = query.list();

		for (DataSet dataSet : dataSetList) {
			dataSet.setNumberOfFiles(OfyService.ofy().load().type(DataSheet.class)
					.filter("dataSet", Ref.create(Key.create(dataSet))).count());

			DataSheet dataSheet = OfyService.ofy().load().type(DataSheet.class)
					.filter("dataSet", Key.create(DataSet.class, dataSet.getId())).first().getValue();
			String currentAdmin = UserServiceFactory.getUserService().getCurrentUser().getEmail();

			if (dataSheet.getUploadedBy().equals(currentAdmin)) {
				dataSet.setOwner(true);
			} else {
				dataSet.setOwner(false);
			}
		}
		if (dataSetList.size() == 0 && offset == 0 && Math.min(count + offset, totalDatasetCount) == 0) {
			offset = -1;
			count += 1;
		}
		Map<String, Object> model = new HashMap<String, Object>();

		model.put("resources", dataSetList);
		model.put("total", totalDatasetCount);
		model.put("from", offset);
		model.put("to", Math.min(count + offset, totalDatasetCount));
		model.put("orderDir", orderDir);
		model.put("orderBy", orderBy);
		model.put("count", originalCount);

		checkOperationLock(model);

		return new ModelAndView("cms/get_datasets", model);
	}

	@RequestMapping(value = "/data/dataset/edit/{id}", method = RequestMethod.GET)
	public ModelAndView editDataset(@PathVariable("id") Long id,
			@RequestParam(value = "orderBy", defaultValue = "name", required = false) String orderBy,
			@RequestParam(value = "orderDir", defaultValue = "asc", required = false) String orderDir,
			@RequestParam(value = "offset", defaultValue = "0", required = false) Integer offset,
			@RequestParam(value = "count", defaultValue = "20", required = false) Integer count) {
		DataSet dataSet = DataSet.getDataSet(id);
		// List<DataSheet> dataSheetList = DataSheet.getDataSheetList(sortBy,
		// order, from, to);

		Integer limit = count - offset;
		if (orderBy == null)
			orderBy = "name";
		if (orderDir == null)
			orderDir = "asc";
		String sortOrder = (orderDir.equals("asc")) ? orderBy : "-" + orderBy;
		List<DataSheet> dataSheetList = ofy().load().type(DataSheet.class)
				.filter("dataSet", Key.create(DataSet.class, id)).limit(limit).order(sortOrder).offset(offset).list();

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("resource", dataSet);
		model.put("dataSheetResource", dataSheetList);

		checkOperationLock(model);
		return new ModelAndView("cms/edit_dataset", model);
	}

	@RequestMapping(value = "/data/dataset/edit/{id}", method = RequestMethod.POST)
	public ModelAndView editDataset(@PathVariable("id") Long id, DataSet dataset) throws IOException {

		ModelAndView blockOperation = blockIfOperationLock();
		if (blockOperation != null) {
			return blockOperation;
		}

		Map<String, Object> model = new HashMap<String, Object>();
		DataSet oldDataSet = OfyService.ofy().load().key(Key.create(DataSet.class, id)).getValue();
		oldDataSet.setName(dataset.getName());
		oldDataSet.setWaveDate(dataset.getWaveDate());
		oldDataSet.setDescription(dataset.getDescription());
		oldDataSet.setShowenInSnapshotOnly(dataset.isShowenInSnapshotOnly());
		oldDataSet.save();
		List<DataSheet> dataSheetList = DataSheet.getDataSheetList();
		model.put("resource", oldDataSet);
		model.put("dataSheetResource", dataSheetList);
		return new ModelAndView("redirect:/cms/data/dataset");
	}

	@RequestMapping(value = "/data/dataset/publish/{id}", method = RequestMethod.POST)
	public ModelAndView publishDataSet(@PathVariable("id") final Long id) throws IOException {

		ModelAndView blockOperation = blockIfOperationLock();
		if (blockOperation != null) {
			return blockOperation;
		}

		DataSet oldDataSet = OfyService.ofy().load().key(Key.create(DataSet.class, id)).getValue();

		DataSheet dataSheet = OfyService.ofy().load().type(DataSheet.class)
				.filter("dataSet", Key.create(DataSet.class, id)).first().getValue();

//		if (AuthorizationValidatior.checkAdminAuthority(dataSheet)) {
			logger.info("User have Authority to Publish");
			oldDataSet.setPublished(Boolean.TRUE);
			OfyService.ofy().save().entity(oldDataSet).now();
			return new ModelAndView("");
//		}
//		return null;
	}

	@RequestMapping(value = "/data/dataset/unpublish/{id}", method = RequestMethod.POST)
	public ModelAndView unpublishDataSet(@PathVariable("id") final Long id) throws IOException {

		ModelAndView blockOperation = blockIfOperationLock();
		if (blockOperation != null) {
			return blockOperation;
		}

		DataSet oldDataSet = OfyService.ofy().load().key(Key.create(DataSet.class, id)).getValue();
		oldDataSet.setPublished(Boolean.FALSE);
		OfyService.ofy().save().entity(oldDataSet).now();
		return new ModelAndView("");
	}

	/***************************************
	 * End Data
	 *******************************************/

	/* ______________________________________________________________________________________ */
	/*************************************** Users *******************************************/
	/* ____________________________________________________________________________________ */

	@RequestMapping("/user")
	public ModelAndView getUsers(
			@RequestParam(value = "orderBy", defaultValue = "email", required = false) String orderBy,
			@RequestParam(value = "orderDir", defaultValue = "asc", required = false) String orderDir,
			@RequestParam(value = "offset", defaultValue = "0", required = false) Integer offset,
			@RequestParam(value = "count", defaultValue = "20", required = false) Integer count) {
		Map<String, Object> model = new HashMap<String, Object>();

		Query<ApplicationUser> query = OfyService.ofy().load().type(ApplicationUser.class)
				.filter("onlyUser", Boolean.TRUE).order("authorities");
		int totalUserCount = query.count();
		int originalCount = count;
		if (offset < 0) {
			offset = 0;
		}
		if (count == 0) {
			count = totalUserCount;
		}
		if (count >= totalUserCount) {
			offset = 0;
			count = totalUserCount;
		}
		if (offset + count > totalUserCount) {
			count = totalUserCount - offset;
		}
		if (offset > 0) {
			query = query.offset(offset);
		}

		if (count > 0) {
			query = query.limit(count);
		}

		if (orderBy != null && orderDir != null) {
			if (orderDir.equals("asc")) {
				query = query.order(orderBy);
			} else {
				query = query.order("-" + orderBy);
			}

		}

		List<ApplicationUser> list = query.list();
		if (list.size() == 0 && offset == 0 && Math.min(count + offset, totalUserCount) == 0) {
			offset = -1;
			count += 1;
		}
		model.put("resources", list);
		model.put("total", totalUserCount);
		model.put("from", offset);
		model.put("to", Math.min(count + offset, totalUserCount));
		model.put("orderDir", orderDir);
		model.put("orderBy", orderBy);
		model.put("count", originalCount);
		return new ModelAndView("cms/get_users", model);
	}

	/***************************************
	 * End Users
	 *****************************************/

	/***************************************** Pages *********************************************/

	/*******************************************************************************************/

	@RequestMapping("/page")
	public ModelAndView getPages(
			@RequestParam(value = "orderBy", defaultValue = "name", required = false) String orderBy,
			@RequestParam(value = "orderDir", defaultValue = "asc", required = false) String orderDir,
			@RequestParam(value = "offset", defaultValue = "0", required = false) Integer offset,
			@RequestParam(value = "count", defaultValue = "20", required = false) Integer count) {
		Map<String, Object> model = new HashMap<String, Object>();
		Query<Page> query = OfyService.ofy().load().type(Page.class);
		int totalPages = query.count();
		int originalCount = count;
		if (offset < 0) {
			offset = 0;
		}
		if (count == 0) {
			count = totalPages;
		}
		if (count >= totalPages) {
			offset = 0;
			count = totalPages;
		}
		if (offset + count > totalPages) {
			count = totalPages - offset;
		}
		if (offset > 0) {
			query = query.offset(offset);
		}
		if (count > 0) {
			query = query.limit(count);
		}
		if (orderBy != null && orderDir != null) {
			if (orderDir.equals("asc")) {
				query = query.order(orderBy);
			} else {
				query = query.order("-" + orderBy);
			}
		}
		List<Page> list = query.list();
		if (list.size() == 0 && offset == 0 && Math.min(count + offset, totalPages) == 0) {
			offset = -1;
			count += 1;
		}
		model.put("resource", list);
		model.put("total", totalPages);
		model.put("from", offset);
		model.put("to", Math.min(count + offset, totalPages));
		model.put("orderDir", orderDir);
		model.put("orderBy", orderBy);
		model.put("count", originalCount);
		return new ModelAndView("cms/get_pages", model);

	}

	@RequestMapping(value = "/page/create", method = RequestMethod.GET)
	public ModelAndView createPage() {
		return new ModelAndView("cms/create_page");
	}

	@RequestMapping(value = "/page/create", method = RequestMethod.POST)
	public ModelAndView postPage(HttpServletRequest request, HttpServletResponse response) {

		Page newPage = new Page();
		newPage.setName(request.getParameter("name"));
		newPage.setTitle(request.getParameter("title"));
		newPage.setContent(new Text(request.getParameter("content")));
		newPage.setDescription(request.getParameter("description"));
		newPage.setKeywords(request.getParameter("keywords"));
		int order = 0;
		if (request.getParameter("order") == "" || request.getParameter("order") == null) {
			order = 0;
		} else {
			order = Integer.parseInt(request.getParameter("order"));
		}
		newPage.setOrder(order);

		OfyService.ofy().save().entity(newPage).now();
		return new ModelAndView("redirect:/cms/page");
	}

	@RequestMapping(value = "/page/edit/{id}", method = RequestMethod.GET)
	public ModelAndView editPages(@PathVariable("id") Long id) {
		Map<String, Object> model = new HashMap<String, Object>();

		Page resource = OfyService.ofy().load().key(Key.create(Page.class, id)).getValue();

		model.put("resource", resource);
		return new ModelAndView("cms/edit_page", model);

	}

	@RequestMapping(value = "/page/edit/{id}", method = RequestMethod.POST)
	public ModelAndView updatePage(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") Long id) {

		Page oldPage = OfyService.ofy().load().key(Key.create(Page.class, id)).getValue();
		oldPage.setTitle(request.getParameter("title"));
		oldPage.setName(request.getParameter("name"));
		oldPage.setContent(new Text(request.getParameter("content")));
		oldPage.setDescription(request.getParameter("description"));
		oldPage.setKeywords(request.getParameter("keywords"));
		oldPage.setOrder(Integer.parseInt((request.getParameter("order"))));

		OfyService.ofy().save().entity(oldPage).now();
		return new ModelAndView("redirect:/cms/page");
	}

	@RequestMapping(value = "/page/delete/{id}", method = RequestMethod.POST)
	public ModelAndView deletePage(@PathVariable("id") Long id) {
		Key<Page> page = Key.create(Page.class, id);
		if (page != null) {
			OfyService.ofy().delete().key(page).now();
		}

		return new ModelAndView("redirect:/cms/page");
	}

	/* ______________________________________________________________________________________ */
	/*************************************** Admins ******************************************/
	/* ____________________________________________________________________________________ */

	@RequestMapping("/admin")
	public ModelAndView getAdmins(
			@RequestParam(value = "orderBy", defaultValue = "firstName", required = false) String orderBy,
			@RequestParam(value = "orderDir", defaultValue = "asc", required = false) String orderDir,
			@RequestParam(value = "offset", defaultValue = "0", required = false) Integer offset,
			@RequestParam(value = "count", defaultValue = "20", required = false) Integer count) {
		Map<String, Object> model = new HashMap<String, Object>();
		Set<String> roles = new HashSet<String>();
		roles.add(ApplicationUserRole.ROLE_ADMIN.getAuthority());
		roles.add(ApplicationUserRole.ROLE_RESEARCHER.getAuthority());

		Query<ApplicationUser> query = OfyService.ofy().load().type(ApplicationUser.class)
				.filter("onlyUser", Boolean.FALSE).filter("superUser", Boolean.FALSE);
		int totalUserCount = query.count();
		int originalCount = count;
		if (offset < 0) {
			offset = 0;
		}
		if (count == 0) {
			count = totalUserCount;
		}
		if (count >= totalUserCount) {
			offset = 0;
			count = totalUserCount;
		}
		if (offset + count > totalUserCount) {
			count = totalUserCount - offset;
		}
		if (offset > 0) {
			query = query.offset(offset);
		}
		if (count > 0) {
			query = query.limit(count);
		}
		if (orderBy != null && orderDir != null) {
			if (orderDir.equals("asc")) {
				query = query.order(orderBy);
			} else {
				query = query.order("-" + orderBy);
			}
		}
		List<ApplicationUser> list = query.list();
		// Just for testing xml marshalling marshallable to be put upon request
		MarshallableList<ApplicationUser> mlist = new MarshallableList<ApplicationUser>(list);
		if (list.size() == 0 && offset == 0 && Math.min(count + offset, totalUserCount) == 0) {
			offset = -1;
			count += 1;
		}
		model.put("resources", mlist);
		model.put("total", totalUserCount);
		model.put("from", offset);
		model.put("to", Math.min(count + offset, totalUserCount));
		model.put("orderDir", orderDir);
		model.put("orderBy", orderBy);
		model.put("count", originalCount);
		return new ModelAndView("cms/get_admins", model);
	}

	@RequestMapping(value = "/user/create", method = RequestMethod.GET)
	public ModelAndView createUser() {
		return new ModelAndView("cms/create_user");
	}

	@RequestMapping(value = "/user/create", method = RequestMethod.POST)
	public ModelAndView postUser(ApplicationUser applicationUser) throws Exception {

		ApplicationUser oldUser = null;
		// Pattern gmailPattern = Pattern.compile(".*@gmail\\.com");
		Pattern mailPattern = Pattern.compile(".*@*\\.com");

		boolean match = false;

		// check if user already exists
		if (applicationUser.getEmail() != null) {
			// Matcher gmailMatcher = gmailPattern.matcher(applicationUser
			// .getEmail());
			//
			// if (gmailMatcher.matches()) {
			// match = true;
			// }
			//

			Matcher mailMatcher = mailPattern.matcher(applicationUser.getEmail());

			if (mailMatcher.matches()) {
				match = true;
			}

			if (match) {
				applicationUser.setEmail(applicationUser.getEmail().trim().toLowerCase());
				oldUser = OfyService.ofy().load().type(ApplicationUser.class)
						.filter("email", applicationUser.getEmail().toLowerCase().trim()).first().getValue();
				if (oldUser != null) {
					// checks if whether the existing user is an admin or a user
					if (oldUser.isOnlyUser()) {
						oldUser.setFirstName(applicationUser.getFirstName());
						oldUser.setLastName(applicationUser.getLastName());
						oldUser.setOnlyUser(true);
						applicationUser = oldUser;
					} else {
						throw new CustomException("Email already exists for another user");
					}
				}

				Set<String> authorities = new HashSet<String>();

				authorities.add(ApplicationUserRole.ROLE_USER.getAuthority());
				applicationUser.setAuthorities(authorities);
				applicationUser.setType(ApplicationUserType.USER.getType());
				applicationUser.setOnlyUser(true);
				applicationUser.setAuthorities(authorities);

				OfyService.ofy().save().entity(applicationUser).now();
				Queue queue = QueueFactory.getDefaultQueue();
				queue.add(TaskOptions.Builder.withUrl("/emailmessage/sendmail")
						.param("type", EmailTypes.ADDED_USER.getBit() + "")
						.param("userId", applicationUser.getId() + "")
						.retryOptions(RetryOptions.Builder.withTaskRetryLimit(10)));

			} else {
				throw new CustomException("Malformed data");
			}
		} else {
			throw new CustomException("Malformed data");
		}

		return new ModelAndView("redirect:/cms/user");

	}

	@RequestMapping(value = "/user/delete/{id}", method = RequestMethod.POST)
	public ModelAndView deleteUser(@PathVariable("id") Long id) {
		Key<ApplicationUser> user = Key.create(ApplicationUser.class, id);
		if (user != null) {
			OfyService.ofy().delete().key(user).now();
		}

		return new ModelAndView("redirect:/cms/user");
	}

	@RequestMapping(value = "/user/edit/{id}", method = RequestMethod.GET)
	public ModelAndView editUser(@PathVariable("id") Long id) {
		Map<String, Object> model = new HashMap<String, Object>();

		ApplicationUser resource = OfyService.ofy().load().key(Key.create(ApplicationUser.class, id)).getValue();

		// if (resource != null) {
		// model.put("roleValidationFn", "gmail");
		// }
		model.put("resource", resource);
		return new ModelAndView("cms/edit_user", model);

	}

	@RequestMapping(value = "/user/edit/{id}", method = RequestMethod.POST)
	public ModelAndView updateUser(@PathVariable("id") Long id, ApplicationUser applicationUser) throws Exception {
		int size = OfyService.ofy().load().type(ApplicationUser.class)
				.filter("email", applicationUser.getEmail().trim().toLowerCase()).filter("id !=", id).list().size();
		if (size > 0) {
			throw new CustomException("Email already exists for another user");
		}

		// Pattern gmailPattern = Pattern.compile(".*@gmail\\.com");

		Pattern mailPattern = Pattern.compile(".*@*\\.com");

		boolean match = false;

		// check if user already exists
		if (applicationUser.getEmail() != null) {

			// Matcher gmailMatcher = gmailPattern.matcher(applicationUser
			// .getEmail());
			//
			// if (gmailMatcher.matches()) {
			// match = true;
			// }
			//

			Matcher mailMatcher = mailPattern.matcher(applicationUser.getEmail());

			if (mailMatcher.matches()) {
				match = true;
			}

			if (match) {

				ApplicationUser oldUser = OfyService.ofy().load().key(Key.create(ApplicationUser.class, id)).getValue();

				oldUser.setFirstName(applicationUser.getFirstName());
				oldUser.setLastName(applicationUser.getLastName());
				oldUser.setEmail(applicationUser.getEmail().trim().toLowerCase());
//				oldUser.setCompany(applicationUser.getCompany());
				oldUser.setOnlyUser(true);
				applicationUser = oldUser;

				Set<String> authorities = new HashSet<String>();
				authorities.add(ApplicationUserRole.ROLE_USER.getAuthority());
				applicationUser.setType(ApplicationUserType.USER.getType());

				applicationUser.setAuthorities(authorities);

				OfyService.ofy().save().entity(applicationUser).now();
			} else {
				throw new CustomException("Malformed data");
			}
		} else {
			throw new CustomException("Malformed data");

		}

		return new ModelAndView("redirect:/cms/user");

	}

	@RequestMapping(value = "/admin/create", method = RequestMethod.GET)
	public ModelAndView createAdmin() {
		return new ModelAndView("cms/create_admin");
	}

	@RequestMapping(value = "/admin/create", method = RequestMethod.POST)
	public ModelAndView postAdmin(ApplicationUser applicationUser, @RequestParam("role") String role) throws Exception {
		String currentAdmin = UserServiceFactory.getUserService().getCurrentUser().getEmail();

		ApplicationUser oldUser = null;
		// Pattern googlePattern = Pattern.compile(".*@google\\.com");
		// Pattern gmailPattern = Pattern.compile(".*@gmail\\.com");

		Pattern mailPattern = Pattern.compile(".*@*\\.com");

		boolean match = false;

		// check if user already exists
		if (applicationUser.getEmail() != null) {
			// Matcher googleMatcher = googlePattern.matcher(applicationUser
			// .getEmail());
			// Matcher gmailMatcher = gmailPattern.matcher(applicationUser
			// .getEmail());

			Matcher mailMatcher = mailPattern.matcher(applicationUser.getEmail());

			// if
			// (role.equals(ApplicationUserRole.ROLE_RESEARCHER.getAuthority()))
			// {
			//
			// if (gmailMatcher.matches()) {
			// match = true;
			// }
			// } else if (role.equals(ApplicationUserRole.ROLE_ADMIN
			// .getAuthority())) {
			// if (googleMatcher.matches()) {
			// match = true;
			// }
			// }

			if (mailMatcher.matches()) {
				match = true;
			}

			if (match) {
				applicationUser.setEmail(applicationUser.getEmail().trim().toLowerCase());
				oldUser = OfyService.ofy().load().type(ApplicationUser.class)
						.filter("email", applicationUser.getEmail().toLowerCase().trim()).first().getValue();
				if (oldUser != null) {
					// checks if whether the existing user is an admin or a user
					if (oldUser.isOnlyUser()) {
						oldUser.setFirstName(applicationUser.getFirstName());
						oldUser.setLastName(applicationUser.getLastName());
						oldUser.setOnlyUser(false);
						applicationUser = oldUser;
					} else {
						throw new CustomException("Email already exists for another user");
					}
				}

				Set<String> authorities = new HashSet<String>();

				authorities.add(ApplicationUserRole.ROLE_USER.getAuthority());
				if (role.equals(ApplicationUserRole.ROLE_ADMIN.getAuthority())) {
					authorities.add(role);
					applicationUser.setAuthorities(authorities);
					applicationUser.setType(ApplicationUserType.ADMINISTRATOR.getType());
					applicationUser.setOnlyUser(Boolean.FALSE);
					applicationUser.setAuthorities(authorities);
					applicationUser.setAddedBy(currentAdmin);

					OfyService.ofy().save().entity(applicationUser).now();
					Queue queue = QueueFactory.getDefaultQueue();
					queue.add(TaskOptions.Builder.withUrl("/emailmessage/sendmail")
							.param("type", EmailTypes.ADDED_ADMIN.getBit() + "")
							.param("userId", applicationUser.getId() + "")
							.retryOptions(RetryOptions.Builder.withTaskRetryLimit(10)));

				} else if (role.equals(ApplicationUserRole.ROLE_RESEARCHER.getAuthority())) {
					authorities.add(role);
					applicationUser.setAuthorities(authorities);
					applicationUser.setType(ApplicationUserType.RESEARCHER.getType());
					applicationUser.setOnlyUser(Boolean.FALSE);
					applicationUser.setAuthorities(authorities);
					applicationUser.setAddedBy(currentAdmin);

					OfyService.ofy().save().entity(applicationUser).now();
					Queue queue = QueueFactory.getDefaultQueue();
					queue.add(TaskOptions.Builder.withUrl("/emailmessage/sendmail")
							.param("type", EmailTypes.ADDED_RESEARCHER.getBit() + "")
							.param("userId", applicationUser.getId() + "")
							.retryOptions(RetryOptions.Builder.withTaskRetryLimit(10)));
				}
			} else {
				throw new CustomException("Malformed data");
			}
		} else {
			throw new CustomException("Malformed data");
		}

		return new ModelAndView("redirect:/cms/admin");

	}

	@RequestMapping(value = "getNumberOfUsersAddedByAdmin")
	public int getNumberOfUsersAddedByAdmin() {
		String currentAdmin = UserServiceFactory.getUserService().getCurrentUser().getEmail();
		return OfyService.ofy().load().type(ApplicationUser.class).filter("addedBy", currentAdmin).list().size();
	}

	@RequestMapping(value = "/admin/edit/{id}", method = RequestMethod.GET)
	public ModelAndView editAdmin(@PathVariable("id") Long id) {
		Map<String, Object> model = new HashMap<String, Object>();

		ApplicationUser resource = OfyService.ofy().load().key(Key.create(ApplicationUser.class, id)).getValue();

		// if (resource != null) {
		// if (resource.isAdmin())
		// model.put("roleValidationFn", "google");
		// else if (resource.isResearcher())
		// model.put("roleValidationFn", "gmailandgoogle");
		// }
		model.put("resource", resource);
		return new ModelAndView("cms/edit_admin", model);

	}

	@RequestMapping(value = "/admin/edit/{id}", method = RequestMethod.POST)
	public ModelAndView updateAdmin(@PathVariable("id") Long id, ApplicationUser applicationUser,
			@RequestParam("role") String role) throws Exception {
		int size = OfyService.ofy().load().type(ApplicationUser.class)
				.filter("email", applicationUser.getEmail().trim().toLowerCase()).filter("id !=", id).list().size();
		if (size > 0) {
			throw new CustomException("Email already exists for another user");
		}

		// Pattern googlePattern = Pattern.compile(".*@google\\.com");
		// Pattern gmailPattern = Pattern.compile(".*@gmail\\.com");

		Pattern mailPattern = Pattern.compile(".*@*\\.com");

		boolean match = false;

		// check if user already exists
		if (applicationUser.getEmail() != null) {

			// Matcher googleMatcher = googlePattern.matcher(applicationUser
			// .getEmail());
			// Matcher gmailMatcher = gmailPattern.matcher(applicationUser
			// .getEmail());

			Matcher mailMatcher = mailPattern.matcher(applicationUser.getEmail());

			// if
			// (role.equals(ApplicationUserRole.ROLE_RESEARCHER.getAuthority()))
			// {
			//
			// if (gmailMatcher.matches()) {
			// match = true;
			// }
			// } else if (role.equals(ApplicationUserRole.ROLE_ADMIN
			// .getAuthority())) {
			// if (googleMatcher.matches()) {
			// match = true;
			// }
			// }

			if (mailMatcher.matches()) {
				match = true;
			}
			if (match) {

				ApplicationUser oldUser = OfyService.ofy().load().key(Key.create(ApplicationUser.class, id)).getValue();

				oldUser.setFirstName(applicationUser.getFirstName());
				oldUser.setLastName(applicationUser.getLastName());
				oldUser.setEmail(applicationUser.getEmail().trim().toLowerCase());
//				oldUser.setCompany(applicationUser.getCompany());
				oldUser.setOnlyUser(false);
				applicationUser = oldUser;

				Set<String> authorities = new HashSet<String>();
				authorities.add(ApplicationUserRole.ROLE_USER.getAuthority());
				if (role.equals(ApplicationUserRole.ROLE_ADMIN.getAuthority())) {
					authorities.add(role);
					applicationUser.setAuthorities(authorities);
					applicationUser.setType(ApplicationUserType.ADMINISTRATOR.getType());
				} else if (role.equals(ApplicationUserRole.ROLE_RESEARCHER.getAuthority())) {
					authorities.add(role);
					applicationUser.setAuthorities(authorities);
					applicationUser.setType(ApplicationUserType.RESEARCHER.getType());
				}

				applicationUser.setAuthorities(authorities);

				OfyService.ofy().save().entity(applicationUser).now();
			} else {
				throw new CustomException("Malformed data");
			}
		} else {
			throw new CustomException("Malformed data");

		}

		return new ModelAndView("redirect:/cms/admin");

	}

	@RequestMapping(value = "/admin/deactivate/{id}", method = RequestMethod.POST)
	public ModelAndView deactivateAdmin(@PathVariable("id") Long id) {
		ApplicationUser user = ApplicationUser.getApplicationUser(id);
		Set<String> roles = new HashSet<String>();
		roles.add(ApplicationUserRole.ROLE_USER.getAuthority());
		user.setAuthorities(roles);
		user.setOnlyUser(true);
		user.setType("");
		user.save();
		return new ModelAndView("redirect:/cms/admin");
	}

	/***************************************
	 * End Admins
	 *****************************************/

	/* _______________________________________________________________________________________ */
	/*************************************** Settings *****************************************/
	/* _____________________________________________________________________________________ */

	/* ____Locations___ */

	@RequestMapping("/settings")
	public ModelAndView getLocationsMain() {
		return new ModelAndView("redirect:/cms/settings/location");
	}

	@RequestMapping("settings/country")
	public ModelAndView getCountries(
			@RequestParam(value = "orderBy", defaultValue = "name", required = false) String orderBy,
			@RequestParam(value = "orderDir", defaultValue = "asc", required = false) String orderDir,
			@RequestParam(value = "offset", defaultValue = "0", required = false) Integer offset,
			@RequestParam(value = "count", defaultValue = "20", required = false) Integer count) {

		Map<String, Object> model = new HashMap<String, Object>();

		Query<Location> query = OfyService.ofy().load().type(Location.class).filter("isRegion", Boolean.FALSE);
		int totalLocationCount = query.count();
		int originalCount = count;
		if (offset < 0) {
			offset = 0;
		}
		if (count == 0) {
			count = totalLocationCount;
		}
		if (count >= totalLocationCount) {
			offset = 0;
			count = totalLocationCount;
		}
		if (offset + count > totalLocationCount) {
			count = totalLocationCount - offset;
		}
		if (offset > 0) {
			query = query.offset(offset);
		}
		if (count > 0) {
			query = query.limit(count);
		}
		if (orderBy != null && orderDir != null) {
			if (orderDir.equals("asc")) {
				query = query.order(orderBy);
			} else {
				query = query.order("-" + orderBy);
			}
		}
		List<Location> resources = query.list();

		for (Location location : resources) {
			if (location.getRegion() != null) {
				Region region = Region.getRegion(location.getRegion().getId());
				location.setRegionObj(region);
			}
		}
		if (resources.size() == 0 && offset == 0 && Math.min(count + offset, totalLocationCount) == 0) {
			offset = -1;
			count += 1;
		}
		model.put("resources", resources);
		model.put("total", totalLocationCount);
		model.put("from", offset);
		model.put("to", Math.min(count + offset, totalLocationCount));
		model.put("orderDir", orderDir);
		model.put("orderBy", orderBy);
		model.put("count", originalCount);

		checkOperationLock(model);

		return new ModelAndView("cms/get_countries", model);

	}

	@RequestMapping("settings/setasregion")
	public ModelAndView SetAsRegion() {

		Map<String, Object> model = new HashMap<String, Object>();

		List<Location> resources = OfyService.ofy().load().type(Location.class).list();

		model.put("resources", resources);

		return new ModelAndView("cms/get_locations", model);

	}

	@RequestMapping("settings/region")
	public ModelAndView getRegions(
			@RequestParam(value = "orderBy", defaultValue = "name", required = false) String orderBy,
			@RequestParam(value = "orderDir", defaultValue = "asc", required = false) String orderDir,
			@RequestParam(value = "offset", defaultValue = "0", required = false) Integer offset,
			@RequestParam(value = "count", defaultValue = "20", required = false) Integer count) {

		Map<String, Object> model = new HashMap<String, Object>();

		Query<Region> query = OfyService.ofy().load().type(Region.class);
		int totalRegionCount = query.count();

		int originalCount = count;
		if (offset < 0) {
			offset = 0;
		}
		if (count == 0) {
			count = totalRegionCount;
		}
		if (count >= totalRegionCount) {
			offset = 0;
			count = totalRegionCount;
		}
		if (offset + count > totalRegionCount) {
			count = totalRegionCount - offset;
		}
		if (offset > 0) {
			query = query.offset(offset);
		}
		if (count > 0) {
			query = query.limit(count);
		}
		if (orderBy != null && orderDir != null) {
			if (orderDir.equals("asc")) {
				query = query.order(orderBy);
			} else {
				query = query.order("-" + orderBy);
			}
		}

		List<Region> resources = query.list();
		if (resources.size() == 0 && offset == 0 && Math.min(count + offset, totalRegionCount) == 0) {
			offset = -1;
			count += 1;
		}
		model.put("resources", resources);
		model.put("total", totalRegionCount);
		model.put("from", offset);
		model.put("to", Math.min(count + offset, totalRegionCount));

		model.put("count", originalCount);
		checkOperationLock(model);
		return new ModelAndView("cms/get_regions", model);

	}

	@RequestMapping("settings/region/show/{id}")
	public ModelAndView getEditRegion(@PathVariable("id") Long id) throws IOException {

		ModelAndView blockOperation = blockIfOperationLock();
		if (blockOperation != null) {
			return blockOperation;
		}

		List<Location> list = ofy().load().type(Location.class).filter("isRegion", Boolean.FALSE).list();
		Collections.sort(list);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("id", id);
		model.put("resources", list);
		return new ModelAndView("cms/edit_region", model);
	}

	@RequestMapping(value = "settings/region/edit/{id}", method = RequestMethod.POST)
	public ModelAndView editRegion(@PathVariable("id") Long id, @RequestParam("locList") List<Long> locs)
			throws IOException {

		ModelAndView blockOperation = blockIfOperationLock();
		if (blockOperation != null) {
			return blockOperation;
		}

		Region reg = Region.getRegion(id);
		if (locs != null && locs.size() > 0) {
			// clear selected
			List<Location> oldAssignedList = ofy().load().type(Location.class).filter("isRegion", Boolean.FALSE)
					.filter("region", Key.create(Region.class, id)).list();
			for (Location old : oldAssignedList) {
				old.setRegion(null);
			}
			ofy().save().entities(oldAssignedList).now();
			List<Location> selectedLocs = new ArrayList<Location>();
			List<Key<Location>> locKeyList = new ArrayList<Key<Location>>();
			for (Long loc : locs) {
				Location location = Location.getLocation(loc);
				location.setRegion(Key.create(Region.class, id));
				selectedLocs.add(location);
				locKeyList.add(Key.create(Location.class, loc));
			}
			reg.setLocations(locKeyList);
			ofy().save().entities(selectedLocs).now();
		}
		reg.save();
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("resource", reg);
		return new ModelAndView("redirect:/cms/settings/region", model);
	}

	@RequestMapping("settings/location")
	public ModelAndView getLocations(
			@RequestParam(value = "orderBy", defaultValue = "name", required = false) String orderBy,
			@RequestParam(value = "orderDir", defaultValue = "asc", required = false) String orderDir,
			@RequestParam(value = "offset", defaultValue = "0", required = false) Integer offset,
			@RequestParam(value = "count", defaultValue = "20", required = false) Integer count) {

		Map<String, Object> model = new HashMap<String, Object>();
		Query<Location> query = OfyService.ofy().load().type(Location.class);
		int totalLocationCount = query.count();

		int originalCount = count;
		if (offset < 0) {
			offset = 0;
		}
		if (count == 0) {
			count = totalLocationCount;
		}
		if (count >= totalLocationCount) {
			offset = 0;
			count = totalLocationCount;
		}
		if (offset + count > totalLocationCount) {
			count = totalLocationCount - offset;
		}
		if (offset > 0) {
			query = query.offset(offset);
		}
		if (count > 0) {
			query = query.limit(count);
		}
		if (orderBy != null && orderDir != null) {
			if (orderDir.equals("asc")) {
				query = query.order(orderBy);
			} else {
				query = query.order("-" + orderBy);
			}
		}

		List<Location> resources = query.list();
		if (resources.size() == 0 && offset == 0 && Math.min(count + offset, totalLocationCount) == 0) {
			offset = -1;
			count += 1;
		}
		model.put("resources", resources);
		model.put("total", totalLocationCount);
		model.put("from", offset);
		model.put("to", Math.min(count + offset, totalLocationCount));

		model.put("count", originalCount);
		checkOperationLock(model);

		return new ModelAndView("cms/get_locations", model);

	}

	@RequestMapping(value = "/defineasregion/{id}", method = RequestMethod.POST)
	public ModelAndView defineAsRegion(@PathVariable("id") Long id) throws IOException {

		ModelAndView blockOperation = blockIfOperationLock();
		if (blockOperation != null) {
			return blockOperation;
		}

		Location location = OfyService.ofy().load().key(Key.create(Location.class, id)).getValue();
		location.setIsRegion(Boolean.TRUE);
		OfyService.ofy().save().entity(location).now();
		Region region = new Region();
		region.setId(id);
		region.setName(location.getName());
		OfyService.ofy().save().entity(region).now();

		return new ModelAndView("redirect:/cms/settings/location");
	}

	@RequestMapping(value = "/defineascountry/{id}", method = RequestMethod.POST)
	public ModelAndView getdefineAsCountry(@PathVariable("id") Long id) throws IOException {

		ModelAndView blockOperation = blockIfOperationLock();
		if (blockOperation != null) {
			return blockOperation;
		}

		Location location = OfyService.ofy().load().key(Key.create(Location.class, id)).getValue();
		location.setIsRegion(Boolean.FALSE);
		OfyService.ofy().save().entity(location).now();

		Region region = OfyService.ofy().load().key(Key.create(Region.class, id)).getValue();
		OfyService.ofy().delete().entity(region).now();
		return new ModelAndView("redirect:/cms/settings/location");
	}

	/* **********Email*********************** */

	@RequestMapping("settings/email")
	public ModelAndView getEmail() {
		Settings setting = Settings.load();
		String email = "";
		if (setting != null && setting.getEmail() != null) {
			email = setting.getEmail();
		}
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("email", email);

		checkOperationLock(model);
		return new ModelAndView("cms/get_email", model);
	}

	@RequestMapping("settings/email/edit")
	public ModelAndView editEmail(@RequestParam(value = "email", required = true, defaultValue = "") String email)
			throws IOException {

		ModelAndView blockOperation = blockIfOperationLock();
		if (blockOperation != null) {
			return blockOperation;
		}

		Settings settings = Settings.load();
		if (settings == null) {
			settings = new Settings();
		}
		settings.setEmail(email);
		settings.save();
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("email", email);
		return new ModelAndView("redirect:/cms/settings/email", model);
	}

	/***************************************
	 * END Settings
	 ******************************************/

	/* __________________________________________________________________________________________ */
	/**************************************
	 * Rolling Back
	 *****************************************/
	/* ________________________________________________________________________________________ */

	@RequestMapping(value = "process/entry/rollbackdatasheet/{id}", method = RequestMethod.POST)
	public void extractEntry(HttpServletResponse resp, @PathVariable("id") Long key) throws IOException {

		Queue q = QueueFactory.getQueue("rolling-back-queue");

		q.add(TaskOptions.Builder.withUrl("/cms/process/extract/rollbackdatasheet/main/" + key).method(Method.GET)

				.header("Host", BackendServiceFactory.getBackendService().getBackendAddress("deleting-backend"))

				.retryOptions(RetryOptions.Builder.withTaskRetryLimit(0)));
		// rollBackDataSheet(key, resp);

		resp.setStatus(HttpServletResponse.SC_OK);
		logger.error("Ended Main Task");
	}

	@RequestMapping(value = "process/entry/rollbackdataset/{id}", method = RequestMethod.POST)
	public void mainRollBackDataSet(HttpServletResponse resp, @PathVariable("id") Long key) throws IOException {
		Queue q = QueueFactory.getQueue("rolling-back-queue");

		q.add(TaskOptions.Builder.withUrl("/cms/process/extract/rollbackdataset/main/" + key).method(Method.GET)

				.header("Host", BackendServiceFactory.getBackendService().getBackendAddress("deleting-backend"))

				.retryOptions(RetryOptions.Builder.withTaskRetryLimit(0)));

		resp.setStatus(HttpServletResponse.SC_OK);
		logger.error("Ended Main Task");

	}

	@RequestMapping("process/extract/rollbackdatasheet/main/{id}")
	public void rollBackDataSheet(@PathVariable("id") Long id, HttpServletResponse response) {
		Key<DataSheet> dataSheetKey = Key.create(DataSheet.class, id);

		DataOperation dataOperation = OfyService.ofy().load().type(DataOperation.class).first().getValue();
		if (dataOperation != null) {
			logger.error("BLOCKED OPERATION! LOCK!");
		} else {

			dataOperation = new DataOperation();
			dataOperation.setMessage(
					"The CMS is currently locked. Data is being removed from the system.\nPlease try again later.");
			logger.info("Lock check passed.");
			OfyService.ofy().save().entity(dataOperation).now();

			// get the dataSheetObject
			DataSheet dataSheet = OfyService.ofy().load().key(dataSheetKey).getValue();

			// get the parent DataSet and check if it has only this dataSheet
			DataSet dataSet = OfyService.ofy().load().key(dataSheet.getDatasetRef().getKey()).getValue();

			// now load all the dataSheets in this DataSet
			List<DataSheet> dataSheets = OfyService.ofy().load().type(DataSheet.class)
					.filter("dataSet", Ref.create(dataSet)).list();

			if (dataSheets.size() == 1) {
				dataSheet.setStatus(dataSheetStatus.ROLLINGBACK.getStatus());
				Queue q = QueueFactory.getQueue("rolling-back-chunk-queue");

				RetryOptions options = RetryOptions.Builder.withTaskRetryLimit(200);
				options.maxBackoffSeconds(900);
				options.maxDoublings(3);
				options.minBackoffSeconds(10);

				Queue queue = QueueFactory.getDefaultQueue();
				queue.add(TaskOptions.Builder.withUrl("/emailmessage/sendmail")
						.param("type", EmailTypes.ROLLBACK_STARTED.getBit() + "").param("datasheetId", id + "")
						.param("datasetId", dataSet.getId() + "")
						.retryOptions(RetryOptions.Builder.withTaskRetryLimit(10)));

				List<List<Key<Division>>> combinationList = generateCombinationsList(dataSheet);
				int backendInstanceIndex = 0;
				int backendIndex = 1;
				for (int i = 0; i < combinationList.size(); i += 50) {
					int end = i + 50;
					if (end > combinationList.size()) {
						end = combinationList.size();
					}
					q.add(TaskOptions.Builder.withUrl("/cms/process/extract/rollback/chunk").param("startindex", i + "")
							.param("endindex", end + "").param("datasetid", dataSet.getId() + "")
							.param("datasheetid", dataSheet.getId() + "").method(Method.GET)

							.header("Host", BackendServiceFactory.getBackendService()
									.getBackendAddress(("deleting-backend" + backendIndex), backendInstanceIndex))

							.retryOptions(options));

					backendInstanceIndex++;

					if (backendInstanceIndex == 20) {
						backendInstanceIndex = 0;
						backendIndex++;
					}

					if (backendIndex == 3) {
						backendIndex = 1;
					}

				} // end loop

				List<Long> dataSheetsIds = new ArrayList<Long>();
				for (DataSheet tempDataSheet : dataSheets) {
					dataSheetsIds.add(tempDataSheet.getId());
				}

				TaskOptions taskOptions = TaskOptions.Builder
						.withUrl("/cms/process/extract/rollback/deleteEverythingForOneDataSheetInDataSet")
						.param("dataSetId", dataSet.getId() + "").method(Method.GET)

						.header("Host",
								BackendServiceFactory.getBackendService().getBackendAddress(("deleting-backend"), 0))

						.retryOptions(options);

				for (Long dataSheetId : dataSheetsIds) {
					taskOptions.param("dataSheetsIds", dataSheetId + "");
				}

				q.add(taskOptions);

				// deleteEverythingForOneDataSheetInDataSet(dataSet.getId(),
				// dataSheetsIds);

			} else if (!dataSheets.isEmpty()) {

				dataSheets = new ArrayList<DataSheet>();
				dataSheets.add(dataSheet);

				Queue q = QueueFactory.getQueue("rolling-back-chunk-queue");

				Queue queue = QueueFactory.getDefaultQueue();

				RetryOptions options = RetryOptions.Builder.withTaskRetryLimit(200);
				options.maxBackoffSeconds(900);
				options.maxDoublings(3);
				options.minBackoffSeconds(10);

				queue.add(TaskOptions.Builder.withUrl("/emailmessage/sendmail")
						.param("type", EmailTypes.ROLLBACK_STARTED.getBit() + "").param("datasheetId", id + "")
						.retryOptions(RetryOptions.Builder.withTaskRetryLimit(10)));

				List<List<Key<Division>>> combinationList = generateCombinationsList(dataSheet);

				int backendInstanceIndex = 0;
				int backendIndex = 1;
				for (int i = 0; i < combinationList.size(); i += 50) {
					int end = i + 50;
					if (end > combinationList.size()) {
						end = combinationList.size();
					}
					q.add(TaskOptions.Builder.withUrl("/cms/process/extract/rollback/chunk").param("startindex", i + "")
							.param("endindex", end + "").param("datasetid", dataSet.getId() + "")
							.param("datasheetid", dataSheet.getId() + "").method(Method.GET)

							.header("Host", BackendServiceFactory.getBackendService()
									.getBackendAddress(("deleting-backend" + backendIndex), backendInstanceIndex))

							.retryOptions(options));

					backendInstanceIndex++;

					if (backendInstanceIndex == 20) {
						backendInstanceIndex = 0;
						backendIndex++;
					}

					if (backendIndex == 3) {
						backendIndex = 1;
					}
				}

				List<Long> dataSheetsIds = new ArrayList<Long>();
				for (DataSheet tempDataSheet : dataSheets) {
					dataSheetsIds.add(tempDataSheet.getId());
				}

				TaskOptions taskOptions = TaskOptions.Builder
						.withUrl("/cms/process/extract/rollback/deleteEverythingForDataSheet")
						.param("dataSetId", dataSet.getId() + "").method(Method.GET)

						.header("Host",
								BackendServiceFactory.getBackendService().getBackendAddress(("deleting-backend"), 0))

						.retryOptions(options);

				for (Long dataSheetId : dataSheetsIds) {
					taskOptions.param("dataSheetsIds", dataSheetId + "");
				}

				q.add(taskOptions);

				// deleteEverythingForDataSheet(dataSet.getId(), dataSheetsIds);
			}
		}
		response.setStatus(HttpServletResponse.SC_OK);

	}

	/**
	 * @param dataSheetsIds
	 */
	@RequestMapping("process/extract/rollback/deleteEverythingForOneDataSheetInDataSet")
	private void deleteEverythingForOneDataSheetInDataSet(@RequestParam("dataSetId") Long dataSetId,
			@RequestParam("dataSheetsIds") List<Long> dataSheetsIds, HttpServletResponse response) {

		Queue q = QueueFactory.getQueue("rolling-back-chunk-queue");

		int number = q.fetchStatistics().getNumTasks();

		if (number <= 1) {

			List<Key<DataSheet>> dataSheetsKeys = new ArrayList<Key<DataSheet>>();

			for (Long id : dataSheetsIds) {
				dataSheetsKeys.add(Key.create(DataSheet.class, id));
			}
			List<DataSheet> dataSheets = new ArrayList<DataSheet>(
					OfyService.ofy().load().keys(dataSheetsKeys).values());

			DataSet dataSetToBeRolledBack = OfyService.ofy().load().key(Key.create(DataSet.class, dataSetId))
					.getValue();

			rollBackBrandsPerDataSetPerLocationPerQuestion(dataSetId, dataSheetsIds);

			logger.error("Rolling Back BrandsPerDataSetPerLocationPerQuestion");

			rollBackQuestionsAndAnswers(dataSetId, dataSheetsIds);
			logger.error("Rolling Back QuestionsAndAnswers");
			rollBackFiltersAndDivisions(dataSetId, dataSheetsIds);
			logger.error("Rolling Back FiltersAndDivisions");
			rollBackBrands(dataSetId, dataSheetsIds);
			logger.error("Rolling Back Brands");

			dataSheets.get(0).setAnswers(new ArrayList<Key<Answer>>());
			dataSheets.get(0).setDatasetRef(null);
			dataSheets.get(0).setQuestions(new ArrayList<Key<Question>>());
			dataSheets.get(0).setFilters(new ArrayList<Key<Filter>>());
			dataSheets.get(0).setBrands(new ArrayList<Key<Brand>>());
			dataSheets.get(0).setDivisions(new ArrayList<Key<Division>>());
			dataSheets.get(0).setStatus(DataSheet.dataSheetStatus.UPLOADED.getStatus());

			OfyService.ofy().delete().entity(dataSetToBeRolledBack).now();
			OfyService.ofy().save().entity(dataSheets.get(0)).now();

			response.setStatus(HttpServletResponse.SC_OK);
			DataOperation dataOperation = OfyService.ofy().load().type(DataOperation.class).first().getValue();
			Queue queue = QueueFactory.getDefaultQueue();
			queue.add(TaskOptions.Builder.withUrl("/emailmessage/sendmail")
					.param("type", EmailTypes.ROLLBACK_ENDED.getBit() + "").param("datasetId", dataSetId + "")
					.retryOptions(RetryOptions.Builder.withTaskRetryLimit(10)));

			if (dataOperation != null) {
				OfyService.ofy().delete().entity(dataOperation).now();
			}
		} else {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
	}

	/**
	 * @param dataSheetsIds
	 */

	@RequestMapping("process/extract/rollback/deleteEverythingForDataSheet")
	private void deleteEverythingForDataSheet(@RequestParam("dataSetId") Long dataSetId,
			@RequestParam("dataSheetsIds") List<Long> dataSheetsIds, HttpServletResponse response) {

		Queue q = QueueFactory.getQueue("rolling-back-chunk-queue");

		int number = q.fetchStatistics().getNumTasks();

		if (number <= 1) {

			List<Key<DataSheet>> dataSheetsKeys = new ArrayList<Key<DataSheet>>();

			for (Long id : dataSheetsIds) {
				dataSheetsKeys.add(Key.create(DataSheet.class, id));
			}
			List<DataSheet> dataSheets = new ArrayList<DataSheet>(
					OfyService.ofy().load().keys(dataSheetsKeys).values());

			DataSet dataSetToBeRolledBack = OfyService.ofy().load().key(Key.create(DataSet.class, dataSetId))
					.getValue();

			rollBackBrandsPerDataSetPerLocationPerQuestion(dataSetId, dataSheetsIds);
			logger.error("Rolling Back BrandsPerDataSetPerLocationPerQuestion");
			rollBackQuestionsAndAnswers(new Long(-100), dataSheetsIds);
			logger.error("Rolling Back QuestionsAndAnswers");
			rollBackFiltersAndDivisions(new Long(-100), dataSheetsIds);
			logger.error("Rolling Back FiltersAndDivisions");
			rollBackBrands(new Long(-100), dataSheetsIds);
			logger.error("Rolling Back Brands");

			dataSheets.get(0).setAnswers(new ArrayList<Key<Answer>>());
			dataSheets.get(0).setDatasetRef(null);
			dataSheets.get(0).setQuestions(new ArrayList<Key<Question>>());
			dataSheets.get(0).setFilters(new ArrayList<Key<Filter>>());
			dataSheets.get(0).setBrands(new ArrayList<Key<Brand>>());
			dataSheets.get(0).setDivisions(new ArrayList<Key<Division>>());
			int index = dataSetToBeRolledBack.getLocations().indexOf(dataSheets.get(0).getLocation());
			if (index != -1) {
				dataSetToBeRolledBack.getLocations().remove(index);
			}
			dataSheets.get(0).setStatus(DataSheet.dataSheetStatus.UPLOADED.getStatus());

			OfyService.ofy().save().entity(dataSheets.get(0)).now();
			OfyService.ofy().save().entity(dataSetToBeRolledBack).now();
			response.setStatus(HttpServletResponse.SC_OK);

			DataOperation dataOperation = OfyService.ofy().load().type(DataOperation.class).first().getValue();

			Queue queue = QueueFactory.getDefaultQueue();
			queue.add(TaskOptions.Builder.withUrl("/emailmessage/sendmail")
					.param("type", EmailTypes.ROLLBACK_ENDED.getBit() + "")
					.param("datasheetId", dataSheetsIds.get(0) + "")
					.retryOptions(RetryOptions.Builder.withTaskRetryLimit(10)));

			if (dataOperation != null) {
				OfyService.ofy().delete().entity(dataOperation).now();
			}
		} else {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
	}

	@RequestMapping(value = "process/extract/rollbackdataset/main/{key}", method = RequestMethod.GET)
	public void rollBackDataSet(@PathVariable("key") Long key, HttpServletResponse response) {

		DataOperation dataOperation = OfyService.ofy().load().type(DataOperation.class).first().getValue();
		if (dataOperation != null) {
			logger.error("BLOCKED OPERATION! LOCK!");
		} else {

			dataOperation = new DataOperation();
			dataOperation.setMessage(
					"The CMS is currently locked. Data is being removed from the system.\nPlease try again later.");
			logger.info("Lock check passed.");

			OfyService.ofy().save().entity(dataOperation).now();
			DataSet dataSetToBeRolledBack = OfyService.ofy().load().key(Key.create(DataSet.class, key)).get();

			// now load all the dataSheets in this DataSet
			List<DataSheet> dataSheets = OfyService.ofy().load().type(DataSheet.class)
					.filter("dataSet", Ref.create(dataSetToBeRolledBack)).list();
			dataSetToBeRolledBack.setPublished(Boolean.FALSE);
			OfyService.ofy().save().entity(dataSetToBeRolledBack).now();

			Queue q = QueueFactory.getQueue("rolling-back-chunk-queue");

			RetryOptions options = RetryOptions.Builder.withTaskRetryLimit(200);
			options.maxBackoffSeconds(900);
			options.maxDoublings(3);
			options.minBackoffSeconds(10);

			Queue queue = QueueFactory.getDefaultQueue();
			queue.add(TaskOptions.Builder.withUrl("/emailmessage/sendmail")
					.param("type", EmailTypes.ROLLBACK_STARTED.getBit() + "").param("datasetId", key + "")
					.retryOptions(RetryOptions.Builder.withTaskRetryLimit(10)));

			logger.error("Distributing Tasks ... ");
			for (DataSheet dataSheet : dataSheets) {
				dataSheet.setStatus(dataSheetStatus.ROLLINGBACK.getStatus());
				Long dataSheetId = dataSheet.getId();
				List<List<Key<Division>>> combinationList = generateCombinationsList(dataSheet);
				int backendInstanceIndex = 0;
				int backendIndex = 1;
				for (int i = 0; i < combinationList.size(); i += 50) {
					int end = i + 50;
					if (end > combinationList.size()) {
						end = combinationList.size();
					}
					q.add(TaskOptions.Builder.withUrl("/cms/process/extract/rollback/chunk").param("startindex", i + "")
							.param("endindex", end + "").param("datasetid", key + "")
							.param("datasheetid", dataSheetId + "").method(Method.GET)

							.header("Host", BackendServiceFactory.getBackendService()
									.getBackendAddress(("deleting-backend" + backendIndex), backendInstanceIndex))

							.retryOptions(options));

					backendInstanceIndex++;

					if (backendInstanceIndex == 20) {
						backendInstanceIndex = 0;
						backendIndex++;
					}

					if (backendIndex == 3) {
						backendIndex = 1;
					}
				}

			}
			List<Long> dataSheetsIds = new ArrayList<Long>();
			for (DataSheet tempDataSheet : dataSheets) {
				dataSheetsIds.add(tempDataSheet.getId());
			}

			TaskOptions taskOptions = TaskOptions.Builder
					.withUrl("/cms/process/extract/rollback/deleteEverythingRollBackDataSet")
					.param("dataSetId", dataSetToBeRolledBack.getId() + "").method(Method.GET)

					.header("Host",
							BackendServiceFactory.getBackendService().getBackendAddress(("deleting-backend"), 0))

					.retryOptions(options);

			for (Long dataSheetId : dataSheetsIds) {
				taskOptions.param("dataSheetsIds", dataSheetId + "");
			}

			q.add(taskOptions);

			// deleteEverythingRollBackDataSet(dataSetToBeRolledBack.getId(),
			// dataSheetsIds);

			response.setStatus(HttpServletResponse.SC_ACCEPTED);
		}
	}

	/**
	 * @param dataSheetsIds
	 */

	@RequestMapping("process/extract/rollback/deleteEverythingRollBackDataSet")
	private void deleteEverythingRollBackDataSet(@RequestParam("dataSetId") Long dataSetId,
			@RequestParam("dataSheetsIds") List<Long> dataSheetsIds, HttpServletResponse response) {

		Queue q = QueueFactory.getQueue("rolling-back-chunk-queue");

		int number = q.fetchStatistics().getNumTasks();

		if (number <= 1) {

			List<Key<DataSheet>> dataSheetsKeys = new ArrayList<Key<DataSheet>>();

			for (Long id : dataSheetsIds) {
				dataSheetsKeys.add(Key.create(DataSheet.class, id));
			}
			List<DataSheet> dataSheets = new ArrayList<DataSheet>(
					OfyService.ofy().load().keys(dataSheetsKeys).values());

			DataSet dataSetToBeRolledBack = OfyService.ofy().load().key(Key.create(DataSet.class, dataSetId))
					.getValue();

			rollBackBrandsPerDataSetPerLocationPerQuestion(dataSetId, dataSheetsIds);
			logger.error("Rolling Back BrandsPerDataSetPerLocationPerQuestion");
			rollBackQuestionsAndAnswers(dataSetId, dataSheetsIds);
			logger.error("Rolling Back QuestionsAndAnswers");
			rollBackFiltersAndDivisions(dataSetId, dataSheetsIds);
			logger.error("Rolling Back FiltersAndDivisions");
			rollBackBrands(dataSetId, dataSheetsIds);
			logger.error("Rolling Back Brands");

			for (DataSheet dataSheet : dataSheets) {
				dataSheet.setAnswers(new ArrayList<Key<Answer>>());
				dataSheet.setDatasetRef(null);
				dataSheet.setQuestions(new ArrayList<Key<Question>>());
				dataSheet.setFilters(new ArrayList<Key<Filter>>());
				dataSheet.setBrands(new ArrayList<Key<Brand>>());
				dataSheet.setDivisions(new ArrayList<Key<Division>>());
				int index = dataSetToBeRolledBack.getLocations().indexOf(dataSheet.getLocation());
				dataSheet.setStatus(DataSheet.dataSheetStatus.UPLOADED.getStatus());
				if (index != -1) {
					dataSetToBeRolledBack.getLocations().remove(index);
				}
			}
			Queue queue = QueueFactory.getDefaultQueue();
			queue.add(TaskOptions.Builder.withUrl("/emailmessage/sendmail")
					.param("type", EmailTypes.ROLLBACK_ENDED.getBit() + "").param("datasetId", dataSetId + "")
					.retryOptions(RetryOptions.Builder.withTaskRetryLimit(10)));

			OfyService.ofy().delete().entity(dataSetToBeRolledBack).now();
			OfyService.ofy().save().entities(dataSheets).now();
			OfyService.ofy().delete().entity(dataSetToBeRolledBack).now();
			response.setStatus(HttpServletResponse.SC_OK);
			DataOperation dataOperation = OfyService.ofy().load().type(DataOperation.class).first().getValue();

			if (dataOperation != null) {
				OfyService.ofy().delete().entity(dataOperation).now();
			}
		} else {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
	}

	/**
	 * @param dataSetToBeRolledBack
	 * @param dataSetKey
	 */
	private void rollBackQuestionsAndAnswers(Long dataSetId, List<Long> dataSheetsIds) {

		List<Key<DataSheet>> dataSheetsKeys = new ArrayList<Key<DataSheet>>();

		for (Long id : dataSheetsIds) {
			dataSheetsKeys.add(Key.create(DataSheet.class, id));
		}
		List<DataSheet> dataSheetsToBeRolledBack = new ArrayList<DataSheet>(
				OfyService.ofy().load().keys(dataSheetsKeys).values());

		DataSet dataSetToBeRolledBack = OfyService.ofy().load().key(Key.create(DataSet.class, dataSetId)).getValue();
		Key<DataSet> dataSetKey = null;
		boolean rollBackDataSet = false;

		List<Question> questionsToBeDeleted = new ArrayList<Question>();

		List<Question> questionsToBeEdited = new ArrayList<Question>();

		List<Answer> answersToBeDeleted = new ArrayList<Answer>();

		List<Answer> rawAnswersToBeEdited = new ArrayList<Answer>();

		List<Answer> answersToBeEdited = new ArrayList<Answer>();

		if (dataSetToBeRolledBack != null) {
			rollBackDataSet = true;
			dataSetKey = Key.create(dataSetToBeRolledBack);
		}

		Set<Key<Question>> questionsKeys = new HashSet<Key<Question>>();

		for (DataSheet dataSheet : dataSheetsToBeRolledBack) {
			questionsKeys.addAll(dataSheet.getQuestions());
		}
		List<Question> candidateQuestions = new ArrayList<Question>();

		if (dataSetToBeRolledBack == null) {
			candidateQuestions = new ArrayList<Question>(OfyService.ofy().load().keys(questionsKeys).values());
		} else {
			candidateQuestions = new ArrayList<Question>(
					OfyService.ofy().load().keys(dataSetToBeRolledBack.getQuestions()).values());
		}

		/*
		 * get all questions in the dataSet and check if it only exists in the
		 * dataSet to be deleted or not
		 */
		for (Question question : candidateQuestions) {

			for (DataSheet dataSheet : dataSheetsToBeRolledBack) {
				if (question.getDatasheets().contains(Key.create(dataSheet))) {
					int index = question.getDatasheets().indexOf(Key.create(dataSheet));
					question.getDatasheets().remove(index);
				}
			}

			List<Answer> questionAnswers = OfyService.ofy().load().type(Answer.class).ancestor(question).list();

			if (rollBackDataSet) {
				if (question.getDataSets().size() == 1) {
					// set Question to be deleted
					questionsToBeDeleted.add(question);

					answersToBeDeleted.addAll(questionAnswers);
				} else {
					int index = question.getDataSets().indexOf(dataSetKey);
					question.getDataSets().remove(index);
				}
			} else {
				questionsToBeEdited.add(question);
				rawAnswersToBeEdited.addAll(questionAnswers);
			}
		}

		for (Answer answer : rawAnswersToBeEdited) {
			for (DataSheet dataSheet : dataSheetsToBeRolledBack) {
				if (answer.getDatasheets().contains(Key.create(dataSheet))) {
					int index = answer.getDatasheets().indexOf(Key.create(dataSheet));
					answer.getDatasheets().remove(index);
				}
			}

			if (rollBackDataSet) {
				if (answer.getDataSets().contains(dataSetKey)) {
					if (answer.getDataSets().size() == 1) {
						// set Question to be deleted
						answersToBeDeleted.add(answer);
					} else {
						int index = answer.getDataSets().indexOf(dataSetKey);
						answer.getDataSets().remove(index);
						answersToBeEdited.add(answer);
					}
				}
			} else {
				answersToBeEdited.add(answer);
			}
		}

		OfyService.ofy().delete().entities(answersToBeDeleted).now();
		OfyService.ofy().delete().entities(questionsToBeDeleted).now();
		detachQuestion(questionsToBeDeleted);
		OfyService.ofy().save().entities(answersToBeEdited).now();
		OfyService.ofy().save().entities(questionsToBeEdited).now();
	}

	private void rollBackBrandsPerDataSetPerLocationPerQuestion(Long dataSetId, List<Long> dataSheetsIds) {

		List<Key<DataSheet>> dataSheetsKeys = new ArrayList<Key<DataSheet>>();

		for (Long id : dataSheetsIds) {
			dataSheetsKeys.add(Key.create(DataSheet.class, id));
		}
		List<DataSheet> dataSheetsToBeRolledBack = new ArrayList<DataSheet>(
				OfyService.ofy().load().keys(dataSheetsKeys).values());

		for (DataSheet dataSheet : dataSheetsToBeRolledBack) {

			Long locationId = null;
			if (dataSheet.getLocation() != null) {
				locationId = dataSheet.getLocation().getId();
			} else {
				locationId = OfyService.ofy().load().type(Location.class).first().getValue().getId();

			}

			List<Key<Question>> questionsKeys = dataSheet.getQuestions();
			List<Key<BrandPerQuestionPerDataSetPerLocation>> brandPerQuestionPerDataSetPerLocationList = new ArrayList<Key<BrandPerQuestionPerDataSetPerLocation>>();
			for (Key<Question> questionKey : questionsKeys) {
				String key = "W" + dataSetId + "Q" + questionKey.getId() + "L" + locationId;
				brandPerQuestionPerDataSetPerLocationList
						.add(Key.create(BrandPerQuestionPerDataSetPerLocation.class, key));

			}
			OfyService.ofy().delete().keys(brandPerQuestionPerDataSetPerLocationList).now();
			OfyService.ofy().clear();
		}

	}

	private void rollBackFiltersAndDivisions(Long dataSetId, List<Long> dataSheetsIds) {

		List<Key<DataSheet>> dataSheetsKeys = new ArrayList<Key<DataSheet>>();

		for (Long id : dataSheetsIds) {
			dataSheetsKeys.add(Key.create(DataSheet.class, id));
		}
		List<DataSheet> dataSheetsToBeRolledBack = new ArrayList<DataSheet>(
				OfyService.ofy().load().keys(dataSheetsKeys).values());

		DataSet dataSetToBeRolledBack = OfyService.ofy().load().key(Key.create(DataSet.class, dataSetId)).getValue();

		Key<DataSet> dataSetKey = null;
		boolean rollBackDataSet = false;

		List<Filter> filtersToBeDeleted = new ArrayList<Filter>();

		List<Filter> filtersToBeEdited = new ArrayList<Filter>();

		List<Division> divisionsToBeDeleted = new ArrayList<Division>();

		List<Division> rawDivisionsToBeEdited = new ArrayList<Division>();

		List<Division> divisionsToBeEdited = new ArrayList<Division>();

		if (dataSetToBeRolledBack != null) {
			rollBackDataSet = true;
			dataSetKey = Key.create(dataSetToBeRolledBack);
		}

		Set<Key<Filter>> filtersKeys = new HashSet<Key<Filter>>();

		for (DataSheet dataSheet : dataSheetsToBeRolledBack) {
			filtersKeys.addAll(dataSheet.getFilters());
		}

		List<Filter> candidateFilters = new ArrayList<Filter>();
		if (dataSetToBeRolledBack == null) {
			candidateFilters = new ArrayList<Filter>(OfyService.ofy().load().keys(filtersKeys).values());
		} else {
			candidateFilters = new ArrayList<Filter>(
					OfyService.ofy().load().keys(dataSetToBeRolledBack.getFilters()).values());
		}
		/*
		 * get all filters in the dataSet and check if it only exists in the
		 * dataSet to be deleted or not
		 */
		for (Filter filter : candidateFilters) {

			for (DataSheet dataSheet : dataSheetsToBeRolledBack) {
				if (filter.getDatasheets().contains(Key.create(dataSheet))) {
					int index = filter.getDatasheets().indexOf(Key.create(dataSheet));
					filter.getDatasheets().remove(index);
				}
			}

			List<Division> filterDivisions = OfyService.ofy().load().type(Division.class).ancestor(filter).list();

			if (rollBackDataSet) {
				if (filter.getDataSets().size() == 1) {
					// set Filter to be deleted
					filtersToBeDeleted.add(filter);

					divisionsToBeDeleted.addAll(filterDivisions);
				} else {
					int index = filter.getDataSets().indexOf(dataSetKey);
					filter.getDataSets().remove(index);
					filtersToBeEdited.add(filter);
					rawDivisionsToBeEdited.addAll(filterDivisions);
				}
			} else {
				filtersToBeEdited.add(filter);
				rawDivisionsToBeEdited.addAll(filterDivisions);
			}
		}

		for (Division division : rawDivisionsToBeEdited) {

			for (DataSheet dataSheet : dataSheetsToBeRolledBack) {
				if (division.getDatasheets().contains(Key.create(dataSheet))) {
					int index = division.getDatasheets().indexOf(Key.create(dataSheet));
					division.getDatasheets().remove(index);
				}
			}

			if (rollBackDataSet) {
				if (division.getDataSets().contains(dataSetKey)) {
					if (division.getDataSets().size() == 1) {
						// set Question to be deleted
						divisionsToBeDeleted.add(division);
					} else {
						int index = division.getDataSets().indexOf(dataSetKey);
						division.getDataSets().remove(index);
						divisionsToBeEdited.add(division);
					}
				}
			} else {
				divisionsToBeEdited.add(division);
			}
		}

		OfyService.ofy().delete().entities(divisionsToBeDeleted).now();
		OfyService.ofy().delete().entities(filtersToBeDeleted).now();
		// System.out.println(filtersToBeDeleted);
		detachFilters(filtersToBeDeleted);
		OfyService.ofy().save().entities(divisionsToBeEdited).now();
		OfyService.ofy().save().entities(filtersToBeEdited).now();
	}

	private void rollBackBrands(Long dataSetId, List<Long> dataSheetsIds) {

		List<Key<DataSheet>> dataSheetsKeys = new ArrayList<Key<DataSheet>>();

		for (Long id : dataSheetsIds) {
			dataSheetsKeys.add(Key.create(DataSheet.class, id));
		}
		List<DataSheet> dataSheetsToBeRolledBack = new ArrayList<DataSheet>(
				OfyService.ofy().load().keys(dataSheetsKeys).values());

		DataSet dataSetToBeRolledBack = OfyService.ofy().load().key(Key.create(DataSet.class, dataSetId)).getValue();

		Key<DataSet> dataSetKey = null;
		boolean rollBackDataSet = false;

		List<Brand> brandsToBeDeleted = new ArrayList<Brand>();

		List<Brand> brandsToBeEdited = new ArrayList<Brand>();

		if (dataSetToBeRolledBack != null) {
			rollBackDataSet = true;
			dataSetKey = Key.create(dataSetToBeRolledBack);
		}

		Set<Key<Brand>> brandsKeys = new HashSet<Key<Brand>>();

		for (DataSheet dataSheet : dataSheetsToBeRolledBack) {
			brandsKeys.addAll(dataSheet.getBrands());
		}

		List<Brand> candidateBrands = new ArrayList<Brand>();

		if (dataSetToBeRolledBack == null) {
			candidateBrands = new ArrayList<Brand>(OfyService.ofy().load().keys(brandsKeys).values());
		} else {
			candidateBrands = new ArrayList<Brand>(
					OfyService.ofy().load().keys(dataSetToBeRolledBack.getBrands()).values());
		}

		/*
		 * get all filters in the dataSet and check if it only exists in the
		 * dataSet to be deleted or not
		 */
		System.out.println(candidateBrands);
		for (Brand brand : candidateBrands) {

			for (DataSheet dataSheet : dataSheetsToBeRolledBack) {
				if (brand.getDatasheets().contains(Key.create(dataSheet))) {
					int index = brand.getDatasheets().indexOf(Key.create(dataSheet));
					brand.getDatasheets().remove(index);
				}
			}

			if (rollBackDataSet) {
				if (brand.getDataSets().size() == 1) {
					// set Brand to be deleted
					brandsToBeDeleted.add(brand);

				} else {
					int index = brand.getDataSets().indexOf(dataSetKey);
					brand.getDataSets().remove(index);
					brandsToBeEdited.add(brand);

				}
			} else {
				brandsToBeEdited.add(brand);
			}
		}

		OfyService.ofy().delete().entities(brandsToBeDeleted).now();
		// System.out.println(brandsToBeDeleted);
		OfyService.ofy().save().entities(brandsToBeEdited).now();
	}

	// removes the reference of the deleted questions from all the datasets and
	// the datasheets
	public void detachQuestion(List<Question> questions) {
		if (!questions.isEmpty()) {
			ResourceList<Question> questionsResource = new ResourceList<Question>(questions);
			List<Key<Question>> questionsKeys = questionsResource.getKeyList();
			List<DataSet> dataSets = OfyService.ofy().load().type(DataSet.class).filter("questions in", questionsKeys)
					.list();
			List<DataSheet> dataSheets = OfyService.ofy().load().type(DataSheet.class)
					.filter("questions in", questionsKeys).list();

			for (Question question : questions) {
				for (DataSet dataSet : dataSets) {
					if (dataSet.getQuestions().contains(Key.create(question))) {
						dataSet.getQuestions().remove(dataSet.getQuestions().indexOf(Key.create(question)));
						// System.out.println("Question Deleted from dataset");

						for (Key<Answer> answerKey : question.getAnswers()) {
							Key<Answer> combinedKey = Key.create(Key.create(question), Answer.class, answerKey.getId());
							if (dataSet.getAnswers().contains(combinedKey)) {
								dataSet.getAnswers().remove(dataSet.getAnswers().indexOf(combinedKey));
							}
						}

					}
				}
				for (DataSheet dataSheet : dataSheets) {
					if (dataSheet.getQuestions().contains(Key.create(question))) {
						// System.out.println("Question Deleted from
						// datasheet");
						dataSheet.getQuestions().remove(dataSheet.getQuestions().indexOf(Key.create(question)));

						for (Key<Answer> answerKey : question.getAnswers()) {
							Key<Answer> combinedKey = Key.create(Key.create(question), Answer.class, answerKey.getId());
							if (dataSheet.getAnswers().contains(combinedKey)) {
								dataSheet.getAnswers().remove(dataSheet.getAnswers().indexOf(combinedKey));
							}
						}
					}

				}
			}
			// System.out.println("dataSets " + dataSets);
			OfyService.ofy().save().entities(dataSets).now();
			// System.out.println("DataSheets " + dataSheets);
			OfyService.ofy().save().entities(dataSheets).now();
		}
	}

	public void detachFilters(List<Filter> filters) {

		if (!filters.isEmpty()) {
			ResourceList<Filter> filtersResource = new ResourceList<Filter>(filters);
			List<Key<Filter>> filtersKeys = filtersResource.getKeyList();
			List<DataSet> dataSets = OfyService.ofy().load().type(DataSet.class).filter("filters in", filtersKeys)
					.list();
			List<DataSheet> dataSheets = OfyService.ofy().load().type(DataSheet.class).filter("filters in", filtersKeys)
					.list();

			for (Filter filter : filters) {
				for (DataSet dataSet : dataSets) {
					if (dataSet.getFilters().contains(Key.create(filter))) {
						dataSet.getFilters().remove(dataSet.getFilters().indexOf(Key.create(filter)));
						// System.out.println("Filter Deleted from dataset");

						for (Key<Division> divisionKey : filter.getDivisions()) {
							Key<Division> combinedKey = Key.create(Key.create(filter), Division.class,
									divisionKey.getId());
							if (dataSet.getDivisions().contains(combinedKey)) {
								dataSet.getDivisions().remove(dataSet.getDivisions().indexOf(combinedKey));
							}
						}

					}
				}
				for (DataSheet dataSheet : dataSheets) {
					if (dataSheet.getFilters().contains(Key.create(filter))) {
						// System.out.println("Filter Deleted from datasheet");
						dataSheet.getFilters().remove(dataSheet.getFilters().indexOf(Key.create(filter)));

						for (Key<Division> divisionKey : filter.getDivisions()) {
							Key<Division> combinedKey = Key.create(Key.create(filter), Division.class,
									divisionKey.getId());
							if (dataSheet.getDivisions().contains(combinedKey)) {
								dataSheet.getDivisions().remove(dataSheet.getDivisions().indexOf(combinedKey));
							}
						}
					}

				}
			}
			// System.out.println("dataSets " + dataSets);
			OfyService.ofy().save().entities(dataSets).now();
			// System.out.println("DataSheets " + dataSheets);
			OfyService.ofy().save().entities(dataSheets).now();
		}
	}

	public List<List<Key<Division>>> orderFiltersAndDivisions(Map<Filter, List<Division>> divisionByFilter) {
		List<Filter> tempFilters = new ArrayList<Filter>();
		List<List<Key<Division>>> divisions = new ArrayList<List<Key<Division>>>();
		for (Filter filter : divisionByFilter.keySet()) {
			tempFilters.add(filter);
		}
		Collections.sort(tempFilters, new Comparator<Filter>() {

			public int compare(Filter o1, Filter o2) {
				return o1.getId().compareTo(o2.getId());

			}
		});

		for (Filter filter : tempFilters) {
			List<Division> tempList = divisionByFilter.get(filter);
			List<Key<Division>> divisionsKeys = new ArrayList<Key<Division>>();
			for (Division division : tempList) {
				divisionsKeys.add(Key.create(division));
			}
			Collections.sort(divisionsKeys, new Comparator<Key<Division>>() {

				public int compare(Key<Division> o1, Key<Division> o2) {
					return (new Long(o1.getId())).compareTo(new Long(o2.getId()));

				}
			});
			divisions.add(divisionsKeys);
		}
		return divisions;
	}

	@RequestMapping("process/extract/rollback/chunk")
	public void deleteFilteredAnswerValues(@RequestParam("datasetid") Long dataSetId,
			@RequestParam("datasheetid") Long dataSheetId, @RequestParam("startindex") int start,
			@RequestParam("endindex") int end, HttpServletResponse response) {

		DataSheet dataSheet = OfyService.ofy().load().key(Key.create(DataSheet.class, dataSheetId)).getValue();

		Map<Question, List<Answer>> answerByQuestion = new HashMap<Question, List<Answer>>();
		Long locationId = null;
		if (dataSheet.getLocation() != null) {
			locationId = dataSheet.getLocation().getId();
		} else {
			locationId = OfyService.ofy().load().type(Location.class).first().getValue().getId();

		}

		List<Key<Question>> questionsKeys = dataSheet.getQuestions();
		List<Question> questions = new ArrayList<Question>(OfyService.ofy().load().keys(questionsKeys).values());

		for (Question question : questions) {
			List<Key<Answer>> rawAnswerKeys = question.getAnswers();
			List<Answer> rawAnswers = new ArrayList<Answer>(OfyService.ofy().load().keys(rawAnswerKeys).values());
			List<Answer> answers = new ArrayList<Answer>();
			for (Answer answer : rawAnswers) {
				if (answer.getDatasheets().contains(Key.create(dataSheet))) {
					answers.add(answer);
				}
			}
			answerByQuestion.put(question, answers);
		}

		List<Key<Brand>> brandsKeys = dataSheet.getBrands();

		List<Key<FilteredAnswerValue>> FilteredAnswerValuesKeys = new ArrayList<Key<FilteredAnswerValue>>();
		logger.error("Rolling Back From Row: " + start + ", To: " + end);
		for (int i = start; i < end; i++) {
			for (Question question : answerByQuestion.keySet()) {
				Long questionId = question.getId();
				for (Answer answer : answerByQuestion.get(question)) {
					Long answerId = answer.getId();
					for (Key<Brand> brandKey : brandsKeys) {
						Long brandId = brandKey.getId();

						String id = ("L" + locationId + "W" + dataSetId + "Q" + questionId + "A" + answerId + "B"
								+ brandId + "C" + i);
						FilteredAnswerValuesKeys.add(Key.create(FilteredAnswerValue.class, id));
					}
				}
			}

			logger.error("Adding row: " + i);
		}
		OfyService.ofy().delete().keys(FilteredAnswerValuesKeys).now();
		OfyService.ofy().clear();
		logger.error("Deleted From Row: " + start + ", To: " + end);
		response.setStatus(HttpServletResponse.SC_OK);
	}

	/**
	 * @param dataSheet
	 * @return
	 */
	private List<List<Key<Division>>> generateCombinationsList(DataSheet dataSheet) {
		List<Key<Filter>> filtersKeys = dataSheet.getFilters();
		List<Filter> filters = new ArrayList<Filter>(OfyService.ofy().load().keys(filtersKeys).values());

		Map<Filter, List<Division>> divisionByFilter = new HashMap<Filter, List<Division>>();
		for (Filter filter : filters) {
			List<Key<Division>> rawDivisionsKeys = filter.getDivisions();
			List<Division> rawDivisions = new ArrayList<Division>(
					OfyService.ofy().load().keys(rawDivisionsKeys).values());
			List<Division> divisions = new ArrayList<Division>();
			for (Division division : rawDivisions) {
				if (division.getDatasheets().contains(Key.create(dataSheet))) {
					divisions.add(division);
				}
			}
			divisionByFilter.put(filter, divisions);
		}
		List<List<Key<Division>>> orderedDivisions = orderFiltersAndDivisions(divisionByFilter);
		List<List<Key<Division>>> combinationList = generateCombinationsList(orderedDivisions, 0);
		return combinationList;
	}

	private List<List<Key<Division>>> generateCombinationsList(List<List<Key<Division>>> filters, int i) {

		// stop condition
		if (i == filters.size()) {
			// return a list with an empty list
			List<List<Key<Division>>> result = new ArrayList<List<Key<Division>>>();
			result.add(new ArrayList<Key<Division>>());
			return result;
		}

		List<List<Key<Division>>> result = new ArrayList<List<Key<Division>>>();
		List<List<Key<Division>>> recursive = generateCombinationsList(filters, i + 1); // recursive
																						// call

		// for each element of the first list of input
		for (int j = 0; j < filters.get(i).size(); j++) {
			// add the element to all combinations obtained for the rest of the
			// lists
			for (int k = 0; k < recursive.size(); k++) {
				// copy a combination from recursive
				List<Key<Division>> newList = new ArrayList<Key<Division>>();
				for (Key<Division> key : recursive.get(k)) {
					newList.add(key);
				}
				// add element of the first list
				newList.add(filters.get(i).get(j));
				// add new combination to result
				result.add(newList);
			}
		}
		return result;
	}

	/***************************************
	 * END Rolling Back
	 *************************************/

	private ModelAndView setDefaultDashboard() {

		DefaultDashBoard defaultDefault = new DefaultDashBoard();

		return new ModelAndView();
	}

	@RequestMapping("/data/defaultdashboard")
	public ModelAndView getDefaultDashboards(
			@RequestParam(value = "orderBy", defaultValue = "lastModificationDate", required = false) String orderBy,
			@RequestParam(value = "orderDir", defaultValue = "asc", required = false) String orderDir,
			@RequestParam(value = "offset", defaultValue = "0", required = false) Integer offset,
			@RequestParam(value = "count", defaultValue = "20", required = false) Integer count) {

		Query<DefaultDashBoard> query = OfyService.ofy().load().type(DefaultDashBoard.class);
		int originalCount = count;
		int totalDashboardCount = query.count();
		if (count == 0) {
			count = totalDashboardCount;
		}
		if (count > totalDashboardCount) {
			offset = 0;
			count = totalDashboardCount;
		}
		if (offset + count > totalDashboardCount) {
			count = totalDashboardCount - offset;
		}
		if (offset > 0) {
			query = query.offset(offset);
		}

		if (count > 0) {
			query = query.limit(count);
		}

		if (orderBy != null && orderDir != null) {
			if (orderDir.equals("asc")) {
				query = query.order(orderBy);
			} else {
				query = query.order("-" + orderBy);
			}
		}
		List<DefaultDashBoard> defaultDashboardsList = query.list();
		if (defaultDashboardsList.size() == 0 && offset == 0 && Math.min(count + offset, totalDashboardCount) == 0) {
			offset = -1;
			count += 1;
		}
		for (DefaultDashBoard key : defaultDashboardsList) {
		}
		List<Key<Dashboard>> dashboardsKeys = new ArrayList<Key<Dashboard>>();
		List<Key<Location>> locationsKeys = new ArrayList<Key<Location>>();

		for (DefaultDashBoard defaultDashBoard : defaultDashboardsList) {
			Long locationId = defaultDashBoard.getId();
			locationsKeys.add(Key.create(Location.class, locationId));
			Key<Dashboard> dashboardKey = defaultDashBoard.getDashboard();
			dashboardsKeys.add(dashboardKey);
		}
		Map<Key<Dashboard>, Dashboard> dashboards = OfyService.ofy().load().keys(dashboardsKeys);

		Map<Key<Location>, Location> locations = OfyService.ofy().load().keys(locationsKeys);

		for (DefaultDashBoard defaultDashBoard : defaultDashboardsList) {
			Long locationId = defaultDashBoard.getId();
			defaultDashBoard.setLocationObject(locations.get(Key.create(Location.class, locationId)));
			Key<Dashboard> dashboardKey = defaultDashBoard.getDashboard();
			defaultDashBoard.setDashboardObject(dashboards.get(dashboardKey));
		}

		Map<String, Object> model = new HashMap<String, Object>();

		// for (DefaultDashBoard key : defaultDashboardsList) {
		// System.out.println(key);
		// }
		model.put("resources", defaultDashboardsList);
		model.put("total", totalDashboardCount);
		model.put("from", offset);
		model.put("to", Math.min(count + offset, totalDashboardCount));
		model.put("orderDir", orderDir);
		model.put("orderBy", orderBy);

		model.put("count", originalCount);
		checkOperationLock(model);

		return new ModelAndView("cms/get_default_dashboards", model);
	}

	@RequestMapping(value = "data/dashboard/setaslocationdefault", method = RequestMethod.POST)
	public ModelAndView assignAsDefaultDashboard(HttpServletRequest request) {
		Map<String, Object> model = new HashMap<String, Object>();
		String userEmail = UserServiceFactory.getUserService().getCurrentUser().getEmail();
		ApplicationUser appUser = null;
		Ref<ApplicationUser> result = null;
		if (userEmail != null) {
			result = OfyService.ofy().load().type(ApplicationUser.class).filter("email", userEmail.toLowerCase())
					.first();
			if (result != null) {
				appUser = result.getValue();
			}
		}
		if (appUser != null && !appUser.isOnlyUser()) {
			Long dashboardId = new Long(request.getParameter("dashboard-hidden-id"));
			Long locationId = new Long(request.getParameter("location"));

			// Dashboard dashboard = OfyService.ofy().load().key(Key
			// .create(Key.create(appUser), Dashboard.class, dashboardId))
			// .getValue();
			Dashboard dashboard = OfyService.ofy().load().type(Dashboard.class).filter("id", dashboardId)
					.filter("applicationUsers", result).first().getValue();
			DefaultDashBoard defaultDashBoard = new DefaultDashBoard();
			defaultDashBoard.setDashboard(Key.create(dashboard));
			defaultDashBoard.setLastModificationDate(new Date());
			defaultDashBoard.setId(locationId);
			OfyService.ofy().save().entity(defaultDashBoard).now();
		}

		return new ModelAndView("", model);
	}

}

package com.brightcreations.gplus.module.controllers;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.brightcreations.gplus.module.controllers.EmailMessageController.EmailTypes;
import com.brightcreations.gplus.module.controllers.MIDataExplorer.AdminTokenGenerator;
import com.brightcreations.gplus.module.controllers.MIDataExplorer.DataRowController;
import com.brightcreations.gplus.module.controllers.MIDataExplorer.StudyBackEnd;
import com.brightcreations.gplus.module.model.Answer;
import com.brightcreations.gplus.module.model.ApplicationUser;
import com.brightcreations.gplus.module.model.Brand;
import com.brightcreations.gplus.module.model.BrandPerQuestionPerDataSetPerLocation;
import com.brightcreations.gplus.module.model.DataOperation;
import com.brightcreations.gplus.module.model.DataRow;
import com.brightcreations.gplus.module.model.DataSet;
import com.brightcreations.gplus.module.model.DataSheet;
import com.brightcreations.gplus.module.model.Division;
import com.brightcreations.gplus.module.model.Filter;
import com.brightcreations.gplus.module.model.FilteredAnswerValue;
import com.brightcreations.gplus.module.model.Location;
import com.brightcreations.gplus.module.model.LogFile;
import com.brightcreations.gplus.module.model.ProcessingQueueEntry;
import com.brightcreations.gplus.module.model.Question;
import com.brightcreations.gplus.module.model.Study;
import com.brightcreations.gplus.module.model.collection.ResourceList;
import com.google.appengine.api.backends.BackendServiceFactory;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.RetryOptions;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.utils.SystemProperty;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;

import au.com.bytecode.opencsv.CSVReader;

@RequestMapping("/dataImporter")
public class DataImporter {
	private Logger logger = LoggerFactory.getLogger(getClass());
	private LogFile logFile = new LogFile();
	private static int questionCount = 0;
	private static int questionStartIndex = -1;
	private static final int BRAND_ROW = 2;
	private static final int ANSWER_ROW = 3;
	private static final int FILTER_ROW = 3;
	private static final int FILTER_ANSWERS_START_ROW = 4;
	private Map<String, Boolean> validations = new HashMap<String, Boolean>();
	private String dataSetName = "";
	private String studyName = "";
	private String studyID = "";

	public BufferedReader getCSVData(BlobKey blobKey, String dataSheetName) throws IOException {

		BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
		int readLimit = 5120;

		int start = 0;
		byte[] buff = null;

		buff = blobstoreService.fetchData(blobKey, start, (start + readLimit));
		start += readLimit + 1;

		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		buffer.write(buff);

		while (buff.length != 0) {

			buff = blobstoreService.fetchData(blobKey, start, (start + readLimit));
			buffer.write(buff);
			buffer.flush();
			start += readLimit + 1;
		}

		ZipInputStream localZin = new ZipInputStream(new ByteArrayInputStream(buffer.toByteArray()));
		buffer.close();
		buffer = null;
		ZipEntry zipEntry = localZin.getNextEntry();

		ByteArrayOutputStream bos = new ByteArrayOutputStream();

		BufferedReader localDis = new BufferedReader(new InputStreamReader(localZin, "UTF-8"));

		return localDis;
	}

	public String[] readRow(String rowString) throws IOException {
		// String rowString = localDis.readLine();
		if (rowString != null) {
			InputStreamReader reader = new InputStreamReader(new ByteArrayInputStream(rowString.getBytes("UTF-8")),
					"UTF-8");
			CSVReader allDataReader = new CSVReader(reader);
			String[] result = allDataReader.readAll().get(0);
			allDataReader.close();
			for (int i = 0; i < result.length; i++) {
				result[i] = result[i].trim();
			}
			return result;
		}
		return null;
	}

	public String[] readDataRow(int rowIndex, DataSheet dataSheet) throws IOException {
		String[] result = null;

		DataRow dataRow = OfyService.ofy().load().type(DataRow.class)
				.filter("datasheetKey", Key.create(DataSheet.class, dataSheet.getId())).filter("rowNumber", rowIndex)
				.first().getValue();
		if (dataRow != null) {
			result = readRow(dataRow.getRowValue());
		}

		return result;
	}

	public void commitPart(HttpServletResponse response, @RequestParam("start") long startIndex,
			@RequestParam("end") long endIndex, @RequestParam("key") long dataSheetKey) throws IOException {
		//
		System.out.println("----commitPart-----");
		logger.error("COMMITTING From: " + startIndex + "... to " + endIndex + " .... ");

		DataSheet localDataSheet = OfyService.ofy().load().key(Key.create(DataSheet.class, dataSheetKey)).get();

		Map<Integer, Question> questionsMap = extractQuestions(localDataSheet);

		replaceNewQuestionsWithAlreadyExisting(questionsMap);

		Map<String, Object> objectsMapAnswers = extractAnswers(questionsMap, localDataSheet);

		Map<Question, Map<String, Answer>> answersMapByQuestion = (Map<Question, Map<String, Answer>>) objectsMapAnswers
				.get("answersMapByQuestion");

		Map<Integer, Answer> answersMapById = (Map<Integer, Answer>) objectsMapAnswers.get("answersMapById");

		replaceNewAnswersWithAlreadyExisting(answersMapByQuestion);

		Map<String, Object> objectsMapBrands = extractBrands(questionsMap, localDataSheet);

		Map<Integer, Brand> brandsMap = (Map<Integer, Brand>) objectsMapBrands.get("brandsMap");
		Map<Question, List<Brand>> brandsByQuestion = (Map<Question, List<Brand>>) objectsMapBrands
				.get("brandsByQuestion");

		replaceNewBrandsWithAlreadyExisting(brandsByQuestion);

		Map<String, Object> objectsMapFilters = extractFilters(localDataSheet);

		Map<Integer, Filter> filtersMapByCol = (Map<Integer, Filter>) objectsMapFilters.get("filtersMapByCol");

		replaceNewFiltersWithAlreadyExisting(filtersMapByCol);
		//
		// BufferedReader bufferedReaderDivisions = getCSVData(new
		// BlobKey(localDataSheet.getBlobstoreKey()),
		// localDataSheet.getName());

		Map<String, Object> objectsMapDivisions = extractDivisionsData(localDataSheet, answersMapByQuestion,
				filtersMapByCol, brandsByQuestion, brandsMap);

		// extractDivisions(bufferedReaderDivisions, answersMapByQuestion,
		// filtersMapByCol, brandsByQuestion, brandsMap);

		Map<Integer, Location> locationByRow = (Map<Integer, Location>) objectsMapDivisions.get("locationByRow");
		Map<Filter, Set<Division>> divisionByFilter = (Map<Filter, Set<Division>>) objectsMapDivisions
				.get("divisionByFilter");
		Map<Integer, List<Division>> divisionsByRow = (Map<Integer, List<Division>>) objectsMapDivisions
				.get("divisionsByRow");
		Map<Integer, Double> valuesMap = (Map<Integer, Double>) objectsMapDivisions.get("valuesMap");

		DataSet wave = (DataSet) objectsMapDivisions.get("wave");
		Study study = (Study) objectsMapDivisions.get("study");

		// BufferedReader bufferedReader = getCSVData(new
		// BlobKey(localDataSheet.getBlobstoreKey()),
		// localDataSheet.getName());
		List<Answer> allAnswers = new ArrayList<Answer>();
		List<Division> allDivisions = new ArrayList<Division>();
		List<Brand> allBrands = new ArrayList<Brand>();

		// for all brands, add the relation to the dataSet and the dataSheet
		for (Question question : brandsByQuestion.keySet()) {
			for (Brand brand : brandsByQuestion.get(question)) {
				if (!allBrands.contains(brand)) {
					allBrands.add(brand);
				}
			}
		}

		for (Question question : answersMapByQuestion.keySet()) {
			allAnswers.addAll(answersMapByQuestion.get(question).values());
		}

		for (Filter filter : divisionByFilter.keySet()) {
			allDivisions.addAll(divisionByFilter.get(filter));
		}
		@SuppressWarnings("unused")
		int index = 0;
		ArrayList<FilteredAnswerValue> copyfilteredAnswerValues;

		List<List<Key<Division>>> combinations = generateCombinationsList(orderFiltersAndDivisions(divisionByFilter),
				0);

		int rowIndex = 0;
		for (int i = 0; i < startIndex; i++) {
			// readRow(bufferedReader);
			readDataRow(i, localDataSheet);
			rowIndex++;
		}

		Map<Question, List<Brand>> questionsBrands = getQuestionsBrandsMap(questionsMap, brandsMap, valuesMap);

		logger.error("Passed Skipping Rows ");
		String[] divisionAndfilteredAnswerDataArray = null;
		while (startIndex < endIndex) {

			// divisionAndfilteredAnswerDataArray = readRow(bufferedReader);
			divisionAndfilteredAnswerDataArray = readDataRow(rowIndex, localDataSheet);

			if (divisionAndfilteredAnswerDataArray == null) {
				break;
			}

			if (divisionAndfilteredAnswerDataArray.length == 0) {
				break;
			}

			copyfilteredAnswerValues = new ArrayList<FilteredAnswerValue>();

			Brand brand = null;
			Question question = null;
			Answer answer = null;

			index++;
			List<String> divisionAndfilteredAnswerDataList = new ArrayList<String>();
			for (String string : divisionAndfilteredAnswerDataArray) {
				divisionAndfilteredAnswerDataList.add(string);
			}

			if (divisionsByRow.get(rowIndex) == null) {
				logger.error(divisionsByRow.toString());
				System.out.println("Start: " + startIndex);
			}

			ResourceList<Division> tempDivisionList = new ResourceList<Division>(divisionsByRow.get(rowIndex));

			int rowDivisionsIndex = compareLists(combinations, tempDivisionList.getKeyList());
			for (int i = questionStartIndex; i < divisionAndfilteredAnswerDataList.size(); i++) {

				if (answersMapById.get(i) != null) {

					if (questionsMap.get(i) != null) {
						question = questionsMap.get(i);
					}
					if (brandsMap.get(i) != null) {
						String tempName = brandsMap.get(i).getName();
						for (Brand tempBrand : allBrands) {
							if (tempBrand.getName().equals(tempName)) {
								brand = tempBrand;
								break;
							}
						}
					}

					boolean found = false;
					for (Question tempQuestion : questionsBrands.keySet())
						if (tempQuestion.getQuestionText().equals(question.getQuestionText())) {
							for (Brand tempBrand : questionsBrands.get(tempQuestion)) {
								if (tempBrand.getName().equals(brand.getName())) {
									found = true;
								}
							}
						}

					if (!found) {
						// System.out.println("skipped");
						continue;
					}

					if (answersMapById.get(i) != null) {
						String tempName = answersMapById.get(i).getAnswerText();
						for (Answer tempAnswer : allAnswers) {
							if (tempAnswer.getAnswerText().equals(tempName)) {
								answer = tempAnswer;
								break;
							}
						}

					}

					if (answersMapByQuestion.get(question) != null) {
						String name = answer.getAnswerText();
						answer = answersMapByQuestion.get(question).get(answer.getAnswerText());
						if (answer == null) {
							for (String temp : answersMapByQuestion.get(question).keySet()) {
								System.out.println(temp);
							}
							System.out.println();
							System.out.println("answer == null" + name);
						}
					}

					FilteredAnswerValue FilteredAnswerValue = new FilteredAnswerValue();
					long locationId = locationByRow.get(rowIndex).getId();
					long waveId = wave.getId();
					// long questionId = answer.getQuestion().getKey().getId();
					long answerId = answer.getId();
					long brandId = brand.getId();
					long studyId = study.getId();

					// FilteredAnswerValue.setId("S" + studyId + "L" +
					// locationId + "W" + waveId + "Q" + questionId + "A"
					// + answerId + "B" + brandId + "C" + rowDivisionsIndex);

					copyfilteredAnswerValues.add(FilteredAnswerValue);

					try {
						FilteredAnswerValue.setValue(Double.parseDouble(divisionAndfilteredAnswerDataList.get(i)));
					} catch (NumberFormatException e) {
						// logger.error("FAILED TO PARSE DOUBLE FOR:
						// "+divisionAndfilteredAnswerDataList
						// .get(i));
						// logger.error("ROW INDEX:"+ rowDivisionsIndex);
						// logger.error("COL INDEX:"+ i);
					}

				} else {
					break;
				}
			}
			OfyService.ofy().clear();
			boolean saved = false;
			for (;;) {
				if (saved == true) {
					break;
				}
				logger.error("Trying to Save");
				if (!copyfilteredAnswerValues.isEmpty()) {
					try {
						OfyService.ofy().save().entities(copyfilteredAnswerValues).now();
						logger.error("Saved row: " + rowIndex);
						saved = true;
						////////////////// TEMP///////////////////////////////////////
						// List<PointNotes> notes = new ArrayList<>();
						// for (FilteredAnswerValue value :
						// copyfilteredAnswerValues) {
						// PointNotes n = new PointNotes();
						// n.setNote("test Note");
						// n.setCreationDate(new Date());
						// n.setFilterdAnswerValue(Ref.create(value));
						// notes.add(n);
						// }
						// OfyService.ofy().save().entities(notes).now();
						////////////////////////////////////////////////////////////
					} catch (Exception e) {
						OfyService.ofy().clear();
						logger.error(e.getMessage());
						e.printStackTrace();
					}
				}
			}
			rowIndex++;

			startIndex++;
		}

		logger.error("END Of " + startIndex + " ... " + endIndex);
		Queue q = QueueFactory.getQueue("importing-chunk");
		logger.error("Current number " + q.fetchStatistics().getNumTasks());

		response.setStatus(HttpServletResponse.SC_OK);
	}

	public Map<Integer, Question> extractQuestions(DataSheet dataSheet) throws IOException {

		// String[] questionData = readRow(0, dataSheet, bufferedReader);
		// String[] baseData = readRow(1, dataSheet, bufferedReader);

		String[] questionData = readDataRow(0, dataSheet);
		String[] baseData = readDataRow(1, dataSheet);

		Map<Integer, Question> questionsMap = new HashMap<Integer, Question>();
		List<Question> questions = new ArrayList<Question>();
		for (int i = 0; i < questionData.length; i++) {

			if (!questionData[i].trim().equals("")) {
				if (questionCount == 0) {
					questionStartIndex = i;
				}
				questionCount++;
				Question question = new Question();
				if (baseData.length > i && baseData[i] != null) {
					question.setBase(baseData[i]);

				}
				question.setQuestionText(questionData[i].trim());
				questions.add(question);
				questionsMap.put(i, question);
			}
		}
		return questionsMap;
	}

	public boolean replaceNewQuestionsWithAlreadyExisting(Map<Integer, Question> rawQuestions) {
		boolean validated = false;

		for (Integer key : rawQuestions.keySet()) {
			String questionName = rawQuestions.get(key).getQuestionText();
			List<Question> foundQuestions = OfyService.ofy().load().type(Question.class)
					.filter("name", questionName.trim()).list();
			if (!foundQuestions.isEmpty()) {
				rawQuestions.put(key, foundQuestions.get(0));
			}
		}
		return validated;
	}

	public Map<String, Object> extractAnswers(Map<Integer, Question> questionsMap, DataSheet dataSheet)
			throws IOException {

		Map<String, Object> objectsMap = new HashMap<String, Object>();
		Map<Question, Map<String, Answer>> answersMapByQuestion = new HashMap<Question, Map<String, Answer>>();
		Map<Integer, Answer> answersMapById = new LinkedHashMap<Integer, Answer>();

		// String[] answerData = readRow(ANSWER_ROW, dataSheet, bufferedReader);
		String[] answerData = readDataRow(ANSWER_ROW, dataSheet);

		List<Answer> answers = new ArrayList<Answer>();
		Question question = null;

		// Fills the answers list for their corresponding questions
		for (int i = questionStartIndex; i < answerData.length; i++) {
			if (!answerData[i].trim().equals("")) {
				// If the column (i) has a different question the question
				// object is updated
				if (questionsMap.get(i) != null) {
					question = questionsMap.get(i);

				}

				Map<String, Answer> answersForCurrentQuestion = answersMapByQuestion.get(question);
				if (answersForCurrentQuestion == null) {
					answersForCurrentQuestion = new HashMap<String, Answer>();
					answersMapByQuestion.put(question, answersForCurrentQuestion);
				}
				Answer answer = answersForCurrentQuestion.get(answerData[i].trim());

				if (answer == null) {
					answer = new Answer();

					answer.setAnswerText(answerData[i].trim());
					answers.add(answer);
					answersForCurrentQuestion.put(answerData[i].trim(), answer);
				}
				answersMapById.put(i, answer);
			}
		}
		objectsMap.put("answersMapByQuestion", answersMapByQuestion);
		objectsMap.put("answersMapById", answersMapById);
		return objectsMap;
	}

	public Map<String, Object> extractBrands(Map<Integer, Question> questionsMap, DataSheet dataSheet)
			throws IOException {

		// String[] brandData = readRow(BRAND_ROW, dataSheet, bufferedReader);
		String[] brandData = readDataRow(BRAND_ROW, dataSheet);

		Map<String, Object> objectsMap = new HashMap<String, Object>();

		Map<Integer, Brand> brandsMap = new HashMap<Integer, Brand>();
		Set<String> brandSet = new HashSet<String>();
		Map<String, Brand> brandsByname = new HashMap<String, Brand>();
		Map<Question, List<Brand>> brandsByQuestion = new HashMap<Question, List<Brand>>();
		List<Brand> brands = new ArrayList<Brand>();

		Question question = null;
		for (int i = questionStartIndex; i < brandData.length; i++) {
			if (!brandData[i].trim().equals("")) {

				if (questionsMap.get(i) != null) {
					question = questionsMap.get(i);
				}

				if (!brandSet.contains(brandData[i])) {
					Brand brand = new Brand();
					brand.setName(brandData[i]);

					brands.add(brand);

					brandsByname.put(brandData[i], brand);

					brandSet.add(brandData[i]);
				}

				List<Brand> brandsForCurrentQuestion = brandsByQuestion.get(question);
				if (brandsForCurrentQuestion == null) {
					brandsForCurrentQuestion = new ArrayList<Brand>();
					brandsByQuestion.put(question, brandsForCurrentQuestion);

				}
				brandsForCurrentQuestion.add(brandsByname.get(brandData[i]));

				brandsMap.put(i, brandsByname.get(brandData[i]));

			}
		}

		objectsMap.put("brandSet", brandSet);
		objectsMap.put("brandsMap", brandsMap);
		objectsMap.put("brandsByname", brandsByname);
		objectsMap.put("brandsByQuestion", brandsByQuestion);
		objectsMap.put("brands", brands);

		return objectsMap;
	}

	public boolean replaceNewAnswersWithAlreadyExisting(Map<Question, Map<String, Answer>> rawAnswers) {
		boolean validated = false;
		// System.out.println("raw Answers " + rawAnswers);
		for (Question question : rawAnswers.keySet()) {
			Map<String, Answer> rawAns = rawAnswers.get(question);
			if (question != null) {
				if (question.getId() != null) {
					for (String answerKey : rawAns.keySet()) {
						if (rawAns.get(answerKey) != null) {

							String answerName = rawAns.get(answerKey).getAnswerText();

							List<Answer> foundAnswers = OfyService.ofy().load().type(Answer.class)
									.filter("name", answerName).ancestor(Key.create(question)).list();
							if (!foundAnswers.isEmpty()) {
								rawAns.put(answerKey, foundAnswers.get(0));
							}
						}
					}
				}
			}
		}

		return validated;
	}

	public boolean replaceNewBrandsWithAlreadyExisting(Map<Question, List<Brand>> rawBrands) {
		boolean validated = false;

		for (Question question : rawBrands.keySet()) {
			int index = 0;
			for (Brand brand : rawBrands.get(question)) {
				String brandName = brand.getName();
				List<Brand> foundBrands = OfyService.ofy().load().type(Brand.class).filter("name", brandName).list();
				if (!foundBrands.isEmpty()) {
					rawBrands.get(question).set(index, foundBrands.get(0));
				}
				index++;
			}
		}
		return validated;
	}

	public Map<String, Object> extractFilters(DataSheet dataSheet) throws IOException {

		Map<String, Object> objectsMap = new HashMap<String, Object>();
		Map<Integer, Filter> filtersMapByCol = new HashMap<Integer, Filter>();
		Map<String, Integer> filtersMap = new HashMap<String, Integer>();

		// String[] filterData = readRow(FILTER_ROW, dataSheet, bufferedReader);
		String[] filterData = readDataRow(FILTER_ROW, dataSheet);

		List<Filter> filters = new ArrayList<Filter>();
		for (int i = 0; i < filterData.length; i++) {
			if (!filterData[i].trim().equals("")) {
				Filter filter = new Filter();
				filter.setName(filterData[i]);
				filters.add(filter);
				filtersMapByCol.put(i, filter);
				filtersMap.put(filterData[i], i);
			}
		}
		objectsMap.put("filtersMapByCol", filtersMapByCol);
		objectsMap.put("filtersMap", filtersMap);
		return objectsMap;
	}

	public boolean replaceNewFiltersWithAlreadyExisting(Map<Integer, Filter> rawFilters) {
		boolean validated = false;
		for (int key : rawFilters.keySet()) {
			Filter filter = rawFilters.get(key);
			String filterName = filter.getName();
			List<Filter> foundFilters = OfyService.ofy().load().type(Filter.class).filter("name", filterName).list();
			if (!foundFilters.isEmpty()) {
				rawFilters.put(key, foundFilters.get(0));
			}
		}

		return validated;
	}

	private Map<Question, List<Brand>> getQuestionsBrandsMap(Map<Integer, Question> questionsMap,
			Map<Integer, Brand> brandsByColumn, Map<Integer, Double> valuesMap) {
		Map<Integer, Double> valuesByBrand = new LinkedHashMap<Integer, Double>();
		Set<Integer> valuesKeySet = valuesMap.keySet();
		List<Integer> valuesKeyList = new ArrayList<Integer>();
		valuesKeyList.addAll(valuesKeySet);
		Collections.sort(valuesKeyList);

		int currentKey = 0;

		for (int valueKey : valuesKeyList) {
			if (brandsByColumn.get(valueKey) != null) {
				valuesByBrand.put(valueKey, new Double(0));
				currentKey = valueKey;
			}
			if (currentKey != 0) {
				Double newValue = valuesMap.get(valueKey) + valuesByBrand.get(currentKey);
				valuesByBrand.put(currentKey, newValue);
			}
		}
		valuesKeySet = valuesByBrand.keySet();
		valuesKeyList = new ArrayList<Integer>();
		valuesKeyList.addAll(valuesKeySet);
		Collections.sort(valuesKeyList);

		Map<Question, List<Brand>> questionsBrands = new HashMap<Question, List<Brand>>();

		Question currentQuestion = null;
		for (Integer brandKey : valuesKeyList) {
			if (questionsMap.get(brandKey) != null) {
				questionsBrands.put(questionsMap.get(brandKey), new ArrayList<Brand>());
				currentQuestion = questionsMap.get(brandKey);
			}
			if (currentQuestion != null) {
				if (!valuesByBrand.get(brandKey).equals(new Double(0))) {
					questionsBrands.get(currentQuestion).add(brandsByColumn.get(brandKey));
				}
			}
		}
		return questionsBrands;
	}

	public Map<String, Object> extractDivisionsData(DataSheet dataSheet,
			Map<Question, Map<String, Answer>> answersMapByQuestion, Map<Integer, Filter> filtersMapByCol,
			Map<Question, List<Brand>> brandsByQuestion, Map<Integer, Brand> brandsByColumn) throws IOException {

		Map<Integer, Location> locationByRow = new HashMap<Integer, Location>();
		Map<Integer, Study> studyByRow = new HashMap<Integer, Study>();
		Map<String, Object> objectsMap = new HashMap<String, Object>();
		List<String> waves = new ArrayList<String>();
		Map<String, Division> divisionsByName = new HashMap<String, Division>();
		Map<Filter, Set<Division>> divisionByFilter = new HashMap<Filter, Set<Division>>();
		Map<Integer, List<Division>> divisionsByRow = new HashMap<Integer, List<Division>>();
		Map<String, Location> locationByName = new HashMap<String, Location>();
		Map<String, Study> studyByName = new HashMap<String, Study>();
		Set<String> locationSet = new HashSet<String>();
		Set<String> studyNameSet = new HashSet<String>();
		DataSet wave = null;
		Study study = null;

		List<Brand> allBrands = new ArrayList<Brand>();
		List<Answer> allAnswers = new ArrayList<Answer>();
		for (Question question : brandsByQuestion.keySet()) {
			for (Brand brand : brandsByQuestion.get(question)) {
				if (!allBrands.contains(brand)) {
					allBrands.add(brand);
				}
			}
		}

		for (Question question : answersMapByQuestion.keySet()) {
			for (Answer ans : answersMapByQuestion.get(question).values()) {
				allAnswers.add(ans);
			}
		}

		String[] divisionAndfilteredAnswerData = null;

		Map<Integer, Double> valuesMap = new LinkedHashMap<Integer, Double>();

		int rowIndex = FILTER_ANSWERS_START_ROW;
		logger.error("filling list");
		// Loop on rows and extract the divisions,wave, country(location) for
		// the whole data-sheet
		while (true) {

			List<Division> divisions = new ArrayList<Division>();

			divisionAndfilteredAnswerData = readDataRow(rowIndex, dataSheet);
			if (divisionAndfilteredAnswerData == null || divisionAndfilteredAnswerData.length == 0) {
				System.out.println("No More Data To Load..");
				break;
			}
			System.out.println("Read Division Data: " + rowIndex);

			if (brandsByColumn != null) {
				if (valuesMap.get(questionStartIndex) == null) {
					for (int i = questionStartIndex; i < divisionAndfilteredAnswerData.length; i++) {
						valuesMap.put(i, new Double(0));
					}
				}
				for (int i = questionStartIndex; i < divisionAndfilteredAnswerData.length; i++) {
					Double value = new Double(0);
					try {
						value = Double.parseDouble(divisionAndfilteredAnswerData[i]);
					} catch (Exception e) {

					}
					value += valuesMap.get(i);
					valuesMap.put(i, value);
				}
			}

			// Extract divisions for the row at hand
			// The column with the name "Country" and the colum with the name
			// "Fieldwork Wave"
			// are not treated as divisions from the datastore side
			for (int i = 0; i < questionStartIndex; i++) {
				Filter filter = filtersMapByCol.get(i);
				if (filter.getName().equalsIgnoreCase("Study Name")) {
					// System.out.println("+++Study Name++++");
					if (!studyNameSet.contains(divisionAndfilteredAnswerData[i])) {
						Study study1 = new Study();
						studyName = divisionAndfilteredAnswerData[i];
						study1.setStudyName(studyName);
						studyID = StudyBackEnd.generateStudyID(studyName);
						study1.setStudyID(studyID);
						// study1.setCreationDate(new Date());
						// study1 = updateStudy(study1);
						studyNameSet.add(divisionAndfilteredAnswerData[i]);
						studyByName.put(divisionAndfilteredAnswerData[i], study1);
						if (study == null) {
							study = new Study();
							study.setStudyName(studyName);
							study.setStudyID(studyID);
							// study.setCreationDate(new Date());
						}
					}
					studyByRow.put(rowIndex, studyByName.get(divisionAndfilteredAnswerData[i]));

				} // end Study
				else if (filter.getName().equalsIgnoreCase("Country")) {
					if (!locationSet.contains(divisionAndfilteredAnswerData[i])) {

						Location location = new Location();
						location.setName(divisionAndfilteredAnswerData[i]);
						location = replaceNewLocationWithAlreadyExisting(location);
						locationSet.add(divisionAndfilteredAnswerData[i]);
						locationByName.put(divisionAndfilteredAnswerData[i], location);
					}
					locationByRow.put(rowIndex, locationByName.get(divisionAndfilteredAnswerData[i]));

				} // end country
				else if (filter.getName().equalsIgnoreCase("Fieldwork Wave")) {
					if (!waves.contains(divisionAndfilteredAnswerData[i])
							&& !divisionAndfilteredAnswerData[i].equals("")) {
						waves.add(divisionAndfilteredAnswerData[i]);
					}
					if (wave == null) {
						wave = new DataSet();
						dataSetName = divisionAndfilteredAnswerData[i];
						wave.setName(divisionAndfilteredAnswerData[i]);
					}

				} // end fieldwork
				else {
					Set<Division> divisionsNamesForCurrentFilter = divisionByFilter.get(filter);
					if (divisionsNamesForCurrentFilter == null) {
						divisionsNamesForCurrentFilter = new HashSet<Division>();
						divisionByFilter.put(filter, divisionsNamesForCurrentFilter);

					}

					boolean containsDivision = false;
					for (Division division : divisionsNamesForCurrentFilter) {
						if (division.getName().equals(divisionAndfilteredAnswerData[i])) {
							containsDivision = true;
							break;
						}
					}

					if (!containsDivision) {
						Division division = new Division();
						division.setName(divisionAndfilteredAnswerData[i]);
						division = replaceNewDivisionWithAlreadyExisting(division, filter);
						divisions.add(division);
						divisionsNamesForCurrentFilter.add(division);
						divisionsByName.put(divisionAndfilteredAnswerData[i], division);
					}
					List<Division> rowDivisions = divisionsByRow.get(rowIndex);
					if (rowDivisions == null) {
						rowDivisions = new ArrayList<Division>();
						divisionsByRow.put(rowIndex, rowDivisions);

					}

					rowDivisions.add(divisionsByName.get(divisionAndfilteredAnswerData[i]));

				}

			} // end loop

			rowIndex++;
		}
		if (studyNameSet.size() == 0) {
			logFile.setStudyStatus("\"Study \" column not found.");
			validations.put("STUDY_VALIDATED", false);
		} else if (studyNameSet.size() == 1) {
			List<Study> studies = OfyService.ofy().load().type(Study.class).filter("studyName", studyName).list();

			if (studies.isEmpty()) {
				study = new Study();
				study.setStudyName(studyName);
				study.setStudyID(studyID);
				// study.setCreationDate(new Date());

				// logFile.setStudyStatus("<strong>STUDY verified \""
				// + studies.get(0) + "\" (NEW STUDY).</strong>");
				validations.put("NEW_STUDY", true);
				validations.put("STUDY_VALIDATED", true);

			} else {
				study = studies.get(0);

				logFile.setStudyStatus("<strong>STUDY verified \"" + studies.get(0) + "\".</strong>");
				validations.put("STUDY_VALIDATED", true);
				validations.put("NEW_STUDY", true);
			}
		}

		if (waves.size() == 0) {
			logFile.setWaveStatus("\"Fieldwork Wave\" column not found.");
			validations.put("DATASET_VALIDATED", false);
		} else if (waves.size() == 1) {

			List<DataSet> datasets = OfyService.ofy().load().type(DataSet.class).filter("name", dataSetName).list();

			if (datasets.isEmpty()) {
				wave = new DataSet();
				wave.setName(dataSetName);
				wave.setWaveDate(new Date());
				wave.setCreationDate(new Date());
				wave.setLastModificationDate(new Date());

				logFile.setWaveStatus("<strong>DataSet verified \"" + waves.get(0) + "\" (NEW DataSet).</strong>");
				validations.put("NEW_DATASET", true);
				validations.put("DATASET_VALIDATED", true);
			} else {
				wave = datasets.get(0);
				wave.setLastModificationDate(new Date());
				logFile.setWaveStatus("<strong>DataSet verified \"" + waves.get(0) + "\".</strong>");
				validations.put("DATASET_VALIDATED", true);
				validations.put("NEW_DATASET", true);
			}
		} else {
			logFile.setWaveStatus("The DataSheet contains multiple DataSets\"" + waves.toString() + "\".");
			validations.put("DATASET_VALIDATED", false);
		}
		objectsMap.put("valuesMap", valuesMap);
		objectsMap.put("locationByRow", locationByRow);
		objectsMap.put("divisionByFilter", divisionByFilter);
		objectsMap.put("divisionsByRow", divisionsByRow);
		objectsMap.put("locationByName", locationByName);
		objectsMap.put("studyByName", studyByName);
		objectsMap.put("studyByRow", studyByRow);
		objectsMap.put("wave", wave);
		objectsMap.put("study", study);
		return objectsMap;
	}

	public Location replaceNewLocationWithAlreadyExisting(Location location) {
		String locationName = location.getName();
		List<Location> foundLocations = OfyService.ofy().load().type(Location.class).filter("name", locationName)
				.list();

		if (!foundLocations.isEmpty()) {
			location = foundLocations.get(0);
		} else {
			location.setIsRegion(false);
		}

		return location;
	}

	public Division replaceNewDivisionWithAlreadyExisting(Division division, Filter filter) {
		if (filter.getId() != null) {

			String divisionName = division.getName();

			List<Division> foundDivisions = OfyService.ofy().load().type(Division.class).filter("name", divisionName)
					.ancestor(filter).list();

			if (!foundDivisions.isEmpty()) {
				division = foundDivisions.get(0);
			}
		}
		return division;
	}

	public void commitWaveData(Study study, Map<String, Study> studyByName,
			Map<Question, Map<String, Answer>> questionsAndAnswers, DataSheet dataSheet,
			Map<Question, List<Brand>> brandByQuestion, Map<Filter, Set<Division>> FiltersAndDivisions,
			BufferedReader bufferedReader, Map<String, Location> locationByName, DataSet wave,
			Map<Question, List<Brand>> questionsBrands) throws IOException {

		logger.info("VALIDATED AND COMMITTING .... ");
		// check if the dataSet Key is NULL ie. New DataSet
		// if new DataSet save it to dataStore

		dataSheet.setStatus(DataSheet.dataSheetStatus.PROCESSING.getStatus());

		OfyService.ofy().save().entity(dataSheet).now();

		Key<DataSet> dataSetKey = null;
		Key<Study> studyKey = null;

		// wave.setUploadBy(UserServiceFactory.getUserService().getCurrentUser().getEmail());

		if (wave.getId() == null) {
			dataSetKey = OfyService.ofy().save().entity(wave).now();
		} else {
			dataSetKey = Key.create(wave);
		}
		if (study.getId() == null) {
			studyKey = OfyService.ofy().save().entity(study).now();
		} else {
			studyKey = Key.create(study);
		}
		// assign the dataSheet to the dataSet contained
		dataSheet.setDatasetRef(Ref.create(Key.create(wave)));

		// assign the dataSheet to the study contained
		dataSheet.setStudy(Ref.create(studyKey));
		OfyService.ofy().save().entity(dataSheet).now();

		// assign the dataset to the study contained
		wave.setStudy(Ref.create(studyKey));

		List<Answer> allAnswers = new ArrayList<Answer>();
		List<Division> allDivisions = new ArrayList<Division>();
		List<Brand> allBrands = new ArrayList<Brand>();

		// for all brands, add the relation to the dataSet and the dataSheet
		for (Question question : brandByQuestion.keySet()) {
			// System.out.println("Question " + question.getName());
			for (Brand brand : brandByQuestion.get(question)) {

				if (!allBrands.contains(brand)) {
					if (!brand.getDataSets().contains(dataSetKey))
						brand.getDataSets().add(dataSetKey);

					if (!brand.getDatasheets().contains(Key.create(dataSheet)))
						brand.getDatasheets().add(Key.create(dataSheet));

					if (!brand.getStudies().contains(studyKey))
						brand.getStudies().add(studyKey);

					allBrands.add(brand);

					// System.out.println("Question: " + question.getName()
					// + " Brand:" + brand.getName() + ", id: "
					// + brand.getId());
				}
				// System.out.println("condition: " +
				// allBrands.contains(brand));

			}
		}

		// SAVE brands
		OfyService.ofy().save().entities(allBrands).now();

		/*
		 * check if the brands for each question are different and add the new
		 * brands to the list of relations in the question add the dataSet and
		 * dataSheet relations to the questions
		 */
		for (Question question : questionsAndAnswers.keySet()) {
			List<Brand> brandsForCurrentQuestion = brandByQuestion.get(question);
			List<Key<Brand>> oldBrands = question.getBrands();

			if (brandsForCurrentQuestion != null)
				for (Brand brand : brandsForCurrentQuestion) {
					// System.out.println(brand.getName() + " " +
					// brand.getId());
					if (!oldBrands.contains(Key.create(brand))) {
						oldBrands.add(Key.create(brand));
					}
				}
			if (!question.getDataSets().contains(dataSetKey)) {
				question.getDataSets().add(dataSetKey);
			}
			if (!question.getDatasheets().contains(Key.create(dataSheet))) {
				question.getDatasheets().add(Key.create(dataSheet));
			}

			// if (!question.getStudies().contains(studyKey)) {
			// question.getStudies().add(studyKey);
			// }

		}

		// SAVE Questions
		OfyService.ofy().save().entities(questionsAndAnswers.keySet()).now();
		logger.error("Questions Saved ");
		/*
		 * Answers
		 * 
		 * Set the question relation (assuming that the questions is already
		 * saved)
		 */

		for (Question question : questionsAndAnswers.keySet()) {
			for (Answer answer : questionsAndAnswers.get(question).values()) {
				// answer.setQuestion(question);
				if (!answer.getDataSets().contains(dataSetKey))
					answer.getDataSets().add(dataSetKey);
				if (!answer.getDatasheets().contains(Key.create(dataSheet)))
					answer.getDatasheets().add(Key.create(dataSheet));

				// if (!answer.getStudies().contains(studyKey))
				// answer.getStudies().add(studyKey);
			}
			allAnswers.addAll(questionsAndAnswers.get(question).values());
		}

		// SAVE all answers
		OfyService.ofy().save().entities(allAnswers).now();

		for (Question question : questionsAndAnswers.keySet()) {
			ResourceList<Answer> answersForSingleQuestion = new ResourceList<Answer>(
					questionsAndAnswers.get(question).values());
			for (Key<Answer> key : answersForSingleQuestion.getKeyList()) {
				if (!question.getAnswers().contains(key))
					question.getAnswers().add(key);
			}
		}

		OfyService.ofy().save().entities(questionsAndAnswers.keySet()).now();

		// Filters and Divisions
		for (Filter filter : FiltersAndDivisions.keySet()) {
			if (!filter.getDataSets().contains(dataSetKey))
				filter.getDataSets().add(dataSetKey);
			if (!filter.getDatasheets().contains(Key.create(dataSheet)))
				filter.getDatasheets().add(Key.create(dataSheet));
			// if (!filter.getStudies().contains(studyKey))
			// filter.getStudies().add(studyKey);
		}
		OfyService.ofy().save().entities(FiltersAndDivisions.keySet()).now();

		for (Filter filter : FiltersAndDivisions.keySet()) {
			for (Division division : FiltersAndDivisions.get(filter)) {
				division.setFilter(filter);
				if (!division.getDataSets().contains(dataSetKey))
					division.getDataSets().add(dataSetKey);
				if (!division.getDatasheets().contains(Key.create(dataSheet)))
					division.getDatasheets().add(Key.create(dataSheet));
				// if (!division.getStudies().contains(studyKey))
				// division.getStudies().add(studyKey);
			}
			allDivisions.addAll(FiltersAndDivisions.get(filter));
		}
		OfyService.ofy().save().entities(allDivisions).now();

		for (Filter filter : FiltersAndDivisions.keySet()) {
			ResourceList<Division> divisionsForSingleFilter = new ResourceList<Division>(
					FiltersAndDivisions.get(filter));
			filter.setDivisions(divisionsForSingleFilter.getKeyList());
		}

		OfyService.ofy().save().entities(FiltersAndDivisions.keySet()).now();

		ResourceList<Question> tempQues = new ResourceList<Question>(questionsAndAnswers.keySet());
		ResourceList<Answer> tempAns = new ResourceList<Answer>(allAnswers);

		wave.setQuestions(tempQues.getKeyList());
		wave.setAnswers(tempAns.getKeyList());
		// System.out.println("setting" + tempQues.getKeyList());
		dataSheet.setQuestions(tempQues.getKeyList());
		dataSheet.setAnswers(tempAns.getKeyList());

		ResourceList<Filter> tempFilters = new ResourceList<Filter>(FiltersAndDivisions.keySet());
		ResourceList<Division> tempDivisions = new ResourceList<Division>(allDivisions);

		wave.setFilters(tempFilters.getKeyList());
		wave.setDivisions(tempDivisions.getKeyList());
		dataSheet.setFilters(tempFilters.getKeyList());
		dataSheet.setDivisions(tempDivisions.getKeyList());

		ResourceList<Brand> tempBrands = new ResourceList<Brand>(allBrands);

		wave.setBrands(tempBrands.getKeyList());
		dataSheet.setBrands(tempBrands.getKeyList());

		OfyService.ofy().save().entities(locationByName.values()).now();

		List<Key<Location>> oldLocations = wave.getLocations();
		for (Location location : locationByName.values()) {
			if (!oldLocations.contains(Key.create(location))) {
				wave.getLocations().add(Key.create(location));
				dataSheet.setLocation(Key.create(location));
			}
		}

		OfyService.ofy().clear();

		for (Question question : questionsBrands.keySet()) {
			for (Question savedQuestion : questionsAndAnswers.keySet()) {
				if (question.getQuestionText().equals(savedQuestion.getQuestionText())) {
					question.setId(savedQuestion.getId());
					break;
				}
			}
			for (Brand brand : questionsBrands.get(question)) {
				for (Brand savedBrand : allBrands) {
					if (brand.getName().equals(savedBrand.getName())) {
						brand.setId(savedBrand.getId());
						break;
					}
				}
			}
		}

		List<BrandPerQuestionPerDataSetPerLocation> brandPerQuestionPerDataSetPerLocationList = new ArrayList<BrandPerQuestionPerDataSetPerLocation>();

		for (Question question : questionsBrands.keySet()) {
			ResourceList<Brand> brandsResourceList = new ResourceList<Brand>(questionsBrands.get(question));
			List<Key<Brand>> brandsKeys = brandsResourceList.getKeyList();
			BrandPerQuestionPerDataSetPerLocation brandPerQuestionPerDataSetPerLocation = new BrandPerQuestionPerDataSetPerLocation();
			brandPerQuestionPerDataSetPerLocation.setBrands(brandsKeys);
			Location location = null;
			if (!locationByName.values().isEmpty()) {
				List<Location> locations = new ArrayList<Location>(locationByName.values());
				location = locations.get(0);
			}
			brandPerQuestionPerDataSetPerLocation
					.setId("W" + wave.getId() + "Q" + question.getId() + "L" + location.getId());
			brandPerQuestionPerDataSetPerLocationList.add(brandPerQuestionPerDataSetPerLocation);
		}

		OfyService.ofy().save().entities(brandPerQuestionPerDataSetPerLocationList).now();

		logger.error("END: ");
		OfyService.ofy().clear();
		OfyService.ofy().save().entity(wave).now();
		OfyService.ofy().clear();

	}

	public List<String> returnAnswersNames(List<Answer> Answers) {
		ArrayList<String> answersNames = new ArrayList<String>();
		for (Answer answer : Answers) {
			answersNames.add(answer.getAnswerText());
		}
		return answersNames;
	}

	/**
	 * @param filters
	 * @return
	 */
	public List<String> returnFiltersNames(List<Filter> filters) {
		ArrayList<String> filtersNames = new ArrayList<String>();
		for (Filter filter : filters) {
			filtersNames.add(filter.getName());
		}
		return filtersNames;
	}

	/**
	 * @param divisions
	 * @return
	 */
	public List<String> returnDivisionsNames(List<Division> divisions) {
		ArrayList<String> divisionsNames = new ArrayList<String>();
		for (Division division : divisions) {
			// divisionsNames.add(division.getName());
		}
		return divisionsNames;
	}

	public boolean validateQuestionsAndAnswers(Map<Question, Map<String, Answer>> questionsAndAnswers, DataSet wave) {
		boolean validated = false;
		List<Question> missingQuestions = new ArrayList<Question>();
		List<Question> extraQuestions = new ArrayList<Question>();
		List<Question> commonQuestions = new ArrayList<Question>();

		Map<String, Map<String, ArrayList<Answer>>> commonQuestionsWithDifferentAnswers = new HashMap<String, Map<String, ArrayList<Answer>>>();

		List<Key<Question>> oldQuestionsKeys = wave.getQuestions();
		List<Question> oldQuestions = new ArrayList<Question>(OfyService.ofy().load().keys(oldQuestionsKeys).values());

		List<String> questionsNames = new ArrayList<String>();
		for (Question question : questionsAndAnswers.keySet()) {
			questionsNames.add(question.getQuestionText());
			if (!oldQuestions.contains(question)) {
				extraQuestions.add(question);
			} else {
				commonQuestions.add(question);
			}
		}
		for (Question question : oldQuestions) {
			if (!questionsAndAnswers.keySet().contains(question)) {
				missingQuestions.add(question);
			}
		}
		for (Question question : commonQuestions) {
			List<Answer> rawAns = new ArrayList<Answer>(questionsAndAnswers.get(question).values());
			commonQuestionsWithDifferentAnswers.put(question.getQuestionText(),
					new HashMap<String, ArrayList<Answer>>());
			commonQuestionsWithDifferentAnswers.get(question.getQuestionText()).put("Missing Answers",
					new ArrayList<Answer>());
			commonQuestionsWithDifferentAnswers.get(question.getQuestionText()).put("Extra Answers",
					new ArrayList<Answer>());

			List<Answer> oldAnswers = OfyService.ofy().load().type(Answer.class).ancestor(question)
					.filter("dataSets", Key.create(wave)).list();

			// System.out.println("old " + returnAnswersNames(oldAnswers));
			// System.out.println("new " + returnAnswersNames(rawAns));

			// System.out.println(wave.getId());
			// System.out.println(question.getId());

			for (Answer answer : rawAns) {
				if (!oldAnswers.contains(answer)) {
					commonQuestionsWithDifferentAnswers.get(question.getQuestionText()).get("Extra Answers")
							.add(answer);
				}
			}
			for (Answer answer : oldAnswers) {
				if (!rawAns.contains(answer)) {
					commonQuestionsWithDifferentAnswers.get(question.getQuestionText()).get("Missing Answers")
							.add(answer);
				}
			}
		}

		String extraQuestionsString = "";
		String missingQuestionsString = "";
		String commonFilesWithProblems = "";

		for (String questionName : commonQuestionsWithDifferentAnswers.keySet()) {
			List<String> tempStatus = new ArrayList<String>();
			for (String status : commonQuestionsWithDifferentAnswers.get(questionName).keySet()) {
				ArrayList<Answer> answers = commonQuestionsWithDifferentAnswers.get(questionName).get(status);
				if (!answers.isEmpty()) {
					tempStatus.add(status);
					// System.out.println(status + " not empty");
				}
			}
			if (!tempStatus.isEmpty()) {
				commonFilesWithProblems += "Question \"" + questionName + "\": \n";
				for (String string : tempStatus) {
					commonFilesWithProblems += "    " + string + ": "
							+ returnAnswersNames(commonQuestionsWithDifferentAnswers.get(questionName).get(string))
									.toString()
							+ ". \n";
				}
			}
		}
		if (missingQuestions.isEmpty() && extraQuestions.isEmpty() && commonFilesWithProblems.equals("")) {
			validated = true;
			logFile.setQuestionsStatus("<strong>\"Validated\" <strong>" + questionsNames.toString() + ". \n");
		} else {
			validated = false;
			if (!extraQuestions.isEmpty()) {
				List<String> extraQuestionsList = returnQuestionsNames(extraQuestions);
				String extraQuestionsListString = "<ul>";
				for (String string : extraQuestionsList) {
					extraQuestionsListString += "<li>" + string + "</li>";
				}
				extraQuestionsString = "<br><strong>Questions Existing in the new datasheet only:</strong>"
						+ extraQuestionsListString + ". \n";
			}
			if (!missingQuestions.isEmpty()) {
				List<String> missingQuestionsList = returnQuestionsNames(missingQuestions);
				String missingQuestionsListString = "<ul>";
				for (String string : missingQuestionsList) {
					missingQuestionsListString += "<li>" + string + "</li>";
				}
				missingQuestionsListString += "</ul>";
				missingQuestionsString = "<br><strong>Questions that are missing from the new dataSheet:</strong>\n"
						+ missingQuestionsListString + ". \n";
			}

			logFile.setQuestionsStatus("<strong>\"Invalid Questions set\"</strong> " + extraQuestionsString
					+ missingQuestionsString + commonFilesWithProblems);
		}
		// System.out.println(validated + extraQuestionsString
		// + missingQuestionsString);
		return validated;
	}

	public List<String> returnQuestionsNames(List<Question> questions) {
		ArrayList<String> questionsNames = new ArrayList<String>();
		for (Question question : questions) {
			questionsNames.add(question.getQuestionText());
		}
		return questionsNames;
	}

	public boolean validateFiltersAndDivisions(Map<Filter, Set<Division>> filtersAndDivisions, DataSet wave) {
		boolean validated = false;
		List<Filter> missingFilters = new ArrayList<Filter>();
		List<Filter> extraFilters = new ArrayList<Filter>();
		List<Filter> commonFilters = new ArrayList<Filter>();
		logger.info("Validating Filters ");
		Map<String, Map<String, ArrayList<Division>>> commonFiltersWithDifferentDivisions = new HashMap<String, Map<String, ArrayList<Division>>>();

		List<Key<Filter>> oldFiltersKeys = wave.getFilters();
		List<Filter> oldFilters = new ArrayList<Filter>(OfyService.ofy().load().keys(oldFiltersKeys).values());
		List<String> filtersNames = new ArrayList<String>();

		// System.out.println("Old Filters: " + returnFiltersNames(oldFilters));

		for (Filter filter : filtersAndDivisions.keySet()) {
			filtersNames.add(filter.getName());
			if (!oldFilters.contains(filter)) {
				extraFilters.add(filter);
			} else {
				commonFilters.add(filter);
			}
		}
		for (Filter filter : oldFilters) {
			if (!filtersAndDivisions.keySet().contains(filter)) {
				missingFilters.add(filter);
			}
		}

		for (Filter filter : commonFilters) {
			List<Division> rawDivisions = new ArrayList<Division>(filtersAndDivisions.get(filter));
			commonFiltersWithDifferentDivisions.put(filter.getName(), new HashMap<String, ArrayList<Division>>());
			commonFiltersWithDifferentDivisions.get(filter.getName()).put("Missing Divisions",
					new ArrayList<Division>());
			commonFiltersWithDifferentDivisions.get(filter.getName()).put("Extra Divisions", new ArrayList<Division>());

			List<Division> oldDivisions = OfyService.ofy().load().type(Division.class).ancestor(filter)
					.filter("dataSets", Key.create(wave)).list();

			// System.out.println("old " + returnDivisionsNames(oldDivisions));
			// System.out.println("new " + returnDivisionsNames(rawDivisions));

			// System.out.println(wave.getId());
			// System.out.println(filter.getId());

			for (Division division : rawDivisions) {
				if (!oldDivisions.contains(division)) {
					commonFiltersWithDifferentDivisions.get(filter.getName()).get("Extra Divisions").add(division);
				}
			}
			for (Division division : oldDivisions) {
				if (!rawDivisions.contains(division)) {
					// System.out.println("Mii");
					commonFiltersWithDifferentDivisions.get(filter.getName()).get("Missing Divisions").add(division);
				}
			}
		}

		String extraFiltersString = "";
		String missingFiltersString = "";
		String commonFilesWithProblems = "";

		for (String filterName : commonFiltersWithDifferentDivisions.keySet()) {
			List<String> tempStatus = new ArrayList<String>();
			for (String status : commonFiltersWithDifferentDivisions.get(filterName).keySet()) {
				ArrayList<Division> divisions = commonFiltersWithDifferentDivisions.get(filterName).get(status);
				if (!divisions.isEmpty()) {
					tempStatus.add(status);
					// System.out.println(status + " not empty");
				}
			}
			if (!tempStatus.isEmpty()) {
				commonFilesWithProblems += "Filter \"" + filterName + "\": \n";
				for (String string : tempStatus) {
					commonFilesWithProblems += "    " + string + ": "
							+ returnDivisionsNames(commonFiltersWithDifferentDivisions.get(filterName).get(string))
									.toString()
							+ ". \n";
				}
			}
		}
		if (missingFilters.isEmpty() && extraFilters.isEmpty() && commonFilesWithProblems.equals("")) {
			validated = true;
			logFile.setFiltersStatus("\"Validated\" " + filtersNames.toString() + ". \n");
		} else {
			validated = false;
			if (!extraFilters.isEmpty()) {
				List<String> extraFiltersList = returnFiltersNames(extraFilters);
				String extraFiltersListString = "<ul>";
				for (String string : extraFiltersList) {
					extraFiltersListString += "<li>" + string + "</li>";
				}
				extraFiltersListString += "</ul>";
				extraFiltersString = "<br><strong>Filters Existing in the new datasheet only:</strong>"
						+ extraFiltersListString + "";
			}
			if (!missingFilters.isEmpty()) {
				List<String> missingFiltersList = returnFiltersNames(missingFilters);
				String missingFiltersListString = "";
				missingFiltersListString += "<ul>";
				for (String string : missingFiltersList) {
					missingFiltersListString += "<li>" + string + "</li>";
				}
				missingFiltersListString += "</ul>";
				missingFiltersString = "<br><strong>Filters that are missing from the new dataSheet:</strong>\n"
						+ missingFiltersListString + "";
			}

			logFile.setFiltersStatus(
					"\"Invalid Filters set\" " + extraFiltersString + missingFiltersString + commonFilesWithProblems);
		}

		return validated;
	}

	public void extractEntry(HttpServletResponse resp, @RequestParam("k") Long key) throws IOException {

		DataOperation dataOperation = OfyService.ofy().load().type(DataOperation.class).first().getValue();
		// Checks if there is an ongoing data operation to prevent data conflict
		// errors
		if (dataOperation != null) {
			logger.error("BLOCKED OPERATION! LOCK!");
		} else {

			dataOperation = new DataOperation();
			dataOperation.setMessage(
					"The CMS is currently locked. Data is being imported to the system.\nPlease try again later.");
			logger.info("Lock check passed and System is being Locked...");

			OfyService.ofy().save().entity(dataOperation).now();

			Queue q = QueueFactory.getQueue("importing-queue");

			RetryOptions options = RetryOptions.Builder.withTaskRetryLimit(40);
			options.maxBackoffSeconds(900);
			options.maxDoublings(3);
			options.minBackoffSeconds(10);

			q.add(TaskOptions.Builder.withUrl("/cms/process/extract/extractandvalidate").param("dataSheetKey", key + "")

					.header("Host", BackendServiceFactory.getBackendService().getBackendAddress("importing-backend"))

					.retryOptions(RetryOptions.Builder.withTaskRetryLimit(0)));

		}
		resp.setStatus(HttpServletResponse.SC_OK);
		logger.error("Ended Main Task");

	}

	@RequestMapping(value = "extractandvalidate", method = RequestMethod.GET)
	public void extractAndValidate(@RequestParam("id") long key, HttpServletResponse resp, HttpServletRequest req)
			throws IOException {

		try {
			String currentAdmin = req.getParameter("_user");
			String adminToken = req.getParameter("_admin_token");
			String instanceAppId = SystemProperty.applicationId.get();
			String requestAppId = req.getParameter("_appId");

			Key<ApplicationUser> appUserKey = OfyService.ofy().load().type(ApplicationUser.class)
					.filter("email", currentAdmin).first().getKey();

			AdminTokenGenerator adminTokenGenerator = new AdminTokenGenerator();
			boolean validateToken = adminTokenGenerator.validateToken(adminToken, appUserKey);

			if (validateToken && (instanceAppId.equals(requestAppId))) {
				long start = System.currentTimeMillis();
				System.out.println("VALIDATING...");
				DataSheet localDataSheet = OfyService.ofy().load().key(Key.create(DataSheet.class, key)).get();

				List<LogFile> log = OfyService.ofy().load().type(LogFile.class).ancestor(localDataSheet).list();

				if (!log.isEmpty()) {
					logFile.setId(log.get(0).getId());
				}

				logFile.setDataSheet(Ref.create(localDataSheet));
				logFile.setProcessStartTime(new Date());

				BufferedReader bufferedReader = getCSVData(localDataSheet.getBlobstoreKey(), localDataSheet.getName());

				DataRowController dataRowController = new DataRowController();
				dataRowController.saveDataRows(bufferedReader, Key.create(DataSheet.class, key));

				logger.info("Buffered Reader Done");

				Map<Integer, Question> questionsMap = extractQuestions(localDataSheet);
				replaceNewQuestionsWithAlreadyExisting(questionsMap);

				logger.info("extractQuestions");

				Map<String, Object> objectsMapAnswers = extractAnswers(questionsMap, localDataSheet);

				Map<Question, Map<String, Answer>> answersMapByQuestion = (Map<Question, Map<String, Answer>>) objectsMapAnswers
						.get("answersMapByQuestion");

				logger.info("extractAnswers");

				@SuppressWarnings("unused")
				Map<Integer, Answer> answersMapById = (Map<Integer, Answer>) objectsMapAnswers.get("answersMapById");
				replaceNewAnswersWithAlreadyExisting(answersMapByQuestion);

				Map<String, Object> objectsMapBrands = extractBrands(questionsMap, localDataSheet);

				Map<Question, List<Brand>> brandsByQuestion = (Map<Question, List<Brand>>) objectsMapBrands
						.get("brandsByQuestion");

				replaceNewBrandsWithAlreadyExisting(brandsByQuestion);

				logger.info("extractBrands");

				Map<Integer, Brand> brandsByColumn = (Map<Integer, Brand>) objectsMapBrands.get("brandsMap");

				Map<String, Object> objectsMapFilters = extractFilters(localDataSheet);

				Map<Integer, Filter> filtersMapByCol = (Map<Integer, Filter>) objectsMapFilters.get("filtersMapByCol");

				replaceNewFiltersWithAlreadyExisting(filtersMapByCol);

				logger.info("extractFilters");

				Map<String, Object> objectsMapDivisions = extractDivisionsData(localDataSheet, answersMapByQuestion,
						filtersMapByCol, brandsByQuestion, brandsByColumn);

				Map<Filter, Set<Division>> divisionByFilter = (Map<Filter, Set<Division>>) objectsMapDivisions
						.get("divisionByFilter");

				/*****************
				 * GET BRANDS PER QUESTION
				 ****************************/

				Map<Integer, Double> valuesMap = (Map<Integer, Double>) objectsMapDivisions.get("valuesMap");

				Map<Question, List<Brand>> questionsBrands = getQuestionsBrandsMap(questionsMap, brandsByColumn,
						valuesMap);

				Map<String, Location> locationByName = (Map<String, Location>) objectsMapDivisions
						.get("locationByName");
				DataSet wave = (DataSet) objectsMapDivisions.get("wave");

				Map<String, Study> studyByName = (Map<String, Study>) objectsMapDivisions.get("studyByName");
				Study study = (Study) objectsMapDivisions.get("study");
				boolean validDataSet = false;
				boolean newDataset = false;
				boolean everyThingValidated = true;
				if (validations.keySet().contains("DATASET_VALIDATED")) {
					validDataSet = validations.get("DATASET_VALIDATED");
				}
				if (validDataSet == true) {
					newDataset = validations.get("NEW_DATASET");
					if (newDataset) {
						commitWaveData(study, studyByName, answersMapByQuestion, localDataSheet, brandsByQuestion,
								divisionByFilter, bufferedReader, locationByName, wave, questionsBrands);
					} else {
						boolean questionsValidated = validateQuestionsAndAnswers(answersMapByQuestion, wave);
						boolean filtersValidated = validateFiltersAndDivisions(divisionByFilter, wave);

						if (questionsValidated && filtersValidated) {
							commitWaveData(study, studyByName, answersMapByQuestion, localDataSheet, brandsByQuestion,
									divisionByFilter, bufferedReader, locationByName, wave, questionsBrands);
						} else {
							everyThingValidated = false;
						}
					}

				}
				System.out.println(
						"\n<<<<<<<<<<<< Total Time: " + (System.currentTimeMillis() - start) + " ms. >>>>>>>>>>>>\n");
				validations.put("wholeDataSetValidated", everyThingValidated);

				logger.error("EveryThingValidated: " + everyThingValidated);
				importingDispatcher(resp, key);
				adminTokenGenerator.deleteToken(adminToken);
			} else {
				System.out.println("Token Is Invalid, Or un-Authorized App trying to Connect..");
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		}
	}

	private void lastTask(@PathVariable("dataSheetKey") Long dataSheetKey, HttpServletResponse resp) {

		Queue q = QueueFactory.getQueue("importing-chunk");

		if (q.fetchStatistics().getNumTasks() <= 1) {
			logger.info("Last Task");
			DataSheet dataSheet = OfyService.ofy().load().key(Key.create(DataSheet.class, dataSheetKey)).get();
			dataSheet.setStatus(DataSheet.dataSheetStatus.PROCESSED.getStatus());
			OfyService.ofy().save().entity(dataSheet).now();

			DataOperation dataOperation = OfyService.ofy().load().type(DataOperation.class).first().getValue();
			if (dataOperation != null) {
				ProcessingQueueEntry nextEntry = OfyService.ofy().load().type(ProcessingQueueEntry.class).first()
						.getValue();

				OfyService.ofy().delete().entity(dataOperation).now();
				logger.error("Removing Lock ...");

				if (nextEntry != null) {
					logger.error("Found more entries to be processed ...");
					Long id = nextEntry.getDataSheetKey().getId();
					OfyService.ofy().delete().entity(nextEntry);
					try {
						extractEntry(resp, id);
					} catch (IOException e) {
						logger.error("Failed To Process File");
					}
				}

			}
			Queue queue = QueueFactory.getDefaultQueue();
			queue.add(TaskOptions.Builder.withUrl("/emailmessage/sendmail")
					.param("type", EmailTypes.PROCESSING_COMPLETE.getBit() + "").param("datasheetId", dataSheetKey + "")
					.retryOptions(RetryOptions.Builder.withTaskRetryLimit(10)));
			resp.setStatus(HttpServletResponse.SC_OK);
		} else {
			resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}

	}

	public int count(long key) throws IOException {

		DataSheet dataSheet = OfyService.ofy().load().key(Key.create(DataSheet.class, key)).getValue();

		// BufferedReader bufferedReader = getCSVData(new
		// BlobKey(dataSheet.getBlobstoreKey()), dataSheet.getName());
		int count = 0;
		int rowIndex = FILTER_ANSWERS_START_ROW;

		while (true) {
			// String[] divisionAndfilteredAnswerDataArray =
			// readRow(bufferedReader);
			String[] divisionAndfilteredAnswerDataArray = readDataRow(rowIndex, dataSheet);

			if (divisionAndfilteredAnswerDataArray == null) {
				break;
			}
			if (divisionAndfilteredAnswerDataArray.length == 0) {
				break;
			}
			count++;
			rowIndex++;

		}
		return count;
	}

	private void importingDispatcher(HttpServletResponse resp, @RequestParam("key") Long key) throws IOException {
		System.out.println("\n++++++++++++++Start importingDispatcher+++++++++++++++\n");

		int fileSize = count(key);
		int backendInstanceIndex = 0;
		int backendIndex = 1;
		for (int i = FILTER_ANSWERS_START_ROW; i < fileSize; i += 100) {
			int end = i + 100;
			if (end > fileSize) {
				end = fileSize;
			}

			commitPart(resp, i, end, key);

			backendInstanceIndex++;

			if (backendInstanceIndex == 20) {
				backendInstanceIndex = 0;
				backendIndex++;
			}

			if (backendIndex == 3) {
				backendIndex = 1;
			}
		}

		lastTask(key, resp);

		resp.setStatus(HttpServletResponse.SC_OK);

	}

	private List<List<Key<Division>>> generateCombinationsList(List<List<Key<Division>>> filters, int i) {

		// stop condition
		if (i == filters.size()) {
			// return a list with an empty list
			List<List<Key<Division>>> result = new ArrayList<List<Key<Division>>>();
			result.add(new ArrayList<Key<Division>>());
			return result;
		}

		List<List<Key<Division>>> result = new ArrayList<List<Key<Division>>>();
		List<List<Key<Division>>> recursive = generateCombinationsList(filters, i + 1); // recursive
																						// call

		// for each element of the first list of input
		for (int j = 0; j < filters.get(i).size(); j++) {
			// add the element to all combinations obtained for the rest of the
			// lists
			for (int k = 0; k < recursive.size(); k++) {
				// copy a combination from recursive
				List<Key<Division>> newList = new ArrayList<Key<Division>>();
				for (Key<Division> key : recursive.get(k)) {
					newList.add(key);
				}
				// add element of the first list
				newList.add(filters.get(i).get(j));
				// add new combination to result
				result.add(newList);
			}
		}

		return result;
	}

	public List<List<Key<Division>>> orderFiltersAndDivisions(Map<Filter, Set<Division>> divisionByFilter) {
		List<Filter> tempFilters = new ArrayList<Filter>();
		List<List<Key<Division>>> divisions = new ArrayList<List<Key<Division>>>();
		for (Filter filter : divisionByFilter.keySet()) {
			tempFilters.add(filter);
		}
		Collections.sort(tempFilters, new Comparator<Filter>() {

			public int compare(Filter o1, Filter o2) {
				return o1.getId().compareTo(o2.getId());

			}

		});

		for (Filter filter : tempFilters) {
			Set<Division> tempList = divisionByFilter.get(filter);
			List<Key<Division>> divisionsKeys = new ArrayList<Key<Division>>();
			for (Division division : tempList) {
				divisionsKeys.add(Key.create(division));
			}
			Collections.sort(divisionsKeys, new Comparator<Key<Division>>() {

				public int compare(Key<Division> o1, Key<Division> o2) {
					return (new Long(o1.getId())).compareTo(new Long(o2.getId()));

				}

			});
			divisions.add(divisionsKeys);
		}
		return divisions;
	}

	private int compareLists(List<List<Key<Division>>> combinations, List<Key<Division>> choice) {
		int index = -1;
		boolean found = false;
		for (int i = 0; i < combinations.size(); i++) {
			if (combinations.get(i).size() == choice.size()) {
				for (int k = 0; k < combinations.get(i).size(); k++) {
					if (!combinations.get(i).contains(choice.get(k))) {
						break;
					}
					if (k == combinations.get(i).size() - 1 && combinations.get(i).contains(choice.get(k))) {
						found = true;
						break;
					}
				}
				if (found == true) {
					index = i;
					break;
				}
			}
		}
		return index;
	}
}

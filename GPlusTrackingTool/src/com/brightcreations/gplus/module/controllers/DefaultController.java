/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.controllers;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.brightcreations.gplus.module.controllers.MIDataExplorer.Util;
import com.google.appengine.api.users.UserServiceFactory;

/**
 * @author Ramy Mahmoudi
 * @author mohammed_eladly
 */
@RequestMapping("")
public class DefaultController {
	private Logger logger = LoggerFactory.getLogger(getClass());
	private Util util;

	public DefaultController() {
		util = new Util();
	}
	// @REQUESTMAPPING("")
	// PUBLIC MODELANDVIEW GETHOME() {
	// RETURN NEW MODELANDVIEW("CMS/DASHBOARD");
	// }

	@RequestMapping("")
	public ModelAndView getHome(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// if production show permission screen
//		if (util.isProduction()) {
//			response.sendRedirect("/cms/MIDataExplorer/getOAuth2");
//		}
		return new ModelAndView("cms/dashboard");
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public ModelAndView getLogout() {

		StringBuffer logoutLink = new StringBuffer(UserServiceFactory.getUserService().createLogoutURL("/"));
		System.err.println(logoutLink.toString());
		return new ModelAndView("redirect:" + logoutLink.toString());

	}
}
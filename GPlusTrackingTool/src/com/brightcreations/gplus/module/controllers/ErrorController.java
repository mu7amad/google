/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.controllers;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.brightcreations.gplus.web.exception.WebError;
import com.google.gwt.resources.client.ResourceException;

/**
 * @author Ahmed Fawzy
 * @author Ismail Marmoush
 */
@RequestMapping("/error")
public class ErrorController extends AbstractController {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@RequestMapping(value = "/raiseexception", method = RequestMethod.GET)
	public ModelAndView raiseException() throws Exception {
		try {
			double k = 1 / 0;
		} catch (Exception ex) {
			throw new Exception("dividing by zero");
		}
		return new ModelAndView();
	}

	@RequestMapping(value = "/exception", method = RequestMethod.GET)
	public ModelAndView exception(HttpServletRequest req,
			@RequestParam(value = "msg", defaultValue = "Exception happened!", required = false) String message)
			throws ResourceException {
		// printAttributes(req);
		/*
		 * Exception ex = (Exception) req
		 * .getAttribute("javax.servlet.error.exception");
		 */
		final Map<String, Object> model = new HashMap<String, Object>();
		/*
		 * if(ex==null){ System.out.println("exception is null"); }else{
		 * System.out.println("exception isn't null"); //model.put("exception",
		 * ex); model.put("message", ex.getMessage()); }
		 */
		WebError webError = new WebError();
		webError.setMessage(message);
		logger.error(webError.getStatus() + ": " + message);
		model.put("status", webError);
		return new ModelAndView("exception_page", model);
	}

	@RequestMapping(value = "/err", method = RequestMethod.GET)
	public ModelAndView err(@RequestParam("status") final Integer status, HttpServletRequest req)
			throws ResourceException {

		final Map<String, Object> model = new HashMap<String, Object>();
		WebError webError = new WebError();
		webError.setCode(status);
		model.put("status", webError);
		return new ModelAndView("error_page", model);
	}

	private void printAttributes(HttpServletRequest req) {
		Enumeration attributeNames = req.getAttributeNames();
		while (attributeNames.hasMoreElements()) {
			String name = (String) attributeNames.nextElement();
			System.out.print(name + "{");
			Object attr = req.getAttribute(name);
			if (attr instanceof Exception) {
				Exception ex = (Exception) attr;
				System.out.println(ex.getMessage());
			}
		}
	}

	private void printHeaders(HttpServletRequest req) {
		Enumeration headerNames = req.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String name = (String) headerNames.nextElement();
			System.out.print(name + "{");
			Enumeration headers = req.getHeaders(name);
			while (headers.hasMoreElements()) {
				String value = (String) headers.nextElement();
				System.out.print(value + ", ");
			}
			System.out.println(name + "}");
			System.out.println("--------------------");
		}
	}
}

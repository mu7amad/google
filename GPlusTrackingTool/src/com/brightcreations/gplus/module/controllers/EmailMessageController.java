/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.brightcreations.gplus.module.model.ApplicationUser;
import com.brightcreations.gplus.module.model.DataSet;
import com.brightcreations.gplus.module.model.DataSheet;
import com.brightcreations.gplus.module.model.EmailMessage;
import com.brightcreations.gplus.module.model.LogFile;
import com.brightcreations.gplus.module.model.Settings;
import com.google.appengine.api.mail.MailService;
import com.google.appengine.api.mail.MailServiceFactory;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;

/*
 * @author Ismail Marmoush
 * @author Mohamed Salah El-Din
 */

@RequestMapping("/emailmessage")
public class EmailMessageController {

	/**
	 * 
	 */
	public EmailMessageController() {
		// TODO Auto-generated constructor stub
	}

	public enum EmailTypes {

		PROCESSING_START(1), PROCESSING_FAILD(2), PROCESSING_COMPLETE(3), ROLLBACK_STARTED(4), ROLLBACK_ENDED(
				5), ADDED_ADMIN(6), ADDED_RESEARCHER(7), ADDED_USER(8);

		private int bit;

		EmailTypes(int bit) {
			this.setBit(bit);
		}

		public String getStatus() {
			return toString();
		}

		/**
		 * @param bit
		 *            the bit to set
		 */
		public void setBit(int bit) {
			this.bit = bit;
		}

		/**
		 * @return the bit
		 */
		public int getBit() {
			return bit;
		}
	}

	private EmailMessage getMessageContent(int type, Long datasetId, Long dataSheetId, Long userId) {
		EmailMessage emailMessage = new EmailMessage();
		switch (type) {
		case 1: {
			// Processing Start
			if (dataSheetId != null) {
				Key<DataSheet> datsheetKey = Key.create(DataSheet.class, dataSheetId);
				DataSheet dataSheet = OfyService.ofy().load().key(datsheetKey).getValue();
				emailMessage.setSubject("GP-BTT Processing Started");
				emailMessage.setContent(dataSheet.getName() + " is being processed");
			}

			break;
		}
		case 2: {
			// Processing Failed
			if (dataSheetId != null) {
				Key<DataSheet> datsheetKey = Key.create(DataSheet.class, dataSheetId);
				LogFile logFile = OfyService.ofy().load().type(LogFile.class).ancestor(datsheetKey).first().getValue();
				emailMessage.setSubject("GP-BTT Processing Failed");
				emailMessage.setContent(logFile.getWaveStatus()
						+ "<br><strong>Questions and Answers status: </strong><br>" + logFile.getQuestionsStatus()
						+ "<br><strong>Filters and Divisions status: </strong><br>" + logFile.getFiltersStatus());
			}

			break;
		}
		case 3: {
			// Processing Complete
			if (dataSheetId != null) {
				Key<DataSheet> datsheetKey = Key.create(DataSheet.class, dataSheetId);
				DataSheet dataSheet = OfyService.ofy().load().key(datsheetKey).getValue();
				emailMessage.setSubject("GP-BTT Processing Complete");
				emailMessage.setContent(dataSheet.getName() + " has been processed");
			}

			break;
		}
		case 4: {
			// Rollback Started
			if (datasetId != null) {
				DataSet dataSet = OfyService.ofy().load().key(Key.create(DataSet.class, datasetId)).getValue();
				List<DataSheet> dataSheets = OfyService.ofy().load().type(DataSheet.class)
						.filter("dataSet", Ref.create(dataSet)).list();

				String content = "";
				content += dataSet.getName() + " is being Rolledback";
				for (DataSheet datasheet : dataSheets) {
					content += "<br><br>Datasheets: ";
					content += "<br>" + datasheet.getName();
				}
				content += "<br><br><strong>Important note:</strong> please don't reply to this email as this is an automated message.";
				emailMessage.setSubject("GP-BTT DataSet Rollback Started");
				emailMessage.setContent(content);
			} else if (dataSheetId != null) {
				Key<DataSheet> datsheetKey = Key.create(DataSheet.class, dataSheetId);
				DataSheet dataSheet = OfyService.ofy().load().key(datsheetKey).getValue();
				emailMessage.setSubject("GP-BTT DataSheet Rollback Started");
				emailMessage.setContent(dataSheet.getName() + " is being rolled back"
						+ "<br><br><strong>Important note:</strong> please don't reply to this email as this is an automated message.");
			}
			break;
		}
		case 5: {
			// Rollback Ended
			if (datasetId != null) {
				// DataSet dataSet = OfyService.ofy().load()
				// .key(Key.create(DataSet.class, datasetId)).getValue();
				// List<DataSheet> dataSheets = OfyService.ofy().load()
				// .type(DataSheet.class)
				// .filter("dataSet", Ref.create(dataSet)).list();
				String content = "<strong>Data set has been rolled back</strong>";
				// content += dataSet.getName() + " has been rolledback";
				// for (DataSheet datasheet : dataSheets) {
				// content += "<br><br>Datasheets: ";
				// content += "<br>" + datasheet.getName();
				// }
				content += "<br><br><strong>Important note:</strong> please don't reply to this email as this is an automated message.";
				emailMessage.setSubject("GP-BTT DataSet Rollback Ended");
				emailMessage.setContent(content);
			} else if (dataSheetId != null) {
				Key<DataSheet> datsheetKey = Key.create(DataSheet.class, dataSheetId);
				LogFile logFile = OfyService.ofy().load().type(LogFile.class).ancestor(datsheetKey).first().getValue();
				emailMessage.setSubject("GP-BTT DataSheet Rollback Ended");
				DataSheet dataSheet = OfyService.ofy().load().key(datsheetKey).getValue();
				String content = "";
				content += "<br>" + dataSheet.getName() + " has been rolledback";
				content += "<br><br><strong>Important note:</strong> please don't reply to this email as this is an automated message.";
				emailMessage.setContent(content);
			}
			break;
		}
		case 6: {
			// Added Admin
			ApplicationUser user = OfyService.ofy().load().key(Key.create(ApplicationUser.class, userId)).getValue();
			emailMessage.setSubject("Google+ Brand Tracker");
			emailMessage.setContent("Dear " + user.getFirstName()
					+ ",<br>	You were added to the Google+ Brand Tracking Project as an administrator. Please login using your Google account to start using the system."
					+ "<br><br>Regards,<br>" + "Google+ Brand Tracking Project Team"
					+ "<br><br><strong>Important note:</strong> please don't reply to this email as this is an automated message.");
			break;
		}
		case 7: {
			// Added Researcher
			ApplicationUser appuser = OfyService.ofy().load().key(Key.create(ApplicationUser.class, userId)).getValue();
			emailMessage.setSubject("Google+ Brand Tracker");
			emailMessage.setContent("Dear " + appuser.getFirstName()
					+ ",<br>	You were added to the Google+ Brand Tracking Project as a researcher. Please login using your Google account to start using the system."
					+ "<br><br>Regards,<br>" + "Google+ Brand Tracking Project Team"
					+ "<br><br><strong>Important note:</strong> please don't reply to this email as this is an automated message.");

			break;
		}
		case 8: {
			// Added User
			ApplicationUser user = OfyService.ofy().load().key(Key.create(ApplicationUser.class, userId)).getValue();
			emailMessage.setSubject("Google+ Brand Tracker");
			emailMessage.setContent("Dear " + user.getFirstName()
					+ ",<br>	You were added to the Google+ Brand Tracking Project as a user. Please login using your Google account to start using the system."
					+ "<br><br>Regards,<br>" + "Google+ Brand Tracking Project Team"
					+ "<br><br><strong>Important note:</strong> please don't reply to this email as this is an automated message.");
			break;
		}
		}
		return emailMessage;
	}

	@RequestMapping(value = "/sendmail", method = RequestMethod.POST)
	private void sendMail(HttpServletResponse response, HttpServletRequest request,
			@RequestParam(value = "type", required = false) int type,
			@RequestParam(value = "datasetId", required = false) Long datasetId,
			@RequestParam(value = "datasheetId", required = false) Long dataSheetId,
			@RequestParam(value = "userId", required = false) Long userId) {

		logger.error("Sending Email...");
		Settings settingsEntity = OfyService.ofy().load().type(Settings.class).first().getValue();
		if (settingsEntity != null && settingsEntity.getEmail() != null) {

			List<String> toEmailAddresses = new ArrayList<String>();
			List<ApplicationUser> toUsers = new ArrayList<ApplicationUser>();

			if (type != 6 && type != 7 && type != 8) {
				toUsers = OfyService.ofy().load().type(ApplicationUser.class).filter("onlyUser", Boolean.FALSE).list();
			} else {
				toUsers.add(OfyService.ofy().load().key(Key.create(ApplicationUser.class, userId)).getValue());
			}

			for (ApplicationUser applicationUser : toUsers) {
				toEmailAddresses.add(applicationUser.getEmail());
				logger.error("Emails To Send To: " + applicationUser.getEmail());
			}

			logger.error("detecting type... type: " + type);
			EmailMessage emailMessage = getMessageContent(type, datasetId, dataSheetId, userId);
			sendMultipleEmailMessages(settingsEntity.getEmail(), toEmailAddresses, emailMessage.getContent(),
					emailMessage.getSubject());
			logger.error("Mail Sent");
			response.setStatus(HttpServletResponse.SC_OK);
		} else {
			response.setStatus(HttpServletResponse.SC_OK);
		}
	}

	public void sendMultipleEmailMessages(String fromEmailAddress, List<String> toEmailAddresses, String msgBody,
			String msgSubject) {
		Properties props = new Properties();
		Session session = Session.getDefaultInstance(props, null);

		try {
			Message msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(fromEmailAddress));

			for (String email : toEmailAddresses) {
				msg.addRecipient(Message.RecipientType.BCC, new InternetAddress(email));
			}
			msg.setSubject(msgSubject);
			// msg.setText(msgBody);
			Multipart mp = new MimeMultipart();
			MimeBodyPart htmlPart = new MimeBodyPart();
			htmlPart.setContent(msgBody, "text/html");
			mp.addBodyPart(htmlPart);
			msg.setContent(mp);
			Transport.send(msg);
			logger.error("Email Sent");
		} catch (AddressException e) {
			// ...
		} catch (MessagingException e) {
			// ...
		}
	}

	public void sendGoogleMail(String fromEmailAddress, String toEmailAddresses, String msgBody, String msgSubject)
			throws AddressException, MessagingException {
		try {
			MailService service = MailServiceFactory.getMailService();
			MailService.Message message = new MailService.Message();

			message.setSender(fromEmailAddress);
			message.setTo(toEmailAddresses);
			message.setSubject(msgSubject);
			message.setHtmlBody(msgBody);

			service.send(message);
			System.out.println("++++++++++\nTO: " + message.getTo());
			System.out.println("++++++++++\nFrom: " + message.getSender());
			logger.info("Email Sent");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private Logger logger = LoggerFactory.getLogger(getClass());

	public MimeMessage getMimeMessage(String fromAddress, String toAddress, String ccAddress, String bccAddress,
			String subject, String content) throws AddressException, MessagingException {
		// Initializing
		Properties props = new Properties();
		Session session = Session.getDefaultInstance(props, null);
		MimeMessage mimeMessage = new MimeMessage(session);
		// Settings
		mimeMessage.setFrom(new InternetAddress(fromAddress));
		if (toAddress != null)
			mimeMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(toAddress));
		if (ccAddress != null)
			mimeMessage.addRecipient(Message.RecipientType.CC, new InternetAddress(ccAddress));
		if (bccAddress != null)
			mimeMessage.addRecipient(Message.RecipientType.BCC, new InternetAddress(bccAddress));
		mimeMessage.setSubject(subject);
		mimeMessage.setContent(content, "text/html");
		return mimeMessage;
	}

	@RequestMapping("/cron/logfilecron")
	public void logFileCron(HttpServletResponse resp) throws IOException {
		logger.info("LogFile Cron job started");
		resp.setStatus(HttpServletResponse.SC_OK);
		resp.setContentType("text/plain");
		PrintWriter out = resp.getWriter();
		Queue queue = QueueFactory.getQueue("logfile-queue");
		try {
			for (int i = 0; i < 5; i++) {
				TaskOptions task = TaskOptions.Builder.withUrl("/emailmessage/sendlogfilemessage");
				queue.add(task.param("key", "" + i).method(Method.GET));
				out.println(task.getUrl());
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.info(e.getMessage());
		}
		out.print("cron is started");
		resp.flushBuffer();
	}

	public void sendEmailMessage(EmailMessage msg) throws AddressException, MessagingException {
		logger.info("Sending Email");
		Transport.send(getMimeMessage(msg.getFromAddress(), msg.getToAddress(), msg.getCcAddress(), msg.getBccAddress(),
				msg.getSubject(), msg.getContent()));
	}

	@RequestMapping("/sendlogfilemessage")
	public void sendLogFileMessageToAdmins(@RequestParam(value = "key", required = false) Long key,
			@RequestParam(value = "recepient", required = false) String recepient, HttpServletResponse resp)
			throws IOException {
		logger.info("Adding Email to Queue");
		logger.info("sendlogfilemessage " + key);
		resp.setStatus(HttpServletResponse.SC_OK);
		resp.setContentType("text/plain");
		PrintWriter out = resp.getWriter();
		out.print("key: " + key);
		resp.flushBuffer();
		LogFile logFile = LogFile.getLogFile(key);
		if (logFile != null && !logFile.isReported()) {
			MailService m = MailServiceFactory.getMailService();
			String fromAddress = Settings.load().getEmail();
			m.sendToAdmins(new MailService.Message(fromAddress, recepient, logFile.getSubject(), logFile.getContent()));
			logFile.setReported(true);
			logFile.save();
			logger.info("Email to Admins sent successfully");
		} else {
			logger.info("Log file wasn't found, or it's already reported");
		}
	}

	public void sendMessage(String fromAddress, String toAddresses, String ccAddresses, String bccAddresses,
			String subject, String content) throws AddressException, MessagingException {
		Transport.send(getMimeMessage(fromAddress, toAddresses, ccAddresses, bccAddresses, subject, content));
	}
}

package com.brightcreations.gplus.module.controllers.MIDataExplorer.customClasses;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.brightcreations.gplus.module.controllers.MIDataExplorer.StudyBackEnd;
import com.brightcreations.gplus.module.controllers.MIDataExplorer.Util;
import com.brightcreations.gplus.web.exception.CustomException;

public class ZipUploader extends HttpServlet {

	private static final long serialVersionUID = -157939581416320051L;

	private Util util;
	private StudyBackEnd studyBackEnd;

	public ZipUploader() {
		studyBackEnd = new StudyBackEnd();
		util = new Util();
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) {
		try {
			String createJsonString = util.createJsonString(studyBackEnd.uploadZipFile(request, response));
			response.setContentType("application/json");

			PrintWriter out = response.getWriter();

			out.print(createJsonString);
			out.flush();
		} catch (IOException | CustomException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

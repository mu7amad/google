package com.brightcreations.gplus.module.controllers.MIDataExplorer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.brightcreations.gplus.module.model.ApplicationUser;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.CredentialRefreshListener;
import com.google.api.client.auth.oauth2.TokenErrorResponse;
import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.oauth2.Oauth2;

@Controller
@RequestMapping("logInController")
public class LogInController {

	private Util util;
	private AdminBackEnd adminBackEnd;
	private List<String> SCOPE = Arrays.asList("https://www.googleapis.com/auth/plus.me",
			"https://www.googleapis.com/auth/plus.stream.write");

	public LogInController() {
		util = new Util();
		adminBackEnd = new AdminBackEnd();
	}

	@RequestMapping(value = "getOAuth2Code", method = RequestMethod.GET)
	public void getOAuth2Code(HttpServletRequest request, HttpServletResponse response) throws IOException {

		System.out.println("******* Login Controller getOAuth2Code ******");

		ApplicationUser loggedInUser = adminBackEnd.getLoggedInUser();

		if (loggedInUser != null) {
			if (loggedInUser.getImgURL() == null || loggedInUser.getImgURL().isEmpty()) {

				Properties readConfigProperties = util.readConfigProperties();

				GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(new NetHttpTransport(),
						new JacksonFactory(), readConfigProperties.getProperty("CLIENT_ID"),
						readConfigProperties.getProperty("CLIENT_SECRET"), SCOPE).setApprovalPrompt("force")
								.setAccessType("offline").build();

				List<String> responseTypes = new ArrayList<>();
				responseTypes.add("code");
				responseTypes.add("token");

				String url = flow.newAuthorizationUrl().setResponseTypes(responseTypes)
						.setRedirectUri(readConfigProperties.getProperty("AUTH_REDIRECT_URL")).build();

				System.out.println(
						"Please open the following URL in your browser then " + "type the authorization code:");
				System.out.println("  " + url);

				response.sendRedirect(url);
			}
		} else
			System.err.println("Logged In User Is Null");

	}

	@RequestMapping(value = "getUserGoogleInfo", method = RequestMethod.GET)
	public void getUserGoogleInfo(HttpServletRequest request, HttpServletResponse response) throws IOException {

		System.out.println("*******Login Controller getUserGoogleInfo*********");

		Properties readConfigProperties = util.readConfigProperties();

		String code = request.getParameter("code");

		Cookie[] cookies = request.getCookies();
		String cookie = "";
		for (int i = 0; i < cookies.length; i++) {
			cookie += cookies[i].getName() + "=" + cookies[i].getValue() + "\n";

		}
		System.out.println("Method: " + request.getMethod());
		System.out.println("Auth Type: " + request.getAuthType());
		System.out.println("Cookie:\n " + cookie);
		System.out.println("CODE: " + code);
		try {
			GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(new NetHttpTransport(),
					new JacksonFactory(), readConfigProperties.getProperty("CLIENT_ID"),
					readConfigProperties.getProperty("CLIENT_SECRET"), SCOPE).setApprovalPrompt("force")
							.setAccessType("offline").build();

			System.out.println("Google Token Response : before execute");

			GoogleTokenResponse tokenResponse = flow.newTokenRequest(code)
					.setRedirectUri(readConfigProperties.getProperty("AUTH_REDIRECT_URL")).execute()
					.setExpiresInSeconds(40L);

			System.out.println("Google Token Response : execute");

			GoogleCredential credential = new GoogleCredential.Builder().setTransport(new NetHttpTransport())
					.setJsonFactory(new JacksonFactory())
					.setClientSecrets(readConfigProperties.getProperty("CLIENT_ID"),
							readConfigProperties.getProperty("CLIENT_SECRET"))
					.addRefreshListener(new CredentialRefreshListener() {
						@Override
						public void onTokenResponse(Credential credential, TokenResponse tokenResponse) {
							// Handle success.
							System.out.println("Credential was refreshed successfully.");
						}

						@Override
						public void onTokenErrorResponse(Credential credential, TokenErrorResponse tokenErrorResponse) {
							// Handle error.
							System.err.println("Credential was not refreshed successfully. "
									+ "Redirect to error page or login screen.");
						}
					}).build();
			System.out.println("Google Credential: build");

			credential.setFromTokenResponse(tokenResponse);

			credential.refreshToken();

			Oauth2 oauth2 = new Oauth2.Builder(new NetHttpTransport(), new JacksonFactory(), credential)
					.setApplicationName("Oauth2").build();
			com.google.api.services.oauth2.model.Userinfo userinfo = oauth2.userinfo().get().execute();
			System.out.println("USER INFO: " + userinfo.toPrettyString());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

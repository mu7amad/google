package com.brightcreations.gplus.module.controllers.MIDataExplorer.customClasses;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.brightcreations.gplus.module.controllers.MIDataExplorer.StudyBackEnd;
import com.brightcreations.gplus.module.controllers.MIDataExplorer.Util;
import com.brightcreations.gplus.web.exception.CustomException;

public class UploadImage extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private Util util;
	private StudyBackEnd studyBackEnd;

	public UploadImage() {
		studyBackEnd = new StudyBackEnd();
		util = new Util();
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		try {
			String jsonString = util.createJsonString(studyBackEnd.uploadImages(request, response));
			response.setContentType("application/json");

			PrintWriter out = response.getWriter();

			out.print(jsonString);
			out.flush();

		} catch (CustomException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}

package com.brightcreations.gplus.module.controllers.MIDataExplorer.customClasses;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.brightcreations.gplus.module.controllers.OfyService;
import com.brightcreations.gplus.module.model.Answer;
import com.brightcreations.gplus.module.model.Division;
import com.brightcreations.gplus.module.model.Filter;
import com.brightcreations.gplus.module.model.Question;
import com.brightcreations.gplus.module.model.SelectorBean;
import com.brightcreations.gplus.module.model.Study;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.googlecode.objectify.Key;

public class DataLoader {
	private DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

	public ServiceOutput loadStudyData(HttpServletRequest request) {
		ServiceOutput output = new ServiceOutput();

		long studyId = Long.parseLong(request.getParameter("studyId"));

		Key<Study> studyKey = Key.create(Study.class, studyId);
		List<Question> questionsList = OfyService.ofy().load().type(Question.class).filter("studyKey", studyKey).list();

		for (Question question : questionsList) {
			Key<Question> questionKey = Key.create(Question.class, question.getId());
			List<Answer> answerList = OfyService.ofy().load().type(Answer.class).filter("questionKey", questionKey)
					.list();
			question.getTransientAnswers().addAll(answerList);
		}

		List<Filter> filterList = OfyService.ofy().load().type(Filter.class).filter("study", studyKey).list();

		for (Filter filter : filterList) {
			Key<Filter> filterKey = Key.create(Filter.class, filter.getId());
			List<Division> divisionList = OfyService.ofy().load().type(Division.class).filter("filterKey", filterKey)
					.list();
			filter.getTransientDivision().addAll(divisionList);
		}

		DataLoaderBean dataLoaderBean = new DataLoaderBean();
		dataLoaderBean.setQuestions(questionsList);
		dataLoaderBean.setFilters(filterList);

		output.setStatusCode("200");
		output.setMessage("Success");
		output.setResult(dataLoaderBean);

		return output;

	}

	public ServiceOutput getFiltersValue(List<Question> questionsList) {
		ServiceOutput output = new ServiceOutput();

		if (questionsList.size() <= 3) {

			for (Question question : questionsList) {
				Map<String, ArrayList<String>> transientFilters = question.getTransientFilters();
				long studyId = question.getStudyKey().getId();
				List<Entity> entities = prepareFilters(transientFilters, studyId);

				for (Entity entity : entities) {
					for (Answer answer : question.getTransientAnswers()) {
						String questionAnswerKey = answer.getQuestionAnswerKey();

						Object property = entity.getProperty(questionAnswerKey);
						if (property != null) {
							System.out.println(property);
							answer.setTransientValue(new Double(property.toString()));
						} else {
							answer.setTransientValue(null);
						}
					}

				}
			}
			output.setStatusCode("200");
			output.setMessage("Success");
			output.setResult(questionsList);
		} else {
			output.setStatusCode("606");
			output.setMessage("Invalid Input");
			output.setResult("");
		}

		return output;
	}

	private List<Entity> prepareFilters(Map<String, ArrayList<String>> transientFilters, long studyId) {
		List<com.google.appengine.api.datastore.Query.Filter> dataStoreFilters = new ArrayList<>();

		for (Map.Entry<String, ArrayList<String>> entry : transientFilters.entrySet()) {
			String filterName = entry.getKey();
			ArrayList<String> filterValues = entry.getValue();

			com.google.appengine.api.datastore.Query.Filter tmp = new Query.FilterPredicate(filterName,
					FilterOperator.EQUAL, filterValues.get(0));
			dataStoreFilters.add(tmp);
		}
		com.google.appengine.api.datastore.Query.CompositeFilter compositeFilter = Query.CompositeFilterOperator
				.and(dataStoreFilters);

		Query q = new Query("lookUpDataTable" + studyId).setFilter(compositeFilter);
		List<Entity> entities = datastore.prepare(q).asList(FetchOptions.Builder.withDefaults());
		return entities;
	}

	class DataLoaderBean {
		private List<Question> questions;
		private List<Filter> filters;

		public List<Question> getQuestions() {
			return questions;
		}

		public void setQuestions(List<Question> questions) {
			this.questions = questions;
		}

		public List<Filter> getFilters() {
			return filters;
		}

		public void setFilters(List<Filter> filters) {
			this.filters = filters;
		}
	}

}

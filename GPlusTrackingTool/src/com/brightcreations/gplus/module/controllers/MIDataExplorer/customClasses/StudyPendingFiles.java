package com.brightcreations.gplus.module.controllers.MIDataExplorer.customClasses;

import java.util.List;

import com.brightcreations.gplus.module.model.DataSheet;
import com.brightcreations.gplus.module.model.Study;

public class StudyPendingFiles {
	private Study study;
	private List<DataSheet> files;

	public Study getStudy() {
		return study;
	}

	public void setStudy(Study study) {
		this.study = study;
	}

	public List<DataSheet> getFiles() {
		return files;
	}

	public void setFiles(List<DataSheet> files) {
		this.files = files;
	}

}

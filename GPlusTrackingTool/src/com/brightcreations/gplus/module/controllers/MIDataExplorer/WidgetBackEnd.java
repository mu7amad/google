package com.brightcreations.gplus.module.controllers.MIDataExplorer;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.brightcreations.gplus.module.controllers.OfyService;
import com.brightcreations.gplus.module.controllers.MIDataExplorer.customClasses.ServiceOutput;
import com.brightcreations.gplus.module.model.ApplicationUser;
import com.brightcreations.gplus.module.model.Study;
import com.brightcreations.gplus.module.model.Widget;
import com.brightcreations.gplus.web.exception.CustomException;
import com.googlecode.objectify.Key;

public class WidgetBackEnd {
	private Logger logger = LoggerFactory.getLogger(getClass());
	private AdminBackEnd adminBackEnd;
	private Util util;

	public WidgetBackEnd() {
		adminBackEnd = new AdminBackEnd();
		util = new Util();
	}

	public ServiceOutput createNewWidgetForLoggedInUser(Widget widget) throws CustomException {
		ServiceOutput output = new ServiceOutput();

		if (widget != null) {

			List<Widget> tmpwidgets = OfyService.ofy().load().type(Widget.class).filter("title", widget.getTitle())
					.list();

			if (util.isNullOrEmpty(tmpwidgets)) {

				ApplicationUser loggedInUser = adminBackEnd.getLoggedInUser();
				Key<ApplicationUser> appUserKey = Key.create(ApplicationUser.class, loggedInUser.getId());

				Key<Study> studyKey = OfyService.ofy().load().type(Study.class).filter("studyID", widget.getStudyID())
						.first().getKey();

				if (studyKey != null && appUserKey != null) {
					widget.setAppUserKey(appUserKey);
					widget.setStudyKey(studyKey);
					widget.setLastModificationDate(new Date());
					widget.setStudyID(null);

					Key<Widget> savedWidgetKey = OfyService.ofy().save().entity(widget).now();

					Widget value = OfyService.ofy().load().type(Widget.class).filter("id", savedWidgetKey.getId())
							.first().getValue();
					output.setStatusCode("200");
					output.setMessage("Success");
					output.setResult(value);
					return output;
				} else {
					output.setStatusCode("404");
					output.setMessage("Null Pointer Exception");
					output.setResult("");
					return output;
				}

			} else {
				output.setStatusCode("403");
				output.setMessage("Widget Already Exists.");
				output.setResult("");
				return output;
			}

		} else {
			output.setStatusCode("404");
			output.setMessage("Null Pointer Exception");
			output.setResult("");
			return output;
		}
	}

	public ServiceOutput renameWidget(Widget widget) throws CustomException {
		ServiceOutput output = new ServiceOutput();

		if (widget != null) {

			Widget oldWidget = OfyService.ofy().load().type(Widget.class).filter("id", widget.getId())
					.filter("deleted", false).first().getValue();

			if (oldWidget != null) {

				List<Widget> tmpwidgets = OfyService.ofy().load().type(Widget.class).filter("title", widget.getTitle())
						.list();

				if (oldWidget.getTitle().equals(widget.getTitle()) || util.isNullOrEmpty(tmpwidgets)) {
					oldWidget.setTitle(widget.getTitle());

					Key<Widget> savedWidgetKey = OfyService.ofy().save().entity(oldWidget).now();

					Widget value = OfyService.ofy().load().type(Widget.class).filter("id", savedWidgetKey.getId())
							.first().getValue();

					output.setStatusCode("200");
					output.setMessage("Success");
					output.setResult(value);
					return output;
				} else {
					output.setStatusCode("403");
					output.setMessage("Widget Title Already Exists.");
					output.setResult("");
					return output;
				}
			} else {
				output.setStatusCode("404");
				output.setMessage("Null Pointer Exception");
				output.setResult("");
				return output;
			}

		} else {
			output.setStatusCode("404");
			output.setMessage("Null Pointer Exception");
			output.setResult("");
			return output;
		}
	}

	public ServiceOutput deleteWidget(Widget widget) throws CustomException {
		ServiceOutput output = new ServiceOutput();

		if (widget.getId() != null) {

			Widget oldWidget = OfyService.ofy().load().type(Widget.class).filter("id", widget.getId())
					.filter("deleted", false).first().getValue();
			if (oldWidget != null) {
				oldWidget.setDeleted(true);

				OfyService.ofy().save().entity(oldWidget).now();

				output.setStatusCode("200");
				output.setMessage("Success");
				output.setResult("");
				return output;

			} else {
				output.setStatusCode("404");
				output.setMessage("Null Pointer Exception");
				output.setResult("");
				return output;
			}

		} else {
			output.setStatusCode("404");
			output.setMessage("Null Pointer Exception");
			output.setResult("");
			return output;
		}
	}

	public ServiceOutput loadLoggedInUserWidgets() throws CustomException {
		ServiceOutput output = new ServiceOutput();

		ApplicationUser loggedInUser = adminBackEnd.getLoggedInUser();

		if (loggedInUser != null) {
			Key<ApplicationUser> appUserKey = Key.create(ApplicationUser.class, loggedInUser.getId());

			List<Widget> list = OfyService.ofy().load().type(Widget.class).filter("appUserKey", appUserKey)
					.filter("deleted", false).list();
			output.setStatusCode("200");
			output.setMessage("Success");
			output.setResult(list);
			return output;
		} else {
			output.setStatusCode("404");
			output.setMessage("Null Pointer Exception.");
			output.setResult("");
			return output;
		}
	}

}

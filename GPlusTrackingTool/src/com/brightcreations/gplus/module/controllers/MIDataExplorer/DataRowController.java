package com.brightcreations.gplus.module.controllers.MIDataExplorer;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;

import com.brightcreations.gplus.module.controllers.OfyService;
import com.brightcreations.gplus.module.model.DataRow;
import com.brightcreations.gplus.module.model.DataSheet;
import com.brightcreations.gplus.module.model.QuestionFilterAnswerRows;
import com.brightcreations.gplus.module.model.RowType;
import com.brightcreations.gplus.module.model.Study;
import com.googlecode.objectify.Key;

@RequestMapping("dataRowController")
public class DataRowController {

	public void saveDataRows(BufferedReader bufferedReader, Key<DataSheet> datasheetKey) {

		List<DataRow> dataRowList = null;

		List<DataRow> datasheetRows = OfyService.ofy().load().type(DataRow.class).filter("datasheetKey", datasheetKey)
				.list();

		if (datasheetRows == null || datasheetRows.isEmpty()) {

			if (bufferedReader != null) {
				dataRowList = new ArrayList<>();
				try {
					String line;
					int count = 0;
					int tracker = 0;

					while ((line = bufferedReader.readLine()) != null) {
						DataRow dataRow = new DataRow();
						dataRow.setDatasheetKey(datasheetKey);

						switch (count) {
						case 0:
							dataRow.setRowType(RowType.QUESTION_INDEX);
							break;
						case 1:
							dataRow.setRowType(RowType.BASE_DATA);
							break;
						case 2:
							dataRow.setRowType(RowType.QUESTION_TEXT);
							break;
						case 3:
							dataRow.setRowType(RowType.FILTER_AND_ANSWER);
							break;
						default:
							dataRow.setRowType(RowType.DIVISION_AND_DATA);
							break;
						}
						dataRow.setRowValue(line);
						dataRow.setRowNumber(count);
						// OfyService.ofy().save().entity(dataRow).now();
						// System.out.println("Row " + count + " Saved.");

						dataRowList.add(dataRow);

						count++;
						tracker++;

						if (tracker == 500) {
							OfyService.ofy().save().entities(dataRowList).now();
							System.out.println("500 Records Inserted..");
							tracker = 0;
							dataRowList = new ArrayList<>();
						}
					} // end while

					if (!dataRowList.isEmpty() && dataRowList != null) {
						OfyService.ofy().save().entities(dataRowList).now();
					}
					System.out.println("\n+++++Saved Data Rows+++++\n");

				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						if (bufferedReader != null)
							bufferedReader.close();
					} catch (IOException ex) {
						ex.printStackTrace();
					}
				}
			}
		} else {
			System.out.println("Data Sheet Already Saved.");
		}
	}

	public void saveDataRows_NewFile(BufferedReader bufferedReader, Key<DataSheet> datasheetKey,
			boolean isFoundSheetProcessedInStudy) {

		List<DataRow> dataRowList = null;

		List<DataRow> datasheetRows = OfyService.ofy().load().type(DataRow.class).filter("datasheetKey", datasheetKey)
				.list();

		if (datasheetRows == null || datasheetRows.isEmpty()) {

			if (bufferedReader != null) {

				QuestionFilterAnswerRows questionFilterAnswerRow = null;
				if (!isFoundSheetProcessedInStudy) {
					questionFilterAnswerRow = new QuestionFilterAnswerRows();
				}
				dataRowList = new ArrayList<>();
				try {
					String line;
					int count = 0;
					int tracker = 0;

					while ((line = bufferedReader.readLine()) != null) {
						DataRow dataRow = new DataRow();
						dataRow.setDatasheetKey(datasheetKey);

						switch (count) {
						case 0:
							dataRow.setRowType(RowType.UNKNOWN);
							break;
						case 1:
							dataRow.setRowType(RowType.BASE_DATA);
							break;
						case 2:
							dataRow.setRowType(RowType.QUESTION_TEXT);
							if (!isFoundSheetProcessedInStudy) {
								questionFilterAnswerRow.setQuestionRow(line);
							}
							break;
						case 3:
							dataRow.setRowType(RowType.FILTER_AND_ANSWER);
							if (!isFoundSheetProcessedInStudy) {
								questionFilterAnswerRow.setFilterAnswerRow(line);
							}
							break;
						default:
							dataRow.setRowType(RowType.DIVISION_AND_DATA);
							break;
						}
						dataRow.setRowValue(line);
						dataRow.setRowNumber(count);

						if (!isFoundSheetProcessedInStudy && questionFilterAnswerRow.getQuestionRow() != null
								&& questionFilterAnswerRow.getFilterAnswerRow() != null) {
							
							System.out.println("only first time");
							Key<Study> studyKey = OfyService.ofy().load().type(DataSheet.class)
									.filter("id", datasheetKey.getId()).first().getValue().getStudy().getKey();

							questionFilterAnswerRow.setStudyKey(studyKey);

							OfyService.ofy().save().entity(questionFilterAnswerRow).now();
							questionFilterAnswerRow = new QuestionFilterAnswerRows();
						}

						dataRowList.add(dataRow);

						count++;
						tracker++;

						if (tracker == 500) {
							OfyService.ofy().save().entities(dataRowList).now();
							System.out.println("500 Records Inserted..");
							tracker = 0;
							dataRowList = new ArrayList<>();
						}
					} // end while

					if (!dataRowList.isEmpty() && dataRowList != null) {
						OfyService.ofy().save().entities(dataRowList).now();
					}
					System.out.println("\n+++++Saved Data Rows+++++\n");

				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						if (bufferedReader != null)
							bufferedReader.close();
					} catch (IOException ex) {
						ex.printStackTrace();
					}
				}
			}
		} else {
			System.out.println("Data Sheet Already Saved.");
		}
	}

	public void deleteRowData(DataSheet dataSheet) {

		while (true) {
			List<DataRow> list = OfyService.ofy().load().type(DataRow.class)
					.filter("datasheetKey", Key.create(DataSheet.class, dataSheet.getId())).limit(500).list();
			if (list == null || list.isEmpty() || list.get(0) == null)
				break;

			OfyService.ofy().delete().entities(list).now();
		}
		System.out.println("********** Data Row for Sheet: " + dataSheet.getName() + " Deleted *******");
	}

	public static void main(String[] args) throws IOException {
		int x = 500000;
		System.out.println(x);
	}

}

package com.brightcreations.gplus.module.controllers.MIDataExplorer.customClasses;

public class ServiceOutput {
	private String statusCode;
	private String message;
	private Object result;
	private String curser;

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	public String getCurser() {
		return curser;
	}

	public void setCurser(String curser) {
		this.curser = curser;
	}

}

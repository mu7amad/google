/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.controllers.MIDataExplorer;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.brightcreations.gplus.module.controllers.EmailMessageController;
import com.brightcreations.gplus.module.controllers.OfyService;
import com.brightcreations.gplus.module.controllers.MIDataExplorer.customClasses.ServiceOutput;
import com.brightcreations.gplus.module.model.ApplicationUser;
import com.brightcreations.gplus.module.model.CmsDashBoard;
import com.brightcreations.gplus.module.model.ConfigEntity;
import com.brightcreations.gplus.module.model.DataSheet;
import com.brightcreations.gplus.module.model.DataSheetsZipEntry;
import com.brightcreations.gplus.module.model.Study;
import com.brightcreations.gplus.web.exception.CustomException;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.CredentialRefreshListener;
import com.google.api.client.auth.oauth2.TokenErrorResponse;
import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.oauth2.Oauth2;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.appengine.api.utils.SystemProperty;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;

/**
 * 
 * @author mohammed eladly
 */

@Controller
@RequestMapping("/cms/MIDataExplorer")
public class CmsMIDataExplorer {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private List<String> SCOPE = Arrays.asList("https://www.googleapis.com/auth/plus.me",
			"https://www.googleapis.com/auth/plus.stream.write");

	private StudyBackEnd studyBackEnd;
	private AdminBackEnd adminBackEnd;
	private Util util;

	public CmsMIDataExplorer() {
		studyBackEnd = new StudyBackEnd();
		adminBackEnd = new AdminBackEnd();
		util = new Util();
	}

	@RequestMapping("")
	public ModelAndView openCMSDashBoard() {
		return new ModelAndView("fake_pages/dashboard_test");
		// return new ModelAndView("cms/dashboard");
	}

	/* ______________________________________________________________________________________ */
	/*************************************** Admins ******************************************/
	/* ____________________________________________________________________________________ */

	@RequestMapping("openAdminsPage")
	public ModelAndView openAdminsPage() {
		return new ModelAndView("fake_pages/admins_test");
	}

	@RequestMapping(value = "loadAdmins", method = RequestMethod.GET)
	@ResponseBody
	public String loadAdmins(HttpServletRequest request, HttpServletResponse response) {

		try {
			ServiceOutput loadAdmins = adminBackEnd.loadAdmins(request);

			return util.createJsonString(loadAdmins);
		} catch (IOException e) {
			logger.error(e.getMessage());
			return "Error: " + e.getMessage();
		}

	}

	@RequestMapping(value = "createNewAdmin", method = RequestMethod.PUT)
	@ResponseBody
	public String createNewAdmin(@RequestBody ApplicationUser newAdmin, HttpServletRequest request,
			HttpServletResponse response) throws CustomException {

		logger.info("create new admin");

		String jsonResult = null;
		try {
			String firstName = newAdmin.getFirstName();
			String lastName = newAdmin.getLastName();
			String email = newAdmin.getEmail();
			String status = newAdmin.getStatus();

			ServiceOutput createNewAdmin = adminBackEnd.createNewAdmin(firstName, lastName, email, status);

			jsonResult = util.createJsonString(createNewAdmin);

			if (createNewAdmin.getStatusCode().equals("200")) {
				if (util.checkProductionEnv()) {
					String mailSubject = OfyService.ofy().load().type(ConfigEntity.class).filter("key", "MAIL_SUBJECT")
							.first().getValue().getValue();
					String mailBody = OfyService.ofy().load().type(ConfigEntity.class).filter("key", "NEW_ADMIN")
							.first().getValue().getValue();

					EmailMessageController emailMessageController = new EmailMessageController();
					emailMessageController.sendGoogleMail("mohammed.eladly@brightcreations.com", email.trim(),
							String.format(mailBody, util.capitalize(firstName)), mailSubject);
				}
			}

		} catch (CustomException e) {
			logger.error(e.getMessage());
			// return "Error: " + e.getMessage();
		} catch (JsonGenerationException e) {
			logger.error(e.getMessage());
			// return "Error: " + e.getMessage();
		} catch (JsonMappingException e) {
			logger.error(e.getMessage());
			// return "Error: " + e.getMessage();
		} catch (IOException e) {
			logger.error(e.getMessage());
			// return "Error: " + e.getMessage();
		} catch (AddressException e) {
			logger.error(e.getMessage());
			// return "Error: " + e.getMessage();
		} catch (MessagingException e) {
			logger.error(e.getMessage());
			// return "Error: " + e.getMessage();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return jsonResult;
	}

	@RequestMapping(value = "editAdmin", method = RequestMethod.PUT)
	@ResponseBody
	public String editAdmin(@RequestBody ApplicationUser admin, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			String createJsonString = null;

			ServiceOutput editAdmin = adminBackEnd.editAdmin(admin);
			Object result = editAdmin.getResult();

			if (result != null && result instanceof ApplicationUser) {
				ApplicationUser applicationUser = (ApplicationUser) editAdmin.getResult();

				createJsonString = util.createJsonString(editAdmin);

				if (editAdmin.getStatusCode().equalsIgnoreCase("200")) {
					if (util.checkProductionEnv()) {

						String mailSubject = OfyService.ofy().load().type(ConfigEntity.class)
								.filter("key", "MAIL_SUBJECT").first().getValue().getValue();
						String mailBody = OfyService.ofy().load().type(ConfigEntity.class).filter("key", "EDIT_ADMIN")
								.first().getValue().getValue();

						EmailMessageController emailMessageController = new EmailMessageController();
						emailMessageController.sendGoogleMail("mohammed.eladly@brightcreations.com",
								applicationUser.getEmail().trim(),
								String.format(mailBody, util.capitalize(applicationUser.getFirstName())), mailSubject);
					}
				}
			}
			return createJsonString;

		} catch (CustomException e) {
			logger.error(e.getMessage());
			return "Error:" + e.getMessage();
		} catch (JsonGenerationException e) {
			logger.error(e.getMessage());
			return "Error:" + e.getMessage();
		} catch (JsonMappingException e) {
			logger.error(e.getMessage());
			return "Error:" + e.getMessage();
		} catch (IOException e) {
			logger.error(e.getMessage());
			return "Error:" + e.getMessage();
		} catch (AddressException e) {
			logger.error(e.getMessage());
			return "Error:" + e.getMessage();
		} catch (MessagingException e) {
			logger.error(e.getMessage());
			return "Error:" + e.getMessage();
		}
	}

	@RequestMapping(value = "deleteAdmin", method = RequestMethod.POST)
	@ResponseBody
	public String deleteAdmin(@RequestBody ApplicationUser admin, HttpServletRequest request,
			HttpServletResponse response) {

		try {
			return util.createJsonString(adminBackEnd.deleteAdmin(admin));
		} catch (CustomException e) {
			logger.error(e.getMessage());
			return "Error:" + e.getMessage();
		} catch (JsonGenerationException e) {
			logger.error(e.getMessage());
			return "Error:" + e.getMessage();
		} catch (JsonMappingException e) {
			logger.error(e.getMessage());
			return "Error:" + e.getMessage();
		} catch (IOException e) {
			logger.error(e.getMessage());
			return "Error:" + e.getMessage();
		}
	}

	@RequestMapping(value = "loadAdminsLookUp", method = RequestMethod.GET)
	@ResponseBody
	public String loadAdminsLookUp(HttpServletRequest request, HttpServletResponse response) {
		try {
			return util.createJsonString(adminBackEnd.loadAdminsLookUp(request));
		} catch (IOException e) {
			logger.error(e.getMessage());
			return "Error: " + e.getMessage();
		}
	}

	@RequestMapping(value = "loadUsersLookUp", method = RequestMethod.GET)
	@ResponseBody
	public String loadUsersLookUp() {
		try {
			return util.createJsonString(adminBackEnd.loadUsersLookUp());
		} catch (IOException e) {
			logger.error(e.getMessage());
			return "Error: " + e.getMessage();
		}
	}

	@RequestMapping(value = "getCurrentUser", method = RequestMethod.GET)
	@ResponseBody
	public String getCurrentUser() {
		try {
			return util.createJsonString(adminBackEnd.getLoggedInUser());
		} catch (IOException e) {
			logger.error(e.getMessage());
			return "Error: " + e.getMessage();
		}
	}

	/* ______________________________________________________________________________________ */
	/*************************************** Study *******************************************/
	/* ____________________________________________________________________________________ */

	@RequestMapping("openStudiesPage")
	public ModelAndView openStudiesPage(HttpServletRequest request, HttpServletResponse response) {

		Map<String, Object> model = new HashMap<>();

		String uploadUrl = studyBackEnd.createUploadURL("/cms/MIDataExplorer/uploadStudyLogo");
		// Study study = new Study();
		// study.setUploadUrl(uploadUrl);
		// String loadAdminsLoopUp = loadAdminsLookUp(request, response);
		// String loadUsersLoopUp = loadUsersLookUp();
		//
		// model.put("study", study);
		// model.put("adminsLookUp", loadAdminsLoopUp);
		// model.put("usersLookUp", loadUsersLoopUp);

		return new ModelAndView("fake_pages/studies_test", model);
	}

	@RequestMapping(value = "createUploadUrl", method = RequestMethod.GET)
	@ResponseBody
	public String createUploadUrl(HttpServletRequest request, HttpServletResponse response) {

		try {
			// String uploadUrl =
			// studyBackEnd.createUploadURL("/cms/MIDataExplorer/uploadStudyLogo");
			String uploadUrl = studyBackEnd.createUploadURL("/uploadImage");

			System.out.println("UPLOAD URL: " + uploadUrl);

			return util.createJsonString(uploadUrl);
		} catch (IOException e) {
			logger.error(e.getMessage());
			return "Error:" + e.getMessage();
		}
	}

	@RequestMapping(value = "createUploadZipUrl", method = RequestMethod.GET)
	@ResponseBody
	public String createUploadZipUrl(HttpServletRequest request, HttpServletResponse response) {

		try {
			String uploadUrl = studyBackEnd.createUploadURL("/zipUploader");

			System.out.println("UPLOAD URL: " + uploadUrl);

			return util.createJsonString(uploadUrl);
		} catch (IOException e) {
			logger.error(e.getMessage());
			return "Error:" + e.getMessage();
		}
	}

	@RequestMapping(value = "uploadStudyLogo", method = RequestMethod.POST)
	@ResponseBody
	public String uploadStudyLogo(HttpServletRequest req, HttpServletResponse res) {
		try {
			System.out.println("uploadStudyLogo");
			System.out.println(req.toString());
			Study study = studyBackEnd.uploadImages(req, res);

			String result = util.createJsonString(study);
			return result;
		} catch (IOException | CustomException e) {
			logger.error(e.getMessage());
			return "Error:" + e.getMessage();
		}

	}

	@RequestMapping(value = "createNewStudy", method = RequestMethod.PUT)
	@ResponseBody
	public String createNewStudy(@RequestBody Study study, HttpServletRequest req, HttpServletResponse res)
			throws JSONException, IOException {

		try {
			return util.createJsonString(studyBackEnd.createNewStudy(study));
		} catch (CustomException e) {
			logger.error(e.getMessage());
			return "Error: " + e.getMessage();
		} catch (IOException e) {
			logger.error(e.getMessage());
			return "Error: " + e.getMessage();
		}
	}

	@RequestMapping(value = "loadStudies", method = RequestMethod.GET)
	@ResponseBody
	public String loadStudies() {
		try {
			return util.createJsonString(studyBackEnd.loadStudies());

		} catch (IOException e) {
			logger.error(e.getMessage());
			return "Error: " + e.getMessage();
		}
	}

	@RequestMapping(value = "deleteStudy", method = RequestMethod.POST)
	@ResponseBody
	public String deleteStudy(@RequestBody Study study, HttpServletRequest req, HttpServletResponse res) {
		try {
			return util.createJsonString(studyBackEnd.deleteStudy(study));
		} catch (IOException e) {
			logger.error(e.getMessage());
			return "Error: " + e.getMessage();
		} catch (CustomException e) {
			logger.error(e.getMessage());
			return "Error: " + e.getMessage();
		}
	}

	@RequestMapping(value = "editStudy", method = RequestMethod.POST)
	@ResponseBody
	public String editStudy(@RequestBody Study study, HttpServletRequest req, HttpServletResponse res) {
		try {
			return util.createJsonString(studyBackEnd.updateStudy(study));
		} catch (CustomException e) {
			logger.error(e.getMessage());
			return "Error: " + e.getMessage();
		} catch (JsonGenerationException e) {
			logger.error(e.getMessage());
			return "Error: " + e.getMessage();
		} catch (JsonMappingException e) {
			logger.error(e.getMessage());
			return "Error: " + e.getMessage();
		} catch (IOException e) {
			logger.error(e.getMessage());
			return "Error: " + e.getMessage();
		}
	}

	@RequestMapping(value = "getStudyById", method = RequestMethod.POST)
	@ResponseBody
	public String getStudyById(@RequestBody Study study, HttpServletRequest req, HttpServletResponse res) {
		try {
			return util.createJsonString(studyBackEnd.getStudyById(study));
		} catch (CustomException e) {
			logger.error(e.getMessage());
			return "Error: " + e.getMessage();
		} catch (JsonGenerationException e) {
			logger.error(e.getMessage());
			return "Error: " + e.getMessage();
		} catch (JsonMappingException e) {
			logger.error(e.getMessage());
			return "Error: " + e.getMessage();
		} catch (IOException e) {
			logger.error(e.getMessage());
			return "Error: " + e.getMessage();
		}
	}

	@RequestMapping(value = "getLoggedInUserStudies", method = RequestMethod.GET)
	@ResponseBody
	public String getLoggedInUserStudies(HttpServletRequest req, HttpServletResponse res) {
		ApplicationUser loggedInUser = adminBackEnd.getLoggedInUser();
		try {
			return util.createJsonString(studyBackEnd.getUserStudies(loggedInUser));
		} catch (CustomException e) {
			logger.error(e.getMessage());
			return "ERROR: " + e.getMessage();
		} catch (JsonGenerationException e) {
			logger.error(e.getMessage());
			return "ERROR: " + e.getMessage();
		} catch (JsonMappingException e) {
			logger.error(e.getMessage());
			return "ERROR: " + e.getMessage();
		} catch (IOException e) {
			logger.error(e.getMessage());
			return "ERROR: " + e.getMessage();
		}
	}

	@RequestMapping(value = "getLoggedInSharedWithStudies", method = RequestMethod.GET)
	@ResponseBody
	public String getLoggedInSharedWithStudies(HttpServletRequest req, HttpServletResponse res) {
		ApplicationUser loggedInUser = adminBackEnd.getLoggedInUser();
		try {
			return util.createJsonString(studyBackEnd.getLoggedInSharedWithStudies(loggedInUser));
		} catch (CustomException e) {
			logger.error(e.getMessage());
			return "ERROR: " + e.getMessage();
		} catch (JsonGenerationException e) {
			logger.error(e.getMessage());
			return "ERROR: " + e.getMessage();
		} catch (JsonMappingException e) {
			logger.error(e.getMessage());
			return "ERROR: " + e.getMessage();
		} catch (IOException e) {
			logger.error(e.getMessage());
			return "ERROR: " + e.getMessage();
		}
	}

	@RequestMapping(value = "getLoggedAssignedStudies", method = RequestMethod.GET)
	@ResponseBody
	public String getLoggedAssignedStudies(HttpServletRequest req, HttpServletResponse res) {
		ApplicationUser loggedInUser = adminBackEnd.getLoggedInUser();
		try {
			return util.createJsonString(studyBackEnd.getLoggedAssignedStudies(loggedInUser));
		} catch (CustomException e) {
			logger.error(e.getMessage());
			return "ERROR: " + e.getMessage();
		} catch (JsonGenerationException e) {
			logger.error(e.getMessage());
			return "ERROR: " + e.getMessage();
		} catch (JsonMappingException e) {
			logger.error(e.getMessage());
			return "ERROR: " + e.getMessage();
		} catch (IOException e) {
			logger.error(e.getMessage());
			return "ERROR: " + e.getMessage();
		}
	}

	@RequestMapping(value = "searchStudy", method = RequestMethod.GET)
	@ResponseBody
	public String searchStudy(HttpServletRequest req, HttpServletResponse res) {

		try {
			String searchWord = req.getParameter("searchWord");
			return util.createJsonString(studyBackEnd.findByPrefixStudyName(searchWord));
		} catch (CustomException e) {
			logger.error(e.getMessage());
			return "ERROR: " + e.getMessage();
		} catch (JsonGenerationException e) {
			logger.error(e.getMessage());
			return "ERROR: " + e.getMessage();
		} catch (JsonMappingException e) {
			logger.error(e.getMessage());
			return "ERROR: " + e.getMessage();
		} catch (IOException e) {
			logger.error(e.getMessage());
			return "ERROR: " + e.getMessage();
		}
	}

	@RequestMapping(value = "openStudy", method = RequestMethod.POST)
	@ResponseBody
	public String openStudy(@RequestBody Study study, HttpServletRequest req, HttpServletResponse res) {
		try {
			return util.createJsonString(studyBackEnd.openStudy(study));
		} catch (CustomException e) {
			logger.error("ERROR: " + e.getMessage());
			return "ERROR: " + e.getMessage();
		} catch (JsonGenerationException e) {
			logger.error("ERROR: " + e.getMessage());
			return "ERROR: " + e.getMessage();
		} catch (JsonMappingException e) {
			logger.error("ERROR: " + e.getMessage());
			return "ERROR: " + e.getMessage();
		} catch (IOException e) {
			logger.error("ERROR: " + e.getMessage());
			return "ERROR: " + e.getMessage();
		}
	}
	
	@RequestMapping(value = "openStudyDataSheets", method = RequestMethod.POST)
	@ResponseBody
	public String openStudyDataSheets(@RequestBody Study study, HttpServletRequest req, HttpServletResponse res) {
		try {
			return util.createJsonString(studyBackEnd.openStudyDataSheets(study));
		} catch (CustomException e) {
			logger.error("ERROR: " + e.getMessage());
			return "ERROR: " + e.getMessage();
		} catch (JsonGenerationException e) {
			logger.error("ERROR: " + e.getMessage());
			return "ERROR: " + e.getMessage();
		} catch (JsonMappingException e) {
			logger.error("ERROR: " + e.getMessage());
			return "ERROR: " + e.getMessage();
		} catch (IOException e) {
			logger.error("ERROR: " + e.getMessage());
			return "ERROR: " + e.getMessage();
		}
	}
	
	@RequestMapping(value = "viewCsvFiles", method = RequestMethod.POST)
	@ResponseBody
	public String viewCsvFiles(@RequestBody DataSheetsZipEntry zipEntry, HttpServletRequest req,
			HttpServletResponse res) {
		try {
			return util.createJsonString(studyBackEnd.getCsvFilesInZipFile(zipEntry));
		} catch (IOException | CustomException e) {
			logger.error("ERROR: " + e.getMessage());
			return "ERROR: " + e.getMessage();
		}
	}

	@RequestMapping(value = "deleteCsvFile", method = RequestMethod.POST)
	@ResponseBody
	public String deleteCsvFile(@RequestBody DataSheet dataSheet, HttpServletRequest req, HttpServletResponse res) {
		try {
			return util.createJsonString(studyBackEnd.deleteCsvFile(dataSheet));
		} catch (IOException | CustomException e) {
			logger.error("ERROR: " + e.getMessage());
			return "ERROR: " + e.getMessage();
		}
	}

	@RequestMapping(value = "deleteZipFile", method = RequestMethod.POST)
	@ResponseBody
	public String deleteZipFile(@RequestBody DataSheetsZipEntry zipEntry, HttpServletRequest req,
			HttpServletResponse res) {
		try {
			return util.createJsonString(studyBackEnd.deleteZipFile(zipEntry));
		} catch (IOException | CustomException e) {
			logger.error("ERROR: " + e.getMessage());
			return "ERROR: " + e.getMessage();
		}
	}

	@RequestMapping(value = "downloadZipFile", method = RequestMethod.POST)
	@ResponseBody
	public String downloadZipFile(@RequestParam("zipId") long zipEntry, HttpServletRequest req, HttpServletResponse res)
			throws JsonGenerationException, JsonMappingException, IOException {
		ServiceOutput output = new ServiceOutput();

		try {

			BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();

			Ref<DataSheetsZipEntry> ref = OfyService.ofy().load().type(DataSheetsZipEntry.class).filter("id", zipEntry)
					.first();
			if (ref != null) {
				DataSheetsZipEntry dataSheetsZipEntry = ref.getValue();

				Key<Study> studyRef = dataSheetsZipEntry.getStudyRef().getKey();
				Study loadedStudy = OfyService.ofy().load().type(Study.class).filter("id", studyRef.getId()).first()
						.getValue();

				Boolean loggedInUserStudyUploader = studyBackEnd.getLoggedInUserStudyUploader(loadedStudy);

				if (studyBackEnd.isAuthorized(loggedInUserStudyUploader)) {
					BlobKey blobstoreKey = dataSheetsZipEntry.getBlobstoreKey();
					String fileName = dataSheetsZipEntry.getName();
					res.setHeader("Content-Disposition", "attachment; filename=\"" + fileName);
					blobstoreService.serve(blobstoreKey, res);
					// return "{\'downloaded\':\'true\'}";
					output.setStatusCode("200");
					output.setMessage("Success");
					output.setResult("");
				} else {
					output.setStatusCode("401");
					output.setMessage("Admin Not Authorized.");
					output.setResult("");
				}
			}
		} catch (IOException e) {
			logger.error(e.getMessage());
		} catch (CustomException e) {
			logger.error(e.getMessage());
		}
		return util.createJsonString(output);
	}

	@RequestMapping(value = "getLoggedInUserPendingFiles", method = RequestMethod.GET)
	@ResponseBody
	public String getLoggedInUserPendingFiles(HttpServletRequest req, HttpServletResponse res) {
		try {
			return util.createJsonString(studyBackEnd.getLoggedAssignedToPublishStudies());
		} catch (IOException | CustomException e) {
			logger.error("ERROR: " + e.getMessage());
			return "ERROR: " + e.getMessage();
		}

	}

	/* ______________________________________________________________________________________ */
	/*************************************** DashBoard ***************************************/
	/* ____________________________________________________________________________________ */
	@RequestMapping(value = "addAboutDashBoard", method = RequestMethod.PUT)
	@ResponseBody
	public String addAboutDashBoard(@RequestBody CmsDashBoard dashBoard, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			return util.createJsonString(adminBackEnd.addAboutDashBoard(dashBoard));
		} catch (CustomException | IOException e) {
			logger.error("ERROR: " + e.getMessage());
			return "ERROR: " + e.getMessage();
		}
	}

	@RequestMapping(value = "loadAboutDashBoard", method = RequestMethod.GET)
	@ResponseBody
	public String loadAboutDashBoard(HttpServletRequest request, HttpServletResponse response) {
		try {
			return util.createJsonString(adminBackEnd.loadAboutDashBoard());
		} catch (IOException e) {
			logger.error("ERROR: " + e.getMessage());
			return "ERROR: " + e.getMessage();
		}
	}

	@RequestMapping(value = "getOAuth2", method = RequestMethod.GET)
	public void getOAuth2(HttpServletRequest request, HttpServletResponse response) throws IOException {
		ApplicationUser loggedInUser = adminBackEnd.getLoggedInUser();

		String CLIENT_ID = OfyService.ofy().load().type(ConfigEntity.class).filter("key", "CLIENT_ID").first()
				.getValue().getValue();
		String CLIENT_SECRET = OfyService.ofy().load().type(ConfigEntity.class).filter("key", "CLIENT_SECRET").first()
				.getValue().getValue();
		String AUTH_REDIRECT_URL = OfyService.ofy().load().type(ConfigEntity.class).filter("key", "AUTH_REDIRECT_URL")
				.first().getValue().getValue();

		System.out.println("CLIENT ID: " + CLIENT_ID);
		System.out.println("AUTH_REDIRECT_URL:  " + AUTH_REDIRECT_URL);

		if (loggedInUser != null) {

			GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(new NetHttpTransport(),
					new JacksonFactory(), CLIENT_ID, CLIENT_SECRET, SCOPE).setApprovalPrompt("force")
							.setAccessType("offline").build();

			List<String> responseTypes = new ArrayList<>();
			responseTypes.add("code");
			responseTypes.add("token");

			String url = flow.newAuthorizationUrl().setResponseTypes(responseTypes).setRedirectUri(AUTH_REDIRECT_URL)
					.build();

			System.out.println("Permission URL: " + url);

			response.sendRedirect(url);
		} else {
			logger.error("Logged In User Is Null.");
			UserService userService = UserServiceFactory.getUserService();
			// response.sendRedirect(userService.createLoginURL(request.getRequestURI()));

			response.sendRedirect("/");
		}

	}

	@RequestMapping(value = "getOAuth2Code", method = RequestMethod.POST)
	public String getOAuth2Code(HttpServletRequest request, HttpServletResponse response) throws IOException {

		ServiceOutput output = new ServiceOutput();

		String CLIENT_ID = OfyService.ofy().load().type(ConfigEntity.class).filter("key", "CLIENT_ID").first()
				.getValue().getValue();
		String CLIENT_SECRET = OfyService.ofy().load().type(ConfigEntity.class).filter("key", "CLIENT_SECRET").first()
				.getValue().getValue();
		String AUTH_REDIRECT_URL = OfyService.ofy().load().type(ConfigEntity.class).filter("key", "AUTH_REDIRECT_URL")
				.first().getValue().getValue();

		String code = request.getParameter("code");

		System.out.println("CODE: " + code);
		try {
			GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(new NetHttpTransport(),
					new JacksonFactory(), CLIENT_ID, CLIENT_SECRET, SCOPE).setApprovalPrompt("force")
							.setAccessType("offline").build();

			System.out.println("Google Token Response : before execute");

			GoogleTokenResponse tokenResponse = flow.newTokenRequest(code).setRedirectUri(AUTH_REDIRECT_URL).execute()
					.setExpiresInSeconds(40L);
			System.out.println("Google Token Response : execute");

			GoogleCredential credential = new GoogleCredential.Builder().setTransport(new NetHttpTransport())
					.setJsonFactory(new JacksonFactory()).setClientSecrets(CLIENT_ID, CLIENT_SECRET)
					.addRefreshListener(new CredentialRefreshListener() {
						@Override
						public void onTokenResponse(Credential credential, TokenResponse tokenResponse) {
							// Handle success.
							System.out.println("Credential was refreshed successfully.");
						}

						@Override
						public void onTokenErrorResponse(Credential credential, TokenErrorResponse tokenErrorResponse) {
							// Handle error.
							System.err.println("Credential was not refreshed successfully. "
									+ "Redirect to error page or login screen.");
						}
					}).build();
			System.out.println("Google Credential: build");

			credential.setFromTokenResponse(tokenResponse);

			credential.refreshToken();

			Oauth2 oauth2 = new Oauth2.Builder(new NetHttpTransport(), new JacksonFactory(), credential)
					.setApplicationName("Oauth2").build();
			com.google.api.services.oauth2.model.Userinfo userinfo = oauth2.userinfo().get().execute();
			System.out.println("USER INFO: " + userinfo.toPrettyString());

			ApplicationUser loggedInUser = adminBackEnd.getLoggedInUser();

			if (loggedInUser != null) {
				String pictureUrl = userinfo.getPicture().replace("/photo.jpg", "/s96-cc/photo.jpg");
				loggedInUser.setImgURL(pictureUrl);

				Key<ApplicationUser> key = OfyService.ofy().save().entity(loggedInUser).now();
				ApplicationUser applicationUser = OfyService.ofy().load().type(ApplicationUser.class)
						.filter("id", key.getId()).first().getValue();

				output.setStatusCode("200");
				output.setMessage("Success");
				output.setResult(applicationUser);
				return util.createJsonString(output);

			} else {
				output.setStatusCode("404");
				output.setMessage("Logged In User Null");
				output.setResult("");
				return util.createJsonString(output);

			}

		} catch (IOException e) {
			logger.error(e.getMessage());
			return "Error: " + e.getMessage();
		} catch (Exception e) {
			logger.error(e.getMessage());
			return "Error: " + e.getMessage();
		}
	}

	/* ______________________________________________________________________________________ */
	/*************************************** Config ******************************************/
	/* ____________________________________________________________________________________ */

	@RequestMapping(value = "addConfig", method = RequestMethod.PUT)
	@ResponseBody
	public String addConfig(@RequestBody ConfigEntity configEntity, HttpServletRequest request,
			HttpServletResponse response) {

		OfyService.ofy().save().entity(configEntity).now();

		return null;
	}

	@RequestMapping("addToDataStore")
	public void addToDataStore() {
		DatastoreService datastoreService = DatastoreServiceFactory.getDatastoreService();

		Entity emp = new Entity("Employee");
		emp.setProperty("firstName", "Antonio");
		emp.setProperty("lastName", "Salieri");
		emp.setProperty("attendedHrTraining", true);

		datastoreService.put(emp);

	}

	/* ______________________________________________________________________________________ */
	/*************************************** RollBack *******************************************/
	/* ____________________________________________________________________________________ */

	@RequestMapping(value = "rollBackData", method = RequestMethod.GET)
	@ResponseBody
	public String rollBackData(HttpServletRequest req, HttpServletResponse res) {

		long sheetId = Long.parseLong(req.getParameter("sheetId"));

		String currentAdmin = UserServiceFactory.getUserService().getCurrentUser().getEmail();
		String appId = SystemProperty.applicationId.get();
		String moduleName = "delete-data";

		AdminTokenGenerator adminTokenGenerator = new AdminTokenGenerator();
		String tokenUrl = util.createUrl("/adminTokenGenerator/generateAdminToken", moduleName) + "?email="
				+ currentAdmin + "&_appId=" + appId;
		String adminToken = adminTokenGenerator.callURL(tokenUrl, req, res);

		if (adminToken != null) {

			String url = util.createUrl("/dataUploadController/rollBackDataSheet", moduleName) + "?sheetId=" + sheetId
					+ "&_admin_token=" + adminToken + "&_appId=" + appId + "&_user=" + currentAdmin;

			logger.info("URL : " + url);

			util.fetchAsyncURL(url, req, res);
		}

		return null;
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public ModelAndView getLogout() {
		StringBuffer logoutLink = new StringBuffer(UserServiceFactory.getUserService().createLogoutURL("/"));
		System.err.println(logoutLink.toString());
		return new ModelAndView("redirect:" + logoutLink.toString());
	}
}

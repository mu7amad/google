/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.controllers.MIDataExplorer;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.brightcreations.gplus.module.controllers.OfyService;
import com.brightcreations.gplus.module.controllers.MIDataExplorer.customClasses.ServiceOutput;
import com.brightcreations.gplus.module.model.ApplicationUser;
import com.brightcreations.gplus.module.model.FilteredAnswerValue;
import com.brightcreations.gplus.module.model.PointNotes;
import com.brightcreations.gplus.module.model.Widget;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;

/**
 * @author Mohammed.Eladly
 */

public class PointNotesBackEnd {
	private Logger logger = LoggerFactory.getLogger(getClass());
	private AdminBackEnd adminBackEnd;

	public PointNotesBackEnd() {
		adminBackEnd = new AdminBackEnd();
	}

	public ServiceOutput createNewPoint(Widget widget, PointNotes pointNotes) {

		ServiceOutput output = new ServiceOutput();
		logger.info("create new point comment");

		ApplicationUser loggedInUser = adminBackEnd.getLoggedInUser();

		Key<FilteredAnswerValue> key = OfyService.ofy().load().type(FilteredAnswerValue.class)
				.filter("id", pointNotes.getFilterAnswerID()).first().getKey();

		if (pointNotes != null && loggedInUser != null && key != null && widget != null) {
			PointNotes point = new PointNotes();
			point.setComment(pointNotes.getComment());
			point.setCreationDate(new Date());
			point.setFilterAnswerID(pointNotes.getFilterAnswerID());
			point.setApplicationUser(Ref.create(loggedInUser));
			point.setFilterdAnswerValue(Ref.create(key));

			Key<PointNotes> now = OfyService.ofy().save().entity(point).now();

			System.out.println("widget id--> " + now.getId());
			System.out.println("new point id --> " + now.getId());
			widget.getPointNotes().add(now);
			OfyService.ofy().save().entity(widget).now();

			PointNotes value = OfyService.ofy().load().type(PointNotes.class).filter("id", now.getId()).first()
					.getValue();

			output.setStatusCode("200");
			output.setMessage("Success");
			output.setResult(value);
			return output;
		} else {
			output.setStatusCode("404");
			output.setMessage("Null Pointer Exception.");
			output.setResult("");
			return output;
		}

	}

	public ServiceOutput updatePoint(String newTitle, String newNote, Long pointNoteID) {
		ServiceOutput output = new ServiceOutput();
		logger.info("update point");

		PointNotes oldPointNotes = OfyService.ofy().load().type(PointNotes.class).filter("id", pointNoteID).first()
				.getValue();

		if (oldPointNotes != null) {
			if (!newTitle.isEmpty() && newTitle != null) {
				oldPointNotes.setTitle(newTitle);
			}
			if (!newNote.isEmpty() && newNote != null) {
				oldPointNotes.setComment(newNote);
			}
			OfyService.ofy().save().entity(oldPointNotes).now();
			output.setStatusCode("200");
			output.setMessage("Success");
			output.setResult("");
			return output;
		} else {
			output.setStatusCode("404");
			output.setMessage("Null Pointer Exception.");
			output.setResult("");
			return output;
		}

	}

	public ServiceOutput deletePoint(Long widgetId, Long pointId) {
		ServiceOutput output = new ServiceOutput();
		logger.info("delete point");

		PointNotes pointValue = OfyService.ofy().load().type(PointNotes.class).filter("id", pointId)
				.filter("deleted", false).first().getValue();

		Widget widgetValue = OfyService.ofy().load().type(Widget.class).filter("id", widgetId).filter("deleted", false)
				.first().getValue();

		if (pointValue != null && widgetValue != null) {
			// delete point note from widget notes list
			Key<PointNotes> pointKey = Key.create(PointNotes.class, pointValue.getId());
			widgetValue.getPointNotes().remove(pointKey);
			OfyService.ofy().save().entity(widgetValue).now();

			// delete point note itself
			pointValue.setDeleted(true);
			OfyService.ofy().save().entity(pointValue).now();

			output.setStatusCode("200");
			output.setMessage("Success");
			output.setResult("");
			return output;
		} else {
			output.setStatusCode("404");
			output.setMessage("Null Pointer Exception");
			output.setResult("");
			return output;
		}
	}

	public ServiceOutput loadWidgetNotePoints(Long widgetId) {
		ServiceOutput output = new ServiceOutput();
		logger.info("load Widget Note Points");

		Widget widgetValue = OfyService.ofy().load().type(Widget.class).filter("id", widgetId).filter("deleted", false)
				.first().getValue();

		if (widgetValue != null) {
			output.setStatusCode("200");
			output.setMessage("Success");
			output.setResult(widgetValue.getPointNotes());
			return output;
		} else {
			output.setStatusCode("404");
			output.setMessage("Null Pointer Exception.");
			output.setResult("");
			return output;
		}
	}

	@RequestMapping(value = "addWidgetDesc", method = RequestMethod.GET)
	public ModelAndView addWidgetDesc(@RequestParam("widgetId") Long widgetId, @RequestParam("desc") String desc) {

		logger.info("add widget desc");
		Widget oldWidget = OfyService.ofy().load().key(Key.create(Widget.class, widgetId)).getValue();
		oldWidget.setDescription(desc);
		OfyService.ofy().save().entity(oldWidget).now();

		return null;
	}
}

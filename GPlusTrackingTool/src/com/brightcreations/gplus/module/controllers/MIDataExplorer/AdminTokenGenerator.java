package com.brightcreations.gplus.module.controllers.MIDataExplorer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.brightcreations.gplus.module.controllers.OfyService;
import com.brightcreations.gplus.module.intercetpors.HandlerInterceptor;
import com.brightcreations.gplus.module.model.AdminToken;
import com.brightcreations.gplus.module.model.ApplicationUser;
import com.google.appengine.api.urlfetch.FetchOptions;
import com.google.appengine.api.urlfetch.HTTPHeader;
import com.google.appengine.api.urlfetch.HTTPMethod;
import com.google.appengine.api.urlfetch.HTTPRequest;
import com.google.appengine.api.urlfetch.HTTPResponse;
import com.google.appengine.api.urlfetch.URLFetchService;
import com.google.appengine.api.urlfetch.URLFetchServiceFactory;
import com.google.appengine.api.utils.SystemProperty;
import com.googlecode.objectify.Key;

@Controller
@RequestMapping("/adminTokenGenerator")
public class AdminTokenGenerator {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	HandlerInterceptor handlerInterceptor = new HandlerInterceptor();

	@RequestMapping("generateAdminToken")
	public void generateAdminToken(@RequestParam("email") String adminEmail, HttpServletRequest req,
			HttpServletResponse res) {

		System.out.println("generateAdminToken");

		AdminToken adminToken = null;
		String instanceAppId = SystemProperty.applicationId.get();
		String requestAppId = req.getParameter("_appId");

		System.out.println("Admin mail: " + adminEmail);
		System.out.println("InstanceAppId: " + instanceAppId);
		System.out.println("requestAppId: " + requestAppId);

		if (instanceAppId.trim().equals(requestAppId.trim())) {

			if (adminEmail != null && !adminEmail.isEmpty()) {
				Key<ApplicationUser> key = OfyService.ofy().load().type(ApplicationUser.class)
						.filter("email", adminEmail).first().getKey();

				if (key != null) {
					adminToken = new AdminToken();
					String token = handlerInterceptor.generateToken();
					adminToken.setAppUserKey(key);
					adminToken.setValid(true);
					adminToken.setToken(token);
					adminToken.setCreationDate(new Date());

					OfyService.ofy().save().entity(adminToken).now();
					logger.info("Saved Admin Token");

					try {
						res.getWriter().println(token);
						res.getWriter().flush();
						res.getWriter().close();
					} catch (IOException e) {
						logger.error(e.getMessage());
					}
				} else {
					System.err.println("AppUser not Found in DB.");
				}
			} else {
				System.err.println("Admin Mail Sent is Empty.");
			}
		} else {
			System.err.println("InstanceAppId (" + instanceAppId + ") not equal requestAppId (" + requestAppId + ")");
		}
	}

	@RequestMapping("validateToken")
	public boolean validateToken(String token, Key<ApplicationUser> appUserKey) {
		AdminToken adminToken = OfyService.ofy().load().type(AdminToken.class).filter("token", token)
				.filter("appUserKey", appUserKey).first().getValue();
		if (adminToken != null) {
			if (adminToken.isValid())
				return true;
		}
		return false;
	}

	@RequestMapping("deleteToken")
	public void deleteToken(String token) {
		AdminToken adminToken = OfyService.ofy().load().type(AdminToken.class).filter("token", token).first()
				.getValue();
		OfyService.ofy().delete().entity(adminToken).now();
	}

	public void fetchURL(String url, HttpServletRequest req, HttpServletResponse res) {

		try {

			URLFetchService urlFetchService = URLFetchServiceFactory.getURLFetchService();

			URL urlObj = new URL(url);

			FetchOptions fetchOptions = FetchOptions.Builder.withDefaults();

			fetchOptions.doNotFollowRedirects();
			fetchOptions.setDeadline((double) 70);

			HTTPRequest request = new HTTPRequest(urlObj, HTTPMethod.GET, fetchOptions);

			String cookie = "";

			Cookie[] cookies = req.getCookies();

			for (int i = 0; i < cookies.length; i++) {
				cookie += cookies[i].getName() + "=" + cookies[i].getValue() + ";";
			}
			cookie = cookie.substring(0, cookie.length() - 1);
			request.setHeader(new HTTPHeader("Cookie", cookie));

			HTTPResponse fetch = urlFetchService.fetch(request);
			String content = new String(fetch.getContent());

		} catch (MalformedURLException e) {
			logger.error(e.getMessage());
		} catch (IOException e) {
			logger.error(e.getMessage());
		}

	}

	public String callURL(String url, HttpServletRequest req, HttpServletResponse res) {
		URL obj;
		try {
			String cookie = "";

			Cookie[] cookies = req.getCookies();

			for (int i = 0; i < cookies.length; i++) {
				cookie += cookies[i].getName() + "=" + cookies[i].getValue() + ";";
			}
			cookie = cookie.substring(0, cookie.length() - 1);

			obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			con.setRequestProperty("Cookie", cookie);
			con.setRequestMethod("GET");
			con.setConnectTimeout(100000);
			con.connect();
			int responseCode = con.getResponseCode();
			System.out.println("POST Response Code :: " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			return new String(response);

		} catch (MalformedURLException e) {
			logger.error(e.getMessage());
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		return null;

	}
}

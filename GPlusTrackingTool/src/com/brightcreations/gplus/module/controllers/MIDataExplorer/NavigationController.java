/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.controllers.MIDataExplorer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author Mohammed Eladly
 */
@Controller
@RequestMapping("/navigationController")
public class NavigationController {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	@RequestMapping(value = "navigateToStudiesPage", method = RequestMethod.GET)
	public ModelAndView navigateToStudiesPage(HttpServletRequest request, HttpServletResponse response) {
		return new ModelAndView("cms/get_study");
	}

	@RequestMapping(value = "navigateToGraphBuilder", method = RequestMethod.GET)
	public ModelAndView navigateToGraphBuilder(HttpServletRequest request, HttpServletResponse response) {
		return new ModelAndView("site/graph_builder");
	}

}

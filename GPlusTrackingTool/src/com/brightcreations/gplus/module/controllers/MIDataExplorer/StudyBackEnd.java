/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.controllers.MIDataExplorer;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.brightcreations.gplus.module.controllers.CMSController;
import com.brightcreations.gplus.module.controllers.EmailMessageController;
import com.brightcreations.gplus.module.controllers.OfyService;
import com.brightcreations.gplus.module.controllers.SiteController;
import com.brightcreations.gplus.module.controllers.MIDataExplorer.customClasses.ServiceOutput;
import com.brightcreations.gplus.module.controllers.MIDataExplorer.customClasses.StudyPendingFiles;
import com.brightcreations.gplus.module.model.Answer;
import com.brightcreations.gplus.module.model.ApplicationUser;
import com.brightcreations.gplus.module.model.Brand;
import com.brightcreations.gplus.module.model.ConfigEntity;
import com.brightcreations.gplus.module.model.Dashboard;
import com.brightcreations.gplus.module.model.DataSet;
import com.brightcreations.gplus.module.model.DataSheet;
import com.brightcreations.gplus.module.model.DataSheetsZipEntry;
import com.brightcreations.gplus.module.model.Division;
import com.brightcreations.gplus.module.model.FavouriteStudy;
import com.brightcreations.gplus.module.model.Filter;
import com.brightcreations.gplus.module.model.PointNotes;
import com.brightcreations.gplus.module.model.Question;
import com.brightcreations.gplus.module.model.QuestionFilterAnswerRows;
import com.brightcreations.gplus.module.model.Study;
import com.brightcreations.gplus.module.model.UserStudy;
import com.brightcreations.gplus.web.exception.CustomException;
import com.google.appengine.api.blobstore.BlobInfo;
import com.google.appengine.api.blobstore.BlobInfoFactory;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.datastore.AsyncDatastoreService;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.images.ServingUrlOptions;
// 
import com.google.appengine.api.users.UserServiceFactory;
import com.google.appengine.api.utils.SystemProperty;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;

import util.AuthorizationValidatior;
//import util.PlusSample;

/**
 * @author Mohammed.Eladly
 */

public class StudyBackEnd {
	private Logger logger = LoggerFactory.getLogger(getClass());
	CMSController cmsController = new CMSController();
	SiteController siteController = new SiteController();
	private BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
	private DatastoreService datastore;
	private AdminBackEnd adminBackEnd;
	private Util util;

	public StudyBackEnd() {
		adminBackEnd = new AdminBackEnd();
		util = new Util();
	}

	public String createUploadURL(String backendUrl) {
		return blobstoreService.createUploadUrl(backendUrl);
	}

	public Study uploadImages(HttpServletRequest req, HttpServletResponse res) throws IOException, CustomException {
		Map<String, List<BlobKey>> blobs = blobstoreService.getUploads(req);

		List<BlobKey> blobKeys = blobs.get("images");

		if (blobKeys == null || blobKeys.isEmpty()) {
			res.sendRedirect("/");

			logger.error("BolbKey Null or Empty");
			throw new CustomException("BolbKey Null or Empty");
		} else {
			Study study = new Study();
			for (BlobKey key : blobKeys) {
				BlobInfoFactory blobInfoFactory = new BlobInfoFactory();
				BlobInfo blobInfo = blobInfoFactory.loadBlobInfo(key);
				String fileName = blobInfo.getFilename();
				ImagesService imagesService = ImagesServiceFactory.getImagesService();
				ServingUrlOptions servingOptions = ServingUrlOptions.Builder.withBlobKey(key);
				String servingUrl = imagesService.getServingUrl(servingOptions);
				logger.info("Image " + fileName + " Uploaded to BlobStore.");

				study.setLogoUrl(servingUrl);
				study.setLogoBlobKey(key.getKeyString());
			}
			return study;
		}
	}

	public ServiceOutput uploadZipFile(HttpServletRequest req, HttpServletResponse res)
			throws IOException, CustomException {

		ServiceOutput output = new ServiceOutput();

		Map<String, List<BlobKey>> blobs = blobstoreService.getUploads(req);

		List<BlobKey> blobKeys = blobs.get("zipFile");
		long studyId = Long.parseLong(req.getParameter("studyId"));

		if (blobKeys == null || blobKeys.isEmpty()) {
			// res.sendRedirect("/");

			logger.error("BolbKey Null or Empty");
			output.setStatusCode("404");
			output.setMessage("BlobKey is Null or Study ID Null");
			output.setResult("");
			return output;
		} else {

			BlobKey blobKey = blobKeys.get(0);

			// DataSheetsZipEntry saveZipContentsAsDataSheets =
			// saveZipContentsAsDataSheets(blobKey, studyRef, fileName);

			// callUncompressZipFileAsync(req, res, blobKey.getKeyString(),
			// studyId, fileName);
			callUncompressZipFileAsync(req, res, blobKey.getKeyString(), studyId);

			output.setStatusCode("200");
			output.setMessage("Success");
			output.setResult("");
			return output;
		}

	}

	private void callUncompressZipFileAsync(HttpServletRequest req, HttpServletResponse res, String blobKeyString,
			long studyId) {

		logger.info("\n++++ callUncompressZipFileAsync ++++\n");

		String currentAdmin = UserServiceFactory.getUserService().getCurrentUser().getEmail();
		String appId = SystemProperty.applicationId.get();

		String moduleName = "import-data";

		AdminTokenGenerator adminTokenGenerator = new AdminTokenGenerator();
		String tokenUrl = util.createUrl("/adminTokenGenerator/generateAdminToken", moduleName) + "?email="
				+ currentAdmin + "&_appId=" + appId;

		String adminToken = adminTokenGenerator.callURL(tokenUrl, req, res);
		System.out.println("Admin Token: " + adminToken);
		if (adminToken != null) {
			//
			String url = util.createUrl("/dataUploadController/uncomressZipFile", moduleName) + "?blobKeyString="
					+ blobKeyString + "&studyId=" + studyId + "&_admin_token=" + adminToken + "&_appId=" + appId
					+ "&_user=" + currentAdmin;
			// String url =
			// util.createUrl("/dataUploadController/uncomressZipFile") +
			// "?blobKeyString=" + blobKeyString
			// + "&studyId=" + studyId;
			logger.info("URL : " + url);

			util.fetchAsyncURL(url, req, res);
		}
	}

	public void rollBackSheet(long sheetId) {
		// TODO: put rollBack Logic
		System.out.println("StudyBackEnd - rollBackSheet: " + sheetId);

	}

	public DataSheetsZipEntry saveZipContentsAsDataSheets(String blobKeyString, long studyId) throws IOException {

		System.out.println("++++++++++saveZipContentsAsDataSheets+++++++++");

		BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
		Ref<Study> studyRef = OfyService.ofy().load().type(Study.class).filter("id", studyId).first();

		BlobKey blobKey = new BlobKey(blobKeyString);

		BlobInfoFactory blobInfoFactory = new BlobInfoFactory();
		BlobInfo blobInfo = blobInfoFactory.loadBlobInfo(blobKey);
		long zipFileSize = blobInfo.getSize();
		String zipFileName = blobInfo.getFilename();

		int readLimit = 5120;

		int start = 0;
		byte[] buff = null;
		buff = blobstoreService.fetchData(blobKey, start, (start + readLimit));
		start += readLimit + 1;

		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		buffer.write(buff);

		while (buff.length != 0) {

			buff = blobstoreService.fetchData(blobKey, start, (start + readLimit));
			buffer.write(buff);
			start += readLimit + 1;
		}

		ZipInputStream zipInputStream = new ZipInputStream(new ByteArrayInputStream(buffer.toByteArray()));
		ZipEntry zipEntry = zipInputStream.getNextEntry();

		List<DataSheet> dataSheets = new ArrayList<DataSheet>();

		while (zipInputStream.available() != 0) {

			DataSheet datasheet = new DataSheet();
			datasheet.setName(zipEntry.getName());
			datasheet.setBlobstoreKey(blobKey);
			datasheet.setUploadDate(new Date());
			datasheet.setStatus(DataSheet.dataSheetStatus.UPLOADED.getStatus());
			datasheet.setSize(zipEntry.getSize());
			datasheet.setUploadedBy(UserServiceFactory.getUserService().getCurrentUser().getEmail());
			datasheet.setStudy(studyRef);
			dataSheets.add(datasheet);
			zipEntry = zipInputStream.getNextEntry();

		}
		Map<Key<DataSheet>, DataSheet> datasheetsKeys = OfyService.ofy().save().entities(dataSheets).now();

		DataSheetsZipEntry dataSheetsZipEntry = new DataSheetsZipEntry();
		dataSheetsZipEntry.setStudyRef(studyRef);
		dataSheetsZipEntry.setUploadedBy(UserServiceFactory.getUserService().getCurrentUser().getEmail());
		dataSheetsZipEntry.setDatasheets(new ArrayList<Key<DataSheet>>(datasheetsKeys.keySet()));
		dataSheetsZipEntry.setName(zipFileName);
		dataSheetsZipEntry.setUploadDate(new Date());
		dataSheetsZipEntry.setBlobstoreKey(blobKey);
		dataSheetsZipEntry.setZipFileSize(zipFileSize);
		OfyService.ofy().save().entity(dataSheetsZipEntry).now();

		Study study = studyRef.getValue();
		study.setPendingFiles(dataSheets.size());
		OfyService.ofy().save().entity(study).now();

		return dataSheetsZipEntry;
	}

	public ServiceOutput loadStudies() {
		logger.info("Loading Studies.");

		ServiceOutput output = new ServiceOutput();

		List<Study> studies = OfyService.ofy().cache(false).load().type(Study.class).filter("deleted", false).list();

		// get users on this study
		for (Study itm : studies) {
			itm.setPublishers(getStudyUsers(itm, true, false));
			itm.setUploaders(getStudyUsers(itm, false, true));
			// itm.setSharedWith(getStudySharedWithUsers(itm));
		}

		Collections.sort(studies, Collections.reverseOrder());

		output.setStatusCode("200");
		output.setMessage("Success");
		output.setResult(studies);

		return output;
	}

	public Set<ApplicationUser> getStudyUsers(Study study, boolean publisher, boolean uploader) {

		Set<ApplicationUser> result = new HashSet<>();

		Ref<Study> studyRef = Ref.create(Key.create(Study.class, study.getId()));
		List<UserStudy> userStudyList = OfyService.ofy().load().type(UserStudy.class).filter("studyRef", studyRef)
				.filter("publisher", publisher).filter("uploader", uploader).list();

		for (UserStudy itm : userStudyList) {
			ApplicationUser user = OfyService.ofy().load().key(itm.getAppUserRef().getKey()).getValue();
			result.add(user);
		}

		return result;
	}

	public Set<ApplicationUser> getStudySharedWithUsers(Study study) {

		Set<ApplicationUser> result = new HashSet<>();
		Set<Key<ApplicationUser>> sharedWithApplicationUsers = study.getSharedWithApplicationUsers();

		if (sharedWithApplicationUsers != null) {
			for (Key<ApplicationUser> userKey : sharedWithApplicationUsers) {
				ApplicationUser value = OfyService.ofy().load().key(userKey).getValue();
				result.add(value);
			}
		}

		return result;
	}

	@RequestMapping(value = "viewStudyData/{studyID}", method = RequestMethod.GET)
	@ResponseBody
	public void viewStudyData(@PathVariable("studyID") String studyID, HttpServletRequest request,
			HttpServletResponse response) {
		logger.info("viewStudyData: " + studyID);

	}

	public ServiceOutput findByPrefixStudyName(String searchWord) throws CustomException {

		ServiceOutput output = new ServiceOutput();

		datastore = DatastoreServiceFactory.getDatastoreService();

		if (searchWord.length() >= 3) {

			com.google.appengine.api.datastore.Query.Filter prefixFilter = new Query.FilterPredicate("studyName",
					FilterOperator.GREATER_THAN_OR_EQUAL, searchWord);

			String endPrefix = util.getEndPrefix(searchWord);

			com.google.appengine.api.datastore.Query.Filter postfixFilter = new Query.FilterPredicate("studyName",
					FilterOperator.LESS_THAN, endPrefix);

			com.google.appengine.api.datastore.Query.CompositeFilter filters = Query.CompositeFilterOperator
					.and(prefixFilter, postfixFilter);

			Query q = new Query(Study.class.getSimpleName()).setFilter(filters);
			List<Entity> entities = datastore.prepare(q).asList(FetchOptions.Builder.withLimit(20));

			List<String> keys = new ArrayList<>();

			for (Entity en : entities) {
				Map<String, Object> properties = en.getProperties();
				keys.add((String) properties.get("studyID"));
			}
			if (!util.isNullOrEmpty(keys)) {
				List<Study> list = OfyService.ofy().load().type(Study.class).filter("studyID IN", keys).list();
				output.setStatusCode("200");
				output.setMessage("Success");
				output.setResult(list);
			}
		} else {
			output.setStatusCode("606");
			output.setMessage("Search Word Lenght is < 3 Characters");
			output.setResult("");
		}
		return output;
	}

	public ServiceOutput createNewStudy(Study study) throws IOException, CustomException {

		ServiceOutput output = new ServiceOutput();

		String studyName = study.getStudyName();
		Set<ApplicationUser> publishersList = study.getPublishers();
		Set<ApplicationUser> uploadersList = study.getUploaders();
		Set<String> sharedWithExternalUsers = study.getSharedWithExternalUsers();
		List<ApplicationUser> superAdmins = adminBackEnd.getSuperAdmins();

		if (util.isNullOrEmpty(publishersList)) {
			publishersList = new HashSet<>();
		}
		if (util.isNullOrEmpty(uploadersList)) {
			uploadersList = new HashSet<>();
		}
		Set<ApplicationUser> intersection = checkIntersection(publishersList, uploadersList);
		ApplicationUser currentUser = adminBackEnd.getLoggedInUser();

		if (util.isNullOrEmpty(intersection)) {

			logger.info("Create New Study");
			studyName = studyName.toUpperCase();
			String studyID = generateStudyID(studyName);
			Study oldStudy = OfyService.ofy().load().type(Study.class).filter("studyID", studyID)
					.filter("deleted", false).first().getValue();

			if (oldStudy == null) {
				Study newStudy = new Study();

				newStudy.setStudyID(generateStudyID(studyName));

				SimpleDateFormat dt = new SimpleDateFormat("MMMM dd, yyyy", Locale.US);
				String date = dt.format(new Date());
				newStudy.setCreationDate(date);

				if (!util.isNullOrEmpty(sharedWithExternalUsers)) {

					newStudy.setPublicStudy(false);
					// newStudy.setSharedWithApplicationUsers(share_with(sharedWithExternalUsers));
					newStudy.setSharedWithExternalUsers(sharedWithExternalUsers);

				} else {
					newStudy.setPublicStudy(true);
				}
				newStudy.setStudyName(studyName);
				newStudy.setLogoUrl(study.getLogoUrl());
				newStudy.setLogoBlobKey(study.getLogoBlobKey());
				newStudy.setOwnerEmail(currentUser.getEmail());
				newStudy.setDescription(study.getDescription());
				newStudy.setTotalInterviewed(study.getTotalInterviewed());

				Key<Study> studyKey = OfyService.ofy().save().entity(newStudy).now();
				Ref<Study> studyRef = Ref.create(studyKey);

				uploadersList.add(currentUser);
				uploadersList.addAll(superAdmins);
				publishersList.addAll(superAdmins);

				boolean flag = saveUserStudy(studyRef, publishersList, uploadersList);

				if (flag) {
					Study resultStudy = OfyService.ofy().load().key(Key.create(Study.class, studyKey.getId())).get();

					resultStudy.setPublishers(getStudyUsers(resultStudy, true, false));
					resultStudy.setUploaders(getStudyUsers(resultStudy, false, true));
					// resultStudy.setSharedWith(getStudySharedWithUsers(resultStudy));
					sendNotificationUserMail(sharedWithExternalUsers, resultStudy.getStudyName());

					output.setStatusCode("200");
					output.setMessage("Success");
					output.setResult(resultStudy);
				} else {

					output.setStatusCode("606");
					output.setMessage("Study Is Already Exists.");
					output.setResult("");
				}

			} else {
				output.setStatusCode("606");
				output.setMessage("Study Is Already Exists.");
				output.setResult("");
			}
		} else {
			output.setStatusCode("606");
			output.setMessage("Publishers and Uploaders Common.");
			output.setResult("");
		}
		return output;
	}

	private void sendNotificationUserMail(Set<String> sharedWithExternalUsers, String studyName) {
		if (util.checkProductionEnv() && !util.isNullOrEmpty(sharedWithExternalUsers)) {
			String mailSubject = OfyService.ofy().load().type(ConfigEntity.class).filter("key", "MAIL_SUBJECT").first()
					.getValue().getValue();
			String sharedStudy = OfyService.ofy().load().type(ConfigEntity.class).filter("key", "SHARED_STUDY").first()
					.getValue().getValue();
			String fromMail = OfyService.ofy().load().type(ConfigEntity.class).filter("key", "FROM_MAIL").first()
					.getValue().getValue();

			EmailMessageController emailMessageController = new EmailMessageController();

			for (String itm : sharedWithExternalUsers) {
				String tmpName = itm.substring(0, itm.indexOf("@"));
				try {
					emailMessageController.sendGoogleMail(fromMail, itm.trim(),
							String.format(sharedStudy, util.capitalize(tmpName), studyName), mailSubject);
				} catch (MessagingException e) {
					e.printStackTrace();
				}
			}

		}
	}

	public ServiceOutput updateStudy(Study study) throws CustomException {
		ServiceOutput output = new ServiceOutput();

		Set<ApplicationUser> publishersList = study.getPublishers();
		Set<ApplicationUser> uploadersList = study.getUploaders();
		Set<String> sharedWithExternalUsers = study.getSharedWithExternalUsers();
		List<ApplicationUser> superAdmins = adminBackEnd.getSuperAdmins();
		Set<String> adminsEmails = new HashSet<>(adminBackEnd.getAdminsEmails(superAdmins));

		if (util.isNullOrEmpty(publishersList)) {
			publishersList = new HashSet<>();
		}
		if (util.isNullOrEmpty(uploadersList)) {
			uploadersList = new HashSet<>();
		}

		Set<ApplicationUser> intersection = checkIntersection(publishersList, uploadersList);

		if (getLoggedInUserStudyUploader(study)) {

			if (util.isNullOrEmpty(intersection)) {
				Ref<Study> studyRef = OfyService.ofy().load().type(Study.class).filter("id", study.getId())
						.filter("deleted", false).first();
				if (studyRef != null) {
					Study oldStudy = studyRef.getValue();

					oldStudy.setStudyName(study.getStudyName());
					if (!util.isNullOrEmpty(sharedWithExternalUsers)) {
						oldStudy.setPublicStudy(false);
						// oldStudy.setSharedWithApplicationUsers(share_with(sharedWith));
						oldStudy.setSharedWithExternalUsers(sharedWithExternalUsers);
						sendNotificationUserMail(sharedWithExternalUsers, oldStudy.getStudyName());
					} else {
						oldStudy.setPublicStudy(true);
					}
					oldStudy.setDescription(study.getDescription());
					oldStudy.setLogoBlobKey(study.getLogoBlobKey());
					oldStudy.setLogoUrl(study.getLogoUrl());
					oldStudy.setTotalInterviewed(study.getTotalInterviewed());
					deleteUserStudy(oldStudy);

					ApplicationUser owner = OfyService.ofy().load().type(ApplicationUser.class)
							.filter("email", oldStudy.getOwnerEmail()).first().getValue();
					uploadersList.add(owner);
					uploadersList.addAll(uploadersList);
					publishersList.addAll(publishersList);
					boolean flag = saveUserStudy(studyRef, publishersList, uploadersList);

					if (flag) {
						Key<Study> newKey = OfyService.ofy().save().entity(oldStudy).now();
						Study resultStudy = OfyService.ofy().load().key(Key.create(Study.class, newKey.getId())).get();

						resultStudy.setPublishers(getStudyUsers(resultStudy, true, false));
						resultStudy.setUploaders(getStudyUsers(resultStudy, false, true));
						// resultStudy.setSharedWith(getStudySharedWithUsers(resultStudy));
						sendNotificationUserMail(sharedWithExternalUsers, resultStudy.getStudyName());

						output.setStatusCode("200");
						output.setMessage("Success");
						output.setResult(resultStudy);
					} else {
						output.setStatusCode("500");
						output.setMessage("Study Not Updated.");
						output.setResult("");
					}
				} else {
					output.setStatusCode("401");
					output.setMessage("User Not Authorized for This Operation.");
					output.setResult("");
				}
			} else {
				output.setStatusCode("606");
				output.setMessage("Publishers and Uploaders Common.");
				output.setResult("");
			}
		} else {
			output.setStatusCode("401");
			output.setMessage("User Not Authorized for This Operation.");
			output.setResult("");
		}
		return output;
	}

	private void deleteUserStudy(Study oldStudy) {
		List<UserStudy> userStudies = OfyService.ofy().load().type(UserStudy.class)
				.filter("studyRef", Ref.create(Key.create(Study.class, oldStudy.getId()))).list();

		if (userStudies != null) {
			OfyService.ofy().delete().entities(userStudies).now();
		}
	}

	private List<String> getAppUsersEmailsList(List<UserStudy> sendedList) throws CustomException {
		List<String> tmp;
		if (sendedList != null) {
			tmp = new ArrayList<>();
			for (UserStudy itm : sendedList) {
				long appUserId = itm.getAppUserRef().getKey().getId();
				ApplicationUser applicationUser = OfyService.ofy().load().type(ApplicationUser.class)
						.filter("id", appUserId).filter("deleted", false).first().getValue();
				if (applicationUser != null) {
					tmp.add(applicationUser.getEmail().toLowerCase());
				}

			}
		} else {
			throw new CustomException("Null Pointer Exception in Update UserStudy Entity");
		}
		return tmp;
	}

	private Set<Key<ApplicationUser>> share_with(Set<ApplicationUser> prepareListOfString) {
		Set<Key<ApplicationUser>> result = new HashSet<>();

		if (prepareListOfString != null) {
			for (ApplicationUser itm : prepareListOfString) {
				Key<ApplicationUser> key = Key.create(ApplicationUser.class, itm.getId());
				if (key != null) {
					result.add(key);
				}
			}
		}

		return result;
	}

	private boolean saveUserStudy(Ref<Study> studyRef, Set<ApplicationUser> publishersList,
			Set<ApplicationUser> uploadersList) throws CustomException {
		logger.info("saveUserStudy");

		List<UserStudy> userStudiesPublisher = setUserStudyFields(studyRef, publishersList, true);
		if (userStudiesPublisher == null) {
			// OfyService.ofy().delete().entity(studyRef).now();
			// throw new CustomException("Required At Least One Publisher.");
			userStudiesPublisher = new ArrayList<>();
		}
		Map<Key<UserStudy>, UserStudy> publishers = OfyService.ofy().save().entities(userStudiesPublisher).now();
		List<UserStudy> userStudiesUploaders = setUserStudyFields(studyRef, uploadersList, false);
		Map<Key<UserStudy>, UserStudy> uploaderes = OfyService.ofy().save().entities(userStudiesUploaders).now();

		// if (userStudiesUploaders != null) {
		// userStudiesPublisher.addAll(userStudiesUploaders);
		// }
		if (uploaderes != null)
			return true;
		return false;

	}

	private List<UserStudy> setUserStudyFields(Ref<Study> studyRef, Set<ApplicationUser> publishersList,
			boolean isPublisher) throws CustomException {
		List<UserStudy> userStudies = null;

		if (!util.isNullOrEmpty(publishersList)) {
			userStudies = new ArrayList<>();

			for (ApplicationUser value : publishersList) {

				if (value != null && value.isAdmin()) {
					Key<ApplicationUser> key = Key.create(ApplicationUser.class, value.getId());
					Ref<ApplicationUser> appUserRef = Ref.create(key);

					UserStudy userStudy = OfyService.ofy().load().type(UserStudy.class).filter("appUserRef", appUserRef)
							.filter("studyRef", studyRef).first().getValue();

					if (userStudy == null) {
						userStudy = new UserStudy();
						userStudy.setStudyRef(studyRef);
					}

					if (isPublisher == true) {
						userStudy.setPublisher(true);
						if (userStudy.getId() == null) {
							userStudy.setUploader(false);
						}
					} else {
						if (userStudy.getId() == null) {
							userStudy.setPublisher(false);
						}
						userStudy.setUploader(true);
					}

					Study studyValue = OfyService.ofy().load().type(Study.class).filter("id", studyRef.getKey().getId())
							.first().getValue();
					ApplicationUser appUserValue = OfyService.ofy().load().type(ApplicationUser.class)
							.filter("id", key.getId()).first().getValue();

					if (appUserValue.getEmail().equals(studyValue.getOwnerEmail())) {
						userStudy.setOwner(true);
					} else {
						userStudy.setOwner(false);
					}
					userStudy.setAppUserRef(appUserRef);
					userStudies.add(userStudy);
				} else {
					OfyService.ofy().delete().entity(studyRef).now();
					throw new CustomException("One Of Publishers, or Uploaders Not An Admin.");
				}
			}
		}

		return userStudies;
	}

	public Boolean getLoggedInUserStudyUploader(Study study) throws CustomException {
		ApplicationUser loggedInUser = adminBackEnd.getLoggedInUser();

		List<UserStudy> userStudies = OfyService.ofy().load().type(UserStudy.class)
				.filter("appUserRef", Ref.create(Key.create(ApplicationUser.class, loggedInUser.getId())))
				.filter("studyRef", Ref.create(Key.create(Study.class, study.getId()))).list();

		if (userStudies != null) {
			for (UserStudy itm : userStudies) {
				if (itm.isUploader() == true)
					return true;
			}
		}

		return false;
	}

	public ServiceOutput deleteStudy(Study study) throws CustomException {
		ServiceOutput output = new ServiceOutput();
		boolean loggedInUserIsSuperUser = adminBackEnd.isLoggedInUserSuperUser();

		if (getLoggedInUserStudyUploader(study) || loggedInUserIsSuperUser) {

			Key<Study> key = Key.create(Study.class, study.getId());
			Study oldStudy = OfyService.ofy().load().key(Key.create(Study.class, study.getId())).getValue();

			deleteDivisionByStudyID(key);
			deleteBrandsByStudyID(key);
			deleteQuestionsByStudyID(key);
			deleteAnswersByStudyID(key);
			deleteFilterByStudyID(key);
			deleteDataSetByStudyID(key);
			deleteDataSheetByStudyID(key);
			deleteSharedStudyWithApplicationUser(key);
			deleteDashBoardByStudyID(key);

			oldStudy.setDeleted(true);
			OfyService.ofy().save().entity(oldStudy).now();
			// OfyService.ofy().delete().entity(oldStudy).now();
			// return "{\'studyDeleted\':\'true\'}";
			output.setStatusCode("200");
			output.setMessage("Success");
			output.setResult("");

		} else {
			output.setStatusCode("401");
			output.setMessage("User Not Authorized for This Operation.");
			output.setResult("");
		}
		return output;
	}

	public ServiceOutput openStudy(Study study) throws CustomException {

		ServiceOutput output = new ServiceOutput();

		if (study != null) {
			Ref<Study> oldStudyRef = OfyService.ofy().load().type(Study.class).filter("studyID", study.getStudyID())
					.filter("deleted", false).first();
			if (oldStudyRef != null) {
				Key<Study> key = oldStudyRef.getKey();

				if (key != null) {
					List<DataSheetsZipEntry> zipFiles = OfyService.ofy().load().type(DataSheetsZipEntry.class)
							.filter("studyRef", oldStudyRef).list();
					Study study2 = oldStudyRef.getValue();
					Collections.sort(zipFiles);
					study2.setZipFiles(zipFiles);

					output.setStatusCode("200");
					output.setMessage("Success");
					output.setResult(study2);
				}
			}

		} else {
			output.setStatusCode("606");
			output.setMessage("Null Pointer Exception in Passed Study Object.");
			output.setResult("");
		}
		return output;
	}

	public ServiceOutput openStudyDataSheets(Study study) throws CustomException {
		ServiceOutput output = new ServiceOutput();
		Ref<Study> studyRef = OfyService.ofy().load().type(Study.class).filter("studyID", study.getStudyID())
				.filter("deleted", false).first();

		if (studyRef.getKey() != null) {
			List<DataSheet> dataSheetFiles = OfyService.ofy().load().type(DataSheet.class).filter("deleted", false)
					.filter("study", studyRef).list();

			output.setStatusCode("200");
			output.setMessage("Success");
			output.setResult(dataSheetFiles);
		} else {
			output.setStatusCode("606");
			output.setMessage("Null Pointer Exception in Passed Study Object.");
			output.setResult("");
		}
		return output;
	}

	public boolean isAuthorized(boolean flag) throws CustomException {
		if (flag) {
			return true;
		} else {
			throw new CustomException("Admin Not Authorized.");
		}
	}

	public ServiceOutput getCsvFilesInZipFile(DataSheetsZipEntry zipEntry) throws CustomException {

		ServiceOutput output = new ServiceOutput();
		if (zipEntry != null) {
			DataSheetsZipEntry oldZipEntry = OfyService.ofy().load().type(DataSheetsZipEntry.class)
					.filter("id", zipEntry.getId()).first().getValue();

			if (oldZipEntry != null) {
				List<Key<DataSheet>> datasheetsKeys = oldZipEntry.getDatasheets();
				ArrayList<DataSheet> dataSheets = new ArrayList<DataSheet>(
						OfyService.ofy().load().keys(datasheetsKeys).values());

				output.setStatusCode("200");
				output.setMessage("Success");
				output.setResult(dataSheets);
				return output;
			}
		} else {
			output.setStatusCode("606");
			output.setMessage("Null Pointer Exception in Passed ZipFile Object.");
			output.setResult("");
		}
		return output;
	}

	public ServiceOutput deleteCsvFile(DataSheet sheet) throws CustomException {
		ServiceOutput output = new ServiceOutput();

		if (sheet != null) {
			DataSheet dataSheet = OfyService.ofy().load().type(DataSheet.class).filter("id", sheet.getId()).first()
					.getValue();
			if (dataSheet != null) {

				Ref<DataSet> datasetRef = dataSheet.getDatasetRef();
				Ref<Study> studyRef = dataSheet.getStudy();

				Study study = OfyService.ofy().load().type(Study.class).filter("id", studyRef.getKey().getId()).first()
						.getValue();

				Boolean loggedInUserStudyUploader = getLoggedInUserStudyUploader(study);

				if (loggedInUserStudyUploader) {
					if (datasetRef == null) {
						dataSheet.setDeleted(true);
						// OfyService.ofy().delete().entity(dataSheet).now();
						// return "{\'csvFileDeleted\':\'true\'}";
						OfyService.ofy().save().entity(dataSheet).now();

						output.setStatusCode("200");
						output.setMessage("Success");
						output.setResult("");

					} else {
						output.setStatusCode("401");
						output.setMessage("Processed CSV Files, Can't Delete.");
						output.setResult("");
					}
				} else {
					output.setStatusCode("401");
					output.setMessage("Admin Not Authorized.");
					output.setResult("");
				}

			} else {
				output.setStatusCode("606");
				output.setMessage("Null Pointer Exception in Passed CsvFile Object.");
				output.setResult("");
			}
		} else {
			output.setStatusCode("606");
			output.setMessage("Null Pointer Exception in Passed CsvFile Object.");
			output.setResult("");
		}
		return output;
	}

	public void calculateStudyPendingFiles(Study study) throws CustomException {
		Ref<Study> oldStudyRef = OfyService.ofy().load().type(Study.class).filter("id", study.getId()).first();

		if (oldStudyRef != null) {

			Study oldStudy = oldStudyRef.getValue();
			List<DataSheet> dataSheetList = OfyService.ofy().load().type(DataSheet.class).filter("study", oldStudyRef)
					.filter("status", DataSheet.dataSheetStatus.UPLOADED.getStatus()).filter("deleted", false).list();

			oldStudy.setPendingFiles(dataSheetList.size());
			OfyService.ofy().save().entity(oldStudy).now();

		} else {
			throw new CustomException("calculateStudyPendingFiles Null Pointer Exception");
		}

	}

	public ServiceOutput deleteZipFile(DataSheetsZipEntry entry) throws CustomException {

		ServiceOutput output = new ServiceOutput();

		if (entry != null) {

			Ref<DataSheetsZipEntry> oldEntryRef = OfyService.ofy().load().type(DataSheetsZipEntry.class)
					.filter("id", entry.getId()).first();

			if (oldEntryRef != null) {

				DataSheetsZipEntry zipEntry = oldEntryRef.getValue();
				if (zipEntry != null) {

					Ref<Study> studyRef = zipEntry.getStudyRef();
					if (studyRef != null) {
						Study loadedStudy = OfyService.ofy().load().type(Study.class)
								.filter("id", studyRef.getKey().getId()).first().getValue();
						Boolean loggedInUserStudyUploader = getLoggedInUserStudyUploader(loadedStudy);

						if (loggedInUserStudyUploader) {
							List<Key<DataSheet>> dataSheetsKeys = zipEntry.getDatasheets();

							ArrayList<DataSheet> dataSheetList = new ArrayList<DataSheet>(
									OfyService.ofy().load().keys(dataSheetsKeys).values());

							List<Key<DataSet>> dataSetKeys = new ArrayList<>();

							for (DataSheet sheet : dataSheetList) {
								if (sheet.getDatasetRef() != null)
									sheet.setDeleted(true);
								dataSetKeys.add(sheet.getDatasetRef().getKey());
							}

							if (util.isNullOrEmpty(dataSetKeys)) {
								zipEntry.setDeleted(true);
								// OfyService.ofy().delete().entities(dataSheetList).now();
								// OfyService.ofy().delete().entity(zipEntry).now();
								// return "{\'ZipFileDeleted\':\'true\'}";

								OfyService.ofy().save().entities(dataSheetList).now();
								OfyService.ofy().save().entities(zipEntry).now();

								output.setStatusCode("200");
								output.setMessage("Success");
								output.setResult("");

							} else {
								output.setStatusCode("403");
								output.setMessage("There Is Processed CSV Files.");
								output.setResult("");
							}
						} else {
							output.setStatusCode("401");
							output.setMessage("Admin Not Authorized.");
							output.setResult("");
						}
					} else {
						output.setStatusCode("404");
						output.setMessage("Null Pointer Exception in ZipFile Object.");
						output.setResult("");

					}
				} else {
					output.setStatusCode("404");
					output.setMessage("Null Pointer Exception in ZipFile Object.");
					output.setResult("");
				}

			} else {
				output.setStatusCode("404");
				output.setMessage("Null Pointer Exception in ZipFile Object.");
				output.setResult("");
			}
		} else {
			output.setStatusCode("404");
			output.setMessage("Null Pointer Exception in ZipFile Object.");
			output.setResult("");
		}
		return output;
	}

	/* ______________________________________________________________________________________ */
	/*************************************** SITE ********************************************/
	/* ____________________________________________________________________________________ */

	public List<Study> loadPublicStudies() {
		ApplicationUser loggedInUser = adminBackEnd.getLoggedInUser();
		Set<Key<ApplicationUser>> sharedWithApplicationUsers = new HashSet<>();
		sharedWithApplicationUsers.add(Key.create(ApplicationUser.class, loggedInUser.getId()));

		Set<Study> result = new HashSet<>();
		Set<Study> tmp = null;

		// load studies shared with loggedin User
		List<Study> studeisSharedWith = OfyService.ofy().cache(false).load().type(Study.class)
				.filter("sharedWithApplicationUsers IN", sharedWithApplicationUsers).list();

		// check sharedStudies is not deleted
		for (Study itm : studeisSharedWith) {
			if (!itm.isDeleted()) {
				result.add(itm);
			}
		}

		// load all public studies and not deleted
		List<Study> publicStudies = OfyService.ofy().cache(false).load().type(Study.class).filter("deleted", false)
				.filter("publicStudy", true).list();

		if (!util.isNullOrEmpty(publicStudies)) {
			tmp = new HashSet<>(publicStudies);
		}
		if (!util.isNullOrEmpty(tmp)) {
			result.addAll(tmp);
		}
		List<Study> sortedList = new ArrayList<>(result);

		Collections.sort(sortedList, Collections.reverseOrder());
		return sortedList;
	}

	public List<Study> searchInPublicStudies(String searchWord) throws CustomException {

		if (searchWord.length() >= 3) {
			List<Study> result = new ArrayList<>();

			List<Study> publicStudies = loadPublicStudies();

			for (Study itm : publicStudies) {
				String studyName = itm.getStudyName().toLowerCase();

				if (studyName.contains(searchWord.toLowerCase())) {
					result.add(itm);
				}
			}
			return result;
		} else {
			throw new CustomException("Search Word Lenght is < 3 Characters");
		}
	}

	public Study addFavouriteStudy(Study study) throws CustomException {

		if (study != null) {

			ApplicationUser loggedInUser = adminBackEnd.getLoggedInUser();

			Ref<Study> oldStudyRef = OfyService.ofy().load().type(Study.class).filter("id", study.getId())
					.filter("deleted", false).first();

			if (oldStudyRef != null && loggedInUser != null) {
				Ref<ApplicationUser> appUserKey = Ref.create(Key.create(ApplicationUser.class, loggedInUser.getId()));

				FavouriteStudy favouriteStudy = new FavouriteStudy();
				favouriteStudy.setStudyRef(oldStudyRef);
				favouriteStudy.setAppUserRef(appUserKey);

				OfyService.ofy().save().entity(favouriteStudy).now();
				return oldStudyRef.getValue();

			} else {
				throw new CustomException("Passed Null Pointer Exception.");
			}
		} else {
			throw new CustomException("Passed Null Pointer Exception.");
		}
	}

	public List<Study> loadFavoStudiesForLoggedInUser() throws CustomException {

		ApplicationUser loggedInUser = adminBackEnd.getLoggedInUser();

		if (loggedInUser != null) {
			Key<ApplicationUser> appUserKey = Key.create(ApplicationUser.class, loggedInUser.getId());
			List<FavouriteStudy> favouriteStudies = OfyService.ofy().load().type(FavouriteStudy.class)
					.filter("appUserRef", Ref.create(appUserKey)).filter("deleted", false).list();
			List<Long> ids = new ArrayList<>();

			for (FavouriteStudy favo : favouriteStudies) {

				Ref<Study> studyRef = favo.getStudyRef();
				if (studyRef != null) {
					ids.add(studyRef.getKey().getId());
				}
			}
			List<Study> studies = new ArrayList<>(OfyService.ofy().load().type(Study.class).ids(ids).values());
			return studies;

		} else {
			throw new CustomException("Passed Null Pointer Exception.");
		}

	}

	public String deleteFavoStudy(Study study) throws CustomException {
		if (study != null) {

			Key<Study> studyKey = OfyService.ofy().load().type(Study.class).filter("id", study.getId()).first()
					.getKey();
			if (studyKey != null) {
				Ref<Study> studyRef = Ref.create(studyKey);

				List<FavouriteStudy> favouriteStudies = OfyService.ofy().load().type(FavouriteStudy.class)
						.filter("studyRef", studyRef).list();

				for (FavouriteStudy favo : favouriteStudies) {
					favo.setDeleted(true);
				}
				OfyService.ofy().save().entities(favouriteStudies).now();
				return "{\'favouriteStudyDeleted\':\'true\'}";
			} else {
				throw new CustomException("Passed Null Pointer Exception.");
			}
		} else {
			throw new CustomException("Passed Null Pointer Exception.");
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////

	@RequestMapping("getUserMailByPrefix")
	@ResponseBody
	public List<String> getUserMailByPrefix(@RequestParam("emailCharacters") String emailCharacters) {
		List<String> resultList = new ArrayList<>();
		datastore = DatastoreServiceFactory.getDatastoreService();

		if (emailCharacters.length() >= 3) {
			com.google.appengine.api.datastore.Query.Filter prefixFilter = new Query.FilterPredicate("email",
					FilterOperator.GREATER_THAN_OR_EQUAL, emailCharacters /* + "%" */);
			Query q = new Query(ApplicationUser.class.getSimpleName()).setFilter(prefixFilter);
			List<Entity> entities = datastore.prepare(q).asList(FetchOptions.Builder.withLimit(5));

			for (Entity en : entities) {
				Map<String, Object> properties = en.getProperties();
				resultList.add((String) properties.get("email"));
			}
			System.out.println();
		}
		return resultList;
	}

	private Set<ApplicationUser> checkIntersection(Set<ApplicationUser> publishersList,
			Set<ApplicationUser> uploadersList) {
		if (publishersList != null && uploadersList != null) {
			// remove superadmins from 2 sets
			int superAdminSize = 0;
			Iterator uploadersListIterator = uploadersList.iterator();
			while (uploadersListIterator.hasNext()) {
				ApplicationUser appUser = (ApplicationUser) uploadersListIterator.next();
				if (appUser.isSuperUser())
					superAdminSize++;
			}
			Set<ApplicationUser> intersection = new HashSet<ApplicationUser>(publishersList);
			intersection.retainAll(uploadersList);
			if (intersection.size() > superAdminSize)
				return intersection;
		}
		return null;
	}

	private List<String> prepareListOfString(String textCommaSeperated) {
		if (!textCommaSeperated.isEmpty())
			return Arrays.asList(textCommaSeperated.split(","));
		return null;
	}

	public ServiceOutput getUserStudies(ApplicationUser applicationUser) throws CustomException {
		ServiceOutput output = new ServiceOutput();
		if (applicationUser != null) {
			List<Study> list = OfyService.ofy().load().type(Study.class)
					.filter("ownerEmail", applicationUser.getEmail()).filter("deleted", false).list();

			// get users on this study
			for (Study itm : list) {
				itm.setPublishers(getStudyUsers(itm, true, false));
				itm.setUploaders(getStudyUsers(itm, false, true));
				// itm.setSharedWith(getStudySharedWithUsers(itm));
			}
			Collections.sort(list, Collections.reverseOrder());

			output.setStatusCode("200");
			output.setMessage("Success");
			output.setResult(list);
		} else {
			output.setStatusCode("404");
			output.setMessage("Applications User Null.");
			output.setResult("");
		}
		return output;
	}

	public ServiceOutput getLoggedInSharedWithStudies(ApplicationUser applicationUser) throws CustomException {
		ServiceOutput output = new ServiceOutput();
		if (applicationUser != null) {
			List<Study> list = OfyService.ofy().load().type(Study.class)
					.filter("sharedWithApplicationUsers", Key.create(ApplicationUser.class, applicationUser.getId()))
					.filter("deleted", false).list();

			output.setStatusCode("200");
			output.setMessage("Success");
			output.setResult(list);

		} else {
			output.setStatusCode("404");
			output.setMessage("Applications User Null.");
			output.setResult("");
		}
		return output;
	}

	public ServiceOutput getLoggedAssignedStudies(ApplicationUser applicationUser) throws CustomException {

		ServiceOutput output = new ServiceOutput();

		if (applicationUser != null) {
			List<UserStudy> userStudies = OfyService.ofy().load().type(UserStudy.class).filter("isOwner", false)
					.filter("appUserRef", Ref.create(Key.create(ApplicationUser.class, applicationUser.getId())))
					.list();
			List<Study> studies = null;

			if (!util.isNullOrEmpty(userStudies)) {
				studies = new ArrayList<>();

				for (UserStudy itm : userStudies) {
					Study value = OfyService.ofy().load().key(itm.getStudyRef().getKey()).getValue();
					if (value != null) {
						if (!value.isDeleted()) {
							value.setPublishers(getStudyUsers(value, true, false));
							value.setUploaders(getStudyUsers(value, false, true));
							studies.add(value);
						}
					}
				}
			}

			if (studies != null)
				Collections.sort(studies, Collections.reverseOrder());

			output.setStatusCode("200");
			output.setMessage("Success");
			output.setResult(studies);

		} else {
			output.setStatusCode("404");
			output.setMessage("Applications User Null.");
			output.setResult("");
		}
		return output;
	}

	public ServiceOutput getLoggedAssignedToPublishStudies() throws CustomException {

		ApplicationUser applicationUser = adminBackEnd.getLoggedInUser();
		ServiceOutput output = new ServiceOutput();

		if (applicationUser != null) {
			List<UserStudy> userStudies = OfyService.ofy().load().type(UserStudy.class)
					.filter("appUserRef", Ref.create(Key.create(ApplicationUser.class, applicationUser.getId())))
					.list();
			List<StudyPendingFiles> result = null;

			if (!util.isNullOrEmpty(userStudies)) {
				result = new ArrayList<>();

				for (UserStudy itm : userStudies) {
					if (itm.isPublisher()) {
						Study study = OfyService.ofy().load().key(itm.getStudyRef().getKey()).getValue();

						List<DataSheet> pendingFiles = OfyService.ofy().load().type(DataSheet.class)
								.filter("study", itm.getStudyRef())
								.filter("status !=", DataSheet.dataSheetStatus.PROCESSED)
								.filter("status !=", DataSheet.dataSheetStatus.ROLLINGBACK).list();

						StudyPendingFiles tmp = new StudyPendingFiles();
						tmp.setStudy(study);
						tmp.setFiles(pendingFiles);

						result.add(tmp);

					}
				}
				output.setStatusCode("200");
				output.setMessage("Success");
				output.setResult(result);
			} else {
				output.setStatusCode("404");
				output.setMessage("Null");
				output.setResult("");
			}
		} else {
			output.setStatusCode("404");
			output.setMessage("User Not Found.");
			output.setResult("");
		}

		return output;
	}

	@RequestMapping(value = "uploadStudyImages", method = RequestMethod.POST)
	public ModelAndView uploadStudyImages(@RequestParam("studyId") String studyId, HttpServletRequest req,
			HttpServletResponse res) throws IOException {
		Map<String, List<BlobKey>> blobs = blobstoreService.getUploads(req);

		List<BlobKey> blobKeys = blobs.get("images[]");

		if (blobKeys == null || blobKeys.isEmpty()) {
			res.sendRedirect("/");
			System.out.println("BolbKey Null or Empty");
			logger.error("BolbKey Null or Empty");

		} else {
			Study study = OfyService.ofy().load().type(Study.class).filter("studyID", studyId).first().getValue();
			for (BlobKey key : blobKeys) {
				BlobInfoFactory blobInfoFactory = new BlobInfoFactory();
				BlobInfo blobInfo = blobInfoFactory.loadBlobInfo(key);
				String fileName = blobInfo.getFilename();
				ImagesService imagesService = ImagesServiceFactory.getImagesService();
				ServingUrlOptions servingOptions = ServingUrlOptions.Builder.withBlobKey(key);
				String servingUrl = imagesService.getServingUrl(servingOptions);
				logger.info("Image " + fileName + " Uploaded to BlobStore.");

				if (fileName.endsWith(".png")) {
					study.setLogoUrl(servingUrl);
					study.setLogoBlobKey(key.getKeyString());
				}
			}
			OfyService.ofy().save().entity(study).now();
		}

		return new ModelAndView("get_study");
	}

	@RequestMapping("dashBoard")
	public void getDashBoard(@RequestParam("studyId") String studyId) {
		Study study = OfyService.ofy().load().type(Study.class).filter("studyID", studyId).first().getValue();
		// return siteController.getStudyDashboard(study);
		// siteController.getStudyDashboard(study);

	}

	private boolean isOwner(Study study) {
		String currentAdmin = UserServiceFactory.getUserService().getCurrentUser().getEmail();
		DataSheet dataSheet = OfyService.ofy().load().type(DataSheet.class)
				.filter("study", Key.create(Study.class, study.getId())).first().getValue();

		if (dataSheet.getUploadedBy().equals(currentAdmin)) {
			return true;
		}
		return false;
	}

	@RequestMapping(value = "publishStudy", method = RequestMethod.GET)
	public ModelAndView publishStudy(@RequestParam("id") Long id, HttpServletRequest req, HttpServletResponse res) {

		logger.info("publish Study");
		try {
			ModelAndView blockOperation = cmsController.blockIfOperationLock();

			if (blockOperation != null) {
				return blockOperation;
			}

			Study oldStudy = OfyService.ofy().load().key(Key.create(Study.class, id)).getValue();
			DataSheet dataSheet = OfyService.ofy().load().type(DataSheet.class)
					.filter("study", Key.create(Study.class, id)).first().getValue();

			if (AuthorizationValidatior.checkAdminAuthority(dataSheet)) {
				logger.info("User have Authority to Publish");
				oldStudy.setPublished(true);
				OfyService.ofy().save().entity(oldStudy).now();
				return new ModelAndView("cms/get_study");
			}
			return null;

		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return new ModelAndView("");
	}

	@RequestMapping(value = "unPublishStudy", method = RequestMethod.GET)
	public ModelAndView unPublishStudy(@RequestParam("id") Long id, HttpServletRequest req, HttpServletResponse res) {
		logger.info("unPublish Study");

		try {
			ModelAndView blockOperation = cmsController.blockIfOperationLock();

			if (blockOperation != null) {
				return blockOperation;
			}

			Study oldStudy = OfyService.ofy().load().key(Key.create(Study.class, id)).getValue();
			DataSheet dataSheet = OfyService.ofy().load().type(DataSheet.class)
					.filter("study", Key.create(Study.class, id)).first().getValue();

			if (AuthorizationValidatior.checkAdminAuthority(dataSheet)) {
				logger.info("User have Authority to Publish");
				oldStudy.setPublished(false);
				OfyService.ofy().save().entity(oldStudy).now();
				return new ModelAndView("get_study");
			}
			return null;

		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return new ModelAndView("");
	}

	@RequestMapping(value = "editStudy", method = RequestMethod.GET)
	public ModelAndView editStudy(@RequestParam("id") Long id, HttpServletRequest req, HttpServletResponse res) {
		logger.info("edit Study");

		Map<String, Object> model = new HashMap<String, Object>();

		Study oldStudy = OfyService.ofy().load().key(Key.create(Study.class, id)).getValue();
		model.put("study", oldStudy);
		return new ModelAndView("edit_study", model);

	}

	@RequestMapping(value = "linkToCreateNewStudy", method = RequestMethod.GET)
	public ModelAndView linkToCreateNewStudy(HttpServletRequest req, HttpServletResponse res) {
		logger.info("Link To Create New Study");

		return new ModelAndView("cms/create_study", null);

	}

	@RequestMapping(value = "linkToShare", method = RequestMethod.GET)
	public ModelAndView linkToShareStudy(@RequestParam("id") Long id, HttpServletRequest req, HttpServletResponse res) {
		logger.info("edit Study");

		Map<String, Object> model = new HashMap<String, Object>();

		Study oldStudy = OfyService.ofy().load().key(Key.create(Study.class, id)).getValue();
		model.put("study", oldStudy);
		return new ModelAndView("share_study", model);

	}

	public void deleteSharedStudyWithApplicationUser(Key<Study> key) {
		logger.info("Delete Study: " + key.getId() + " Application User..");

		List<ApplicationUser> usersList = OfyService.ofy().load().type(ApplicationUser.class)
				.filter("sharedStudies", key).filter("deleted", false).list();

		if (usersList != null) {
			for (ApplicationUser user : usersList) {
				List<Key<Study>> sharedStudies = user.getSharedStudies();
				if (sharedStudies.contains(key)) {
					sharedStudies.remove(key);
				}
			}
			OfyService.ofy().save().entities(usersList).now();
		} else {
			logger.warn("There Is No SharedStudies for Study: " + key.getId() + " to Delete.");
		}

	}

	private void deleteDivisionByStudyID(Key<Study> key) {
		logger.info("Delete Study:" + key.getId() + " Divisions... ");

		List<Division> divisions = OfyService.ofy().load().type(Division.class).filter("studies", key)
				.filter("deleted", false).list();
		if (divisions != null) {
			List<Division> tmp = new ArrayList<>();
			for (Division div : divisions) {
				// if (div.getStudies().size() == 1) {
				// div.setDeleted(true);
				// tmp.add(div);
				// }
			}
			OfyService.ofy().save().entities(tmp).now();
			// OfyService.ofy().delete().entities(tmp).now();
		} else {
			logger.warn("There Is No Divisions for Study: " + key.getId() + " to Delete.");
		}
	}

	private void deleteBrandsByStudyID(Key<Study> key) {
		logger.info("Delete Study: " + key.getId() + " Brands... ");

		List<Brand> brands = OfyService.ofy().load().type(Brand.class).filter("studies", key).filter("deleted", false)
				.list();

		if (brands != null) {
			List<Brand> tmp = new ArrayList<>();
			for (Brand brand : brands) {
				if (brand.getStudies().size() == 1) {
					brand.setDeleted(true);
					tmp.add(brand);
				}
			}
			OfyService.ofy().save().entities(tmp).now();
			// OfyService.ofy().delete().entities(tmp).now();
		} else {
			logger.warn("There Is No Brands for Study: " + key.getId() + " to Delete.");
		}
	}

	private void deleteQuestionsByStudyID(Key<Study> key) {
		logger.info("Delete Study: " + key.getId() + "Questions... ");

		List<Question> questions = OfyService.ofy().load().type(Question.class).filter("studies", key)
				.filter("deleted", false).list();

		if (questions != null) {
			List<Object> tmp = new ArrayList<>();

			for (Question q : questions) {
				// if (q.getStudies().size() == 1) {
				// q.setDeleted(true);
				// tmp.add(q);
				// }
			}
			OfyService.ofy().save().entities(tmp).now();
			// OfyService.ofy().delete().entities(tmp).now();
		} else {
			logger.warn("There Is No Questions for Study: " + key.getId() + " to Delete.");
		}
	}

	private void deleteAnswersByStudyID(Key<Study> key) {
		logger.info("Delete Study: " + key.getId() + " Answers... ");

		List<Answer> answers = OfyService.ofy().load().type(Answer.class).filter("studies", key)
				.filter("deleted", false).list();

		if (answers != null) {
			List<Object> tmp = new ArrayList<>();
			for (Answer ans : answers) {
				// if (ans.getStudies().size() == 1) {
				// ans.setDeleted(true);
				// tmp.add(ans);
				// }
			}
			OfyService.ofy().save().entities(tmp).now();
			// OfyService.ofy().delete().entities(tmp).now();
		} else {
			logger.warn("There Is No Answers for Study: " + key.getId() + " to Delete.");
		}
	}

	private void deleteFilterByStudyID(Key<Study> key) {
		logger.info("Delete Study: " + key.getId() + " Filters... ");

		List<Filter> filters = OfyService.ofy().load().type(Filter.class).filter("studies", key)
				.filter("deleted", false).list();

		if (filters != null) {
			List<Object> tmp = new ArrayList<>();
			for (Filter fil : filters) {
				// if (fil.getStudies().size() == 1) {
				//// fil.setDeleted(true);
				// tmp.add(fil);
				// }
			}
			OfyService.ofy().save().entities(tmp).now();
			// OfyService.ofy().delete().entities(tmp).now();
		} else {
			logger.warn("There Is No Filters for Study: " + key.getId() + " to Delete.");
		}
	}

	private void deleteDataSetByStudyID(Key<Study> key) {
		logger.info("Delete Study: " + key.getId() + " DataSet..");
		List<DataSet> dataSets = OfyService.ofy().load().type(DataSet.class).filter("study", key)
				.filter("deleted", false).list();
		if (dataSets != null) {
			for (DataSet ds : dataSets) {
				ds.setDeleted(true);
			}
			OfyService.ofy().save().entities(dataSets).now();
			// OfyService.ofy().delete().entities(dataSets).now();
		} else {
			logger.warn("There Is No DataSets for Study: " + key.getId() + " to Delete.");
		}
	}

	private void deleteDataSheetByStudyID(Key<Study> key) {
		logger.info("Delete Study: " + key.getId() + " DataSheets");

		List<DataSheet> dataSheets = OfyService.ofy().load().type(DataSheet.class).filter("study", key)
				.filter("deleted", false).list();
		if (dataSheets != null) {
			for (DataSheet dsh : dataSheets) {
				dsh.setDeleted(true);
			}
			OfyService.ofy().save().entities(dataSheets).now();
			// OfyService.ofy().delete().entities(dataSheets).now();
		} else {
			logger.warn("There Is No DataSheets for Study: " + key.getId() + " to Delete.");
		}
	}

	private void deleteDashBoardByStudyID(Key<Study> key) {
		logger.info("Delete Study: " + key.getId() + " DashBoard... ");

		List<Dashboard> dashboards = OfyService.ofy().load().type(Dashboard.class).filter("studies", key)
				.filter("deleted", false).list();

		if (dashboards != null) {
			List<Object> tmp = new ArrayList<>();
			for (Dashboard dashboard : dashboards) {
				if (dashboard.getStudies().size() == 1) {
					dashboard.setDeleted(true);
					tmp.add(dashboard);
				}
			}
			OfyService.ofy().save().entities(tmp).now();
			// OfyService.ofy().delete().entities(tmp).now();
		} else {
			logger.warn("There Is No DashBoards for Study: " + key.getId() + " to Delete.");
		}
	}

	public static String generateStudyID(String studyName) {
		return studyName.replaceAll("[^a-zA-Z0-9]+", "-");
	}

	@SuppressWarnings("unchecked")
	private <T> List<Object> getDeletedList(List<T> entities) {
		List<Object> tmp = new ArrayList<>();
		Object obj = null;
		Object tmpObj = null;

		for (int i = 0; i < entities.size(); i++) {
			tmpObj = entities.get(i);
			// if (Division.class.isInstance(tmpObj)) {
			// obj = ((Division) tmpObj).getStudies();
			// } else if (Brand.class.isInstance(tmpObj)) {
			// obj = ((Brand) tmpObj).getStudies();
			// } else if (Question.class.isInstance(tmpObj)) {
			// obj = ((Question) tmpObj).getStudies();
			// } else if (Answer.class.isInstance(tmpObj)) {
			// obj = ((Answer) tmpObj).getStudies();
			// } else if (Filter.class.isInstance(tmpObj)) {
			//// obj = ((Filter) tmpObj).getStudies();
			// }

			if (((List<Study>) obj).size() == 1) {
				tmp.add(obj);
			}
		}
		return tmp;
	}

	public static void main(String[] args) {

		Date d1 = new Date();
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Date d2 = new Date();

		long t1 = d1.getTime();
		System.out.println("T1: " + t1);

		long t2 = d2.getTime();
		System.out.println("T2: " + t2);

		long expireDate = t2 - t1;
		long diffSec = expireDate / 1000 / 60;
		System.out.println("Diff: " + diffSec + " mint.");

	}

	public ServiceOutput getStudyById(Study study) throws CustomException {
		ServiceOutput output = new ServiceOutput();
		Study study2 = OfyService.ofy().load().type(Study.class).filter("id", study.getId()).first().getValue();
		//
		// Set<AdminLookUp> publishers =
		// adminBackEnd.getAdminsByMail(study2.getPublishers());
		// Set<AdminLookUp> uploaders =
		// adminBackEnd.getAdminsByMail(study2.getUploaders());
		// Set<AdminLookUp> sharedWith =
		// adminBackEnd.getAdminsByMail(study2.getSharedWith());

		if (study2 != null) {
			output.setStatusCode("200");
			output.setMessage("Success");
			output.setResult(study2);
		} else {
			output.setStatusCode("404");
			output.setMessage("Study Not Found.");
			output.setResult("");
		}
		return output;
	}

	public int getNumberOfProcessdedSheetsInStudy(Study study) throws CustomException {
		Ref<Study> studyRef = Ref.create(Key.create(Study.class, study.getId()));
		if (studyRef != null) {
			List<DataSheet> datasheets = OfyService.ofy().load().type(DataSheet.class).filter("study", studyRef)
					.filter("deleted", false).filter("status", DataSheet.dataSheetStatus.PROCESSED).list();
			return (datasheets == null) ? 0 : datasheets.size();
		}
		return 0;
	}

	public void deleteLookupTableSheetRecordsInStudy(Study study, DataSheet dataSheet) throws CustomException {
		AsyncDatastoreService ds = DatastoreServiceFactory.getAsyncDatastoreService();

		DataSheet sheet = OfyService.ofy().load().type(DataSheet.class).filter("id", dataSheet.getId())
				.filter("deleted", false).first().getValue();
		Key<DataSheet> sheetKey = Key.create(DataSheet.class, sheet.getId());

		com.google.appengine.api.datastore.Query.Filter keyFilter = new Query.FilterPredicate("dataSheetKey",
				FilterOperator.EQUAL, sheetKey.toString());
		Query mediaQuery = new Query("lookUpDataTable" + study.getId()).setFilter(keyFilter);
		List<Entity> results = datastore.prepare(mediaQuery).asList(FetchOptions.Builder.withDefaults());
		for (Entity entity : results) {
			ds.delete(entity.getKey());
		}
	}

	public ServiceOutput rollBackDataSheet(DataSheet dataSheet) throws CustomException {
		Study study = dataSheet.getStudy().getValue();

		if (getNumberOfProcessdedSheetsInStudy(study) == 1) {
			deleteQuestionFilterAnswerRowsInStudy(study);
			deleteAllQuestionsAndAnswersAndFiltersInStudy(study);
		} else {
			deleteDataSheetKeyFromAllQuestionsAndAnswersAndFiltersInStudy(study, dataSheet);
		}
		deleteLookupTableSheetRecordsInStudy(study, dataSheet);

		ServiceOutput output = new ServiceOutput();
		output.setStatusCode("200");
		output.setMessage("Success");
		output.setResult("");
		return output;
	}

	private void deleteDataSheetKeyFromAllQuestionsAndAnswersAndFiltersInStudy(Study study, DataSheet dataSheet) {
		Key<Study> studyKey = Key.create(Study.class, study.getId());
		Key<DataSheet> dataSheetKey = Key.create(DataSheet.class, dataSheet.getId());

		List<Question> questions = OfyService.ofy().load().type(Question.class).filter("studyKey", studyKey).list();
		for (int i = 0; i < questions.size(); i++) {
			questions.get(i).getDatasheets().remove(dataSheetKey);
		}
		OfyService.ofy().save().entities(questions).now();

		List<Answer> answers = OfyService.ofy().load().type(Answer.class).filter("studyKey", studyKey).list();
		for (int i = 0; i < answers.size(); i++) {
			answers.get(i).getDatasheets().remove(dataSheetKey);
		}
		OfyService.ofy().save().entities(answers).now();

		List<Filter> filters = OfyService.ofy().load().type(Filter.class).filter("studyKey", studyKey).list();
		for (int i = 0; i < filters.size(); i++) {
			filters.get(i).getDatasheets().remove(dataSheetKey);
		}
		OfyService.ofy().save().entities(filters).now();
	}

	private void deleteAllQuestionsAndAnswersAndFiltersInStudy(Study study) {
		Key<Study> studyKey = Key.create(Study.class, study.getId());

		List<Question> questions = OfyService.ofy().load().type(Question.class).filter("studyKey", studyKey).list();
		OfyService.ofy().delete().entities(questions).now();

		List<Answer> answers = OfyService.ofy().load().type(Answer.class).filter("studyKey", studyKey).list();
		OfyService.ofy().delete().entities(answers).now();

		List<Filter> filters = OfyService.ofy().load().type(Filter.class).filter("studyKey", studyKey).list();
		OfyService.ofy().delete().entities(filters).now();
	}

	private void deleteQuestionFilterAnswerRowsInStudy(Study study) {
		Key<Study> studyKey = Key.create(Study.class, study.getId());
		QuestionFilterAnswerRows questionFilterAnswerRows = OfyService.ofy().load().type(QuestionFilterAnswerRows.class)
				.filter("studyKey", studyKey).first().getValue();
		OfyService.ofy().delete().entity(questionFilterAnswerRows).now();
	}
}

package com.brightcreations.gplus.module.controllers.MIDataExplorer;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tools.ant.util.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.brightcreations.gplus.module.controllers.OfyService;
import com.brightcreations.gplus.module.model.Answer;
import com.brightcreations.gplus.module.model.ApplicationUser;
import com.brightcreations.gplus.module.model.DataRow;
import com.brightcreations.gplus.module.model.DataSheet;
import com.brightcreations.gplus.module.model.Division;
import com.brightcreations.gplus.module.model.Filter;
import com.brightcreations.gplus.module.model.LogFile;
import com.brightcreations.gplus.module.model.Question;
import com.brightcreations.gplus.module.model.QuestionFilterAnswerRows;
import com.brightcreations.gplus.module.model.RowType;
import com.brightcreations.gplus.module.model.Study;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.QueryResultIterator;
import com.google.appengine.api.utils.SystemProperty;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;

import au.com.bytecode.opencsv.CSVReader;

@RequestMapping("/sheetImporter")
public class SheetImporter {
	private Logger logger = LoggerFactory.getLogger(getClass());
	private LogFile logFile = new LogFile();
	private int questionStartIndex = -1;
	private static final int QUESTION_SHEET_INDEX = 0;
	private static final int BASE_DATA = 1;
	private static final int QUESTION_TEXT = 2;
	private static final int ANSWER_ROW = 3;
	private static final int FILTER_ROW = 3;
	private static final int DIVISION_ROW = 4;
	private static final int DIVISION_LIMIT = 500;
	private List<String> headers;
	private Util util;
	private DataRowController dataRowController;

	private DatastoreService datastoreService = DatastoreServiceFactory.getDatastoreService();

	public SheetImporter() {
		util = new Util();
		dataRowController = new DataRowController();
		headers = new ArrayList<>();
	}

	public BufferedReader getCSVData(BlobKey blobKey, String dataSheetName) throws IOException {

		BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
		int readLimit = 5120;

		int start = 0;
		byte[] buff = null;

		buff = blobstoreService.fetchData(blobKey, start, (start + readLimit));
		start += readLimit + 1;

		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		buffer.write(buff);

		while (buff.length != 0) {

			buff = blobstoreService.fetchData(blobKey, start, (start + readLimit));
			buffer.write(buff);
			buffer.flush();
			start += readLimit + 1;
		}

		ZipInputStream localZin = new ZipInputStream(new ByteArrayInputStream(buffer.toByteArray()));
		buffer.close();
		buffer = null;
		ZipEntry zipEntry = localZin.getNextEntry();

		ByteArrayOutputStream bos = new ByteArrayOutputStream();

		BufferedReader localDis = new BufferedReader(new InputStreamReader(localZin, "UTF-8"));

		return localDis;
	}

	public String[] readRow(String rowString) throws IOException {
		// String rowString = localDis.readLine();
		if (rowString != null) {
			InputStreamReader reader = new InputStreamReader(new ByteArrayInputStream(rowString.getBytes("UTF-8")),
					"UTF-8");
			CSVReader allDataReader = new CSVReader(reader);
			String[] result = allDataReader.readAll().get(0);
			allDataReader.close();
			for (int i = 0; i < result.length; i++) {
				result[i] = result[i].trim();
			}
			return result;
		}
		return null;
	}

	public String[] readDataRow(int rowIndex, DataSheet dataSheet) throws IOException {
		String[] result = null;

		DataRow dataRow = OfyService.ofy().load().type(DataRow.class)
				.filter("datasheetKey", Key.create(DataSheet.class, dataSheet.getId())).filter("rowNumber", rowIndex)
				.first().getValue();
		if (dataRow != null) {
			result = readRow(dataRow.getRowValue());
		}

		return result;
	}

	private DataSheetBean getExcelQuestionsAndAnswersFields(DataSheet dataSheet) throws IOException {
		Key<Study> studyKey = getStudyKey(dataSheet);
		List<Key<DataSheet>> dataSheetKeyList = getListDataSheetKeys(dataSheet);
		List<String> questionsIndexs = new ArrayList<>(Arrays.asList(readDataRow(QUESTION_SHEET_INDEX, dataSheet)));
		List<String> baseData = new ArrayList<>(Arrays.asList(readDataRow(BASE_DATA, dataSheet)));
		List<String> questionData = new ArrayList<>(Arrays.asList(readDataRow(QUESTION_TEXT, dataSheet)));
		List<String> answerData = new ArrayList<>(Arrays.asList(readDataRow(ANSWER_ROW, dataSheet)));
		questionData.add("");
		answerData.add("");

		DataSheetBean bean = new DataSheetBean();
		bean.setStudyKey(studyKey);
		bean.setDataSheetKeyList(dataSheetKeyList);
		bean.setQuestionsIndexs(questionsIndexs);
		bean.setBaseData(baseData);
		bean.setQuestionData(questionData);
		bean.setAnswerData(answerData);

		return bean;
	}

	/**
	 * <h1>Extract Questions And Answers</h1>
	 * 
	 * @return List of Questions
	 * @param dataSheet
	 *            object
	 * 
	 **/
	private List<Question> extractQuestionAndAnswers(DataSheet dataSheet) throws IOException {

		// extract question and answer rawData
		DataSheetBean tmp = getExcelQuestionsAndAnswersFields(dataSheet);

		// where to start reading questions and answers pointer
		questionStartIndex = countFiltersNumber(tmp.getQuestionData());

		List<Question> questionsAndAnswers = new ArrayList<Question>();

		Question question = null;

		for (int i = questionStartIndex; i < tmp.getQuestionData().size(); i++) {

			// check pointer is Question or Answer

			// if Question
			if (!tmp.getQuestionData().get(i).trim().isEmpty()) {

				if (question != null)
					questionsAndAnswers.add(question);

				question = new Question();

				question = extractQuestion(tmp.getStudyKey(), tmp.dataSheetKeyList, tmp.getQuestionsIndexs(),
						tmp.getBaseData(), tmp.getQuestionData(), tmp.getAnswerData(), question, i);

			}
			// if not Question, so it's Answer for current Question
			else {
				// check End Of File
				if (!tmp.getAnswerData().get(i).isEmpty())
					question = extractAnswer(tmp.getStudyKey(), tmp.dataSheetKeyList, tmp.getAnswerData(), question, i);
				else {
					questionsAndAnswers.add(question);
					break;
				}

				/*
				 * just for testing json object of question
				 */
				// Map<String, ArrayList<String>> transientFilters = new
				// HashMap<String, ArrayList<String>>();
				//
				// ArrayList<String> wave = new ArrayList<>();
				// wave.add("2016");
				// ArrayList<String> age = new ArrayList<>();
				// age.add("10-15");
				// ArrayList<String> Occupation = new ArrayList<>();
				// Occupation.add("Not doing paid work");
				// ArrayList<String> gender = new ArrayList<>();
				// gender.add("male");
				// ArrayList<String> parent = new ArrayList<>();
				// parent.add("Yes");
				//
				// transientFilters.put("Wave", wave);
				// transientFilters.put("Agegroup", age);
				// transientFilters.put("Occupation", Occupation);
				// transientFilters.put("Gender", gender);
				// transientFilters.put("Parent", parent);

				// question.setTransientFilters(transientFilters);
				// System.out.println(util.createJsonString(question));
			}

		}
		return questionsAndAnswers;
	}

	/**
	 * <h1>extract question</h1>
	 * <p>
	 * construct Question Object to insert it in DB
	 * </p>
	 * 
	 * @param studykey
	 *            - Key<Study>
	 * @param dataSheetKeyList
	 *            - List<Key<DataSheet>>
	 * @param questionsIndexs
	 *            - title of question
	 * @param baseData
	 *            -
	 * @param questionData
	 *            - question raw text
	 * @param answerData
	 *            - answer raw text
	 * @param question
	 *            - question object
	 * @param i
	 *            - index
	 * 
	 */
	private Question extractQuestion(Key<Study> studyKey, List<Key<DataSheet>> dataSheetKeyList,
			List<String> questionsIndexs, List<String> baseData, List<String> questionData, List<String> answerData,
			Question question, int i) {

		question.setQuestionText(questionData.get(i).trim());
		question = extractBaseDataAndQuestionTitle(questionsIndexs, baseData, question, i);

		question.setStudyKey(studyKey);
		question.setDatasheets(dataSheetKeyList);

		// insert To DB
		question = addNewEntity(question);

		// save first answer for question
		question = extractAnswer(studyKey, dataSheetKeyList, answerData, question, i);
		return question;
	}

	/**
	 * <h1>Extract Answer</h1>
	 * 
	 * <p>
	 * Construct Answer Object
	 * </p>
	 * 
	 * @param studyKey
	 * @param dataSheetKeyList
	 * @param answerData
	 * @param question
	 * @param i
	 *            - index
	 */

	private Question extractAnswer(Key<Study> studyKey, List<Key<DataSheet>> dataSheetKeyList, List<String> answerData,
			Question question, int i) {

		Answer answer = new Answer();

		answer.setAnswerText(answerData.get(i).trim());
		answer.setStudyKey(studyKey);
		answer.setDatasheets(dataSheetKeyList);
		answer.setQuestionKey(Key.create(Question.class, question.getId()));

		answer = addNewEntity(answer);

		// generate questio and answer filter key
		String questionAnswerGeneratedKey = "Q" + question.getId() + "A" + answer.getId();
		answer.setQuestionAnswerKey(questionAnswerGeneratedKey);
		answer = addNewEntity(answer);
		// add generated key to header as filter
		headers.add(questionAnswerGeneratedKey);
		question.getTransientAnswers().add(answer);

		return question;
	}

	private List<Key<DataSheet>> getListDataSheetKeys(DataSheet dataSheet) {
		List<Key<DataSheet>> keys = new ArrayList<>();
		keys.add(Key.create(DataSheet.class, dataSheet.getId()));
		return keys;
	}

	private Key<Study> getStudyKey(DataSheet dataSheet) {
		Ref<Study> studyRef = dataSheet.getStudy();
		return studyRef.getKey();
	}

	private Question extractBaseDataAndQuestionTitle(List<String> questionsIndexs, List<String> baseData,
			Question question, int i) {
		if (baseData.size() > i && baseData.get(i) != null) {
			question.setBase(baseData.get(i).trim());
		}
		if (questionsIndexs.size() > i && questionsIndexs.get(i) != null) {
			question.setQuestionIndex(questionsIndexs.get(i).trim());
		}
		return question;
	}

	/**
	 * <h1>count filters</h2
	 */
	private int countFiltersNumber(List<String> questionData) {
		int numberOfFilters = 0;

		for (String itm : questionData) {
			if (itm.isEmpty()) {
				numberOfFilters++;
			} else {
				break;
			}
		}

		return numberOfFilters;
	}

	/**
	 * <h1>Add New Entity</h1>
	 * <p>
	 * Generic Function To Insert Entity into DB
	 * </p>
	 * 
	 * @param obj
	 *            - send any model Object -- ex: {Study, Question,...}
	 * 
	 * @return generic object of the same type inserted
	 */

	private <T> T addNewEntity(T obj) {

		Key<T> key = OfyService.ofy().save().entity(obj).now();

		return (T) OfyService.ofy().load().key(key).getValue();

	}

	private List<Filter> extractFilters(DataSheet dataSheet) throws IOException {

		// get study key from sheet
		Key<Study> studyKey = getStudyKey(dataSheet);

		// get dataSheet Key as list
		List<Key<DataSheet>> dataSheetKeyList = getListDataSheetKeys(dataSheet);

		// read filterData row
		String[] filterData = readDataRow(FILTER_ROW, dataSheet);

		List<Filter> filters = new ArrayList<Filter>();
		for (int i = 0; i < questionStartIndex; i++) {
			if (!filterData[i].trim().equals("")) {
				Filter filter = new Filter();
				filter.setName(filterData[i]);
				filter.setStudyKey(studyKey);
				filter.setDatasheets(dataSheetKeyList);
				filter = addNewEntity(filter);
				filters.add(filter);

				// add filter names to header list
				headers.add(i, filterData[i]);
			}
		}

		return filters;

	}

	@RequestMapping(value = "extractandvalidate", method = RequestMethod.GET)
	public void extractAndValidate(@RequestParam("id") long key, HttpServletResponse resp, HttpServletRequest req)
			throws IOException {

		try {
			// validate Tokens to processed in exporting data sheet
			if (util.validTokens(req)) {
				headers = new ArrayList<>();
				DataSheet localDataSheet = OfyService.ofy().load().key(Key.create(DataSheet.class, key)).get();

				// DataSheet localDataSheet = updateSheetStatus(key,
				// DataSheet.dataSheetStatus.PROCESSING.getStatus());

				// truncatExtractedData();

				// Read excel Data
				// BufferedReader bufferedReader = getCSVData(new
				// BlobKey(localDataSheet.getBlobstoreKey()),
				// localDataSheet.getName());
				if (isSheetValid(localDataSheet)) {

					BufferedReader bufferedReader = getCSVData(localDataSheet.getBlobstoreKey(),
							localDataSheet.getName());

					boolean isFoundSheetProcessedInStudy = isFoundSheetProcessedInStudy(localDataSheet);
					
					dataRowController.saveDataRows_NewFile(bufferedReader, Key.create(DataSheet.class, key),
							isFoundSheetProcessedInStudy);
					
					if (isFoundSheetProcessedInStudy) { 
						// update questions (add current sheet key to list of
						// keys in every question)
						// update filters
						// add devisions { set headers list with <-- all filters
						// + all answers[QidAid] of same study id}
					} else {
						// insert row data into DB 
						
						List<Question> questionsAndAnswers = extractQuestionAndAnswers(localDataSheet);

						List<Filter> filters = extractFilters(localDataSheet);

						List<Division> divisions = extractDivions(localDataSheet, filters);
					}

					System.out.println("********* Sheet Extracted. ***********");
					dataRowController.deleteRowData(localDataSheet);

					localDataSheet = updateSheetStatus(key, DataSheet.dataSheetStatus.PROCESSED.getStatus());
				} else {
					System.err.println("Sheet Is Differnet Formate.");
				}
			} else {
				System.out.println("Token Is Invalid, Or un-Authorized App trying to Connect..");
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		}
	}

	private boolean isFoundSheetProcessedInStudy(DataSheet dataSheet) {
		Key<Study> studyKey = dataSheet.getStudy().getKey();
		System.out.println("------>key " + studyKey);
		QuestionFilterAnswerRows questionFilterAnswerRows = OfyService.ofy().load().type(QuestionFilterAnswerRows.class)
				.filter("studyKey", studyKey).first().getValue();
		return questionFilterAnswerRows != null;
	}

	private boolean isSheetValid(DataSheet dataSheet) throws IOException {
		Key<Study> studyKey = dataSheet.getStudy().getKey();
		QuestionFilterAnswerRows questionFilterAnswerRows = OfyService.ofy().load().type(QuestionFilterAnswerRows.class)
				.filter("studyKey", studyKey).first().getValue();

		if ((questionFilterAnswerRows == null)) {
			return true;
		} else {
			BufferedReader bufferedReader = getCSVData(dataSheet.getBlobstoreKey(), dataSheet.getName());
			String line;
			int tracker = 0;
			List<String> newQuestionStrings = null;
			List<String> newFilterAnswerStrings = null;

			while ((line = bufferedReader.readLine()) != null) {
				if (tracker == 2) {
					newQuestionStrings = Arrays.asList(readRow(line));
				} else if (tracker == 3) {
					newFilterAnswerStrings = Arrays.asList(readRow(line));
				} else if (tracker > 3) {
					break;
				}
				tracker++;
			}
			List<String> oldQuestionString = Arrays.asList(readRow(questionFilterAnswerRows.getQuestionRow()));
			List<String> oldFilterAnswerString = Arrays.asList(readRow(questionFilterAnswerRows.getFilterAnswerRow()));

			if (oldQuestionString.size() != newQuestionStrings.size())
				return false;
			else if (oldFilterAnswerString.size() != newFilterAnswerStrings.size())
				return false;
			else {
				boolean oldQuestionEqualNew = oldQuestionString.equals(newQuestionStrings);
				boolean oldFilterEqualNew = oldFilterAnswerString.equals(newFilterAnswerStrings);

				if (!oldQuestionEqualNew) {
					return false;
				}

				else if (!oldFilterEqualNew) {
					return false;
				} else {
					return true;
				}
			}
		}
	}

	private DataSheet updateSheetStatus(long key, String status) {
		DataSheet localDataSheet = OfyService.ofy().load().key(Key.create(DataSheet.class, key)).get();
		localDataSheet.setStatus(status);
		Key<DataSheet> tmpKey = OfyService.ofy().save().entity(localDataSheet).now();
		localDataSheet = OfyService.ofy().load().key(tmpKey).getValue();
		return localDataSheet;
	}

	public List<Division> extractDivions(DataSheet dataSheet, List<Filter> filters) throws IOException {
		boolean flag = true;

		String curser = null;
		List<Division> divisions = new ArrayList<>();

		while (flag == true) {
			DivisionBulkCurser bulkOfDivisions = getBulkOfDivisions(dataSheet, curser, filters);
			curser = bulkOfDivisions.getCurser();
			List<Division> tmp = bulkOfDivisions.getDivisions();
			if (util.isNullOrEmpty(tmp))
				flag = false;
			divisions.addAll(tmp);

		}
		return divisions;
	}

	private void truncatExtractedData() {
		List<Question> questions = OfyService.ofy().load().type(Question.class).list();
		List<Answer> answers = OfyService.ofy().load().type(Answer.class).list();
		List<Filter> filters = OfyService.ofy().load().type(Filter.class).list();
		List<Division> divisions = OfyService.ofy().load().type(Division.class).list();
		List<DataRow> dataRows = OfyService.ofy().load().type(DataRow.class).list();

		OfyService.ofy().delete().entities(questions).now();
		OfyService.ofy().delete().entities(answers).now();
		OfyService.ofy().delete().entities(filters).now();
		OfyService.ofy().delete().entities(divisions).now();
		OfyService.ofy().delete().entities(dataRows).now();
		System.out.println("************Tables Truncated.**********");
	}

	private DivisionBulkCurser getBulkOfDivisions(DataSheet dataSheet, String curserString, List<Filter> filters)
			throws IOException {

		DivisionBulkCurser divisionBulkCurser = new DivisionBulkCurser();

		List<Key<DataSheet>> dataSheetKeyList = getListDataSheetKeys(dataSheet);
		Key<Study> studyKey = getStudyKey(dataSheet);

		List<Division> divisions = new ArrayList<>();

		com.googlecode.objectify.cmd.Query<DataRow> query = OfyService.ofy().load().type(DataRow.class)
				.limit(DIVISION_LIMIT).filter("datasheetKey", Key.create(DataSheet.class, dataSheet.getId()))
				.filter("rowType", RowType.DIVISION_AND_DATA);

		if (curserString != null && !curserString.isEmpty())
			query = query.startAt(Cursor.fromWebSafeString(curserString));

		QueryResultIterator<DataRow> iterator = query.iterator();

		List<Entity> entities = new ArrayList<>();
		// loop on result from DB
		while (iterator.hasNext()) {
			DataRow next = iterator.next();
			String rowValue = next.getRowValue();

			List<String> readRow = new ArrayList<>(Arrays.asList(readRow(rowValue)));

			for (int i = 0; i < readRow.size(); i++) {
				Division division = new Division();

				if (i < questionStartIndex) {
					division.setFilterKey(Key.create(Filter.class, filters.get(i).getId()));
				}
				String rowData = readRow.get(i);

				division.setName(rowData);
				division.setDatasheets(dataSheetKeyList);
				division.setStudyKey(studyKey);
				division = addNewEntity(division);
				divisions.add(division);
			}
			Entity createLookUpTable = createLookUpTable(readRow, dataSheetKeyList.get(0), studyKey);
			if (createLookUpTable != null)
				entities.add(createLookUpTable);
		}
		if (!entities.isEmpty()) {
			datastoreService.put(entities);
			System.out.println("LookUpTable Saved " + entities.size() + " Row.");

		}
		String currentCurser = iterator.getCursor().toWebSafeString();

		divisionBulkCurser.setCurser(currentCurser);
		divisionBulkCurser.setDivisions(divisions);

		return divisionBulkCurser;
	}

	public Entity createLookUpTable(List<String> rowData, Key<DataSheet> dataSheetKey, Key<Study> studyKey) {

		if (headers.size() == rowData.size()) {
			Entity lookUpDataTable = new Entity("lookUpDataTable" + studyKey.getId());

			for (int i = 0; i < headers.size(); i++) {
				lookUpDataTable.setProperty(headers.get(i), rowData.get(i));
			}
			lookUpDataTable.setProperty("dataSheetKey", dataSheetKey.toString());
			lookUpDataTable.setProperty("studyKey", studyKey.toString());

			return lookUpDataTable;
			// datastoreService.put(lookUpDataTable);
		} else {
			System.out.println("Header Size Is NOT Equal to Row Data Size.");
		}
		return null;
	}

	class DivisionBulkCurser {
		private List<Division> divisions;
		private String curser;

		public List<Division> getDivisions() {
			return divisions;
		}

		public void setDivisions(List<Division> divisions) {
			this.divisions = divisions;
		}

		public String getCurser() {
			return curser;
		}

		public void setCurser(String curser) {
			this.curser = curser;
		}

	}

	class DataSheetBean {
		private Key<Study> studyKey;
		private List<Key<DataSheet>> dataSheetKeyList;
		private List<String> questionsIndexs;
		private List<String> baseData;
		private List<String> questionData;
		private List<String> answerData;

		public List<String> getQuestionsIndexs() {
			return questionsIndexs;
		}

		public void setQuestionsIndexs(List<String> questionsIndexs) {
			this.questionsIndexs = questionsIndexs;
		}

		public List<String> getBaseData() {
			return baseData;
		}

		public void setBaseData(List<String> baseData) {
			this.baseData = baseData;
		}

		public List<String> getQuestionData() {
			return questionData;
		}

		public void setQuestionData(List<String> questionData) {
			this.questionData = questionData;
		}

		public List<String> getAnswerData() {
			return answerData;
		}

		public void setAnswerData(List<String> answerData) {
			this.answerData = answerData;
		}

		public List<Key<DataSheet>> getDataSheetKeyList() {
			return dataSheetKeyList;
		}

		public void setDataSheetKeyList(List<Key<DataSheet>> dataSheetKeyList) {
			this.dataSheetKeyList = dataSheetKeyList;
		}

		public Key<Study> getStudyKey() {
			return studyKey;
		}

		public void setStudyKey(Key<Study> studyKey) {
			this.studyKey = studyKey;
		}

	}

}

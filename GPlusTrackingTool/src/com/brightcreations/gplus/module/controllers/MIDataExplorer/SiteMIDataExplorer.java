/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.controllers.MIDataExplorer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.lang.reflect.Type;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.brightcreations.gplus.module.controllers.OfyService;
import com.brightcreations.gplus.module.controllers.MIDataExplorer.customClasses.DataLoader;
import com.brightcreations.gplus.module.model.Answer;
import com.brightcreations.gplus.module.model.DataRow;
import com.brightcreations.gplus.module.model.Division;
import com.brightcreations.gplus.module.model.PointNotes;
import com.brightcreations.gplus.module.model.Question;
import com.brightcreations.gplus.module.model.SelectorBean;
import com.brightcreations.gplus.module.model.Study;
import com.brightcreations.gplus.module.model.Widget;
import com.brightcreations.gplus.web.exception.CustomException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.googlecode.objectify.Key;

/**
 * 
 * @author mohammed eladly
 */

@Controller
@RequestMapping("/site/MIDataExplorer")
public class SiteMIDataExplorer {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private StudyBackEnd studyBackEnd;
	private WidgetBackEnd widgetBackEnd;
	private Util util;
	private PointNotesBackEnd notesBackEnd;
	private DataLoader dataLoader;

	public SiteMIDataExplorer() {
		studyBackEnd = new StudyBackEnd();
		widgetBackEnd = new WidgetBackEnd();
		util = new Util();
		notesBackEnd = new PointNotesBackEnd();
		dataLoader = new DataLoader();
	}

	@RequestMapping("openSite")
	public ModelAndView openSite() {

		return new ModelAndView("fake_pages/site_test");
	}

	/* ______________________________________________________________________________________ */
	/*************************************** Studies *****************************************/
	/* ____________________________________________________________________________________ */

	@RequestMapping(value = "loadPublicStudies", method = RequestMethod.GET)
	@ResponseBody
	public String loadPublicStudies(HttpServletRequest request, HttpServletResponse response) {

		try {
			return util.createJsonString(studyBackEnd.loadPublicStudies());
		} catch (IOException e) {
			logger.error(e.getMessage());
			return "ERROR: " + e.getMessage();
		}
	}

	@RequestMapping(value = "searchInPublicStudies", method = RequestMethod.GET)
	@ResponseBody
	public String searchInPublicStudies(HttpServletRequest request, HttpServletResponse response) {

		try {
			String searchWord = request.getParameter("searchWord");
			return util.createJsonString(studyBackEnd.searchInPublicStudies(searchWord));

		} catch (IOException | CustomException e) {
			logger.error(e.getMessage());
			return "ERROR: " + e.getMessage();
		}
	}

	@RequestMapping(value = "addFavoStudy", method = RequestMethod.PUT)
	@ResponseBody
	public String addFavoStudy(@RequestBody Study study, HttpServletRequest request, HttpServletResponse response) {

		try {

			return util.createJsonString(studyBackEnd.addFavouriteStudy(study));

		} catch (IOException | CustomException e) {
			logger.error(e.getMessage());
			return "ERROR: " + e.getMessage();
		}
	}

	@RequestMapping(value = "loadFavoStudies", method = RequestMethod.GET)
	@ResponseBody
	public String loadFavoStudies(HttpServletRequest request, HttpServletResponse response) {

		try {

			return util.createJsonString(studyBackEnd.loadFavoStudiesForLoggedInUser());

		} catch (IOException | CustomException e) {
			logger.error(e.getMessage());
			return "ERROR: " + e.getMessage();
		}
	}

	@RequestMapping(value = "removeFavoStudies", method = RequestMethod.POST)
	@ResponseBody
	public String removeFavoStudies(@RequestBody Study study, HttpServletRequest request,
			HttpServletResponse response) {

		try {

			return util.createJsonString(studyBackEnd.deleteFavoStudy(study));

		} catch (IOException | CustomException e) {
			logger.error(e.getMessage());
			return "ERROR: " + e.getMessage();
		}
	}

	/* ______________________________________________________________________________________ */
	/*************************************** Widgets *****************************************/
	/* ____________________________________________________________________________________ */

	@RequestMapping(value = "addWidget", method = RequestMethod.PUT)
	@ResponseBody
	public String addWidget(@RequestBody Widget widget, HttpServletRequest request, HttpServletResponse response) {

		try {
			return util.createJsonString(widgetBackEnd.createNewWidgetForLoggedInUser(widget));
		} catch (CustomException e) {
			logger.error(e.getMessage());
			return "ERROR: " + e.getMessage();
		} catch (JsonGenerationException e) {
			logger.error(e.getMessage());
			return "ERROR: " + e.getMessage();
		} catch (JsonMappingException e) {
			logger.error(e.getMessage());
			return "ERROR: " + e.getMessage();
		} catch (IOException e) {
			logger.error(e.getMessage());
			return "ERROR: " + e.getMessage();
		}
	}

	@RequestMapping(value = "renameWidget", method = RequestMethod.PUT)
	@ResponseBody
	public String renameWidget(@RequestBody Widget widget, HttpServletRequest request, HttpServletResponse response) {

		try {
			return util.createJsonString(widgetBackEnd.renameWidget(widget));
		} catch (CustomException e) {
			logger.error(e.getMessage());
			return "ERROR: " + e.getMessage();
		} catch (JsonGenerationException e) {
			logger.error(e.getMessage());
			return "ERROR: " + e.getMessage();
		} catch (JsonMappingException e) {
			logger.error(e.getMessage());
			return "ERROR: " + e.getMessage();
		} catch (IOException e) {
			logger.error(e.getMessage());
			return "ERROR: " + e.getMessage();
		}
	}

	@RequestMapping(value = "deleteWidget", method = RequestMethod.PUT)
	@ResponseBody
	public String deleteWidget(@RequestBody Widget widget, HttpServletRequest request, HttpServletResponse response) {
		try {
			return util.createJsonString(widgetBackEnd.deleteWidget(widget));
		} catch (IOException | CustomException e) {
			logger.error(e.getMessage());
			return "ERROR: " + e.getMessage();
		}
	}

	@RequestMapping(value = "loadLoggedInUserWidget", method = RequestMethod.GET)
	@ResponseBody
	public String loadLoggedInUserWidget(HttpServletRequest request, HttpServletResponse response) {
		try {
			return util.createJsonString(widgetBackEnd.loadLoggedInUserWidgets());
		} catch (IOException | CustomException e) {
			logger.error(e.getMessage());
			return "ERROR: " + e.getMessage();
		}
	}

	/* ______________________________________________________________________________________ */
	/*************************************** PointNotes **************************************/
	/* ____________________________________________________________________________________ */

	@RequestMapping(value = "createNewPointNote", method = RequestMethod.PUT)
	@ResponseBody
	public String createNewPointNote(@RequestBody Widget widget, @RequestBody PointNotes pointNotes,
			HttpServletRequest request, HttpServletResponse response) {
		try {
			return util.createJsonString(notesBackEnd.createNewPoint(widget, pointNotes));
		} catch (IOException e) {
			logger.error(e.getMessage());
			return "ERROR: " + e.getMessage();
		}

	}

	@RequestMapping(value = "updatePointNote", method = RequestMethod.GET)
	public String updatePoint(@RequestParam("title") String newTitle, @RequestParam("note") String newNote,
			@RequestParam("pointNoteID") Long pointNoteID) {
		try {
			return util.createJsonString(notesBackEnd.updatePoint(newTitle, newNote, pointNoteID));
		} catch (IOException e) {
			logger.error(e.getMessage());
			return "ERROR: " + e.getMessage();
		}
	}

	@RequestMapping(value = "deletePointNote", method = RequestMethod.GET)
	public String deletePoint(@RequestParam("widgetId") Long widgetId, @RequestParam("pointNoteID") Long pointId) {
		try {
			return util.createJsonString(notesBackEnd.deletePoint(widgetId, pointId));
		} catch (IOException e) {
			logger.error(e.getMessage());
			return "ERROR: " + e.getMessage();
		}
	}

	@RequestMapping(value = "loadWidgetNotePoints", method = RequestMethod.GET)
	public String loadWidgetNotePoints(@RequestParam("widgetId") Long widgetId) {
		try {
			return util.createJsonString(notesBackEnd.loadWidgetNotePoints(widgetId));
		} catch (IOException e) {
			logger.error(e.getMessage());
			return "ERROR: " + e.getMessage();
		}
	}

	@RequestMapping(value = "getFiltersValue", method = RequestMethod.POST)
	@ResponseBody
	public String getFiltersValue(@RequestBody List<Question> questions, HttpServletRequest request,
			HttpServletResponse response) {

		try {
			return util.createJsonString(dataLoader.getFiltersValue(questions));
		} catch (IOException e) {
			return "ERROR: " + e.getMessage();
		}

	}

	@RequestMapping(value = "truncate_db", method = RequestMethod.GET)
	public void truncateDb() {
		List<Question> questions = OfyService.ofy().load().type(Question.class).list();
		List<Answer> answers = OfyService.ofy().load().type(Answer.class).list();
		List<com.brightcreations.gplus.module.model.Filter> filters = OfyService.ofy().load()
				.type(com.brightcreations.gplus.module.model.Filter.class).list();
		List<Division> divisions = OfyService.ofy().load().type(Division.class).list();
		List<DataRow> dataRows = OfyService.ofy().load().type(DataRow.class).list();

		OfyService.ofy().delete().entities(questions).now();
		OfyService.ofy().delete().entities(answers).now();
		OfyService.ofy().delete().entities(filters).now();
		OfyService.ofy().delete().entities(divisions).now();
		OfyService.ofy().delete().entities(dataRows).now();

		System.out.println("************Tables Truncated.**********");
	}

	@RequestMapping("getValue")
	public String getValue() throws JsonGenerationException, JsonMappingException, IOException {
		List<Filter> tmp = new ArrayList<>();
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

		com.google.appengine.api.datastore.Query.Filter f1 = new Query.FilterPredicate("Agegroup", FilterOperator.EQUAL,
				"All");
		com.google.appengine.api.datastore.Query.Filter f2 = new Query.FilterPredicate("Country", FilterOperator.EQUAL,
				"All");
		com.google.appengine.api.datastore.Query.Filter f3 = new Query.FilterPredicate("Gender", FilterOperator.EQUAL,
				"male");
		com.google.appengine.api.datastore.Query.Filter f4 = new Query.FilterPredicate("InetUserP",
				FilterOperator.EQUAL, "All");
		com.google.appengine.api.datastore.Query.Filter f5 = new Query.FilterPredicate("Occupation",
				FilterOperator.EQUAL, "Not doing paid work (incl. DK/NA)");
		com.google.appengine.api.datastore.Query.Filter f6 = new Query.FilterPredicate("Parent", FilterOperator.EQUAL,
				"No");
		com.google.appengine.api.datastore.Query.Filter f7 = new Query.FilterPredicate("YTUser", FilterOperator.EQUAL,
				"All");
		com.google.appengine.api.datastore.Query.Filter f8 = new Query.FilterPredicate(Entity.KEY_RESERVED_PROPERTY,
				Query.FilterOperator.EQUAL,
				KeyFactory.createKey("lookUpDataTable", "Q4936120013946880A4584276293058560"));
		tmp.add(f1);
		tmp.add(f2);
		tmp.add(f3);
		tmp.add(f4);
		tmp.add(f5);
		tmp.add(f6);
		tmp.add(f7);
		// tmp.add(f8);

		String questionAnswerKey = "Q4936120013946880A4584276293058560";

		com.google.appengine.api.datastore.Query.CompositeFilter filters = null;

		filters = Query.CompositeFilterOperator.and(tmp);

		Query q = new Query("lookUpDataTable").setFilter(filters);

		List<Entity> entities = datastore.prepare(q).asList(FetchOptions.Builder.withLimit(20));
		for (Entity en : entities) {
			System.out.println(en.getProperty(questionAnswerKey));
		}
		return util.createJsonString(entities);
	}

	@RequestMapping(value = "loadStudyData", method = RequestMethod.GET)
	@ResponseBody
	public String loadStudyData(HttpServletRequest request, HttpServletResponse response) {
		try {
			return util.createJsonString(dataLoader.loadStudyData(request));
		} catch (IOException e) {
			logger.error(e.getMessage());
			return "ERROR: " + e.getMessage();
		}
	}
}

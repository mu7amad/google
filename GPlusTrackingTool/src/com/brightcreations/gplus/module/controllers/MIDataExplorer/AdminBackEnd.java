/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.controllers.MIDataExplorer;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.brightcreations.gplus.module.controllers.OfyService;
import com.brightcreations.gplus.module.controllers.MIDataExplorer.customClasses.AdminLookUp;
import com.brightcreations.gplus.module.controllers.MIDataExplorer.customClasses.ServiceOutput;
import com.brightcreations.gplus.module.model.ApplicationUser;
import com.brightcreations.gplus.module.model.ApplicationUserStatusEnum;
import com.brightcreations.gplus.module.model.CmsDashBoard;
import com.brightcreations.gplus.web.exception.CustomException;
import com.brightcreations.gplus.web.security.ApplicationUserRole;
import com.brightcreations.gplus.web.security.ApplicationUserType;
import com.google.api.services.drive.model.App;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.users.UserServiceFactory;
import com.googlecode.objectify.Key;

/**
 * @author Mohammed Eladly
 */

public class AdminBackEnd {
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private Util util = new Util();
	private DatastoreService datastore;

	public ServiceOutput createNewAdmin(String firstName, String lastName, String email, String status)
			throws CustomException, JsonGenerationException, JsonMappingException, IOException, ParseException {

		ServiceOutput output = new ServiceOutput();
		ApplicationUser user = getLoggedInUser();

		if (user.isSuperUser()) {
			if (!isStringNull(firstName) && !isStringNull(lastName) && !isStringNull(email) && !firstName.isEmpty()
					&& !lastName.isEmpty() && !email.isEmpty()) {

				if (validMailPattern(email)) {
					ApplicationUser applicationUser = OfyService.ofy().load().type(ApplicationUser.class)
							.filter("email", email.trim()).first().getValue();

					if (applicationUser == null) {

						Set<String> authorities = new HashSet<String>();

						authorities.add(ApplicationUserRole.ROLE_USER.getAuthority());
						authorities.add(ApplicationUserRole.ROLE_ADMIN.getAuthority());

						applicationUser = new ApplicationUser();

						applicationUser.setFirstName(firstName.trim());
						applicationUser.setLastName(lastName.trim());
						applicationUser.setFullName(firstName.trim() + " " + lastName.trim());
						applicationUser.setEmail(email);
						applicationUser.setAddedBy(user.getEmail());
						applicationUser.setAuthorities(authorities);

						if (status.equalsIgnoreCase("ACTIVE")) {
							applicationUser.setStatus(ApplicationUserStatusEnum.ACTIVE.toString());
						} else {
							applicationUser.setStatus(ApplicationUserStatusEnum.INACTIVE.toString());
						}
						SimpleDateFormat dt = new SimpleDateFormat("MMMM dd, yyyy", Locale.US);
						String date = dt.format(new Date());

						applicationUser.setCreationDate(date);
						applicationUser.setType(ApplicationUserType.ADMINISTRATOR.getType());
						long id = OfyService.ofy().save().entity(applicationUser).now().getId();

						ApplicationUser applicationUser2 = OfyService.ofy().load()
								.key(Key.create(ApplicationUser.class, id)).get();

						output.setMessage("Admin added successfully");
						output.setStatusCode("200");
						output.setResult(applicationUser2);
						return output;

					} // end if
					else {
						output.setMessage("Admin already exists.");
						output.setStatusCode("400");
						output.setResult("");
						return output;
					}
				} else {
					output.setMessage("Invalid Input");
					output.setResult("");
					output.setStatusCode("606");
					return output;
				}
			} // end if
			else {
				output.setMessage("Invalid Input");
				output.setResult("");
				output.setStatusCode("606");
				return output;
			}
		} else {
			output.setMessage("Unauthorized");
			output.setResult("");
			output.setStatusCode("401");
			return output;
		}

	}

	public ApplicationUser getAdminToEdit(Long adminId) throws CustomException {
		ApplicationUser applicationUser = OfyService.ofy().load().type(ApplicationUser.class).filter("id", adminId)
				.first().getValue();
		if (applicationUser != null) {
			return applicationUser;
		} else {
			throw new CustomException("Admin Not Found.");
		}
	}

	public ServiceOutput editAdmin(ApplicationUser admin)
			throws CustomException, JsonGenerationException, JsonMappingException, IOException {

		String firstName = admin.getFirstName();
		String lastName = admin.getLastName();
		String newEmail = admin.getEmail();
		String status = admin.getStatus();

		ServiceOutput output = new ServiceOutput();
		ApplicationUser adminToEdit = getAdminToEdit(admin.getId());
		ApplicationUser loggedInUser = getLoggedInUser();
		boolean superUser = isLoggedInUserSuperUser();

		if (adminToEdit != null) {

			if (superUser) {
				ApplicationUser existAdmin = OfyService.ofy().load().type(ApplicationUser.class)
						.filter("email", newEmail).first().getValue();

				if (existAdmin == null) {
					adminToEdit.setFirstName(firstName);
					adminToEdit.setLastName(lastName);
					adminToEdit.setStatus(status);
					adminToEdit.setEmail(newEmail);
					adminToEdit.setFullName(firstName + " " + lastName);
				} else {
					if (adminToEdit.getId().equals(existAdmin.getId())) {
						adminToEdit.setFirstName(firstName);
						adminToEdit.setLastName(lastName);
						adminToEdit.setStatus(status);
						adminToEdit.setEmail(newEmail);
						adminToEdit.setFullName(firstName + " " + lastName);
					} else {
						output.setStatusCode("403");
						output.setMessage("Email in Use.");
						output.setResult("");
						return output;
					}
				}
				long id = OfyService.ofy().save().entity(adminToEdit).now().getId();

				ApplicationUser applicationUser = OfyService.ofy().load().key(Key.create(ApplicationUser.class, id))
						.get();

				output.setStatusCode("200");
				output.setMessage("Admin edited successfully");
				output.setResult(applicationUser);

			} else if (loggedInUser.getId().equals(adminToEdit.getId())) {
				adminToEdit.setFirstName(firstName);
				adminToEdit.setLastName(lastName);
				adminToEdit.setFullName(firstName + " " + lastName);
				// adminToEdit.setStatus(status);
				if (!adminToEdit.getEmail().equalsIgnoreCase(newEmail)) {
					output.setStatusCode("401");
					output.setMessage("UnAuthorized To Edit Email.");
					output.setResult("");
					return output;
				}
				long id = OfyService.ofy().save().entity(adminToEdit).now().getId();

				ApplicationUser applicationUser = OfyService.ofy().load().key(Key.create(ApplicationUser.class, id))
						.get();

				output.setStatusCode("200");
				output.setMessage("Admin edited successfully");
				output.setResult(applicationUser);
			} else {
				output.setStatusCode("401");
				output.setMessage("UnAuthorized.");
				output.setResult("");
				return output;
			}

		} else {
			output.setStatusCode("404");
			output.setMessage("Null Pointer Exception");
			output.setResult("");
		}
		return output;

	}

	public ServiceOutput deleteAdmin(ApplicationUser admin)
			throws CustomException, JsonGenerationException, JsonMappingException, IOException {

		ApplicationUser adminToDelete = getAdminToEdit(admin.getId());
		ServiceOutput output = new ServiceOutput();

		if (adminToDelete.isSuperUser()) {
			output.setMessage("super admin can not be deleted");
			output.setStatusCode("401");
			output.setResult("");
			return output;
		}
		
		if (adminToDelete != null) {

			boolean superUser = isLoggedInUserSuperUser();
			if (superUser == true) {
				adminToDelete.setDeleted(true);
				// OfyService.ofy().save().entity(adminToDelete).now();
				OfyService.ofy().delete().entity(adminToDelete).now();
				output.setMessage("Admin deleted successfully");
				output.setStatusCode("200");
				output.setResult("");
				return output;

			} else {
				output.setMessage("Unauthorized");
				output.setStatusCode("401");
				output.setResult("");
				return output;
			}
		} else {

			output.setMessage("Not Found");
			output.setStatusCode("404");
			output.setResult("");
			return output;
		}

	}

	public boolean validMailPattern(String email) {
		// Pattern mailPattern = Pattern.compile(".*@google.com");
		// Matcher mailMatcher = mailPattern.matcher(email);
		// return mailMatcher.matches();
		return true;
	}

	public boolean isStringNull(String text) {
		if (text == null)
			return true;
		return false;
	}

	public ApplicationUser getLoggedInUser() {
		String loggedInUserEmail = UserServiceFactory.getUserService().getCurrentUser().getEmail();

		ApplicationUser appuser = OfyService.ofy().load().type(ApplicationUser.class)
				.filter("email", loggedInUserEmail.trim()).filter("deleted", false)
				.filter("status", ApplicationUserStatusEnum.ACTIVE.toString()).first().getValue();

		return appuser;
	}

	public boolean isLoggedInUserSuperUser() {

		if (getLoggedInUser().isSuperUser() == true)
			return true;

		return false;
	}

	public ServiceOutput loadAdmins(HttpServletRequest request)
			throws JsonGenerationException, JsonMappingException, IOException {
		ServiceOutput output = new ServiceOutput();

		List<ApplicationUser> list = OfyService.ofy().cache(false).load().type(ApplicationUser.class)
				.filter("type", "ADMINISTRATOR").list();

		// int limit = Integer.parseInt(request.getParameter("limit"));
		//
		// List<ApplicationUser> list = new ArrayList<>();
		// Query<ApplicationUser> query =
		// OfyService.ofy().load().type(ApplicationUser.class).limit(limit);
		//
		// String curserString = request.getParameter("curser");
		//
		// if (curserString != null && !curserString.isEmpty())
		// query = query.startAt(Cursor.fromWebSafeString(curserString));
		//
		// QueryResultIterator<ApplicationUser> iterator = query.iterator();
		//
		// while (iterator.hasNext()) {
		// ApplicationUser next = iterator.next();
		// list.add(next);
		// }
		// String currentCurser = iterator.getCursor().toWebSafeString();

		output.setMessage("Success");
		output.setStatusCode("200");
		output.setResult(list);
		// output.setCurser(currentCurser);

		return output;
	}

	public ServiceOutput loadAdminsLookUp(HttpServletRequest request)
			throws JsonGenerationException, JsonMappingException, IOException {
		ServiceOutput output = new ServiceOutput();

		datastore = DatastoreServiceFactory.getDatastoreService();
		String searchWord = request.getParameter("searchWord");
		boolean isAdmin = request.getParameter("isAdmin").equalsIgnoreCase("true") ? true : false;

		if (searchWord.length() >= 2) {

			Query q = searchByFirstName(searchWord, isAdmin);
			List<Entity> entities = datastore.prepare(q).asList(FetchOptions.Builder.withLimit(20));
			if (entities == null || entities.isEmpty()) {
				q = searchByLastName(searchWord, isAdmin);
				entities = datastore.prepare(q).asList(FetchOptions.Builder.withLimit(20));
			}
			if (entities == null || entities.isEmpty()) {
				q = searchByFullName(searchWord, isAdmin);
				entities = datastore.prepare(q).asList(FetchOptions.Builder.withLimit(20));
			}
			List<Key<ApplicationUser>> keys = new ArrayList<>();

			for (Entity en : entities) {
				Key<ApplicationUser> key = Key.create(ApplicationUser.class, en.getKey().getId());
				keys.add(key);
			}
			if (!util.isNullOrEmpty(keys)) {
				Collection<ApplicationUser> values = OfyService.ofy().load().keys(keys).values();
				output.setStatusCode("200");
				output.setMessage("Success");
				output.setResult(values);
				return output;
			}
		} else {
			output.setStatusCode("606");
			output.setMessage("Search Word Lenght is < 2 Characters");
			output.setResult("");
			return output;
		}
		output.setStatusCode("404");
		output.setMessage("No Matched Result.");
		output.setResult("");
		return output;
	}

	private Query searchByFirstName(String searchWord, boolean isAdmin) {
		com.google.appengine.api.datastore.Query.Filter prefixFilter = new Query.FilterPredicate("firstName",
				FilterOperator.GREATER_THAN_OR_EQUAL, searchWord);

		String endPrefix = util.getEndPrefix(searchWord);

		com.google.appengine.api.datastore.Query.CompositeFilter filters;

		com.google.appengine.api.datastore.Query.Filter postfixFilter = new Query.FilterPredicate("firstName",
				FilterOperator.LESS_THAN, endPrefix);
		com.google.appengine.api.datastore.Query.Filter statusFilter = new Query.FilterPredicate("status",
				FilterOperator.EQUAL, ApplicationUserStatusEnum.ACTIVE.toString());

		if (isAdmin) {
			com.google.appengine.api.datastore.Query.Filter typeFilter = new Query.FilterPredicate("type",
					FilterOperator.EQUAL, ApplicationUserType.ADMINISTRATOR.getType());

			filters = Query.CompositeFilterOperator.and(prefixFilter, postfixFilter, typeFilter, statusFilter);
		} else {
			filters = Query.CompositeFilterOperator.and(prefixFilter, postfixFilter, statusFilter);
		}
		Query q = new Query(ApplicationUser.class.getSimpleName()).setFilter(filters);
		return q;
	}

	private Query searchByFullName(String searchWord, boolean isAdmin) {
		com.google.appengine.api.datastore.Query.Filter prefixFilter = new Query.FilterPredicate("fullName",
				FilterOperator.GREATER_THAN_OR_EQUAL, searchWord);

		String endPrefix = util.getEndPrefix(searchWord);
		com.google.appengine.api.datastore.Query.CompositeFilter filters;

		com.google.appengine.api.datastore.Query.Filter postfixFilter = new Query.FilterPredicate("fullName",
				FilterOperator.LESS_THAN, endPrefix);
		com.google.appengine.api.datastore.Query.Filter statusFilter = new Query.FilterPredicate("status",
				FilterOperator.EQUAL, ApplicationUserStatusEnum.ACTIVE.toString());

		if (isAdmin) {
			com.google.appengine.api.datastore.Query.Filter typeFilter = new Query.FilterPredicate("type",
					FilterOperator.EQUAL, ApplicationUserType.ADMINISTRATOR.getType());

			filters = Query.CompositeFilterOperator.and(prefixFilter, postfixFilter, typeFilter, statusFilter);
		} else {
			filters = Query.CompositeFilterOperator.and(prefixFilter, postfixFilter, statusFilter);
		}
		Query q = new Query(ApplicationUser.class.getSimpleName()).setFilter(filters);
		return q;
	}

	private Query searchByLastName(String searchWord, boolean isAdmin) {
		com.google.appengine.api.datastore.Query.Filter prefixFilter = new Query.FilterPredicate("lastName",
				FilterOperator.GREATER_THAN_OR_EQUAL, searchWord);

		String endPrefix = util.getEndPrefix(searchWord);

		com.google.appengine.api.datastore.Query.CompositeFilter filters;

		com.google.appengine.api.datastore.Query.Filter postfixFilter = new Query.FilterPredicate("lastName",
				FilterOperator.LESS_THAN, endPrefix);

		com.google.appengine.api.datastore.Query.Filter statusFilter = new Query.FilterPredicate("status",
				FilterOperator.EQUAL, ApplicationUserStatusEnum.ACTIVE.toString());

		if (isAdmin) {
			com.google.appengine.api.datastore.Query.Filter typeFilter = new Query.FilterPredicate("type",
					FilterOperator.EQUAL, ApplicationUserType.ADMINISTRATOR.getType());

			filters = Query.CompositeFilterOperator.and(prefixFilter, postfixFilter, typeFilter, statusFilter);
		} else {
			filters = Query.CompositeFilterOperator.and(prefixFilter, postfixFilter, statusFilter);
		}
		Query q = new Query(ApplicationUser.class.getSimpleName()).setFilter(filters);
		return q;
	}

	public ServiceOutput loadUsersLookUp() {

		ServiceOutput output = new ServiceOutput();

		return output;
	}

	public List<ApplicationUser> getSuperAdmins() {
		return OfyService.ofy().load().type(ApplicationUser.class).filter("superUser", Boolean.TRUE)
				.filter("deleted", false).list();
	}

	public List<String> getAdminsEmails(List<ApplicationUser> applicationUsers) {
		List<String> tmp = null;

		if (applicationUsers != null) {
			tmp = new ArrayList<>();
			for (ApplicationUser itm : applicationUsers) {
				tmp.add(itm.getEmail());
			}
		}
		return tmp;
	}

	public Set<AdminLookUp> getAdminsByMail(Set<String> emails) {
		Set<AdminLookUp> lookUps = new HashSet<>();

		for (String itm : emails) {
			ApplicationUser user = OfyService.ofy().load().type(ApplicationUser.class).filter("email", itm).first()
					.getValue();
			if (user != null) {
				AdminLookUp tmp = new AdminLookUp();
				tmp.setFirstName(user.getFirstName());
				tmp.setLastName(user.getLastName());
				tmp.setFullName(user.getFirstName() + " " + user.getLastName());
				tmp.setEmail(user.getEmail());
				tmp.setImgURL(user.getImgURL());
				lookUps.add(tmp);
			}

		}
		return lookUps;
	}

	public ServiceOutput addAboutDashBoard(CmsDashBoard dashBoard) throws CustomException {
		ServiceOutput output = new ServiceOutput();

		ApplicationUser loggedInUser = getLoggedInUser();

		if (loggedInUser.isSuperUser()) {

			CmsDashBoard oldCmsDashboard = OfyService.ofy().cache(false).load().type(CmsDashBoard.class).first()
					.getValue();

			if (oldCmsDashboard != null) {
				OfyService.ofy().delete().entity(oldCmsDashboard).now();
			}

			CmsDashBoard newBoard = new CmsDashBoard();
			newBoard.setAbout(dashBoard.getAbout());

			SimpleDateFormat dt = new SimpleDateFormat("MMMM dd, yyyy", Locale.US);
			String date = dt.format(new Date());

			newBoard.setCreationDate(date);
			newBoard.setModifiedDate(date);

			Key<CmsDashBoard> key = OfyService.ofy().save().entity(newBoard).now();
			CmsDashBoard value = OfyService.ofy().load().type(CmsDashBoard.class).filter("id", key.getId()).first()
					.getValue();

			output.setStatusCode("200");
			output.setMessage("Success");
			output.setResult(value);

		} else {
			output.setStatusCode("401");
			output.setMessage("Admin Not Authorized.");
			output.setResult("");
		}
		return output;
	}

	public ServiceOutput loadAboutDashBoard() {
		ServiceOutput output = new ServiceOutput();
		CmsDashBoard value = OfyService.ofy().cache(false).load().type(CmsDashBoard.class).first().getValue();
		// if (value == null) {
		// value.setAbout("about");
		// }
		output.setStatusCode("200");
		output.setMessage("Success");
		output.setResult(value);
		return output;
	}

	/**
	 * check is loggedIn user admin or not.
	 * 
	 * @param request
	 * @param response
	 * @return boolean.
	 */
	@RequestMapping(value = "isLoggedInUserAdmin", method = RequestMethod.GET)
	@ResponseBody
	public boolean isLoggedInUserAdmin(HttpServletRequest request, HttpServletResponse response) {
		logger.info("isLoggedInUserAdmin");
		String currentAdmin = UserServiceFactory.getUserService().getCurrentUser().getEmail();

		String loggedInUserType = OfyService.ofy().load().type(ApplicationUser.class).filter("email", currentAdmin)
				.filter("deleted", false).first().getValue().getType();

		if (ApplicationUserType.ADMINISTRATOR.getType().equals(loggedInUserType)) {
			logger.info("LoggedIn User Is Admin.");
			return true;
		}
		logger.info("LoggedIn User Is Not Admin.");
		return false;
	}

}

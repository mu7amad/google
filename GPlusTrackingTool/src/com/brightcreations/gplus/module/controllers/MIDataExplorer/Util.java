package com.brightcreations.gplus.module.controllers.MIDataExplorer;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.Properties;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;

import com.brightcreations.gplus.module.controllers.OfyService;
import com.brightcreations.gplus.module.model.ApplicationUser;
import com.brightcreations.gplus.module.model.ConfigEntity;
import com.google.appengine.api.modules.ModulesServiceFactory;
import com.google.appengine.api.urlfetch.FetchOptions;
import com.google.appengine.api.urlfetch.HTTPHeader;
import com.google.appengine.api.urlfetch.HTTPMethod;
import com.google.appengine.api.urlfetch.HTTPRequest;
import com.google.appengine.api.urlfetch.URLFetchService;
import com.google.appengine.api.urlfetch.URLFetchServiceFactory;
import com.google.appengine.api.utils.SystemProperty;
import com.google.gson.Gson;
import com.googlecode.objectify.Key;

public class Util {
	public String createJsonString(Object obj) throws JsonGenerationException, JsonMappingException, IOException {
		// ObjectMapper mapper = new ObjectMapper();
		// String writeValueAsString = mapper.writeValueAsString(obj);
		Gson gson = new Gson();
		String json = gson.toJson(obj);
		return json;
	}

	public Object createJsonObject(String str, Object obj) {
		Gson gson = new Gson();
		Object fromJson = gson.fromJson(str, obj.getClass());

		return fromJson;
	}

	public boolean isNullOrEmpty(final Collection<?> c) {
		return c == null || c.isEmpty();
	}

	public boolean isProduction() {
		return SystemProperty.environment.value() == SystemProperty.Environment.Value.Production;
	}

	public Properties readConfigProperties() {
		Properties prop = new Properties();
		InputStream input = null;

		try {
			// input =
			// this.getClass().getClassLoader().getResourceAsStream("./config.properties");
			String propertiesFilePath = System.getProperty("user.dir") + "/WEB-INF/config.properties";

			input = new FileInputStream(propertiesFilePath);

			if (input != null) {
				// load a properties file
				prop.load(input);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return prop;
	}

	public String capitalize(String line) {
		return Character.toUpperCase(line.charAt(0)) + line.substring(1);
	}

	public boolean checkProductionEnv() {
		boolean PRODUCTION_MODE = SystemProperty.environment.value() == SystemProperty.Environment.Value.Production;
		return PRODUCTION_MODE;
	}

	public String getEndPrefix(String prefix) {

		if (prefix == null || prefix.isEmpty()) {
			return prefix;
		}

		char lastChar = prefix.charAt(prefix.length() - 1);
		int ascii = (int) lastChar;
		char nextLastChar = (char) ++ascii;

		if (prefix.length() <= 1) {
			return Character.toString(nextLastChar);
		}

		return prefix.substring(0, prefix.length() - 1) + Character.toString(nextLastChar);
	}

	/**
	 * <p>
	 * create url to call a specific service in module
	 * </p>
	 * PLEASE DON'T CHANGE ANYTHING IN THIS FUNCTION EXCEPT, MAHMOUD_ZAKI,
	 * MOHAMMED_ELADLY
	 * 
	 * @param serviceName
	 *            : Service To Call
	 * @param moduleName
	 *            : module to call like {import-data, delete-data}
	 */
	public String createUrl(String serviceName, String moduleName) {

		System.out.println("+++ Creating URL+++");

		boolean PRODUCTION_MODE = checkProductionEnv();

		// type specific version is your Issue.
		// String versionHostname =
		// ModulesServiceFactory.getModulesService().getVersionHostname("import-data",
		// null);

		System.out.println("Service Name: " + serviceName);
		System.out.println("Module Name: " + moduleName);

		String versionHostname = ModulesServiceFactory.getModulesService().getVersionHostname(moduleName, null);

		String url = "http://" + versionHostname + serviceName;

		if (PRODUCTION_MODE == true) {

			System.out.println("PRODUCTION_MODE: ");

			url = "https://";
			versionHostname = versionHostname.replace(".appspot.com", "").replace(".", "-dot-");
			url += versionHostname + ".appspot.com" + serviceName;
		}
		return url;
	}

	public void fetchAsyncURL(String url, HttpServletRequest req, HttpServletResponse res) {

		try {

			URLFetchService urlFetchService = URLFetchServiceFactory.getURLFetchService();

			URL urlObj = new URL(url);

			FetchOptions fetchOptions = FetchOptions.Builder.withDefaults();

			HTTPRequest request = new HTTPRequest(urlObj, HTTPMethod.GET, fetchOptions.doNotFollowRedirects());

			String cookie = "";

			Cookie[] cookies = req.getCookies();

			for (int i = 0; i < cookies.length; i++) {
				cookie += cookies[i].getName() + "=" + cookies[i].getValue() + ";";
			}
			cookie = cookie.substring(0, cookie.length() - 1);
			request.setHeader(new HTTPHeader("Cookie", cookie));

			System.out.println("++++ COOKIE: " + cookie);

			urlFetchService.fetchAsync(request);

			System.out.println("done");

		} catch (MalformedURLException e) {
			System.err.println(e.getMessage());
		} catch (IOException e) {
			System.err.println(e.getMessage());

		}

	}

	public boolean validTokens(HttpServletRequest req) {
		System.out.println("ValidTokens");
		String currentAdmin = req.getParameter("_user");
		String adminToken = req.getParameter("_admin_token");
		String instanceAppId = SystemProperty.applicationId.get();
		String requestAppId = req.getParameter("_appId");

		Key<ApplicationUser> appUserKey = OfyService.ofy().load().type(ApplicationUser.class)
				.filter("email", currentAdmin).first().getKey();

		AdminTokenGenerator adminTokenGenerator = new AdminTokenGenerator();
		boolean validateToken = adminTokenGenerator.validateToken(adminToken, appUserKey);
		if (validateToken && (instanceAppId.equals(requestAppId)))
			return true;

		return false;
	}

	public static void main(String[] args) {
		String url = "https://lh6.googleusercontent.com/-bWIfeE4-feE/AAAAAAAAAAI/AAAAAAAAAAk/JsuGxuGqsmw/photo.jpg";
		System.out.println(url);
		System.out.println(url.replace("/photo.jpg", "/s96-cc/photo.jpg"));

	}
}

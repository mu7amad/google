/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.brightcreations.gplus.module.model.ApplicationUser;
import com.google.appengine.api.backends.BackendServiceFactory;

/**
 * 
 * @author Bright Creations
 * 
 */

@RequestMapping("/user")
public class UserController {
	private Logger logger = LoggerFactory.getLogger(getClass());

	@RequestMapping(value = "/{id}")
	public ModelAndView getUser(@PathVariable("id") Long id, HttpServletResponse resp) {
		String backendAddress = BackendServiceFactory.getBackendService().getBackendAddress("importing-backend");
		resp.setHeader("host", backendAddress);

		long c = System.currentTimeMillis();
		long cend = c + 1000 * 60 * 3;
		while (true) {
			c = System.currentTimeMillis();
			if (c > cend) {
				break;
			}
		}
		ApplicationUser user = ApplicationUser.getApplicationUser(id);
		logger.info(user.getFirstName());
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("user", user);
		return new ModelAndView("user", model);
	}

	@RequestMapping("")
	public ModelAndView getUserList(@RequestParam(value = "sortby", defaultValue = "firstName") String sortBy,
			@RequestParam(value = "order", defaultValue = "asc") String order,
			@RequestParam(value = "from", defaultValue = "0") Long from,
			@RequestParam(value = "to", defaultValue = "100000") Long to) {
		List<ApplicationUser> applicationUserList = ApplicationUser.getApplicationUserList(sortBy, order, from, to);
		for (ApplicationUser user : applicationUserList) {
			logger.info(user.getFirstName());
		}
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("user", applicationUserList);
		return new ModelAndView("user", model);
	}

	@RequestMapping(value = "/post", method = RequestMethod.POST)
	public ModelAndView postUser(ApplicationUser user) {
		logger.info(user.getFirstName());
		Map<String, Object> model = new HashMap<String, Object>();
		return new ModelAndView("user", model);
	}

	@RequestMapping(value = "/put", method = RequestMethod.POST)
	public ModelAndView putUser(ApplicationUser user) {
		user.save();
		logger.info(user.getFirstName());
		Map<String, Object> model = new HashMap<String, Object>();
		return new ModelAndView("user", model);
	}

}

/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package com.brightcreations.gplus.module.intercetpors;

/**
 * @author Ramy Mahmoudi
 */
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class HandlerInterceptor extends HandlerInterceptorAdapter {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	// before the actual handler will be executed
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		logger.info("preHandle");
		if (request.getMethod().equals(RequestMethod.GET.toString())) {

			logger.info("Request prehandle.");
			logger.info("Session token: " + request.getSession().getAttribute("stk"));
			logger.info("Request method: " + request.getMethod());

			logger.info("REQUEST TOKEN: " + request.getParameter("tk"));

			Cookie[] cookies = request.getCookies();
			Cookie cookie = null;
			if (cookies != null) {
				for (int i = 0; i < cookies.length; i++) {
					if (cookies[i].getName().equals("stk")) {
						cookie = cookies[i];
					}
				}
			}

			HttpSession session = request.getSession();
			@SuppressWarnings("unchecked")
			Map<String, String> sessionTokens = (Map<String, String>) session.getAttribute("tkm");

			// System.out.println(request.getRequestURI());

			if (sessionTokens == null || cookie == null || sessionTokens.get(request.getRequestURI()) == null
					|| !cookie.getValue().equals((String) session.getAttribute("globalToken"))) {

				String token = generateToken();

				if (sessionTokens == null) {
					sessionTokens = new HashMap<String, String>();
					session.setAttribute("tkm", sessionTokens);
				}

				String globalSessionToken = (String) session.getAttribute("globalToken");

				if (globalSessionToken == null) {
					String globalToken = generateToken();
					session.setAttribute("globalToken", globalToken);

					Cookie newCookie = new Cookie("stk", "" + globalToken);
					newCookie.setPath("/");
					if (!request.getServerName().equals("localhost")) {
						newCookie.setSecure(true);
					}
					// newCookie.setMaxAge(60);
					response.addCookie(newCookie);
				}

				sessionTokens.put(request.getRequestURI(), token);

				session.setMaxInactiveInterval(36000);
			}

			logger.info("TOKEN: " + ((Map<String, String>) session.getAttribute("tkm")).get(request.getRequestURI()));
		}
		return true;
	}

	/**
	 * @return
	 */
	public String generateToken() {
		/*
		 * Assign a string that contains the set of characters you allow.
		 */
		final String symbols = "ABCDEFGJKLMNPRSTUVWXYZ-abcdefghijklmnopqrstuvwxyz_0123456789";

		final Random random = new SecureRandom();

		char[] buf;

		int length = 50;
		buf = new char[length];
		for (int idx = 0; idx < buf.length; ++idx)
			buf[idx] = symbols.charAt(random.nextInt(symbols.length()));
		return new String(buf);
	}

	// after the handler is executed
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {

		List<MediaType> acceptableMediaTypes = MediaType.parseMediaTypes(request.getHeader("Accept"));

		if (acceptableMediaTypes.size() > 0) {
			if (acceptableMediaTypes.get(0).getSubtype().equals(MediaType.TEXT_HTML.getSubtype())) {
				if (modelAndView != null) {
					Map<String, Object> model = modelAndView.getModel();
					if (model != null) {
						HttpSession session = request.getSession();
						if (session != null) {
							@SuppressWarnings("unchecked")
							Map<String, String> sessionTokens = ((Map<String, String>) session.getAttribute("tkm"));
							String sessionToken = sessionTokens.get(request.getRequestURI());
							logger.info("Injecting view with the token");
							model.put("tk", sessionToken);
						}
					}
				}
			}
		}

	}
}

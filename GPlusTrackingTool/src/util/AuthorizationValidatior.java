/*******************************************************
* Copyright (c) 2016 Bright Creations , All Rights Reserved.
* NOTICE:  All information contained herein is, and remains the property of Bright Creations. 
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* Access to the source code contained herein is hereby forbidden to anyone except current Bright Creations employees, managers or parties given license to view and create derivative works for the source code and who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
*******************************************************/

package util;

import com.brightcreations.gplus.module.model.DataSheet;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserServiceFactory;

/**
 * 
 * @author Mohammed.ElAdly
 * 
 */

public class AuthorizationValidatior {

	public static boolean checkAdminAuthority(DataSheet dataSheet) {
		String currentAdmin = AuthorizationValidatior.getCurrentUser()
				.getEmail();
		if (!dataSheet.getUploadedBy().equals(currentAdmin)) {
			return true;
		}
		return false;
	}
	public static User getCurrentUser() {
		return UserServiceFactory.getUserService().getCurrentUser();
	}

}

var Site = {

	/**
	 * Init Function
	 */
	init : function() {
		// $("body").removeClass("no-js");
		Site.tableSort();
		Site.Selectbox();
		Site.validate();
		Site.processAndVerify();
		Site.rollback();
		Site.confirm();
		Site.initEditQuestion();
		Site.initFiltersOrderSortable();
		Site.initCategoriesOrderSortable();
		Site.publishAndUnpublishWave();
		Site.confirmDeleteCategory();
		Site.updateSortingArrows();
		Site.rollBackDataSheetOnEditDataSetPage();
	},

	/**
	 * Table Sort
	 */
	tableSort : function() {
		// $('#tab-01 .sorting-arrow').click(fucntion(){
		//		
		// });
	},

	/**
	 * Select Box
	 */
	Selectbox : function() {
		$(function() {
			$(".regions-select, .type-select, .trending-select").selectbox();
		});
	},

	/**
	 * @author Ismail Marmoush
	 */
	confirm : function() {
		$('.confirm').click(function() {
			var answer = confirm("Are you sure?");
			if (answer) {
				return true;
			} else {
				return false;
			}
		});
	},

	/**
	 * Validate Forms class="maia-form-error"
	 */

	validate : function() {

		$.validator.addMethod("required-field", function(value, element) {
			return this.optional(element) || /^[\w-\.]+$/i.test(value);
		}, "this field is required");

		$.validator.addMethod("gmailandgoogle", function(value, element) {
			return this.optional(element)
					|| /^[\w-\.]+@(gmail\.com|google\.com)+$/i.test(value);
		}, "Email address must be gmail or google address.");

		$.validator.addMethod("gmail", function(value, element) {
			return this.optional(element)
					|| /^[\w-\.]+@(gmail\.com)+$/i.test(value);
		}, "Email address must be gmail.");

		$.validator.addMethod("google", function(value, element) {
			return this.optional(element)
					|| /^[\w-\.]+@(google\.com)+$/i.test(value);
		}, "Email address must be google address.");

		$('.roles').change(function() {
			var selectVal = $('.roles :selected').val();
			// alert(selectVal);
			// if ($(this).val() == 'ROLE_RESEARCHER') {
			// $('li.email-role').find('input.emailval').addClass(
			// 'gmailandgoogle').removeClass('google');
			// }
			// if ($(this).val() == 'ROLE_ADMIN') {
			// $('li.email-role').find('input.emailval').addClass(
			// 'google').removeClass('gmailandgoogle');
			// }
			return false;
		});

		$(".validate").validate(
				{
					errorElement : "p",
					invalidHandler : function(form, validator) {
						var errors = validator.numberOfInvalids();
						if (errors) {
							var message = errors == 1
							$(".maia-form-error-msg").show().parent('li')
									.addClass('maia-form-error');
						} else {
							$(".maia-form-error-msg").hide().parent('li')
									.Class('maia-form-error');
						}
					},
					rules : {
						email : {
							required : true,
							email : true
						}
					}
				});
	},

	confirmDeleteCategory : function() {
		$(".confirm-delete-catgeory")
				.live(
						'click',
						function(e) {
							var answer = confirm("Are you sure you want to delete this category?");
							if (answer) {
								return true;
							} else {
								e.preventDefault();
								return false;
							}
						});
	},
	processAndVerify : function() {
		$('.process-datasheet')
				.live(
						'click',
						function(e) {
							e.preventDefault();
							var answer = confirm("Are you sure you want to process this file?");
							if (answer) {
								var form = $(this).parents('form');
								$.ajax({
									url : form.attr('action'),
									data : form.serialize(),
									async : false,
									success : function() {
										$(this).html('Processing..');
										location.reload();
									},
									error : function(jqXHR, textStatus,
											errorThrown) {
									},
									dataType : 'json',
									type : 'POST'
								});
							}
						});

	},

	publishAndUnpublishWave : function() {
		$('.publish-unpublish').live('click', function(e) {
			e.preventDefault();
			var form = $(this).parents('form');
			var url = form.attr('action');
			var a = $(this);
			$.ajax({
				url : url,
				async : false,
				data : form.serialize(),
				success : function() {
					if (url.indexOf('unpublish') != -1) {
						a.html("Publish");
						var newUrl = url.replace('unpublish', 'publish');
						form.attr('action', newUrl);
					} else {
						a.html("Unpublish");
						var newUrl = url.replace('publish', 'unpublish');
						form.attr('action', newUrl);
					}
				},
				error : function(jqXHR, textStatus, errorThrown) {
				},
				dataType : 'json',
				type : 'POST'
			});

		});

	},

	rollback : function() {
		$('.rollback-datasheet').live('click', function(e) {
			e.preventDefault();
			var answer = confirm("Are you sure you want to rollback?");
			if (answer) {
				var button = $(this);
				$.ajax({
					url : $(this).parents("form").attr('action'),
					data : $(this).parents("form").serialize(),
					async : false,
					success : function() {
						button.html('Rollingback..');
						location.reload();
					},
					error : function(jqXHR, textStatus, errorThrown) {
					},
					dataType : 'json',
					type : 'POST'
				});
			}
		});

	},

	rollBackDataSheetOnEditDataSetPage : function() {

		$('#rollback-datasheet-edit-dataset').on('click', function(e) {

			var id = document.getElementById('datasheet-id').value;
			// alert("rollback-datasheet-edit-dataset " + id);
			$.ajax({

				type : 'POST',
				url : "/cms/process/entry/rollbackdatasheet/" + id,
				data : {
					"tk" : $('#_tk').val(),
					"_csrf" : $('#_csrf').val()
				},
				success : function(data) {
					console.log('success', data);

				},
				error : function(exception) {
					console.log('Exeption:' + exception);
				}
			});

		});
	},

	initEditQuestion : function() {
		$("#addGroup")
				.click(
						function() {
							var name = $('#newGroupName').val();
							if (name != null) {
								name = Site.escapeHtml(name);
							}
							$('#newGroupName').val("");
							if (name != "") {
								var count = $(".groupRow").length;
								$("#groups_table")
										.append(
												"<tr class='tiny-raw groupRow grp-"
														+ count
														+ "'><td>"
														+ name
														+ "<input type='text'"
														+ "hidden='true' value='"
														+ name
														+ "' name='grp-"
														+ count
														+ "'></input></td>"
														+ "<td class='tiny-col'><a href='' class='remove-icon grp-remove-icon' id='"
														+ count
														+ "'></a></td></tr>");
								$('.trendSelect').each(
										function() {
											$(this).append(
													"<option class='grp-"
															+ count + "'>"
															+ name
															+ "</option>");
										});
							}
						});
		$('.grp-remove-icon').live('click', function(e) {
			e.preventDefault();
			$('.grp-' + $(this).attr('id')).each(function() {
				$(this).remove();
			});
		});

		$('.rollback-dataset').live('click', function(e) {
			e.preventDefault();
			var answer = confirm("Are you sure?");
			if (answer) {
				var url = $(this).parents("form").attr("action");
				request = $.ajax({
					url : url,
					data : $(this).parents("form").serialize(),
					type : "post",
					success : function() {
						window.location.reload(true);
						return false;
					}
				});

			} else {
				return false;
			}
		});

	},

	initFiltersOrderSortable : function() {
		$('#filters-order-list').sortable({
			update : function(event, ui) {
				$('.ui-state-default').each(function(index, element) {
					$(this).find('.order').val(index);
				});
			}
		});
		$('#filters-order-list').disableSelection();
	},
	initCategoriesOrderSortable : function() {
		$('#resources-order-list').sortable({
			update : function(event, ui) {
				$('.ui-state-default').each(function(index, element) {
					$(this).find('.order').val(index);
				});
			}
		});
		$('#resources-order-list').disableSelection();
	},

	escapeHtml : function(unsafe) {
		unsafe = unsafe.toString();

		return unsafe.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(
				/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#039;");
	},

	updateSortingArrows : function() {

		var urlSplit = Site.splitUrl(location.href);
		if (urlSplit.orderBy != null && urlSplit.orderDir != null) {
			$('.' + urlSplit.orderBy).removeClass('sorting-arrow').removeClass(
					'sorting-arrow-up');
			if (urlSplit.orderDir == "asc") {
				$('.' + urlSplit.orderBy).addClass('sorting-arrow');
			} else {
				$('.' + urlSplit.orderBy).addClass('sorting-arrow-up');
			}
		}
	},

	splitUrl : function(uri) {
		var splitRegExp = new RegExp('^' + '(?:' + '([^:/?#.]+)' + // scheme -
		// ignore
		// special
		// characters
		// used by other URL parts such as :,
		// ?, /, #, and .
		':)?' + '(?://' + '(?:([^/?#]*)@)?' + // userInfo
		'([\\w\\d\\-\\u0100-\\uffff.%]*)' + // domain - restrict to letters,
		// digits, dashes, dots, percent
		// escapes, and unicode characters.
		'(?::([0-9]+))?' + // port
		')?' + '([^?#]+)?' + // path
		'(?:\\?([^#]*))?' + // query
		'(?:#(.*))?' + // fragment
		'$');

		var split;
		split = uri.match(splitRegExp);
		queryData = split[6];
		queryDataOut = new Object();
		var orderBy;
		var orderDir;
		if (queryData != null) {
			queryData = queryData.split('&');
			for (var i = 0; i < queryData.length; i++) {
				queryData[i] = decodeURIComponent(queryData[i]);
				item = queryData[i].split("=");
				if (item[0] == 'orderBy') {
					orderBy = item[1];
				} else if (item[0] == 'orderDir') {
					orderDir = item[1];
				} else {
					queryDataOut[item[0]] = item[1];
				}
			}
		}
		return {
			'scheme' : split[1],
			'user_info' : split[2],
			'domain' : split[3],
			'port' : split[4],
			'path' : split[5],
			'query_data' : queryDataOut,
			'fragment' : split[7],
			'orderBy' : orderBy,
			'orderDir' : orderDir
		};
	}
}

$(document).ready(function() {
	var count = parseInt(($('#input-count').val()), 10);
	$('.entries-per-page').val(count);
	Site.init();
	// listeners for sorting and paging
	$('.entries-per-page').live("change", function() {
		var count = $(this).val();
		var orderBy = $("#input-orderBy").val();
		var orderDir = $("#input-orderDir").val();
		var url = "?offset=" + $("#input-from").val() + "&count=" + count;
		if (orderBy != "") {
			url += "&orderBy=" + orderBy;
		}
		if (orderDir != "") {
			url += "&orderDir=" + orderDir;
		}
		window.location = url;

	});
	if (($.browser.msie) && ($.browser.version <= "9.0")) {

		$('input[placeholder], textarea[placeholder]').placeholder();

		var CSS3PIE_selectors = [ '.user-login', '.sbHolder', '.sort-arrows' ];
		$(CSS3PIE_selectors.join(',')).each(function() {
			PIE.attach(this);
		});

	}
});

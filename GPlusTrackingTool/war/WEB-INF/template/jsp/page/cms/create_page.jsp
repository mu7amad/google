<%@ taglib tagdir="/WEB-INF/tags/cms" prefix="templates"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="widgets"%>

<c:set var="title">
</c:set>
<templates:cms title="${title}" activeTopNavItemId="pagesItem"
	studyId="${studyId }">
	<templates:topNav title="Pages - Pages List"
		topNavFirstItemLink="/cms/page/create" topNavSecondItem="Pages List"
		topNavSelectedItemIndex="0" topNavFirstItem="Add New Pages"
		topNavSecondItemLink="/cms/page"></templates:topNav>

	<widgets:form method="post" action="/cms/page/create"
		classes="validate">
		<fieldset>
			<ul>
				<%--<li><label>Name</label> <input name="name"
					class="required-field required" type="text"></li> --%>
				<li><label>Name</label> <input name="name" class="required"
					type="text"></li>
				<li><label>Title</label> <input name="title" class="required"
					type="text"></li>
				<li><label>Order</label> <input name="order" class="required"
					type="number" min="1"></li>
				<li><label>Key words</label> <input name="keywords" type="text"></li>
				<!-- 				<li class="email-role"> -->
				<!-- 					<label>E-mail</label>  -->
				<!-- 					<input class="required emailval google" name="email" type="text" > -->
				<!-- 				</li> -->
				<li><label>Description</label> <input name="description"
					type="text"></li>
				<li><label>Content</label> <textarea name="content"
						class="required">${resource.content}</textarea></li>
				<!-- 				<li> -->
				<!-- 					<label>Type</label>  -->
				<!-- 					<select name="role"  size="1" class="type-select roles"> -->
				<!-- 							<option value="ROLE_ADMIN">Administrator</option> -->
				<!-- 							<option value="ROLE_RESEARCHER">Researcher</option> -->
				<!-- 					</select> -->
				<!-- 				</li> -->

			</ul>
		</fieldset>
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
		<button class="maia-button" type="submit">Save</button>
		<a href="/cms/page" class="maia-button maia-button-secondary"
			type="reset">Cancel</a>
	</widgets:form>
</templates:cms>



<%@page import="org.apache.taglibs.standard.tag.common.xml.IfTag"%>
<%@page import="com.googlecode.objectify.impl.IfConditionGenerator"%>
<%@page import="com.googlecode.objectify.condition.If"%>
<%@ taglib tagdir="/WEB-INF/tags/cms" prefix="templates"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="widgets"%>

<c:set var="title">
Google+ Brand Tracker - Users List
</c:set>
<templates:cms title="${title }" activeTopNavItemId="usersItem"
	studyId="${studyId }">
	<templates:topNav title="Users - Users List"
		topNavFirstItemLink="/cms/user/create" topNavSecondItem="Users List"
		topNavSelectedItemIndex="1" topNavFirstItem="Add New User"
		topNavSecondItemLink="/cms/user"></templates:topNav>
	<div class="sort">
		<div class="sort-numbers">
			<b>${from + 1}</b> - <b>${to}</b> of <b>${total}</b>
		</div>
		<a class="sort-arrows arrow-left"
			href="<c:if test="${from > 0}">?offset=${from-count}&count=${count}&orderBy=${orderBy}&orderDir=${orderDir}</c:if><c:if test="${from <= 0}"></c:if>"
			id="questions-table-previous"><span></span></a> <a
			class="sort-arrows arrow-right"
			href="<c:if test="${to < total}">?offset=${to}&count=${count}&orderBy=${orderBy}&orderDir=${orderDir}</c:if><c:if test="${to == total}"></c:if>"
			id="questions-table-next"><span></span></a>
	</div>
	<table>
		<tr>
			<th scope="col">E-mail <a
				href="?orderBy=email&orderDir=<c:if test="${orderDir!='asc' || orderBy!='email'}">asc</c:if><c:if test="${orderDir=='asc'&& orderBy=='email'}">desc</c:if>&offset=${from}&count=${count}"
				class="sorting-arrow email">arrow</a></th>
			<th scope="col">First Logged In <a
				href="?orderBy=firstLoggedIn&orderDir=<c:if test="${orderDir!='asc' || orderBy!='firstLoggedIn'}">asc</c:if><c:if test="${orderDir=='asc' && orderBy=='firstLoggedIn'}">desc</c:if>&offset=${from}&count=${count}"
				class="sorting-arrow firstLoggedIn">arrow</a></th>
			<th scope="col" class="tiny-raw">Edit</th>
			<th scope="col" class="tiny-raw">Delete</th>
		</tr>
		<c:forEach items="${resources }" var="resource">
			<tr>
				<td>${resource.email }</td>
				<td><fmt:formatDate value="${resource.firstLoggedIn}"
						pattern="dd-MM-yyyy" /></td>
				<td><a href="/cms/user/edit/${resource.id }" class="show-icon">Edit</a></td>
				<td><widgets:form action="/cms/user/delete/${resource.id}"
						method="post">
						<button type="submit" class="unstyled-button delete-icon confirm"></button>
					</widgets:form></td>
			</tr>
		</c:forEach>
	</table>
	<div class="sort">
		<div class="show-items">
			Show items per page
			<form>
				<select class="type-select entries-per-page">
					<option value="20">20</option>
					<option value="15">15</option>
					<option value="10">10</option>
					<option value="5">5</option>
				</select>
			</form>
		</div>

		<div class="sort-numbers">
			<b>${from + 1}</b> - <b>${to}</b> of <b>${total}</b>
		</div>
		<a class="sort-arrows arrow-left"
			href="<c:if test="${from > 0}">?offset=${from-count}&count=${count}&orderBy=${orderBy}&orderDir=${orderDir}</c:if><c:if test="${from <= 0}"></c:if>"
			id="questions-table-previous"><span></span></a> <a
			class="sort-arrows arrow-right"
			href="<c:if test="${to < total}">?offset=${to}&count=${count}&orderBy=${orderBy}&orderDir=${orderDir}</c:if><c:if test="${to == total}"></c:if>"
			id="questions-table-next"><span></span></a> <input type="hidden"
			id="input-from" value="${from}"></input> <input type="hidden"
			id="input-to" value="${to}"></input> <input type="hidden"
			id="input-total" value="${total}"></input> <input type="hidden"
			id="input-orderDir" value="${orderDir}"></input> <input type="hidden"
			id="input-orderBy" value="${orderBy}"></input> <input type="hidden"
			id="input-count" value="${count}"></input>
	</div>
	<%-- 	<a href="<c:if test="${from >= 2}">?offset=${from-2}&count=2&orderBy=${orderBy}&orderDir=${orderDir}</c:if><c:if test="${from < 2}">#</c:if>" id="questions-table-previous">Previous</a> --%>
	<%-- 				<a href="<c:if test="${to < total}">?offset=${from-2}&count=2&orderBy=${orderBy}&orderDir=${orderDir}</c:if><c:if test="${to < total}">#</c:if>" id="questions-table-next">Next</a> --%>
	<%-- 				<p><c:if test="${from == 0 && to != 0}">1</c:if><c:if test="${from > 0 || to == 0}">${from}</c:if>-${to} of ${total}</p> --%>
</templates:cms>
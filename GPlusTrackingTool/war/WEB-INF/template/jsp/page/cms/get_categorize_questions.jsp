<%@ taglib tagdir="/WEB-INF/tags/cms" prefix="templates"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="widgets"%>

<c:set var="title">
Google+ Brand Tracker - Questions List
</c:set>
<templates:cms title="${title }" activeTopNavItemId="dataItem"
	studyId="${studyId }">
	<templates:data topNavSecondItem="Categorize Questions List"
		topNavSelectedItemIndex="1"
		sideNavActiveItemId="categorizeQuestionsItem"
		title="Questions - Categorize Questions List" showTopNav="false">

		<article class="maia-article" role="article">
			<!-- 			<div class="sort"> -->
			<!-- 				<div class="sort-numbers"> -->
			<%-- 					<b>${from + 1}</b> - <b>${to}</b> of <b>${total}</b> --%>
			<!-- 				</div> -->
			<!-- 				<a class="sort-arrows arrow-left" -->
			<%-- 					href="<c:if test="${from > 0}">?offset=${from-count}&count=${count}</c:if><c:if test="${from <= 0}"></c:if>" --%>
			<!-- 					id="questions-table-previous"><span></span></a> <a -->
			<!-- 					class="sort-arrows arrow-right" -->
			<%-- 					href="<c:if test="${to < total}">?offset=${to}&count=${count}</c:if><c:if test="${to == total}"></c:if>" --%>
			<!-- 					id="questions-table-next"><span></span></a> -->
			<!-- 			</div> -->
			<widgets:form action="/cms/data/question/categorize" method="post">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
				<table>
					<tr>
						<th scope="col">Questions</th>
						<th scope="col" class="tiny-raw">Category</th>
					</tr>
					<c:forEach items="${resources}" var="resource">
						<tr>
							<td><c:out value="${resource.name}"></c:out></td>
							<td><select name="${resource.id}">

									<option value="-1"
										<c:if test="${empty resource.category.id}"> selected="true"
										</c:if>>Default</option>
									<c:forEach items="${categories}" var="category">

										<option value="${category.id}"
											<c:if test="${category.id eq resource.category.id}"> selected="true"
										</c:if>>
											<c:out value="${category.name}"></c:out>

										</option>

									</c:forEach>
							</select></td>
						</tr>
					</c:forEach>
				</table>
				<button class="maia-button" type="submit">Submit</button>
			</widgets:form>
			<!-- 			<div class="sort"> -->
			<!-- 				<div class="show-items"> -->
			<!-- 					Show items per page -->
			<!-- 					<form> -->
			<!-- 						<select class="type-select entries-per-page"> -->
			<!-- 							<option value="5">5</option> -->
			<!-- 							<option value="4">4</option> -->
			<!-- 							<option value="3">3</option> -->
			<!-- 							<option value="2">2</option> -->
			<!-- 							<option value="1">1</option> -->
			<!-- 						</select> -->
			<!-- 					</form> -->
			<!-- 				</div> -->

			<!-- 				<div class="sort-numbers"> -->
			<%-- 					<b>${from + 1}</b> - <b>${to}</b> of <b>${total}</b> --%>
			<!-- 				</div> -->
			<!-- 				<a class="sort-arrows arrow-left" -->
			<%-- 					href="<c:if test="${from > 0}">?offset=${from-count}&count=${count}</c:if><c:if test="${from <= 0}"></c:if>" --%>
			<!-- 					id="questions-table-previous"><span></span></a> <a -->
			<!-- 					class="sort-arrows arrow-right" -->
			<%-- 					href="<c:if test="${to < total}">?offset=${to}&count=${count}</c:if><c:if test="${to == total}"></c:if>" --%>
			<!-- 					id="questions-table-next"><span></span></a> <input type="hidden" -->
			<%-- 					id="input-from" value="${from}"></input> <input type="hidden" --%>
			<%-- 					id="input-to" value="${to}"></input> <input type="hidden" --%>
			<%-- 					id="input-total" value="${total}"></input> <input type="hidden" --%>
			<%-- 					id="input-orderDir" value="${orderDir}"></input> <input --%>
			<%-- 					type="hidden" id="input-orderBy" value="${orderBy}"></input> <input --%>
			<%-- 					type="hidden" id="input-count" value="${count}"></input> --%>
			<!-- 			</div> -->
			<%-- 			<a href="<c:if test="${from >= 2}">?offset=${from-2}&count=2</c:if><c:if test="${from < 2}">#</c:if>" id="questions-table-previous">Previous</a> --%>
			<%-- 				<a href="<c:if test="${to < total}">?offset=${to}&count=2</c:if><c:if test="${to < total}">#</c:if>" id="questions-table-next">Next</a> --%>
			<%-- 				<p><c:if test="${from == 0 && to != 0}">1</c:if><c:if test="${from > 0 || to == 0}">${from}</c:if>-${to} of ${total}</p> --%>
		</article>
	</templates:data>
</templates:cms>



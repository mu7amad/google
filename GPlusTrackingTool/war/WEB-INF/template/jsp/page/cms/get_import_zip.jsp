<%@ taglib tagdir="/WEB-INF/tags/cms" prefix="templates"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="widgets"%>

<c:set var="title">
Google+ Brand Tracker - Import
</c:set>
<c:set var="footer">

</c:set>
<c:set var="head">

</c:set>
<templates:cms title="${title}" head="${head}" footer="${footer }"
	activeTopNavItemId="importItem" studyId="${studyId }">

	<div class="sort">
		<div class="sort-numbers">
			<b>${from + 1}</b> - <b>${to}</b> of <b>${total}</b>
		</div>
		<a class="sort-arrows arrow-left"
			href="<c:if test="${from > 0}">?offset=${from-count}&count=${count}&orderBy=${orderBy}&orderDir=${orderDir}</c:if><c:if test="${from <= 0}"></c:if>"
			id="questions-table-previous"><span></span></a> <a
			class="sort-arrows arrow-right"
			href="<c:if test="${to < total}">?offset=${to}&count=${count}&orderBy=${orderBy}&orderDir=${orderDir}</c:if><c:if test="${to == total}"></c:if>"
			id="questions-table-next"><span></span></a>
	</div>
	<table class="table-upload">
		<tr>
			<th scope="col" id="test">Date <a
				href="?orderBy=uploadDate&orderDir=<c:if test="${orderDir!='asc' || orderBy!='uploadDate'}">asc</c:if><c:if test="${orderDir=='asc' && orderBy=='uploadDate'}">desc</c:if>&offset=${from}&count=${count}"
				class="sorting-arrow uploadDate">arrow</a></th>
			<th scope="col">File Name<a
				href="?orderBy=name&orderDir=<c:if test="${orderDir!='asc' || orderBy!='name'}">asc</c:if><c:if test="${orderDir=='asc' && orderBy=='name'}">desc</c:if>&offset=${from}&count=${count}"
				class="sorting-arrow name">arrow</a></th>
			<th scope="col">Actions</th>
			<!-- 				<th scope="col">Format</th> -->
			<!-- 			<th scope="col">Size<a -->
			<%-- 				href="?orderBy=size&orderDir=<c:if test="${orderDir!='asc' || orderBy!='size'}">asc</c:if><c:if test="${orderDir=='asc' && orderBy=='size'}">desc</c:if>&offset=${from}&count=${count}" --%>
			<!-- 				class="sorting-arrow">arrow</a></th> -->
			<!-- 			<th scope="col">Status<a -->
			<%-- 				href="?orderBy=status&orderDir=<c:if test="${orderDir!='asc' || orderBy!='status'}">asc</c:if><c:if test="${orderDir=='asc' && orderBy=='status'}">desc</c:if>&offset=${from}&count=${count}" --%>
			<!-- 				class="sorting-arrow">arrow</a></th> -->
			<!-- 			<th scope="col" class="tiny-raw">Actions</th> -->
		</tr>
		<c:forEach items="${resources }" var="resource" varStatus="rindex">
			<tr>
				<td><fmt:formatDate value="${resource.uploadDate}"
						pattern="dd/MM/yyyy" />
				<td><c:out value="${resource.name}"></c:out></td>
				<td><c:if test="${status[rindex.index] == 'PU'}">
						<widgets:form action="/cms/process/entry/zip" method="post">
							<input type="hidden" name="zk" value="${resource.id}">
							<button type="submit" class="maia-btn">Process</button>
						</widgets:form>
						<widgets:form>
							<input type="hidden" name="zk" value="${resource.id}">
							<button type="submit" class="maia-btn">Rollback</button>
						</widgets:form>
					</c:if> <c:if test="${status[rindex.index] == 'U'}">
						<widgets:form action="/cms/process/entry/zip" method="post">
							<input type="hidden" name="zk" value="${resource.id}">
							<button type="submit" class="maia-btn" method="post">Process</button>
						</widgets:form>
					</c:if> <c:if test="${status[rindex.index] == 'P'}">
						<widgets:form>
							<input type="hidden" name="zk" value="${resource.id}">
							<button type="submit" class="maia-btn">Rollback</button>
						</widgets:form>
					</c:if></td>
			</tr>
			<tr>
				<td colspan="3"><c:forEach items="${dataSheets[rindex.index]}"
						var="dataSheet" varStatus="index">
						<%-- 					<c:if test="${index.index  == 0}"> --%>

						<div>
							<c:out value="${dataSheet.name }"></c:out>
						</div>



					</c:forEach></td>
			</tr>

		</c:forEach>

	</table>
	<div class="sort">
		<div class="show-items">
			Show items per page
			<form>
				<select class="type-select entries-per-page">
					<option value="20">20</option>
					<option value="15">15</option>
					<option value="10">10</option>
					<option value="5">5</option>
				</select>
			</form>
		</div>

		<div class="sort-numbers">
			<b>${from + 1}</b> - <b>${to}</b> of <b>${total}</b>
		</div>
		<a class="sort-arrows arrow-left"
			href="<c:if test="${from > 0}">?offset=${from-count}&count=${count}&orderBy=${orderBy}&orderDir=${orderDir}</c:if><c:if test="${from <= 0}"></c:if>"
			id="questions-table-previous"><span></span></a> <a
			class="sort-arrows arrow-right"
			href="<c:if test="${to < total}">?offset=${to}&count=${count}&orderBy=${orderBy}&orderDir=${orderDir}</c:if><c:if test="${to == total}"></c:if>"
			id="questions-table-next"><span></span></a>

	</div>
	<%-- 				<p><c:if test="${from == 0 && to != 0}">1</c:if><c:if test="${from > 0 || to == 0}">${from}</c:if>-${to} of ${total}</p> --%>
	<input type="hidden" id="input-from" value="${from}"></input>
	<input type="hidden" id="input-to" value="${to}"></input>
	<input type="hidden" id="input-total" value="${total}"></input>
	<input type="hidden" id="input-orderDir" value="${orderDir}"></input>
	<input type="hidden" id="input-orderBy" value="${orderBy}"></input>
	<input type="hidden" id="input-count" value="${count}"></input>
	</li>
	</ul>
	</article>
	</section>

</templates:cms>



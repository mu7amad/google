<%@ taglib tagdir="/WEB-INF/tags/cms" prefix="templates"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="widgets"%>

<c:set var="title">
</c:set>
<templates:cms title="${title }" activeTopNavItemId="dataItem"
	studyId="${studyId }">
	<templates:data topNavSecondItem="Categories List"
		topNavSecondItemLink="/cms/data/category"
		topNavFirstItemLink="/cms/data/category/create"
		topNavFirstItem="Add New Category" topNavSelectedItemIndex="2"
		topNavThirdItem="Order Categories"
		topNavThirdItemLink="/cms/data/category/order"
		sideNavActiveItemId="categoriesItem"
		title="Categories - Categories List" showTopNav="true">


		<article class="maia-article" role="article">
			<widgets:form action="/cms/data/category/order" method="post">
				<ul id="resources-order-list">
					<c:forEach items="${resources }" var="resource" varStatus="iter">
						<li class="ui-state-default"><span><c:out
									value="${resource.name}"></c:out> </span> <input id="${resource.id}"
							type="hidden" class="order" name="${resource.id }"
							value="${iter.index}" /></li>
					</c:forEach>
				</ul>
				<button class="maia-button" type="submit">Save</button>
			</widgets:form>
		</article>
	</templates:data>
</templates:cms>



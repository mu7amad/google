<%@ taglib tagdir="/WEB-INF/tags/cms" prefix="templates"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="widgets"%>

<c:set var="title">
</c:set>
<templates:cms title="${title }" activeTopNavItemId="usersItem"
	studyId="${studyId }">
	<templates:topNav title="Users - Edit User"
		topNavFirstItemLink="/cms/user/create" topNavSecondItem="Users List"
		topNavSelectedItemIndex="0" topNavFirstItem="Add New User"
		topNavSecondItemLink="/cms/user"></templates:topNav>

	<widgets:form method="post" action="/cms/user/edit/${resource.id }"
		classes="validate">
		<fieldset>
			<ul>
				<li><label>First Name</label> <input name="firstName"
					type="text" value='<c:out value="${resource.firstName }"></c:out>'></li>
				<li><label>Last Name</label> <input name="lastName" type="text"
					value='<c:out value="${resource.lastName }"></c:out>'></li>

				<li class="email-role"><label>E-mail</label> <input
					class="required emailval ${roleValidationFn}" name="email"
					type="text" value='<c:out value="${resource.email}"></c:out>'>
				</li>
				<li><label>Company</label> <input name="company" type="text"
					value='<c:out value="${resource.company }"></c:out>'></li>

			</ul>
		</fieldset>
		<button class="maia-button" type="submit">Save</button>
		<a href="/cms/user" class="maia-button maia-button-secondary"
			type="reset">Cancel</a>
	</widgets:form>
</templates:cms>



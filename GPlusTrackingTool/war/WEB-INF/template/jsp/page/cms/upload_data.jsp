<%@ taglib tagdir="/WEB-INF/tags" prefix="widgets"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<%@ page
	import="com.google.appengine.api.blobstore.BlobstoreServiceFactory"%>
<%@ page import="com.google.appengine.api.blobstore.BlobstoreService"%>

<%
	BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
%>


<html>
<head>
<style>
table, th, td {
	border: 1px solid black;
	border-collapse: collapse;
}

th, td {
	padding: 5px;
	text-align: left;
}
</style>
<script type="text/javascript">
	// 	var _validFileExtensions = [ ".jpg", "jpeg", ".bmp", ".gif", ".png" ];
	var _validFileExtensions = [ ".zip" ];
	function Validate(oForm) {
		var arrInputs = oForm.getElementsByTagName("input");
		for (var i = 0; i < arrInputs.length; i++) {
			var oInput = arrInputs[i];
			if (oInput.type == "file") {
				var sFileName = oInput.value;
				if (sFileName.length > 0) {
					var blnValid = false;
					for (var j = 0; j < _validFileExtensions.length; j++) {
						var sCurExtension = _validFileExtensions[j];
						if (sFileName.substr(
								sFileName.length - sCurExtension.length,
								sCurExtension.length).toLowerCase() == sCurExtension
								.toLowerCase()) {
							blnValid = true;
							break;
						}
					}

					if (!blnValid) {
						alert("Sorry, " + sFileName
								+ " is invalid, allowed extensions are: "
								+ _validFileExtensions.join(", "));
						return false;
					}
				}
			}
		}

		return true;
	}

	function change() // no ';' here
	{
		var elem = document.getElementById("process_button");
		if (elem.value == "Process") {
			elem.value = "Processing..";
// 			elem.disabled = true;
		} else
			elem.value = "Process";
	}
</script>
<title>Upload Test</title>
</head>
<body>
	<form
		action="<%=blobstoreService
					.createUploadUrl("/dataUploadController/upload")%>"
		method="post" enctype="multipart/form-data"
		onsubmit="return Validate(this);">
		<input type="file" name="myFile" accept=".zip" required> <input
			id="upload_button" type="submit" value="Upload">
	</form>

	<a href="/cms">Back</a>

	<table style="width: 100%">
		<tr>
			<th>Date</th>
			<th>File Name</th>
			<th>UploadedBy</th>
			<th>Size</th>
			<th>Status</th>
			<th>Actions</th>
		</tr>
		<c:forEach items="${resources}" var="resource">
			<tr>
				<td>${resource.uploadDate}</td>
				<td>${resource.name }</td>
				<td>${resource.uploadedBy }</td>
				<td>${resource.size }</td>
				<td>${resource.status }</td>
				<c:if test="${resource.status eq 'UPLOADED'}">
					<c:if test="${resource.owner }">
						<td><form action="/dataUploadController/extractDataSheet"
								method="post">
								<input type="hidden" value="${resource.id}" name="id" /> <input
									type="hidden" name="${_csrf.parameterName}"
									value="${_csrf.token}" /> <input type="submit" value="Process"
									disabled>
							</form></td>
					</c:if>
					<c:if test="${!resource.owner }">
						<td><form action="/dataUploadController/extractDataSheet"
								method="post">
								<input type="hidden" value="${resource.id}" name="id" /> <input
									type="hidden" name="${_csrf.parameterName}"
									value="${_csrf.token}" /> <input id="process_button"
									type="submit" value="Process" onclick="change()">
							</form></td>
					</c:if>
				</c:if>
				<%-- 				<c:if test="${resource.status eq 'PROCESSED'}"> --%>
				<%-- 					<td><widgets:form method="post" --%>
				<%-- 							action="/cms/process/entry/rollbackdatasheet/${resource.id}"> --%>
				<%-- 							<input type="hidden" name="${_csrf.parameterName}" --%>
				<%-- 								value="${_csrf.token}" /> --%>

				<!-- 							<button type="submit" class="maia-button rollback-datasheet"> -->
				<!-- 								Rollback</button> -->
				<%-- 						</widgets:form></td> --%>

				<%-- 				</c:if> --%>
				<c:if test="${resource.status eq 'PROCESSING'}">
					<td><a href="" class="maia-button">Processing</a></td>
				</c:if>
				<c:if test="${resource.status eq 'ROLLINGBACK'}">
					<td><a href="" class="maia-button">Rollingback</a></td>
				</c:if>
			</tr>
		</c:forEach>
	</table>

</body>
</html>
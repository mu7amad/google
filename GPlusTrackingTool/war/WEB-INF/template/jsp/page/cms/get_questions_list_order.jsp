<%@ taglib tagdir="/WEB-INF/tags/cms" prefix="templates"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="widgets"%>

<c:set var="title">
</c:set>
<templates:cms title="${title }" activeTopNavItemId="dataItem"
	studyId="${studyId }">
	<templates:data topNavFirstItem="Question List"
		topNavFirstItemLink="/cms/data/question" topNavSelectedItemIndex="1"
		topNavSecondItem="Questions List Order"
		topNavSecondItemLink="/cms/data/question/order_list"
		sideNavActiveItemId="questionsItem"
		title="Questions - Questions List Order" showTopNav="true">


		<article class="maia-article" role="article">
			<widgets:form action="/cms/data/question/list_order" method="post">
				<ul id="resources-order-list">
					<c:forEach items="${resources }" var="resource" varStatus="iter">
						<li class="ui-state-default"><span><c:out
									value="${resource.name}"></c:out> </span> <input id="${resource.id}"
							type="hidden" class="order" name="rom[${resource.id }]"
							value="${iter.index}" /></li>
					</c:forEach>
				</ul>
				<button class="maia-button" type="submit">Save</button>
			</widgets:form>
		</article>
	</templates:data>
</templates:cms>



<%@ taglib tagdir="/WEB-INF/tags/cms" prefix="templates"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="widgets"%>

<c:set var="title">
Google+ Brand Tracker - Admins List
</c:set>
<templates:cms title="${title }" activeTopNavItemId="adminsItem"
	studyId="${studyId }">
	<templates:topNav title="Admins - Admins List"
		topNavFirstItemLink="/cms/admin/create" topNavSecondItem="Admins List"
		topNavSelectedItemIndex="1" topNavFirstItem="Add New Admin"
		topNavSecondItemLink="/cms/admin"></templates:topNav>
	<div class="sort">
		<div class="sort-numbers">
			<b>${from + 1}</b> - <b>${to}</b> of <b>${total}</b>
		</div>
		<a class="sort-arrows arrow-left"
			href="<c:if test="${from > 0}">?offset=${from-count}&count=${count}&orderBy=${orderBy}&orderDir=${orderDir}</c:if><c:if test="${from <= 0}"></c:if>"
			id="questions-table-previous"><span></span></a> <a
			class="sort-arrows arrow-right"
			href="<c:if test="${to < total}">?offset=${to}&count=${count}&orderBy=${orderBy}&orderDir=${orderDir}</c:if><c:if test="${to == total}"></c:if>"
			id="questions-table-next"><span></span></a>
	</div>
	<table>
		<tr>
			<th scope="col">First Name <a
				href="?orderBy=firstName&orderDir=<c:if test="${orderDir!='asc' || orderBy !='firstName'}">asc</c:if><c:if test="${orderDir=='asc'&& orderBy =='firstName'}">desc</c:if>&offset=${from}&count=${count}"
				class="sorting-arrow firstName">arrow</a></th>
			<th scope="col">Last Name <a
				href="?orderBy=lastName&orderDir=<c:if test="${orderDir!='asc' || orderBy !='lastName'}">asc</c:if><c:if test="${orderDir=='asc' && orderBy =='lastName'}">desc</c:if>&offset=${from}&count=${count}"
				class="sorting-arrow lastName">arrow</a></th>
			<th scope="col">E-mail <a
				href="?orderBy=email&orderDir=<c:if test="${orderDir!='asc' || orderBy !='email'}">asc</c:if><c:if test="${orderDir=='asc' && orderBy =='email'}">desc</c:if>&offset=${from}&count=${count}"
				class="sorting-arrow email">arrow</a></th>
			<th scope="col">Company <a
				href="?orderBy=company&orderDir=<c:if test="${orderDir!='asc' || orderBy !='company'}">asc</c:if><c:if test="${orderDir=='asc' && orderBy =='company'}">desc</c:if>&offset=${from}&count=${count}"
				class="sorting-arrow company">arrow</a></th>
			<th scope="col">Type<a
				href="?orderBy=type&orderDir=<c:if test="${orderDir!='asc' || orderBy !='type'}">asc</c:if><c:if test="${orderDir=='asc' && orderBy =='type'}">desc</c:if>&offset=${from}&count=${count}"
				class="sorting-arrow type">arrow</a></th>
			<th scope="col" class="tiny-raw">Edit</th>
			<th scope="col" class="tiny-raw">Deactivate</th>
		</tr>

		<c:forEach items="${resources.list}" var="resource">
			<c:if test="${fn:length(resource.authorities)< 3}">
				<tr>
					<td><c:out value="${resource.firstName}"></c:out></td>
					<td><c:out value="${resource.lastName}"></c:out></td>
					<td><c:out value="${resource.email}"></c:out></td>
					<td><c:out value="${resource.company}"></c:out></td>
					<td><c:if
							test="${fn:contains(resource.authorities,'ROLE_ADMIN')}">Administrator</c:if>
						<c:if
							test="${fn:contains(resource.authorities,'ROLE_RESEARCHER')}">Researcher</c:if></td>
					<td><a href="/cms/admin/edit/${resource.id }"
						class="show-icon">Edit</a></td>
					<td><widgets:form
							action="/cms/admin/deactivate/${resource.id}" method="post">
							<button type="submit"
								class="unstyled-button deactivate-icon confirm">Deactivate</button>
						</widgets:form></td>
				</tr>
			</c:if>
		</c:forEach>
	</table>
	<div class="sort">
		<div class="show-items">
			Show items per page
			<form>
				<select class="type-select entries-per-page">
					<option value="20">20</option>
					<option value="15">15</option>
					<option value="10">10</option>
					<option value="5">5</option>
				</select>
			</form>
		</div>

		<div class="sort-numbers">
			<b>${from + 1}</b> - <b>${to}</b> of <b>${total}</b>
		</div>
		<a class="sort-arrows arrow-left"
			href="<c:if test="${from > 0}">?offset=${from-count}&count=${count}&orderBy=${orderBy}&orderDir=${orderDir}</c:if><c:if test="${from <= 0}"></c:if>"
			id="questions-table-previous"><span></span></a> <a
			class="sort-arrows arrow-right"
			href="<c:if test="${to < total}">?offset=${to}&count=${count}&orderBy=${orderBy}&orderDir=${orderDir}</c:if><c:if test="${to == total}"></c:if>"
			id="questions-table-next"><span></span></a> <input type="hidden"
			id="input-from" value="${from}"></input> <input type="hidden"
			id="input-to" value="${to}"></input> <input type="hidden"
			id="input-total" value="${total}"></input> <input type="hidden"
			id="input-orderDir" value="${orderDir}"></input> <input type="hidden"
			id="input-orderBy" value="${orderBy}"></input> <input type="hidden"
			id="input-count" value="${count}"></input>
	</div>
	<%-- 	<a href="<c:if test="${from >= 2}">?offset=${from-2}&count=2&orderBy=${orderBy}&orderDir=${orderDir}</c:if><c:if test="${from < 2}">#</c:if>" id="questions-table-previous">Previous</a> --%>
	<%-- 				<a href="<c:if test="${to < total}">?offset=${from-2}&count=2&orderBy=${orderBy}&orderDir=${orderDir}</c:if><c:if test="${to < total}">#</c:if>" id="questions-table-next">Next</a> --%>
	<%-- 				<p><c:if test="${from == 0 && to != 0}">1</c:if><c:if test="${from > 0 || to == 0}">${from}</c:if>-${to} of ${total}</p> --%>
</templates:cms>



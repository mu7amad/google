<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="widgets"%>

<%@ page
	import="com.google.appengine.api.blobstore.BlobstoreServiceFactory"%>
<%@ page import="com.google.appengine.api.blobstore.BlobstoreService"%>

<%
	BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- <meta http-equiv="refresh" 	content="100;URL=/studyController/loadStudies" /> -->

<title>Studies</title>

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>


<script type="text/javascript">
	$(function() {
		$("input[type='submit']").click(function() {
			var $fileUpload = $("input[type='file']");
			if (parseInt($fileUpload.get(0).files.length) > 2) {
				alert("You can only upload a maximum of 2 files");
			}
		});
	});
	$('#find-study-by-name').keydown(function(e) {
		if (e.keyCode == 13) {
			$('#form').submit();
		}
	});
</script>

<style>
table, th, td {
	border: 1px solid black;
	border-collapse: collapse;
}

th, td {
	padding: 5px;
	text-align: left;
}
</style>
</head>

<body>
	<p align="right">${user }&nbsp;&nbsp;&nbsp;
		<a href="/logout" class="maia-button">Logout</a>
	</p>


	<a href="/studyController/linkToCreateNewStudy">Create New Study</a>

	<br>

	<div>
		<form id="find-study-by-name"
			action="/studyController/autocompleteStudySearch" method="post">
			<input id="studyName" name="studyName">
		</form>
	</div>

	<div align="center">
		<table id="studies_table" style="width: 100%">
			<tr>
				<th>Image</th>
				<th>Study Name</th>
				<th>Creation Date</th>
				<th>Action</th>
				<th>Edit</th>
				<th>Delete</th>
				<th>Share</th>
			</tr>

			<c:if test="${empty studies }">
				<tr>
					<span style="background-color: yellow;"><c:out
							value="There Is No Studies"></c:out></span>
				</tr>
			</c:if>
			<c:if test="${not empty studies }">
				<c:forEach items="${studies}" var="study">

					<tr>
						<td><c:if test="${not empty study.iconUrl }">
								<img src="<c:url value="${study.iconUrl }"/>"
									style="width: 50px; height: 50px;">

							</c:if> <c:if test="${empty study.iconUrl }">
								<c:if test="${study.owner }">
									<form method="post"
										action="<%=blobstoreService.createUploadUrl("/studyController/uploadStudyImages")%>"
										enctype="multipart/form-data">
										<input type="hidden" name="studyId" value="${study.studyID }">
										<input id="images" type="file" name="images[]" value="upload"
											multiple required onchange="this.form.submit();">
									</form>
								</c:if>
							</c:if></td>
						<!-- 						<td> -->
						<!-- 						<form action="/studyController/dashBoard"> -->
						<td>
							<%-- 						<form action="/site/studyDashboard/${study.studyID }"> --%>
							<%-- 								<input type="hidden" name="studyId" value="${study.studyID }"> --%>
							<%-- 								<input type="submit" value="${study.studyName}" --%>
							<!-- 									style="background: none !important; border: none; padding: 0 !important; font: inherit; cursor: pointer; color: blue;"> -->

							<!-- 							</form> --> <a
							href="/site/studyDashboard/${study.studyID }">${study.studyName}</a>
						</td>
						<td>${study.creationDate }</td>
						<td><c:if test="${!study.owner }">
								<c:if test="${not study.published }">
									<form method="get" action="/studyController/publishStudy">
										<input type="hidden" value="${study.id}" name="id" /> <input
											type="hidden" name="${_csrf.parameterName}"
											value="${_csrf.token}" /> <input type="submit"
											value="Publish">
									</form>


								</c:if>
								<c:if test="${study.published }">
									<form method="get" action="/studyController/unPublishStudy">
										<input type="hidden" value="${study.id}" name="id" /> <input
											type="hidden" name="${_csrf.parameterName}"
											value="${_csrf.token}" /> <input type="submit"
											value="UnPublish">
									</form>
								</c:if>
							</c:if> <c:if test="${study.owner }">
								<c:if test="${not study.published }">
									<form method="get" action="/studyController/publishStudy">
										<input type="hidden" value="${study.id}" name="id" /> <input
											type="hidden" name="${_csrf.parameterName}"
											value="${_csrf.token}" /> <input type="submit"
											value="Publish" disabled>
									</form>
								</c:if>

								<c:if test="${study.published }">
									<form method="get" action="/studyController/unPublishStudy">
										<input type="hidden" value="${study.id}" name="id" /> <input
											type="hidden" name="${_csrf.parameterName}"
											value="${_csrf.token}" /> <input type="submit"
											value="UnPublish" disabled>
									</form>
								</c:if>
							</c:if></td>
						<td><c:if test="${study.owner }">
								<form action="/studyController/editStudy">
									<input type="hidden" value="${study.id}" name="id" /> <input
										type="hidden" name="${_csrf.parameterName}"
										value="${_csrf.token}" /> <input type="submit" value="Edit">
								</form>
							</c:if> <c:if test="${not study.owner }">
								<form action="/studyController/editStudy">
									<input type="hidden" value="${study.id}" name="id" /> <input
										type="hidden" name="${_csrf.parameterName}"
										value="${_csrf.token}" /> <input type="submit" value="Edit"
										disabled="disabled">
								</form>
							</c:if></td>
						<td><c:if test="${study.owner }">
								<form action="/studyController/delete"
									onsubmit="return confirm('Do you really want to Delete the Study?');">
									<input type="hidden" value="${study.id}" name="id" /> <input
										type="submit" value="Delete">
								</form>
							</c:if> <c:if test="${not study.owner }">
								<form action="/studyController/delete"
									onsubmit="return confirm('Do you really want to Delete the Study?');">
									<input type="hidden" value="${study.id}" name="id" /> <input
										type="submit" value="Delete" disabled="disabled">
								</form>
							</c:if></td>

						<td><c:if test="${study.owner }">
								<form action="/studyController/linkToShare">
									<input type="hidden" value="${study.id}" name="id" /> <input
										type="submit" value="Share">
								</form>

							</c:if> <c:if test="${not study.owner }">
								<form action="/studyController/linkToShare">
									<input type="hidden" value="${study.id}" name="id" /> <input
										type="submit" value="Share" disabled="disabled">
								</form>
							</c:if></td>
					</tr>
				</c:forEach>
			</c:if>
		</table>
	</div>

	<div>
		<a href="/downloadController/download/Brazil.csv.zip">Download</a>
	</div>

</body>
</html>
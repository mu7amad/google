<%@ taglib tagdir="/WEB-INF/tags/cms" prefix="templates"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:set var="title">
Google+ Brand Tracker - Questions List
</c:set>
<templates:cms title="${title }" activeTopNavItemId="dataItem"
	studyId="${studyId }">
	<templates:data topNavFirstItem="Questions List"
		topNavFirstItemLink="/cms/data/question" topNavSelectedItemIndex="0"
		sideNavActiveItemId="questionsItem"
		topNavSecondItem="Questions List Order"
		topNavSecondItemLink="/cms/data/question/list_order"
		title="Questions - Questions List" showTopNav="true">

		<article class="maia-article" role="article">
			<div class="sort">
				<div class="sort-numbers">
					<b>${from + 1}</b> - <b>${to}</b> of <b>${total}</b>
				</div>
				<a class="sort-arrows arrow-left"
					href="<c:if test="${from > 0}">?offset=${from-count}&count=${count}</c:if><c:if test="${from <= 0}"></c:if>"
					id="questions-table-previous"><span></span></a> <a
					class="sort-arrows arrow-right"
					href="<c:if test="${to < total}">?offset=${to}&count=${count}</c:if><c:if test="${to == total}"></c:if>"
					id="questions-table-next"><span></span></a>
			</div>
			<table>
				<tr>
					<th scope="col">Questions</th>
					<th scope="col" class="tiny-raw">Edit</th>
				</tr>
				<c:forEach items="${resources}" var="resource">
					<tr>
						<td><c:out value="${resource.name}"></c:out></td>
						<td><a href="/cms/data/edit/question/${resource.id}"
							class="show-icon">Show</a></td>
					</tr>
				</c:forEach>
			</table>
			<div class="sort">
				<div class="show-items">
					Show items per page
					<form>
						<select class="type-select entries-per-page">
							<option value="20">20</option>
							<option value="15">15</option>
							<option value="10">10</option>
							<option value="5">5</option>
						</select>
					</form>
				</div>

				<div class="sort-numbers">
					<b>${from + 1}</b> - <b>${to}</b> of <b>${total}</b>
				</div>
				<a class="sort-arrows arrow-left"
					href="<c:if test="${from > 0}">?offset=${from-count}&count=${count}</c:if><c:if test="${from <= 0}"></c:if>"
					id="questions-table-previous"><span></span></a> <a
					class="sort-arrows arrow-right"
					href="<c:if test="${to < total}">?offset=${to}&count=${count}</c:if><c:if test="${to == total}"></c:if>"
					id="questions-table-next"><span></span></a> <input type="hidden"
					id="input-from" value="${from}"></input> <input type="hidden"
					id="input-to" value="${to}"></input> <input type="hidden"
					id="input-total" value="${total}"></input> <input type="hidden"
					id="input-orderDir" value="${orderDir}"></input> <input
					type="hidden" id="input-orderBy" value="${orderBy}"></input> <input
					type="hidden" id="input-count" value="${count}"></input>
			</div>
			<%-- 			<a href="<c:if test="${from >= 2}">?offset=${from-2}&count=2</c:if><c:if test="${from < 2}">#</c:if>" id="questions-table-previous">Previous</a> --%>
			<%-- 				<a href="<c:if test="${to < total}">?offset=${to}&count=2</c:if><c:if test="${to < total}">#</c:if>" id="questions-table-next">Next</a> --%>
			<%-- 				<p><c:if test="${from == 0 && to != 0}">1</c:if><c:if test="${from > 0 || to == 0}">${from}</c:if>-${to} of ${total}</p> --%>
		</article>
	</templates:data>
</templates:cms>



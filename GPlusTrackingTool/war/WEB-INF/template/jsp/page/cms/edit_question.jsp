<%@ taglib tagdir="/WEB-INF/tags/cms" prefix="templates"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="widgets"%>

<c:set var="title">
</c:set>
<templates:cms title="${title }" activeTopNavItemId="dataItem"
	studyId="${studyId }">
	<templates:data topNavSelectedItemIndex="1"
		sideNavActiveItemId="questionsItem" title="Questions - Edit Question"
		showTopNav="false">

		<article class="maia-article" role="article">
			<widgets:form action="/cms/data/save/question/${resource.id}"
				method="post">
				<fieldset>
					<ul>
						<li><label>Question</label> <input type="text" name="q_name"
							value='<c:out value="${resource.name}"></c:out>'></li>
						<li><label>Question Description</label> <textarea
								name="q_description" maxlength="600">
								<c:out value="${resource.description}"></c:out> </textarea>
							<p class="label-info">Maximum 600 characters</p> <!-- 						<li><label>Question Type</label> -->
							<!-- 							<div class="maia-cols"> --> <!-- 								<div class="maia-col-2"> -->
							<!-- 									<label><input checked="checked" name="form03" --> <!-- 										type="radio" value="1"> Yes/No</label> -->
							<!-- 								</div> --> <!-- 								<div class="maia-col-2"> -->
							<!-- 									<label><input name="form03" type="radio" value="2"> -->
							<!-- 										Scale 5 points </label> --> <!-- 								</div> -->
							<!-- 								<div class="maia-col-2"> --> <!-- 									<label><input name="form03" type="radio" value="3"> -->
							<!-- 										Scale 10 points </label> --> <!-- 								</div> -->
							<!-- 							</div></li> -->
						<li><label>Alternative Title/ Buzz Word</label> <input
							type="text" name="q_buzz"
							value='<c:out value="${resource.buzzWord}"></c:out>'>
						<li>
							<!-- 						<li><label>When Trending Choose</label> <select --> <!-- 							class="trending-select"> -->
							<!-- 								<option value="">Multiple</option> --> <!-- 								<option value="1">Single</option> -->
							<!-- 								<option value="9">All</option> --> <!-- 						</select></li> -->
						<li><label>Waves</label>
							<table>
								<tr>
									<th scope="col" class="tiny-col">Date<%--<a href="#"
										class="sorting-arrow">arrow</a> --%></th>
									<th scope="col">Name</th>
									<th scope="col" class="tiny-raw">Show</th>
								</tr>
								<c:forEach items="${dataSets}" var="dataSet">
									<tr>
										<td><fmt:formatDate value="${dataSet.waveDate}"
												pattern="dd/MM/yyyy" /></td>
										<td><c:out value="${dataSet.name}" /></td>
										<td><a href="/cms/data/dataset/edit/${dataSet.id}"
											class="show-icon-eye">show</a></td>
									</tr>
								</c:forEach>
							</table></li>
						<li><label>Trend Groups</label>
							<table id="groups_table">
								<tr class="tiny-raw">
									<th>Group</th>
									<th class="tiny-col">Delete</th>
								</tr>
								<c:forEach items="${resource.answerGroups}" var="group"
									varStatus="var">
									<tr class="tiny-raw groupRow  grp-${var.index}">
										<td class="tiny-col"><c:out value="${group}" /><input
											type="text" hidden="true"
											value='<c:out value="${group}"></c:out>'
											name="grp-${var.index}"></input></td>
										<td class="tiny-col"><a href=""
											class="remove-icon grp-remove-icon" id="${var.index}"></a></td>
									</tr>
								</c:forEach>
							</table> <input id="addGroup" class="maia-button" type="button"
							value="Add"><input class="maia-col-2" id="newGroupName"
							type="text" /></li>
						<li>
							<table>
								<tr>
									<th>Answer</th>
									<th scope="col" class="tiny-raw">Snapshot</th>
									<th scope="col" class="tiny-raw">Trend</th>
									<th scope="col" class="tiny-raw">Group</th>
								</tr>
								<c:forEach items="${answers}" var="answer">
									<tr>
										<td><c:out value="${answer.name}"></c:out></td>
										<c:if test="${answer.onSnapshot}">
											<td><input type="checkBox" checked="checked"
												name="snap_${answer.id}" /></td>
										</c:if>
										<c:if test="${!answer.onSnapshot}">
											<td><input type="checkBox" name="snap_${answer.id}" /></td>
										</c:if>
										<c:if test="${answer.onTrend}">
											<td><input type="checkBox" checked="checked"
												name="trend_${answer.id}" /></td>
										</c:if>
										<c:if test="${!answer.onTrend}">
											<td><input type="checkBox" name="trend_${answer.id}" /></td>
										</c:if>
										<td><select class="trendSelect" name="group_${answer.id}">
												<option>Default</option>
												<c:forEach items="${resource.answerGroups}" var="groupName"
													varStatus="count">
													<c:if test="${groupName eq answer.answerGroup}">
														<option class="grp-${count.index}" selected="selected">
															<c:out value="${groupName}" />
														</option>
													</c:if>
													<c:if test="${groupName != answer.answerGroup}">
														<option class="grp-${count.index}">
															<c:out value="${groupName}" />
														</option>
													</c:if>
												</c:forEach>
										</select></td>

									</tr>
								</c:forEach>
							</table>
						</li>
					</ul>
				</fieldset>
				<button class="maia-button" type="submit">Save</button>
				<a href="/cms/data/question"
					class="maia-button maia-button-secondary" type="reset">Cancel</a>
			</widgets:form>
		</article>
	</templates:data>
</templates:cms>



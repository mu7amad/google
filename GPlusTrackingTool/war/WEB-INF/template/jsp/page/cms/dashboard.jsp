<%@ taglib tagdir="/WEB-INF/tags/cms" prefix="templates"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="title">
Google+ Brand Tracker - Admin Dashboard
</c:set>
<templates:cms title="${title}" activeTopNavItemId="dashboardItem"
	studyId="${studyId }">
	<article class="main-intro">
		<h1>Dashboard</h1>
		<p>The new Google+ Brand Tracker aims at providing researchers
			with detailed information about users' perception of social networks
			in their country of residence. Using the Google+ Brand Tracker will
			also help understand trends and user behavior across various social
			networks. The application allows researchers to compare multiple
			questions across social networks while at the same time looking at
			different filters, such as demographics, internet usage or preferred
			devices.</p>
	</article>
	<article class="main-bashboard">
		<ul class="dashboard maia-cols">
			<li class="maia-col-6">
				<div class="maia-col-3">
					<img src="/_ui/images/icon-01.png" alt="" width="47" height="46">
				</div>
				<div class="maia-col-9">
					<h2>
						<h3>${usersAddedByAdmin }</h3>
					</h2>
					<p>
						Added by You..
						<!-- 						 <a href="">Read More</a> -->
					</p>
				</div>
			</li>
			<li class="maia-col-6">
				<div class="maia-col-3">
					<img src="/_ui/images/icon-01.png" alt="" width="47" height="46">
				</div>
				<div class="maia-col-9">
					<h2>
						<a href="/cms/data">Data</a>
					</h2>
					<p>
						Edit, delete and manage all data waves available in the tracking
						tool.
						<!-- 						 <a href="">Read More</a> -->
					</p>
				</div>
			</li>
			<li class="maia-col-6">
				<div class="maia-col-3">
					<img src="/_ui/images/icon-01.png" alt="" width="47" height="46">
				</div>
				<div class="maia-col-9">
					<h2>
						<a href="/studyController/">Studies</a>
					</h2>
					<p>
						Manage the data import process, this section allows you to upload,
						process, rollback files and view their status.
						<!-- 						<a href="">Read -->
						<!-- 							More</a> -->
					</p>
				</div>
			</li>
			<li class="maia-col-6">
				<div class="maia-col-3">
					<img src="/_ui/images/icon-01.png" alt="" width="47" height="46">
				</div>
				<div class="maia-col-9">
					<h2>
						<a href="/cms/import">Import</a>
					</h2>
					<p>
						Manage the data import process, this section allows you to upload,
						process, rollback files and view their status.
						<!-- 						<a href="">Read -->
						<!-- 							More</a> -->
					</p>
				</div>
			</li>
			<li class="maia-col-6">
				<div class="maia-col-3">
					<img src="/_ui/images/icon-02.png" alt="" width="47" height="46">
				</div>
				<div class="maia-col-9">
					<h2>
						<a href="/cms/settings">Settings</a>
					</h2>
					<p>
						Define countries and regions from this section and emails to
						receive notifications.
						<!-- 						<a href="">Read More</a> -->
					</p>
				</div>
			</li>
			<li class="maia-col-6">
				<div class="maia-col-3">
					<img src="/_ui/images/icon-03.png" alt="" width="47" height="46">
				</div>
				<div class="maia-col-9">
					<h2>
						<a href="/cms/admin">Admins</a>
					</h2>
					<p>
						Manage user privileges and access to the application features.
						<!-- 						<a -->
						<!-- 							href="">Read More</a> -->
					</p>
				</div>
			</li>
		</ul>
	</article>
	<%--TOKEN ${tk }<br />
	${resource} --%>
</templates:cms>



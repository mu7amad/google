<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page
	import="com.google.appengine.api.blobstore.BlobstoreServiceFactory"%>
<%@ page import="com.google.appengine.api.blobstore.BlobstoreService"%>

<%
	BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<form method="post"
		action=<%=blobstoreService.createUploadUrl("/studyController/createNewStudy")%>
		enctype="multipart/form-data">
		Study Name: <input type="text" name="studyName"> <br> <br>
		Description:
		<textarea rows="20" cols="40" name="description"></textarea>
		<br> <input id="images" type="file" name="images[]"
			value="upload" multiple> <br> <input type="submit"
			value="Create"> <a href="/studyController/loadStudies">Cancel</a>
	</form>
</body>
</html>
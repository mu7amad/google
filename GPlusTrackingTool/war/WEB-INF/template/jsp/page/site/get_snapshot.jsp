<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ taglib tagdir="/WEB-INF/tags/site" prefix="templates"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="widgets"%>

<c:set var="footer">


</c:set>
<templates:site footer="${footer}" activeTopNavItemId="snapshotItem"
	title="Snapshot - Google+ Brand Tracker" logo="${logo }"
	studyId="${studyId}">

	<div class="left-side">
		<form class="left-side-form">
			<a href="#" class="maia-button orange-button" id="trend-it-button">Trend
				it!</a>

			<div class="filter-slider">
				<h3>
					<span class="icon-01">Options</span><a href="#" class="expand"></a>
				</h3>

				<div id="scroll" class="expand-list">
					<div class="scrollbar">
						<div class="track">
							<div class="thumb">
								<div class="end"></div>
							</div>
						</div>
					</div>
					<div class="viewport">
						<div class="overview">
							<ul class="form-start">
								<li class="options-item"><label>Data wave</label><br /> <select
									id="wave-select" class="type-select filter-item">
										<c:forEach items="${dataSets}" var="dataSet">
											<option value="${dataSet.id}"
												<c:if test="dataSet.id == selectedDataSets">selected="selected"</c:if>>
												<c:out value="${dataSet.name} - " />
												<fmt:formatDate value="${dataSet.waveDate}"
													pattern="MMM yyyy" />
											</option>
										</c:forEach>
								</select></li>
								<li class="options-item"><label>Country</label><br /> <select
									id="location-select" class="select-with-checkbox"
									multiple="multiple" size="5">
										<!-- 				 class="select-with-checkbox" -->
										<!-- 					multiple="multiple" size="5"> -->

								</select></li>

							</ul>
							<!-- 							<div class="maia-meta"> -->
							<!-- 								<a href="#" id="trend-it-link" class="compare-countries">Select -->
							<!-- 									multiple markets to compare?</a> -->
							<!-- 							</div> -->
						</div>
					</div>
				</div>
			</div>
			<div class="filter-slider">
				<h3>
					<span class="icon-02">Brands</span><a href="#" class="expand"></a>
				</h3>

				<div id="scrollbar1" class="expand-list">
					<div class="scrollbar">
						<div class="track">
							<div class="thumb">
								<div class="end"></div>
							</div>
						</div>
					</div>
					<div class="viewport">
						<div class="overview">

							<ul id="brands-list">
								<c:forEach items="${brands}" var="brand">
									<li style="display: none"><label class="check-brand"><input
											class="brand-input filter-item" type="checkbox"
											value="${brand.id}"> <c:out value="${brand.name }"></c:out>
									</label></li>
								</c:forEach>
							</ul>

						</div>
					</div>
				</div>
			</div>
			<div class="filter-slider">
				<h3>
					<span class="icon-03">Filter</span><a href="#" class="expand"></a>
				</h3>
				<ul class="expand-list">

					<c:forEach items="${filters}" var="filter">
						<c:set var="CompareStrg1" scope="session" value="All" />
						<c:set var="CompareStrg2" scope="session" value="All " />


						<c:forEach items="${divisions[filter.id]}" var="division">
							<c:if
								test="${division.name eq CompareStrg1 or fn:contains(division.name,CompareStrg2)}">
								<c:set var="SumId" scope="session" value="${division.id }" />
							</c:if>
						</c:forEach>


						<li class='filters filter-slider' id="filter${filter.id }">
							<h4 class="MS-filter">
								<c:out value="${filter.name}"></c:out>
								<a href="#" class="expand colapse"></a>
							</h4>
							<ul class="filters-list divisions filters-expand-list"
								style="display: none;">
								<li class="sum-item division-item" id="division${SumId}">All</li>
								<li class="compare-item division-item"
									title="compare all the sub-filters in <c:out value="${filter.name}"></c:out>">Compare</li>
								<c:forEach items="${divisions[filter.id]}" varStatus="status"
									var="division">
									<c:if test="${division.id!=SumId }">
										<li id="division${division.id }"
											class="division-item <c:if test='${status.count== fn:length(divisions[filter.id]) and fn:length(divisions[filter.id]) mod 2 == 0}'>full</c:if>"><c:out
												value="${division.name
											}" /></li>
									</c:if>
								</c:forEach>
							</ul>
						</li>
					</c:forEach>
					<!-- 				</li> -->
				</ul>
			</div>
		</form>
	</div>
	<section class="right-side ">

		<div class="setting-bar">
			<div class="intro-dash clearfix">


				<div id="main-question" class="intro-holder">
					<h2 class="blue-line">
						<%-- 					${selectedQuestionId } --%>
						<div class="question-title">
							<span id="" class='main-question question-header Q0'></span> <a
								href="#" class="expand question-list-expand"></a>
						</div>
						<div class="group-data">
							<span class="group-date-info group-name"></span><a href="#"
								class="group-expand"></a>
						</div>
						<%-- 					<span>Groups</span><select class=""> --%>
						<%--  								<option class="grp-INDEX">Group</option> --%>
						<%--  						</select> --%>
						<%-- 									<span>Groups</span><select class=""> -->
						<%-- 										<c:forEach items="${question.answerGroups}" var="groupName" --%>
						<%-- 											varStatus="count"> --%>
						<%-- 											<option class="grp-${count.index}">${groupName}</option> --%>
						<%-- 										</c:forEach> --%>
						<%-- 									</select> --%>
					</h2>
					<div class="expand-items">
						<ul class="questions-list">
							<c:forEach items="${questions }" var="question">
								<c:if test="${question.id != selectedQuestionId }">
									<li id="${question.id}"><a class="question-item" href="#">
											<c:if test="${empty question.buzzWord}">
												<c:out value="${question.name}" />
											</c:if> <c:if test="${not empty question.buzzWord}">
												<c:out value="${question.buzzWord}" />
											</c:if>
									</a></li>
								</c:if>
							</c:forEach>
						</ul>
					</div>
					<div class="expand-groups-items" style="display: none;">
						<ul class="ans-groups-list">
						</ul>
					</div>
				</div>
				<!-- 				<div class="intro-holder"> -->
				<!-- 					<div class="close-icon"> -->
				<!-- 						<a href="#" class="maia-button maia-button-secondary">X</a> -->
				<!-- 						<h2 class="green-line"> -->
				<!-- 							Awareness of < BRAND> as a social network website<a href="#" -->
				<!-- 								class="expand"></a> -->
				<!-- 						</h2> -->
				<!-- 						<div class="expand-items"> -->
				<!-- 							<ul> -->
				<!-- 								<li>Awareness of 1</li> -->
				<!-- 								<li>Awareness of 2</li> -->
				<!-- 								<li>Awareness of 3</li> -->
				<!-- 								<li>Awareness of 4</li> -->
				<!-- 							</ul> -->
				<!-- 						</div> -->
				<!-- 					</div> -->
				<!-- 				</div> -->
				<a href="#" id="compare-question-button"
					class="maia-button maia-button-secondary">+ Compare Questions</a>
			</div>
			<div class="setting-clear">
				<div class="setting-icon">
					<div class="s-icon"></div>
					<div class="setting-arrow"></div>
				</div>
				<div class="setting-content">
					<ul>
						<li><a href="" class="add-widget">Add to dashboard</a></li>
						<li><a href="#export-file" class="popuplink">Export</a></li>
						<li><a href="/site/allquestions/${studyId }?trend=false"
							class="">View all questions</a></li>
						<%-- 						<li class="border"><a href="#">Help</a></li> --%>
					</ul>
				</div>
			</div>
		</div>
		<div id="notification-container" class="maia-notification"
			style="display: none;"></div>
		<div id="container1"></div>
		<div id="container2" class="export-div" style="display: none;"></div>
		<div class="maia-notification" style="display: none"></div>

		<div class="trend-holder">
			<div class="no-data" style="display: none;">
				<div class="maia-notification">There is no data to display.</div>
			</div>



		</div>

		<button class="maia-button maia-button-secondary" id="legend-button"
			style="float: right; display: none;">Hide legend</button>
		<div class="divisions">
			<button class="maia-button maia-button-secondary" id="bar-to-stacked"
				style="display: none;">
				<div class="stacked"></div>
			</button>
			<button class="maia-button maia-button-secondary selected"
				id="stacked-to-bar" style="display: none;">
				<div class="bars"></div>
			</button>
		</div>

		<div class="hidden-data maia-meta" style="display: none;">
			The data hidden is either insufficient or with no data, <a
				class="hidden-data hidden-check" href="">Show hidden data</a>.
		</div>

		<div id="question-table1" class="question-tables"></div>
		<div id="question-table2" class="question-tables"></div>
		<div id="question-table3" class="question-tables"></div>

		<widgets:form id="exporting-form" action="/site/export" method="post">
			<input name="json" id="hidden-json" type="hidden"></input>
			<input name="tablesNames" id="hidden-table-names" type="hidden"></input>
			<input type="submit" hidden="" id="export" value="Export" />
		</widgets:form>

		<br> <br> <br> <br> <br> <br> <br>
		<br> <br> <br>
		<div style="clear: both;"></div>
	</section>

	<div id="add-new-widget" class="popup"
		style="display: none; width: 650px; height: 460px;">
		<h3>Add New Widget</h3>
		<widgets:form action="/site/dashboard/addnewwidget"
			id="add-dashboard-form" classes="validate" method="post">
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
			<fieldset>
				<input id="table-or-chart" name="table" type="hidden" value="chart"></input>
				<input id="snap-or-trend" name="trend" type="hidden"></input> <input
					id="widget-url" name="contentURL" type="hidden"></input> <input
					id="questions-list" name="questions" type="hidden"></input> <input
					id="brands-widget-list" name="brands" type="hidden"></input> <input
					id="filtersAndDivisions-list" name="filtersAndDivisions"
					type="hidden"></input> <input id="countries-list" name="countries"
					type="hidden"></input> <input id="waves-list" name="waves"
					type="hidden"></input> <label>Widget title</label> <input
					type="text" id="widget-title" class="required" name="title">
				<!-- 					<label>Question</label> -->
				<!-- 				<div class="longtext"> -->
				<!-- 					<select class="type-select"> -->
				<!-- 						<option value="">Awareness of Brands as a social -->
				<!-- 							networking tool</option> -->
				<!-- 						<option value="1">Awareness of Brands as a social -->
				<!-- 							networking tool1</option> -->
				<!-- 						<option value="9">Awareness of Brands as a social -->
				<!-- 							networking tool2</option> -->
				<!-- 						<option value="2">Awareness of Brands as a social -->
				<!-- 							networking tool3</option> -->
				<!-- 					</select> -->
				<!-- 				</div> -->

				<label>Dashboards</label>
				<div class="longtext">

					<select class="type-select" id="user-dashboards"
						name="dashboard_id">


					</select> <label>Add to new dashboard</label> <input name="newDashboardName"></input>
				</div>
				<div class="tab-nav">
					<ul id="tabs" class="clearfix">
						<li><a id="snap-tab"><span class="icon">Snapshot</span></a></li>
					</ul>
				</div>
				<div id="tab-01" class="tab-content clearfix">
					<div class="tab-content-left">
						<div class="clearfix">
							<a href="" id="widget-chart-button"
								class="maia-button maia-button-secondary" disabled="disabled">Chart</a>
							<a href="#" id="widget-table-button"
								class="maia-button maia-button-secondary">Table</a>
						</div>
					</div>

					<!-- 						<div class="clearfix"> -->
					<!-- 							<label>Data Wave</label> <select class="type-select"> -->
					<!-- 								<option value="">Dave Wave 1</option> -->
					<!-- 								<option value="1">Dave Wave 2</option> -->
					<!-- 								<option value="9">Dave Wave 3</option> -->
					<!-- 								<option value="2">Dave Wave 4</option> -->
					<!-- 							</select> -->
					<!-- 						</div> -->
					<!-- 						<div class="clearfix"> -->
					<!-- 							<label>Brands</label> <select class="type-select"> -->
					<!-- 								<option value="">Brand 1</option> -->
					<!-- 								<option value="1">Brand 2</option> -->
					<!-- 								<option value="9">Brand 3</option> -->
					<!-- 								<option value="2">Brand 4</option> -->
					<!-- 							</select> -->
					<!-- 						</div> -->
					<!-- 						<div class="clearfix"> -->
					<!-- 							<label>Filter</label> -->
					<!-- 							<div class="right-select"> -->
					<!-- 								<select class="type-select"> -->
					<!-- 									<option value="">Age</option> -->
					<!-- 									<option value="1">33</option> -->
					<!-- 									<option value="9">33</option> -->
					<!-- 									<option value="2">55</option> -->
					<!-- 								</select> -->
					<!-- 							</div> -->
					<!-- 							<div class="right-select"> -->
					<!-- 								<select class="type-select"> -->
					<!-- 									<option value="">22-35</option> -->
					<!-- 									<option value="1">22-35</option> -->
					<!-- 									<option value="9">22-34</option> -->
					<!-- 									<option value="2">22-35</option> -->
					<!-- 								</select> -->
					<!-- 							</div> -->
					<!-- 						</div> -->
					<!-- 						<div class="clearfix"> -->
					<!-- 							<label></label> <a href="#">Add filter</a> -->
					<!-- 						</div> -->
					<!-- 					</div> -->
					<!-- 					<div class="tab-content-right"> -->
					<!-- 						<div class="clearfix"> -->
					<!-- 							<a href="#" id="widget-chart-button" -->
					<!-- 								class="maia-button maia-button-secondary">Chart</a> <a href="#" -->
					<!-- 								id="widget-table-button" -->
					<!-- 								class="maia-button maia-button-secondary">Table</a> -->
					<!-- 						</div> -->
					<!-- 						<div class="clearfix"> -->
					<!-- 							<label>Country</label> <select class="type-select"> -->
					<!-- 								<option value="">country</option> -->
					<!-- 								<option value="1">egypt</option> -->
					<!-- 								<option value="9">uk</option> -->
					<!-- 								<option value="2">thai</option> -->
					<!-- 							</select> -->
					<!-- 						</div> -->
					<!-- 					</div> -->
				</div>


			</fieldset>
			<button type="submit" class="maia-button" id="save-to-dashboard">Save</button>
			<a href="javascript:parent.$.fancybox.close();" class="maia-button ">Close</a>
		</widgets:form>
	</div>
</templates:site>

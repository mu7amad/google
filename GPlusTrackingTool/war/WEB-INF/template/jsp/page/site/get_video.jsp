<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ taglib tagdir="/WEB-INF/tags/site" prefix="templates"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<templates:site title="BTT - Video" logo="${logo }" studyId="${studyId}">
	<div class="right-side full-width-block">
		<div class="video-container">
			<h3>Google Plus Brand Tracker Video</h3>
			<p>Use this tool to uncover insights from our quarterly Google+
				brand research. Quickly and easily find the data you need to
				understand users, compare performance between countries and learn
				about other social networks in market. For a brief overview, and to
				learn more about using the tool, please click the video below.</p>
			<div class="youtube-shadow">
				<iframe width="640" height="390"
					src="//www.youtube.com/embed/grPVUAXQI-I" frameborder="0"
					allowfullscreen></iframe>
			</div>
		</div>
	</div>
</templates:site>
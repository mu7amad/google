<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
<head>
<style>
div.error {
	margin: 7% auto 0;
	max-width: 390px;
	min-height: 180px;
	padding: 30px 0 15px;
	background: url(//www.google.com/images/errors/robot.png) 100% 5px
		no-repeat;
	padding-right: 205px;
}

div.error p {
	margin: 11px 0 22px;
	overflow: hidden;
}

div.error ins {
	color: #777;
	text-decoration: none;
}
</style>

</head>
<body>
	<!-- 	<div class="error"> -->
	<!-- 		<a href="http://www.googleplus.com/"><img -->
	<!-- 			src="//www.google.com/images/errors/logo_sm.gif" alt="Google"> -->
	<!-- 		</a> -->
	<%-- 		<p>${exceptionCaught.status}</p> --%>
	<%-- 		<p>${exceptionCaught.code}</p> --%>
	<%-- 		<p>${status.message}</p> --%>
	<%-- 		<a href="${exceptionCaught.moreInfoUrl}">Support</a> --%>

	<!-- 	</div> -->

	<div class="error">

		<a href=""><img src="//www.google.com/images/errors/logo_sm.gif"
			alt="Google"> </a>
		<p>
			<b>${exceptionCaught.code}.</b>
			<c:if test="${not empty exceptionCaught.message}">${exceptionCaught.message}.</c:if>
			<ins>That's an error.</ins>
		</p>
		<p>
			An error occured on the server. <br /> Try again later.
			<ins>That's all we know.</ins>
		</p>
	</div>
</body>
</html>
<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags/cms" prefix="templates"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ attribute name="sideNavActiveItemId" description="" required="true"%>
<%@ attribute name="topNavFirstItem" description="" required="false"%>
<%@ attribute name="topNavFirstItemLink" description="" required="false"%>
<%@ attribute name="topNavSecondItem" description="" required="false"%>
<%@ attribute name="topNavSecondItemLink" description=""
	required="false"%>
<%@ attribute name="topNavThirdItem" description="" required="false"%>
<%@ attribute name="topNavThirdItemLink" description=""
	required="false"%>
<%@ attribute name="topNavSelectedItemIndex" description=""
	required="false"%>
<%@ attribute name="showTopNav" description="" type="Boolean"
	required="false"%>
<%@ attribute name="title" description="" required="true"%>

<templates:topNav title="${title }"
	topNavFirstItemLink="${topNavFirstItemLink }"
	topNavSecondItem="${topNavSecondItem}"
	topNavSelectedItemIndex="${topNavSelectedItemIndex}"
	topNavFirstItem="${topNavFirstItem}"
	topNavSecondItemLink="${topNavSecondItemLink}"
	topNavThirdItem="${topNavThirdItem}"
	topNavThirdItemLink="${topNavThirdItemLink}" showTopNav="${showTopNav}"></templates:topNav>
<nav class="maia-nav" id="maia-nav-y" role="navigation">
	<ul>
		<li
			<c:if test="${sideNavActiveItemId == 'dataWavesItem'}">class="active"</c:if>><a
			href="/cms/data/dataset">Data waves</a></li>
		<li
			<c:if test="${sideNavActiveItemId == 'categoriesItem'}">class="active"</c:if>><a
			href="/cms/data/category">Categories</a></li>
		<li
			<c:if test="${sideNavActiveItemId == 'questionsItem'}">class="active"</c:if>><a
			href="/cms/data/question">Questions</a></li>
		<li
			<c:if test="${sideNavActiveItemId == 'categorizeQuestionsItem'}">class="active"</c:if>><a
			href="/cms/data/question/categorize">Categorize Questions</a></li>
		<li
			<c:if test="${sideNavActiveItemId == 'brandsItem'}">class="active"</c:if>><a
			href="/cms/data/brand">Brands</a></li>
		<%-- 		<li <c:if test="${sideNavActiveItemId == 'answersItem'}">class="active"</c:if>><a href="#">Answers</a></li> --%>
		<li
			<c:if test="${sideNavActiveItemId == 'filtersItem'}">class="active"</c:if>><a
			href="/cms/data/filter">Filters</a></li>
		<li id="defaultDashboardsItem"
			<c:if test="${sideNavActiveItemId == 'defaultDashboardsItem'}">class="active"</c:if>><a
			href="/cms/data/defaultdashboard">Default Dashboards</a></li>
	</ul>
</nav>
<article class="maia-article" role="article">
	<jsp:doBody />
</article>






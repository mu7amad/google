<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ tag import="com.google.appengine.api.users.UserService"%>
<%@ tag import="com.google.appengine.api.users.UserServiceFactory"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="title" description="" required="true"%>
<%@ attribute name="head" description="" required="false"%>
<%@ attribute name="footer" description="" required="false"%>
<%@ attribute name="activeTopNavItemId" description="" required="true"%>
<%@ attribute name="lock" description="" type="Boolean" required="false"%>
<%@ attribute name="studyId" description="" required="true"%>

<%-- <jsp:attribute name="template"></jsp:attribute> --%>

<!DOCTYPE html>
<html class="google" lang="en">
	<head>
		<script>
			(function (H) {
				H.className = H.className.replace(/\bgoogle\b/, 'google-js')
			})(document.documentElement)
		</script>
		<meta charset="utf-8">
		<meta content="initial-scale=1, minimum-scale=1, width=device-width" name="viewport">
		<title>${title}</title>
		<link rel="stylesheet" media="all" href="/cms/app/styles/main.css"/>
		<script src="//www.google.com/js/google.js"></script>
		<script>
			new gweb.analytics.AutoTrack({profile: "UA-38263893-2"});
		</script>

		${head}
	</head>
<body ng-app="app" <c:if test="${lock == true }">class="body-hidden"</c:if>>
<!--[if lt IE 10]> <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p> <![endif]-->
<!-- start header -->
<base href="/">
<header data-crfcode="${_csrf.token}" layout="row" flex="100">
	<md-toolbar layout="row" layout-align="center">
		<div class="md-toolbar-tools" flex-md="80" flex-gt-md="90">
			<span>MI Data Explorer</span>
			<span flex></span>
			<!-- main menu  -->
			<md-list-item>
					<md-button ui-sref="dashboard" ui-sref-opts="{reload: true}" class="md-secondary">Dashboard</md-button>
					<md-button ui-sref="studies" ui-sref-opts="{reload: true}"  class="md-secondary">Studies</md-button>
					<md-button ui-sref="admins" class="md-secondary">Admins</md-button>
			</md-list-item>
		</div>
	</md-toolbar>
</header>
<!-- /Navigtion -->
<ui-view></ui-view>
<!-- /Content -->
<footer layout="row" layout-align="center">
	<div layout-align="space-between center" flex-md="80" flex-gt-md="90">
		<ul class="mi-footer-nav">
			<li>
				<a href="#">Google</a>
			</li>
			<li>
				<a href="#">About google</a>
			</li>
			<li>
				<a href="#">Privacy & Terms</a>
			</li>
			<li>
				<a href="#">Help</a>
			</li>
		</ul>
	</div>
</footer>
<script src="/cms/tmp/scripts/bundle.js"></script>
</body>
</html>

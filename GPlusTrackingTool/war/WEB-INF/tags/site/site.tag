<%@tag import="com.google.appengine.api.users.User"%>
<%@tag
	import="com.brightcreations.gplus.web.security.ApplicationUserRole"%>
<%@tag import="com.brightcreations.gplus.module.controllers.OfyService"%>
<%@tag import="com.brightcreations.gplus.module.model.ApplicationUser"%>
<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ tag import="com.google.appengine.api.users.UserService"%>
<%@ tag import="com.google.appengine.api.users.UserServiceFactory"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="title" description="" required="true"%>
<%@ attribute name="studyId" description="" required="true"%>
<%@ attribute name="description" description="" required="false"%>
<%@ attribute name="keywords" description="" required="false"%>
<%@ attribute name="head" description="" required="false"%>
<%@ attribute name="footer" description="" required="false"%>
<%@ attribute name="activeTopNavItemId" description="" required="false"%>

<%@ attribute name="logo" description="" required="true"%>


<!DOCTYPE html>
<!--[if IE 7]> <html class="google ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="google ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="google ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="google" lang="en">
<!--<![endif]-->
<head>
<script>
	(function(H) {
		H.className = H.className.replace(/\bgoogle\b/, 'google-js')
	})(document.documentElement)
</script>
<meta charset="utf-8">
<meta content="initial-scale=1, minimum-scale=1, width=device-width"
	name="viewport">
<meta name="description" content="${description}">
<meta name="keywords" content="${keywords}">
<title>${title }</title>
<link rel="stylesheet" media="all" href="/site_ui/css/default.css">
<script src="//www.google.com/js/google.js"></script>
<script>
	new gweb.analytics.AutoTrack({
		profile : "UA-38263893-1"
	});
</script>
<link
	href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;lang=en"
	rel="stylesheet">
<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

${head}
</head>
<body>
	<header>
		<div class="maia-header" id="maia-header" role="banner">
			<div class="maia-aux">
				<h1>
					<!-- 					<a href="/"><img alt="Google" -->
					<!-- 						src="//www.google.com/images/logos/google_logo_41.png">+ -->

					<img src="${logo }"> Brand Tracker
				</h1>
			</div>
		</div>
	</header>
	<!-- /Header -->
	<script type="text/javascript">
		function geturl() {
			alert(window.location.href);
		}
	</script>
	<nav class="maia-nav" id="maia-nav-x" role="navigation">
		<div class="maia-aux">
			<ul>
				<li
					<c:if test="${activeTopNavItemId == 'homeItem' }">class="active"</c:if>>
					<a href="/site/dashboard/${studyId}">Dashboard</a>
				</li>
				<li
					<c:if test="${activeTopNavItemId == 'snapshotItem' }">class="active"</c:if>><a
					href="/site/snapshot/${studyId}">Snapshot</a></li>
				<li
					<c:if test="${activeTopNavItemId == 'trendsItem' }">class="active"</c:if>><a
					href="/site/trend/${studyId }">Trends</a></li>

				<c:if test="${admin}">
					<li class="user-login"><a href="/cms/${studyId }"
						class="maia-button orange-button">Admin</a></li>
				</c:if>
			</ul>
		</div>
	</nav>
	<%
		User u = UserServiceFactory.getUserService().getCurrentUser();
	%>
	<nav class="sub-nav clearfix">
		<ul>
			<li class="user"><a href="#"><%=u == null ? "anonymous" : UserServiceFactory.getUserService().getCurrentUser().getEmail()%></a></li>
			<li><a href="#Country-popup" id="location-popup"
				class="popuplink">Settings</a></li>
			<li><a href="/logout">Logout</a></li>
			<li style="display: none" id="UI"><%=u.getUserId()%></li>
		</ul>
	</nav>
	<!-- /Navigtion -->

	<section id="main-content" class="clearfix colums main-hide"
		role="main">
		<div class="loading" style="display: none;">
			<div id="kill-cont">
				<img src="/site_ui/images/ajax-loader.gif" width="31" height="31">
			</div>
		</div>
		<jsp:doBody />
		<input id="_tk" type="hidden" value=${tk } />
	</section>
	<!-- /Content -->
	<div id="export-file" class="popup"
		style="display: none; width: 310px; height: 320px;">
		<h3>Export</h3>
		<div id="popup-content">
			<div class="maia-meta drive-notification popup-notification">Browser
				pop-ups should be allowed for this domain.</div>
			<fieldset>
				<label>Type of file</label> <select class="" id="export-select">
					<option value="1">Google Drive</option>
					<option value="0">XLS</option>
					<!-- 					<option value="1">PDF</option> -->
					<option value="2">PNG</option>
					<!-- 					<option value="3">JPEG</option> -->
					<!-- 					<option value="4">SVG</option> -->
				</select>
				<div class="maia-notification drive-notification">When
					exporting to Google Drive, if you are prompted to login, please
					reclick save again to Google Drive once logged in.</div>
			</fieldset>
			<button type="submit" class="maia-button export-chart">Save</button>
			<!-- 		<a href="" class="maia-button ">Close </a> -->
			<a href="javascript:parent.$.fancybox.close();" class="maia-button ">Close</a>
		</div>
	</div>



	<div id="Country-popup" class="popup"
		style="display: none; width: 310px; height: 220px;">
		<form action="setDefaultCountry" method="post">
			<h3>Set default country</h3>
			<div id="popup-content">
				<fieldset>
					<label>Country</label> <select class="type-select"
						id="location-set">
					</select>
					<!-- 					<input type="text" id="location-set" name="country" -->
					<!-- 						value='5539339580735488' /> -->
				</fieldset>
				<input id="_csrf" type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
				<button id="save-location" class="maia-button ">Save</button>
				<!-- 				<input type="submit" value="Submit"> -->
<!-- 				<button id="close-location" class="maia-button ">Close</button> -->
				<a href="javascript:parent.$.fancybox.close();" class="maia-button ">Close</a>
				<input type="hidden" id="_tk" name="tk" value="${tk }" />
			</div>
		</form>
	</div>

	<!-- / Popop -->
	<div id="maia-signature"></div>
	<footer class="maia-footer" id="maia-footer">
		<div id="maia-footer-local">
			<div class="maia-aux pages-links">
				<%-- 				<p> 

 					<a href="#">About the research</a> | <a href="#">Terms & 
 						Conditions</a> 
 				</p> --%>
			</div>
		</div>
		<div id="maia-footer-global">
			<div class="maia-aux">
				<ul>
					<li><a href="//www.google.com/">Google</a></li>
					<li><a href="//www.google.com/intl/en/policies/">Privacy
							&amp; Terms</a></li>
					<li><a href="/site/dashboard/video">Explore Google Plus
							Brand Tracker</a></li>
					<%--<li><a href="">Terms of Service</a></li>--%>

				</ul>
			</div>
		</div>
	</footer>
	<!-- /Footer -->

	<script src="//www.google.com/js/maia.js"></script>

	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	<script>
		window.jQuery
				|| document
						.write("<script src='/site_ui/js/jquery-1.8.3.min.js'>\x3C/script>")
	</script>
	<!--[if lte IE 9]>
		<script src="/site_ui/js/PIE.js"></script>
		<script src="/site_ui/js/jquery.placeholder.min.js"></script>
	<![endif]-->
	<script src="/site_ui/js/jquery.selectbox-0.2.min.js"></script>
	<script src="/site_ui/js/jquery.fancybox.pack.js"></script>
	<script src="/site_ui/js/jquery.tinyscrollbar.min.js"></script>
	<script src="/site_ui/js/jquery-ui-1.9.2.custom.min.js"></script>
	<script src="/site_ui/js/jquery.multiselect.min.js"></script>
	<script src="/site_ui/js/jquery.history.js"></script>
	<!-- 	<script src="/site_ui/js/highcharts/highcharts.src.js" -->
	<!-- 		type="text/javascript"></script> -->

	<!-- 	<script src="https://code.highcharts.com/highcharts.src.js" -->
	<!-- 		type="text/javascript"></script> -->
	<!-- 	<script src="https://code.highcharts.com/highcharts-more.js" -->
	<!-- 		type="text/javascript"></script> -->
	<script src="https://code.highcharts.com/stock/highstock.js"></script>
	<script src="https://code.highcharts.com/stock/modules/exporting.js"></script>
	<script
		src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>


	<script src="/site_ui/js/jquery-tablesorter/jquery.tablesorter.min.js"></script>
	<script src="/site_ui/js/table-scroll/jquery.mousewheel.js"></script>
	<script src="/site_ui/js/table-scroll/jquery.jscrollpane.js"></script>
	<script src="/site_ui/js/canvg/rgbcolor.js"></script>
	<script src="/site_ui/js/canvg/canvg.js"></script>
	<script src="/site_ui/js/main.js"></script>
	<script src="/_ui/js/jquery.validate.min.js"></script>
	${footer}
</body>
</html>


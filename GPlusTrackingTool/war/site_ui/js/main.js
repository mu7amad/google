var Site = {

	/**
	 * Init Function
	 */
	init : function() {
		// $("body").removeClass("no-js");
		Site.closeDropDown();
		Site.AutoLaunch();
		Site.saveButtonEvent();
		Site.Popup();
		Site.Setting();
		Site.palette();
		Site.validate();
		Site.getCustomPages();
		Site.checkVideoCookie();
		Site.registerNotificationEvents();
		Site.noteForm("#container1");
		Site.noteForm('#tc-container');
		// if (window.location.href.indexOf("site/dashboard") != -1) {
		// if (window.location.href.indexOf("site/studyDashboard") != -1) {
		Site.popupShareDashboard();
		// }
		if (window.location.search != ""
				&& (window.location.href.indexOf("site/snapshot") != -1 || window.location.href
						.indexOf("site/trend") != -1)) {

			Site.Selectbox();
			Site.SlideSidebar();
			Site.Popup();
			Site.Tabs();
			Site.ScrollBar();
			Site.expand();
			// Site.tableselect();
			Site.ArrowDown();
			Site.setHistory();
			// Site.initComponents(createRequestForWaveData = true);
			Site.registerEvents();
			Site.urlSplit = Site.splitUrl(location.search);

			if (window.location.href.indexOf("site/snapshot") != -1) {

				EventBus.getInstance().registerHandler('loadWaveData', this,
				'createSnaphotWaveChangeRequest');
			} else if (window.location.href.indexOf("site/trend") != -1) {
				EventBus.getInstance().registerHandler('loadWaveData', this,
						'createTrendWaveChangeRequest');
			}
			EventBus.getInstance().registerHandler('initSelectedFilters', this,
					'initFilters');

			EventBus.getInstance().registerHandler('initQuestions', this,
					'initQuestions');

			EventBus.getInstance().registerHandler('initBrands', this,
					'initBrands');

			Site.initComponents();

			Site.dashboardFunctionalities();
		} else {
			Site.registerCountryPopEvents();
			// Site.saveButtonEvent();
		}

	},
	getStudyIdFromUrl : function() {
		var name = window.location.href;
		name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
		var regexS = "[\\?&]" + name + "=([^&#]*)";
		var regex = new RegExp(regexS);
		var results = regex.exec(window.location.href);
		if (results == null)
			return "";
		else
			return results[1];

	},
	palette : function() {
		Site.colorsNewPallete = [ "3779ff", "c1342d", "ffdc39", "67ad33" ];
		Site.symbolsList = [ "square", "diamond", "triangle-down", "triangle",
				"circle", "circle" ];
		var colorsRed = [ "#a7291b", "#ac3426", "#b03f31", "#b5493d",
				"#b9544a", "#bd5f55", "#c26a60", "#c7746c", "#c97f76",
				"#cf8b82", "#d3948d", "#d79e97", "#dca9a5", "#e0b5af",
				"#e4bebb", "#e8c9c6", "#edd4d0", "#f3dfde", "#f7e9e8" ];
		var colorsBlue = [ "#013e7f", "#0d4786", "#1a528b", "#265b91",
				"#336598", "#406e9f", "#4d78a5", "#5a81ac", "#658bb2",
				"#7395b8", "#7f9ebd", "#8da8c5", "#99b1cb", "#a6bcd3",
				"#b3c5d9", "#bfcfdf", "#ccd8e6", "#dae2ed", "#e4ebf3" ];
		var colorsGreen = [ "#009345", "#0e974f", "#1a9d59", "#26a361",
				"#34a86b", "#40ad74", "#4cb37c", "#5ab886", "#66be8e",
				"#72c39a", "#80c8a3", "#8ccdac", "#98d3b7", "#a7d9be",
				"#b2dec7", "#bfe5d0", "#cde9da", "#d9efe3", "#e5f4ed" ];
		Site.colorPallete = new Array();
		Site.colorPallete.push(colorsBlue);
		Site.colorPallete.push(colorsGreen);
		Site.colorPallete.push(colorsRed);
	},
	initComponentsCallback : function() {

		Site.initCompareQuestions();
		Site.initFilters();

		Site.selectboxDetach();
		Site.Selectbox();
		Site.ScrollBar();
		Site.initCompareQuestionButton();
		// Site.initData();

	},
	validate : function() {

		$.validator.addMethod("gmailandgoogle", function(value, element) {
			return this.optional(element)
					|| /^[\w-\.]+@(gmail\.com|google\.com)+$/i.test(value);
		}, "Email address must be gmail or google address.");

		$.validator.addMethod("google", function(value, element) {
			return this.optional(element)
					|| /^[\w-\.]+@(google\.com)+$/i.test(value);
		}, "Email address must be google address.");

		$('.roles').change(
				function() {
					var selectVal = $('.roles :selected').val();
					// alert(selectVal);
					if ($(this).val() == 'ROLE_RESEARCHER') {
						$('li.email-role').find('input.emailval').addClass(
								'gmailandgoogle').removeClass('google');
					}
					if ($(this).val() == 'ROLE_ADMIN') {
						$('li.email-role').find('input.emailval').addClass(
								'google').removeClass('gmailandgoogle');
					}
					return false;
				});

		$(".validate").validate(
				{
					errorElement : "p",
					invalidHandler : function(form, validator) {
						var errors = validator.numberOfInvalids();
						if (errors) {
							var message = errors == 1
							$(".maia-form-error-msg").show().parents('li')
									.addClass('maia-form-error');
						} else {
							$(".maia-form-error-msg").hide().parent('li')
									.Class('maia-form-error');
						}
					}
				});
	},

	renderQuestions : function(questions) {
		$('#main-question .questions-list').empty();
		$('.compare-question-holder').remove();
		var index = 0;
		for (q in questions) {

			// Site.showRelatedBrands(Site.waveData.questions[q].brandsIds);

			if (questions[q].buzzWord != null) {
				if (questions[q].buzzWord == "") {
					var questionName = Site.escapeHtml(questions[q].name);
				} else {
					var questionName = Site.escapeHtml(questions[q].buzzWord);
				}
			} else {
				var questionName = Site.escapeHtml(questions[q].name);
			}

			$('.questions-list').append(
					'<li id="question' + questions[q].id
							+ '"><a class="question-item" href="#">'
							+ questionName + '</a></li>');
		}
	},
	initQuestions : function() {
		var mainQuestionId = Site.urlSplit.mainQuestion;
		var compareQuestions = Site.urlSplit.compareQuestions;

		Site.renderQuestions(Site.waveData.questionsList);
		Site.setMainQuestion(mainQuestionId);
		$(compareQuestions).each(function() {
			Site.addCompareQuestion(this);
		});
		Site.initCompareQuestionButton();
	},

	saveButtonEvent : function() {
		$('#save-location')
				.live(
						'click',
						function(event) {
							selectedCountry = $('#location-set').val();
							alert("save js button" + selectedCountry)
							var query = $
									.post(
											"/site/setDefaultCountry",
											{
												country : selectedCountry,
												tk : $("#_tk").val(),
												"_csrf" : $('#_csrf').val()
											},
											function() {
//												alert("d5l gowa el-function")
												Site.hasDefaultCountry = true;
												Site.defaultCountryId = selectedCountry;
												parent.$.fancybox.close();
												if (window.location.href
														.indexOf("site/dashboard") != -1
														&& window.location.href
																.indexOf("site/dashboard/video") == -1) {
													location.reload();
												}
											}, "json");
							event.preventDefault();
						});
	},

	initComponents : function() {// function(createRequestForWaveData) {
		// var urlSplit = Site.splitUrl(location.href);
		// var datasetId = urlSplit.wave;
		// var endDataSetId = urlSplit.secondWave;
		// var mainQuestionId = urlSplit.mainQuestion;
		//
		// if (createRequestForWaveData) {
		// if (window.location.href.indexOf("site/snapshot") != -1) {
		//
		// Site.createSnaphotWaveChangeRequest(datasetId, mainQuestionId,
		// Site.initComponentsCallback);
		// } else if (window.location.href.indexOf("site/trend") != -1) {
		// Site.createTrendWaveChangeRequest(datasetId, endDataSetId,
		// mainQuestionId, Site.initComponentsCallback);
		// }
		//
		// } else {
		// Site.initWaveRelatedData(mainQuestionId);
		// Site.initComponentsCallback();
		// // Site.initFilters();
		// }

		e = EventBus.getInstance();
		e.fireEvent('loadWaveData', {
			datasetId : Site.urlSplit.wave,
			endDataSetId : Site.urlSplit.secondWave,
			mainQuestionId : Site.urlSplit.mainQuestion,
			callbackFunction : function() {
				e.fireEvent('initSelectedFilters');
				Site.selectboxDetach();
				Site.Selectbox();

				e.fireEvent('initQuestions');

				e.fireEvent('initBrands', {
					callbackFunction : function() {
						Site.createRequest();
					},
					createBrandsRequest : true
				});
			}
		});

	},

	drawTrendTables : function(sort, sorting, tableIndex, showHidden) {
		Site.mainFlag = 1;
		var hideDataCheck = true;
		Site.startingIndex = 0;
		Site.filtersStartingIndex = 0;
		Site.filtersLastIndex = 0;
		if (tableIndex == null) {
			$('.question-tables').empty();
		} else {
			$("#question-table" + tableIndex.charAt(1)).empty();
		}
		// for ( var i = 1; i < 4; i++) {
		// if (tableIndex == null)
		// $('#Questions-table' + i).empty();
		// else if (parseInt(tableIndex.charAt(1)) == i) {
		// $('#Questions-table' + i).empty();
		// }
		// }
		$('.question-header')
				.each(
						function() {
							if (tableIndex == null
									|| (tableIndex != null && parseInt(tableIndex
											.charAt(1)) == Site.mainFlag)) {
								var questionName = "<h><b>"
										+ Site.escapeHtml($(this).text())
										+ "</b></h>";
								var questionNameNotBuzz = "<h6 class='table-question-name'>"
										+ Site
												.escapeHtml(Site.waveData.questions[this.id
														.replace('question', '')].name)
										+ "</h6>";
								// Creating the table head row
								var table = $("<table class='to-export'></table>");
								var thead = $("<thead></thead>");
								var rowHead = $("<tr> </tr>");
								var brandClass = "brand-header-trend";
								var filterClass = "filter-header-trend";
								var colHead1 = $("<th style='cursor: pointer' class='"
										+ brandClass + "' >Brand</th>");
								var colHead2 = $("<th >Answer</th>");
								var colHead3 = $("<th style='cursor: pointer' class='"
										+ filterClass + "'>Filter</th>");
								var colHead4 = $("<th class='current-val' style='cursor: pointer' >Last</th>");
								var colHead5 = $("<th class='previous-val' style='cursor: pointer'>First</th>");
								var colHead6 = $("<th class='change-val' style='cursor: pointer'>Change</th>");
								var body = $("<tbody></tbody>");
								rowHead.append(colHead1);
								rowHead.append(colHead2);
								rowHead.append(colHead3);
								rowHead.append(colHead4);
								rowHead.append(colHead5);
								rowHead.append(colHead6);
								thead.append(rowHead);
								table.append(thead);

								// count the numbers of brands selected
								var brandCount = $('.brand-input:checked:visible').length;
								if (brandCount > 1) {
									var BrandsValues = new Array();
									for (var j = 0; j < brandCount; j++) {
										for (var i = j; i < Site.data.values.length; i += brandCount) {
											BrandsValues
													.push(Site.data.values[i]);
										}
									}
									// for ( var i = 1; i <
									// Site.data.values.length; i++) {
									// BrandsValues.push(Site.data.values[i]);
									// i += brandCount - 1;
									// }
								}
								// get the filter count and the selected filters
								var filtersCount = 1;
								var filters = new Array();
								$('.division-item.selected:not(".sum-item")')
										.each(
												function() {
													if ($(this).text() != "Compare")
														Site
																.escapeHtml(filters
																		.push($(
																				this)
																				.text()));
													else {
														compare = true;
														filtersCount = $(this)
																.siblings().length - 1;
														filters
																.push("All sub-filters of "
																		+ Site
																				.escapeHtml($(
																						this)
																						.parent()
																						.prev(
																								"h4")
																						.text()));
													}

												});

								// creating the filters header
								var currFilter = "";
								for (var i = 0; i < filters.length; i++)
									currFilter += ", " + filters[i];
								if (currFilter.length == 0)
									currFilter = ", No Filters";
								currFilter = currFilter.substring(1);
								var currFilterHeader = $("<p class='maia-meta'></p>");
								currFilterHeader.append(currFilter);
								// creating brand column
								selectedLocations = new Array();
								$('#location-select option:selected').each(
										function() {
											selectedLocations.push($(this)
													.text());
										});
								for (var loc = 0; loc < selectedLocations.length; loc++) {
									if (selectedLocations.length > 1)
										var currLocation = ", "
												+ selectedLocations[loc];
									else
										currLocation = "";
									$('.brand-input:checked:visible')
											.each(
													function() {
														var secondColumn = true;
														var answerCol = '';
														var filterColumn = '';
														var mainRow = $("<tr> </tr>");
														if (sort == false) {
															var brandCol = $("<td rowspan= '"
																	+ (Site.data.counts[Site.mainFlag - 1] * filtersCount)
																	+ "'>"
																	+ Site
																			.escapeHtml($(
																					this)
																					.parent()
																					.text())
																	+ Site
																			.escapeHtml(currLocation)
																	+ "</td>");
															mainRow
																	.append(brandCol);
														}
														var brandRowSpan = 0;
														var firstColumn = true;
														for (var i = 0; i < Site.data.counts[Site.mainFlag - 1]; i++)// loop
														// on
														// each
														// answer
														{
															var addedAnswer = true;
															// if (brandCount ==
															// 1)
															// {
															// Site.filtersLastIndex
															// = 0;
															// }
															if (!firstColumn
																	&& sort == false)
																mainRow = $("<tr></tr>");
															if (sort == false) {
																answerCol = $("<td rowspan= '"
																		+ filtersCount
																		+ "'>"
																		+ "</td>");
																answerCol
																		.append(Site.data.answers[i
																				+ Site.startingIndex]);
																// mainRow
																// .append(answerCol);
															}
															if (sort == false)
																secondColumn = true;
															var insufficientCount = 0;

															for (var j = 0; j < filtersCount; j++) // loop
															// on
															// each
															// filter
															{

																var insufficientCheck = false;
																if (!showHidden) {
																	$(
																			".hidden-check")
																			.text(
																					"Show hidden data");
																	if (brandCount > 1) {
																		if (BrandsValues[Site.filtersStartingIndex
																				+ j][Site.data.dataSetsNames.length - 1] == -1000
																				|| BrandsValues[Site.filtersStartingIndex
																						+ j][0] == -1000) {
																			insufficientCheck = true;
																			insufficientCount++;
																		} else if (BrandsValues[Site.filtersStartingIndex
																				+ j][Site.data.dataSetsNames.length - 1] == 0
																				&& BrandsValues[Site.filtersStartingIndex
																						+ j][0] == 0) {
																			insufficientCheck = true;
																			insufficientCount++;
																		}
																	} else {
																		if (Site.data.values[Site.filtersStartingIndex
																				+ j][Site.data.dataSetsNames.length - 1] == -1000
																				|| Site.data.values[Site.filtersStartingIndex
																						+ j][0] == -1000) {
																			insufficientCheck = true;
																			insufficientCount++;
																		} else if (Site.data.values[Site.filtersStartingIndex
																				+ j][Site.data.dataSetsNames.length - 1] == 0
																				&& Site.data.values[Site.filtersStartingIndex
																						+ j][0] == 0) {
																			insufficientCheck = true;
																			insufficientCount++;
																		}
																	}
																} else if (showHidden) {
																	$(
																			".hidden-check")
																			.text(
																					"Hide insufficient data");
																	insufficientCheck = false;
																	hideDataCheck = false;
																}

																if (showHidden == false
																		&& (insufficientCheck == true)) {
																	hideDataCheck = false;
																}
																if (!secondColumn)
																	mainRow = $("<tr></tr>");
																filterColumn = $("<td></td>");

																if (sort == true) {
																	mainRow = $("<tr></tr>");
																	var brandCol = $("<td>"
																			+ $(
																					this)
																					.parent()
																					.text()
																			+ currLocation
																			+ "</td>");
																	mainRow
																			.append(brandCol);

																	answerCol = $("<td >"
																			+ "</td>");
																	answerCol
																			.append(Site.data.answers[i
																					+ Site.startingIndex]);
																	mainRow
																			.append(answerCol);
																}

																if (Site.data.divisions[j].length != 0)
																	filterColumn
																			.append(Site.data.divisions[j]);
																else
																	filterColumn
																			.append("No Filters");
																if (!insufficientCheck)
																	mainRow
																			.append(filterColumn);
																var currentColumn = $("<td></td>");
																var previousColumn = $("<td></td>");
																var changeColumn = $("<td></td>");
																if (brandCount > 1) {
																	if (BrandsValues[Site.filtersStartingIndex
																			+ j][Site.data.dataSetsNames.length - 1] == -1000) {
																		currentColumn
																				.append("Insufficient data");
																	} else {
																		currentColumn
																				.append(BrandsValues[Site.filtersStartingIndex
																						+ j][Site.data.dataSetsNames.length - 1]
																						+ "%");
																	}
																	if (BrandsValues[Site.filtersStartingIndex
																			+ j][0] == -1000) {
																		previousColumn
																				.append("Insufficient data");
																	} else {
																		previousColumn
																				.append(BrandsValues[Site.filtersStartingIndex
																						+ j][0]
																						+ "%");
																	}
																	if (BrandsValues[Site.filtersStartingIndex
																			+ j][0] == -1000
																			|| BrandsValues[Site.filtersStartingIndex
																					+ j][Site.data.dataSetsNames.length - 1] == -1000) {
																		changeColumn
																				.append("Insufficient data");
																	} else {
																		changeColumn
																				.append((BrandsValues[Site.filtersStartingIndex
																						+ j][0] - BrandsValues[Site.filtersStartingIndex
																						+ j][Site.data.dataSetsNames.length - 1])
																						.toFixed(2)
																						+ "%");
																	}
																} else {
																	if (Site.data.values[Site.filtersStartingIndex
																			+ j][Site.data.dataSetsNames.length - 1] == -1000) {
																		currentColumn
																				.append("Insufficient data");
																	} else {
																		currentColumn
																				.append(Site.data.values[Site.filtersStartingIndex
																						+ j][Site.data.dataSetsNames.length - 1]
																						+ "%");
																	}
																	if (Site.data.values[Site.filtersStartingIndex
																			+ j][0] == -1000) {
																		previousColumn
																				.append("Insufficient data");
																	} else {
																		previousColumn
																				.append(Site.data.values[Site.filtersStartingIndex
																						+ j][0]
																						+ "%");
																	}
																	if (Site.data.values[Site.filtersStartingIndex
																			+ j][0] == -1000
																			|| Site.data.values[Site.filtersStartingIndex
																					+ j][Site.data.dataSetsNames.length - 1] == -1000) {
																		changeColumn
																				.append("Insufficient data");
																	} else {
																		changeColumn
																				.append((Site.data.values[Site.filtersStartingIndex
																						+ j][0] - Site.data.values[Site.filtersStartingIndex
																						+ j][Site.data.dataSetsNames.length - 1])
																						.toFixed(2)
																						+ "%");
																	}
																}
																if (!insufficientCheck) {
																	if (addedAnswer
																			&& filtersCount > 1) {
																		filterColumn
																				.before(answerCol);
																		addedAnswer = false;
																	}

																	mainRow
																			.append(currentColumn);
																	mainRow
																			.append(previousColumn);
																	mainRow
																			.append(changeColumn);

																	secondColumn = false;
																	body
																			.append(mainRow);
																}
															}
															if (sort == false)
																answerCol
																		.attr(
																				'rowspan',
																				filtersCount
																						- insufficientCount);
															brandRowSpan += filtersCount
																	- insufficientCount;
															if (filtersCount
																	- insufficientCount != 0
																	&& (filtersCount == 1)
																	&& sort == false) {
																filterColumn
																		.before(answerCol);
															}
															Site.filtersStartingIndex += ((filtersCount));
															if (secondColumn == false
																	|| sort == true)
																firstColumn = false;
															Site.filtersLastIndex += filtersCount;
														}

														if (sort == false)
															brandCol
																	.attr(
																			'rowspan',
																			brandRowSpan);

														Site.filtersStartingIndex = Site.filtersLastIndex;

													});
								}
								var divId = 0;
								table.append(body);

								if (body.html() == "") {
									body
											.append("<tr><td colspan='6'> Insufficient data please select other filters or click on show hidden data</td></tr>");
								}
								Site.startingIndex += Site.data.counts[Site.mainFlag - 1];
								if (Site.mainFlag == 1) {
									table.addClass("blue-table");
									divId = 1;
									table.attr("id", "Q1T");
								} else if (Site.mainFlag == 2) {
									divId = 2;
									table.attr("id", "Q2T");
									table.addClass("green-table");
									table.removeClass("blue-table");
								} else if (Site.mainFlag == 3) {
									divId = 3;
									table.attr("id", "Q3T");
									table.addClass("red-table");
									table.removeClass("green-table");
									table.removeClass("blue-table");
								}

								$('#question-table' + divId).append(
										questionName);
								$('#question-table' + divId).append(
										questionNameNotBuzz);

								$('#question-table' + divId).append(
										currFilterHeader);
								$('#question-table' + divId).append(table);
								$('#question-table' + divId).append("<br/>");
								if (sort == true) {
									table.tablesorter({
										sortList : sorting,
										headers : {
											0 : {
												sorter : false
											},
											1 : {
												sorter : false
											}
										// ,
										// 2 : {
										// sorter : false
										// }
										}
									});
								}
							}
							Site.mainFlag++;
						});

		if (hideDataCheck == true)
			$(".hidden-data").hide();
		else
			$(".hidden-data").show();
	},

	drawTables : function(sort, sorting, tableIndex, showHidden) {

		Site.mainFlag = 1;
		Site.startingIndex = 0;
		var hideDataCheck = true;
		if (tableIndex == null) {
			$('.question-tables').empty();
		} else {
			$("#question-table" + tableIndex.charAt(1)).empty();
		}
		// for ( var i = 1; i < 4; i++) {
		// if (tableIndex == null)
		// $('#Questions-table' + i).empty();
		// else if (parseInt(tableIndex.charAt(1)) == i) {
		// $('#Questions-table' + i).empty();
		// }
		// }
		$('.question-header')
				.each(
						function() {

							if (tableIndex == null
									|| (tableIndex != null && parseInt(tableIndex
											.charAt(1)) == Site.mainFlag)) {
								var compare = false;
								var questionName = "<h6><b>"
										+ Site.escapeHtml($(this).text())
										+ "</b></h6>";
								var questionNameNotBuzz = "<h6 class='table-question-name'>"
										+ Site
												.escapeHtml(Site.waveData.questions[this.id
														.replace('question', '')].name)
										+ "</h6>";
								var tableScrollDiv = $("<div class='scroll-pane horizontal-only'></div>");
								var table = $("<table  class='tablesorter to-export'> </table>");
								tableScrollDiv.append(table);
								var thead = $("<thead></thead>");
								var rowHead = $("<tr> </tr>");
								var brandClass = "brand-header";
								var filterClass = "filter-header";
								var colHead1 = $("<th style='cursor: pointer' class='"
										+ brandClass + "' >Brand </th>");
								var colHead2 = $("<th style='cursor: pointer' class='"
										+ filterClass + "'>Filter</th>");
								var body = $("<tbody></tbody>");
								rowHead.append(colHead1);
								rowHead.append(colHead2);

								var filtersCount = 0;
								var currFilterText = "";
								$('.division-item.selected:not(".sum-item")')
										.each(
												function() {
													if ($(this).text() != "Compare")
														currFilterText += Site
																.escapeHtml($(
																		this)
																		.text())
																+ ", ";
													else {
														compare = true;
														filtersCount = $(this)
																.siblings().length - 1;
														currFilterText += "All sub-filters of "
																+ Site
																		.escapeHtml($(
																				this)
																				.parent()
																				.prev(
																						"h4")
																				.text())
																+ ", ";
													}

												});
								var answerClass = "answer";
								if (compare && sort == false)
									answerClass = "answer-header";
								// if(tableIndex!=null &&
								// parseInt(tableIndex.charAt(1))==Site.mainFlag)
								// answerClass="answer";
								var answerIndexInTable = 2;
								for (var j = Site.startingIndex; j < (Site.startingIndex + Site.data.counts[Site.mainFlag - 1]); j++) {
									if (Site.data.answers[j].length > 60)
										var tempCol = $("<th  style='cursor: pointer;min-width:250px;word-wrap: break-word;' class='"
												+ answerClass
												+ "' id='answer-"
												+ answerIndexInTable
												+ "'> </th>");
									else if (Site.data.answers[j].length > 40)
										var tempCol = $("<th  style='cursor: pointer;min-width:200px;word-wrap: break-word;' class='"
												+ answerClass
												+ "' id='answer-"
												+ answerIndexInTable
												+ "'> </th>");
									else if (Site.data.answers[j].length > 30)
										var tempCol = $("<th  style='cursor: pointer;min-width:150px;word-wrap: break-word;' class='"
												+ answerClass
												+ "' id='answer-"
												+ answerIndexInTable
												+ "'> </th>");
									else if (Site.data.answers[j].length > 20)
										var tempCol = $("<th  style='cursor: pointer;min-width:100px;word-wrap: break-word;' class='"
												+ answerClass
												+ "' id='answer-"
												+ answerIndexInTable
												+ "'> </th>");
									else
										var tempCol = $("<th  style='cursor: pointer;' class='"
												+ answerClass
												+ "' id='answer-"
												+ answerIndexInTable
												+ "'> </th>");
									answerIndexInTable++;
									tempCol.text(Site.data.answers[j]);
									rowHead.append(tempCol);
								}
								thead.append(rowHead);
								table.append(thead);

								currFilterText = currFilterText.substring(0,
										currFilterText.length - 2);
								var brandIndexCount = -1;
								if (!compare) {
									var answersSkipForCurrentLocationCount = 0;
									var checkedLocations = new Array();
									if ($("#location-select option:checked").length > 1) {
										$("#location-select option:checked")
												.each(
														function(index, element) {
															checkedLocations
																	.push($(
																			element)
																			.text());
														});
									}
									for (var countryIter = 0; countryIter < $("#location-select option:checked").length; countryIter++) {
										var currentLocation = "";
										if ($("#location-select option:checked").length > 1) {
											var currentLocation = ", "
													+ Site
															.escapeHtml(checkedLocations[countryIter]);
										}
										brandIndexCount = -1;
										$('.brand-input:checked:visible')
												.each(
														function() {

															var row = $("<tr> </tr>");
															var col1 = $("<td>"
																	+ $(this)
																			.parent()
																			.text()
																	+ currentLocation
																	+ "</td>");
															var col2 = "";
															if (currFilterText.length > 0)
																col2 = $("<td>"
																		+ currFilterText
																		+ "</td>");
															else
																col2 = $("<td>No Filters</td>");

															row.append(col1);
															row.append(col2);
															brandIndexCount++;

															var insufficientCheck = true;
															var zeroCheck = true;
															if (showHidden == false) {
																$(
																		".hidden-check")
																		.text(
																				"Show hidden data");
																for (var j = Site.startingIndex; j < (Site.startingIndex + Site.data.counts[Site.mainFlag - 1]); j++) {
																	if (Site.data.values[j
																			+ answersSkipForCurrentLocationCount][brandIndexCount] != -1000) {
																		insufficientCheck = false;
																	}
																	if (Site.data.values[j
																			+ answersSkipForCurrentLocationCount][brandIndexCount] != 0) {
																		zeroCheck = false;
																	}
																}
															} else if (showHidden == true) {
																$(
																		".hidden-check")
																		.text(
																				"Hide insufficient data");
																insufficientCheck = false;
																zeroCheck = false;
																hideDataCheck = false;
															}
															if (showHidden == false
																	&& !(zeroCheck == false && insufficientCheck == false)) {
																hideDataCheck = false;
															}
															if (zeroCheck == false
																	&& insufficientCheck == false) {
																for (var j = Site.startingIndex; j < (Site.startingIndex + Site.data.counts[Site.mainFlag - 1]); j++) {
																	if (Site.data.values[j
																			+ answersSkipForCurrentLocationCount][brandIndexCount] == -1000) {
																		var tempCol = $("<td>"
																				+ "Insufficient data"
																				+ "</td>");
																	} else {
																		var tempCol = $("<td>"
																				+ Site.data.values[j
																						+ answersSkipForCurrentLocationCount][brandIndexCount]
																				+ "%</td>");
																	}
																	row
																			.append(tempCol);

																}

																body
																		.append(row);
															}

														});

										answersSkipForCurrentLocationCount += Site.data.counts[countryIter];
									}
								} else {
									var brandCount = 0;
									var brandIndex = 0;
									$('.brand-input:checked:visible')
											.each(
													function() {
														var brandMainRow = $("<tr > </tr>");
														var brandColumn = "";

														if (sort == false) {
															brandColumn = $("<td rowspan= '"
																	+ filtersCount
																	+ "'>"
																	+ $(this)
																			.parent()
																			.text()
																	+ "</td>");
															brandMainRow
																	.append(brandColumn);
														}

														var firstFilter = true;
														var brandRowspan = 0;
														for (var j = 0; j < filtersCount; j++) {
															if (!firstFilter) {
																brandMainRow = $("<tr></tr>");
															}

															var insufficientCheck = true;
															var zeroCheck = true;

															if (showHidden == false) {
																$(
																		".hidden-check")
																		.text(
																				"Show hidden data");
																for (var k = Site.startingIndex; k < (Site.startingIndex + Site.data.counts[Site.mainFlag - 1]); k++) {
																	if (Site.data.values[k][j
																			+ brandIndex] != -1000) {
																		insufficientCheck = false;
																	}
																	if (Site.data.values[k][j
																			+ brandIndex] != 0) {
																		zeroCheck = false;
																	}
																}
															} else if (showHidden == true) {
																$(
																		".hidden-check")
																		.text(
																				"Hide insufficient data");
																insufficientCheck = false;
																zeroCheck = false;
																hideDataCheck = false;
															}

															if (showHidden == false
																	&& !(zeroCheck == false && insufficientCheck == false)) {
																hideDataCheck = false;
															}

															if (sort == true) {
																brandColumn = $("<td>"
																		+ $(
																				this)
																				.parent()
																				.text()
																		+ "</td>");
																brandMainRow
																		.append(brandColumn);
															}
															if (zeroCheck == false
																	&& insufficientCheck == false)

															{
																tempCol = $("<td>"
																		+ Site.data.divisions[j]
																		+ "</td>");
																brandMainRow
																		.append(tempCol);

																for (var k = Site.startingIndex; k < (Site.startingIndex + Site.data.counts[Site.mainFlag - 1]); k++) {
																	if (Site.data.values[k][j
																			+ brandIndex] == -1000) {
																		var answerCol = $("<td>"
																				+ "Insufficient data"
																				+ "</td>");
																	} else {
																		var answerCol = $("<td>"
																				+ Site.data.values[k][j
																						+ brandIndex]
																				+ "%</td>");
																	}
																	// brandColumn
																	// .attr(
																	// 'rowspan',
																	// 1);
																	brandMainRow
																			.append(answerCol);
																}

																body
																		.append(brandMainRow);
																firstFilter = false;
															} else {
																brandRowspan++;
															}
														}
														brandIndex += filtersCount;
														brandCount++;
														if (!showHidden
																&& sort == false) {
															brandColumn
																	.attr(
																			'rowspan',
																			filtersCount
																					- brandRowspan);
														}
													});
								}
								table.append(body);
								if (body.html() == "") {
									body
											.append("<tr><td colspan='"
													+ (Site.data.counts[Site.mainFlag - 1] + 2)
													+ "'> Insufficient data please select other filters or click on show hidden data</td></tr>");
								}
								var currFilter = $("<p class='maia-meta'></p>");
								var divId = 0;
								if (currFilterText.length > 0)
									currFilter.append(currFilterText);
								else
									currFilter.append("No Filters");
								Site.startingIndex += Site.data.counts[Site.mainFlag - 1];
								if (Site.mainFlag == 1) {
									divId = 1;
									table.addClass("blue-table");
									table.attr("id", "Q1T");
								} else if (Site.mainFlag == 2) {
									divId = 2;
									table.addClass("green-table");
									table.removeClass("blue-table");
									table.attr("id", "Q2T");
								} else if (Site.mainFlag == 3) {

									table.addClass("red-table");
									table.removeClass("green-table");
									table.removeClass("blue-table");
									table.attr("id", "Q3T");
									divId = 3;
								}
								$('#question-table' + divId).append(
										questionName);
								$('#question-table' + divId).append(
										questionNameNotBuzz);
								$('#question-table' + divId).append(currFilter);
								$('#question-table' + divId).append(
										tableScrollDiv);
								$('#question-table' + divId).append("<br/>");

								$(function() {
									$('.scroll-pane')
											.each(
													function() {
														$(this)
																.jScrollPane(
																		{
																			showArrows : $(
																					this)
																					.is(
																							'.arrow')
																		});
														var api = $(this).data(
																'jsp');
														var throttleTimeout;
														$(window)
																.bind(
																		'resize',
																		function() {
																			if ($.browser.msie) {

																				if (!throttleTimeout) {
																					throttleTimeout = setTimeout(
																							function() {
																								api
																										.reinitialise();
																								throttleTimeout = null;
																							},
																							50);
																				}
																			} else {
																				api
																						.reinitialise();
																			}
																		});
													})

								});
								$(window).resize(
										function() {
											var element = $('.scroll-pane')
													.jScrollPane({});
											var api = element.data('jsp');
											api.destroy();
											$('.scroll-pane').jScrollPane();
										});

								if (!compare)
									try {
										table.tablesorter({
											headers : {
												0 : {
													sorter : false
												},
												1 : {
													sorter : false
												}
											}
										});
									} catch (e) {
									}
								if (sort == true) {
									try {
										table.tablesorter({
											sortList : sorting,
											headers : {
												0 : {
													sorter : false
												}
											// ,
											// 1 : {
											// sorter : false
											// }
											}
										});
									} catch (e) {
									}
								}
							}
							Site.mainFlag++;

						});
		if (hideDataCheck == true)
			$(".hidden-data").hide();
		else
			$(".hidden-data").show();
	},

	/**
	 * Select Box
	 */
	Selectbox : function() {
		$(".regions-select, .type-select, .trending-select").selectbox();
		$("#export-select").selectbox({
			onChange : function(val, inst) {
				if (val == 1) {
					$('.drive-notification').show();
					$('#export-file').height('300px')
				} else {
					$('.drive-notification').hide();
					$('#export-file').height('220px')
				}
			}
		});

		/* with custom checkbox Multi */
		$(".select-with-checkbox").multiselect(
				{
					header : true,
					close : function(event, ui) {
						// event handler here
						Site.singleDimensionChanges("location");
						Site.createRequest();
					},
					click : function(event, ui) {
						if ($(".select-with-checkbox")
								.multiselect("getChecked").length > 0
								&& $(".select-with-checkbox").multiselect(
										"getChecked").length < 4) {
							$(
									'#location-select option[value="'
											+ ui.value + '"]').attr('selected',
									ui.checked);
						} else {
							event.preventDefault();
						}
					}
				});
	},

	/**
	 * Select Box Detach
	 */
	selectboxDetach : function() {
		$(".regions-select, .type-select, .trending-select")
				.selectbox('detach');

		// /* with custom checkbox Multi */
		$(".select-with-checkbox").multiselect("destroy");
	},

	/**
	 * Setting
	 */
	Setting : function() {
		$('.setting-icon').live('click', function() {
			if ($('.setting-content').css('display') == 'none') {
				$('.setting-content').slideDown('medium');
			} else {
				$('.setting-content').slideUp('medium');
			}
		});
		// $('.setting-icon').toggle(function() {
		// $('.setting-content').slideDown('medium');
		//
		// }, function() {
		// $('.setting-content').slideUp('medium');
		// });
		$(document).live(
				"click",
				function(elem) {

					if (elem.srcElement != null
							&& elem.srcElement.className != "s-icon"
							&& elem.srcElement.className != "setting-icon"
							&& elem.srcElement.className != "setting-arrow") {
						$('.setting-content').slideUp('medium');
					}
				});
	},

	/**
	 * Slide SideBar
	 */
	SlideSidebar : function() {
		$('.slide-button').click(function() {
			$('.left-side').slideDown('slow').hide('slow');
			$('.right-side').css('margin-left', '0').css('padding-left', '1%');
		});
	},

	/**
	 * Auto Launch
	 */

	AutoLaunch : function() {
		Site.hasDefaultCountry = false;
		var i, x, y, ARRcookies = document.cookie.split(";");
		for (i = 0; i < ARRcookies.length; i++) {
			x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
			y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
			x = x.replace(/^\s+|\s+$/g, "");
			var foundVideoCookie = false;
			if (x == ("location_" + $('#UI').text())) {
				Site.hasDefaultCountry = true;
				Site.defaultCountryId = y;
			}
		}
		if (!Site.hasDefaultCountry) {
			alert("Site dosen't have default country")
			var query = $.get("/site/checkDefaultCountry", function(data) {

				if (window.location.href.indexOf("site/dashboard") != -1) {
					if (data.showVideo != null) {
						if (data.showVideo == true) {
							$('#notification-video').slideDown();
						}
					}
				}
				if (data.country != null) {
					Site.defaultCountryId = data.country;
					Site.hasDefaultCountry = true;
				} else {
					$.fancybox({
						content : $("#Country-popup"),
						closeClick : false,
						openEffect : 'none',
						closeEffect : 'none',
						helpers : {
							overlay : {
								closeClick : false,
							}
						}
					});
					Site.getCountriesToSetDefault();
				}
			}, "json");

			// window.location="#Country-popup";

		}

	},

	checkVideoCookie : function() {
		var i, x, y, ARRcookies = document.cookie.split(";");
		for (i = 0; i < ARRcookies.length; i++) {
			x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
			y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
			x = x.replace(/^\s+|\s+$/g, "");
			var foundVideoCookie = false;
			if (x == ("showVideo_" + $('#UI').text())) {
				if (y == "true") {
					foundVideoCookie = true;
					$('#notification-video').slideDown();
				}
				foundVideoCookie = true;
			}

		}
	},
	closeDropDown : function() {

		$('body').live('click', function(event) {

			Site.hideQuestionListsDropDowns();

			Site.hideAnsGroupsListsDropDowns();

		});

	},

	hideQuestionListsDropDowns : function() {
		$('.intro-holder:has(".expand-items:visible") .question-title').click();
	},

	hideAnsGroupsListsDropDowns : function() {
		$('.intro-holder:has(".expand-groups-items:visible") .group-data')
				.click();
	},

	/**
	 * Popup
	 */
	Popup : function() {
		$('.popuplink').fancybox({
			helpers : {
				title : null
			}
		// ,
		// afterShow : function() {
		// if ($('#widgets-scrollbar').length > 0) {
		// $('#widgets-scrollbar').tinyscrollbar();
		// }
		// }

		});
	},

	/**
	 * Popup
	 */
	popupShareDashboard : function() {

		$('.remove-email').live(
				'click',
				function() {
					var dashboardId = $('#dashboards li.active').find(
							'input.id').val();
					var uid = $('#dashboards li.active').find('input.userId')
							.val();
					var email = $(this).siblings('.user-mail-share').text();
					var that = this;

					$.ajax({
						url : "dashboard/" + dashboardId + "/remove",
						data : {
							email : email,
							uid : uid
						},
						success : function() {
							$(that).parents('li').hide('medium', function() {
								$(this).remove();
							});
						},
						cache : false,
						type : "post",
						dataType : "json"
					});
				});

		$('.shareDashBoard')
				.fancybox(
						{
							beforeLoad : function() {
								alert("share dashboard")
								var dashboardId = $('#dashboards li.active')
										.find('input.id').val();
								$('#share-dashboard-form').find('.hidden-id')
										.val(dashboardId);

								$
										.get(
												"/site/dashboard/"
												// "/site/studyDashboard/"
												+ dashboardId + "/access",
												function(data) {
													// console.log(data.users);
													$('#users-with-access')
															.empty();
													$(data.users)
															.each(
																	function() {
																		$(
																				'#users-with-access')
																				.append(
																						"<li><span class='user-mail-share'>"
																								+ Site
																										.escapeHtml(this)
																								+ "</span>&nbsp;&nbsp;<a class='remove-email' href='#'>X</a</li>");
																	});

													$('#users-scrollbar')
															.tinyscrollbar();
												}, "json");
							}
						});
	},

	/**
	 * Tabs
	 */
	Tabs : function() {
		/*
		 * $(".tab-content").hide(); $("ul#tabs li:first").addClass("active");
		 * $(".tab-content:first").show(); $("ul#tabs li").click(function() {
		 * $("ul#tabs li").removeClass("active"); $(this).addClass("active")
		 * $(".tab-content").hide(); var activeTab =
		 * $(this).find("a").attr("href"); $(activeTab).fadeIn(); return false;
		 * });
		 */
	},

	/**
	 * Scroll Bar
	 */
	ScrollBar : function() {
		$('#scrollbar1').tinyscrollbar();
		$('.dashboard .expand-list').tinyscrollbar();

	},

	/**
	 * Expand & colapse
	 */
	expand : function() {
		$('.left-side-form .filter-slider h3').toggle(
				function() {
					$(this).parents('.filter-slider').find('.expand-list')
							.slideUp('fast');
					$(this).find('a.expand').addClass('colapse');
				},
				function() {
					$(this).parents('.filter-slider').find('.expand-list')
							.slideDown('fast');
					$(this).find('a.expand').removeClass('colapse');
				});
		$('.filter-slider > h4').toggle(

				function() {
					$(this).parent().find('.filters-list.filters-expand-list')
							.slideDown('fast');
					$(this).find('a.expand').removeClass('colapse');
				},
				function() {
					$(this).parent().find('.filters-list.filters-expand-list')
							.slideUp('fast');
					$(this).find('a.expand').addClass('colapse');
				});
	},

	/**
	 * Tablet Select
	 */

	tableselect : function() {
		$('.filter-slider td').toggle(function() {
			$(this).addClass('selected');
		}, function() {
			$(this).removeClass('selected');
		});
	},

	/**
	 * Arrow Down for Title
	 */

	ArrowDown : function() {

		$('.intro-holder .question-title').live('click', function() {
			$(this).toggle(function() {
				$('.expand-items').hide();
				$('.expand').removeClass('colapse');
				$(this).parents('.intro-holder').find('.expand-items').show();
				$(this).find('.expand').addClass('colapse');
			}, function() {
				$(this).parents('.intro-holder').find('.expand-items').hide();
				$(this).find('.expand').removeClass('colapse');
			});
			$(this).trigger('click');
			return false
		});

		$('.intro-holder .group-data')
				.live(
						'click',
						function() {
							$(this)
									.toggle(
											function() {
												$('.expand-groups-items')
														.hide();
												$('.group-expand').removeClass(
														'colapse');
												$(this)
														.parents(
																'.intro-holder')
														.find(
																'.expand-groups-items')
														.show();
												$(this)
														.parents(
																'.intro-holder')
														.find(
																'.expand-groups-items')
														.css(
																'left',
																$(this)
																		.parents(
																				'.intro-holder')
																		.find(
																				'.group-data')
																		.position().left);

												$(this).find('.group-expand')
														.addClass('colapse');
											},
											function() {
												$(this)
														.parents(
																'.intro-holder')
														.find(
																'.expand-groups-items')
														.hide();
												$(this).find('.group-expand')
														.removeClass('colapse');
											});
							$(this).trigger('click');
							return false
						});
	},

	/**
	 * Initialise history
	 */
	setHistory : function() {

		// Check Location
		if (document.location.protocol === 'file:') {
			alert('The HTML5 History API (and thus History.js) do not work on files, please upload it to a server.');
		}

		// Establish Variables
		var History = window.History; // Note: We are using a capital H
		// instead of a lower h
		var State = History.getState();

		// Bind to State Change
		History.Adapter.bind(window, 'statechange', function() { // Log the

			// Site.hideLoadingOverlay();
			// State
			var State = History.getState();
			// if (History.getState().data.init) {
			// Note: We are
			// using
			// History.getState() instead of
			// event.state
			// History.log('statechange:', State.data,
			// State.title, State.url);
			// History.log('Location:' + location.search);

			Site.data = History.getState().data.data;
			Site.waveData = History.getState().data.waveData;
			Site.urlSplit = Site.splitUrl(location.href);
			Site.brands = History.getState().data.brands;
			// Site.initWaveRelatedData();

			// if ((Site.wave != History.getState().data.wave &&
			// Site.secondWave
			// != null)
			// || (Site.wave != History.getState().data.wave &&
			// Site.secondWave
			// != History
			// .getState().data.secondWave)) {

			// Site.initComponents(createRequestForWaveData = false);
			// } else {
			// Site.initComponentsCallback();
			// }
			Site.wave = $('#wave-select').val();
			Site.secondWave = $('#second-wave-select').val();
			// Site.initCompareQuestions();
			// Site.drawCharts();
			// }

			e = EventBus.getInstance();

			Site.initWaveRelatedData();
			e.fireEvent('initSelectedFilters');
			Site.selectboxDetach();
			Site.Selectbox();
			e.fireEvent('initQuestions');

			e.fireEvent('initBrands', {
				callbackFunction : function() {

					Site.drawCharts();
				},
				createRequest : false
			});

		});
	},

	splitUrl : function(uri) {
		var splitRegExp = new RegExp('^' + '(?:' + '([^:/?#.]+)' + // scheme -
		// ignore
		// special
		// characters
		// used by other URL parts such as :,
		// ?, /, #, and .
		':)?' + '(?://' + '(?:([^/?#]*)@)?' + // userInfo
		'([\\w\\d\\-\\u0100-\\uffff.%]*)' + // domain - restrict to letters,
		// digits, dashes, dots, percent
		// escapes, and unicode characters.
		'(?::([0-9]+))?' + // port
		')?' + '([^?#]+)?' + // path
		'(?:\\?([^#]*))?' + // query
		'(?:#(.*))?' + // fragment
		'$');

		var split;
		split = uri.match(splitRegExp);
		queryData = split[6];
		queryDataOut = new Object();
		brands = new Array();
		divisions = new Array();
		filters = new Array();
		compareQuestions = new Array();
		answerGroupsByQuestion = new Object();

		if (queryData != null) {
			queryData = queryData.split('&');
			var wave;
			var secondWave;
			var locs = new Array();
			var mainQuestion;
			for (var i = 0; i < queryData.length; i++) {
				queryData[i] = decodeURIComponent(queryData[i]);
				item = queryData[i].split("=");
				if (item[0] == 'b') {
					brands.push(item[1]);
				} else if (item[0] == 'fd') {
					var filterDivision = item[1].split('_');
					filters.push(filterDivision[0]);
					divisions.push(filterDivision[1]);
				} else if (item[0] == 'w') {
					wave = item[1];
				} else if (item[0] == 'ws') {
					secondWave = item[1];
				} else if (item[0] == 'l') {
					locs.push(item[1]);
				} else if (item[0] == 'q') {
					mainQuestion = item[1];
				} else if (item[0] == 'qs') {
					compareQuestions.push(item[1]);
				} else if (item[0] == 'ag') {
					var groupQuestion = item[1].substring(0, item[1]
							.indexOf('_'));
					answerGroupsByQuestion[groupQuestion] = item[1]
							.substring(item[1].indexOf('_') + 1);
				} else {
					queryDataOut[item[0]] = item[1];
				}
			}
		}
		return {
			'scheme' : split[1],
			'user_info' : split[2],
			'domain' : split[3],
			'port' : split[4],
			'path' : split[5],
			'query_data' : queryDataOut,
			'fragment' : split[7],
			'brands' : brands,
			'filters' : filters,
			'divisions' : divisions,
			'wave' : wave,
			'secondWave' : secondWave,
			'location' : locs,
			'mainQuestion' : mainQuestion,
			'compareQuestions' : compareQuestions,
			'answerGroupsByQuestion' : answerGroupsByQuestion
		};
	},

	/**
	 * Initialise filters and divisions using current url
	 */
	initFilters : function() {
		// window.console.log(Site.splitUrl(location.href));
		var urlSplit = Site.splitUrl(location.href);
		$('.division-item').removeClass('selected');
		$(urlSplit.filters).each(
				function(index, element) {
					$('#filter' + element + ' .division-item').removeClass(
							'selected');
					if (divisions[index] == 's') {
						$('#filter' + element).find('.sum-item').addClass(
								'selected');
					} else if (divisions[index] == 'c') {
						$('#filter' + element).find('.compare-item').addClass(
								'selected');
					} else {
						$('#filter' + element).find(
								'#division' + divisions[index]).addClass(
								'selected');
						$('#filter' + element).find('#' + divisions[index])
								.addClass('selected');
					}
				});

		$('.brand-input').removeAttr('checked');
		$(urlSplit.brands).each(
				function(index, element) {
					$('.brand-input[value="' + element + '"]').attr('checked',
							'checked');
				});

		$('#wave-select').val(urlSplit.wave);
		if (urlSplit.secondWave != null) {
			$('#second-wave-select').val(urlSplit.secondWave);
		}

		$(urlSplit.location).each(
				function() {
					$('#location-select option[value="' + this + '"]').attr(
							'selected', true);
				});

		Site.selectboxDetach();
		Site.Selectbox();

	},

	initCompareQuestions : function() {
		var urlSplit = Site.splitUrl(location.href);
		var compareQuestions = urlSplit.compareQuestions;
		$('.compare-question-holder .compare-question').each(
				function() {
					$('.questions-list').append(
							'<li id="' + this.id
									+ '"><a href="#" class="question-item">'
									+ $(this).text() + '</a></li>');
					// $(this).addClass('disabled');

				});

		$('.compare-question-holder').remove();
		$(compareQuestions).each(function(index, element) {
			Site.addCompareQuestion(this);
		});

	},

	drawCharts : function() {
		
		Site.showLegend = true;
		if (window.location.href.indexOf("site/trend") !== -1) {
			Site.drawTrendCharts();
			Site.drawTrendTables(false, null, null, false);
		} else if (window.location.href.indexOf("site/snapshot") !== -1) {
			Site.drawSnapshotCharts();
			// Site.drawSnapshotForExport();
			Site.drawTables(false, null, null, false);
		}
	},

	drawSnapshotForExport : function(forDrive) {
		alert("drawSnapShotForExport")
		result = Site.data;
		if (result.answers.length != 0) {

			series = new Array();
			var startingIndex = 0;
			var updatedAnswersCount = new Array();

			filteredValues = new Array();
			filteredBrandsFilters = new Array();
			filteredQuestionsAnswers = new Array();

			for (var i = 0; i < result.valuesForChart.length; i++) {
				filteredValues.push(new Array());
			}

			for (var i = 0; i < result.valuesForChart[0].length; i++) {
				var zeroCheck = 0;
				for (var j = 0; j < result.valuesForChart.length; j++) {
					zeroCheck += result.valuesForChart[j][i];
				}
				if (zeroCheck != 0) {
					// for ( var k = 0; k < result.valuesForChart.length; k++) {
					// filteredValues[k].push(result.valuesForChart[k][i]);
					// emptyChart = true;
					//
					// }
					// filteredBrandsFilters.push(result.brandsAndFilters[i]);

					for (var k = 0; k < result.valuesForChart.length; k++) {
						if (result.values[k][i] == -1000) {
							filteredValues[k].push(null);
						} else
							filteredValues[k].push(result.valuesForChart[k][i]);
						emptyChart = true;

					}
					filteredBrandsFilters.push(result.brandsAndFilters[i]);
				}
			}
			for (var i = 0; i < filteredValues.length; i++) {
				var zeroCheck = 0;
				for (var j = 0; j < filteredValues[0].length; j++) {
					zeroCheck += filteredValues[i][j];
				}
				if (zeroCheck == 0)
					filteredQuestionsAnswers.push(i);
			}

			for (var j = 0; j < result.counts.length; j++) {
				var noData = true;
				for (var k = startingIndex; k < (startingIndex + result.counts[j]); k++) {
					if ($.inArray(k, filteredQuestionsAnswers) == -1) {
						if ($('#location-select option:checked').length > 1) {
							seriesName = $(
									'#location-select option:checked:eq(' + j
											+ ')').text();
						} else {
							seriesName = '' + $('.Q' + j).text();
						}
						series.push({
							name : seriesName,
							data : null,
							stack : j,
							showInLegend : true,
							color : '#FFFFFF',
						// groupPadding : 0.001
						});
						noData = false;
						break;
					}
				}
				if (!noData) {
					var currentAnswersCount = result.counts[j];
					var skipper = Math.floor(19 / currentAnswersCount);
					var colorIndex = -skipper;

					for (var i = startingIndex; i < (startingIndex + result.counts[j]); i++) {

						colorIndex += skipper;
						var showInLegend = true;
						if ($.inArray(i, filteredQuestionsAnswers) != -1) {
							showInLegend = false;
							currentAnswersCount--;
							colorIndex -= skipper;
						}
						if ((colorIndex >= Site.colorPallete[j % 3].length || colorIndex < 0)
								&& colorIndex % 2 == 0)
							colorIndex = 1;
						else if ((colorIndex >= Site.colorPallete[j % 3].length || colorIndex < 0)
								&& colorIndex % 2 != 0)
							colorIndex = 0;
						series.push({
							name : result.answers[i],
							data : filteredValues[i],
							stack : j,
							showInLegend : showInLegend,
							// color :"#80c8a3"
							color : Site.colorPallete[j % 3][colorIndex]
						});

					}
					updatedAnswersCount.push(currentAnswersCount);
				}
				startingIndex += result.counts[j];
			}
			series.reverse();
			if (series[0] != null) {
				var sizeOfBars = series.length * series[0].data.length;

				Site.size = 150 + (sizeOfBars * 15);
				var strgSize = Site.size.toString();
				if (Site.bars == null || Site.bars == false) {
					if (result.counts.length > 1) {
						var tempSize = result.counts.length * 40 + 150;
						strgSize = tempSize.toString();
					} else {
						var tempSize = filteredBrandsFilters.length * 40 + 150;
						strgSize = tempSize.toString();
					}
				}
			} else
				strgSize = null;
			Site.chartOptionsForExport = {
				chart : {
					renderTo : 'container2',
					type : 'bar',
					height : strgSize * 3,
					events : {
						load : function(event) {

							Site.deselectSeriesFromExportChart(this);

							var serializer = new XMLSerializer();
							var svg = $('.export-div svg').clone();
							var svnStr = serializer.serializeToString(svg[0]);

							var svgWidth = svg.attr('width') + 200;
							svg.attr('width', svgWidth);
							var svgHeight = svg.attr('height');

							var canv = $('<canvas id="canv"></canvas>');
							canv.hide();
							canv.attr('width', svgWidth + 'px');
							canv.attr('height', svgHeight + 'px');
							canv.appendTo('body');

							canvg(canv[0], svnStr);
							var data = document.getElementById('canv')
									.toDataURL();

							canv.remove();

							var imageDataInput = $('<input name="svg" type="hidden" />');

							var date = new Date();

							var filenNameInput = $('<input name="fn" type="hidden" value="export-'
									+ date.getDate()
									+ '-'
									+ (date.getMonth() + 1)
									+ '-'
									+ date.getFullYear() + '" />');
							imageDataInput.val(data);

							if (!forDrive) {
								var form = $('<form style="display:none;"></form>');
								form.append(imageDataInput).append(
										filenNameInput).attr('action',
										'/site/exportpng').attr('method',
										'post');
								var token = $('#_tk').val();
								form
										.append("<input type='hidden' name='tk' value='"
												+ Site.escapeHtml(token)
												+ "' />");

								form.appendTo('body');
								form.submit();

								// form.attr('action', '');
								form.remove();
								parent.$.fancybox.close();
							} else {
								var form = $('#exporting-form');
								form.attr('action', '/site/exportdrive');
								form.append(imageDataInput);
								// form
								// .append("<input type='hidden' name='link'
								// value='"
								// + location.pathname
								// + location.search + "'/>");
								form.append(imageDataInput).append(
										filenNameInput);
								var loadingImage = $('<img class="loader-image" src="/site_ui/images/ajax-loader.gif" width="31" height="31"/>');
								loadingImage.insertBefore($('.export-chart'));
								Site.preExport();
								$
										.post(
												$(form).attr('action'),
												$(form).serialize(),
												function(data) {
													if (data != null) {

														if (data.redirect != null) {
															// location.href =
															// data.redirect;
															window
																	.open(
																			data.redirect,
																			"",
																			"width=600,height=400,left=10,top=0");
														} else {

															Site
																	.showNotification(
																			"Export to Google Drive is complete.  You can access the exported files <a target='_BLANK' href='"
																					+ Site
																							.escapeHtml(data.folderLink)
																					+ "'>here</a>",
																			showUndo = false);

															parent.$.fancybox
																	.close();
														}
													}
													// else {
													// Site
													// .showNotification(
													// "Export to google drive
													// is complete. You can
													// access the exported files
													// <a
													// target='_BLANK' href='"
													// + Site
													// .escapeHtml(data.folderLink)
													// + "'>here</a>",
													// showUndo = false);

													// parent.$.fancybox
													// .close();
													// }
													loadingImage.remove();
												}, "json");

							}

							imageDataInput.remove();
							filenNameInput.remove();

						}
					}

				},
				title : {
					text : ''
				},
				credits : {
					enabled : false
				},
				exporting : {
					enabled : false
				},
				plotOptions : {
					series : {
						stacking : Site.chartOptions.plotOptions.series.stacking,
						shadow : false,
						states : {
							hover : {
								enabled : false
							}
						},
						animation : false
					// animation : {
					// complete : function() {
					//
					//
					// }
					// }
					},
					bar : {
						dataLabels : {
							enabled : true,
							color : 'black',
							style : {
								fontWeight : 'bold'
							},
							// formatter : function() {
							// if (this.percentage > 8
							// || Site.chartOptions.plotOptions.series.stacking
							// != "normal")
							// return this.y + '%';
							// else
							// return null;
							// }
							// ,
							formatter : function() {
								if (this.percentage > 7)
									return this.y + '%';
								else if (this.percentage == null)
									return this.y + '%';
								else
									return null;
							}
						}
					}
				},

				tooltip : {

					enabled : false
				},
				legend : {
					enabled : (emptyChart && Site.showLegend),
					reversed : true,
					layout : 'horizontal',

					labelFormatter : function() {
						if (this.name == ('' + $('.Q0').text())
								|| this.name == ('' + $('.Q1').text())
								|| this.name == ('' + $('.Q2').text()))
							return "<strong>" + this.name + ":</strong><br>";
						else
							return this.name;
					}
				},
				xAxis : {
					groupPadding : 0.2,
					categories : filteredBrandsFilters,
					labels : {
						style : {
							width : 150,
							fontWeight : 'bold'
						}
					}
				},
				yAxis : {
					showLastLabel : (Site.chartOptions.yAxis.max == 110),
					max : Site.chartOptions.yAxis.max,
					title : {
						text : '(%)'
					},

					showEmpty : false

				},

				series : series
			};
			if (Site.chart2 != null) {
				Site.chart2.destroy();
				Site.chart2 = null;
			}
			Site.chart2 = new Highcharts.Chart(Site.chartOptionsForExport);

			var lineHeight = -14;
			var counter = 0;
			var questionIndex = 0;
			var lineCount = 1;
			var questionWidth = 0;
			var questionStartingIndex = 1;
			$('.export-div  .highcharts-legend-item  text')
					.each(
							function() {
								if (counter == questionStartingIndex) {
									questionWidth = parseInt($(this).parent()
											.attr('transform').split(",")[0]
											.substring(10)) - 8;
									lineHeight = lineHeight + 14;
									questionStartingIndex += updatedAnswersCount[questionIndex] + 1;
									questionIndex++;
									lineCount++;
								}
								if (counter == questionStartingIndex - 1) {
									lineHeight = lineHeight + 14;
									lineCount++;
									questionWidth = parseInt($(this).parent()
											.attr('transform').split(",")[0]
											.substring(10)) - 8;
									if ((parseInt($(this).parent().attr(
											'transform').split(",")[0]
											.substring(10)) - 8) != 0)
										lineHeight = lineHeight + 14;
								}
								if ((parseInt($(this).parent()
										.attr('transform').split(",")[0]
										.substring(10)) - 8) == 0) {
									questionWidth = 0;
								}
								if ((parseInt($(this).parent()
										.attr('transform').split(",")[0]
										.substring(10)))
										- questionWidth == 8
										&& counter != questionStartingIndex - 1
										&& counter != questionStartingIndex) {
									lineCount++;
								}
								$(this).attr(
										'transform',
										'translate(' + -questionWidth + ','
												+ lineHeight + ')');
								$(this).next().attr(
										'transform',
										'translate(' + -questionWidth + ','
												+ lineHeight + ')');
								counter++;
							});

			var oldLegendHeight = $('.export-div  .highcharts-legend > rect')
					.attr('height');
			if ($('.export-div .highcharts-legend > g').length > 1) {
				$('.tc-container-exportExport .highcharts-legend > g').last()
						.hide();
			}
			$('.export-div  .highcharts-legend > rect').attr('height',
					(lineCount) * 14);
			var svgHeight = $('.export-div .highcharts-container  svg').attr(
					'height');
			$('.export-div  .highcharts-container svg').height(
					svgHeight - oldLegendHeight + ((lineCount) * 14));
			$('.export-div  .highcharts-container svg > rect').attr('height',
					svgHeight - oldLegendHeight + ((lineCount) * 14));
			$('.export-div  .highcharts-container').height(
					svgHeight - oldLegendHeight + ((lineCount) * 14));
			Site.chartSize = svgHeight - oldLegendHeight
					+ ((lineCount - 1) * 14);
			Site.chartLegendSize = svgHeight - oldLegendHeight
					+ ((lineCount - 1) * 14);
			Site.legendheight = (lineCount - 1) * 14;
		}
	},

	drawSnapshotCharts : function() {
//		alert("drawSnapshotCharts")
		result = Site.data;
		console.log(window.location.href)
		console.log(result);
		if (result.answers.length == 0) {
			$('.no-data').show();
			$('#legend-button').hide();
			$('#bar-to-stacked').hide();
			$('#stacked-to-bar').hide();

			if (Site.chart1 != null) {
				Site.chart1.destroy();
				Site.chart1 = null;
			}
		} else {

			series = new Array();
			var startingIndex = 0;
			var emptyChart = false;
			var updatedAnswersCount = new Array();

			filteredValues = new Array();
			filteredBrandsFilters = new Array();
			filteredQuestionsAnswers = new Array();

			if (result.valuesForChart) {

				for (var i = 0; i < result.valuesForChart.length; i++) {
					filteredValues.push(new Array());
				}
				for (var i = 0; i < result.valuesForChart[0].length; i++) {
					var zeroCheck = 0;
					for (var j = 0; j < result.valuesForChart.length; j++) {
						zeroCheck += result.valuesForChart[j][i];
					}
					if (zeroCheck != 0) {
						for (var k = 0; k < result.valuesForChart.length; k++) {
							if (result.values[k][i] == -1000) {
								filteredValues[k].push(null);
							} else
								filteredValues[k]
										.push(result.valuesForChart[k][i]);
							emptyChart = true;

						}
						filteredBrandsFilters.push(result.brandsAndFilters[i]);
					}
				}
			}

			for (var i = 0; i < filteredValues.length; i++) {
				var zeroCheck = 0;
				for (var j = 0; j < filteredValues[0].length; j++) {
					zeroCheck += filteredValues[i][j];
				}
				if (zeroCheck == 0)
					filteredQuestionsAnswers.push(i);
			}

			if (result.counts) {
				for (var j = 0; j < result.counts.length; j++) {
					var noData = true;
					for (var k = startingIndex; k < (startingIndex + result.counts[j]); k++) {
						if ($.inArray(k, filteredQuestionsAnswers) == -1) {
							if ($('#location-select option:checked').length > 1) {
								seriesName = $(
										'#location-select option:checked:eq('
												+ j + ')').text();
							} else {
								seriesName = '' + $('.Q' + j).text();
							}
							series.push({
								name : seriesName,
								data : null,
								stack : j,
								showInLegend : true,
								color : '#FFFFFF',
							});
							noData = false;
							break;
						}
					}
					if (!noData) {
						var currentAnswersCount = result.counts[j];
						var skipper = Math.floor(19 / currentAnswersCount);
						var colorIndex = -skipper;

						for (var i = startingIndex; i < (startingIndex + result.counts[j]); i++) {

							colorIndex += skipper;
							var showInLegend = true;
							if ($.inArray(i, filteredQuestionsAnswers) != -1) {
								showInLegend = false;
								currentAnswersCount--;
								colorIndex -= skipper;
							}
							if ((colorIndex >= Site.colorPallete[j % 3].length || colorIndex < 0)
									&& colorIndex % 2 == 0)
								colorIndex = 1;
							else if ((colorIndex >= Site.colorPallete[j % 3].length || colorIndex < 0)
									&& colorIndex % 2 != 0)
								colorIndex = 0;
							series.push({
								name : result.answers[i],
								data : filteredValues[i],
								stack : j,
								showInLegend : showInLegend,
								color : Site.colorPallete[j % 3][colorIndex]
							});
						}
						updatedAnswersCount.push(currentAnswersCount);
					}
					startingIndex += result.counts[j];
				}
			}
			series.reverse();
			if (series[0] != null) {
				sizeOfBars = series.length * series[0].data.length;

				Site.size = 150 + (sizeOfBars * 15);
				var strgSize = Site.size.toString();
				if (Site.bars == null || Site.bars == false) {
					if (result.counts.length > 1) {
						var tempSize = result.counts.length * 40 + 150;
						strgSize = tempSize.toString();
					} else {
						var tempSize = filteredBrandsFilters.length * 40 + 150;
						strgSize = tempSize.toString();
					}
				}
			} else {
				strgSize = null;
			}

			Site.chartOptions = {
				chart : {
					renderTo : 'container1',
					type : 'bar',
					height : function() {
						if (Site.size) {

							return Site.size.toString();

						}
						return "180";
					},
					events: {
		                load: function() {
		                    this.myTooltip = new Highcharts.Tooltip(this, this.options.tooltip);
		                }
		            },
				},
				title : {
					text : ''
				},
				credits : {
					enabled : false
				},
				exporting : {
					enabled : false
				},

				plotOptions : {

					series : {
						groupPadding : 0.01,
						// grouping:false,
						// pointWidth: 15 ,
						stacking : null,
						shadow : false,
						states : {
							hover : {
								enabled : false
							}
						},
						events: {
		                    click: function(evt) {
		                    	
//		                    	alert("click");

		                        this.chart.myTooltip.refresh(evt.point, evt);


		                    },
		                    mouseOut: function() {			                    	
//		                    	alert("mouseOut");

//		                        this.chart.myTooltip.hide();
		                    },


		                },
//						allowPointSelect : true,
//						events : {
//							click : function(evt) {
//								alert("click");
//							},
//						}
					// events : {
					//							
					// legendItemClick : function(event) {
					// return !(this.name == ('' + $('.Q0').text())
					// || this.name == ('' + $('.Q1').html()) || this.name ==
					// ('' + $(
					// '.Q2').text()));
					// }
					// }
					},
					bar : {
						dataLabels : {
							enabled : true,
							color : 'black',
							style : {
								fontWeight : 'bold'
							},
							formatter : function() {
								if (this.percentage > 7)
									return this.y + '%';
								else if (this.percentage == null)
									return this.y + '%';
								else
									return null;
							},           


						}
					}
				},

				tooltip : {
					 enabled: false,
			            hideDelay: 0,
			            followPointer: false,
			            shared: false,
			            useHTML: true,
					style : {

						fontWeight : 'bold',
						width : 400
					},
					formatter : function() {
//						 return '<b>' + this.series.name + '</b>:<br/>' +
//						 this.y
//						 + '%';
//						// return result['pointNotes'][0].note
						console.log(result)
						var resultNote;
						for (var i = 0; i < result.filterAnswerIDs.length; i++) {
							if (result.filterAnswerIDs[i] != null) {
								if (result.filterAnswerIDs[i]['value'] === this.y) {
									console.log(this.y)
									// console.log(result)
									// return '<div>' + 'Title: </div>' +
									// '<div>' +'Desc: </div>' + '<button
									// id="edit" class="btn btn-default"
									// >Edit</button>' + '<form id="myform"
									// method="get"><div><input type="hidden"
									// id="filterObject" name="filterObject"
									// value="'+result['filterAnswerIDs'][i]['id']+'"><input
									// type="text" id="title" maxlength="50"
									// required></div><br><textarea
									// id="description" type="text"
									// maxlength="361"></textarea><br><button
									// id="send"
									// type="submit">Send</button></form>';
									
									if ( result.pointNotes.length > 0) {
										for (var j = 0; j < result.pointNotes.length; j++) {
											if (result.pointNotes[j] != null) {
												if (result.filterAnswerIDs[i]['id'] === result.pointNotes[j]['filterAnswerID']) {
													resultNote= '' + 'Title:'+result['pointNotes'][j]['title']+'<br/>Desc: '+result['pointNotes'][j]['note']+'<button  id="edit"  class="btn btn-default" >Edit</button><form id="deleteForm" method="get"><input type="hidden" id="deleteNoteID" value="'+result.pointNotes[j]['id']+'"><input type="submit"  id="delete" value="Delete"></form>' + '<form id="editForm" method="get"><div><input type="hidden" id="editNoteID" value="'+result.pointNotes[j]['id']+'"><input type="hidden" id="filterObject" name="filterObject" value="'+result['filterAnswerIDs'][i]['id']+'"><input type="text" id="title" maxlength="50"  required></div><br><textarea id="description" type="text" maxlength="361"></textarea><br><button id="update"  type="submit">Update</button></form>';
													break;
												}
	 											else {
	 												resultNote= '' + 'Title:<br/>Desc: <br/><button  id="edit"  class="btn btn-default" >Edit</button>' + '<form id="addForm" method="get"><div><input type="hidden" id="filterObject" name="filterObject" value="'+result['filterAnswerIDs'][i]['id']+'"><input type="text" id="title" maxlength="50"  required></div><br><textarea id="description" type="text" maxlength="361"></textarea><br><button id="send"  type="submit">Send</button></form>';
												}
											}
											else {
 												resultNote= '' + 'Title:<br/>Desc: <br/><button  id="edit"  class="btn btn-default" >Edit</button>' + '<form id="addForm" method="get"><div><input type="hidden" id="filterObject" name="filterObject" value="'+result['filterAnswerIDs'][i]['id']+'"><input type="text" id="title" maxlength="50"  required></div><br><textarea id="description" type="text" maxlength="361"></textarea><br><button id="send"  type="submit">Send</button></form>';
											}
										}
									} else {
										resultNote= '' + 'Title:<br/>Desc: <br/><button  id="edit"  class="btn btn-default" >Edit</button>' + '<form id="addForm" method="get"><div><input type="hidden" id="filterObject" name="filterObject" value="'+result['filterAnswerIDs'][i]['id']+'"><input type="text" id="title" maxlength="50"  required></div><br><textarea id="description" type="text" maxlength="361"></textarea><br><button id="send"  type="submit">Send</button></form>';
										break;
									}
									
								}
							}
						}
						return resultNote;
					}
//					useHTML : true
				},
				legend : {
					enabled : (emptyChart && Site.showLegend),
					reversed : true,
					align : 'right',
					verticalAlign : 'top',
					layout : 'vertical',
					itemStyle : {
						width : 200,
						paddingBottom : '5px'
					},
					labelFormatter : function() {
						if (this.name == ('' + $('.Q0').text())
								|| this.name == ('' + $('.Q1').text())
								|| this.name == ('' + $('.Q2').text()))
							return "<strong>" + this.name + ":</strong><br>";
						else
							return this.name;
					}
				},
				xAxis : {
					groupPadding : 0,
					categories : filteredBrandsFilters,
					labels : {
						style : {
							width : 150,
							fontWeight : 'bold'
						}
					}
				},
				yAxis : {
					title : {
						text : '(%)'
					},

					showEmpty : false

				},

				series : series
			};
			if (Site.chart1 != null) {
				Site.chart1.destroy();
				Site.chart1 = null;
			}
			if (!emptyChart) {
				// Site.chartOptions.yAxis.title.text = "No data to display";
				$('.no-data').show();
				$('#legend-button').hide();
				$('#bar-to-stacked').hide();
				$('#stacked-to-bar').hide();
			} else {
				$('.no-data').hide();
				$('#legend-button').show();
				$('#bar-to-stacked').show();
				$('#stacked-to-bar').show();
				Site.chart1 = new Highcharts.Chart(Site.chartOptions);
				$('.highcharts-legend-item > rect[fill="#FFFFFF"]').each(
						function() {
							$(this).prev().attr('x',
									parseInt($(this).prev().attr('x')) - 15);
							$(this).attr('width', '2');
							$(this).parent().mouseout();
						});

			}

		}
	},
	

	

		noteForm : function(e) {
		$(e).on("click", "#edit", function() {
			Site.change();
		});

		$(e).on("click", "#delete", function() {
			Site.deletePointNote();
		});

		$(e).on("click", "#send", function() {
			Site.sendInfo();
		});
		$(e).on("click", "#update", function() {
			Site.updatePointNote();
		});
		$(e).on("click", "#title", function() {
			$("#title").focus();
		});

		$(e).on("click", "#description", function() {
			$('#description').focus();
		});
	},
	deletePointNote : function() {
//		alert("delete note")

		$("#deleteForm").validate({
			success : "valid",
			submitHandler : function() {
				// alert("Title:" + $('#title').val() + '\n'
				// + "Description:" + $('#description').val());
				$.ajax({
					type : "GET",
					url : "/pointNotesController/delete",
					data : {

						"pointNoteID" : $('#deleteNoteID').val()
					},
					success : function() {
						location.reload();
					},
					error : function(e) {
						console.log(e)
//						alert("error: "+e)
					}
				});

			}
		});
	},

	// hides the edit button and shows the title input, description textarea and
	// send button
	change : function() {
		$('#edit').toggle();
		$('#title').toggle();
		$('#description').toggle();
		$('#delete').toggle();
		$('#send').toggle();
	},

	// validates the form, if the user did not input the title then this field
	// is required is stated, if the form is valid then an alert is produced
	// with the title and the description
	sendInfo : function() {
//		alert("add note")

		$("#addForm").validate({
			success : "valid",
			submitHandler : function() {
				// alert("Title:" + $('#title').val() + '\n'
				// + "Description:" + $('#description').val());
				$.ajax({
					type : "GET",
					url : "/pointNotesController/create",
					data : {
						"title" : $('#title').val(),
						"note" : $('#description').val(),
						"filterObject" : $('#filterObject').val(),

					},
					success : function() {
						location.reload();
					},
					error : function(e) {
						console.log(e)
//						alert("error "+e)
					}
				});

			}
		});

	},

	updatePointNote : function() {
//		alert("update note")
//		alert($('#editNoteID').val())
		$("#editForm").validate({
			success : "valid",
			submitHandler : function() {
				// alert("Title:" + $('#title').val() + '\n'
				// + "Description:" + $('#description').val());
				$.ajax({
					type : "GET",
					url : "/pointNotesController/update",
					data : {
						"title" : $('#title').val(),
						"note" : $('#description').val(),
						"pointNoteID" : $('#editNoteID').val()
					},
					success : function() {
						location.reload();
					},
					error : function(e) {
						console.log(e)
//						alert("ERROR:" + e)
					}
				});

			}
		});
	},
	

	
	initData : function() {
		Site.createRequest();
	},

	slideUpNotification : function() {
		$('#notification-container').slideUp();
	},
	createRequest : function() {
		// Site.showLoadingOverlay();
		Site.showLoadingOverlay();
		var callback = function() {
			Site.drawCharts();
			Site.hideLoadingOverlay();
		};
		// window.console.log('request created');
		if (window.location.href.indexOf("site/trend") !== -1) {
			Site.createTrendRequest(callback);
		} else if (window.location.href.indexOf("site/snapshot") !== -1) {
			Site.createSnapshotRequest(callback);
		}
	},

	createSnapshotRequestUrl : function(url) {
		url += "qs=" + $('.main-question').attr('id').replace('question', '');
		$('.compare-question').each(function(index, element) {
			url += "&";
			url += "qs=" + element.id.replace('question', '');
		});
		url += "&";
		url += "w=" + $('#wave-select').val();

		$($('#location-select').val()).each(function(index, element) {
			url += "&";
			url += "l=" + element;
		});
		$('.brand-input:checked:visible').each(function(index, element) {
			url += "&";
			url += "b=" + element.value;
		});
		$('.filters')
				.each(
						function(index, element) {
							var filterId = element.id.replace('filter', '');
							$(element)
									.find(
											'.divisions .division-item.selected:not(.compare-item)')
									.each(
											function(index, element) {
												var divisionId = element.id
														.replace('division', '');
												url += "&";
												url += "fd=" + filterId + "_"
														+ divisionId;
											});
							$(element)
									.find(
											'.divisions .division-item.selected.compare-item')
									.siblings('li:not(.sum-item)')
									.each(
											function(index, element) {
												var divisionId = element.id
														.replace('division', '');
												url += "&";
												url += "fd=" + filterId + "_"
														+ divisionId;
											});
						});

		$('.intro-holder').each(
				function(index, element) {
					var qid = $(this).find('.question-header').attr('id')
							.replace('question', '');
					var groupName = $(this).find('.group-name').text();
					if (groupName != "") {
						url += "&";
						url += "ag=" + qid + "_"
								+ encodeURIComponent(groupName);
					}
				});

		return url;
	},

	createUrl : function(url, type) {
		url += "q=" + $('.main-question').attr('id').replace('question', '');
		$('.compare-question').each(function(index, element) {
			url += "&";
			url += "qs=" + element.id.replace('question', '');
		});
		url += "&";
		url += "w=" + $('#wave-select').val();
		if ($('#second-wave-select').length > 0) {
			url += "&";
			url += "ws=" + $('#second-wave-select').val();
		}

		$($('#location-select').val()).each(function(index, element) {
			url += "&";
			url += "l=" + element;
		});

		$('.brand-input:checked:visible').each(function(index, element) {
			url += "&";
			url += "b=" + element.value;
		});
		$('.filters')
				.each(
						function(index, element) {
							var filterId = element.id.replace('filter', '');
							$(element)
									.find(
											'.divisions .division-item.selected:not(.compare-item)')
									.each(
											function(index, element) {
												var divisionId = element.id
														.replace('division', '');
												url += "&";
												url += "fd=" + filterId + "_"
														+ divisionId;
											});
							$(element)
									.find(
											'.divisions .division-item.selected.compare-item')
									.each(function(index, element) {
										url += "&";
										url += "fd=" + filterId + "_" + "c";
									});
							// $(element)
							// .find(
							// '.divisions
							// .division-item.selected.compare-item')
							// .siblings('li:not(.sum-item)')
							// .each(
							// function(index, element) {
							// var divisionId = element.id
							// .replace('division', '');
							// url += "&";
							// url += "fd=" + filterId + "_"
							// + divisionId;
							// });
						});
		$('.intro-holder').each(
				function(index, element) {
					var qid = $(this).find('.question-header').attr('id')
							.replace('question', '');
					var groupName = $(this).find('.group-name').text();
					if (groupName != "") {
						url += "&";
						url += "ag=" + qid + "_"
								+ encodeURIComponent(groupName);
					}
				});
		return url;
	},

	createSnapshotRequest : function(callback) {
		var baseUrl = "/site/querySnapshot?";
		var requestUrl = Site.createSnapshotRequestUrl('');
		var url = Site.createUrl('', page = "snapshot");
		var oldWave = $("#wave-select").val();
		$.ajax({
			url : baseUrl + requestUrl,
			async : true,
			success : function(result) {
				var oldData = Site.data;
				Site.data = result;
				Site.wave = oldWave;
				Site.waveData.brands = result.brands;

				if (oldData != null) {
					History.pushState({
						data : Site.data,
						wave : oldWave,
						waveData : Site.waveData,
						brands : Site.brands,
						init : false
					}, document.title, "?" + url);
				} else {
					History.replaceState({
						data : Site.data,
						wave : oldWave,
						waveData : Site.waveData,
						brands : Site.brands,
						init : true
					}, document.title, "?" + url);
					// Site.drawCharts();
				}
				if (callback != null) {
					callback();
				}

				Site.hideLoadingOverlay();
			},
			error : Site.error,
			dataType : 'json',
			type : 'GET'
		});

	},

	createTrendRequestUrl : function(url) {
		url += "qs=" + $('.main-question').attr('id').replace('question', '');
		$('.compare-question').each(function(index, element) {
			url += "&";
			url += "qs=" + element.id.replace('question', '');
		});
		url += "&";
		url += "w=" + $('#wave-select').val();
		url += "&";
		url += "ws=" + $('#second-wave-select').val();
		$($('#location-select').val()).each(function(index, element) {
			url += "&";
			url += "l=" + element;
		});
		$('.brand-input:checked:visible').each(function(index, element) {
			url += "&";
			url += "b=" + element.value;
		});
		$('.filters')
				.each(
						function(index, element) {
							var filterId = element.id.replace('filter', '');
							$(element)
									.find(
											'.divisions .division-item.selected:not(.compare-item)')
									.each(
											function(index, element) {
												var divisionId = element.id
														.replace('division', '');
												url += "&";
												url += "fd=" + filterId + "_"
														+ divisionId;
											});
							$(element)
									.find(
											'.divisions .division-item.selected.compare-item')
									.siblings('li:not(.sum-item)')
									.each(
											function(index, element) {
												var divisionId = element.id
														.replace('division', '');
												url += "&";
												url += "fd=" + filterId + "_"
														+ divisionId;
											});
						});

		$('.intro-holder').each(
				function(index, element) {
					var qid = $(this).find('.question-header').attr('id')
							.replace('question', '');
					var groupName = $(this).find('.group-name').text();
					if (groupName != "") {
						url += "&";
						url += "ag=" + qid + "_"
								+ encodeURIComponent(groupName);
					}
				});

		return url;
	},
	createTrendRequest : function(callback) {
		// alert("createTrendRequest")
		var baseUrl = "/site/trendquery?";
		var requestUrl = Site.createTrendRequestUrl('');
		var url = Site.createUrl('', page = "trend");
		var oldWave = $("#wave-select").val();
		var oldSecondWave = $("#second-wave-select").val();
		$.ajax({
			url : baseUrl + requestUrl,
			async : true,
			success : function(result) {
				// console.log(Site.data.values+"1");
				var oldData = Site.data;
				Site.data = result;
				Site.wave = oldWave;
				Site.secondWave = oldSecondWave;

				if (oldData != null) {
					History.pushState({
						data : Site.data,
						wave : Site.wave,
						secondWave : Site.secondData,
						waveData : Site.waveData,
						brands : Site.brands,
						init : false
					}, document.title, "?" + url);
				} else {
					History.replaceState({
						data : Site.data,
						wave : Site.wave,
						secondWave : Site.secondData,
						waveData : Site.waveData,
						brands : Site.brands,
						init : true
					}, document.title, "?" + url);
					// Site.drawCharts();
					// Site.wave = Site.splitUrl(location.search).wave;
				}
				if (callback != null) {
					callback();
				}

			},
			error : Site.error,
			dataType : 'json',
			type : 'GET'
		});

	},

	drawTrendChartsForExport : function() {
		result = Site.data;
		series = new Array();
		var updatedAnswersCount = new Array();
		var valuesCopy = new Array();
		var startingIndex = 0;
		var emptyCheckCount = 0;
		var valuesSizeCount = 0;
		var emptyChart = true;
		for (var i = 0; i < result.values.length; i++) {
			valuesCopy.push(new Array());
			for (var j = 0; j < result.values[0].length; j++) {
				if (result.values[i][j] == -1000) {
					emptyCheckCount++;
					valuesCopy[i].push(null);
				} else {
					valuesCopy[i].push(result.values[i][j]);
				}
				valuesSizeCount++;
			}
		}

		if (valuesSizeCount == emptyCheckCount) {
			emptyChart = false;
		}

		filteredLegend = new Array();
		for (var i = 0; i < valuesCopy.length; i++) {
			var nullCheck = false;
			for (var j = 0; j < valuesCopy[0].length; j++) {
				if (valuesCopy[i][j] != null) {
					nullCheck = true;
					break;
				}
			}
			if (nullCheck == false)
				filteredLegend.push(i);
		}
		var selectedBrands = new Array();
		// for ( var i = 0; i < result.all.length; i++) {
		// // alert(result.all[i] + result.values[i] );
		// series.push({
		// name : result.all[i],
		// data : valuesCopy[i],
		// });
		// }
		if (result.counts.length > 1) {
			for (var j = 0; j < result.counts.length; j++) {
				var noData = true;
				for (var i = startingIndex; i < result.counts[j]
						+ startingIndex; i++) {
					if ($.inArray(i, filteredLegend) == -1) {
						series.push({
							name : '' + $('.Q' + j).text(),
							data : null,
							showInLegend : true,
							color : '#FFFFFF'
						});
						noData = false;
						break;
					}
				}
				if (!noData) {
					var currentAnswersCount = result.counts[j];
					for (var i = startingIndex; i < result.counts[j]
							+ startingIndex; i++) {
						// alert(result.all[i] + result.values[i] );
						var showInLegend = true;
						if ($.inArray(i, filteredLegend) != -1) {
							showInLegend = false;
							currentAnswersCount--;
						}
						series.push({
							name : result.answers[i],
							data : valuesCopy[i],
							showInLegend : showInLegend
						});

					}

					updatedAnswersCount.push(currentAnswersCount);
				}
				startingIndex += result.counts[j];
			}
		} else {

			$('.brand-input:checked:visible').each(function() {
				selectedBrands.push($(this).parent().text());
			});
			if (selectedBrands.length > 1) {
				var fullAnswer = new Array();
				var curranswer = 0;
				updatedAnswersCount.length = 0;
				for (var i = 0; i < result.values.length; i++) {
					var showInLegend = false;
					var currentAnswersCount = 0;
					for (var j = 0; j < selectedBrands.length; j++) {

						if ($.inArray(i + j, filteredLegend) == -1) {
							showInLegend = true;
							break;

						}
					}
					series.push({
						name : result.answers[curranswer],
						data : null,
						showInLegend : showInLegend,
						color : '#FFFFFF'
					});

					fullAnswer.push(null);
					for (var j = 0; j < selectedBrands.length; j++) {

						if (j == 5) {
							series.push({
								name : selectedBrands[j],
								data : valuesCopy[i + j],
								showInLegend : showInLegend,
								marker : {
									fillColor : '#FFFFFF',
									lineWidth : 2,
									lineColor : null,
									symbol : "circle"
								}
							});
						} else {
							series.push({
								name : selectedBrands[j],
								data : valuesCopy[i + j],
								showInLegend : showInLegend,
								marker : {
									symbol : Site.symbolsList[j]
								}
							});
						}
						fullAnswer.push(result.all[i + j]);
						currentAnswersCount++;

					}
					updatedAnswersCount.push(currentAnswersCount);
					i += selectedBrands.length - 1;
					curranswer++;
				}
			} else if ($('.compare-item.selected').length > 0) {
				updatedAnswersCount.length = 0;
				var filters = new Array();
				var fullAnswer = new Array();
				$('.compare-item.selected').siblings(':not(".sum-item")').each(
						function(index, element) {
							filters.push(result.divisions[index]);
						});
				var curranswer = 0;
				for (var i = 0; i < result.values.length; i++) {
					var currentAnswersCount = 0;
					var showInLegend = false;
					for (var j = 0; j < filters.length; j++) {

						if ($.inArray(i + j, filteredLegend) == -1) {
							showInLegend = true;
							break;

						}
					}
					series.push({
						name : result.answers[curranswer],
						data : null,
						showInLegend : showInLegend,
						color : '#FFFFFF'
					});
					fullAnswer.push(null);
					for (var j = 0; j < filters.length; j++) {

						series.push({
							name : filters[j],
							data : valuesCopy[i + j],
							showInLegend : showInLegend
						});
						fullAnswer.push(result.all[i + j]);
						currentAnswersCount++;
					}

					i += filters.length - 1;
					curranswer++;
					updatedAnswersCount.push(currentAnswersCount);
				}
			} else {
				if ($('#location-select option:selected').length > 1) {
					var selectedCountries = new Array();
					$('#location-select option:selected').each(function() {
						selectedCountries.push($(this).text());
					});
					var AnswersPerCountry = new Array();
					for (var k = 0; k < selectedCountries.length; k++) {
						AnswersPerCountry.push(result.counts[0]);
					}
					var fullAnswer = new Array();
					// var curranswer = 0;
					updatedAnswersCount.length = 0;
					for (var j = 0; j < AnswersPerCountry.length; j++) {
						var noData = true;
						for (var i = startingIndex; i < AnswersPerCountry[j]
								+ startingIndex; i++) {
							if ($.inArray(i, filteredLegend) == -1) {
								series.push({
									name : selectedCountries[j],
									data : null,
									showInLegend : true,
									color : '#FFFFFF'
								});
								fullAnswer.push(null);
								noData = false;
								break;
							}
						}
						if (!noData) {
							var answersIndexForCountry = 0;
							var currentAnswersCount = AnswersPerCountry[j];
							for (var i = startingIndex; i < AnswersPerCountry[j]
									+ startingIndex; i++) {
								// alert(result.all[i] + result.values[i] );
								var showInLegend = true;
								if ($.inArray(i, filteredLegend) != -1) {
									showInLegend = false;
									currentAnswersCount--;
								}
								series
										.push({
											name : result.answers[answersIndexForCountry],
											data : valuesCopy[i],
											showInLegend : showInLegend
										});
								fullAnswer.push(result.all[i]);
								answersIndexForCountry++;
							}
							startingIndex += AnswersPerCountry[j];
							updatedAnswersCount.push(currentAnswersCount);
						}
					}
					// for ( var i = 0; i < result.values.length; i++) {
					// var showInLegend = false;
					// var currentAnswersCount = 0;
					// for ( var j = 0; j < selectedCountries.length; j++) {
					//
					// if ($.inArray(i + j, filteredLegend) == -1) {
					// showInLegend = true;
					// break;
					//
					// }
					// }
					// series.push({
					// name : result.answers[curranswer],
					// data : null,
					// showInLegend : showInLegend,
					// color : '#FFFFFF'
					// });
					//
					// fullAnswer.push(null);
					// for ( var j = 0; j < selectedCountries.length; j++) {
					//
					// series.push({
					// name : selectedCountries[j],
					// data : valuesCopy[i+j],
					// showInLegend : showInLegend
					// });
					// fullAnswer.push(result.all[i + j]);
					// currentAnswersCount++;
					//
					// }
					// updatedAnswersCount.push(currentAnswersCount);
					// i += selectedCountries.length - 1;
					// curranswer++;
					// }
				} else {
					var currentAnswersCount = result.all.length;
					for (var i = 0; i < result.all.length; i++) {
						var showInLegend = true;
						if ($.inArray(i, filteredLegend) != -1) {
							showInLegend = false;
							currentAnswersCount--;
						}
						series.push({
							name : result.answers[i],
							data : valuesCopy[i],
							showInLegend : showInLegend
						});
					}
					updatedAnswersCount.push(currentAnswersCount);
				}
			}
		}

		Site.trendChartOptions = {
			chart : {
				renderTo : 'tc-container-export',
				type : 'spline',
				height : "400",
				events : {
					load : function(event) {

						Site.deselectSeriesFromExportChart(this);

						var serializer = new XMLSerializer();
						var svg = $('.tc-container-export svg').clone();
						var svnStr = serializer.serializeToString(svg[0]);

						var svgWidth = svg.attr('width') + 200;
						svg.attr('width', svgWidth);
						var svgHeight = svg.attr('height');

						var canv = $('<canvas id="canv"></canvas>');
						canv.hide();
						canv.attr('width', svgWidth + 'px');
						canv.attr('height', svgHeight + 'px');
						canv.appendTo('body');

						canvg(canv[0], svnStr);
						var data = document.getElementById('canv').toDataURL();

						canv.remove();

						var imageDataInput = $('<input name="svg" type="hidden" />');

						var date = new Date();

						var filenNameInput = $('<input name="fn" type="hidden" value="export-'
								+ date.getDate()
								+ '-'
								+ (date.getMonth() + 1)
								+ '-' + date.getFullYear() + '" />');
						imageDataInput.val(data);

						if (!forDrive) {
							var form = $('<form style="display:none;"></form>');
							form.append(imageDataInput).append(filenNameInput)
									.attr('action', '/site/exportpng').attr(
											'method', 'post');
							var token = $('#_tk').val();
							form
									.append("<input type='hidden' name='tk' value='"
											+ Site.escapeHtml(token) + "' />");
							form.appendTo('body');
							form.submit();
							form.remove();
							parent.$.fancybox.close();
						} else {
							var form = $('#exporting-form');
							form.attr('action', '/site/exportdrive');
							form.append(imageDataInput);
							// form
							// .append("<input type='hidden' name='link'
							// value='"
							// + location.pathname
							// + location.search + "'/>");
							form.append(imageDataInput).append(filenNameInput);
							var loadingImage = $('<img class="loader-image" src="/site_ui/images/ajax-loader.gif" width="31" height="31"/>');
							loadingImage.insertBefore($('.export-chart'));
							Site.preExport();
							$
									.post(
											$(form).attr('action'),
											$(form).serialize(),
											function(data) {
												if (data != null) {

													if (data.redirect != null) {
														// location.href =
														// data.redirect;
														window
																.open(
																		data.redirect,
																		"",
																		"width=600,height=400,left=10,top=0");
													} else {

														Site
																.showNotification(
																		"Export to Google Drive is complete.  You can access the exported files <a target='_BLANK' href='"
																				+ Site
																						.escapeHtml(data.folderLink)
																				+ "'>here</a>",
																		showUndo = false);

														parent.$.fancybox
																.close();
													}
												}
												// else {
												// Site
												// .showNotification(
												// "Export to google drive
												// is complete. You can
												// access the exported files
												// <a
												// target='_BLANK' href='"
												// + Site
												// .escapeHtml(data.folderLink)
												// + "'>here</a>",
												// showUndo = false);

												// parent.$.fancybox
												// .close();
												// }
												loadingImage.remove();
											}, "json");

						}

						imageDataInput.remove();
						filenNameInput.remove();

					}
				}

			},
			title : {
				text : ''
			},
			xAxis : {
				categories : result.dataSetsNames
			},
			exporting : {
				enabled : false
			},
			credits : {
				enabled : false
			},
			yAxis : {
				// max : 125,
				min : 0,
				// tickInterval : 25,
				showLastLabel : false,
				title : {
					text : '(%)'
				}
			},
			tooltip : {
				style : {
					fontWeight : 'bold',
					width : 400
				},
				enabled : true,
				formatter : function() {
					var index = parseInt(this.series.index);
					if (fullAnswer != null) {
						return '<b>' + fullAnswer[index] + '</b><br/>' + this.x
								+ ': ' + this.y + '%';
					} else {

						return '<b>' + this.series.name + '</b><br/>' + this.x
								+ ': ' + this.y + '%';
					}
				},

			},
			legend : {
				enabled : (emptyChart && Site.showLegend),
				layout : 'horizontal',
				itemStyle : {
				// width : 200
				},
				labelFormatter : function() {
					if (this.name == ('' + $('.Q0').text())
							|| this.name == ('' + $('.Q1').text())
							|| this.name == ('' + $('.Q2').text())
							|| ((selectedBrands.length > 1 || filters != null) && $
									.inArray(this.name, result.answers) != -1)
							|| (selectedCountries != null
									&& selectedCountries.length > 1 && $
									.inArray(this.name, selectedCountries) != -1))
						return "<strong>" + this.name + ":</strong><br>";
					else
						return this.name;
				}
			},

			plotOptions : {
				line : {

					dataLabels : {
						enabled : false
					},
					enableMouseTracking : true,
				},

				series : {
					events : {
						legendItemClick : function(event) {
							return !(this.name == ('' + $('.Q0').text())
									|| this.name == ('' + $('.Q1').text())
									|| this.name == ('' + $('.Q2').text())
									|| ((selectedBrands.length > 1 || filters != null) && $
											.inArray(this.name, result.answers) != -1) || (selectedCountries != null
									&& selectedCountries.length > 1 && $
									.inArray(this.name, selectedCountries) != -1));
						}
					},
					animation : false
				// animation : {
				// complete : function() {
				// console.log("printing");
				//							
				//
				// }
				// }
				}

			},
			series : series
		};

		if (Site.trendChart2 != null) {
			Site.trendChart2.destroy();
			Site.trendChart2 = null;
		}
		if (!emptyChart) {
		} else {
			Site.trendChart2 = new Highcharts.Chart(Site.trendChartOptions);
		}

		var lineHeight = -14;
		var counter = 0;
		var questionIndex = 0;
		var lineCount = 1;
		var questionWidth = 0;
		var questionStartingIndex = 1;
		$('.tc-container-export .highcharts-legend-item > text')
				.each(
						function() {
							if (counter == questionStartingIndex) {
								questionWidth = parseInt($(this).parent().attr(
										'transform').split(",")[0]
										.substring(10)) - 8;
								lineHeight = lineHeight + 14;
								if ((parseInt($(this).parent()
										.attr('transform').split(",")[0]
										.substring(10)) - 8) == 0)
									lineHeight = lineHeight - 14;
								questionStartingIndex += updatedAnswersCount[questionIndex] + 1;
								questionIndex++;
								lineCount++;
							}
							if (counter == questionStartingIndex - 1) {
								lineHeight = lineHeight + 14;
								lineCount++;
								questionWidth = parseInt($(this).parent().attr(
										'transform').split(",")[0]
										.substring(10)) - 8;
								if ((parseInt($(this).parent()
										.attr('transform').split(",")[0]
										.substring(10)) - 8) != 0)
									lineHeight = lineHeight + 15;
							}
							if ((parseInt($(this).parent().attr('transform')
									.split(",")[0].substring(10)) - 8) == 0) {
								questionWidth = 0;
							}
							if ((parseInt($(this).parent().attr('transform')
									.split(",")[0].substring(10)))
									- questionWidth == 8
									&& counter != questionStartingIndex - 1
									&& counter != questionStartingIndex) {
								lineCount++;
							}
							$(this).attr(
									'transform',
									'translate(' + -questionWidth + ','
											+ lineHeight + ')');
							$(this).siblings().attr(
									'transform',
									'translate(' + -questionWidth + ','
											+ lineHeight + ')');
							counter++;
						});

		var oldLegendHeight = $(
				'.tc-container-export .highcharts-legend > rect')
				.attr('height');
		$('.tc-container-export .highcharts-legend > rect').attr('height',
				((lineCount) * 14));
		if ($('.tc-container-export .highcharts-legend > g').length > 1) {
			$('.tc-container-export .highcharts-legend > g').last().hide();
		}
		var svgHeight = $('.tc-container-export .highcharts-container > svg')
				.attr('height');
		$('.tc-container-export .highcharts-container >svg > defs rect')
				.first().attr('height',
						svgHeight - oldLegendHeight + ((lineCount) * 14));
		$('.tc-container-export  .highcharts-container svg > rect').attr(
				'height', svgHeight - oldLegendHeight + ((lineCount) * 14));
		$('.tc-container-export .highcharts-container >svg').attr('height',
				svgHeight - oldLegendHeight + ((lineCount) * 14));
		$('.tc-container-export .highcharts-container').height(
				svgHeight - oldLegendHeight + ((lineCount) * 14));
		Site.chartSize = svgHeight - oldLegendHeight + ((lineCount) * 14);
		Site.chartLegendSize = svgHeight - oldLegendHeight + ((lineCount) * 14);
		Site.legendheight = (lineCount) * 14;
	},

	drawTrendCharts : function() {
//		alert("drawTrendCharts")
		result = Site.data;
		console.log(result)
		series = new Array();
		var updatedAnswersCount = new Array();
		var valuesCopy = new Array();
		var startingIndex = 0;
		var emptyCheckCount = 0;
		var valuesSizeCount = 0;
		var emptyChart = true;
		for (var i = 0; i < result.values.length; i++) {
			valuesCopy.push(new Array());
			for (var j = 0; j < result.values[0].length; j++) {
				if (result.values[i][j] == -1000) {
					emptyCheckCount++;
					valuesCopy[i].push(null);
				} else {
					valuesCopy[i].push(result.values[i][j]);
				}
				valuesSizeCount++;
			}
		}

		if (valuesSizeCount == emptyCheckCount) {
			emptyChart = false;
		}

		filteredLegend = new Array();
		for (var i = 0; i < valuesCopy.length; i++) {
			var nullCheck = false;
			for (var j = 0; j < valuesCopy[0].length; j++) {
				if (valuesCopy[i][j] != null) {
					nullCheck = true;
					break;
				}
			}
			if (nullCheck == false)
				filteredLegend.push(i);
		}
		var selectedBrands = new Array();
		if (result.counts.length > 1) {
			for (var j = 0; j < result.counts.length; j++) {
				for (var i = startingIndex; i < result.counts[j]
						+ startingIndex; i++) {
					if ($.inArray(i, filteredLegend) == -1) {
						series.push({
							name : '' + $('.Q' + j).text(),
							data : null,
							showInLegend : true,
							color : '#FFFFFF'
						});
						break;
					}
				}
				var currentAnswersCount = result.counts[j];
				for (var i = startingIndex; i < result.counts[j]
						+ startingIndex; i++) {
					// alert(result.all[i] + result.values[i] );
					var showInLegend = true;
					if ($.inArray(i, filteredLegend) != -1) {
						showInLegend = false;
						currentAnswersCount--;
					}
					series.push({
						name : result.answers[i],
						data : valuesCopy[i],
						showInLegend : showInLegend
					});
				}
				startingIndex += result.counts[j];
				updatedAnswersCount.push(currentAnswersCount);
			}
		} else {

			$('.brand-input:checked:visible').each(function() {
				selectedBrands.push($(this).parent().text());
			});
			if (selectedBrands.length > 1) {
				var fullAnswer = new Array();
				var curranswer = 0;
				updatedAnswersCount.length = 0;
				for (var i = 0; i < result.values.length; i++) {
					var showInLegend = false;
					var currentAnswersCount = 0;
					for (var j = 0; j < selectedBrands.length; j++) {

						if ($.inArray(i + j, filteredLegend) == -1) {
							showInLegend = true;
							break;

						}
					}
					series.push({
						name : result.answers[curranswer],
						data : null,
						showInLegend : showInLegend,
						color : '#FFFFFF'
					});

					fullAnswer.push(null);
					for (var j = 0; j < selectedBrands.length; j++) {

						if (j == 5) {
							series.push({
								name : selectedBrands[j],
								data : valuesCopy[i + j],
								showInLegend : showInLegend,
								marker : {
									fillColor : '#FFFFFF',
									lineWidth : 2,
									lineColor : null,
									symbol : "circle"
								}
							});
						} else {
							series.push({
								name : selectedBrands[j],
								data : valuesCopy[i + j],
								showInLegend : showInLegend,
								marker : {
									symbol : Site.symbolsList[j]
								}
							});
						}
						fullAnswer.push(result.all[i + j]);
						currentAnswersCount++;
					}
					updatedAnswersCount.push(currentAnswersCount);
					i += selectedBrands.length - 1;
					curranswer++;
				}
			} else if ($('.compare-item.selected').length > 0) {
				updatedAnswersCount.length = 0;
				var filters = new Array();
				var fullAnswer = new Array();
				$('.compare-item.selected').siblings(':not(".sum-item")').each(
						function(index, element) {
							filters.push(result.divisions[index]);
						});
				var curranswer = 0;
				for (var i = 0; i < result.values.length; i++) {
					var currentAnswersCount = 0;
					var showInLegend = false;
					for (var j = 0; j < filters.length; j++) {

						if ($.inArray(i + j, filteredLegend) == -1) {
							showInLegend = true;
							break;

						}
					}
					series.push({
						name : result.answers[curranswer],
						data : null,
						showInLegend : showInLegend,
						color : '#FFFFFF'
					});
					fullAnswer.push(null);
					for (var j = 0; j < filters.length; j++) {

						series.push({
							name : filters[j],
							data : valuesCopy[i + j],
							showInLegend : showInLegend
						});
						fullAnswer.push(result.all[i + j]);
						currentAnswersCount++;

					}
					i += filters.length - 1;
					curranswer++;
					updatedAnswersCount.push(currentAnswersCount);
				}
			} else {
				if ($('#location-select option:selected').length > 1) {
					var selectedCountries = new Array();
					$('#location-select option:selected').each(function() {
						selectedCountries.push($(this).text());
					});
					var AnswersPerCountry = new Array();
					for (var k = 0; k < selectedCountries.length; k++) {
						AnswersPerCountry.push(result.counts[0]);
					}
					var fullAnswer = new Array();
					// var curranswer = 0;
					updatedAnswersCount.length = 0;
					for (var j = 0; j < AnswersPerCountry.length; j++) {
						var noData = true;
						for (var i = startingIndex; i < AnswersPerCountry[j]
								+ startingIndex; i++) {
							if ($.inArray(i, filteredLegend) == -1) {
								series.push({
									name : selectedCountries[j],
									data : null,
									showInLegend : true,
									color : '#FFFFFF'
								});
								fullAnswer.push(null);
								noData = false;
								break;
							}
						}
						if (!noData) {
							var answersIndexForCountry = 0;
							var currentAnswersCount = AnswersPerCountry[j];
							for (var i = startingIndex; i < AnswersPerCountry[j]
									+ startingIndex; i++) {
								// alert(result.all[i] + result.values[i] );
								var showInLegend = true;
								if ($.inArray(i, filteredLegend) != -1) {
									showInLegend = false;
									currentAnswersCount--;
								}
								series
										.push({
											name : result.answers[answersIndexForCountry],
											data : valuesCopy[i],
											showInLegend : showInLegend
										});

								// series.push({
								//
								// type : 'flags',
								// name : 'Flags on series',
								// data : [ {
								// x : Facebook,
								// title : 'On series'
								// }, {
								// x : lastDate - 30 * days,
								// title : 'On series'
								// } ]
								// });
								fullAnswer.push(result.all[i]);
								answersIndexForCountry++;
							}
							startingIndex += AnswersPerCountry[j];
							updatedAnswersCount.push(currentAnswersCount);
						}
					}

					// if ($('#location-select option:selected').length > 1) {
					// var selectedCountries = new Array();
					// $('#location-select option:selected').each(function() {
					// selectedCountries.push($(this).text());
					// });
					// var fullAnswer = new Array();
					// var curranswer = 0;
					// updatedAnswersCount.length = 0;
					// for ( var i = 0; i < result.values.length; i++) {
					// var showInLegend = false;
					// var currentAnswersCount = 0;
					// for ( var j = 0; j < selectedCountries.length; j++) {
					// if ($.inArray(i + j, filteredLegend) == -1) {
					// showInLegend = true;
					// break;
					//
					// }
					// }
					// series.push({
					// name : result.answers[curranswer],
					// data : null,
					// showInLegend : showInLegend,
					// color : '#FFFFFF'
					// });
					//
					// fullAnswer.push(null);
					// for ( var j = 0; j < selectedCountries.length; j++) {
					//
					// series.push({
					// name : selectedCountries[j],
					// data : valuesCopy[i+j],
					// showInLegend : true
					// });
					// fullAnswer.push(result.all[i + j]);
					// currentAnswersCount++;
					//
					// }
					// updatedAnswersCount.push(currentAnswersCount);
					// i += selectedCountries.length - 1;
					// curranswer++;
					// }
				} else {
					var currentAnswersCount = result.all.length;
					for (var i = 0; i < result.all.length; i++) {
						var showInLegend = true;
						if ($.inArray(i, filteredLegend) != -1) {
							showInLegend = false;
							currentAnswersCount--;
						}
						series.push({
							name : result.answers[i],
							data : valuesCopy[i],
							showInLegend : showInLegend
						});
						updatedAnswersCount.push(currentAnswersCount);
					}
				}
			}
		}

		// for (var i = 0; i < result.all.length; i++) {
		// series.push({
		// type : 'flags',
		// // name : result.pointNotes[i].note,
		// fillColor : 'rgba(255,255,255,0.8)',
		// data : [ {
		// y : result.values[i][0],
		// // title : rCesult.all[i],
		// } ]
		// // onSeries : result.all[i]
		//
		// });
		// }

		// series.push({
		//
		// type : 'flags',
		// name : 'Flags on series',
		// data : [ {
		// // y : 25,
		// y : 0.35,
		// text : "sfdsfd dxdnote content",
		// title : 'note title'
		// } ],
		// onSeries : series[0].name
		// });
		// document.myseries = series;
		// series.push({
		//
		// type : 'flags',
		// name : 'Flags on series',
		// data : [ {
		// //y : 25,
		// y: 4.47,
		// text : 'note content',
		// title : 'note title'
		// } ]
		// });
		//		
		// series.push({
		//
		// type : 'flags',
		// name : 'Flags on series',
		// data : [ {
		// //y : 25,
		// y: 99.65,
		// text : 'note content',
		// title : 'note title'
		// } ]
		// });

		Site.trendChartOptions = {
			chart : {
				renderTo : 'tc-container',
				type : 'spline',
				height : "400",
				events : {
					load : function() {
						this.myTooltip = new Highcharts.Tooltip(this,
								this.options.tooltip);
					}
				},

			},
			title : {
				text : ''
			},
			xAxis : {
				categories : result.dataSetsNames
			},
			exporting : {
				enabled : false
			},
			credits : {
				enabled : false
			},
			yAxis : {
				// max : 125,
				min : 0,
				// tickInterval : 25,
				showLastLabel : false,
				title : {
					text : '(%)'
				}
			},
			tooltip : {
				 enabled: false,
		            hideDelay: 0,
		            followPointer: false,
		            shared: false,
		            useHTML: true,
				style : {
					fontWeight : 'bold',
					width : 400
				},
				
//				 formatter : function() {
//				 var index = parseInt(this.series.index);
//				 if (fullAnswer != null) {
//				 return '<b>' + fullAnswer[index] + '</b><br/>' + this.x
//				 + ': ' + this.y + '%';
//				 } else {
//				
//				 return '<b>' + this.series.name + '</b><br/>' + this.x
//				 + ': ' + this.y + '%';
//				 }
//				 },
				formatter : function() {
					console.log(result)
					
					var resultNote;
					for (var i = 0; i < result.filterAnswerIDs.length; i++) {
						if (result.filterAnswerIDs[i] != null) {
							if (result.filterAnswerIDs[i]['value'] === this.y) {
								console.log(this.y)
								if ( result.pointNotes.length > 0) {
									for (var j = 0; j < result.pointNotes.length; j++) {
										if (result.pointNotes[j] != null) {
											if (result.filterAnswerIDs[i]['id'] === result.pointNotes[j]['filterAnswerID']) {
												resultNote= '' + 'Title:'+result['pointNotes'][j]['title']+'<br/>Desc: '+result['pointNotes'][j]['note']+'<button  id="edit"  class="btn btn-default" >Edit</button><form id="deleteForm" method="get"><input type="hidden" id="deleteNoteID" value="'+result.pointNotes[j]['id']+'"><input type="submit"  id="delete" value="Delete"></form>' + '<form id="editForm" method="get"><div><input type="hidden" id="editNoteID" value="'+result.pointNotes[j]['id']+'"><input type="hidden" id="filterObject" name="filterObject" value="'+result['filterAnswerIDs'][i]['id']+'"><input type="text" id="title" maxlength="50"  required></div><br><textarea id="description" type="text" maxlength="361"></textarea><br><button id="update"  type="submit">Update</button></form>';
												break;
											}
 											else {
 												resultNote= '' + 'Title:<br/>Desc: <br/><button  id="edit"  class="btn btn-default" >Edit</button>' + '<form id="addForm" method="get"><div><input type="hidden" id="filterObject" name="filterObject" value="'+result['filterAnswerIDs'][i]['id']+'"><input type="text" id="title" maxlength="50"  required></div><br><textarea id="description" type="text" maxlength="361"></textarea><br><button id="send"  type="submit">Send</button></form>';
											}
										}
										else {
												resultNote= '' + 'Title:<br/>Desc: <br/><button  id="edit"  class="btn btn-default" >Edit</button>' + '<form id="addForm" method="get"><div><input type="hidden" id="filterObject" name="filterObject" value="'+result['filterAnswerIDs'][i]['id']
													+ '"><input type="text" id="title" maxlength="50"  required></div><br><textarea id="description" type="text" maxlength="361"></textarea><br><button id="send"  type="submit">Send</button></form>';
										}
									}
								} else {
									resultNote = ''
											+ 'Title:<br/>Desc: <br/><button  id="edit"  class="btn btn-default" >Edit</button>'
											+ '<form id="addForm" method="get"><div><input type="hidden" id="filterObject" name="filterObject" value="'
											+ result['filterAnswerIDs'][i]['id']
											+ '"><input type="text" id="title" maxlength="50"  required></div><br><textarea id="description" type="text" maxlength="361"></textarea><br><button id="send"  type="submit">Send</button></form>';
									break;
								}

							}
						}
					}
					return resultNote;
				},

			},
			legend : {
				enabled : (emptyChart && Site.showLegend),
				// reversed : true,
				align : 'right',
				verticalAlign : 'top',
				// floating: true,
				layout : 'vertical',
				itemStyle : {
					width : 200,
					paddingBottom : '5px'
				},
				labelFormatter : function() {
					if (this.name == ('' + $('.Q0').text())
							|| this.name == ('' + $('.Q1').text())
							|| this.name == ('' + $('.Q2').text())
							|| ((selectedBrands.length > 1 || filters != null) && $
									.inArray(this.name, result.answers) != -1)
							|| (selectedCountries != null
									&& selectedCountries.length > 1 && $
									.inArray(this.name, selectedCountries) != -1))
						return "<strong>" + this.name + ":</strong><br>";
					else
						return this.name;
				}
			},

			plotOptions : {
				line : {

					dataLabels : {
						enabled : false
					},
					enableMouseTracking : true,

				},
				series : {
					events : {
						legendItemClick : function(event) {
							return !(this.name == ('' + $('.Q0').text())
									|| this.name == ('' + $('.Q1').text())
									|| this.name == ('' + $('.Q2').text())
									|| ((selectedBrands.length > 1 || filters != null) && $
											.inArray(this.name, result.answers) != -1) || (selectedCountries != null
									&& selectedCountries.length > 1 && $
									.inArray(this.name, selectedCountries) != -1));
						},
						click : function(evt) {
							this.chart.myTooltip.refresh(evt.point, evt);
						},
					}
				}

			},
			series : series
		};
		// if (!emptyChart) {
		// Site.trendChartOptions.xAxis.categories = [ "<b>No data to
		// display</b>" ];
		// }

		if (Site.trendChart != null) {
			Site.trendChart.destroy();
			Site.trendChart = null;
		}
		if (!emptyChart) {
			$('.no-data').show();
			$('#legend-button').hide();
		} else {

			$('.no-data').hide();
			$('#legend-button').show();
			Site.trendChart = new Highcharts.Chart(Site.trendChartOptions);
			$(
					'.highcharts-legend-item > path[fill="#FFFFFF"][stroke="#FFFFFF"]')
					.each(
							function() {
								$(this).next()
										.attr(
												'x',
												parseInt($(this).next().attr(
														'x')) - 15);
								$(this).parent().mouseout();
							});

		}

	},

	showRelatedBrands : function(brands) {
		$(brands).each(function() {
			$('.brand-input[value="' + this + '"]').parent().show('fast');
		});
		Site.ScrollBar();
	},

	setMainQuestion : function(mainQuestionId) {
		// var mainQuestion = Site.waveData.questions[mainQuestionId];
		// if (mainQuestion == null) {
		//
		// var answerGroups =
		// Site.waveData.questions[mainQuestion.id].answerGroups;
		// if (answerGroups != null && answerGroups.length > 0) {
		// // groupsOptions += '<li><a class="group-item"
		// // href="#">Default</a></li>';
		// $(answerGroups)
		// .each(
		// function() {
		// if ($
		// .isEmptyObject(Site.urlSplit.answerGroupsByQuestion)) {
		// questionInnerContainer.find(
		// '.group-name').text(
		// 'Default group');
		// }
		// groupsOptions += '<li><a class="group-item" href="#">'
		// + this + '</a></li>';
		// });
		// // groupsOptions = '<span>Groups</span><select class="">'
		// // + groupsOptions + '</select>'
		//
		// } else {
		// questionInnerContainer.find('.group-name')
		// .text('Default group');
		// }
		// }

		var mainQuestion = Site.waveData.questions[mainQuestionId];
		if (mainQuestion == null) {
			for (q in Site.waveData.questionsList) {
				mainQuestion = Site.waveData.questionsList[q];
				break;
			}
		}

		var questionText = "";
		if (mainQuestion.buzzWord == null || mainQuestion.buzzWord == "") {
			questionText = mainQuestion.name;
		} else {
			questionText = mainQuestion.buzzWord;
		}
		$('.main-question').text(questionText);
		$('.main-question').attr('id', ('question' + mainQuestion.id));
		$('.questions-list li#question' + mainQuestion.id).remove();

		var groupsOptions = "";
		var answerGroups = Site.waveData.questions[mainQuestionId].answerGroups;
		if (answerGroups != null && answerGroups.length > 0) {
			// groupsOptions += '<li><a class="group-item"
			// href="#">Default Group</a></li>';
			$(answerGroups)
					.each(
							function(index, element) {
								if ($
										.isEmptyObject(Site.urlSplit.answerGroupsByQuestion)
										|| Site.urlSplit.answerGroupsByQuestion[mainQuestionId] == null) {
									if (index == 0) {

										$('#main-question').find('.group-name')
												.text(element);
									}

								} else {
									$('#main-question')
											.find('.group-name')
											.text(
													Site.urlSplit.answerGroupsByQuestion[mainQuestionId]);
								}
								if (Site.urlSplit.answerGroupsByQuestion[mainQuestionId] != this) {
									groupsOptions += '<li><a class="group-item" href="#">'
											+ Site.escapeHtml(this)
											+ '</a></li>';
								}

								// else {
								// groupsOptions += '<li><a class="group-item"
								// href="#">Default group</a></li>';
								// }
							});
			// groupsOptions = '<span>Groups</span><select class="">'
			// + groupsOptions + '</select>'
			$('#main-question').find('.group-data').show();

		} else {
			// $('#main-question').find('.group-name').text('Default group');
			$('#main-question').find('.group-data').hide();
		}
		$('#main-question').find('.ans-groups-list').empty().append(
				groupsOptions);

	},
	addCompareQuestion : function(questionId) {
		if ($('.intro-holder').length < 3) {
			// <div class="intro-holder"> <div
			// class="close-icon"> <a href="#"
			// class="maia-button
			// maia-button-secondary">X</a>
			// <h2 class="green-line"> Awareness of < BRAND>
			// as
			// a social network website<a href="#"
			// class="expand"></a> </h2> <div
			// class="expand-items"> <ul> <li>Awareness of
			// 1</li>
			// <li>Awareness of 2</li> <li>Awareness of
			// 3</li>
			// <li>Awareness of 4</li> </ul> </div> </div>
			// </div>

			var questionContainer = $('<div class="intro-holder compare-question-holder"></div>');
			var questionInnerContainer = $('<div class="close-icon"></div>');
			if (questionId == null) {
				var firstQuestionInList = $('.questions-list:last li').first();
				if (firstQuestionInList.length != 0) {
					if (Site.mainFlag == 2) {
						Site.color = "green-line";
						Site.questionIndex = 1;
					} else if (Site.mainFlag == 3) {
						Site.color = "red-line";
						Site.questionIndex = 2;
					}
				}
			} else {
				var firstQuestionInList = $('.questions-list:last li[id="question'
						+ questionId + '"]');
				if ($('.compare-question').length == 0) {
					Site.color = "green-line";
					Site.questionIndex = 1;
				} else if ($('.compare-question').length == 1) {
					Site.color = "red-line";
					Site.questionIndex = 2;
				}
			}

			if (firstQuestionInList.length != 0) {

				var lastIntroHolder = $('.intro-holder').last().find(
						'.expand-items');
				questionInnerContainer
						.append('<a href="#" class="maia-button maia-button-secondary remove-question">X</a>');

				var groupsListContainer = '<div class="group-data"><span class="group-date-info group-name"></span><a href="#" class="group-expand"></a></div>';

				questionInnerContainer
						.append('<h2 class="'
								+ Site.escapeHtml(Site.color)
								+ '"><div class="question-title"><span class="compare-question question-header Q'
								+ Site.questionIndex
								+ '"id="'
								+ Site.escapeHtml(firstQuestionInList
										.attr('id'))
								+ '">'
								+ Site.escapeHtml(firstQuestionInList.find('a')
										.text())
								+ '</span><a href="#" class="expand question-list-expand"></a></div>'
								+ groupsListContainer + '</h2>');
				questionContainer.append(questionInnerContainer);

				questionContainer.insertAfter($('.intro-holder').last());
				lastIntroHolder.find(
						'li[id="' + firstQuestionInList.attr('id') + '"]')
						.remove();

				$(
						'.questions-list li[id="'
								+ firstQuestionInList.attr('id') + '"]')
						.remove();

				questionContainer.append(lastIntroHolder.clone());

				// ////////////////////////////Question Groups
				// <div class="group-data">
				// <span class="group-date-info">Default Data</span><a href="#"
				// class="group-expand"></a>
				// </div>
				var groupsOptions = "";
				var answerGroups = Site.waveData.questions[firstQuestionInList
						.attr('id').replace('question', '')].answerGroups;
				if (answerGroups != null && answerGroups.length > 0) {
					// groupsOptions += '<li><a class="group-item"
					// href="#">Default Group</a></li>';
					$(answerGroups)
							.each(
									function(index, element) {
										if ($
												.isEmptyObject(Site.urlSplit.answerGroupsByQuestion)
												|| Site.urlSplit.answerGroupsByQuestion[questionId] == null) {
											// questionInnerContainer.find(
											// '.group-name').text(
											// 'Default group');
											if (index == 0) {

												questionInnerContainer.find(
														'.group-name').text(
														element);
											}
										} else {
											questionInnerContainer
													.find('.group-name')
													.text(
															Site.urlSplit.answerGroupsByQuestion[questionId]);
										}
										if (Site
												.escapeHtml(Site.urlSplit.answerGroupsByQuestion[questionId]) != Site
												.escapeHtml(this)) {
											groupsOptions += '<li><a class="group-item" href="#">'
													+ Site.escapeHtml(this)
													+ '</a></li>';
										}

										// else {
										// groupsOptions += '<li><a
										// class="group-item" href="#">Default
										// group</a></li>';
										// }
									});
					// groupsOptions = '<span>Groups</span><select class="">'
					// + groupsOptions + '</select>'

					questionInnerContainer.find('.group-data').show();
				} else {
					// questionInnerContainer.find('.group-name').text(
					// 'Default group')
					questionInnerContainer.find('.group-data').hide();
				}
				// ///////////////////////////////////////////////////
				groupsListContainer = $('<div class="expand-groups-items" style="display: none; "></div>');
				groupsExpandList = $('<ul class="ans-groups-list"></ul>');
				groupsExpandList.append(groupsOptions);
				groupsListContainer.append(groupsExpandList);
				questionContainer.append(groupsListContainer);

				if (questionId == null) {
					Site.singleDimensionChanges('question');
					EventBus.getInstance().fireEvent('initBrands', {
						callbackFunction : function() {
							Site.createRequest();

						},
						createBrandsRequest : true
					});
				} else {
					EventBus.getInstance().fireEvent('initBrands', {
						callbackFunction : function() {
						},
						createBrandsRequest : false
					});
				}

			}

		}
	},
	getCountriesToSetDefault : function() {
		alert("getCountriesToSetDefault")
		var query = $
				.get(
						"/site/country",
						function(data) {
							$('#location-set').empty().selectbox('detach');
							for (var i = 0; i < data.publishedLocations.length; i++) {

								$('#location-set')
										.append(
												'<option value="'
														+ data.publishedLocations[i].id
														+ '">'
														+ Site
																.escapeHtml(data.publishedLocations[i].name)
														+ '</option>');
							}

							$("#location-set").selectbox();
						}, "json");
	},
	registerCountryPopEvents : function() {
		// alert("registerCountryPopEvents")
		$('#location-popup')
				.live(
						'click',
						function() {

							// $.ajax({
							// url: '/site/country',
							// data: {
							// format: 'json'
							// },
							// error: function() {
							// alert("error")
							// },
							// dataType: 'jsonp',
							// success: function(data) {
							// alert("success")
							// },
							// type: 'GET'
							// });

							//							
							var query = $
									.get(
											"/site/country",
											function(data, s) {
												alert(s);
												$('#location-set').empty()
														.selectbox('detach');
												for (var i = 0; i < data.publishedLocations.length; i++) {

													if (data.publishedLocations[i].id == Site.defaultCountryId) {
														$('#location-set')
																.append(
																		'<option selected value="'
																				+ data.publishedLocations[i].id
																				+ '">'
																				+ Site
																						.escapeHtml(data.publishedLocations[i].name)
																				+ '</option>');
													} else {
														$('#location-set')
																.append(
																		'<option value="'
																				+ data.publishedLocations[i].id
																				+ '">'
																				+ Site
																						.escapeHtml(data.publishedLocations[i].name)
																				+ '</option>');
													}
												}
												$("#location-set").selectbox();
											}, "json");
							//
						});
	},
	registerNotificationEvents : function() {
		$('#close-video-notification').live("click", function(event) {
			if ($("#show-video-checkbox:checked").length > 0) {

				var query = $.ajax({
					url : "/site/setDefaultVideoNotification",
					type : "POST",
					data : {
						"tk" : $('#_tk').val()
					},
					success : function() {
						return false;
					}
				});
			}
			$('#notification-video').slideUp();
			event.preventDefault();
		})
	},
	registerEvents : function() {

		$('#legend-button')
				.live(
						'click',
						function() {
							// $('.highcharts-legend').toggle();
							Site.showLegend = !Site.showLegend;
							if ($(this).text() == "Show legend")
								$(this).text("Hide legend");
							else if ($(this).text() == "Hide legend")
								$(this).text("Show legend");
							if (window.location.href.indexOf("site/snapshot") != -1) {
								Site.chartOptions.legend.enabled = Site.showLegend;
								Site.chart1 = new Highcharts.Chart(
										Site.chartOptions);
								$(
										'.highcharts-legend-item > rect[fill="#FFFFFF"]')
										.each(
												function() {
													$(this)
															.prev()
															.attr(
																	'x',
																	parseInt($(
																			this)
																			.prev()
																			.attr(
																					'x')) - 15);
													$(this).attr('width', '2');
													$(this).parent().mouseout();
												});

							} else if (window.location.href
									.indexOf("site/trend") != -1) {
								Site.trendChartOptions.legend.enabled = Site.showLegend;
								Site.trendChart = new Highcharts.Chart(
										Site.trendChartOptions);
							}
						});

		$('#bar-to-stacked').live(
				"click",
				function() {
					$(this).addClass("selected");
					$('#stacked-to-bar').removeClass("selected");
					Site.chartOptions.plotOptions.series.stacking = "normal";
					Site.chartOptions.yAxis.max = null;
					if (result.counts.length > 1) {
						var tempSize = result.counts.length * 40 + 150;
					} else {
						var tempSize = filteredBrandsFilters.length * 40 + 150;
					}
					Site.chartOptions.chart.height = tempSize.toString();
					Site.bars = false;
					Site.chart1 = new Highcharts.Chart(Site.chartOptions);
					$('.highcharts-legend-item > rect[fill="#FFFFFF"]').each(
							function() {
								$(this).prev()
										.attr(
												'x',
												parseInt($(this).prev().attr(
														'x')) - 15);
								$(this).attr('width', '2');
								$(this).parent().mouseout();
							});
				});

		$('#stacked-to-bar').live(
				"click",
				function() {
					$(this).addClass("selected");
					$('#bar-to-stacked').removeClass("selected");
					Site.chartOptions.plotOptions.series.stacking = null;
					// Site.chartOptions.yAxis.max = 110;
					Site.chartOptions.chart.height = Site.size.toString();
					Site.chart1 = new Highcharts.Chart(Site.chartOptions);
					Site.bars = true;
					$('.highcharts-legend-item > rect[fill="#FFFFFF"]').each(
							function() {
								$(this).prev()
										.attr(
												'x',
												parseInt($(this).prev().attr(
														'x')) - 15);
								$(this).attr('width', '2');
								$(this).parent().mouseout();
							});
				});

		$('.compare-item').live('click', function() {

			var alreadyAcitveCompare = $('.compare-item.selected');
			alreadyAcitveCompare.siblings('.sum-item').addClass('selected');
			alreadyAcitveCompare.removeClass('selected');
			$(this).siblings('li').removeClass('selected');

			Site.singleDimensionChanges('division');

			if (!$(this).hasClass('selected')) {
				$(this).addClass('selected');
				Site.createRequest();
			}

		});

		$('.division-item').live('click', function() {
			if (!$(this).hasClass('selected')) {
				$(this).siblings('.division-item').removeClass('selected');
				$(this).addClass('selected');
				Site.createRequest();
			}
		});

		// $('.check-brand').change(function() {
		// var row2 = $("<tr> </tr>");
		// var col2 = $("<td>entered</td>");
		// row.append(col2);
		// $('#brandTable').append(row2);
		// $('.brand-input').each(function() {
		// if ($(this).is(':checked')) {
		// var row = $("<tr> </tr>");
		// var col = $("<td>" + $(this).parent().text() + "</td>");
		// row.append(col);
		// $('#brandTable').append(row);
		// }
		// });
		//
		// });

		$('.filter-item')
				.live(
						'change',
						function() {
							if ($(this).hasClass('brand-input')) {
								if ($('.brand-input:checked:visible').length <= 6
										&& $('.brand-input:checked:visible').length > 0) {
									Site.singleDimensionChanges("brand");
									Site.createRequest();
								} else {
									if ($('.brand-input:checked:visible').length == 0) {
										$(this).attr('checked', true);
									} else {
										$(this).attr('checked', false);
									}
								}
							} else if (this.id == 'wave-select'
									|| this.id == 'second-wave-select') {

								var secondWaveId = $('#second-wave-select')
								if (secondWaveId.length != 0) {
									secondWaveId = secondWaveId.val();
									if (parseFloat($(
											$('#wave-select option:selected'))
											.attr('id')) > parseFloat($(
											$('#second-wave-select option:selected'))
											.attr('id'))) {
										$('#second-wave-select').selectbox(
												'detach');
										$('#second-wave-select')
												.val(
														window.location.href
																.split('&')[2]
																.substring(3));
										$('#second-wave-select').selectbox();
										$('#wave-select').selectbox('detach');
										$('#wave-select')
												.val(
														window.location.href
																.split('&')[1]
																.substring(2));
										$('#wave-select').selectbox();
										return null;
									}
								}
								EventBus
										.getInstance()
										.fireEvent(
												'loadWaveData',
												{
													datasetId : $(
															'#wave-select')
															.val(),
													endDataSetId : secondWaveId,
													callbackFunction : function() {

														EventBus
																.getInstance()
																.fireEvent(
																		'initQuestions');

														EventBus
																.getInstance()
																.fireEvent(
																		'initBrands',
																		{
																			callbackFunction : function() {
																				Site
																						.createRequest();

																			},
																			createBrandsRequest : true
																		});
													}
												});
								//						
								// Site.createSnaphotWaveChangeRequest($('#wave-select')
								// .val(),
								// $('.main-question').attr('id').replace(
								// 'question', ''), function() {
								// });
							} else if (this.id == 'location-select') {
								EventBus.getInstance().fireEvent('initBrands',
										{
											callbackFunction : function() {
												Site.createRequest();

											},
											createBrandsRequest : true
										});
							}

						});

		$('.export-chart').click(function(event) {

			var value = parseInt($('#export-select').val());
			// Site.chart1.options.legend.borderWidth = 0;
			event.preventDefault();

			Site.exportChart(value);
		});

		$('#trend-it-button').click(function(event) {
			if (!$(this).hasClass('disable-button')) {
				var fullUrl = '/site/trend' + location.search;
				window.location = fullUrl;
			}
			event.preventDefault();
		});
		$('#trend-it-link').click(function(event) {
			var fullUrl = '/site/trend' + location.search;
			window.location = fullUrl;
			event.preventDefault();
		});

		$('#compare-question-button').click(function(event) {

			if ($('.intro-holder').length < 3) {
				$('.expand-items').hide();
				$('.expand').removeClass('colapse');

				// <div class="intro-holder"> <div
				// class="close-icon"> <a href="#"
				// class="maia-button
				// maia-button-secondary">X</a>
				// <h2 class="green-line"> Awareness of < BRAND>
				// as
				// a social network website<a href="#"
				// class="expand"></a> </h2> <div
				// class="expand-items"> <ul> <li>Awareness of
				// 1</li>
				// <li>Awareness of 2</li> <li>Awareness of
				// 3</li>
				// <li>Awareness of 4</li> </ul> </div> </div>
				// </div>

				Site.addCompareQuestion(null);
			}
			event.preventDefault();
		});

		$('.previous-val').live("click", function() {
			var sorting = [ [ 4, 0 ] ];
			var tableIndex = $(this).parents('table').attr('id');
			if ($(".hidden-check").text() == "Show hidden data") {
				Site.drawTrendTables(true, sorting, tableIndex, false);
			} else if ($(".hidden-check").text() == "Hide insufficient data") {
				Site.drawTrendTables(true, sorting, tableIndex, true);
			}
		});
		$('.change-val').live("click", function() {
			var sorting = [ [ 5, 0 ] ];
			var tableIndex = $(this).parents('table').attr('id');

			if ($(".hidden-check").text() == "Show hidden data") {
				Site.drawTrendTables(true, sorting, tableIndex, false);
			} else if ($(".hidden-check").text() == "Hide insufficient data") {
				Site.drawTrendTables(true, sorting, tableIndex, true);
			}
		});
		$('.current-val').live("click", function() {
			var sorting = [ [ 3, 0 ] ];
			var tableIndex = $(this).parents('table').attr('id');

			if ($(".hidden-check").text() == "Show hidden data") {
				Site.drawTrendTables(true, sorting, tableIndex, false);
			} else if ($(".hidden-check").text() == "Hide insufficient data") {
				Site.drawTrendTables(true, sorting, tableIndex, true);
			}

		});

		$('.answer-header').live("click", function() {
			var answerIndex = parseInt($(this).attr('id').substring(7), 10);
			var sorting = [ [ answerIndex, 0 ] ];
			var tableIndex = $(this).parents('table').attr('id');

			if ($(".hidden-check").text() == "Show hidden data") {
				Site.drawTables(true, sorting, tableIndex, false);
			} else if ($(".hidden-check").text() == "Hide insufficient data") {
				Site.drawTables(true, sorting, tableIndex, true);
			}
		});
		$('.brand-header').live("click", function() {
			var tableIndex = $(this).parents('table').attr('id');
			if ($(".hidden-check").text() == "Show hidden data") {
				Site.drawTables(false, null, tableIndex, false);
			} else if ($(".hidden-check").text() == "Hide insufficient data") {
				Site.drawTables(false, null, tableIndex, true);
			}
		});
		$('.brand-header-trend').live("click", function() {
			var tableIndex = $(this).parents('table').attr('id');

			if ($(".hidden-check").text() == "Show hidden data") {
				Site.drawTrendTables(false, null, tableIndex, false);
			} else if ($(".hidden-check").text() == "Hide insufficient data") {
				Site.drawTrendTables(false, null, tableIndex, true);
			}
		});
		$('.filter-header').live("click", function() {
			var sorting = [ [ 1, 1 ] ];
			var tableIndex = $(this).parents('table').attr('id');
			if ($(".hidden-check").text() == "Show hidden data") {
				Site.drawTables(true, sorting, tableIndex, false);
			} else if ($(".hidden-check").text() == "Hide insufficient data") {
				Site.drawTables(true, sorting, tableIndex, true);
			}
		});
		$('.filter-header-trend').live("click", function() {
			var sorting = [ [ 2, 1 ] ];
			var tableIndex = $(this).parents('table').attr('id');

			if ($(".hidden-check").text() == "Show hidden data") {
				Site.drawTrendTables(true, sorting, tableIndex, false);
			} else if ($(".hidden-check").text() == "Hide insufficient data") {
				Site.drawTrendTables(true, sorting, tableIndex, true);
			}
		});
		$(".hidden-check").live("click", function(e) {
			e.preventDefault();
			if ($(".hidden-check").text() == "Show hidden data") {

				if (window.location.href.indexOf("site/snapshot") != -1) {
					Site.drawTables(false, null, null, true);
				} else if (window.location.href.indexOf("site/trend") != -1) {
					Site.drawTrendTables(false, null, null, true);
				}
			} else if ($(".hidden-check").text() == "Hide insufficient data") {
				if (window.location.href.indexOf("site/snapshot") != -1) {
					Site.drawTables(false, null, null, false);
				} else if (window.location.href.indexOf("site/trend") != -1) {
					Site.drawTrendTables(false, null, null, false);
				}
			}
		});

		// jQuery('#container .highcharts-legend-item text').live("mouseover",
		// function() {
		//
		// // graphic position
		// // var coord = new Array(e.clientX, e.clientY - 40),
		// // serieName = jQuery(this).children('tspan').text();
		//		    
		// // get legend items
		// // var text = candleChart.getItemsDescription(serieName);
		//
		// // candleChart.buildTooltip(text, coord);
		// alert("in");
		//		    
		//
		// });

		$('.remove-question').live(
				'click',
				function(event) {
					var questionContainter = $(this).parents(
							'.compare-question-holder');
					var compareQuestion = questionContainter
							.find('.compare-question');
					questionContainter.find('.compare-question')
					$(".red-line").addClass("green-line").removeClass(
							"red-line");
					$('.questions-list').append(
							'<li id="' + compareQuestion.attr('id') + '">'
									+ '<a href="#" class="question-item">'
									+ Site.escapeHtml(compareQuestion.text())
									+ '</a>' + '</li>');
					questionContainter.remove();

					EventBus.getInstance().fireEvent('initBrands', {
						callbackFunction : function() {
							Site.createRequest();
						},
						createBrandsRequest : true
					});
					event.preventDefault();

				});

		$('.question-item').live(
				'click',
				function(event) {

					var questionText = $(this).html();
					var questionId = $(this).parents('li').attr('id');

					var introHolder = $(this).parents('.intro-holder');
					var questionHeader = introHolder.find('.question-header');

					var selectedQuestionText = questionHeader.html();
					var selectedQuestionId = questionHeader.attr('id');

					questionHeader.html(questionText);
					questionHeader.attr('id', questionId);

					var newLi = $('<li id="' + selectedQuestionId
							+ '"><a href="#" class="question-item">'
							+ selectedQuestionText + '</a></li>')

					$('.questions-list').append(newLi);
					$(this).parents('.intro-holder').find('.question-title')
							.click();

					$('.questions-list li[id="' + questionId + '"]').remove();

					newGroup = Site.waveData.questions[questionId.replace(
							"question", "")].answerGroups[0]
					if (newGroup == null) {
						newGroup = "";
					}
					$('#' + this.parentNode.id).parents('.intro-holder').find(// //////////////////////////////////////////////////////
					'.group-name').text(newGroup);

					// Site.showRelatedBrands(Site.waveData.questions[questionId
					// .replace('question', '')].brandsIds);

					EventBus.getInstance().fireEvent('initBrands', {
						createBrandsRequest : true,
						callbackFunction : function() {
							Site.createRequest();
						}
					});

					event.preventDefault();
				});

		$('.question-list-expand').live('click', function(event) {
			event.preventDefault();
		});

		$('.notification-undo').live('click', function(event) {
			History.back();
			$("#notification-container").slideToggle();
			event.preventDefault();
		});

		$('.group-item').live(
				'click',
				function(event) {
					var clickedGroup = $(this).text();
					var groupNameContainer = $(this).parents('.intro-holder')
							.find('.group-name');
					$(this).text(groupNameContainer.text());
					groupNameContainer.text(clickedGroup);

					$(this).parents('.intro-holder').find('.group-name')
							.click();

					Site.createRequest();

					event.preventDefault();
				});

		Site.registerExportTables();

		Site.registerCountryPopEvents();
	},
	removeCompareQuestions : function() {
		var questionsLists = $('.questions-list');

		$('.compare-question-holder').each(
				function() {
					var compareQuestion = $(this).find('.question-header');
					questionsLists.append('<li id="'
							+ compareQuestion.attr('id') + '">'
							+ '<a href="#" class="question-item">'
							+ compareQuestion.text() + '</a>' + '</li>');
					$(this).remove();
				});

		EventBus.getInstance().fireEvent('initBrands', {
			callbackFunction : null,
			createBrandsRequest : true
		});
	},
	showNotification : function(message, showUndo) {
		clearTimeout(Site.timeOut);
		if (!showUndo) {
			$("#notification-container").html(message).slideDown();
		} else {

			$("#notification-container")
					.html(
							message
									+ ' <a class="notification-undo" href="#">Undo</a>')
					.slideDown();
		}
		Site.timeOut = setTimeout('Site.slideUpNotification()', 15000);
	},
	singleDimensionChanges : function(selectedType) {
		var questionsResetmessage = "Compare questions have been reset.";
		var divisionsResetmessage = "All filters set to compare have been reset to \"All\".";
		var brandsResetMessage = "Brands have been reset to \"Google+\".";
		var locationsResetMessage = "Locations have been reset.";

		if (selectedType == "brand") {

			var compareDivisionsLength = $('.compare-item.selected').length;
			$('.compare-item.selected').removeClass('selected').siblings(
					'.sum-item').addClass('selected');

			var compareQuestionsLength = $('.compare-question').length;
			Site.removeCompareQuestions();
			if (compareQuestionsLength > 0) {
				message = questionsResetmessage;
			}
			if (compareDivisionsLength > 0) {
				message = divisionsResetmessage;
			}
			if (compareDivisionsLength + compareQuestionsLength > 0) {
				Site.showNotification(message = message, showUndo = true);
			}

			if ($('#location-select').val().length > 1) {
				message = locationsResetMessage;
				Site.showNotification(message = message, showUndo = true);
				$('#location-select option:not(:first)')
						.attr('selected', false);
				$('#location-select option:first').attr('selected', true);
			}

		} else if (selectedType == "division") {

			var checkedBrandsLength = $('.check-brand .brand-input:checked:visible').length;
			if (checkedBrandsLength > 1) {
				message = brandsResetMessage;
				$('.brand-input:checked:visible').attr('checked', false);
				$('.check-brand').filter(function() {
					return $(this).text().trim() == 'Google+';
				}).find('.brand-input').attr('checked', true);
			}
			var compareQuestionsLength = $('.compare-question').length;
			if (compareQuestionsLength > 0) {
				message = questionsResetmessage;
			}
			Site.removeCompareQuestions();

			if (checkedBrandsLength + compareQuestionsLength > 1) {
				Site.showNotification(message = message, showUndo = true);
			}

			if ($('#location-select').val().length > 1) {
				message = locationsResetMessage;
				Site.showNotification(message = message, showUndo = true);
				$('#location-select option:not(:first)')
						.attr('selected', false);
				$('#location-select option:first').attr('selected', true);
			}

		} else if (selectedType == "question") {

			var compareDivisionsLength = $('.compare-item.selected').length;
			$('.compare-item.selected').removeClass('selected').siblings(
					'.sum-item').addClass('selected');

			if (compareDivisionsLength > 0) {
				message = divisionsResetmessage;
			}

			var checkedBrandsLength = $('.check-brand .brand-input:checked:visible').length;
			if (checkedBrandsLength > 1) {
				message = brandsResetMessage;
				$('.brand-input:checked:visible').attr('checked', false);
				EventBus.getInstance().fireEvent('initBrands', {
					callbackFunction : null,
					createBrandsRequest : true
				});
				// $('.check-brand').filter(function() {
				// return $(this).text().trim() == 'Google+';
				// }).find('.brand-input').attr('checked', true);
			}

			if (checkedBrandsLength + compareDivisionsLength > 1) {
				Site.showNotification(message = message, showUndo = true);
			}

			if ($('#location-select').val().length > 1) {
				message = locationsResetMessage;
				Site.showNotification(message = message, showUndo = true);
				$('#location-select option:not(:first)')
						.attr('selected', false);
				$('#location-select option:first').attr('selected', true);
			}

		} else if (selectedType == "location") {

			var compareDivisionsLength = $('.compare-item.selected').length;
			$('.compare-item.selected').removeClass('selected').siblings(
					'.sum-item').addClass('selected');

			if (compareDivisionsLength > 0) {
				message = divisionsResetmessage;
			}

			if ($('.check-brand .brand-input:checked:visible').length > 1) {
				message = brandsResetMessage;
				$('.brand-input:checked:visible').attr('checked', false);
				$('.check-brand').filter(function() {
					return $(this).text().trim() == 'Google+';
				}).find('.brand-input').attr('checked', true);
			}

			var compareQuestionsLength = $('.compare-question').length;
			if (compareQuestionsLength > 0) {
				message = questionsResetmessage;
			}
			Site.removeCompareQuestions();

			if (checkedBrandsLength + compareDivisionsLength
					+ compareQuestionsLength > 1) {
				Site.showNotification(message = message, showUndo = true);
			}

		}
	},

	deselectSeriesFromExportChart : function(chart) {
		if (window.location.href.indexOf("site/snapshot") != -1) {
			var series1 = Site.chart1.series;
			var series2 = chart.series;
			for (var i = 0; i < series1.length; i++) {
				if (!series1[i].visible) {
					series2[i].hide();
				}
			}
		} else if (window.location.href.indexOf("site/trend") != -1) {
			var series1 = Site.trendChart.series;
			var series2 = chart.series;
			for (var i = 0; i < series1.length; i++) {
				if (!series1[i].visible) {
					series2[i].hide();
				}
			}
		}
	},
	preExport : function() {
		var tablesNamesObject = new Object();
		var tablesNames = "{";
		var urlParameters = "{";
		var tablesCount = $('.to-export').length;
		$('.to-export')
				.each(
						function(mainIndex) {
							tablesNamesObject[$(this).attr('class')] = $(
									'#question-table' + (mainIndex + 1)
											+ ' .table-question-name').text();
							// tablesNames += '"'
							// + $(this).attr('class')
							// + '":"'
							// + Site.escapeHtml($(
							// '#question-table' + (mainIndex + 1)
							// + ' .table-question-name')
							// .text()) + '"';
							// if (mainIndex != ($('.to-export').length - 1)) {
							// tablesNames += ", ";
							// }
							urlParameters += '"' + $(this).attr('class') + '"'
									+ ": [";
							var rowsCount = $(this).find("tr").length;
							$(this)
									.find("tr")
									.each(
											function(rowIndex) {
												var thLength = $(this).find(
														"th").length;
												if (thLength != 0) {
													urlParameters += "[";
													$(this)
															.find("th")
															.each(
																	function(
																			index) {
																		urlParameters += ('"'
																				+ $(
																						this)
																						.text() + '"')
																		if (index != (thLength - 1)) {
																			urlParameters += ", ";
																		}
																	});
													urlParameters += "]";
												}
												if ($(this).find("td").length != 0) {
													var tdLength = $(this)
															.find("td").length;
													if (tdLength != 0) {
														urlParameters += "[";
														$(this)
																.find("td")
																.each(
																		function(
																				index) {
																			urlParameters += ('"'
																					+ $(
																							this)
																							.text() + '"')
																			if (index != (tdLength - 1)) {
																				urlParameters += ", ";
																			}
																		});
														urlParameters += "]";
													}
												}
												if (rowIndex != (rowsCount - 1)) {
													urlParameters += ", ";
												}
											});

							urlParameters += "]";
							if (mainIndex != (tablesCount - 1)) {
								urlParameters += ", ";
							}
						});
		// tablesNames += "}";
		tablesNames = JSON.stringify(tablesNamesObject);
		urlParameters += "}";

		$("#hidden-json").val(urlParameters);
		$("#hidden-table-names").val(tablesNames);
	},
	registerExportTables : function() {
		$('#exporting-form').live('submit', function(event) {
			Site.preExport();
		});
	},

	initBrands : function(eventObject) {
		if (eventObject.createBrandsRequest) {
			var url = "/site/getbrands?";
			url += "q="
					+ $('.main-question').attr('id').replace('question', '');
			$('.compare-question').each(function(index, element) {
				url += "&";
				url += "q=" + element.id.replace('question', '');
			});
			url += "&";
			url += "w=" + $('#wave-select').val();
			if ($('#second-wave-select').length > 0) {
				url += "&";
				url += "ws=" + $('#second-wave-select').val();
			}

			var locsIds = $('#location-select').val();
			if (locsIds != null) {
				$(locsIds).each(function() {

					url += "&";
					url += "l=" + this;

				});
			}

			$.get(url, null, function(data) {
				// console.log(data.brands);
				Site.brands = data.brands;
				var brands = $(data.brands);
				$(".brand-input").parents("li").hide();
				$(brands).each(
						function() {
							$(".brand-input[value='" + this.id + "']").parents(
									"li").show();

							// $(".brand-input[value='" + this.id + "']").attr(
							// 'checked', false);
						});

				$(".brand-input:checked:hidden").attr('checked', false);

				if ($(".brand-input:checked:visible").length == 0) {
					$('.check-brand').filter(function() {
						return $(this).text().trim() == 'Google+';
					}).find('.brand-input').attr('checked', true);
				}

				Site.ScrollBar();
				if (eventObject.callbackFunction != null) {
					eventObject.callbackFunction();

				}
			}, "json");
		} else {
			var brands = Site.brands;
			$(".brand-input").parents("li").hide();
			$(brands).each(
					function() {
						$(".brand-input[value='" + this.id + "']")
								.parents("li").show();
						// $(".brand-input[value='" + this.id + "']").attr(
						// "checked", false)
					});

			$(".brand-input:checked:hidden").attr('checked', false);
			if ($(".brand-input:checked:visible").length == 0) {
				$('.check-brand').filter(function() {
					return $(this).text().trim() == 'Google+';
				}).find('.brand-input').attr('checked', true);
			}

			Site.ScrollBar();

			eventObject.callbackFunction();
		}

	},
	initWaveRelatedData : function() {

		var divisions = $(Site.waveData.divisions);
		var questions = Site.waveData.questionsList;
		var locations = $(Site.waveData.locations);

		// $('#wave-select').val(datasetId);

		$('.division-item.sum-item').addClass('selected');
		$('.division-item:not(".sum-item").compare-item').removeClass(
				'selected');
		$('.division-item:not(".sum-item"):not(".compare-item")').remove();

		divisions.each(function() {
			filterId = this.filterId;
			if (this.name.toLowerCase().indexOf("all ") == 0
					|| this.name.toLowerCase() == "all") {
				$('#filter' + filterId + ' .filters-list .sum-item').attr('id',
						this.id);
			} else {
				$('#filter' + filterId + ' .filters-list').append(
						'<li id="division' + this.id
								+ '" class="division-item">' + this.name
								+ '</li>');
			}

		});
		$('.filters-list').each(function() {
			if ($(this).find('li').length % 2 != 0) {
				$(this).find('li:last').addClass("full");
			}

		});
		$('.questions-list').empty();
		$('.compare-question-holder').remove();

		$('.main-question').text("");
		$('.main-question').attr('id', ('question'));

		Site.renderQuestions(questions);

		$('#location-select').empty();

		locations.each(function(index, element) {
			$('#location-select').append(
					'<option value="' + this.id + '">'
							+ Site.escapeHtml(this.name) + '</option>');
		});
		var locationIsSelected = false;
		if (Site.urlSplit.location != null
				&& Site.urlSplit.location.length != 0) {
			for (var i = 0; i < Site.urlSplit.location.length; i++) {

				// console.log(Site.urlSplit.location[i]);
				if ($('#location-select option[value='
						+ Site.urlSplit.location[i] + ']').length != 0) {
					$(
							'#location-select option[value='
									+ Site.urlSplit.location[i] + ']').attr(
							'selected', 'selected');
					locationIsSelected = true;
				}
			}
			if (!locationIsSelected) {

				// console.log("not found it");
				if ($('#location-select option[selected="selected"]').length == 0) {
					$('#location-select option').first().attr('selected',
							'selected');
				}
			}
		} else {
			if ($('#location-select option[selected="selected"]').length == 0) {
				$('#location-select option').first().attr('selected',
						'selected');
			}
		}
		Site.ScrollBar();
	},

	createSnaphotWaveChangeRequest : function(eventObject) {
		$.ajax({

			url : "dataset/" + eventObject.datasetId + "/related",
			async : true,
			success : function(result) {
				// window.console.log(result);
				if (result.showInSnapshotOnly) {
					$("#trend-it-button").addClass('disable-button');
				} else {
					$("#trend-it-button").removeClass('disable-button');
				}
				Site.waveData = result;

				Site.initWaveRelatedData();
				if (eventObject.callbackFunction != null) {
					eventObject.callbackFunction();
				}
				// Site.initData();

			},
			error : Site.error,
			dataType : 'json',
			type : 'GET'
		});
	},

	createTrendWaveChangeRequest : function(eventObject) {
		$.ajax({
			url : "dataset/" + eventObject.datasetId + "/"
					+ eventObject.endDataSetId + "/related",
			async : true,
			success : function(result) {
				Site.waveData = result;
				Site.initWaveRelatedData(eventObject.mainQuestionId);
				if (eventObject.callbackFunction != null) {
					eventObject.callbackFunction();
				}
				// Site.initData();

			},
			error : Site.error,
			dataType : 'json',
			type : 'GET'
		});
	},

	error : function(jqXHR, textStatus, errorThrown) {
		// location.href = "/error/err?status=" + textStatus;
	},

	initCompareQuestionButton : function() {
		if ($('.intro-holder').length < 3) {
			$('#compare-question-button').removeClass('disable-button');
		} else {
			$('#compare-question-button').addClass('disable-button');
		}
	},

	showLoadingOverlay : function() {
		$('div.loading').show();
	},

	hideLoadingOverlay : function() {
		$('div.loading').hide();
	},
	// getCustomPages : function() {
	// $.get("/site/getPagesForSite", function(data) {
	// $('.pages-links').empty();
	// if (data != null) {
	// for (var i = 0; i < data.names.length; i++) {
	//	
	// $('.pages-links').append(
	// "<a href=/site/pages/"
	// + encodeURIComponent(data.names[i]) + ">"
	// + data.names[i] + "</a>");
	// if (i != data.names.length - 1) {
	// $('.pages-links').append(" | ");
	// }
	// }
	// }
	// }, "json");
	// },
	getCustomPages : function() {
		// alert("getCustomPages")
		$.ajax({
			type : "GET",
			url : "/site/getPagesForSite",
			dataType : "json",
			success : function(data) {
				$('.pages-links').empty()
				if (data != null) {
					for (var i = 0; i < data.names.length; i++) {
						$('.pages-links').append(
								"<a href=/site/pages/"
										+ encodeURIComponent(data.names[i])
										+ ">" + data.names[i] + "</a>");
						if (i != data.names.length - 1) {
							$('.pages-links').append(" | ");
						}

					}
				}

			}

		});
	},

	exportChart : function(value) {
		switch (value) {

		case 0:
			// var serializer = new XMLSerializer();
			// var svg = $('.export-div svg').clone();
			// var svnStr = serializer.serializeToString(svg[0]);
			// var svgWidth = svg.attr('width') + 200;
			// svg.attr('width', svgWidth);
			// var svgHeight = svg.attr('height');
			//
			// var canv = $('<canvas id="canv"></canvas>');
			// canv.hide();
			// canv.attr('width', svgWidth + 'px');
			// canv.attr('height', svgHeight + 'px');
			// canv.appendTo('body');
			//
			// canvg(canv[0], svnStr);
			// var data = document.getElementById('canv').toDataURL();
			//
			// canv.remove();
			//
			// var imageDataInput = $('<input name="svg" type="hidden" />');
			//
			// imageDataInput.val(data);
			var form = $('#exporting-form');
			form.attr('action', '/site/export');

			// form.append(imageDataInput);

			$('#exporting-form').submit();
			parent.$.fancybox.close();
			break;
		case 1:

			if (window.location.href.indexOf("site/snapshot") != -1) {
				$('.export-div').show();
				Site.drawSnapshotForExport(forDrive = true);
				$('.export-div').hide();
			} else if (window.location.href.indexOf("site/trend") != -1) {
				$('.tc-container-export').show();
				Site.drawTrendChartsForExport(forDrive = true);
				$('.tc-container-export').hide();

			}

			break;
		case 2:
			// Site.chart1.exportChart({
			// type : 'image/png',
			// filename : 'new-chart'
			// });
			if (window.location.href.indexOf("site/snapshot") != -1) {
				$('.export-div').show();
				Site.drawSnapshotForExport(forDrive = false);
				$('.export-div').hide();
			} else if (window.location.href.indexOf("site/trend") != -1) {
				$('.tc-container-export').show();
				Site.drawTrendChartsForExport(forDrive = false);
				$('.tc-container-export').hide();

			}
			break;
		case 3:
			Site.chart1.exportChart({
				type : 'image/jpeg',
				filename : 'new-chart'
			});
			break;
		case 4:
			Site.chart1.exportChart({
				type : 'image/svg+xml',
				filename : 'new-chart'
			});
			break;
		}
		return false;
	},

	dashboardFunctionalities : function() {
//		alert("dashboard")
		$('.add-widget')
				.live(
						"click",
						function(event) {

							$.fancybox({
								content : $("#add-new-widget"),
								closeClick : false,
								openEffect : 'none',
								closeEffect : 'none',
								helpers : {
									overlay : {
										closeClick : false,
									}
								}
							});

							// get the list of questions
							var questionArray = new Array();
							$('.question-header').each(function() {
								questionArray.push($(this).html());
							});

							$('#questions-list').val(questionArray);

							// get the list of brands
							var selectedBrands = new Array();
							$('.brand-input:checked').each(function() {
								selectedBrands.push($(this).parent().text());
							});
							$('#brands-widget-list').val(selectedBrands);

							// get the list of filters
							var filterText = "";
							var filterTextInTable = "";
							var filterList = new Array();
							var compare = false;
							$('.division-item.selected:not(".sum-item")')
									.each(
											function(index, elem) {
												var b = 1;
												var x = $(elem).text();
												if ($(elem).text() != "Compare") {
													filterText += Site
															.escapeHtml($(elem)
																	.text())
															+ ", ";
													filterTextInTable += Site
															.escapeHtml($(elem)
																	.text())
															+ ", ";
												} else {
													compare = true;
													filtersCount = $(elem)
															.siblings().length - 1;
													filterText += "All sub-filters of "
															+ Site
																	.escapeHtml($(
																			elem)
																			.parent()
																			.prev(
																					"h4")
																			.text())
															+ ", ";
												}
											});
							filterText = filterText.substring(0,
									filterText.length - 2);
							filterTextInTable = filterTextInTable.substring(0,
									filterTextInTable.length - 2);
							if (compare) {
								$('.compare-item.selected')
										.siblings(':not(".sum-item")')
										.each(
												function(index, elem) {
													if (filterTextInTable.length != 0)
														filterList
																.push($(elem)
																		.text()
																		+ ", "
																		+ filterTextInTable);
													else
														filterList.push($(elem)
																.text());
												});
							} else {
								if (filterText == "")
									filterText = "No filters";
								filterList.push(filterText);
							}

							$('#filtersAndDivisions-list').val(filterList);

							var waveList = new Array();
							waveList.push($('#wave-select option:selected')
									.text());
							waveList.push($(
									'#second-wave-select option:selected')
									.text());

							$('#waves-list').val(waveList);

							var countryList = new Array();
							$('#location-select option:selected').each(
									function() {
										countryList.push($(this).text());
									});
							$('#countries-list').val(countryList);

							var query = $
									.get(
											"/site/dashboard/getall",
											function(data) {
												$('#user-dashboards').empty()
														.selectbox('detach');
												if (data.defaultDashboards.length > 0) {
													$('#user-dashboards')
															.append(
																	"<optgroup label='My Dashboards'>");
												}
												for (var i = 0; i < data.dashboards.length; i++) {

													$('#user-dashboards')
															.append(
																	'<option value="'
																			+ data.dashboards[i].id
																			+ '">'
																			+ Site
																					.escapeHtml(data.dashboards[i].name)
																			+ '</option>');
												}
												if (data.defaultDashboards.length > 0) {
													$('#user-dashboards')
															.append(
																	"</optgroup><optgroup label='Default Dashboards'>");
													for (var i = 0; i < data.defaultDashboards.length; i++) {

														$('#user-dashboards')
																.append(
																		'<option value="'
																				+ data.defaultDashboards[i].id
																				+ "_"
																				+ data.defaultDashboards[i].userId
																				+ '">'
																				+ Site
																						.escapeHtml(data.defaultDashboards[i].name)
																				+ '</option>');
													}
													$('#user-dashboards')
															.append(
																	"</optgroup>");
												}
												$("#user-dashboards")
														.selectbox();
											}, "json");
							if (window.location.href.indexOf("site/snapshot") != -1) {
								var stacked;
								if ($('#stacked-to-bar').hasClass('selected')) {
									stacked = false;
								} else {
									stacked = true;
								}

								$('#widget-url').val(
										Site.createSnapshotRequestUrl('')
												+ "&stacked=" + stacked);
								$('#trend-tab').hide();
								$('#snap-tab').show();
								$("#snap-or-trend").empty();
								$("#snap-or-trend").val("snap");
							}
							if (window.location.href.indexOf("site/trend") != -1) {
								$('#widget-url').val(
										Site.createTrendRequestUrl(''));
								$('#trend-tab').show();
								$('#snap-tab').hide();
								$("#snap-or-trend").empty();
								$("#snap-or-trend").val("trend");
							}
							$("#widget-title").val("");
							event.preventDefault();
						});
		$('#widget-chart-button').live('click', function(event) {
			$('#table-or-chart').val("chart");
			$('#widget-chart-button').attr('disabled', 'disabled');
			$('#widget-table-button').removeAttr('disabled');
			event.preventDefault();
		});
		$('#widget-table-button').live('click', function(event) {
			$('#widget-table-button').attr('disabled', 'disabled');
			$('#widget-chart-button').removeAttr('disabled');
			$('#table-or-chart').val("table");
			event.preventDefault();
		});

		$("#save-to-dashboard").live("click", function(event) {
			event.preventDefault();
			$('#add-dashboard-form').submit();
			return false;
		});

		$('#add-dashboard-form').submit(function(event) {
			if ($(this).valid()) {
				var $form = $(this);
				var $inputs = $form.find("input, select, button, textarea");
				var serializedData = $form.serialize();
				$inputs.prop("disabled", true);
				request = $.ajax({
					url : "/site/dashboard/addnewwidget",
					type : "post",
					dataType : 'json',
					data : serializedData,
					success : function() {
						$inputs.prop("disabled", false);
						parent.$.fancybox.close();
						return false;
					}
				});
			}
			return false;
		});
	},

	escapeHtml : function(unsafe) {
		if (unsafe == null) {
			return null;
		}
		unsafe = unsafe.toString();

		return unsafe.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(
				/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#039;");
	}

}
/*
 * * Event Bus used to deliver events to all parts of the application through
 * event registration and handling
 * 
 * @returns {EventBus}
 */
function EventBus() {
	// The array that will contain the names of events and their associated
	// handlers
	this.events = new Object();
}

// Will contain the one and only instance of the class
EventBus.instance = null;

// This function ensures that I always use the same instance of the object
EventBus.getInstance = function() {
	if (EventBus.instance == null) {
		EventBus.instance = new EventBus();
	}

	return EventBus.instance;
};

// EventBus.prototype allows you to add instance functions to the EventBus
// without having to
// define it within the EventBus class{}
// Function to register a handler
EventBus.prototype.registerHandler = function(eventName, object, methodName) {

	if (this.events[eventName] != null) {
		var eventHandler = {
			handlerObject : object,
			method : methodName
		};
		this.events[eventName].push(eventHandler);
	} else {
		var eventHandler = {
			handlerObject : object,
			method : methodName
		};
		this.events[eventName] = [ eventHandler ];
	}
};

// Function to fire an event to all handlers registered to this event while
// supplying the event as the parameter
EventBus.prototype.fireEvent = function(eventName, eventObject) {

	if (this.events[eventName] != null) {
		for (var i = 0, leni = this.events[eventName].length; i < leni; i++) {
			var eventHandler = this.events[eventName][i];

			// this executes the handler code and keeps the "this" reference
			// pointing to the handler object
			eventHandler.handlerObject[eventHandler.method](eventObject,
					eventHandler.handlerObject);
		}
	}
};

$(document).ready(function() {

	Site.init();
	/*
	 * if (($.browser.msie) && ($.browser.version <= "9.0")) { //
	 * $('input[placeholder], textarea[placeholder]').placeholder();
	 * 
	 * var CSS3PIE_selectors = [ '.user-login', '.orange-button', '.left-nav li
	 * span', '.setting-icon' ]; $(CSS3PIE_selectors.join(',')).each(function() {
	 * PIE.attach(this); }); }
	 */

});

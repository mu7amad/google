var dashBoards = new Object();
var chartsCount = 0;
$('.add-widget').live("click", function(event) {
	alert("entered");
});
$('#test').live("click", function(event) {
	init();
});

/** ***************video configurations****************** */

// $('#location-popup')
// .live(
// 'click',
// function() {
// var query = $
// .get(
// "/site/country",
// function(data) {
// $('#location-set').empty().selectbox(
// 'detach');
// for ( var i = 0; i < data.publishedLocations.length; i++) {
//
// if (data.publishedLocations[i].id == Site.defaultCountryId) {
// $('#location-set')
// .append(
// '<option selected value="'
// + data.publishedLocations[i].id
// + '">'
// + data.publishedLocations[i].name
// + '</option>');
// } else {
// $('#location-set')
// .append(
// '<option value="'
// + data.publishedLocations[i].id
// + '">'
// + data.publishedLocations[i].name
// + '</option>');
// }
// }
// $("#location-set").selectbox();
// }, "json");
//
// });
var checkVideoCookie = function() {
	var i, x, y, ARRcookies = document.cookie.split(";");
	for (i = 0; i < ARRcookies.length; i++) {
		x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
		y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
		x = x.replace(/^\s+|\s+$/g, "");
		var foundVideoCookie = false;
		if (x == ("showVideo_" + $('#UI').text())) {
			if (y == "true") {
				foundVideoCookie = true;
				$('#notification-video').slideDown();
			}
			foundVideoCookie = true;
		}

	}
};

$('#close-video-notification').live("click", function(event) {
	if ($("#show-video-checkbox:checked").length > 0) {

		var query = $.ajax({
			url : "/site/setDefaultVideoNotification",
			type : "POST",
			data : {
				"tk" : $('#_tk').val()
			},
			success : function() {
				return false;
			}
		});
	}
	$('#notification-video').slideUp();
	event.preventDefault();
});

/** ***************set default dashboard Functions****************** */
var confirmDefault = function(name) {
	var x = confirm("Are you sure you want to set \"" + name
			+ "\" as default dashboard?");
	if (x) {
		return true;
	} else {
		return false;
	}
};
$('.setDefaultDashBoard').live("click", function(event) {
	event.preventDefault();
	var url = $(this).attr('href');
	var name = $('#dashboards').find('.active').find('h3 span').text();
	var id = $('#dashboards').find('.active').find('h3 input.id').val();
	var userConfirmation = confirmDefault(name);
	if (userConfirmation) {
		alert($('#_csrf').val());
		request = $.ajax({
			url : url + id,
			type : "POST",
			data : {
				"tk" : $('#_tk').val(),
				"_csrf" : $('#_csrf').val()
			},
			success : function() {
				return false;
			}
		});
		$('.setting-content').slideUp('medium');
		return false;
	} else {
		$('.setting-content').slideUp('medium');
		return false;
	}
});

/** ***************Delete Functions****************** */

var ConfirmDelete = function(name) {
	var x = confirm("Are you sure you want to delete \"" + name + "\"?");
	if (x) {
		return true;
	} else {
		return false;
	}
};

$('.delete')
		.live(
				"click",
				function(event) {
					alert("xxxxxx")
					event.preventDefault();

					if ($('#dashboards .filter-slider').size() == 1
							&& $(this).parents(".dash-block-wrap").length == 0
							&& !$('.active').hasClass('default-dashboard')
							&& !$('.active').hasClass('shared-dashboard')) {
						alert("You must at least have one dashboard in your dashboards.")
					}

					else {

						var name = "";
						var url = $(this).attr('href');
						var value = $('.active').find('.id').val();
						var userId = $('.active').find('.userId').val();
						var object = $(this);

						if (url.indexOf("widget") !== -1) {
							name = $(this).parent().find('span').text();
							var rawWidgetId = $(this).parents(
									'.dash-block-wrap').attr('id');
							var index = rawWidgetId.lastIndexOf("-");
							var widgetId = rawWidgetId.substring(index + 1);
							url += value + "/" + widgetId;
							url += "?userId=" + userId
						}

						else {
							if ($('.active').parents('#shared-dashboards').length > 0) {
								name = $('.active').find('h3 span').text();
								url += value;
								url += "?userId=" + userId + "&shared=true";

							} else {
								name = $('.active').find(
										'.filter-slider h3 span').text();
								url += value;
								url += "?userId=" + userId
							}
						}

						var userConfirmation = ConfirmDelete(name);
						if (userConfirmation) {
							alert($('#_csrf').val())
							request = $
									.ajax({
										url : url,
										data : {
											tk : $("#_tk").val(),
											"_csrf" : $('#_csrf').val()
										},
										type : "post",
										success : function() {

											if (url.indexOf("widget") == -1) {
												removeFromLeftNav(name);
											}

											else {
												$(object)
														.parents(
																'.dash-block-wrap')
														.slideUp(
																'medium',
																function() {
																	$(object)
																			.remove();
																});
												$('.active')
														.find('.identity')
														.each(
																function() {
																	if ($(this)
																			.val() == widgetId) {
																		$(this)
																				.parent(
																						"li")
																				.slideUp(
																						'medium',
																						function() {
																							$(
																									this)
																									.remove();
																						});
																	}
																});
											}
											// if ($('.container-0').length ==
											// 0) {
											// $('#instructions-div').show();
											// }
											return false;
										}
									});
							$('.setting-content').slideUp('medium');
							return false;
						} else {
							$('.setting-content').slideUp('medium');
							return false;
						}
					}
				});

var removeFromLeftNav = function() {
	var toRemove = $('.filter-slider').parent('.active');

	if ($('.left-nav').find('.active').next('li').length > 0) {
		var name = $('.left-nav').find('.active').next().find(
				'.filter-slider h3').click();
	} else if ($('.left-nav').find('.active').prev('li').length > 0) {
		var name = $('.left-nav').find('.active').prev().find(
				'.filter-slider h3').click();
	} else {
		$('.left-side .filter-all h3.new-exp').first().click();
	}
	$(toRemove).remove();
};

/** ************* Generic Form Submit ************** */

$(".save-popup-form").live("click", function(event) {
	event.preventDefault();
	var id = $(this).parents('form').attr('id');

	if (id == "edit-form") {
		// var name = $(this).parents('form').find('.name').val();
		value = $('.active').find('.id').val();
		$("#hidden-id").val(value);
		// changeDashboardTitle(name);
	}

	else if (id == "edit-widget-form") {
		var name = $(this).parents('form').find('.name').val();
		value = $('.active').find('.id').val();
		$(this).parents('form').find(".hidden-id").val(value);
		var widgetId = $(this).parents('form').find(".hidden-widget-id").val();
		$('#widget-wrapper-' + widgetId).find("h2 span").text(name);

		$('.active').find('.identity').each(function() {
			if ($(this).val() == widgetId) {
				$(this).parent().find(".widget-focus").text(name);
			}
		});

	}

	else if (id == "update-widget-form") {
		var name = $(this).parents('form').find('.name').val();
		value = $('.active').find('.id').val();
		$(this).parents('form').find(".hidden-id").val(value);
	}

	else if (id == "set-default-dashboard-form") {
		value = $('.active').find('.id').val();
		$(this).parents('form').find(".hidden-id").val(value);
	}

	$('#' + id).submit();
	return false;
});

$('.form')
		.submit(
				function(event) {
					if ($(this).valid()) {
						var $form = $(this);
						var $inputs = $form
								.find("input, select, button, textarea");
						var serializedData = $form.serialize();
						var name = $form.find('.name').val();
						$inputs.prop("disabled", true);

						request = $
								.ajax({
									url : $(this).attr('action'),
									type : "post",
									data : serializedData,
									dataType : "json",
									success : function(data) {
										$inputs.prop("disabled", false);
										parent.$.fancybox.close();

										if ($form.attr('id') == "edit-form") {
											var name = $form.find('.name')
													.val();
											changeDashboardTitle(name);
										} else if ($form.attr('id') == "update-widget-form") {
											window.location.reload(true);
										}

										else if ($form.attr('id') == "set-default-dashboard-form") {
											window.location.reload(true);
										}

										else if ($form.attr('id') == "add-dashboard-form") {
											addDashboardToLeftNav(
													data.dashboardName,
													data.userId,
													data.dashboardId);
										}
										return false;
									}
								});
					}
					return false;
				});

$('.edit-widget-btn').live("click", function(e) {

	var rawWidgetId = $(this).parents('.dash-block-wrap').attr('id');
	var index = rawWidgetId.lastIndexOf("-");
	var widgetId = rawWidgetId.substring(index + 1);
	$('#edit-widget-form').find(".hidden-user-id").val("");

	if ($('.active').hasClass('default-dashboard')) {
		var userId = $('.active').find('.userId').val();
		$('#edit-widget-form').find(".hidden-user-id").val(userId);
	}
	$('#edit-widget-form').find(".name").val("");
	$('#edit-widget-form').find(".hidden-widget-id").val(widgetId);
});
$('.setLocationDefaultDashBoard')
		.live(
				"click",
				function() {
					request = $
							.ajax({
								url : "/site/dashboard/getlocations",
								type : "get",
								dataType : "json",
								success : function(data) {
									$('#locations-select').empty().selectbox(
											'detach');
									for (var i = 0; i < data.locations.length; i++) {
										$('#locations-select')
												.append(
														"<option value='"
																+ data.locations[i].id
																+ "'>"
																+ Site
																		.escapeHtml(data.locations[i].name)
																+ "</option>");
									}
									$('#locations-select').selectbox();
									return false;
								}
							});
				});
$('.setWidgetsOrder')
		.live(
				"click",
				function() {

					var dashboardId = $('.active').find('.id').val();
					var userId = $('.active').find('.userId').val();

					$('form.order-widget-form').attr('action',
							'/site/dashboard/' + dashboardId + '/orderwidgets');
					$('form.order-widget-form').find(".hidden-user-id").val(
							userId);
					$('#widgets-sortable').empty();
					$(
							'#dashboards li.active,#default-dashboards li.active,#shared-dashboards li.active')
							.find('.widget-name')
							.each(
									function(index, element) {
										var widgetNameSpan = $("<span></span>");
										widgetNameSpan.text($(element).text());
										name = "rom[${resource.id }]"
										var widgetOrderInput = $("<input type='hidden'></input>");
										widgetOrderInput.attr('name', "rom["
												+ $(element).siblings(
														'input.identity').val()
												+ "]");
										widgetOrderInput.addClass('order');
										widgetOrderInput.val(index);

										var widgetLi = $("<li></li>");
										widgetLi
												.addClass('ui-wiget-item-state-default');

										widgetLi.append(widgetNameSpan);
										widgetLi.append(widgetOrderInput);

										$('#widgets-sortable').append(widgetLi);
									});

					$('#widgets-sortable').sortable(
							{
								update : function(event, ui) {
									$('.ui-wiget-item-state-default').each(
											function(index, element) {
												$(this).find('.order').val(
														index);
											});
								}
							});

					// $('#widgets-scrollbar').tinyscrollbar();

				});
$('.update-widget-btn')
		.live(
				"click",
				function() {
					var rawWidgetId = $(this).parents('.dash-block-wrap').attr(
							'id');
					var index = rawWidgetId.lastIndexOf("-");
					var widgetId = rawWidgetId.substring(index + 1);
					$('#update-widget-form').find(".hidden-widget-id").val(
							widgetId);
					var dashboardid = $('.active').find('.id').val();
					var userId = null;
					var forDefault = false;
					if ($('.active').hasClass('default-dashboard')) {
						forDefault = true;
						userId = $('.active').find('.userId').val();
						$('#edit-form').find(".hidden-user-id").val(userId);
					}

					var snap_or_trend = "";
					if ($(this).parents('.dash-block-wrap').find(
							'.dash-block-cont').hasClass("snap")) {
						snap_or_trend = "snapshot";
					} else {
						snap_or_trend = "trend";
					}
					var widgetURL = "";
					$('.active').find('.identity').each(
							function() {
								if ($(this).val() == widgetId) {
									widgetURL = $(this).parent("li").find(
											'.url').val();
								}
							});
					$('#update-options-list').find("tr").each(function() {
						$(this).remove();
					});
					$('#update-header').text("");
					if (forDefault) {
						widgetURL += "&userId=" + userId;
					}
					request = $
							.ajax({
								url : "/site/dashboard/checkforupdate?widgetid="
										+ widgetId
										+ "&dashboardid="
										+ dashboardid
										+ "&s_or_t="
										+ snap_or_trend + "&" + widgetURL,
								type : "get",
								async : false,
								dataType : "json",
								success : function(data) {
									var counter = 0;
									for (key in data.options) {
										for (i = 0; i < data.options[key].dataSetsNames.length; i++) {
											if (counter == 0) {
												$('#update-header')
														.text(
																"The data of this question has been"
																		+ " updated since you've last modified this widget.");
												$('#update-options-list')
														.append(
																"<tr><th></th><th>Name</th><th>Date</th></tr>");
											}
											var date = new Date(
													data.lastModificationDates[counter]);
											$('#update-options-list')
													.append(
															"<tr><td class='tiny-col'>"
																	+ "<input type='radio' name='selected-option' checked='true' value='"
																	+ key
																	+ "' group='update-options'> </input>"
																	+ "</td><td>"
																	+ Site
																			.escapeHtml(data.options[key].dataSetsNames[i])
																	+ "</td>"
																	+ "<td>"
																	+ $.format
																			.date(
																					date,
																					"dd-MM-yyyy")
																	+ "</td></tr>");

											counter++;
										}
									}
									if (counter == 0) {
										$('#update-header').text(
												"No updates available.");
										$('#update-form-btn').prop("disabled",
												true);
									} else {
										$('#update-form-btn').prop("disabled",
												false);
									}
									return false;
								}
							});
				});

var addDashboardToLeftNav = function(name, userId, dashboardId) {

	$('#dashboards li.active').removeClass('active');

	var count = $('#dashboards').find(".filter-slider").length;
	var listItem = " <li class='active'>" + "<div class='filter-slider'>"
			+ "<h3>" + "	<span>"
			+ Site.escapeHtml(name)
			+ "</span><a href='#'"
			+ "		class='expand colapse'></a> "
			+ "<input type='hidden' value='"
			+ dashboardId
			+ "' class='id'>"
			+ "<input type='hidden' value='"
			+ userId
			+ "' class='userId'>"
			+ "</h3>"
			+ "<div id='scrollbars"
			+ count
			+ "' class='expand-list'"
			+ "	style='display: none;'>"
			+ "	<div class='scrollbar'>"
			+ "		<div class='track'>"
			+ "			<div class='thumb'>"
			+ "				<div class='end'></div>"
			+ "			</div>"
			+ "		</div>"
			+ "	</div>"
			+ "	<div class='viewport'>"
			+ "		<div class='overview'>"
			+ "		</div>"
			+ "	</div>"
			+ "</div>"
			+ "</div>" + "</li>";

	$('#dashboards').append(listItem);
	$('#dashboards li.active .filter-slider h3').click()
}

var changeDashboardTitle = function(name) {
	$('.dashboard-title').text(name);
	value = $('.active').find('h3').find('span').text(name);
}

$('#add-dashboard-btn').live("click", function() {
	$("#add-dashboard-form").find(".name").val("");
});
$('.dashboard-title').live("click", function() {
	$("#edit-form").find(".name").val("");
	if ($('.active').hasClass('default-dashboard')) {
		var userId = $('.active').find('.userId').val();
		$('#edit-form').find(".hidden-user-id").val(userId);
	}

});

/** *************************************************** */

$(".widget-focus").live("click", function(event) {
	event.preventDefault();
	var id = $(this).parent().find('.identity').val();
	$(".widget-focus").removeClass("current");

	$('.dash-block-wrap').each(function(index, elem) {
		if ($(elem).attr('id') != ('widget-wrapper-' + id)) {
			$('html, body').animate({
				hide : $(elem).hide("fast")
			}, 2000);
		} else {
			$('html, body').animate({
				show : $(elem).show("fast")
			}, 2000);
		}
	});

	$(this).addClass("current");
	// $('html, body').animate({
	// // scrollTop : $(('#widget-wrapper-' + id)).offset().top
	//
	// }, 500);
});

var drawAllCharts = function() {
	$('.dash-block-cont')
			.each(
					function(index) {

						var url = $(this).find('.url').val();
						url += "&forDashBoard=true";
						var htmlElement = $(this);
						if (htmlElement.attr('class').indexOf('snap') != -1) {
							url = '/site/query?' + url;
						} else if (htmlElement.attr('class').indexOf('trend') != -1) {
							// alert("drawAllCharts")
							url = '/site/trendquery?' + url;
						}
						$
								.ajax({
									cache : false,
									async : true,
									url : url,
									type : 'GET',
									dataType : 'json',
									success : function(data) {
										var stacked = data.stacked;
										chartsCount++;
										if (data.values[0] != null) {
											// fill the questions header in the
											// widget
											for (var i = 0; i < data.questions.length; i++) {
												if (i == data.questions.length - 1) {
													htmlElement
															.parent()
															.find(
																	'.widget-question')
															.append(
																	Site
																			.escapeHtml(data.questions[i])
																			+ "");
												} else {
													htmlElement
															.parent()
															.find(
																	'.widget-question')
															.append(
																	Site
																			.escapeHtml(data.questions[i])
																			+ ", ");
												}
											}
											// fill the filter header in the
											// widget
											htmlElement
													.parent()
													.find('.widget-filters')
													.append(
															"Filters: "
																	+ Site
																			.escapeHtml(data.filterString));
											// fill the datasetsNames and dates
											// in
											// the widget
											for (var i = 0; i < data.dataSetsNames.length; i++) {
												if (i == data.dataSetsNames.length - 1) {
													htmlElement
															.parent()
															.find(
																	'.widget-wave')
															.append(
																	Site
																			.escapeHtml(data.dataSetsNames[i])
																			+ ", ");
													htmlElement
															.parent()
															.find(
																	'.widget-wave')
															.append(
																	Site
																			.escapeHtml(data.dataSetsDates[i])
																			+ "");
												} else {
													htmlElement
															.parent()
															.find(
																	'.widget-wave')
															.append(
																	Site
																			.escapeHtml(data.dataSetsNames[i])
																			+ ", ");
													htmlElement
															.parent()
															.find(
																	'.widget-wave')
															.append(
																	Site
																			.escapeHtml(data.dataSetsDates[i])
																			+ " - ");
												}
											}
											// fill the locations header in the
											// widget
											var countriesToAppend = "";
											if (data.locations.length == 1) {

												countriesToAppend += " <span>Country: ";
											} else {
												countriesToAppend += "<span>Countries: ";
											}
											for (var i = 0; i < data.locations.length; i++) {
												if (i == data.locations.length - 1) {
													countriesToAppend += ""
															+ data.locations[i]
															+ "</span>";
												} else {
													countriesToAppend += ""
															+ data.locations[i]
															+ ", ";
												}
											}
											htmlElement.parent().find(
													'.widget-wave').append(
													countriesToAppend);
											// check for the type of the widget
											// and
											// call the corresponding function
											if (htmlElement.attr('class')
													.indexOf('snap') != -1) {
												if (htmlElement.attr('class')
														.indexOf('table') != -1) {
													// draw snapshot table
													drawTables(false, null,
															"#container-"
																	+ index,
															false, data);
												} else {
													drawSnapshotCharts(
															"container-"
																	+ index,
															data, stacked);
												}
											} else {
												if (htmlElement.attr('class')
														.indexOf('table') != -1) {
													// draw trend table
													drawTrendTables(false,
															null, "#container-"
																	+ index,
															false, data);

												} else {
													// draw trend chart
													drawTrendCharts(
															"container-"
																	+ index,
															data);
												}
											}
											if (chartsCount == $('.dash-block-cont').length) {
												$(
														'.highcharts-legend-item > rect[fill="#FFFFFF"]')
														.each(
																function() {
																	$(this)
																			.prev()
																			.attr(
																					'x',
																					parseInt($(
																							this)
																							.prev()
																							.attr(
																									'x')) - 15);
																	$(this)
																			.attr(
																					'width',
																					'2');
																	$(this)
																			.parent()
																			.mouseout();
																});
												$(
														'.highcharts-legend-item > path[fill="#FFFFFF"][stroke="#FFFFFF"]')
														.each(
																function() {
																	$(this)
																			.next()
																			.attr(
																					'x',
																					parseInt($(
																							this)
																							.next()
																							.attr(
																									'x')) - 15);
																	$(this)
																			.parent()
																			.mouseout()
																});
											}
										} else {
											$(
													"#container-" + index
															+ " .dash-block")
													.append(
															"<div class='no-data'><div class='maia-notification'>The data may have been rolled back.</div></div>");
										}
									},
									error : function(jqXHR, textStatus,
											errorThrown) {
										chartsCount++;
										$(
												"#container-" + index
														+ " .dash-block")
												.append(
														"<div class='no-data'> <div class='maia-notification'>The data may have been rolled back.</div></div>");
									}
								});

					});

};

var container = function(widgetId, title, url, trend, table, index) {
	var val = "";
	val += "<div class='dash-block-wrap' id='widget-wrapper-"
			+ Site.escapeHtml(widgetId)
			+ "'>"
			+ "<h2><span> "
			+ Site.escapeHtml(title)
			+ "</span> <a "
			+ "href='/site/dashboard/deletewidget/'"
			+ "class='remove-icon delete' title='Remove this widget permanently'>remove</a>"
			+ "<a href='#update-widget' title='Check if data has been updated'"
			+ "class='update-icon popuplink update-widget-btn'>update</a>"
			+ "<a href='#edit-widget' title='Edit the name'"
			+ "class='show-icon popuplink edit-widget-btn'>edit</a>" + "</h2>"
			+ "<div class='widget-question'>" + "</div>" + "<p class='small'>"
			+ "</p>" + "<p class='widget-wave'>" + "</p>"
			+ "<div id='container-" + Site.escapeHtml(index) + "'"
			+ "class='dash-block-cont clearfix " + Site.escapeHtml(trend) + " "
			+ Site.escapeHtml(table) + "'>"
			+ "<input type='hidden' class='url' value='" + Site.escapeHtml(url)
			+ "' />" + "<div class='dash-block'></div>" + "</div>" + "</div>";
	return val;

};

var sharedContainer = function(widgetId, title, url, trend, table, index) {
	var val = "";
	val += "<div class='dash-block-wrap' id='widget-wrapper-"
			+ Site.escapeHtml(widgetId) + "'>" + "<h2><span> "
			+ Site.escapeHtml(title) + "</span>" + "</h2>"
			+ "<div class='widget-question'>" + "</div>" + "<p class='small'>"
			+ "</p>" + "<p class='widget-wave'>" + "</p>"
			+ "<div id='container-" + Site.escapeHtml(index) + "'"
			+ "class='dash-block-cont clearfix " + Site.escapeHtml(trend) + " "
			+ Site.escapeHtml(table) + "'>"
			+ "<input type='hidden' class='url' value='" + Site.escapeHtml(url)
			+ "' />" + "<div class='dash-block'></div>" + "</div>" + "</div>";
	return val;

};

$('.left-side .filter-all h3.new-exp')
		.live(
				'click',
				function() {

					if ($(this).parents('.filter-all').find(".filter-slider").length != 0) {
						$(this).parents('.filter-all')
								.find(".filter-slider h3").first().click();
						if ($(this).parents('.filter-all').find(
								"#shared-dashboards").length != 0) {

							$('#dashboard-options').find("#set-as-default")
									.hide();
							$('#dashboard-options').find(
									"#set-as-location-default").hide();
							$('#dashboard-options').find(
									"#share-dashboard-link").hide();

						} else if ($(this).parents('.filter-all').find(
								"#default-dashboards").length != 0) {

							$('#dashboard-options').find("#set-as-default")
									.hide();
							$('#dashboard-options').find(
									"#set-as-location-default").hide();
							$('#dashboard-options').find(
									"#share-dashboard-link").hide();

						} else if ($(this).parents('.filter-all').find(
								"#dashboards").length != 0) {

							$('#dashboard-options').find("#delete-dashboard")
									.show();
							$('#dashboard-options').find("#set-as-default")
									.show();
							$('#dashboard-options').find(
									"#set-as-location-default").show();
							$('#dashboard-options').find(
									"#share-dashboard-link").show();

						}

						$('.new-nav-tog').slideUp('fast');
						$(this).parents('.filter-all').find('.new-nav-tog')
								.slideDown('fast');
					}
				});

$('.filter-slider h3')
		.live(
				"click",
				function() {

					if ($(this).parent().find('.expand-list').css("display") == "block") {
						$(this).parent().find('.widget-focus').each(function() {
							$(this).removeClass("current");
						});

						$('.dash-block-wrap').each(function(index, elem) {

							$('html, body').animate({
								hide : $(elem).hide("fast")
							}, 2000);
						});

						$('.dash-block-wrap').each(function(index, elem) {
							$('html, body').animate({
								show : $(elem).show("fast")
							}, 2000);
						});
					}

					else {
						$('.dashboard-title').html($(this).find('span').html());
						$('.expand-list').slideUp('fast');
						$('.filter-slider h3 a').addClass('colapse');
						$('.filter-slider h3').parents('li').removeClass(
								"active");
						// $('.expand-list').find(".active").removeClass("active");
						$(this).parent().find('.expand-list').slideDown('fast');
						/*
						 * $(this).parents("ul").find(".active") .each(
						 * function(index, elem) {
						 * $(elem).removeClass("active");
						 * $(elem).find('.expand-list') .slideUp('fast');
						 * 
						 */
						$(this).parents('.filter-slider').parent("li")
								.addClass("active");
						$('.dashboard-blocks').find('.dash-block-wrap').each(
								function(index, elem) {
									$(elem).remove();
								})
						var blocksContainer = $('.dashboard-blocks').find(
								'.clearfix');

						$(this)
								.parent()
								.find('li')
								.each(
										function(index, elem) {
											var widgetId = $(this).find(
													'.identity').val();
											var title = $(this).find(
													'.widget-focus').text();
											var url = $(this).find('.url')
													.val();
											var trend = $(this).find('.trend')
													.val();
											var table = $(this).find('.table')
													.val();
											if ($(this).parents(
													'.shared-dashboard').length == 0) {
												var newContainer = container(
														widgetId, title, url,
														trend, table, index);
											} else {
												var newContainer = sharedContainer(
														widgetId, title, url,
														trend, table, index);
											}
											blocksContainer
													.append(newContainer);
										});
						if ($(this).parent().find('li').length == 0) {
							$("#instructions-div").show();
						} else {
							$("#instructions-div").hide();
						}
						drawAllCharts();
						$(this).find('.colapse').removeClass('colapse');

					}

				})

/*
 * $('.dashboard .filter-slider h3').toggle( function() {
 * $(this).parents('.filter-slider').find('.expand-list') .slideUp('fast');
 * $(this).find('a.expand').addClass('colapse'); }, function() {
 * $(this).parents('.filter-slider').find('.expand-list') .slideDown('fast');
 * $(this).find('a.expand').removeClass('colapse'); } //return false );
 */
/** ************* draw charts ************** */
var drawSnapshotCharts = function(containerId, data, stacked) {
	result = data;
	series = new Array();
	var startingIndex = 0;
	var emptyChart = false;
	var updatedAnswersCount = new Array();
	filteredValues = new Array();
	filteredBrandsFilters = new Array();
	filteredQuestionsAnswers = new Array();
	for (var i = 0; i < result.valuesForChart.length; i++) {
		filteredValues.push(new Array());
	}
	// remove insufficient data by pushing null in the chart series
	for (var i = 0; i < result.valuesForChart[0].length; i++) {
		var zeroCheck = 0;
		for (var j = 0; j < result.valuesForChart.length; j++) {
			zeroCheck += result.valuesForChart[j][i];
		}
		if (zeroCheck != 0) {
			for (var k = 0; k < result.valuesForChart.length; k++) {
				if (result.values[k][i] == -1000) {
					filteredValues[k].push(null);
				} else
					filteredValues[k].push(result.valuesForChart[k][i]);
				emptyChart = true;

			}
			filteredBrandsFilters.push(result.brandsAndFilters[i]);
		}
	}
	for (var i = 0; i < filteredValues.length; i++) {
		var zeroCheck = 0;
		for (var j = 0; j < filteredValues[0].length; j++) {
			zeroCheck += filteredValues[i][j];
		}
		if (zeroCheck == 0)
			filteredQuestionsAnswers.push(i);
	}
	for (var j = 0; j < result.counts.length; j++) {
		var noData = true;
		for (var k = startingIndex; k < (startingIndex + result.counts[j]); k++) {
			if (result.locations.length > 1) {
				seriesName = result.locations[j];
			} else {
				seriesName = result.questions[j];
			}
			if ($.inArray(k, filteredQuestionsAnswers) == -1) {
				series.push({
					name : seriesName,
					data : null,
					stack : j,
					showInLegend : true,
					color : '#FFFFFF',
				});
				noData = false;
				break;
			}
		}
		if (!noData) {
			var currentAnswersCount = result.counts[j];
			var skipper = Math.floor(19 / currentAnswersCount);
			var colorIndex = -skipper;
			for (var i = startingIndex; i < (startingIndex + result.counts[j]); i++) {
				var showInLegend = true;
				colorIndex += skipper;
				if ($.inArray(i, filteredQuestionsAnswers) != -1) {
					showInLegend = false;
					currentAnswersCount--;
					colorIndex -= skipper;
				}
				if ((colorIndex >= Site.colorPallete[j % 3].length || colorIndex < 0)
						&& colorIndex % 2 == 0) {
					colorIndex = 1;
				} else if ((colorIndex >= Site.colorPallete[j % 3].length || colorIndex < 0)
						&& colorIndex % 2 != 0) {
					colorIndex = 0;
				}
				series.push({
					name : result.answers[i],
					data : filteredValues[i],
					stack : j,
					showInLegend : showInLegend,
					color : Site.colorPallete[j % 3][colorIndex]
				});
			}
			updatedAnswersCount.push(currentAnswersCount);
		}
		startingIndex += result.counts[j];
	}
	series.reverse();
	var sizeOfBars = filteredBrandsFilters.length;
	// if (!stacked) {
	// sizeOfBars = 0;
	// for ( var j = 0; j < result.valuesForChart.length; j++) {
	// for ( var i = 0; i < result.valuesForChart[j].length; i++) {
	// alert()
	// if (result.valuesForChart[j][i] != -1000
	// && result.valuesForChart[j][i] != 0)
	// sizeOfBars++;
	// }
	// }
	// }
	var size = 300 + (sizeOfBars * 80);
	var strgSize = size.toString();

	// create the chart
	chartOptions = {
		chart : {
			renderTo : containerId,
			type : 'bar',
			height : strgSize,

		},
		credits : {
			enabled : false
		},
		title : {
			text : ''
		},

		exporting : {
			enabled : false
		},
		plotOptions : {
			series : {
				stacking : (stacked == true) ? 'normal' : false,
				shadow : false,
				states : {
					hover : {
						enabled : false
					}
				},
				events : {
					legendItemClick : function(event) {
						return !(this.name == result.questions[0]
								|| (result.questions[1] != null && this.name == result.questions[1]) || (result.questions[2] != null && this.name == result.questions[2]));
					}
				}
			},
			bar : {
				dataLabels : {
					enabled : true,
					color : 'black',
					style : {
						fontWeight : 'bold'
					},
					formatter : function() {
						if (this.y < 10 && stacked)
							null;
						else if (this.y > 0) {
							return this.y + '%';
						} else
							return null;
					}
				}
			}
		},
		tooltip : {
			style : {

				fontWeight : 'bold',
				width : 400
			},
			formatter : function() {
				return '<b>' + this.series.name + '</b> :<br/>' + this.y + '%';
			}
		},
		legend : {
			enabled : (emptyChart),
			reversed : true,
			align : 'right',
			verticalAlign : 'top',
			layout : 'vertical',
			itemStyle : {
				paddingBottom : '5px',
				width : 200
			},
			labelFormatter : function() {
				if (this.name == result.questions[0]
						|| (result.questions[1] != null && this.name == result.questions[1])
						|| (result.questions[2] != null && this.name == result.questions[2]))
					return "<strong>" + this.name + ":</strong><br>";
				else
					return this.name;
			}
		},
		xAxis : {
			groupPadding : 0.2,
			categories : filteredBrandsFilters,
			labels : {
				style : {
					width : 150,
					fontWeight : 'bold'
				}
			}
		},
		yAxis : {
			title : {
				text : '(%)'
			},

			showEmpty : false
		},
		series : series
	};
	if (series.length == 0 || series[0].length == 0) {
		$("#" + containerId + " .dash-block")
				.append(
						"<div class='no-data'><div class='maia-notification'>The data may have been rolled back.</div></div>");

	} else
		chart = new Highcharts.Chart(chartOptions);

};

var drawTrendCharts = function(containerId, data) {

	result = data;
	series = new Array();
	var updatedAnswersCount = new Array();
	var valuesCopy = new Array();
	var startingIndex = 0;
	var emptyCheckCount = 0;
	var valuesSizeCount = 0;
	var emptyChart = true;
	// check for insufficient data and push null in the series
	for (var i = 0; i < result.values.length; i++) {
		valuesCopy.push(new Array());
		for (var j = 0; j < result.values[0].length; j++) {
			if (result.values[i][j] == -1000) {
				emptyCheckCount++;
				valuesCopy[i].push(null);
			} else {
				valuesCopy[i].push(result.values[i][j]);
			}
			valuesSizeCount++;
		}
	}
	if (valuesSizeCount == emptyCheckCount) {
		emptyChart = false;
	}
	filteredLegend = new Array();
	for (var i = 0; i < valuesCopy.length; i++) {
		var nullCheck = false;
		for (var j = 0; j < valuesCopy[0].length; j++) {
			if (valuesCopy[i][j] != null) {
				nullCheck = true;
				break;
			}
		}
		if (nullCheck == false)
			filteredLegend.push(i);
	}
	var selectedBrands = new Array();
	// if multiple questions sort the legend by question
	if (result.counts.length > 1) {
		for (var j = 0; j < result.counts.length; j++) {
			for (var i = startingIndex; i < result.counts[j] + startingIndex; i++) {
				if ($.inArray(i, filteredLegend) == -1) {
					series.push({
						name : result.questions[j],
						data : null,
						showInLegend : true,
						color : '#FFFFFF'
					});
					break;
				}
			}
			var currentAnswersCount = result.counts[j];
			for (var i = startingIndex; i < result.counts[j] + startingIndex; i++) {
				var showInLegend = true;
				if ($.inArray(i, filteredLegend) != -1) {
					showInLegend = false;
					currentAnswersCount--;
				}
				series.push({
					name : result.answers[i],
					data : valuesCopy[i],
					showInLegend : showInLegend
				});
			}
			startingIndex += result.counts[j];
			updatedAnswersCount.push(currentAnswersCount);
		}
	} else {
		// if multiple brands sort the legend by brands
		for (var x = 0; x < result.brandsNames.length; x++) {
			selectedBrands.push(result.brandsNames[x]);
		}
		if (selectedBrands.length > 1) {
			var fullAnswer = new Array();
			var curranswer = 0;
			updatedAnswersCount.length = 0;
			for (var i = 0; i < result.values.length; i++) {
				var showInLegend = false;
				var currentAnswersCount = 0;
				for (var j = 0; j < selectedBrands.length; j++) {

					if ($.inArray(i + j, filteredLegend) == -1) {
						showInLegend = true;
						break;
					}
				}
				series.push({
					name : result.answers[curranswer],
					data : null,
					showInLegend : showInLegend,
					color : '#FFFFFF'
				});
				fullAnswer.push(null);
				for (var j = 0; j < selectedBrands.length; j++) {

					if (j == 5) {
						series.push({
							name : selectedBrands[j],
							data : valuesCopy[i + j],
							showInLegend : showInLegend,
							marker : {
								fillColor : '#FFFFFF',
								lineWidth : 2,
								lineColor : null,
								symbol : "circle"
							}
						});
					} else {
						series.push({
							name : selectedBrands[j],
							data : valuesCopy[i + j],
							showInLegend : showInLegend,
							marker : {
								symbol : Site.symbolsList[j]
							}
						});
					}
					fullAnswer.push(result.all[i + j]);
					currentAnswersCount++;
				}
				updatedAnswersCount.push(currentAnswersCount);
				i += selectedBrands.length - 1;
				curranswer++;
			}
		} else if (result.divisions.length > 0 && result.divisions[0] != "") {
			// if multiple filters (compare) sort the legend by filters
			updatedAnswersCount.length = 0;
			var filters = new Array();
			var fullAnswer = new Array();
			for (var x = 0; x < result.divisions.length; x++) {
				filters.push(result.divisions[x]);
			}
			var curranswer = 0;
			for (var i = 0; i < result.values.length; i++) {
				var currentAnswersCount = 0;
				var showInLegend = false;
				for (var j = 0; j < filters.length; j++) {

					if ($.inArray(i + j, filteredLegend) == -1) {
						showInLegend = true;
						break;
					}
				}
				series.push({
					name : result.answers[curranswer],
					data : null,
					showInLegend : showInLegend,
					color : '#FFFFFF'
				});
				fullAnswer.push(null);
				for (var j = 0; j < filters.length; j++) {
					if (result.values[i + j] != -1000) {
						series.push({
							name : filters[j],
							data : valuesCopy[i + j],
							showInLegend : showInLegend
						});
						fullAnswer.push(result.all[i + j]);
						currentAnswersCount++;
					}
				}
				i += filters.length - 1;
				curranswer++;
				updatedAnswersCount.push(currentAnswersCount);
			}
		} else {
			// if multiple countries sort by country
			if (data.locations.length > 1) {
				var AnswersPerCountry = new Array();
				for (var k = 0; k < data.locations.length; k++) {
					AnswersPerCountry.push(data.counts[0]);
				}
				var fullAnswer = new Array();
				// var curranswer = 0;
				updatedAnswersCount.length = 0;
				for (var j = 0; j < AnswersPerCountry.length; j++) {
					var noData = true;
					for (var i = startingIndex; i < AnswersPerCountry[j]
							+ startingIndex; i++) {
						if ($.inArray(i, filteredLegend) == -1) {
							series.push({
								name : data.locations[j],
								data : null,
								showInLegend : true,
								color : '#FFFFFF'
							});
							noData = false;
							fullAnswer.push(null);
							break;
						}
					}
					if (!noData) {
						var answersIndexForCountry = 0;
						var currentAnswersCount = AnswersPerCountry[j];
						for (var i = startingIndex; i < AnswersPerCountry[j]
								+ startingIndex; i++) {
							// alert(result.all[i] + result.values[i] );
							var showInLegend = true;
							if ($.inArray(i, filteredLegend) != -1) {
								showInLegend = false;
								currentAnswersCount--;
							}
							series.push({
								name : data.answers[answersIndexForCountry],
								data : valuesCopy[i],
								showInLegend : showInLegend
							});
							fullAnswer.push(data.all[i]);
							answersIndexForCountry++;
						}

						updatedAnswersCount.push(currentAnswersCount);
					}
					startingIndex += AnswersPerCountry[j];
				}

			} else {
				// if single dimension display the legend normally
				var currentAnswersCount = result.all.length;
				for (var i = 0; i < result.all.length; i++) {
					var showInLegend = true;
					if ($.inArray(i, filteredLegend) != -1) {
						showInLegend = false;
						currentAnswersCount--;
					}
					series.push({
						name : result.answers[i],
						data : valuesCopy[i],
						showInLegend : showInLegend
					});
					updatedAnswersCount.push(currentAnswersCount);
				}
			}
		}
	}
	trendChartOptions = {
		chart : {
			renderTo : containerId,
			type : 'spline',
			height : "400"
		},
		title : {
			text : ''
		},
		xAxis : {
			categories : result.dataSetsNames
		},
		exporting : {
			enabled : false
		},
		credits : {
			enabled : false
		},
		yAxis : {
			// max : 125,
			min : 0,
			// tickInterval : 25,
			showLastLabel : false,
			title : {
				text : '(%)'
			}
		},
		tooltip : {
			style : {
				fontWeight : 'bold',
				width : 400
			},
			enabled : true,
			formatter : function() {
				var index = parseInt(this.series.index);
				if (fullAnswer != null) {
					return '<b>' + fullAnswer[index] + '</b><br/>' + this.x
							+ ': ' + this.y + '%';
				} else {

					return '<b>' + this.series.name + '</b><br/>' + this.x
							+ ': ' + this.y + '%';
				}
			},
		},
		legend : {
			enabled : (emptyChart),
			align : 'right',
			verticalAlign : 'top',
			layout : 'vertical',
			itemStyle : {
				paddingBottom : '5px',
				width : 200
			},
			labelFormatter : function() {
				if (this.name == result.questions[0]
						|| (result.questions[1] != null && this.name == result.questions[1])
						|| (result.questions[2] != null && this.name == result.questions[2])
						|| ((selectedBrands.length > 1 || filters != null) && $
								.inArray(this.name, result.answers) != -1)
						|| (data.locations.length > 1 && $.inArray(this.name,
								data.locations) != -1))
					return "<strong>" + this.name + ":</strong><br>";
				else
					return this.name;
			}
		},
		plotOptions : {
			line : {
				dataLabels : {
					enabled : false
				},
				enableMouseTracking : true,
			},
			series : {
				events : {
					legendItemClick : function(event) {
						return !(this.name == result.questions[0]
								|| (result.questions[1] != null && this.name == result.questions[1])
								|| (result.questions[2] != null && this.name == result.questions[2])
								|| ((selectedBrands.length > 1 || filters != null) && $
										.inArray(this.name, result.answers) != -1) || (data.locations.length > 1 && $
								.inArray(this.name, data.locations) != -1))
					}
				}
			}
		},
		series : series
	};
	if (series.length == 0 || series[0].length == 0) {
		$("#" + containerId + " .dash-block")
				.append(
						"<div class='no-data'><div class='maia-notification'>The data may have been rolled back.</div></div>");

	} else
		trendChart = new Highcharts.Chart(trendChartOptions);
};

/** *************************************************** */
/** *************** draw tables ******************* */
drawTrendTables = function(sort, sorting, tableIndex, showHidden, data) {
	mainFlag = 1;
	// add the data to the dashboard cause it will be needed for redrawing at
	// sorting and regrouping
	dashBoards[tableIndex] = data;
	startingIndex = 0;
	filtersStartingIndex = 0;
	filtersLastIndex = 0;
	$(tableIndex).empty();
	// loop on all questions and draw a table for each
	for (var x = 0; x < data.questions.length; x++) {
		// Creating the table head row
		var table = $("<table class='to-export'></table>");
		var thead = $("<thead></thead>");
		var rowHead = $("<tr> </tr>");
		var brandClass = "brand-header-trend";
		var colHead1 = $("<th style='cursor: pointer' class='" + brandClass
				+ "' >Brand</th>");
		var colHead2 = $("<th >Answer</th>");
		var colHead3 = $("<th>Filter</th>");
		var colHead4 = $("<th class='current-val' style='cursor: pointer' >Last</th>");
		var colHead5 = $("<th class='previous-val' style='cursor: pointer'>First</th>");
		var colHead6 = $("<th class='change-val' style='cursor: pointer'>Change</th>");
		var body = $("<tbody></tbody>");
		rowHead.append(colHead1);
		rowHead.append(colHead2);
		rowHead.append(colHead3);
		rowHead.append(colHead4);
		rowHead.append(colHead5);
		rowHead.append(colHead6);
		thead.append(rowHead);
		table.append(thead);
		// get the number of filters to use for setting the rowspan
		var filtersCount = data.divisions.length / data.dataSetsNames.length;
		// if multiple locations repeat the data of each brand but for the
		// corresponding location
		for (var loc = 0; loc < data.locations.length; loc++) {
			if (data.locations.length > 1)
				var currLocation = ", " + data.locations[loc];
			else
				currLocation = "";
			// loop on each brand
			for (var y = 0; y < data.brandsNames.length; y++) {
				var secondColumn = true;
				var answerCol = '';
				var filterColumn = '';
				var mainRow = $("<tr> </tr>");
				// if not sort give the brand column a rowspan
				if (sort == false) {
					var brandCol = $("<td rowspan= '"
							+ (data.counts[mainFlag - 1] * filtersCount) + "'>"
							+ data.brandsNames[y] + currLocation + "</td>");
					mainRow.append(brandCol);
				}
				var brandRowSpan = 0;
				var firstColumn = true;
				// loop on all the answer of the current question and start
				// adding values row by row
				for (var i = 0; i < data.counts[mainFlag - 1]; i++) {
					var addedAnswer = true;
					if (!firstColumn && sort == false)
						mainRow = $("<tr></tr>");
					if (sort == false) {
						answerCol = $("<td rowspan= '" + filtersCount + "'>"
								+ "</td>");
						answerCol.append(data.answers[i + startingIndex]);
					}
					if (sort == false)
						secondColumn = true;
					var insufficientCount = 0;
					// each filter correspond to a row of data and all filters
					// corresponding to an answer are grouped together by giving
					// the answer a rowspan
					for (var j = 0; j < filtersCount; j++) {
						// check the row with insufficient data
						var insufficientCheck = false;
						if (data.values[filtersStartingIndex + j][data.dataSetsNames.length - 1] == -1000
								|| data.values[filtersStartingIndex + j][0] == -1000) {
							insufficientCheck = true;
							insufficientCount++;
						} else if (data.values[filtersStartingIndex + j][data.dataSetsNames.length - 1] == 0
								&& data.values[filtersStartingIndex + j][0] == 0) {
							insufficientCheck = true;
							insufficientCount++;
						}
						if (showHidden == false && (insufficientCheck == true)) {
							hideDataCheck = false;
						}
						if (!secondColumn)
							mainRow = $("<tr></tr>");
						filterColumn = $("<td></td>");
						// case sort is true remove all rowspans and draw table
						// row by row
						if (sort == true) {
							mainRow = $("<tr></tr>");
							var brandCol = $("<td>" + data.brandsNames[y]
									+ currLocation + "</td>");
							mainRow.append(brandCol);
							answerCol = $("<td >" + "</td>");
							answerCol.append(data.answers[i + startingIndex]);
							mainRow.append(answerCol);
						}
						// if the filters string is empty that means no filters
						if (data.divisions[j].length != 0)
							filterColumn.append(data.divisions[j]);
						else
							filterColumn.append("No Filters");
						if (!insufficientCheck)
							mainRow.append(filterColumn);
						var currentColumn = $("<td></td>");
						var previousColumn = $("<td></td>");
						var changeColumn = $("<td></td>");
						// start inserting data
						if (data.values[filtersStartingIndex + j][data.dataSetsNames.length - 1] == -1000) {
							currentColumn.append("Insufficient data");
						} else {
							currentColumn
									.append(data.values[filtersStartingIndex
											+ j][data.dataSetsNames.length - 1]
											+ "%");
						}
						if (data.values[filtersStartingIndex + j][0] == -1000) {
							previousColumn.append("Insufficient data");
						} else {
							previousColumn
									.append(data.values[filtersStartingIndex
											+ j][0]
											+ "%");
						}
						if (data.values[filtersStartingIndex + j][0] == -1000
								|| data.values[filtersStartingIndex + j][data.dataSetsNames.length - 1] == -1000) {
							changeColumn.append("Insufficient data");
						} else {
							changeColumn
									.append((data.values[filtersStartingIndex
											+ j][0] - data.values[filtersStartingIndex
											+ j][data.dataSetsNames.length - 1])
											.toFixed(2)
											+ "%");
						}
						if (!insufficientCheck) {
							if (addedAnswer && filtersCount > 1) {
								filterColumn.before(answerCol);
								addedAnswer = false;
							}

							mainRow.append(currentColumn);
							mainRow.append(previousColumn);
							mainRow.append(changeColumn);

							secondColumn = false;
							body.append(mainRow);
						}
					}
					filtersLastIndex += filtersCount;
					if (sort == false)
						answerCol.attr('rowspan', filtersCount
								- insufficientCount);
					brandRowSpan += filtersCount - insufficientCount;
					if (filtersCount - insufficientCount != 0
							&& (filtersCount == 1) && sort == false) {
						filterColumn.before(answerCol);
					}
					filtersStartingIndex += ((filtersCount));
					if (secondColumn == false || sort == true)
						firstColumn = false;
				}
				if (sort == false)
					brandCol.attr('rowspan', brandRowSpan);
				filtersStartingIndex = filtersLastIndex;
			}
		}
		// append the body
		table.append(body);
		if (body.html() == "") {
			body.append("<tr><td colspan='6'> Insufficient data </td></tr>");
		}
		startingIndex += data.counts[mainFlag - 1];
		// add the color class for the table based on wich question it
		// represents
		if (mainFlag == 1) {
			table.addClass("blue-table");
			table.attr("id", "Q1T");
		} else if (mainFlag == 2) {
			table.attr("id", "Q2T");
			table.addClass("green-table");
			table.removeClass("blue-table");
		} else if (mainFlag == 3) {
			table.attr("id", "Q3T");
			table.addClass("red-table");
			table.removeClass("green-table");
			table.removeClass("blue-table");
		}
		$(tableIndex).append(table);
		$(tableIndex).append("<br/>");
		// if sort then append the tablesorter plugin
		if (sort == true) {
			table.tablesorter({
				sortList : sorting,
				headers : {
					0 : {
						sorter : false
					},
					1 : {
						sorter : false
					},
					2 : {
						sorter : false
					}
				}
			});
		}
		// increase mainflag to move to the next question
		mainFlag++;
	}
};

var drawTables = function(sort, sorting, tableIndex, showHidden, data) {
	mainFlag = 1;
	startingIndex = 0;
	$(tableIndex).empty();
	// loop on each question to draw corresponding table
	for (var i = 0; i < data.questions.length; i++) {
		var compare = false;
		// initialize the header row
		var tableScrollDiv = $("<div class='scroll-pane horizontal-only'></div>");
		var table = $("<table  class='tablesorter to-export'> </table>");
		tableScrollDiv.append(table);
		var thead = $("<thead></thead>");
		var rowHead = $("<tr> </tr>");
		var brandClass = "brand-header";
		var colHead1 = $("<th style='cursor: pointer' class='" + brandClass
				+ "' >Brand </th>");
		var colHead2 = $("<th>Filter</th>");
		var body = $("<tbody></tbody>");
		rowHead.append(colHead1);
		rowHead.append(colHead2);
		var filtersCount = data.divisions.length;
		var currFilterText = "";
		// set the compare flag if compare==true enable row span else just draw
		// each row without grouping
		if (filtersCount == 1) {
			compare = false;
		} else
			compare = true;
		var answerClass = "answer";
		if (compare && sort == false)
			answerClass = "answer-header";
		var answerIndexInTable = 2;
		// check the length of the answer to fit the table with the scroller
		for (var j = startingIndex; j < (startingIndex + data.counts[mainFlag - 1]); j++) {
			if (data.answers[j].length > 60)
				var tempCol = $("<th  style='cursor: pointer;min-width:250px;word-wrap: break-word;' class='"
						+ answerClass
						+ "' id='answer-"
						+ answerIndexInTable
						+ "'> </th>");
			else if (data.answers[j].length > 40)
				var tempCol = $("<th  style='cursor: pointer;min-width:200px;word-wrap: break-word;' class='"
						+ answerClass
						+ "' id='answer-"
						+ answerIndexInTable
						+ "'> </th>");
			else if (data.answers[j].length > 30)
				var tempCol = $("<th  style='cursor: pointer;min-width:150px;word-wrap: break-word;' class='"
						+ answerClass
						+ "' id='answer-"
						+ answerIndexInTable
						+ "'> </th>");
			else if (data.answers[j].length > 20)
				var tempCol = $("<th  style='cursor: pointer;min-width:100px;word-wrap: break-word;' class='"
						+ answerClass
						+ "' id='answer-"
						+ answerIndexInTable
						+ "'> </th>");
			else
				var tempCol = $("<th  style='cursor: pointer;' class='"
						+ answerClass + "' id='answer-" + answerIndexInTable
						+ "'> </th>");
			answerIndexInTable++;
			tempCol.text(data.answers[j]);
			rowHead.append(tempCol);
		}
		thead.append(rowHead);
		table.append(thead);
		var brandIndexCount = -1;
		// case not compare each filter represents a single row
		if (!compare) {
			var answersSkipForCurrentLocationCount = 0;
			for (var locationIter = 0; locationIter < data.locations.length; locationIter++) {
				var currentLocation = "";
				if (data.locations.length > 1) {
					var currentLocation = ", "
							+ Site.escapeHtml(data.locations[locationIter]);
				}
				// loop on all brands
				var brandIndexCount = -1;
				for (var j = 0; j < data.brandsNames.length; j++) {
					var row = $("<tr> </tr>");
					var col1 = $("<td>" + Site.escapeHtml(data.brandsNames[j])
							+ currentLocation + "</td>");
					var col2 = "";
					if (currFilterText.length > 0)
						col2 = $("<td>" + currFilterText + "</td>");
					else
						col2 = $("<td>No Filters</td>");
					row.append(col1);
					row.append(col2);
					brandIndexCount++;
					var insufficientCheck = true;
					var zeroCheck = true;
					// check for insufficient data
					for (var x = startingIndex; x < (startingIndex + data.counts[mainFlag - 1]); x++) {
						if (data.values[x + answersSkipForCurrentLocationCount][brandIndexCount] != -1000) {
							insufficientCheck = false;
						}
						if (data.values[x + answersSkipForCurrentLocationCount][brandIndexCount] != 0) {
							zeroCheck = false;
						}
					}
					if (showHidden == false
							&& !(zeroCheck == false && insufficientCheck == false)) {
						hideDataCheck = false;
					}
					// insert the data
					if (zeroCheck == false && insufficientCheck == false) {
						for (var x = startingIndex; x < (startingIndex + data.counts[mainFlag - 1]); x++) {
							if (data.values[x
									+ answersSkipForCurrentLocationCount][brandIndexCount] == -1000) {
								var tempCol = $("<td>" + "Insufficient data"
										+ "</td>");
							} else {
								var tempCol = $("<td>"
										+ data.values[x
												+ answersSkipForCurrentLocationCount][brandIndexCount]
										+ "%</td>");
							}
							row.append(tempCol);
						}
						body.append(row);
					}
				}
				answersSkipForCurrentLocationCount += data.counts[locationIter];

			}
		} else {
			// case compare there will be grouping by brand
			var brandCount = 0;
			var brandIndex = 0;
			// loop on all brands
			for (var j = 0; j < data.brandsNames.length; j++) {
				var brandMainRow = $("<tr > </tr>");
				var brandColumn = "";
				// case sort != false that means no grouping and draw all rows
				// normally
				if (sort == false) {
					brandColumn = $("<td rowspan= '" + filtersCount + "'>"
							+ data.brandsNames[j] + "</td>");
					brandMainRow.append(brandColumn);
				}
				var firstFilter = true;
				var brandRowspan = 0;
				for (var j = 0; j < filtersCount; j++) {
					if (!firstFilter) {
						brandMainRow = $("<tr></tr>");
					}
					var insufficientCheck = true;
					var zeroCheck = true;
					// check insufficient data
					for (var k = startingIndex; k < (startingIndex + data.counts[mainFlag - 1]); k++) {
						if (data.values[k][j + brandIndex] != -1000) {
							insufficientCheck = false;
						}
						if (data.values[k][j + brandIndex] != 0) {
							zeroCheck = false;
						}
					}
					if (showHidden == false
							&& !(zeroCheck == false && insufficientCheck == false)) {
						hideDataCheck = false;
					}
					if (sort == true) {
						brandColumn = $("<td>" + data.brandsNames[j] + "</td>");
						brandMainRow.append(brandColumn);
					}
					// insert data
					if (zeroCheck == false && insufficientCheck == false) {
						tempCol = $("<td>" + data.divisions[j] + "</td>");
						brandMainRow.append(tempCol);
						for (var k = startingIndex; k < (startingIndex + data.counts[mainFlag - 1]); k++) {
							if (data.values[k][j + brandIndex] == -1000) {
								var answerCol = $("<td>" + "Insufficient data"
										+ "</td>");
							} else {
								var answerCol = $("<td>"
										+ data.values[k][j + brandIndex]
										+ "%</td>");
							}
							brandMainRow.append(answerCol);
						}
						body.append(brandMainRow);
						firstFilter = false;
					} else {
						brandRowspan++;
					}
				}
				brandIndex += filtersCount;
				brandCount++;
				if (!showHidden && sort == false) {
					brandColumn.attr('rowspan', filtersCount - brandRowspan);
				}
			}
		}
		// append the table
		table.append(body);
		startingIndex += data.counts[mainFlag - 1];
		if (mainFlag == 1) {
			table.addClass("blue-table");
			table.attr("id", "Q1T");
		} else if (mainFlag == 2) {
			table.addClass("green-table");
			table.removeClass("blue-table");
			table.attr("id", "Q2T");
		} else if (mainFlag == 3) {
			table.addClass("red-table");
			table.removeClass("green-table");
			table.removeClass("blue-table");
			table.attr("id", "Q3T");
		}
		// the scroller logic
		$(tableIndex).append(tableScrollDiv);
		$(function() {
			$('.scroll-pane').each(function() {
				$(this).jScrollPane({
					showArrows : $(this).is('.arrow')
				});
				var api = $(this).data('jsp');
				var throttleTimeout;
				$(window).bind('resize', function() {
					if ($.browser.msie) {
						if (!throttleTimeout) {
							throttleTimeout = setTimeout(function() {
								api.reinitialise();
								throttleTimeout = null;
							}, 50);
						}
					} else {
						api.reinitialise();
					}
				});
			});
			$(window).resize(function() {
				var element = $('.scroll-pane').jScrollPane({});
				var api = element.data('jsp');
				api.destroy();
				$('.scroll-pane').jScrollPane();
			});
		});
		// if !compare start the table as a sortable ordinary table
		if (!compare) {
			table.tablesorter({
				headers : {
					0 : {
						sorter : false
					},
					1 : {
						sorter : false
					}
				}
			});
		} else {
			// save the table data cause it will be needed for redrawing and
			// regrouping
			dashBoards[tableIndex] = data;
		}
		// if sort then append the tablesorter plugin
		if (sort == true) {
			table.tablesorter({
				sortList : sorting,
				headers : {
					0 : {
						sorter : false
					},
					1 : {
						sorter : false
					}
				}
			});
		}
		// increase the mainflag to start drawing the next question
		mainFlag++;
	}
};

/** *************************************************** */

/** *************** listeners for table sorting ****************** */

$('.brand-header').live("click", function() {
	var tableIndex = "#" + $(this).parents('.dash-block-cont').attr('id');
	drawTables(false, null, tableIndex, false, dashBoards[tableIndex]);
});

$('.answer-header').live("click", function() {
	var answerIndex = parseInt($(this).attr('id').substring(7), 10);
	var sorting = [ [ answerIndex, 0 ] ];
	var tableIndex = "#" + $(this).parents('.dash-block-cont').attr('id');
	drawTables(true, sorting, tableIndex, false, dashBoards[tableIndex]);
});

$('.brand-header-trend').live("click", function() {
	var tableIndex = "#" + $(this).parents('.dash-block-cont').attr('id');
	drawTrendTables(false, null, tableIndex, false, dashBoards[tableIndex]);
});

$('.previous-val').live("click", function() {
	var sorting = [ [ 4, 0 ] ];
	var tableIndex = "#" + $(this).parents('.dash-block-cont').attr('id');
	drawTrendTables(true, sorting, tableIndex, false, dashBoards[tableIndex]);
});
$('.change-val').live("click", function() {
	var sorting = [ [ 5, 0 ] ];
	var tableIndex = "#" + $(this).parents('.dash-block-cont').attr('id');
	drawTrendTables(true, sorting, tableIndex, false, dashBoards[tableIndex]);
});
$('.current-val').live("click", function() {
	var sorting = [ [ 3, 0 ] ];
	var tableIndex = "#" + $(this).parents('.dash-block-cont').attr('id');
	drawTrendTables(true, sorting, tableIndex, false, dashBoards[tableIndex]);
});

/** *************************************************** */
$(document).ready(
		function() {

			Site.palette();
			$('.dashboard-title').text(
					$('#dashboards').find('.active').find('h3 span').text());
			drawAllCharts();
			Site.AutoLaunch();
			checkVideoCookie();

		});

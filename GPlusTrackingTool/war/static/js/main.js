/*
 * jQuery File Upload Plugin JS Example 7.0
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/*jslint nomen: true, unparam: true, regexp: true */
/*global $, window, document */

$(function() {

	// listeners for sorting and paging
	$('.entries-per-page').live("change", function() {
		var count = $(this).val();
		var url = "?offset=" + $("#input-from").val() + "&count=" + count;
		if ($("#input-orderBy").val() != "") {
			url += "&orderBy=" + $("#input-orderBy").val();
		}
		if ($("#input-orderDir").val() != "") {
			url += "&orderDir=" + $("#input-orderDir").val();
		}
		window.location = url;
	});

	'use strict';

	// $('#fileupload').bind('fileuploadsubmit', function(e, data) {
	// // The example input, doesn't have to be part of the upload form:
	// var input = $('#_tk').val();
	// data.formData = {
	// tk : input
	// };
	// if (!data.formData.tk) {
	// // input.focus();
	// return false;
	// }
	// });

	// Initialize the jQuery File Upload widget:
	$('#fileupload')
			.fileupload(
					{

						// Uncomment the following to send cross-domain cookies:
						xhrFields : {
							withCredentials : true,
							"_csrf" : $('#_csrf').val()
						},

						// var token = $("#_tk").val();

						// url : '/cms/import/upload?tk=' + $('#_tk').val()
						// + "&_csrf=" + $("#_csrf").val(),
						// path = $(this).attr("action"),
						// url : '/dataUploadController/upload?tk='
						// + $('#_tk').val() + "&_csrf="
						// + $("#_csrf").val(),
						url : $(this).attr("action"),
						// data : new FormData($("#fileupload")[0]),
						success : function(data) {
							alert(url)
							$
									.each(
											data.files,
											function() {

												$
														.each(
																this.dataSheets,
																function() {

																	// alert($('#_csrf'))
																	// alert($(
																	// "#blob")
																	// .val())

																	var row = $("<tr> </tr>");
																	var upsize = this.size / 1000000;
																	var date = new Date(
																			this.uploadDate);
																	var month = ('0'
																			+ date
																					.getUTCMonth() + 1)
																			.slice(-2);
																	var day = ('0' + date
																			.getUTCDate())
																			.slice(-2);
																	var year = date
																			.getUTCFullYear()
																			.toString();
																	var col1 = $("<td> "
																			+ day
																			+ "/"
																			+ month
																			+ "/"
																			+ year
																			+ "</td> ");
																	var col2 = $("<td>"
																			+ this.name
																			+ "</td>");
																	if (this.uploadedBy == null)
																		var col3 = $("<td> "
																				+ ""
																				+ "</td> ");
																	else
																		var col3 = $("<td> "
																				+ this.uploadedBy
																						.toString()
																				+ "</td> ");
																	var col4 = $("<td> "
																			+ upsize
																					.toFixed(
																							1)
																					.toString()
																			+ " MB</td>");
																	var col5 = $("<td> "
																			+ this.status
																					.toString()
																			+ "</td>");
																	if (this.status == "UPLOADED")
																		var col6 = $("<td>"
																				+ "<form action='/cms/process/entry?k="
																				+ this.id
																				+ "'>"
																				+ "<input type='hidden' name='tk' value='"
																				+ $(
																						'#_tk')
																						.val()
																				+ "'/>"
																				+ "<button type='submit' class='maia-button process-datasheet'>Process</button>"
																				+ "</td>");
																	else if (this.status == "PROCESSED")
																		var col6 = $("<td><a href='#' class='maia-button process-datasheet'>Processed</a></td>");
																	row
																			.append(col1);
																	row
																			.append(col2);
																	row
																			.append(col3);
																	row
																			.append(col4);
																	row
																			.append(col5);
																	row
																			.append(col6);

																	$(
																			'.table-recent-upload > tbody')
																			.eq(
																					0)
																			.append(
																					row);
																});
												if (!$('.table-recent-upload')
														.is(":visible"))
													$('.table-recent-upload')
															.tablesorter(
																	{
																		headers : {
																			5 : {
																				sorter : false
																			},
																			2 : {
																				sorter : false
																			}
																		}
																	});
												$(".table-recent-upload")
														.trigger("update");
												$('.table-recent-upload')
														.show();

											});
						}
					});

	// Enable iframe cross-domain access via redirect option:
	$('#fileupload').fileupload('option', 'redirect',
			window.location.href.replace(/\/[^\/]*$/, '/cors/result.html?%s'));
	// Load existing files:
	$.ajax({
		// Uncomment the following to send cross-domain cookies:
		xhrFields : {
			withCredentials : true
		},
		url : $('#fileupload').fileupload('option', 'url'),
		dataType : 'json',
		context : $('#fileupload')[0]
	}).done(function(result) {
		$(this).fileupload('option', 'done').call(this, null, {
			result : result
		});
	});

});
$(document).ready(function() {
	var count = parseInt(($('#input-count').val()), 10);
	$('.entries-per-page').val(count);
});

export default class MiStudiesService {
  constructor($http, Networking) {
    'ngInject'
    this._$http = $http
    this._Networking = Networking
      // Apis Urls
  }
  studiesListing(FilterBy) {
    return this._Networking.Get(FilterBy)
  }
  createUploadUrl(StudyData) {
    return this._Networking.Get('createUploadUrl')
  }
  addNewStudy(StudyData) {
    return this._Networking.Put('CreateNewStudy', StudyData)
  }
  deleteStudy(StudyId) {
    return this._Networking.Post('DeleteStudy', StudyId)
  }
  updateStudy(StudyData) {
    return this._Networking.Post('UpdateStudy', StudyData)
  }

  viewStudy(StudyData) {
    return this._Networking.Post('studyDataSheets', StudyData)
  }
}
function MiLoging($location, $cookies, Networking, $rootScope, $state, $stateParams) {
  var IsSuperAdmin;
  var IsAdmin;
  var LoginedUserMail;
  $rootScope.$state = $state;
  $rootScope.$stateParams = $stateParams;

  // get logined user data
  if (!$cookies.get('IsSuperAdmin') || !$cookies.get('LoginedUserMail') || !$cookies.get('IsAdmin')) {
    Networking.Get('getCurrentUser').then((res) => {
      //console.log(res);
      $rootScope.IsSuperAdmin = res.data.superUser;
      $rootScope.LoginedUserMail = res.data.email;
      $rootScope.IsAdmin = res.data.type == 'ADMINISTRATOR' ? true : false;
      $cookies.put('IsSuperAdmin', $rootScope.IsSuperAdmin);
      $cookies.put('LoginedUserMail', $rootScope.LoginedUserMail);
      $cookies.put('IsAdmin', $rootScope.IsAdmin);
    })
  } else {
    // console.log($cookies.get('IsSuperAdmin'))
    $rootScope.IsSuperAdmin = JSON.parse($cookies.get('IsSuperAdmin'));
    $rootScope.LoginedUserMail = $cookies.get('LoginedUserMail');
    $rootScope.IsAdmin = JSON.parse($cookies.get('IsAdmin'));
    console.log($rootScope.IsSuperAdmin);
    //console.log($rootScope.IsAdmin);
    //console.log($rootScope.LoginedUserMail);
  }


  if ($location.host() != 'localhost' && $location.search().code) {
    Networking.Post('getUserGoogleInfo', angular.toJson({ 'code': $location.search().code })).then((res) => {
      $cookies.put('UserImg', res.data.imgURL);
      console.log(res)
      console.log(res.data)
      console.log($cookies.get('UserImg'))
    })
  }
}
export default MiLoging;
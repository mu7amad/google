export default class MiApiServices {
  constructor($http) {
      'ngInject'
      this._$http = $http;
      // Apis Urls
      this._ApiUrls = {
        // user infos
        'getUserGoogleInfo': '/logInController/getUserGoogleInfo',
        'getCurrentUser': '/cms/MIDataExplorer/getCurrentUser',
        // admins apis
        'MiAdmins': '/cms/MIDataExplorer/loadAdmins',
        'AdminsLookUp': '/cms/MIDataExplorer/loadAdminsLookUp',
        // 'UsersLookUp': '/cms/MIDataExplorer/loadUsersLookUp',
        'NewAdmin': '/cms/MIDataExplorer/createNewAdmin',
        'EditAdmin': '/cms/MIDataExplorer/editAdmin',
        'DeleteAdmin': '/cms/MIDataExplorer/deleteAdmin',
        // Studies apis
        'ListStudies': '/cms/MIDataExplorer/loadStudies',
        'LoggedInUserStudies': '/cms/MIDataExplorer/getLoggedInUserStudies',
        'LoggedAssignedStudies': '/cms/MIDataExplorer/getLoggedAssignedStudies',
        'CreateNewStudy': '/cms/MIDataExplorer/createNewStudy',
        'createUploadUrl': '/cms/MIDataExplorer/createUploadUrl',
        'DeleteStudy': '/cms/MIDataExplorer/deleteStudy',
        'UpdateStudy': '/cms/MIDataExplorer/editStudy',
        'openStudy': '/cms/MIDataExplorer/openStudy',
        'studyDataSheets': '/cms/MIDataExplorer/openStudyDataSheets',
        // Dashboard
        'UserPendingFiles': '/cms/MIDataExplorer/getLoggedInUserPendingFiles',
        'AboutDashBoard': '/cms/MIDataExplorer/loadAboutDashBoard',
        'addAboutDashBoard': '/cms/MIDataExplorer/addAboutDashBoard'
      }
    }
    //Method used to generate URL based on the provided parameters
  GetFullUrl(ApiUrl, params) {
    let CrfCode = angular.element(document.querySelector('header')).attr('data-crfcode');
    let QueryString = '';
    //loop over the parameters array to construct the query string
    for (let key in params) {
      QueryString += '&' + key + '=' + params[key];
    }
    //append the base url and the url of the API, csrf code and the query string
    return this._ApiUrls[ApiUrl] + '/?_csrf=' + CrfCode + QueryString;
  };

  Get(ApiUrl, params) {
    let url = this.GetFullUrl(ApiUrl, params);
    let result = this._$http({ method: 'GET', url: url });
    return result;
  };

  Put(ApiUrl, data, params) {
    let url = this.GetFullUrl(ApiUrl, params);
    let result = this._$http.put(url, data, {
      // transformRequest: angular.identity,
      headers: {
        'Content-Type': 'application/json'
      }
    });
    return result;
  };
  Post(ApiUrl, data, params) {
    let url = this.GetFullUrl(ApiUrl, params);
    let result = this._$http.post(url, data, {
      // transformRequest: angular.identity,
      headers: {
        'Content-Type': 'application/json'
      }
    });
    return result;
  };
}
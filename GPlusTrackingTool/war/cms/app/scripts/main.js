//include angular
import 'angular';
// include angular routing
import 'angular-ui-router';
// include angular cookies
import 'angular-cookies';
// include angular matrial
import 'angular-material';
// include angular matrial data tabel
import 'angular-material-data-table';
// include angular file uploader 
import 'ng-file-upload';
// include angular message
import 'angular-messages';

import '../../bower_components/angular-editable-text/dist/angular-editable-text';


// get app configs
import routing from './routes';

// include Services
import MiApiServices from './services/networking';

// login modules
import MiLoging from './model/login';

// include Admin
import MiAdminComponent from './controllers/mi-admin';
import MiAdminService from './model/admin';

// include Studies
import MiStudiesComponent from './controllers/mi-studies';
import MiStudyFilesComponent from './controllers/mi-study-files';

import MiStudiesService from './model/studies';
import MiStToDateFilter from './filters/filters';

// include Dashboard
import MiDashboardComponent from './controllers/mi-dashboard';
import MiDashboardService from './model/dashboard';

// setup angular main app
angular.module('app', ['ui.router', 'ngMessages', 'gg.editableText', 'ngCookies', 'ngMaterial', 'md.data.table', 'ngFileUpload'])
  // set app config
  .config(routing)
  .run(MiLoging)
  .service('Networking', MiApiServices)
  .service('MiAdminModel', MiAdminService)
  .component('miAdmin', MiAdminComponent)
  .service('MiStudiesModel', MiStudiesService)
  .component('miStudies', MiStudiesComponent)
  .component('miStudyfiles', MiStudyFilesComponent)
  .component('miDashboard', MiDashboardComponent)
  .service('MiDashboardModel', MiDashboardService)
  .filter('miStToDate', MiStToDateFilter)
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>


	<div id="fileupload">
		<form method="post" enctype="multipart/form-data">
			<div class="fileupload-buttonbar">
				<label class="fileinput-button">
					<button type="submit" name="submit">
						Upload </> <input type="file" name="files[]" multiple>
				</label>
			</div>
		</form>
		<div class="fileupload-content">
			<table class="files"></table>
		</div>
	</div>

	<script id="template-upload" type="text/x-jquery-tmpl">
...<!-- report all the templates from the jQuery Upload Sample code (link above) -->...
</script>
	<script type="text/javascript">
	$(function () {
	    $('#fileupload').fileupload({
	        dataType: 'json',
	        done: function (e, data) {
	            $.each(data.result.files, function (index, file) {
	                $('<p/>').text(file.name).appendTo(document.body);
	            });
	        }
	    });
	});
	</script>
</body>
</html>
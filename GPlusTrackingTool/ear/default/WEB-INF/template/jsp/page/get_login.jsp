<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<!--[if IE 7]> <html class="google ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="google ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="google ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="google" lang="en">
<!--<![endif]-->
<head>
<script>
	(function(H) {
		H.className = H.className.replace(/\bgoogle\b/, 'google-js')
	})(document.documentElement)
</script>
<meta charset="utf-8">
<meta content="initial-scale=1, minimum-scale=1, width=device-width"
	name="viewport">
<title>Google+ Brand Tracker - Home Page</title>
<link rel="stylesheet" media="all" href="/site_ui/css/default.css" />
<script src="//www.google.com/js/google.js"></script>
<script>
	new gweb.analytics.AutoTrack({
		profile : "UA-38263893-1"
	});
</script>
<link
	href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;lang=en"
	rel="stylesheet">
<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>
<body class="get-login">

	<header>
		<div class="maia-header" id="maia-header" role="banner">
			<div class="maia-aux">
				<h1>
					<a href="../"><img alt="Google"
						src="//www.google.com/images/logos/google_logo_41.png">+
						Brand Tracker</a>
				</h1>
			</div>
		</div>
	</header>
	<!-- /Header -->

	<nav class="maia-nav" id="maia-nav-x" role="navigation">
		<div class="maia-aux">
			<ul>
				<li
					<c:if test="${activeTopNavItemId == 'homeItem' }">class="active"</c:if>><a
					href="/site/dashboard">Dashboard</a></li>
				<li
					<c:if test="${activeTopNavItemId == 'snapshotItem' }">class="active"</c:if>><a
					href="/site/snapshot">Snapshot</a></li>
				<li
					<c:if test="${activeTopNavItemId == 'trendsItem' }">class="active"</c:if>><a
					href="/site/trend">Trends</a></li>

				<c:if test="${admin}">
					<li class="user-login"><a href="/cms"
						class="maia-button orange-button">Admin</a></li>
				</c:if>
				<li class="user-login user-go"><a href="/site/allquestions?trend=false"
					class="maia-button orange-button">Proceed to the tool</a></li>
			</ul>
		</div>
	</nav>

	<!-- /Navigtion -->
	<section id="landing-page" class="clearfix" role="main">
		<div class="landing-border">
			<section class="landing-slide">
				<div class="slider-bg">
					<div class="bx-controls bx-has-controls-direction">
						<div class="bx-controls-direction">
							<a href="#" class="bx-prev">Prev</a><a href="#" class="bx-next">Next</a>
						</div>
					</div>
					<ul class="home-slider">
						<li><img src="/site_ui/images/slide-01.jpg" alt=""></li>
						<li><img src="/site_ui/images/slide-02.jpg" alt=""></li>
						<li><img src="/site_ui/images/slide-03.jpg" alt=""></li>
					</ul>
				</div>
			</section>
		</div>
		<ul class="landing-blocks clearfix">
			<li><img src="/site_ui/images/block-001.jpg" alt="">
				<p>
					<b>Snapshot and Trends:</b> uncover Google+ insights at a
					particular moment in time, or see how brand ratings are changing
					with time
				</p></li>
			<li><img src="/site_ui/images/block-002.jpg" alt="">
				<p>
					<b>Easy comparisons:</b> see how Google+ stacks up against other
					social networks, or compare to different countries/regions around
					the world
				</p></li>
			<li><img src="/site_ui/images/block-003.jpg" alt="">
				<p>
					<b>Dashboard:</b> add your favorite charts to a custom dashboard,
					and easily share with others on the team
				</p></li>
		</ul>
	</section>
	<!-- /Content -->



	<div id="maia-signature"></div>
	<footer class="maia-footer" id="maia-footer">
		<div id="maia-footer-local">
			<div class="maia-aux">
				<%-- 				<p> 
					<a href="#">About the research</a> | <a href="#">Terms &
						Conditions</a>
				</p>--%>
			</div>
		</div>
		<div id="maia-footer-global">
			<div class="maia-aux">
				<ul>
					<li><a href="//www.google.com/">Google</a></li>
					<li><a href="//www.google.com/intl/en/policies/">Privacy
							&amp; Terms</a></li>
					<%--<a href="">Terms of Service</a></li>--%>
				</ul>
			</div>
		</div>
	</footer>
	<!-- /Footer -->

	<script src="//www.google.com/js/maia.js"></script>
	<script src="//code.jquery.com/jquery-1.8.3.min.js"></script>
	<script>
		window.jQuery
				|| document
						.write("<script src='/site_ui/js/jquery-1.8.3.min.js'>\x3C/script>")
	</script>
	<!--[if lte IE 9]>
		<script src="/site_ui/js/PIE.js"></script>
		<script src="/site_ui/js/jquery.placeholder.min.js"></script>
	<![endif]-->
	<script src="/site_ui/js/jquery.selectbox-0.2.min.js"></script>
	<script src="/site_ui/js/jquery.fancybox.pack.js"></script>
	<script src="/site_ui/js/jquery.tinyscrollbar.min.js"></script>
	<script src="/site_ui/js/jquery-ui-1.9.2.custom.min.js"></script>
	<script src="/site_ui/js/jquery.multiselect.min.js"></script>
	<script src="/site_ui/js/jquery.bxslider.min.js"></script>
	<!-- 	<script src="/site_ui/js/main.js"></script> -->
	<script>
		slider = $('.home-slider').bxSlider({
			mode : 'fade',
			adaptiveHeight : true,
			pager : false,
			controls : false
		});

		$('a.bx-prev').click(function(e) {
			e.preventDefault();
			slider.goToPrevSlide();
		});
		$('a.bx-next').click(function(ee) {
			ee.preventDefault();
			slider.goToNextSlide();
		});
	</script>
</body>
</html>


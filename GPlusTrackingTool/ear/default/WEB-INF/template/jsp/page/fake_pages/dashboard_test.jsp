<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript">
	$(document)
			.ready(
					function() {
						var csrfToken = document.getElementsByName("_csrf")[0].value;
						console.log(csrfToken);

						$("#about-form")
								.submit(
										function() {

											var vars = {};
											vars["about"] = $("#about-text")
													.val();
											$
													.ajax({
														type : "PUT",
														contentType : "application/json",

														url : "/cms/MIDataExplorer/addAboutDashBoard?_csrf="
																+ csrfToken,

														data : JSON
																.stringify(vars),
														dataType : 'json',
														success : function(e) {
															console
																	.log(
																			"Success: ",
																			e)

														},
														error : function(e) {
															console.log(
																	"Error: ",
																	e);
															sharedWithApplicationUsers
														},
														done : function(e) {
															console.log("DONE");
														}
													});
											return false;
										});
					});
	$(document)
			.ready(
					function() {
						var csrfToken = document.getElementsByName("_csrf")[0].value;
						console.log(csrfToken);

						$("#load-about")
								.submit(
										function() {
											$
													.ajax({
														type : "GET",
														contentType : "application/json",

														url : "/cms/MIDataExplorer/loadAboutDashBoard?_csrf="
																+ csrfToken,

														dataType : 'json',
														success : function(e) {
															console
																	.log(
																			"Success: ",
																			e)
															document
																	.getElementById("about-div").innerHTML = JSON
															setSco.stringify(e)
														},
														error : function(e) {
															console.log(
																	"Error: ",
																	e);
														},
														done : function(e) {
															console.log("DONE");
														}
													});
											return false;
										});
					});
	//========================
	$(document).ready(function() {
		var url = window.location.href;
		// 		console.log(url);

	});

	//=============

	function test() {
		var a = window.location.href;
		console.log("URL", a);
		var result = null;

		var splitUrl = a.split("code=");

		console.log("CODE:", encodeURI(splitUrl[1]))
		result = encodeURI(splitUrl[1]);

		$.ajax({
			type : "POST",
			contentType : "application/json",
			url : "/cms/MIDataExplorer/getOAuth2Code",
			// 			url : "/logInController/getUserGoogleInfo",
			data : {
				"code" : result
			},
			// 			dataType : 'json',
			success : function(e) {
				console.log("Success: ", e)

			},
			error : function(e) {
				console.log("Error: ", e);
			},
			done : function(e) {
				console.log("DONE");
			}
		});
	}

	//===========================================
	$(document).ready(function() {
		var csrfToken = document.getElementsByName("_csrf")[0].value;
		console.log(csrfToken);

		$("#config").submit(function() {
			var vars = {};
			vars["key"] = $("#key").val();
			vars["value"] = $("#value").val();
			$.ajax({
				type : "PUT",
				contentType : "application/json",

				url : "/cms/MIDataExplorer/addConfig?_csrf=" + csrfToken,
				data : JSON.stringify(vars),
				dataType : 'json',
				success : function(e) {
					console.log("Success: ", e)
				},
				error : function(e) {
					console.log("Error: ", e);
				},
				done : function(e) {
					console.log("DONE");
				}
			});
		});
	});
</script>

</head>
<body>
	<button onclick="test()">test</button>

	<a href="/cms/MIDataExplorer/openAdminsPage">Admins</a>
	<br />
	<br />
	<a href="/cms/MIDataExplorer/openStudiesPage">Studies</a>
	<br />
	<br />
	<a href="/site/MIDataExplorer/openSite">Site</a>
	<br />
	<br />
	<a href="/cms/MIDataExplorer/getOAuth2">OAtuh2</a>
	<br />
	<br />

	<hr>
	<form id="load-about">
		<input type="hidden" name="_csrf" value="${_csrf.token}" /> <input
			type="submit">
	</form>

	<div id="about-div"></div>

	<br />
	<br />

	<form id="about-form">
		<input type="hidden" name="_csrf" value="${_csrf.token}" />
		<textarea rows="30" cols="60" id="about-text"></textarea>
		<input type="submit">
	</form>
	<hr>

	<h1>Config</h1>
	<form id="config">
		<input type="hidden" name="_csrf" value="${_csrf.token}" /> Key: <input
			id="key" type="text"> Value: <input id="value" type="text">
		<input type="submit">
	</form>

</body>
</html>
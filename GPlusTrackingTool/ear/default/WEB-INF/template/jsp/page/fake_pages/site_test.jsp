<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<script type="text/javascript">
	$(document)
			.ready(
					function() {
						// 						var csrfToken = document.getElementsByName("_csrf")[0].value;
						// 						console.log(csrfToken);

						$("#public-studies-list")
								.submit(
										function() {
											$
													.ajax({
														type : "GET",
														contentType : "application/json",

														url : "/site/MIDataExplorer/loadPublicStudies",
														// 																+ csrfToken,

														dataType : 'json',
														success : function(e) {
															console
																	.log(
																			"Success: ",
																			e)
															document
																	.getElementById("studies-list").innerHTML = JSON
																	.stringify(e)
														},
														error : function(e) {
															console.log(
																	"Error: ",
																	e);
														},
														done : function(e) {
															console.log("DONE");
														}
													});
											return false;
										});
					});

	//==================================================
	$(document).ready(function() {
		$("#study-search-input").keyup(function() {
			var vars = $("#study-search-input").val();
			$.ajax({
				type : "GET",
				contentType : "application/json",

				url : "/site/MIDataExplorer/searchInPublicStudies",

				data : {
					"searchWord" : vars
				},
				dataType : 'json',
				success : function(e) {
					console.log("Success: ", e)

				},
				error : function(e) {
					console.log("Error: ", e);
				},
				done : function(e) {
					console.log("DONE");
				}
			});
			return false;
		});
	});

	//=========================================

	$(document).ready(function() {
		$("#add-favo-study").submit(function() {
			var csrfToken = document.getElementsByName("_csrf")[0].value;
			console.log(csrfToken)
			var vars = {}
			vars['id'] = $("#add-study-id").val();
			$.ajax({
				type : "PUT",
				contentType : "application/json",

				url : "/site/MIDataExplorer/addFavoStudy?_csrf=" + csrfToken,

				data : JSON.stringify(vars),
				dataType : 'json',
				success : function(e) {
					console.log("Success: ", e)

				},
				error : function(e) {
					console.log("Error: ", e);
				},
				done : function(e) {
					console.log("DONE");
				}
			});
			return false;
		});
	});

	//===================================================

	$(document)
			.ready(
					function() {

						$("#load-favo-studies")
								.submit(
										function() {
											$
													.ajax({
														type : "GET",
														contentType : "application/json",

														url : "/site/MIDataExplorer/loadFavoStudies",
														// 																+ csrfToken,

														dataType : 'json',
														success : function(e) {
															console
																	.log(
																			"Success: ",
																			e)
															document
																	.getElementById("favo-studies-list").innerHTML = JSON
																	.stringify(e)
														},
														error : function(e) {
															console.log(
																	"Error: ",
																	e);
														},
														done : function(e) {
															console.log("DONE");
														}
													});
											return false;
										});
					});

	//=========================================

	$(document)
			.ready(
					function() {
						$("#delete-favo-study")
								.submit(
										function() {
											var csrfToken = document
													.getElementsByName("_csrf")[0].value;
											console.log(csrfToken)
											var vars = {}
											vars['id'] = $("#delete-study-id")
													.val();
											$
													.ajax({
														type : "POST",
														contentType : "application/json",

														url : "/site/MIDataExplorer/removeFavoStudies?_csrf="
																+ csrfToken,

														data : JSON
																.stringify(vars),
														dataType : 'json',
														success : function(e) {
															console
																	.log(
																			"Success: ",
																			e)

														},
														error : function(e) {
															console.log(
																	"Error: ",
																	e);
														},
														done : function(e) {
															console.log("DONE");
														}
													});
											return false;
										});
					});

	//=========================================

	$(document).ready(function() {
		$("#create-widget").submit(function() {
			var csrfToken = document.getElementsByName("_csrf")[0].value;
			console.log(csrfToken)
			var vars = {}
			vars['title'] = $("#widget-name").val();
			vars['studyID'] = $("#study-id").val();
			vars['contentURL'] = $("#widget-url").val();
			vars['type'] == $("#widget-type").val();
			$.ajax({
				type : "PUT",
				contentType : "application/json",

				url : "/site/MIDataExplorer/addWidget?_csrf=" + csrfToken,

				data : JSON.stringify(vars),
				dataType : 'json',
				success : function(e) {
					console.log("Success: ", e)

				},
				error : function(e) {
					console.log("Error: ", e);
				},
				done : function(e) {
					console.log("DONE");
				}
			});
			return false;
		});
	});
	//=========================================

	$(document).ready(function() {
		$("#rename-widget").submit(function() {
			var csrfToken = document.getElementsByName("_csrf")[0].value;
			console.log(csrfToken)
			var vars = {}
			vars['title'] = $("#new-name").val();
			vars['id'] = $("#widget-id").val();

			$.ajax({
				type : "PUT",
				contentType : "application/json",

				url : "/site/MIDataExplorer/renameWidget?_csrf=" + csrfToken,

				data : JSON.stringify(vars),
				dataType : 'json',
				success : function(e) {
					console.log("Success: ", e)

				},
				error : function(e) {
					console.log("Error: ", e);
				},
				done : function(e) {
					console.log("DONE");
				}
			});
			return false;
		});
	});

	//=========================================

	$(document).ready(function() {
		$("#delete-widget").submit(function() {
			var csrfToken = document.getElementsByName("_csrf")[0].value;
			console.log(csrfToken)
			var vars = {}

			vars['id'] = $("#delete-widget-id").val();

			$.ajax({
				type : "PUT",
				contentType : "application/json",

				url : "/site/MIDataExplorer/deleteWidget?_csrf=" + csrfToken,

				data : JSON.stringify(vars),
				dataType : 'json',
				success : function(e) {
					console.log("Success: ", e)

				},
				error : function(e) {
					console.log("Error: ", e);
				},
				done : function(e) {
					console.log("DONE");
				}
			});
			return false;
		});
	});

	//=========================================

	$(document)
			.ready(
					function() {
						$("#load-widgets")
								.submit(
										function() {
											var csrfToken = document
													.getElementsByName("_csrf")[0].value;

											$
													.ajax({
														type : "GET",
														contentType : "application/json",

														url : "/site/MIDataExplorer/loadLoggedInUserWidget?_csrf="
																+ csrfToken,
														dataType : 'json',
														success : function(e) {
															console
																	.log(
																			"Success: ",
																			e)
															document
																	.getElementById("loaded-widgets").innerHTML = JSON
																	.stringify(e)

														},
														error : function(e) {
															console.log(
																	"Error: ",
																	e);
														},
														done : function(e) {
															console.log("DONE");
														}
													});
											return false;
										});
					});

	//=========================================

	$(document)
			.ready(
					function() {
						$("#load-study-data")
								.submit(
										function() {

											var csrfToken = document
													.getElementsByName("_csrf")[0].value;

											$
													.ajax({
														type : "GET",
														contentType : "application/json",

														url : "/site/MIDataExplorer/loadStudyData?_csrf="
																+ csrfToken,
														data : {
															"studyId" : document
																	.getElementsByName("load-study-id")[0].value
														},
														dataType : 'json',
														success : function(e) {
															console
																	.log(
																			"Success: ",
																			e)

														},
														error : function(e) {
															console.log(
																	"Error: ",
																	e);
														},
														done : function(e) {
															console.log("DONE");
														}
													});
											return false;
										});
					});
</script>


</head>
<body>

	<h1>Load Public Studies</h1>
	<form id="public-studies-list">
		<input type="submit">
	</form>
	<div id="studies-list"></div>

	<hr>
	<h1>Search Study</h1>
	<form id="study-search">
		Search Word: <input type="text" id="study-search-input">
	</form>


	<hr>

	<h1>Add Favo Study</h1>
	<form id="add-favo-study">
		<input type="hidden" name="_csrf" value="${_csrf.token}" /> Study ID:
		<input type="text" id="add-study-id" /> <input type="submit">
	</form>


	<hr>

	<h1>Load Favor. Studies for LoggedIn USer</h1>
	<form id="load-favo-studies">
		<input type="hidden" name="_csrf" value="${_csrf.token}" /> <input
			type="submit">
	</form>
	<div id="favo-studies-list"></div>

	<hr>

	<h1>Remove Favo. Study</h1>
	<form id="delete-favo-study">
		<input type="hidden" name="_csrf" value="${_csrf.token}" /> Study ID:
		<input type="text" id="delete-study-id" /> <input type="submit">
	</form>
	<hr>
	<hr>
	<center>
		<h1>Widget</h1>
	</center>

	<h1>Create New Widget</h1>
	<form id="create-widget">
		Widget Name: <input type="text" id="widget-name"><br />
		Widget Type: <input type="text" id="widget-type"><br /> Study
		ID: <input type="text" id="study-id"><br /> <input
			id="widget-url" name="contentURL" type="hidden"
			value="qs=828012&amp;w=998002&amp;l=882001&amp;b=828001&amp;
			fd=828019_4&amp;fd=828018_2&amp;fd=828020_2&amp;fd=828024_2&amp;
			fd=828023_4&amp;fd=828022_1&amp;fd=828021_8&amp;stacked=false">
		<input type="hidden" name="_csrf" value="${_csrf.token}" /> <br /> <input
			type="submit">
	</form>

	<hr>
	<h1>Rename Widget</h1>
	<form id="rename-widget">
		<input type="hidden" name="_csrf" value="${_csrf.token}" /> Widget
		ID: <input type="text" id="widget-id"><br /> New Name: <input
			type="text" id="new-name"><br /> <input type="submit">
	</form>
	<hr>
	<h1>Delete Widget</h1>
	<form id="delete-widget">
		<input type="hidden" name="_csrf" value="${_csrf.token}" /> Widget
		ID: <input type="text" id="delete-widget-id"><br /> <input
			type="submit">
	</form>

	<hr>
	<h1>Load Widgets</h1>
	<form id="load-widgets">
		<input type="hidden" name="_csrf" value="${_csrf.token}" /> <input
			type="submit">
	</form>
	<div id="loaded-widgets"></div>

	<hr>
	<h1>Get Filters Value</h1>
	<form action="/site/MIDataExplorer/getValue">
		<input type="hidden" name="_csrf" value="${_csrf.token}" /> <input
			type="submit">
	</form>
	<hr>
	<h1>Load Study Exported Data</h1>
	<form id="load-study-data">
		<input type="hidden" name="_csrf" value="${_csrf.token}" /> Study Id:
		<input type="text" name="load-study-id"> <input type="submit">
	</form>
	<!-- 	================================================================================================================ -->
	<!-- 	================================================================================================================ -->
	<!-- 	================================================================================================================ -->

	<script type="text/javascript">
		$(document)
				.ready(
						function() {
							$("#truncat")
									.submit(
											function() {
												var csrfToken = document
														.getElementsByName("_csrf")[0].value;

												$
														.ajax({
															type : "GET",
															contentType : "application/json",

															url : "/site/MIDataExplorer/truncate_db?_csrf="
																	+ csrfToken,

															dataType : 'json',
															success : function(
																	e) {
																console
																		.log(
																				"Success: ",
																				e)

															},
															error : function(e) {
																console
																		.log(
																				"Error: ",
																				e);
															},
															done : function(e) {
																console
																		.log("DONE");
															}
														});
												return false;
											});
						});
	</script>

	<hr>

	<script type="text/javascript">
		var questionObject = {
			"questionText" : "How often do you access the internet for any reason (no matter if it is personal or job-related)?",
			"datasheets" : [ {
				"raw" : {
					"kind" : "DataSheet",
					"appId" : "gweb-scalable-research-staging",
					"id" : 4910865606246400
				}
			} ],
			"base" : "All Respondents",
			"transientAnswers" : [ {
				"answerText" : "At least once a day",
				"questionKey" : {
					"raw" : {
						"kind" : "Question",
						"appId" : "gweb-scalable-research-staging",
						"id" : 6678880303710208
					}
				},
				"datasheets" : [ {
					"raw" : {
						"kind" : "DataSheet",
						"appId" : "gweb-scalable-research-staging",
						"id" : 4910865606246400
					}
				} ],
				"dataSets" : [

				],
				"onTrend" : true,
				"onSnapshot" : true,
				"questionAnswerKey" : "Q4781947867889664A5344897821310976",
				"studyKey" : {
					"raw" : {
						"kind" : "Study",
						"appId" : "gweb-scalable-research-staging",
						"id" : 5410868518977536
					}
				},
				"transientValue" : 0.0,
				"deleted" : false,
				"id" : 4638186722557952
			}, {
				"answerText" : "At least once a week",
				"questionKey" : {
					"raw" : {
						"kind" : "Question",
						"appId" : "gweb-scalable-research-staging",
						"id" : 6678880303710208
					}
				},
				"datasheets" : [ {
					"raw" : {
						"kind" : "DataSheet",
						"appId" : "gweb-scalable-research-staging",
						"id" : 4910865606246400
					}
				} ],
				"dataSets" : [

				],
				"onTrend" : true,
				"onSnapshot" : true,
				"questionAnswerKey" : "Q6678880303710208A5764086629400576",
				"studyKey" : {
					"raw" : {
						"kind" : "Study",
						"appId" : "gweb-scalable-research-staging",
						"id" : 5410868518977536
					}
				},
				"transientValue" : 0.0,
				"deleted" : false,
				"id" : 5764086629400576
			} ],
			"transientFilters" : {
				"Parent" : [ "No" ],
				"Agegroup" : [ "All" ],
				"Occupation" : [ "Not doing paid work (incl. DK/NA)" ],
				"Gender" : [ "male" ],
				"Wave" : [ "2016" ],
				"Country" : [ "All" ],
				"InetUserP" : [ "All" ],
				"YTUser" : [ "All" ]
			},
			"listOrder" : 0,
			"studyKey" : {
				"raw" : {
					"kind" : "Study",
					"appId" : "gweb-scalable-research-staging",
					"id" : 5410868518977536
				}
			},
			"questionIndex" : "Q5 : Overall Internet Usage",
			"deleted" : false,
			"id" : 6678880303710208
		};

		var x = JSON.stringify([ questionObject, questionObject ]);
		var bean = {}
		bean["name"] = x

		$(document)
				.ready(
						function() {
							$("#get-filter-value")
									.submit(
											function() {
												console.log(x);

												$
														.ajax({
															type : "POST",
															contentType : "application/json",

															url : "/site/MIDataExplorer/getFiltersValue?_csrf="
																	+ document
																			.getElementsByName("_csrf")[0].value,

															data : x,
															dataType : 'json',
															success : function(
																	e) {

															},
															error : function(e) {

															},
															done : function(e) {

															}
														});
												return false;
											});
						});
	</script>
	<h1>Get Value From LookUp</h1>

	<form id="get-filter-value">
		<input type="hidden" name="_csrf" value="${_csrf.token}" /> <input
			type="submit">
	</form>
	<hr>
	<h1>Truncat DB</h1>
	<form id="truncat">
		<input type="hidden" name="_csrf" value="${_csrf.token}" /> <input
			type="submit">
	</form>

</body>
</html>
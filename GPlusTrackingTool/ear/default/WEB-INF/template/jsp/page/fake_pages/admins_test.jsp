<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page
	import="com.google.appengine.api.blobstore.BlobstoreServiceFactory"%>
<%@ page import="com.google.appengine.api.blobstore.BlobstoreService"%>

<%
	BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>
	$(document)
			.ready(
					function() {
						$("#add-admin-form")
								.submit(
										function() {
											var vars = {}
											vars["firstName"] = $("#firstName")
													.val();
											vars["lastName"] = $("#lastName")
													.val();
											vars["email"] = $("#email").val();
											vars["status"] = "ACTIVE";
											$
													.ajax({
														type : "PUT",
														contentType : "application/json",
														url : "/cms/MIDataExplorer/createNewAdmin?_csrf="
																+ $(
																		'input#_csrf')
																		.val(),
														data : JSON
																.stringify(vars),
														dataType : 'json',
														success : function(e) {
															console
																	.log(
																			"Success: ",
																			e);
														},
														error : function(e) {
															console.log(
																	"Error: ",
																	e);
														},
														done : function(e) {
															console.log("DONE");
														}
													});
											return false;
										});
					});
	// 	List All Admin

	$(document)
			.ready(
					function() {
						$("#admins-list")
								.submit(
										function() {
											console.log($('input#_csrf').val())
											$
													.ajax({
														type : "GET",
														contentType : "application/json",
														url : "/cms/MIDataExplorer/loadAdmins?_csrf="
																+ $(
																		'input#_csrf')
																		.val(),
														data : {
															"limit" : $(
																	'input#limit')
																	.val(),
															"curser" : $(
																	'input#curser')
																	.val()
														},
														dataType : 'json',
														success : function(e) {
															console
																	.log(
																			"Success: ",
																			e);

															document
																	.getElementById("admins").innerHTML = "FirstName &thinsp; LastName &thinsp;  Email &thinsp; ID<br>";
															for (var i = 0; i < e.length; i++) {
																var itm = e[i];

																document
																		.getElementById("admins").innerHTML += itm['firstName']
																		+ " &thinsp;"
																		+ itm['lastName']
																		+ "&thinsp;"
																		+ itm['email']
																		+ "&thinsp;"
																		+ itm['id']
																		+ "<br>";
															}

														},
														error : function(e) {
															console.log(
																	"Error: ",
																	e);
														},
														done : function(e) {
															console.log("DONE");
														}
													});
											return false;
										});
					});

	///=====================================
	$(document).ready(function() {
		$("#admins-page").submit(function() {
			console.log($('input#_csrf').val())
			$.ajax({
				type : "GET",
				contentType : "application/json",
				url : "/cms/MIDataExplorer?_csrf=" + $('input#_csrf').val(),
				dataType : 'json',
				success : function(e) {
					console.log("Success: ", e);

				},
				error : function(e) {
					console.log("Error: ", e);
				},
				done : function(e) {
					console.log("DONE");
				}
			});
			return false;
		});
	});

	//=======================

	$(document).ready(
			function() {
				$("#edit-admin-by-id").submit(
						function() {
							var vars = {}
							vars["firstName"] = $("#firstName-edit").val();
							vars["lastName"] = $("#lastName-edit").val();
							vars["email"] = $("#email-edit").val();
							vars["status"] = $('#status-edit').is(":checked");
							vars['id'] = $("#admin-id-edit").val();
							console.log($("#admin-id-edit").val());

							$.ajax({
								type : "PUT",
								contentType : "application/json",
								url : "/cms/MIDataExplorer/editAdmin?_csrf="
										+ $('input#_csrf').val(),
								data : JSON.stringify(vars),
								dataType : 'json',
								success : function(e) {
									console.log("Success: ", e);
								},
								error : function(e) {
									console.log("Error: ", e);
								},
								done : function(e) {
									console.log("DONE");
								}
							});
							return false;
						});
			});

	//=======================

	$(document).ready(
			function() {
				$("#delete-admin").submit(
						function() {
							var vars = {}
							vars['id'] = $("#delete-admin-input").val();

							$.ajax({
								type : "POST",
								contentType : "application/json",
								url : "/cms/MIDataExplorer/deleteAdmin?_csrf="
										+ $('input#_csrf').val(),
								data : JSON.stringify(vars),
								dataType : 'json',
								success : function(e) {
									console.log("Success: ", e);
								},
								error : function(e) {
									console.log("Error: ", e);
								},
								done : function(e) {
									console.log("DONE");
								}
							});
							return false;
						});
			});

	//=======================
	$(document)
			.ready(
					function() {
						$("#admin-lookup")
								.submit(
										function() {

											$
													.ajax({
														type : "GET",
														contentType : "application/json",
														url : "/cms/MIDataExplorer/loadAdminsLoopUp?_csrf="
																+ $(
																		'input#_csrf')
																		.val(),

														dataType : 'json',
														success : function(e) {
															console
																	.log(
																			"Success: ",
																			e);
															document
																	.getElementById("admin-lookup-div").innerHTML = JSON
																	.stringify(e)
														},
														error : function(e) {
															console.log(
																	"Error: ",
																	e);
														},
														done : function(e) {
															console.log("DONE");
														}
													});
											return false;
										});
					});
</script>
</head>
<body>
	<h1>AdminsPAge</h1>
	<form id="admins-page">
		<input type="hidden" id="_csrf" value="${_csrf.token}" /> <input
			type="submit">
	</form>
	<h1>List of Admins</h1>
	<form id="admins-list">
		Curser: <input type="text" id="curser"> <br> Limit: <input
			type="text" id="limit"> <input type="hidden" id="_csrf"
			value="${_csrf.token}" /> <input type="submit">
	</form>
	<div id="admins"></div>

	<hr>


	<h1>Add New Admin</h1>
	<form id="add-admin-form">
		First Name:<input type="text" id="firstName"> <br> Last
		Name: <input type="text" id="lastName"> <br> Email: <input
			type="text" id="email"><br> <input type="hidden"
			id="_csrf" value="${_csrf.token}" /> <input id="add-admin"
			type="submit">
	</form>
	<hr>

	<h1>Edit Admin By ID</h1>

	<form id="edit-admin-by-id">
		First Name:<input type="text" id="firstName-edit"> <br>
		Last Name: <input type="text" id="lastName-edit"> <br>
		Email: <input type="text" id="email-edit"><br> Status: <input
			type="checkbox" id="status-edit"> <br> <input
			type="hidden" id="_csrf" value="${_csrf.token}" /> ID: <input
			id='admin-id-edit' type="text"> <input id="add-admin"
			type="submit">
	</form>
	<hr>

	<h1>Delete Admin By ID</h1>

	<form id="delete-admin">
		<input id='delete-admin-input' type="text"> <input
			type="hidden" id="_csrf" value="${_csrf.token}" /> <input
			type="submit">
	</form>

	<hr>
	<h1>Load Admins LookUp</h1>
	<form id="admin-lookup">
		<input type="hidden" id="_csrf" value="${_csrf.token}" /> <input
			type="submit">
	</form>
	<div id="admin-lookup-div"></div>


</body>
</html>
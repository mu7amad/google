<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>



<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<script>
	var logoUrl;
	var logoBlobKey;
	var upload_url;

	$(document).ready(function() {
		$("#upload-logo").submit(function() {
			var obj = new FormData(this)
			console.log(obj)
			$.ajax({
				type : "POST",
				// 				url : '${study.uploadUrl }',
				url : upload_url,
				data : new FormData(this),
				contentType : false,
				cache : false,
				processData : false,
				dataType : 'json',
				success : function(e) {
					console.log("Success: ", e);
					var jsonString = JSON.stringify(e);
					var parser = JSON.parse(jsonString);
					logoUrl = parser.logoUrl;
					logoBlobKey = parser.logoBlobKey;
					document.getElementById("logo-id").src = logoUrl;

				},
				error : function(e) {
					console.log("Error: ", e);
				},
				done : function(e) {
					console.log("DONE");
				}
			});
			return false;
		});
	});
	///=================

	$(document)
			.ready(
					function() {
						$("#create-study")
								.submit(
										function() {
											var vars = {}

											vars['studyName'] = $("#study-name")
													.val();
											vars['description']
											$("#study-desc").val();
											vars['totalInterviewed'] = $(
													"#study-interviewed").val();
											vars['logoUrl'] = logoUrl;
											vars['logoBlobKey'] = JSON
													.stringify(logoBlobKey);
											vars['publishers'] = $(
													"#STUDY-PUBLISHERS").val()
													.split(",");
											vars['uploaders'] = $(
													"#STUDY-UPLOADERS").val()
													.split(",");
											vars['sharedWith'] = $(
													"#study-shared-suers")
													.val().split(",");

											console.log("LogoURL: ", logoUrl);
											console.log("LogoBlobKey: ",
													logoBlobKey);

											$
													.ajax({
														type : "PUT",
														contentType : "application/json",
														url : "/cms/MIDataExplorer/createNewStudy?_csrf="
																+ $(
																		'input#_csrf')
																		.val(),
														data : JSON
																.stringify(vars),
														dataType : 'json',
														success : function(e) {
															console
																	.log(
																			"Success: ",
																			e);
														},
														error : function(e) {
															console.log(
																	"Error: ",
																	e);
														},
														done : function(e) {
															console.log("DONE");
														}
													});
											return false;
										});
					});

	/////////////===========================

	$(document)
			.ready(
					function() {
						$("#studies-list")
								.submit(
										function() {
											$
													.ajax({
														type : "GET",
														contentType : "application/json",
														url : "/cms/MIDataExplorer/loadStudies?_csrf="
																+ $(
																		'input#_csrf')
																		.val(),
														dataType : 'json',
														success : function(e) {
															console
																	.log(
																			"Success: ",
																			e);

															document
																	.getElementById("studies").innerHTML = "Logo &thinsp; Name &thinsp;  Owner &thinsp; PendingFiles &thinsp; ID<br>";
															for (var i = 0; i < e.length; i++) {
																var itm = e[i];

																document
																		.getElementById("studies").innerHTML += "<img width=60 height=60 src="
																		+ itm['logoUrl']+">"
																		+ itm['studyName']
																		+ " &thinsp;"
																		+ itm['ownerEmail']
																		+ "&thinsp;"
																		+ "PendingFiles"
																		+ "&thinsp;"
																		+ itm['id']
																		+ "<br>";
															}

														},
														error : function(e) {
															console.log(
																	"Error: ",
																	e);
														},
														done : function(e) {
															console.log("DONE");
														}
													});
											return false;
										});
					});
	//=======================

	$(document).ready(
			function() {
				$("#delete-study").submit(
						function() {
							var vars = {}
							vars['id'] = $("#delete-study-input").val();

							$.ajax({
								type : "POST",
								contentType : "application/json",
								url : "/cms/MIDataExplorer/deleteStudy?_csrf="
										+ $('input#_csrf').val(),
								data : JSON.stringify(vars),
								dataType : 'json',
								success : function(e) {
									console.log("Success: ", e);
								},
								error : function(e) {
									console.log("Error: ", e);
								},
								done : function(e) {
									console.log("DONE");
								}
							});
							return false;
						});
			});
	///=====================================

	$(document).ready(
			function() {
				$("#edit-study").submit(
						function() {
							var vars = {}
							vars['id'] = $("#edit-study-id").val();
							vars['studyName'] = $("#edit-study-name").val();
							vars['description'] = $("#edit-study-desc").val();
							vars['totalInterviewed'] = $(
									"#edit-study-interviewed").val();
							vars['logoUrl'] = logoUrl;
							vars['logoBlobKey'] = JSON.stringify(logoBlobKey);
							vars['publishers'] = $("#edit-STUDY-PUBLISHERS")
									.val().split(",");
							vars['uploaders'] = $("#edit-STUDY-UPLOADERS")
									.val().split(",");
							vars['sharedWith'] = $("#edit-study-shared-suers")
									.val().split(",");

							console.log("LogoURL: ", logoUrl);
							console.log("LogoBlobKey: ", logoBlobKey);

							$.ajax({
								type : "POST",
								contentType : "application/json",
								url : "/cms/MIDataExplorer/editStudy?_csrf="
										+ $('input#_csrf').val(),
								data : JSON.stringify(vars),
								dataType : 'json',
								success : function(e) {
									console.log("Success: ", e);
								},
								error : function(e) {
									console.log("Error: ", e);
								},
								done : function(e) {
									console.log("DONE");
								}
							});
							return false;
						});
			});

	/////////////===========================
	$(document).ready(function() {
		$("#upload-logo2").submit(function() {
			$.ajax({
				type : "POST",
				url : '${study.uploadUrl }',
				data : new FormData(this),
				contentType : false,
				cache : false,
				processData : false,
				dataType : 'json',
				success : function(e) {
					console.log("Success: ", e);
					var jsonString = JSON.stringify(e);
					var parser = JSON.parse(jsonString);
					logoUrl = parser.logoUrl;
					logoBlobKey = parser.logoBlobKey;
					document.getElementById("logo-id2").src = logoUrl;

				},
				error : function(e) {
					console.log("Error: ", e);
				},
				done : function(e) {
					console.log("DONE");
				}
			});
			return false;
		});
	});
	///=====================================
	$(document).ready(
			function() {
				$("#get-study-by-id").submit(
						function() {
							var vars = {}
							vars["id"] = $("#study-id").val();
							console.log(vars['id']);
							$.ajax({
								type : "POST",
								contentType : "application/json",
								url : "/cms/MIDataExplorer/getAdminById?_csrf="
										+ $('input#_csrf').val(),
								data : JSON.stringify(vars),
								dataType : 'json',
								success : function(e) {
									console.log("Success: ", e);
								},
								error : function(e) {
									console.log("Error: ", e);
								},
								done : function(e) {
									console.log("DONE");
								}
							});
							return false;
						});
			});
	///========================
	$(document)
			.ready(
					function() {
						$("#studies-by-loggedIn-user")
								.submit(
										function() {
											$
													.ajax({
														type : "GET",
														contentType : "application/json",
														url : "/cms/MIDataExplorer/getLoggedInUserStudies?_csrf="
																+ $(
																		'input#_csrf')
																		.val(),

														// 				data : JSON.stringify(vars),
														dataType : 'json',
														success : function(e) {
															console
																	.log(
																			"Success: ",
																			e);
															document
																	.getElementById("studies-user").innerHTML = JSON
																	.stringify(e)
														},
														error : function(e) {
															console.log(
																	"Error: ",
																	e);
														},
														done : function(e) {
															console.log("DONE");
														}
													});
											return false;
										});
					});
	///=================
	$(document)
			.ready(
					function() {
						$("#studies-shared-with-loggedIn-user")
								.submit(
										function() {
											$
													.ajax({
														type : "GET",
														contentType : "application/json",
														url : "/cms/MIDataExplorer/getLoggedInSharedWithStudies?_csrf="
																+ $(
																		'input#_csrf')
																		.val(),

														// 				data : JSON.stringify(vars),
														dataType : 'json',
														success : function(e) {
															console
																	.log(
																			"Success: ",
																			e);
															document
																	.getElementById("studies-shared-with-user").innerHTML = JSON
																	.stringify(e)
														},
														error : function(e) {
															console.log(
																	"Error: ",
																	e);
														},
														done : function(e) {
															console.log("DONE");
														}
													});
											return false;
										});
					});
	///=================
	$(document)
			.ready(
					function() {
						$("#studies-assigned-to-loggedIn-user")
								.submit(
										function() {
											$
											upload_url
													.ajax({
														type : "GET",
														contentType : "application/json",
														url : "/cms/MIDataExplorer/getLoggedAssignedStudies?_csrf="
																+ $(
																		'input#_csrf')
																		.val(),

														// 				data : JSON.stringify(vars),
														dataType : 'json',
														success : function(e) {
															console
																	.log(
																			"Success: ",
																			e);
															document
																	.getElementById("studies-assigned-to-user").innerHTML = JSON
																	.stringify(e)
														},
														error : function(e) {
															console.log(
																	"Error: ",
																	e);
														},
														done : function(e) {
															console.log("DONE");
														}
													});
											return false;
										});
					});
	///=================
	$(document).ready(
			function() {
				$("#study-search-input").keyup(
						function() {
							var vars = $("#study-search-input").val();
							$.ajax({
								type : "GET",
								contentType : "application/json",

								url : "/cms/MIDataExplorer/searchStudy?_csrf="
										+ $('input#_csrf').val(),

								data : {
									"searchWord" : vars
								},
								dataType : 'json',
								success : function(e) {
									console.log("Success: ", e)

								},
								error : function(e) {
									console.log("Error: ", e);
								},
								done : function(e) {
									console.log("DONE");
								}
							});
							return false;
						});
			});
	///=================
	$(document)
			.ready(
					function() {
						$("#open-study")
								.submit(
										function() {
											var vars = {}
											vars['studyID'] = $("#open-study-id")
													.val();
											$
													.ajax({
														type : "POST",
														contentType : "application/json",
														url : "/cms/MIDataExplorer/openStudyDataSheets?_csrf="
																+ $(
																		'input#_csrf')
																		.val(),

														data : JSON
																.stringify(vars),
														dataType : 'json',
														success : function(e) {
															console
																	.log(
																			"Success: ",
																			e);
															document
																	.getElementById("open-study-div").innerHTML = JSON
																	.stringify(e)
														},
														error : function(e) {
															console.log(
																	"Error: ",
																	e);
														},
														done : function(e) {
															console.log("DONE");
														}
													});
											return false;
										});
					});
	///=================
	$(document)
			.ready(
					function() {
						$("#open-zip-file")
								.submit(
										function() {
											var vars = {}
											vars['id'] = $("#zip-file-id")
													.val();
											$
													.ajax({
														type : "POST",
														contentType : "application/json",
														url : "/cms/MIDataExplorer/viewCsvFiles?_csrf="
																+ $(
																		'input#_csrf')
																		.val(),

														data : JSON
																.stringify(vars),
														dataType : 'json',
														success : function(e) {
															console
																	.log(
																			"Success: ",
																			e);
															document
																	.getElementById("open-zip-div").innerHTML = JSON
																	.stringify(e)
														},
														error : function(e) {
															console.log(
																	"Error: ",
																	e);
														},
														done : function(e) {
															console.log("DONE");
														}
													});
											return false;
										});
					});
	///=================
	$(document)
			.ready(
					function() {
						$("#delete-zip-file")
								.submit(
										function() {
											var vars = {}
											vars['id'] = $(
													"#delete-zip-file-id")
													.val();
											$
													.ajax({
														type : "POST",
														contentType : "application/json",
														url : "/cms/MIDataExplorer/deleteZipFile?_csrf="
																+ $(
																		'input#_csrf')
																		.val(),

														data : JSON
																.stringify(vars),
														dataType : 'json',
														success : function(e) {
															console
																	.log(
																			"Success: ",
																			e);
															document
																	.getElementById("open-zip-div").innerHTML = JSON
																	.stringify(e)
														},
														error : function(e) {
															console.log(
																	"Error: ",
																	e);
														},
														done : function(e) {
															console.log("DONE");
														}
													});
											return false;
										});
					});
	///=================
	$(document)
			.ready(
					function() {
						$("#download-zip-file")
								.submit(
										function() {
											alert()
											$
													.ajax({
														type : "POST",
														url : "/cms/MIDataExplorer/downloadZipFile?_csrf="
																+ $(
																		'input#_csrf')
																		.val(),

														data : {
															"zipId" : $(
																	"#zipId")
																	.val()
														},
														success : function(e) {
															// 															console
															// 																	.log(
															// 																			"Success: ",
															// 																			e);

														},
														error : function(e) {
															console.log(
																	"Error: ",
																	e);
														},
														done : function(e) {
															console.log("DONE");
														}
													});
											return false;
										});
					});
	///=================
	$(document)
			.ready(
					function() {
						$("#delete-csv")
								.submit(
										function() {
											var vars = {}
											vars['id'] = $("#delete-csv-id")
													.val();

											$
													.ajax({
														type : "POST",
														contentType : "application/json",
														url : "/cms/MIDataExplorer/deleteCsvFile?_csrf="
																+ $(
																		'input#_csrf')
																		.val(),
														data : JSON
																.stringify(vars),
														dataType : 'json',
														success : function(e) {
															console
																	.log(
																			"Success: ",
																			e);
														},
														error : function(e) {
															console.log(
																	"Error: ",
																	e);
														},
														done : function(e) {
															console.log("DONE");
														}
													});
											return false;
										});
					});
	///=====================================
	$(document)
			.ready(
					function() {
						$("#pendingFiles")
								.submit(
										function() {

											$
													.ajax({
														type : "GET",
														contentType : "application/json",
														url : "/cms/MIDataExplorer/getLoggedInUserPendingFiles?_csrf="
																+ $(
																		'input#_csrf')
																		.val(),
														// 														data : JSON
														// 																.stringify(vars),
														dataType : 'json',
														success : function(e) {
															console
																	.log(
																			"Success: ",
																			e);
															document
																	.getElementById("pending-list").innerHTML = JSON
																	.stringify(e)
														},
														error : function(e) {
															console.log(
																	"Error: ",
																	e);
														},
														done : function(e) {
															console.log("DONE");
														}
													});
											return false;
										});
					});
	///=====================================
	$(document)
			.ready(
					function() {
						$("#upload-url")
								.submit(
										function() {

											$
													.ajax({
														type : "GET",
														contentType : "application/json",
														url : "/cms/MIDataExplorer/createUploadUrl?_csrf="
																+ $(
																		'input#_csrf')
																		.val(),

														dataType : 'json',
														success : function(e) {
															console
																	.log(
																			"Success: ",
																			e);
															upload_url = e;
														},
														error : function(e) {
															console.log(
																	"Error: ",
																	e);
														},
														done : function(e) {
															console.log("DONE");
														}
													});
											return false;
										});
					});
	///=====================================
	$(document)
			.ready(
					function() {
						$("#data-store")
								.submit(
										function() {

											$
													.ajax({
														type : "GET",
														contentType : "application/json",
														url : "/cms/MIDataExplorer/addToDataStore?_csrf="
																+ $(
																		'input#_csrf')
																		.val(),

														dataType : 'json',
														success : function(e) {
															console
																	.log(
																			"Success: ",
																			e);
															upload_url = e;
														},
														error : function(e) {
															console.log(
																	"Error: ",
																	e);
														},
														done : function(e) {
															console.log("DONE");
														}
													});
											return false;
										});
					});
</script>
</head>
<body>

	<h1>Create UploadURL</h1>
	<form id="upload-url">
		<input type="hidden" name="_csrf" value="${_csrf.token}" /> <input
			type="submit">
	</form>

	<hr>
	<!-- 	<h1>Create Study</h1> -->
	<%-- 	<h3>URL: ${study.uploadUrl }</h3> --%>
	<!-- 	<hr> -->
	<br> AMINS: ${adminsLookUp }
	<br>USERS: ${usersLookUp}
	<hr>

	<form id="upload-logo">
		<input id="images" type="file" name="images" value="upload"
			onchange=" $('#upload-logo').submit()">
	</form>
	<img id="logo-id" width="100" height="100">

	<br>
	<form id="create-study">
		<input type="hidden" id="_csrf" value="${_csrf.token}" /> <br>Name:
		<input type="text" id="study-name"> <br> Desc.: <input
			type="text" id="study-desc"> <br> <input type="hidden"
			id="imageUrl" value="${study.logoUrl}" /> <input type="hidden"
			id="imageKey" value="${study.logoBlobKey}" /> UPLOADERS: <INPUT
			TYPE="TEXT" ID="STUDY-UPLOADERS"> <BR> PUBLISHERS: <INPUT
			TYPE="TEXT" ID="STUDY-PUBLISHERS"> <BR> <br>Shared
		Users: <input type="text" id="study-shared-suers"><br>
		Total Interviwed: <input type="text" id="study-interviewed"> <br>
		<input type="submit">
	</form>
	<hr>
	<h1>Studies:</h1>
	<form id="studies-list">
		<input type="hidden" id="_csrf" value="${_csrf.token}" /> <input
			type="submit">
	</form>
	<div id="studies"></div>
	<hr>
	<h1>Delete Study</h1>
	<form id="delete-study">
		<input id='delete-study-input' type="text"> <input
			type="hidden" id="_csrf" value="${_csrf.token}" /> <input
			type="submit">
	</form>
	<hr>
	<h1>Edit Study</h1>

	<form id="upload-logo2">
		<input id="images" type="file" name="images" value="upload"
			onchange=" $('#upload-logo2').submit()">
	</form>
	<img id="logo-id2" width="100" height="100">

	<br>
	<form id="edit-study">
		ID: <input type="text" id="edit-study-id"><br> <input
			type="hidden" id="_csrf" value="${_csrf.token}" /> <br>Name: <input
			type="text" id="edit-study-name"> <br> Desc.: <input
			type="text" id="edit-study-desc"> <br> <input
			type="hidden" id="edit-imageUrl" value="${study.logoUrl}" /> <input
			type="hidden" id="edit-imageKey" value="${study.logoBlobKey}" />
		UPLOADERS: <INPUT TYPE="TEXT" ID="edit-STUDY-UPLOADERS"> <BR>
		PUBLISHERS: <INPUT TYPE="TEXT" ID="edit-STUDY-PUBLISHERS"> <BR>
		<br>Shared Users: <input type="text" id="edit-study-shared-suers"><br>
		Total Interviwed: <input type="text" id="edit-study-interviewed">
		<br> <input type="submit">
	</form>
	<hr>

	<h1>Get Study By ID:</h1>
	<form id="get-study-by-id">
		ID: <input type="text" id="study-id"> <input type="hidden"
			id="_csrf" value="${_csrf.token}" /> <input type="submit">
	</form>

	<hr>

	<h1>Studies Created By LogedIn User:</h1>
	<form id="studies-by-loggedIn-user">
		<input type="hidden" id="_csrf" value="${_csrf.token}" /> <input
			type="submit">
	</form>
	<div id="studies-user"></div>
	<hr>

	<h1>Studies Shared With LogedIn User:</h1>
	<form id="studies-shared-with-loggedIn-user">
		<input type="hidden" id="_csrf" value="${_csrf.token}" /> <input
			type="submit">
	</form>
	<div id="studies-shared-with-user"></div>

	<hr>

	<h1>Studies Assigned To LogedIn User:</h1>
	<form id="studies-assigned-to-loggedIn-user">
		<input type="hidden" id="_csrf" value="${_csrf.token}" /> <input
			type="submit">
	</form>
	<div id="studies-assigned-to-user"></div>

	<hr>

	<form id="study-search">
		<input type="hidden" id="_csrf" value="${_csrf.token}" /> <input
			type="text" id="study-search-input">
	</form>

	<hr>

	<h1>Open Study Sheets</h1>
	<form id="open-study">
		<input type="hidden" id="_csrf" value="${_csrf.token}" /> ID: <input
			type="text" id="open-study-id"> <input type="submit">
	</form>
	<div id="open-study-div"></div>
	<hr>


	<h1>See Csv Files In ZipFile</h1>
	<form id="open-zip-file">
		<input type="hidden" id="_csrf" value="${_csrf.token}" /> ID: <input
			type="text" id="zip-file-id"> <input type="submit">
	</form>
	<div id="open-zip-div"></div>
	<hr>


	<h1>Delete Zip File</h1>
	<form id="delete-zip-file">
		<input type="hidden" id="_csrf" value="${_csrf.token}" /> ID: <input
			type="text" id="delete-zip-file-id"> <input type="submit">
	</form>
	<div id="open-zip-div"></div>
	<hr>

	<h1>Download Zip File</h1>
	<!-- 	<form id="download-zip-file"> -->
	<form action="/cms/MIDataExplorer/downloadZipFile" method="post">
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />ID: <input type="text" name="zipId"
			id="zipId"> <input type="submit">
	</form>

	<hr>

	<h1>Delete Csv File</h1>

	<form id="delete-csv">
		<input type="hidden" name="_csrf" value="${_csrf.token}" />ID: <input
			type="text" id="delete-csv-id"> <input type="submit">
	</form>
	<hr>

	<h1>Logged In User Pending Files</h1>
	<form id="pendingFiles">
		<input type="hidden" name="_csrf" value="${_csrf.token}" /> <input
			type="submit">
	</form>
	<div id="pending-list"></div>
	<hr>

	<h1>DataStore</h1>
	<form id="data-store">
		<input type="hidden" name="_csrf" value="${_csrf.token}" /> <input
			type="submit">
	</form>
	<hr>
	<script type="text/javascript">
		var zip_upload_url;

		$(document)
				.ready(
						function() {
							$("#create-zip-url")
									.submit(
											function() {
												$
														.ajax({
															type : "GET",
															contentType : "application/json",
															url : "/cms/MIDataExplorer/createUploadZipUrl?_csrf="
																	+ $(
																			'input#_csrf')
																			.val(),
															dataType : 'json',
															success : function(
																	e) {
																var jsonString = JSON
																		.stringify(e);
																var parser = JSON
																		.parse(jsonString);
																zip_upload_url = e;
																// 																console.log(e)

															},
															error : function(e) {
																console
																		.log(
																				"Error: ",
																				e);
															},
															done : function(e) {
																console
																		.log("DONE");
															}
														});
												return false;
											});
						});
		var x;
		function previewFile() {
			var preview = document.querySelector('img');
			var file = document.querySelector('#uploadfile').files[0];
			var studyid = $("#upload-zip-study-id").val();
			var reader = new FileReader();

			reader.addEventListener("load", function() {
				preview.src = reader.result;
				console.log(file)
			}, false);

			if (file) {
				reader.readAsDataURL(file);
				// 				console.log(file)
				x = {
					'zipFile' : file,
					'studyid' : studyid
				}
				console.log(x)
			}
		}
		$(document).ready(
				function() {

					$("#upload-zip").submit(
							function() {
								var sId = $("#upload-zip-study-id").val();
								console.log(sId);

								var uploadFile = document
										.getElementById("uploadfile").files[0];
								console.log(uploadFile);
								console.log(uploadFile.name);

								var data = new FormData();
								data.append("zipFile", uploadFile);
								data.append("studyId", sId);
								console.log("data" + data);

								$.ajax({
									type : "POST",
									url : zip_upload_url,
									data : data,
									contentType : false,
									cache : false,
									processData : false,
									dataType : 'json',
									success : function(e) {
										console.log("Success: ", e);

									},
									error : function(e) {
										console.log("Error: ", e);
									},
									done : function(e) {
										console.log("DONE");
									}
								});
								return false;
							});
				});
	</script>
	<h1>Create Zip URL</h1>
	<form id="create-zip-url">
		<input type="hidden" name="_csrf" value="${_csrf.token}" /> <input
			type="submit">
	</form>
	<h1>Upload Zip File</h1>
	<form id="upload-zip">
		<input type="hidden" name="_csrf" value="${_csrf.token}" /> Study ID:<input
			type="text" id="upload-zip-study-id"><br> <input
			type="file" id='uploadfile' name="zipFile"> <input
			type="submit">
	</form>


	<script type="text/javascript">
		$(document)
				.ready(
						function() {

							$("#process-sheet-form")
									.submit(
											function() {
												var sheetID = $("#sheet-id")
														.val();
												console.log(sheetID);

												$
														.ajax({
															type : "GET",
															url : "/dataUploadController/extractDataSheet?_csrf="
																	+ $(
																			'input#_csrf')
																			.val(),
															data : {
																"id" : sheetID
															},
															contentType : "application/json",
															dataType : 'json',
															success : function(
																	e) {
																console
																		.log(
																				"Success: ",
																				e);

															},
															error : function(e) {
																console
																		.log(
																				"Error: ",
																				e);
															},
															done : function(e) {
																console
																		.log("DONE");
															}
														});
												return false;
											});
						});
	</script>

	<hr>
	<h1>Process DataSheet</h1>
	<form id="process-sheet-form">
		<input type="hidden" name="_csrf" value="${_csrf.token}" /> Sheet ID:
		<input type="text" id="sheet-id"> <input type="submit">
	</form>
	<hr>

	<script type="text/javascript">
		$(document)
				.ready(
						function() {

							$("#roll-sheet-form")
									.submit(
											function() {
												var sheetID = $("#sheetId")
														.val();
												console.log(sheetID);

												$
														.ajax({
															type : "GET",
															url : "/cms/MIDataExplorer/rollBackData?_csrf"
																	+ $(
																			'input#_csrf')
																			.val(),
															data : {
																"sheetId" : sheetID
															},
															contentType : "application/json",
															dataType : 'json',
															success : function(
																	e) {
																console
																		.log(
																				"Success: ",
																				e);

															},
															error : function(e) {
																console
																		.log(
																				"Error: ",
																				e);
															},
															done : function(e) {
																console
																		.log("DONE");
															}
														});
												return false;
											});
						});
	</script>

	<h1>RollBackDataSheet</h1>
	<form id="roll-sheet-form">
		<input type="hidden" name="_csrf" value="${_csrf.token}" /> Sheet ID:
		<input type="text" id="sheetId"> <input type="submit">
	</form>
</body>
</html>
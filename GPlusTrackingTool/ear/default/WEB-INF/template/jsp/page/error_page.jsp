<%@ page isErrorPage="true" language="java"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags/site" prefix="templates"%>
<c:choose>
	<c:when test="${status.code == '403' }">
		<templates:site_vanilla footer="${footer}"
			activeTopNavItemId="homeItem"
			title="Dashboard - Google+ Brand Tracker">
<!-- 			<section id="main-content" class="dashboard clearfix colums" -->
<!-- 				role="main"> -->
<!-- 				<p> -->
<!-- 					Please contact <a href="mailTo:googlebtp@googlegroups.com">googlebtp@googlegroups.com</a> -->
<!-- 					to request authorization. -->
<!-- 				</p> -->
<!-- 			</section> -->
			<div class="right-side full-width-block">
				<div class="error"
					style="margin: 7% auto 0; max-width: 390px; min-height: 480px; padding: 30px 0 15px; background: url(//www.google.com/images/errors/robot.png) 100% 5px no-repeat; padding-right: 205px;">
					<a href=""><img
						src="//www.google.com/images/errors/logo_sm.gif" alt="Google">
					</a>
					<p>
						<b>403.</b>
						<ins>Unauthorized access.</ins>
					</p>
					<p>
						Please contact <a href="mailTo:googlebtp@googlegroups.com">googlebtp@googlegroups.com</a>
						to request authorization.
						<ins>That's all we know.</ins>
					</p>
					<p></p>
					<p></p>
				</div>
			</div>
		</templates:site_vanilla>
	</c:when>
	<c:otherwise>


		<!DOCTYPE HTML>
		<html>
<head>
<style>
div.error {
	margin: 7% auto 0;
	max-width: 390px;
	min-height: 180px;
	padding: 30px 0 15px;
	background: url(//www.google.com/images/errors/robot.png) 100% 5px
		no-repeat;
	padding-right: 205px;
}

div.error p {
	margin: 11px 0 22px;
	overflow: hidden;
}

div.error ins {
	color: #777;
	text-decoration: none;
}
</style>

</head>
<body>
	<div class="error">

		<a href=""><img src="//www.google.com/images/errors/logo_sm.gif"
			alt="Google"> </a>
		<p>
			<b>${status.code}.</b>
			<ins>That's an error.</ins>
		</p>
		<p>
			The requested URL was not found on this server.
			<ins>That's all we know.</ins>
		</p>
		<p>${status.message}</p>
		<p></p>
	</div>
</body>
		</html>
	</c:otherwise>
</c:choose>
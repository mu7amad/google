<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ taglib tagdir="/WEB-INF/tags/site" prefix="templates"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="widgets"%>

<c:set var="footer">
	<script src="/site_ui/js/dashboard_main.js"></script>
	<script src="/site_ui/js/jquery.dateFormat-1.0.js"></script>
</c:set>

<templates:site footer="${footer}" activeTopNavItemId="homeItem"
	title="${study.studyName }" logo="${study.logoUrl }"
	studyId="${studyId }">
	<%-- 	title="Dashboard - Google+ Brand Tracker"> --%>
	<section id="main-content" class="dashboard clearfix colums"
		role="main">
		<nav class="left-side">
			<ul class="left-nav">
				<!-- 				<li class="slide-button">Slide</li> -->
				<li><a href="#add-new-dashboard"
					class="popuplink maia-button orange-button" id="add-dashboard-btn">+
						Add New Dashboard</a></li>
				<li class="dashboardsCont">
					<div class="filter-all">
						<div class="main-filter-slider">
							<h3 class="new-exp">
								<span>My Dashboards</span> <a class="plus-icon"></a>
							</h3>
						</div>
						<ul class="left-nav new-nav-tog" id="dashboards"
							style="display: block;">

							<c:forEach var="dashboard" items="${dashboards}"
								varStatus="status">
								<c:if
									test="${(defaultCheck && dashboard.defaultDashBoard==false) || (!defaultCheck && status.index !=0)}">
									<li class="">
										<div class="filter-slider">
											<h3>
												<span class="dashboard-name"><c:out
														value="${dashboard.name}" /></span><a href="#"
													class="expand colapse"></a> <input type="hidden"
													value="${dashboard.id}" class="id" /> <input type="hidden"
													value="${dashboard.userId}" class="userId" />

											</h3>
											<div id="scrollbars${status.index}" class="expand-list"
												style="display: none;">

												<div class="viewport">
													<div class="overview">

														<ul id="brands-list${status.index}">
															<c:if test="${fn:length(widgets)>0}">
																<c:forEach items="${widgets[status.index]}" var="widget">
																	<li><a href="" class="widget-focus widget-name"><c:out
																				value="${widget.title}" /></a> <input type="hidden"
																		class="identity"
																		value="<c:out
																				value='${widget.id}'/>" />
																		<input type="hidden" class="url"
																		value="<c:out
																				value='${widget.contentURL}'/>" />
																		<input type="hidden" class="table"
																		value="<c:out
																				value='${widget.table}'/>" />
																		<input type="hidden" class="trend"
																		value="<c:out
																				value='${widget.trend}'/>" /></li>

																</c:forEach>
															</c:if>
														</ul>

													</div>
												</div>
											</div>
										</div>
									</li>
								</c:if>
								<c:if
									test="${(defaultCheck && dashboard.defaultDashBoard==true) || (!defaultCheck && status.index ==0)}">
									<c:set var="defaultIndex" value="${status.index }"></c:set>
									<li class="active">
										<div class="filter-slider">
											<h3>
												<span class="dashboard-name"><c:out
														value="${dashboard.name}" /></span> <a href="#" class="expand"></a>
												<input type="hidden" value="${dashboard.id}" class="id" />
												<input type="hidden" value="${dashboard.userId}"
													class="userId" />
											</h3>
											<div id="scrollbars${status.index}" class="expand-list"
												style="display: block;">
												<div class="viewport">
													<div class="overview">

														<ul id="brands-list${status.index}">
															<c:if test="${fn:length(widgets)>0}">
																<c:forEach items="${widgets[status.index]}" var="widget">
																	<li><a href="" class="widget-focus widget-name"><c:out
																				value="${widget.title}" /></a> <input type="hidden"
																		class="identity"
																		value="<c:out
																				value='${widget.id}'/>" />
																		<input type="hidden" class="url"
																		value="<c:out
																				value='${widget.contentURL}'/>" />
																		<input type="hidden" class="table"
																		value="<c:out
																				value='${widget.table}'/>" />
																		<input type="hidden" class="trend"
																		value="<c:out
																				value='${widget.trend}'/>" /></li>

																</c:forEach>
															</c:if>
														</ul>

													</div>
												</div>
											</div>
										</div>
									</li>
								</c:if>
							</c:forEach>
						</ul>
					</div>
				</li>
				<c:if test="${not empty sharedDashboards}">
					<li class="dashboardsCont">
						<div class=" filter-all">
							<div class="main-filter-slider">
								<h3 class="new-exp">
									<span>Shared Dashboards</span>
								</h3>
							</div>
							<ul class="left-nav new-nav-tog" id="shared-dashboards">
								<c:forEach var="dashboard" items="${sharedDashboards}"
									varStatus="status">
									<li class="shared-dashboard">
										<div class="filter-slider">
											<h3>
												<span><c:out value="${dashboard.name}" /></span><a href="#"
													class="expand colapse"></a> <input type="hidden"
													value="${dashboard.id}" class="id" /> <input type="hidden"
													value="${dashboard.userId}" class="userId" />

											</h3>
											<div id="scrollbars${status.index}" class="expand-list"
												style="display: none;">
												<div class="viewport">
													<div class="overview">

														<ul id="brands-list${status.index}">
															<c:if test="${fn:length(widgets)>0}">
																<c:forEach
																	items="${sharedDashboardsWidgets[status.index]}"
																	var="widget">
																	<li><a href="" class="widget-focus widget-name"><c:out
																				value="${widget.title}" /></a> <input type="hidden"
																		class="identity"
																		value="<c:out
																				value='${widget.id}'/>">
																		<input type="hidden" class="url"
																		value="<c:out
																				value='${widget.contentURL}'/>" />
																		<input type="hidden" class="table"
																		value="<c:out
																				value='${widget.table}'/>" />
																		<input type="hidden" class="trend"
																		value="<c:out
																				value='${widget.trend}'/>" /></li>

																</c:forEach>
															</c:if>
														</ul>

													</div>
												</div>
											</div>
										</div>
									</li>
								</c:forEach>
							</ul>
						</div>
					</li>
				</c:if>
				<c:if test="${admin}">
					<li class="dashboardsCont">
						<div class="filter-all">
							<div class="main-filter-slider">
								<h3 class="new-exp">
									<span>Admin Dashboards</span>
								</h3>
							</div>
							<ul class="left-nav new-nav-tog" id="default-dashboards">
								<c:forEach var="dashboard" items="${defaultDashboards}"
									varStatus="status">
									<li class="default-dashboard">
										<div class="filter-slider">
											<h3>
												<span><c:out value="${dashboard.name}" /></span><a href="#"
													class="expand colapse"></a> <input type="hidden"
													value="${dashboard.id}" class="id" /> <input type="hidden"
													value="${dashboard.userId}" class="userId" />
											</h3>
											<div id="default-scrollbars${status.index}"
												class="expand-list" style="display: none;">

												<div class="viewport">
													<div class="overview">

														<ul id="default-brands-list${status.index}">
															<c:if test="${fn:length(defaultDashboardsWidgets)>0}">
																<c:forEach
																	items="${defaultDashboardsWidgets[status.index]}"
																	var="widget">
																	<li><a href="" class="widget-focus widget-name"><c:out
																				value="${widget.title}" /></a> <input type="hidden"
																		class="identity"
																		value="<c:out
																				value='${widget.id}'/>">
																		<input type="hidden" class="url"
																		value="<c:out
																				value='${widget.contentURL}'/>" />
																		<input type="hidden" class="table"
																		value="<c:out
																				value='${widget.table}'/>" />
																		<input type="hidden" class="trend"
																		value="<c:out
																				value='${widget.trend}'/>" /></li>

																</c:forEach>
															</c:if>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</li>
								</c:forEach>
							</ul>

						</div>
					</li>
				</c:if>

			</ul>
		</nav>
		<section class="right-side ">

			<div class="maia-notification" id="notification-video"
				style="display: none;">
				<div class="clearfix" style="height: 125px;">
					<div>
						<h2>Welcome to The Google+ Brand Tracker</h2>
						<p>Use this tool to uncover insights from our quarterly
							Google+ brand research. Quickly and easily find the data you need
							to understand users, compare performance between countries and
							learn about other social networks in market. For a brief
							overview, and to learn more about using the tool, please click
							"Watch Video".</p>
						<div class="show-info">
							<widgets:form>
								<fieldset>
									<label>Don’t show this message again <input
										id="show-video-checkbox" type="checkbox">
									</label>
								</fieldset>
							</widgets:form>
							<a href="#" id="close-video-notification"
								class="maia-button maia-button-secondary">Close</a> <a
								href="/site/dashboard/video" id="view-video-notification"
								class="maia-button maia-button-secondary">Watch video</a>
						</div>
					</div>
				</div>
			</div>
			<div class="maia-notification notice" hidden="">
				<div class="clearfix">
					<h2>Choose your default country</h2>
					<form>
						<fieldset>
							<select class="type-select">
								<option value="">Egypt</option>
								<option value="1">France</option>
								<option value="9">Italy</option>
								<option value="2">Canada</option>
							</select>
						</fieldset>
					</form>
					<div class="show-info">
						<a href="#" class="maia-button maia-button-secondary">Save</a> <a
							href="#" class="maia-button maia-button-secondary">Close</a>
					</div>
				</div>
			</div>
			<div class="setting-bar">
				<h1>
					<a href="#edit-dashboard-name" class="popuplink dashboard-title"></a>
				</h1>
				<div class="setting-icon">
					<div class="s-icon"></div>
					<div class="setting-arrow"></div>
				</div>
				<div class="setting-content">
					<ul id="dashboard-options">
						<!-- 						<li><a href="#add-new-widget" class="popuplink">Add new -->
						<!-- 								widget</a></li> -->
						<li id="set-as-default"><a
							href="/site/dashboard/setDefaultDashBoard/"
							class="setDefaultDashBoard">Set as default dashboard</a> <input
							id="_csrf" type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" /></li>

						<c:if test="${admin}">
							<li id="set-as-location-default"><a
								href="#set-default-dashboard"
								class="setLocationDefaultDashBoard popuplink">Set as
									location default dashboard</a></li>
						</c:if>
						<li id="set-order-widgets"><a href="#order-widgets"
							class="setWidgetsOrder popuplink">Order widgets</a></li>
						<li id="share-dashboard-link"><a href="#share-dashboard"
							class="shareDashBoard">Share dashboard</a><input id="_csrf"
							type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" /></li>
						<li>
							<!-- 						<a href="/site/dashboard/delete/" class="delete" --> <a
							href="/site/studyDashBoard/delete/" class="delete"
							id="delete-dashboard">Delete dashboard </a> <input id="_csrf"
							type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
						</li>
						<!-- 						<li class="border"><a href="#">Help</a></li> -->
					</ul>
				</div>
			</div>
			<div class="dashboard-blocks">
				<div class="clearfix">
					<c:if test="${not empty dashboards && fn:length(widgets)>0}">
						<c:forEach var="widget" items="${widgets[defaultIndex]}"
							varStatus="status">

							<div class="dash-block-wrap" id="widget-wrapper-${widget.id}">
								<h2>
									<span><c:out value="${widget.title}" /></span><a
										href="/site/dashboard/deletewidget/"
										class="remove-icon delete"
										title="Remove this widget permanently">remove</a> <a
										href="#update-widget"
										class="update-icon popuplink update-widget-btn"
										title="Check if data has been updated">update</a> <a
										href="#edit-widget"
										class="show-icon popuplink edit-widget-btn"
										title="Edit the widget's name">edit</a>

								</h2>
								<div class="widget-question"></div>
								<p class="small">
								<p class="widget-filters"></p>
								<p class="widget-wave">
									<!-- 										Dataset 1, 4th Jan - Dataset 1, 4th Jun. <span>Country: -->
									<%-- 											<c:forEach var="country" items="${widget.countries}" --%>
									<%-- 												varStatus="staus"> --%>
									<%-- 												<c:if test="${status.index+1 < fn:length(widget.countries)}"> --%>
									<%-- 										${country}, --%>
									<%-- 										</c:if> --%>
									<%-- 												<c:if --%>
									<%-- 													test="${status.index+1 == fn:length(widget.countries)}"> --%>
									<%-- 										${country}. --%>
									<%-- 										</c:if> --%>
									<%-- 											</c:forEach> --%>
									<!-- 										</span> -->
								</p>
								<div id="container-${status.index}"
									class="dash-block-cont clearfix <c:out value='${widget.trend}'/> <c:out	value='${widget.table}'/>">
									<input type="hidden" class="url"
										value="<c:out
																				value='${widget.contentURL}'/>" />
									<input type="hidden" class="questions"
										value="<c:out
																				value='${widget.questions}'/>" />
									<input type="hidden" class="countries"
										value="<c:out
																				value='${widget.countries}'/>" />
									<input type="hidden" class="brands"
										value="<c:out
																				value='${widget.brands}'/>" />
									<input type="hidden" class="filtersAndDivisions"
										value="<c:out
																				value='${widget.filtersAndDivisions}'/>" />
									<input type="hidden" class="waves"
										value="<c:out
																				value='${widget.waves}'/>" />

									<div class="dash-block "></div>

								</div>
							</div>
						</c:forEach>
					</c:if>
					<div id="instructions-div"
						style="display: <c:if test="${ not empty widgets && not empty widgets[defaultIndex]}">none</c:if> <c:if test="${empty widgets}">block</c:if>">



						<b>To add a widget to your dashboard from the "Snapshot" Tool:</b>
						<ul>
							<li>Choose the data wave and country</li>
							<li>Select the question(s) you'd like to display (you may
								compare up to 3 questions simultaneously)</li>
							<li>Choose the brands and demographic filters</li>
							<li>Once the desired data are displayed, click on the
								settings Cog (top right hand corner) and select 'Add to
								dashboard'</li>
							<li>Select the dashboard name where you'd like this data to
								be displayed</li>
							<li>Indicate whether you'd like to show the data as a graph
								or a table</li>
						</ul>


						<b>To add a widget to your dashboard from the "Trends" Tool:</b>
						<ul>
							<li>Choose the data waves and country you'd like to view</li>
							<li>Select the question(s) you'd like to display (you may
								compare up to 3 questions simultaneously)</li>
							<li>Choose the brands and demographic filters</li>
							<li>Once the desired data are displayed, click on the
								settings Cog (top right hand corner) and select 'Add to
								dashboard'</li>
							<li>Select the dashboard name where you'd like this data to
								be displayed</li>
							<li>Indicate whether you'd like to show the data as a graph
								or a table</li>

						</ul>

						<b>Notes:</b>
						<ul>
							<li>You can only use one set of demographic filters at a
								time</li>
							<li>You can only compare up to 6 brands at once</li>
							<li>Available brands differs by country</li>
						</ul>



					</div>

				</div>
			</div>
		</section>
		<div id="add-new-dashboard" class="popup"
			style="display: none; width: 350px; height: 230px;">
			<h3>Add New Dashboard</h3>
			<widgets:form classes="form validate" id="add-dashboard-form"
				action="/site/studyDashBoard/addnew" method="post">
				<!-- 								action="/site/dashboard/addnew" method="post"> -->
				<fieldset>
					<label>Dashboard Title</label> <input name="name"
						class="name required" type="text"> <input type="hidden"
						value="${study.studyID}" name="studyId" />

				</fieldset>
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
				<button type="submit" id="save-dashboard"
					class="maia-button save-popup-form">Save</button>
				<a href="javascript:parent.$.fancybox.close();" class="maia-button ">Close</a>
			</widgets:form>

		</div>

		<div id="edit-dashboard-name" class="popup"
			style="display: none; width: 350px; height: 230px;">
			<h3>Edit Dashboard</h3>
			<widgets:form classes="form validate" id="edit-form"
				action="/site/dashboard/edit" method="post">
				<fieldset>
					<label>New Dashboard Title</label> <input name="name"
						class="name required" type="text"> <input type="hidden"
						name="dashboard-hidden-id" id="hidden-id" /> <input type="hidden"
						name="hidden-user-id" class="hidden-user-id" />
				</fieldset>
				<button type="submit" id="save-dashboard"
					class="maia-button save-popup-form">Save</button>
				<a href="javascript:parent.$.fancybox.close();" class="maia-button ">Close</a>
			</widgets:form>
		</div>

		<div id="edit-widget" class="popup"
			style="display: none; width: 350px; height: 230px;">
			<h3>Edit Widget</h3>
			<widgets:form classes="form" id="edit-widget-form"
				action="/site/dashboard/widgetedit" method="post">
				<fieldset>
					<label>New Widget Title</label> <input name="name" class="name"
						type="text"> <input type="hidden"
						name="dashboard-hidden-id" class="hidden-id" /> <input
						type="hidden" name="widget-hidden-id" class="hidden-widget-id" />
					<input type="hidden" name="hidden-user-id" class="hidden-user-id" />
				</fieldset>
				<button type="submit" id="save-dashboard"
					class="maia-button save-popup-form">Save</button>
				<a href="javascript:parent.$.fancybox.close();" class="maia-button ">Close</a>
			</widgets:form>
		</div>
		<div id="update-widget" class="popup"
			style="display: none; width: 450px; height: 330px;">
			<h3>Update Widget</h3>
			<div style="margin: 20px;">
				<span id="update-header">The data of this question has been
					updated since you've last modified this widget.</span>
			</div>
			<widgets:form classes="form" id="update-widget-form"
				action="/site/dashboard/updatewidget" method="post">
				<fieldset>
					<table id="update-options-list">
					</table>
					<input type="hidden" name="dashboard-hidden-id" class="hidden-id" />
					<input type="hidden" name="widget-hidden-id"
						class="hidden-widget-id" /> <input type="hidden"
						name="hidden-user-id" class="hidden-user-id" />
				</fieldset>
				<button type="submit" id="update-form-btn"
					class="maia-button save-popup-form">Update</button>
				<a href="javascript:parent.$.fancybox.close();" class="maia-button ">Cancel</a>
			</widgets:form>
		</div>
		<div id="set-default-dashboard" class="popup"
			style="display: none; width: 350px; height: 230px;">
			<h3>Set as location default dashboard</h3>
			<widgets:form classes="form" id="set-default-dashboard-form"
				action="/cms/data/dashboard/setaslocationdefault" method="post">
				<fieldset>
					<label>Location:</label> <select name="location"
						id="locations-select" class="locations">
					</select> <input type="hidden" name="dashboard-hidden-id" class="hidden-id" />
				</fieldset>
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
				<button type="submit" id="save-default-btn"
					class="maia-button save-popup-form">Save</button>
				<a href="javascript:parent.$.fancybox.close();" class="maia-button ">Close</a>
			</widgets:form>
		</div>
		<div id="order-widgets" class="popup"
			style="display: none; width: 450px; height: 330px;">
			<h3>Order Widgets</h3>

			<widgets:form classes="order-widget-form" id="order-widget-form"
				action="" method="post">
				<fieldset>
					<p class='maia-meta'>Drag and drop elements to reorder the
						widgets.</p>
					<div style="margin: 20px;">
						<div id="widgets-scrollbar" style="height: 150px;">
							<!-- 							<div class="scrollbar"> -->
							<!-- 								<div class="track"> -->
							<!-- 									<div class="thumb"> -->
							<!-- 										<div class="end"></div> -->
							<!-- 									</div> -->
							<!-- 								</div> -->
							<!-- 							</div> -->
							<!-- 							<div class="viewport"> -->
							<!-- 								<div class="overview"> -->
							<ul id="widgets-sortable"></ul>
							<!-- 								</div> -->
							<!-- 							</div> -->
						</div>
					</div>
					<table id="update-options-list">
					</table>

					<input type="hidden" name="hidden-user-id" class="hidden-user-id" />

				</fieldset>
				<button type="submit" class="maia-button">Update</button>
				<a href="javascript:parent.$.fancybox.close();" class="maia-button ">Cancel</a>

			</widgets:form>
		</div>
		<div id="share-dashboard" class="popup"
			style="display: none; width: 450px; height: 330px;">
			<h3>Share Dashboard</h3>
			<div style="margin: 20px;">
				<span id="update-header">Who has access:</span>

				<div id="users-scrollbar">
					<div class="scrollbar disable">
						<div class="track">
							<div class="thumb">
								<div class="end"></div>
							</div>
						</div>
					</div>
					<div class="viewport">
						<div class="overview">
							<ul id="users-with-access">
							</ul>
						</div>
					</div>
				</div>
			</div>

			<widgets:form classes="form" id="share-dashboard-form"
				action="/site/dashboard/sharedashboard" method="post">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
				<fieldset>
					<table id="update-options-list">
					</table>
					<input type="hidden" name="dashboard-hidden-id" class="hidden-id" />
					<label>E-mails:</label> <input type="text" name="emails" />
					<p class='maia-meta'>Multiple e-mails can be added using a ","
						separator.
					<p class='maia-meta'>Dashboards can only be shared with e-mails
						for existing users only.</p>
				</fieldset>
				<button type="submit" id="save-dashboard"
					class="maia-button save-popup-form">Update</button>
				<a href="javascript:parent.$.fancybox.close();" class="maia-button ">Cancel</a>

			</widgets:form>
		</div>
	</section>
	<!-- 	<button type="submit" id="test" class="maia-button">INIT</button> -->
</templates:site>
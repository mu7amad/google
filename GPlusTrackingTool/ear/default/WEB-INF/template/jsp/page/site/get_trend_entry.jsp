<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ taglib tagdir="/WEB-INF/tags/site" prefix="templates"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<templates:site activeTopNavItemId="trendsItem"
	title="Google+ Brand Tracker - Trends" logo="${logo }"
	studyId="${studyId}">
	<section class="right-side full-width-block">

		<div class="setting-bar">
			<div class="intro-dash">
				<h2>Choose a question to start your navigation</h2>


			</div>

			<div class="setting-icon">
				<div class="s-icon"></div>
				<div class="setting-arrow"></div>
			</div>
			<div class="setting-content">
				<ul>
					<li><a href="#add-new-dashboard" class="popuplink">Add to
							dashboard</a></li>
					<li><a href="#">Export</a></li>
					<li class="border"><a href="#">Help</a></li>
				</ul>
			</div>
		</div>
		<img src="/site_ui/media/img-04.jpg" alt="" class="imgcls1">

		<ul class="snap-q">
			<c:forEach items="${questions }" var="question">
				<li><a href="/site/trend?q=${question.id }"><c:out
							value="${question.name}" /></a> <c:if
						test="${not empty question.description}">
						<p>Description: ${question.description}</p>
					</c:if> <c:if test="${not empty question.buzzWord}">
						<p>Buzz Word: ${question.buzzWord}</p>
					</c:if></li>

			</c:forEach>

		</ul>
</templates:site>
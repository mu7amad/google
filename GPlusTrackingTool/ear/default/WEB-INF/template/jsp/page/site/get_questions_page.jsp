<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ taglib tagdir="/WEB-INF/tags/site" prefix="templates"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="widgets"%>
<c:set var="footer">
	<script>
		$(document)
				.ready(
						function() {

							var dummy = new Array();

							var dataArray = new Array(), questionObject;
							var headers = new Array();

							<c:forEach var="array" items="${dataArray}">
							var array = new Array();
							<c:forEach var="question" items="${array}">
							questionObject = {
								name : ('<c:out value='${question.name}'></c:out>'),
								buzzWord : ('<c:out value='${question.buzzWord}'></c:out>'),
								id : '${question.id}'
							};
							array.push(questionObject);
							</c:forEach>
							dataArray.push(array);
							</c:forEach>

							var counter = 0;
							<c:forEach var="category" items="${categories}">
							headers
									.push('<c:out value='${category.name}'></c:out>');
							counter++;
							</c:forEach>

							var url = ""
							<c:if test="${isTrend}">url = "/trend?";
							</c:if>
							if (url == "") {
								url = "/snapshot?";
							}

							var maxWidth = new Array();
							maxWidth.push(830);
							maxWidth.push(677);
							maxWidth.push(522);
							maxWidth.push(370);
							maxWidth.push(220);

							var images = new Array();
							images.push("blue-strip");
							images.push("red-strip");
							images.push("yellow-strip");
							images.push("second-blue-strip");
							images.push("green-strip");

							var arrows = new Array();
							arrows.push("blue");
							arrows.push("red");
							arrows.push("yellow");
							arrows.push("blue");
							arrows.push("green");

							var colors = new Array();
							colors.push("#007aff");
							colors.push("#e10728");
							colors.push("#ebc91e");
							colors.push("#007aff");
							colors.push("#21b129");

							var minBlockWidth = new Array();
							minBlockWidth.push(207);
							minBlockWidth.push(225);
							minBlockWidth.push(261);
							minBlockWidth.push(185);
							minBlockWidth.push(110);

							$("#container").append(
									container(dataArray, maxWidth,
											minBlockWidth, images, headers,
											url, colors, arrows));

							$("#container").css("height",
									$("#container").outerHeight());
							$(".questions-container").hide();
							$(".last-strip").hide();
							$('.questions-container')
									.each(
											function(index) {
												$(this)
														.delay(index * 150)
														.fadeIn(
																1000,
																function() {
																	//	$(this).find('.question').slideDown(500);
																	$(this)
																			.find(
																					'.question, .left-arrow, .right-arrow')
																			.show();

																	$(this)
																			.find(
																					'.question, .left-arrow, .right-arrow')
																			.animate(
																					{
																						top : '+=50px'
																					},
																					1000,
																					'easeOutElastic');

																});

												$('.last-strip')
														.delay(
																250 * (dataArray.length - 1))
														.fadeIn(1000);

											});
							$(".questions-area").find(".question:last-child")
									.css("border-right", "0");

							// 		$(".right-arrow").hover(
							// 				  function () {
							// 					var questionPopup =  $(this).prev('.question').find('.question-popup'); 

							// 					questionPopup.parents('.question').find('a').css('color' , '#fff');
							// 					   	var color =  questionPopup.parents('li').find('.color').attr("id");
							// 					   	questionPopup.parent().find('.question-popup').css({	'width':  (questionPopup.parents('.question').width() + 30)+'px',
							// 				    'height': (questionPopup.parents('.question').height() + 55)+'px',
							// 				    'margin-left':  '-5px',
							// 				    'background-color':  color,
							// 				    'z-index': '15',
							// 				    'cursor': 'pointer'
							// 				    });
							// 					  $(this).next('.r-hover').show();
							// 				  },
							//  				 function () {
							// 				  });

							// 		$(".r-hover").hover(
							// 				  function () {

							// 				  },
							// 				 function () {
							// 					  var questionPopup =  $(this).prev('.question').find('.question-popup');

							// 						questionPopup.parents('.question').find('a').css('color' , '#444');
							// 						questionPopup.parent().find('.question-popup').css({	'width': '100%',
							// 							    'height': '100%',
							// 							    'margin-left':  '0',
							// 							    'background-color':  '#f5f5f5',
							// 							    'z-index': '10'});

							// 						$(this).next('.r-left').hide();
							// 				  });	

							$(".question a, .question-popup")
									.hover(
											function() {
												$(this).parents('.question')
														.find('a').css('color',
																'#fff');
												var color = $(this).parents(
														'li').find('.color')
														.attr("id");
												var height = 87;
												if ($(this).parents(
														'.category-extend').length == 0) {
													height += 55;
												}
												$(this)
														.parent()
														.find('.question-popup')
														.css(
																{
																	'width' : ($(
																			this)
																			.parents(
																					'.question')
																			.width() + 30)
																			+ 'px',
																	'height' : height
																			+ 'px',
																	'margin-left' : '-5px',
																	'background-color' : color,
																	'z-index' : '15',
																	'cursor' : 'pointer'
																});

												if ($(this).parents(
														".questions-area")
														.find('.question').length == 1) {
													$(this)
															.parents(
																	'.questions-container')
															.find('.r-hover')
															.show();
													$(this)
															.parents(
																	'.questions-container')
															.find('.l-hover')
															.show();
												} else if ($(this).parents(
														'.question').next(
														'.question').length == 0) {
													$(this)
															.parents(
																	'.questions-container')
															.find('.r-hover')
															.show();
												} else if ($(this).parents(
														'.question').prev(
														'.question').length == 0) {
													$(this)
															.parents(
																	'.questions-container')
															.find('.l-hover')
															.show();
												}

											},
											function() {
												$(this).parents('.question')
														.find('a').css('color',
																'#444');
												$(this)
														.parent()
														.find('.question-popup')
														.css(
																{
																	'width' : '100%',
																	'height' : '82px',
																	'margin-left' : '0',
																	'background-color' : '#f5f5f5',
																	'z-index' : '10'
																});

												if ($(this).parents(
														".questions-area")
														.find('.question').length == 1) {
													$(this)
															.parents(
																	'.questions-container')
															.find('.r-hover')
															.hide();
													$(this)
															.parents(
																	'.questions-container')
															.find('.l-hover')
															.hide();
												} else if ($(this).parents(
														'.question').next(
														'.question').length == 0) {
													$(this)
															.parents(
																	'.questions-container')
															.find('.r-hover')
															.hide();
												} else if ($(this).parents(
														'.question').prev(
														'.question').length == 0) {
													$(this)
															.parents(
																	'.questions-container')
															.find('.l-hover')
															.hide();
												}

											}

									);
							$(".question").live("click", function() {
								location.href = $(this).find('a').attr('href');
							});

						});

		var block = function(width, question, url) {
			var blockString = "";
			var name = "";
			width -= 20;
			if (question.buzzWord != null && question.buzzWord != "") {
				name = question.buzzWord;
			} else {
				name = question.name;
			}
			blockString += "<div class='question' style='display: none; width :"
				+ width
				+ "px;'><div class='question-popup'></div><a href='/site"
					+ url.replace('?', '')
					+ "/${studyId}"
					+ "?q="
					+ question.id + "'>" + name + "</a></div>";
			return blockString;
		}

		var blockContainer = function(array, width, extra, imageClass,
				headerName, url, colorId, arrow) {
			var blockString = "";
			if (!extra) {
				var headerWidth = width + 73;
				blockString += "<div class='category category-1 questions-container' style='width:"
					+ width + "px'>"
				blockString += "<div class='" + imageClass + " color' id='"+colorId+"'>"
						+ headerName + "</div>";
				blockString += "<div class='left-arrow' style='display: none;'></div><div class='l-hover left-arrow-"+arrow+"' style='display: none;'></div>";
			} else {
				blockString += "<div class='category-extend category-extend-1 questions-container' style='width:"
					+ width + "px'>";
			}

			blockString += "<div class='questions-area' style=' width :"
				+ width + "px;'>";

			var borders = (array.length * 2) - 1;
			var blockWidth = (width - borders) / (array.length);

			var carry = 0;
			blockWidth = Math.floor(blockWidth);
			if ((blockWidth * array.length) < (width - borders)) {
				carry = (width - borders) - (blockWidth * array.length);
			}

			for (var i = 0; i < array.length; i++) {

				if (i == array.length - 1) {
					blockString += block(blockWidth + carry, array[i], url);
				} else {
					blockString += block(blockWidth, array[i], url);
				}
			}
			if (!extra) {
				blockString += "<div class='right-arrow' style='display: none;'></div><div class='r-hover right-arrow-"+arrow+"' style='display: none;'></div>";
			}
			blockString += "</div>";
			blockString += "</div>";
			return blockString;
		}

		var container = function(dataArray, maxWidth, minBlockWidth, images,
				headers, url, colors, arrows) {
			var blockString = "";
			blockString += "<div style=' margin: 0 auto; width :1060px;'>";
			blockString += "<ul class='unstyled'>";

			for (var i = 0; i < dataArray.length; i++) {
				blockString += "<li>";
				var blocksArray = new Array();
				if ((dataArray[i].length * minBlockWidth[i]) > maxWidth[i]) {
					var length = dataArray[i].length * minBlockWidth[i];
					var noOfSegements = 1;
					// alert("length: " + length + ", maxWidth[i]: "
					// + maxWidth[i]);
					while (length > maxWidth[i]) {
						noOfSegements++;
						length = (dataArray[i].length * minBlockWidth[i])
								/ noOfSegements;
					}
					var segementSize = Math
							.floor((dataArray[i].length / noOfSegements));
					var carry = 0;

					if ((segementSize * noOfSegements) != dataArray[i].length) {
						carry = dataArray[i].length
								- (segementSize * noOfSegements);
					}
					// alert(carry)
					var index = 0;
					for (var j = 0; j < noOfSegements; j++) {
						var currentSegementSize = segementSize;
						if (carry > 0) {
							currentSegementSize = segementSize + 1;
							carry--;
						}
						var segement = new Array();

						for (var k = 0; k < currentSegementSize; k++) {
							segement.push(dataArray[i][index]);
							index++;
						}
						blocksArray.push(segement);
					}
				}

				else {
					blocksArray.push(dataArray[i]);
				}

				for (var j = 0; j < blocksArray.length; j++) {
					if (j == 0) {
						blockString += blockContainer(blocksArray[j],
								maxWidth[i], false, images[i], headers[i], url,
								colors[i], arrows[i]);
					} else {
						blockString += blockContainer(blocksArray[j],
								maxWidth[i], true, images[i], "", url,
								colors[i], arrows[i]);
					}
				}
				blockString += "</li>";
			}
			if (dataArray.length == 5) {
				blockString += "<li>";
				blockString += "<div class='last-strip'></div>";
			}
			blockString += "</li>";
			blockString += "</ul>";
			blockString += "</div>";
			return blockString;
		}
	</script>
</c:set>
<c:set var="description" value="${description }">
</c:set>
<c:set var="keywords" value="${keywords}">
</c:set>

<c:set var="activeItem">
	<c:if test="${isTrend}">trendsItem</c:if>
	<c:if test="${!isTrend}">snapshotItem</c:if>
</c:set>
<templates:site footer="${footer}" title="All Questions"
	activeTopNavItemId="${activeItem}" logo="${logo }" studyId="${studyId}">

	<div class="right-side full-width-block">
		<div class="maia-notification" id="notification-video"
			style="display: none;">
			<div class="clearfix" style="height: 125px;">
				<div>
					<h2>Welcome to The Google+ Brand Tracker</h2>
					<p>Use this tool to uncover insights from our quarterly Google+
						brand research. Quickly and easily find the data you need to
						understand users, compare performance between countries and learn
						about other social networks in market. For a brief overview, and
						to learn more about using the tool, please click "Watch Video".</p>
					<div class="show-info">
						<widgets:form>
							<fieldset>
								<label>Don’t show this message again <input
									id="show-video-checkbox" type="checkbox">
								</label>
							</fieldset>
						</widgets:form>
						<a href="#" id="close-video-notification"
							class="maia-button maia-button-secondary">Close</a> <a
							href="/site/dashboard/video" id="view-video-notification"
							class="maia-button maia-button-secondary">Watch video</a>
					</div>
				</div>
			</div>
		</div>
		<p class="maia-meta">Please choose the question you'd like to
			display</p>
		<div id="container" style="margin: 0 auto;"></div>
		<div style="clear: both"></div>
	</div>
</templates:site>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ taglib tagdir="/WEB-INF/tags/site" prefix="templates"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="footer">
</c:set>
<c:set var="description" value="${description }">
</c:set>
<c:set var="keywords" value="${keywords}">
</c:set>
<templates:site footer="${footer}" title="${title}" logo="${logo }"
	studyId="${studyId}">
	<div class="right-side full-width-block">
		<div style="margin: 0px auto; width: 960px">
			<div>
				<h1>${title}</h1>
			</div>
			<br>
			<div>${content}</div>
		</div>
	</div>
</templates:site>
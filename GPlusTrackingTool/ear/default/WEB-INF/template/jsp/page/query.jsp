<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- <script type="text/javascript" -->
<!-- 	src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> -->
<script type="text/javascript" src="/jquery.min.js"></script>
<title>Test Query</title>


<script type="text/javascript">
	$(function() {
		$('#answers').html($('#answers${questions[0].id}').html());
		$('#questions').change(function() {

			$('#answers').html($('#answers' + this.value).html());

		});
		$('form').submit(function() {

			$.ajax({
				url : '/site/allquery',
				data : $(this).serialize(),
				async : false,
				success : function(result) {
					alert("Error: " + result.error + ", Success: " + result.success);

				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert('error');
				},
				dataType : 'json',
				type : 'POST'
			});
			return false;
		});

	});
</script>
<style>
#progress_bar {
	margin: 10px 0;
	padding: 3px;
	border: 1px solid #000;
	font-size: 14px;
	clear: both;
	opacity: 0;
	-moz-transition: opacity 1s linear;
	-o-transition: opacity 1s linear;
	-webkit-transition: opacity 1s linear;
}

#progress_bar.loading {
	opacity: 1.0;
}

#progress_bar .percent {
	background-color: #99ccff;
	height: auto;
	width: 0;
}
</style>
</head>
<body>
	<form>

		<label>Locations:</label> <select name="loc" id="locations">
			<c:forEach items="${locations}" var="location">

				<option value="${location.id}">${location.name }</option>
			</c:forEach>
		</select> <br /> <label>Question:</label> <select name="ques" id="questions">
			<c:forEach items="${questions}" var="question">

				<option value="${question.id}">${question.name }</option>
			</c:forEach>
		</select> <br /> <label>Answers:</label> <select name="ans" id="answers"></select>
		<c:forEach items="${questions}" var="question" varStatus="stat">
			<select id="answers${question.id }" hidden="hidden">
				<c:set var="questionId" value="${question.id}"></c:set>
				<c:forEach items="${answers[questionId]}" var="answer">
					<option value="${answer.id}">${answer.name }</option>
				</c:forEach>
			</select>
		</c:forEach>
		<br /> <label>Filters:</label>
		<c:forEach items="${filters}" var="filter">
			<div>
				<input name="fil" hidden="hidden" value="${filter.id}" />${filter.name
				}
			</div>

			<%-- --%>
			<select name="div" id="divisions">
				<c:forEach items="${divisions[filter.id]}" var="division">
					<option value="${division.id}">${division.name }</option>
				</c:forEach>
			</select>
		</c:forEach>
		<br /> <label>Data Sets:</label> <select name="ds" id="dataSets">
			<c:forEach items="${dataSets}" var="dataSet">

				<option value="${dataSet.id}">${dataSet.name }</option>
			</c:forEach>

		</select> <label>Brands:</label> <select name="br" id="brands">
			<c:forEach items="${brands}" var="brand">

				<option value="${brand.id}">${brand.name }</option>
			</c:forEach>
		</select> <input type="submit" />
	</form>
</body>
</html>
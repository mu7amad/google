<%@ taglib tagdir="/WEB-INF/tags/cms" prefix="templates"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="widgets"%>
<c:set var="title">
</c:set>
<templates:cms title="${title }" activeTopNavItemId="dataItem"
	studyId="${studyId }">
	<templates:data topNavFirstItemLink="/cms/data/dataset/create"
		topNavSecondItem="Data Waves List" topNavSelectedItemIndex="0"
		sideNavActiveItemId="dataWavesItem" topNavFirstItem="Edit Data Wave"
		title="Data Waves - Edit Data Wave"
		topNavSecondItemLink="/cms/data/dataset">

		<article class="maia-article" role="article">
			<widgets:form action="/cms/data/dataset/edit/${resource.id}"
				method="post">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
				<fieldset>
					<ul>
						<li><label>Wave Name</label> <input type="text" name="name"
							value="<c:out value="${resource.name}"></c:out>"></li>
						<li><label>Wave Description</label> <textarea
								name="description">${resource.description.value}</textarea>
							<p class="label-info">Maximum 600 characters</p></li>
						<li><label>Wave Date</label> <input type="text"
							name="waveDateString"
							value="<fmt:formatDate value="${resource.waveDate}" pattern="dd/MM/yyyy"/>">
						</li>
						<li><label>Wave Creation Date</label> <label><fmt:formatDate
									value="${resource.creationDate}" pattern="dd/MM/yyyy" /></label></li>
						<li>
							<p>Wave Files</p>
							<table>
								<tr>
									<th scope="col" class="tiny-col">Date<a href="#"
										class="sorting-arrow">arrow</a></th>
									<th scope="col">Name</th>
									<th scope="col">Created by</th>
									<th scope="col">RollBack</th>

									<%--<th scope="col" class="tiny-raw">View</th> --%>
								</tr>
								<c:forEach items="${dataSheetResource}" var="datasheet">
									<tr>
										<td>${datasheet.uploadDate}</td>
										<td><c:out value="${datasheet.name}"></c:out></td>
										<td>${datasheet.uploadedBy}</td>
										<td><input id="_csrf" type="hidden"
											name="${_csrf.parameterName}" value="${_csrf.token}" /> <input
											type="hidden" id="datasheet-id" value="${datasheet.id }">
											<button id="rollback-datasheet-edit-dataset">
												Rollback</button></td>
									</tr>
								</c:forEach>
							</table>
						</li>
						<li><label>Show in snapshot only</label> <input
							name="showenInSnapshotOnly" type="checkbox"
							<c:if test="${resource.showenInSnapshotOnly}">checked="true"
												</c:if>></li>

					</ul>
				</fieldset>
				<button class="maia-button" type="submit">Save</button>
				<!-- 				<button class="maia-button" type="submit">Publish</button> -->
				<a href="/cms/data/dataset"
					class="maia-button maia-button-secondary" type="reset">Cancel</a>
			</widgets:form>
		</article>
	</templates:data>
</templates:cms>
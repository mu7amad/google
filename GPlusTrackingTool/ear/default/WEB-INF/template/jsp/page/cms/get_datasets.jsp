<%@ taglib tagdir="/WEB-INF/tags/cms" prefix="templates"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="widgets"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:set var="title">
Google+ Brand Tracker - Data Waves List
</c:set>
<templates:cms title="${title }" activeTopNavItemId="dataItem"
	studyId="${studyId }">
	<templates:data topNavFirstItemLink="/cms/data/dataset/create"
		topNavSecondItem="Data Waves List" topNavSelectedItemIndex="1"
		sideNavActiveItemId="dataWavesItem"
		topNavFirstItem="Create New Data Wave"
		title="Data Waves - Data Waves List"
		topNavSecondItemLink="/cms/data/dataset" showTopNav="false">
		<div class="sort">
			<div class="sort-numbers">
				<b>${from + 1}</b> - <b>${to}</b> of <b>${total}</b>
			</div>
			<a class="sort-arrows arrow-left"
				href="<c:if test="${from > 0}">?offset=${from-count}&count=${count}&orderBy=${orderBy}&orderDir=${orderDir}</c:if><c:if test="${from <= 0}"></c:if>"
				id="questions-table-previous"><span></span></a> <a
				class="sort-arrows arrow-right"
				href="<c:if test="${to < total}">?offset=${to}&count=${count}&orderBy=${orderBy}&orderDir=${orderDir}</c:if><c:if test="${to == total}"></c:if>"
				id="questions-table-next"><span></span></a>
		</div>
		<table>
			<tr>
				<th scope="col">Date<a
					href="?orderBy=creationDate&orderDir=<c:if test="${orderDir!='asc' || orderBy!='creationDate'}">asc</c:if><c:if test="${orderDir=='asc' && orderBy=='creationDate'}">desc</c:if>&offset=${from}&count=${count}"
					class="sorting-arrow creationDate">arrow</a></th>
				<th scope="col">Name<a
					href="?orderBy=name&orderDir=<c:if test="${orderDir!='asc' || orderBy!='name' }">asc</c:if><c:if test="${orderDir=='asc' && orderBy=='name'}">desc</c:if>&offset=${from}&count=${count}"
					class="sorting-arrow name">arrow</a></th>
				<th scope="col">Wave time <a
					href="?orderBy=waveDate&orderDir=<c:if test="${orderDir != 'asc' || orderBy != 'waveDate'}">asc</c:if><c:if test="${orderDir == 'asc' && orderBy == 'waveDate'}">desc</c:if>&offset=${from}&count=${count}"
					class="sorting-arrow waveDate">arrow</a></th>
				<th scope="col">Files</th>
				<th scope="col" class="tiny-raw">Publish</th>
				<th scope="col">Edit</th>
				<th scope="col">Delete</th>
			</tr>
			<c:forEach items="${resources}" var="resource">
				<tr>
					<td><fmt:formatDate value="${resource.creationDate}"
							pattern="dd/MM/yyyy" /></td>
					<td><c:out value="${resource.name}"></c:out></td>
					<td><fmt:formatDate value="${resource.waveDate}"
							pattern="dd/MM/yyyy" /></td>
					<td>${resource.numberOfFiles}</td>
<%-- 					<c:if test="${resource.owner }"> --%>
						<td><widgets:form
								action="/cms/data/dataset/publish/${resource.id}" method="Post">
								<input type="hidden" name="${_csrf.parameterName}"
									value="${_csrf.token}" />
								<button type="submit" class="maia-button red publish-unpublish"
									>Publish</button>
							</widgets:form></td>

<%-- 					</c:if> --%>
<%-- 					<c:if test="${!resource.owner }"> --%>
<%-- 						<c:if test="${!resource.published}"> --%>
<%-- 							<td><widgets:form --%>
<%-- 									action="/cms/data/dataset/publish/${resource.id}" method="Post"> --%>
<%-- 									<input type="hidden" name="${_csrf.parameterName}" --%>
<%-- 										value="${_csrf.token}" /> --%>
<!-- 									<button type="submit" class="maia-button red publish-unpublish">Publish</button> -->
<%-- 								</widgets:form></td> --%>
<%-- 						</c:if> --%>
						<c:if test="${resource.published}">
							<c:if test="${resource.owner }">
								<td><widgets:form
										action="/cms/data/dataset/unpublish/${resource.id}"
										method="Post">
										<input type="hidden" name="${_csrf.parameterName}"
											value="${_csrf.token}" />
										<button type="submit"
											class="maia-button red publish-unpublish" disabled>Unpublish</button>
									</widgets:form></td>

							</c:if>
							<td><widgets:form
									action="/cms/data/dataset/unpublish/${resource.id}"
									method="Post">
									<input type="hidden" name="${_csrf.parameterName}"
										value="${_csrf.token}" />
									<button type="submit" class="maia-button red publish-unpublish">Unpublish</button>
								</widgets:form></td>
						</c:if>
<%-- 					</c:if> --%>

					<td><a href="/cms/data/dataset/edit/${resource.id}"
						class="show-icon">Edit</a></td>
					<td><widgets:form
							action="/cms/process/entry/rollbackdataset/${resource.id}">
							<button type="submit"
								class="unstyled-button remove-icon rollback-dataset">Delete</button>
						</widgets:form></td>
				</tr>
			</c:forEach>
		</table>
		<div class="sort">
			<div class="show-items">
				Show items per page
				<form>
					<select class="type-select entries-per-page">
						<option value="20">20</option>
						<option value="15">15</option>
						<option value="10">10</option>
						<option value="5">5</option>
					</select>
				</form>
			</div>

			<div class="sort-numbers">
				<b>${from + 1}</b> - <b>${to}</b> of <b>${total}</b>
			</div>
			<a class="sort-arrows arrow-left"
				href="<c:if test="${from > 0}">?offset=${from-count}&count=${count}&orderBy=${orderBy}&orderDir=${orderDir}</c:if><c:if test="${from <= 0}"></c:if>"
				id="questions-table-previous"><span></span></a> <a
				class="sort-arrows arrow-right"
				href="<c:if test="${to < total}">?offset=${to}&count=${count}&orderBy=${orderBy}&orderDir=${orderDir}</c:if><c:if test="${to == total}"></c:if>"
				id="questions-table-next"><span></span></a> <input type="hidden"
				id="input-from" value="${from}"></input> <input type="hidden"
				id="input-to" value="${to}"></input> <input type="hidden"
				id="input-total" value="${total}"></input> <input type="hidden"
				id="input-orderDir" value="${orderDir}"></input> <input
				type="hidden" id="input-orderBy" value="${orderBy}"></input> <input
				type="hidden" id="input-count" value="${count}"></input>
		</div>
		<%-- 		<a href="<c:if test="${from >= 2}">?offset=${from-2}&count=2&orderBy=${orderBy}&orderDir=${orderDir}</c:if><c:if test="${from < 2}">#</c:if>" id="questions-table-previous">Previous</a> --%>
		<%-- 				<a href="<c:if test="${to < total}">?offset=${from-2}&count=2&orderBy=${orderBy}&orderDir=${orderDir}</c:if><c:if test="${to < total}">#</c:if>" id="questions-table-next">Next</a> --%>
		<%-- 				<p><c:if test="${from == 0 && to != 0}">1</c:if><c:if test="${from > 0 || to == 0}">${from}</c:if>-${to} of ${total}</p> --%>
	</templates:data>
</templates:cms>



<%@ taglib tagdir="/WEB-INF/tags/cms" prefix="templates"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:set var="title">
Google+ Brand Tracker - Default Dashboards
</c:set>
<templates:cms title="${title }" activeTopNavItemId="dataItem"
	studyId="${studyId }">
	<templates:data topNavFirstItemLink="/cms/data/dataset/create"
		topNavSecondItem="Default Dashboards List" topNavSelectedItemIndex="1"
		sideNavActiveItemId="defaultDashboardsItem" topNavFirstItem=""
		title="Default Dashboards List"
		topNavSecondItemLink="/cms/data/dataset" showTopNav="false">
		<div class="sort">
			<div class="sort-numbers">
				<b>${from + 1}</b> - <b>${to}</b> of <b>${total}</b>
			</div>
			<a class="sort-arrows arrow-left"
				href="<c:if test="${from > 0}">?offset=${from-count}&count=${count}&orderBy=${orderBy}&orderDir=${orderDir}</c:if><c:if test="${from <= 0}"></c:if>"
				id="questions-table-previous"><span></span></a> <a
				class="sort-arrows arrow-right"
				href="<c:if test="${to < total}">?offset=${to}&count=${count}&orderBy=${orderBy}&orderDir=${orderDir}</c:if><c:if test="${to == total}"></c:if>"
				id="questions-table-next"><span></span></a>
		</div>
		<table>
			<tr>

				<!-- 				<th scope="col">Name<a -->
				<%-- 					href="?orderby=name&orderdir=<c:if test="${orderDir!='asc'}">asc</c:if><c:if test="${orderDir=='asc'}">desc</c:if>&offset=${from}&count=2" --%>
				<!-- 					class="sorting-arrow">arrow</a></th> -->
				<th scope="col">Location<%-- <a
					href="?orderBy=locationObject&orderDir=<c:if test="${orderDir!='asc' || orderBy!='locationObject'}">asc</c:if><c:if test="${orderDir=='asc'&& orderBy=='locationObject'}">desc</c:if>&offset=${from}&count=${count}"
					class="sorting-arrow">arrow</a> --%></th>
				<th scope="col">Dashboard<%-- <a
					href="?orderBy=dashboardObject&orderDir=<c:if test="${orderDir!='asc' || orderBy!='dashboardObject'}">asc</c:if><c:if test="${orderDir=='asc'&& orderBy=='dashboardObject'}">desc</c:if>&offset=${from}&count=${count}"
					class="sorting-arrow">arrow</a> --%></th>
				<th scope="col">Date<a
					href="?orderBy=lastModificationDate&orderDir=<c:if test="${orderDir!='asc' || orderBy!='lastModificationDate'}">asc</c:if><c:if test="${orderDir=='asc'&& orderBy=='lastModificationDate'}">desc</c:if>&offset=${from}&count=${count}"
					class="sorting-arrow lastModificationDate">arrow</a></th>
				<!-- 				<th scope="col">Edit</th> -->
				<!-- 				<th scope="col">Delete</th> -->
			</tr>
			<c:forEach items="${resources}" var="resource">
				<tr>
					<%-- 					<td>${resource.name}</td> --%>
					<td>${resource.locationObject.name}</td>
					<td>${resource.dashboardObject.name}</td>
					<td><fmt:formatDate value="${resource.lastModificationDate}"
							pattern="dd/MM/yyyy" /></td>
					<%-- 					<td><a href="/cms/data/dataset/edit/${resource.id}" --%>
					<!-- 						class="show-icon">Edit</a></td> -->
					<!-- 					<td><a -->
					<%-- 						href="/cms/process/entry/rollbackdataset/${resource.id}" --%>
					<!-- 						class="remove-icon rollback-dataset">Delete</a></td> -->
				</tr>
			</c:forEach>
		</table>
		<div class="sort">
			<div class="show-items">
				Show items per page
				<form>
					<select class="type-select entries-per-page">
						<option value="20">20</option>
						<option value="15">15</option>
						<option value="10">10</option>
						<option value="5">5</option>
					</select>
				</form>
			</div>

			<div class="sort-numbers">
				<b>${from + 1}</b> - <b>${to}</b> of <b>${total}</b>
			</div>
			<a class="sort-arrows arrow-left"
				href="<c:if test="${from > 0}">?offset=${from-count}&count=${count}&orderBy=${orderBy}&orderDir=${orderDir}</c:if><c:if test="${from <= 0}"></c:if>"
				id="questions-table-previous"><span></span></a> <a
				class="sort-arrows arrow-right"
				href="<c:if test="${to < total}">?offset=${to}&count=${count}&orderBy=${orderBy}&orderDir=${orderDir}</c:if><c:if test="${to == total}"></c:if>"
				id="questions-table-next"><span></span></a> <input type="hidden"
				id="input-from" value="${from}"></input> <input type="hidden"
				id="input-to" value="${to}"></input> <input type="hidden"
				id="input-total" value="${total}"></input> <input type="hidden"
				id="input-orderDir" value="${orderDir}"></input> <input
				type="hidden" id="input-orderBy" value="${orderBy}"></input> <input
				type="hidden" id="input-count" value="${count}"></input>
		</div>
	</templates:data>
</templates:cms>



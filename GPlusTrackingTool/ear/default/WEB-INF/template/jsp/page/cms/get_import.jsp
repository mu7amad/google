<%@ taglib tagdir="/WEB-INF/tags/cms" prefix="templates"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="widgets"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>



<%@ page
	import="com.google.appengine.api.blobstore.BlobstoreServiceFactory"%>
<%@ page import="com.google.appengine.api.blobstore.BlobstoreService"%>

<%
	BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
	String url = blobstoreService.createUploadUrl("/dataUploadController/upload");
%>

<c:set var="title">
Google+ Brand Tracker - Import
</c:set>
<c:set var="footer">


	<script src="/static/js/vendor/jquery.ui.widget.js"></script>
	<!-- The Templates plugin is included to render the upload/download listings -->
	<script src="/static/js/tmpl.min.js"></script>
	<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
	<script src="/static/js/load-image.min.js"></script>
	<!-- The Canvas to Blob plugin is included for image resizing functionality -->
	<script src="/static/js/canvas-to-blob.min.js"></script>
	<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
	<script src="/static/js/bootstrap-image-gallery.min.js"></script>
	<script src="/static/js/jquery.iframe-transport.js"></script>
	<!-- The basic File Upload plugin -->
	<script src="/static/js/jquery.fileupload.js"></script>
	<!-- The File Upload file processing plugin -->
	<script src="/static/js/jquery.fileupload-fp.js"></script>
	<!-- The File Upload user interface plugin -->
	<script src="/static/js/jquery.fileupload-ui.js"></script>

	<!-- The main application script -->
	<script src="/static/js/main.js"></script>
	<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE8+ -->
	<!--[if gte IE 8]><script src="js/cors/jquery.xdr-transport.js"></script><![endif]-->


</c:set>
<c:set var="head">
	<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
	<link rel="stylesheet" href="/static/css/jquery.fileupload-ui.css">
	<!-- CSS adjustments for browsers with JavaScript disabled -->
	<noscript>
		<link rel="stylesheet"
			href="/static/css/jquery.fileupload-ui-noscript.css">
	</noscript>
	<!-- Shim to make HTML5 elements usable in older Internet Explorer versions -->
	<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->


</c:set>
<templates:cms title="${title}" head="${head}" footer="${footer } "
	activeTopNavItemId="importItem" studyId="${studyId }">
	<div class="container">

		<section id="maia-main" role="main">
			<h1>Import - Import Files</h1>



			<article class="maia-article import-page" role="article">
				<ul>
					<!-- 					<li> -->
					<!-- 						<div class="maia-notification error"> -->
					<!-- 							There has been an error verifying the file: UK.csv. <a href="#">Please -->
					<!-- 								click</a> here for more information about the error. -->
					<!-- 						</div> -->

					<!-- 						<div class="maia-notification steps"> -->
					<!-- 							<h5>A Place holder for an illustration explaining the steps</h5> -->
					<!-- 						</div> -->
					<!-- 					</li> -->
					<!-- 					The file upload form used as target for the file upload widget -->

					<!-- 						The file upload form used as target for the file upload widget -->
					<!-- 		<form id="fileupload" -->
					<%-- 			action="<%=blobstoreService.createUploadUrl("/dataUploadController/upload")%>" --%>
					<!-- 			method="POST" enctype="multipart/form-data"> -->
					<%-- 			<input id="_csrf" type="hidden" name="${_csrf.parameterName}" --%>
					<%-- 				value="${_csrf.token}" /> Redirect browsers with JavaScript --%>
					<!-- 			disabled to the origin page -->
					<!-- 			<noscript> -->
					<!-- 				<input type="hidden" name="redirect" value="#"> -->
					<!-- 			</noscript> -->
					<!-- 			The fileupload-buttonbar contains buttons to add/delete files and -->
					<!-- 			start/cancel the upload -->
					<!-- 			<div class="row fileupload-buttonbar"> -->
					<!-- 				<div class="span7"> -->
					<!-- 					The fileinput-button span is used to style the file input field as -->
					<!-- 					button <span class="btn btn-success fileinput-button  maia-button"> -->
					<!-- 						Select files <input type="file" name="files[]" multiple> -->
					<!-- 					</span> -->

					<!-- 					<button type="submit" class="btn btn-primary start maia-button"> -->
					<!-- 						Start upload</button> -->
					<!-- 					<button type="reset" class="btn btn-warning cancel  maia-button"> -->
					<!-- 						Cancel upload</button> -->

					<!-- 				</div> -->
					<!-- 				The global progress information -->
					<!-- 				<div class="span5 fileupload-progress fade"> -->
					<!-- 					The global progress bar -->
					<!-- 					<div class="progress progress-success progress-striped active" -->
					<!-- 						role="progressbar" aria-valuemin="0" aria-valuemax="100"> -->
					<!-- 						<div class="bar" style="width: 0%;"></div> -->
					<!-- 					</div> -->
					<!-- 					The extended global progress information -->
					<!-- 					<div class="progress-extended">&nbsp;</div> -->
					<!-- 				</div> -->
					<!-- 			</div> -->
					<!-- 			The loading indicator is shown during file processing -->
					<!-- 			<div class="fileupload-loading"></div> -->
					<!-- 			<br> The table listing the files available for upload/download -->

					<!-- 			<div role="presentation" id="succeededUploads" -->
					<!-- 				class="table table-striped"> -->

					<!-- 				<div class="files" data-toggle="modal-gallery" id="uploaded" -->
					<!-- 					data-target="#modal-gallery"></div> -->

					<!-- 			</div> -->
					<!-- 		</form> -->



					<widgets:form id="fileupload" action="${action}" method="POST"
						enctype="multipart/form-data">


						<%-- 						<input id="blob" type="hidden" name="blobName" value='<%=url%>' /> --%>
						<input id="_csrf" type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
						<!-- 											Redirect browsers with JavaScript disabled to the origin page -->
						<!-- 											<noscript> -->
						<input type="hidden" name="redirect" value="#">
						<!-- 						</noscript> -->
						<!-- 													The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
						<div class="row fileupload-buttonbar">
							<div class="span7">
								<!-- 								The fileinput-button span is used to style the file input field	as button  -->
								<span class="btn btn-success fileinput-button  maia-button">
									Select files <input type="file" name="files[]" multiple>
								</span>

								<button type="submit" class="btn btn-primary start maia-button">Start
									upload</button>
								<button type="reset" class="btn btn-warning cancel  maia-button">
									Cancel upload</button>

							</div>
							<!-- 							The global progress information -->
							<div class="span5 fileupload-progress fade">
								<!-- 								The global progress bar -->
								<div class="progress progress-success progress-striped active"
									role="progressbar" aria-valuemin="0" aria-valuemax="100">
									<div class="bar" style="width: 0%;"></div>
								</div>
								<!-- 								The extended global progress information -->
								<div class="progress-extended">&nbsp;</div>
							</div>
						</div>
						<!-- 																				The loading indicator is shown during file processing -->
						<div class="fileupload-loading"></div>
						<br>
						<!-- 																				The table listing the files available for upload/download -->

						<div role="presentation" id="succeededUploads"
							class="table table-striped">

							<div class="files" data-toggle="modal-gallery" id="uploaded"
								data-target="#modal-gallery"></div>
						</div>
					</widgets:form>
					<br>
	</div>
	<!-- modal-gallery is the modal dialog used for the image gallery -->
	<!-- 	<div id="modal-gallery" class="modal modal-gallery hide fade" -->
	<!-- 		data-filter=":odd" tabindex="-1"> -->
	<!-- 		<div class="modal-header"> -->
	<!-- 			<a class="close" data-dismiss="modal">&times;</a> -->
	<!-- 			<h3 class="modal-title"></h3> -->
	<!-- 		</div> -->
	<!-- 		<div class="modal-body"> -->
	<!-- 			<div class="modal-image"></div> -->
	<!-- 		</div> -->
	<!-- 		<div class="modal-footer"> -->
	<!-- 			<a class="btn modal-download" target="_blank"> <i -->
	<!-- 				class="icon-download"></i> <span>Download</span> -->
	<!-- 			</a> <a class="btn btn-success modal-play modal-slideshow" -->
	<!-- 				data-slideshow="5000"> <i class="icon-play icon-white"></i> <span>Slideshow</span> -->
	<!-- 			</a> <a class="btn btn-info modal-prev"> <i -->
	<!-- 				class="icon-arrow-left icon-white"></i> <span>Previous</span> -->
	<!-- 			</a> <a class="btn btn-primary modal-next"> <span>Next</span> <i -->
	<!-- 				class="icon-arrow-right icon-white"></i> -->
	<!-- 			</a> -->
	<!-- 		</div> -->
	<!-- 	</div> -->
	<script id="template-upload" type="text/x-tmpl">
			
{% for (var i=0, file; file=o.files[i]; i++) { %}
 
						
						
	<div class="maia-notification progress-bar template-upload fade" >
						{%=file.name%} - {%=o.formatFileSize(file.size)%}<br>
  {% if (file.error) { %}
 <p class="error">{%=file.name%} <strong>Error</strong> {%=file.error%}</p>
           
        {% } else if (o.files.valid && !i) { %}
            <div class="prog-bar">
                <div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="bar" style="width:0%;"></div></div>
           </div>
            <div class="start">{% if (!o.options.autoUpload) { %}
                <button class="btn btn-primary play">
                    Start
                </button>
            {% } %}</div>
        {% } else { %}
            <div colspan="2"></div>
        {% } %}
        <div class="cancel">{% if (!i) { %}
            <button class="btn btn-warning del-it">
                Cancel
            </button>
        {% } %}</div>



{% } %}
</script>
	<!-- The template to display files available for download -->
	<script id="template-download" type="text/x-tmpl">
		
{% for (var i=0, file; file=o.files[i]; i++) { %}
<div class="maia-notification succed-bar template-download fade" >
        {% if (file.error) { %}                  
            <p class="error">{%=file.name%} <strong>Error</strong> {%=file.error%}</p>
        {% } else { %}
            <p id="added_file" >{%=file.name%} - {%=o.formatFileSize(file.size)%}</p>
		
				
			
        {% } %}

        <div class="delete">
            <button class="btn btn-danger del-it" data-type="{%=file.delete_type%}" data-url="{%=file.delete_url%}"{% if (file.delete_with_credentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
               Delete
            </button>
        </div>
    </div>
{% } %}
</script>
	<table class="table-recent-upload " style="display: none;">
		<thead>
			<tr>
				<th scope="col" style='cursor: pointer'>Date</th>
				<th scope="col" style='cursor: pointer'>File Name</th>
				<th scope="col">Uploaded by</th>
				<th scope="col" style='cursor: pointer'>Size</th>
				<th scope="col" style='cursor: pointer'>Status</th>
				<th scope="col" class="tiny-raw">Actions</th>
			</tr>
		</thead>
		<tbody></tbody>
	</table>
	<div class="sort">
		<div class="sort-numbers">
			<b>${from + 1}</b> - <b>${to}</b> of <b>${total}</b>
		</div>
		<a class="sort-arrows arrow-left"
			href="<c:if test="${from > 0}">?offset=${from-count}&count=${count}&orderBy=${orderBy}&orderDir=${orderDir}</c:if><c:if test="${from <= 0}"></c:if>"
			id="questions-table-previous"><span></span></a> <a
			class="sort-arrows arrow-right"
			href="<c:if test="${to < total}">?offset=${to}&count=${count}&orderBy=${orderBy}&orderDir=${orderDir}</c:if><c:if test="${to == total}"></c:if>"
			id="questions-table-next"><span></span></a>
	</div>



	<form action="/cms/import/filterStudyFiles">

		Studies : <select>
			<c:forEach items="${studies }" var="study">
				<option />
				<option id="studyId" value="${study.studyID}">${study.studyName}</option>
			</c:forEach>
		</select> <input type="submit">
	</form>


	<table class="table-upload">
		<tr>
			<th scope="col" id="test">Date <a
				href="?orderBy=uploadDate&orderDir=<c:if test="${orderDir!='asc' || orderBy!='uploadDate'}">asc</c:if><c:if test="${orderDir=='asc' && orderBy=='uploadDate'}">desc</c:if>&offset=${from}&count=${count}"
				class="sorting-arrow uploadDate">arrow</a></th>
			<th scope="col">File Name<a
				href="?orderBy=name&orderDir=<c:if test="${orderDir!='asc' || orderBy!='name'}">asc</c:if><c:if test="${orderDir=='asc' && orderBy=='name'}">desc</c:if>&offset=${from}&count=${count}"
				class="sorting-arrow name">arrow</a></th>
			<th scope="col">Uploaded by</th>
			<!-- 				<th scope="col">Format</th> -->
			<th scope="col">Size<a
				href="?orderBy=size&orderDir=<c:if test="${orderDir!='asc' || orderBy!='size'}">asc</c:if><c:if test="${orderDir=='asc' && orderBy=='size'}">desc</c:if>&offset=${from}&count=${count}"
				class="sorting-arrow size">arrow</a></th>
			<th scope="col">Status<a
				href="?orderBy=status&orderDir=<c:if test="${orderDir!='asc' || orderBy!='status'}">asc</c:if><c:if test="${orderDir=='asc' && orderBy=='status'}">desc</c:if>&offset=${from}&count=${count}"
				class="sorting-arrow status">arrow</a></th>
			<th scope="col" class="tiny-raw">Actions</th>
		</tr>
		<c:forEach items="${resources }" var="resource">
			<tr>
				<td><fmt:formatDate value="${resource.uploadDate}"
						pattern="dd/MM/yyyy" />
				<td><c:out value="${resource.name}"></c:out></td>
				<td>${resource.uploadedBy}</td>
				<!-- 					<td>csv</td> -->
				<td><fmt:formatNumber type="number" maxFractionDigits="1"
						value="${resource.size/1000000}" /> MB</td>
				<td>${resource.status}</td>
				<c:if test="${resource.status eq 'UPLOADED'}">
					<%-- 				<td><widgets:form method="post" --%>
					<%-- 						action="/cms/process/entry?k=${resource.id}"> --%>
					<!-- 						<button type="submit" class="maia-button process-datasheet">Process</button> -->
					<%-- 					</widgets:form></td> --%>
					<c:if test="${resource.owner }">
						<td><form action="/dataUploadController/extractDataSheet"
								method="get">
								<input type="hidden" value="${resource.id}" name="id" /> <input
									type="hidden" name="${_csrf.parameterName}"
									value="${_csrf.token}" /> <input type="submit" value="Process">
								<!-- 									disabled> -->
							</form></td>
					</c:if>
					<%-- 					<c:if test="${!resource.owner }"> --%>
					<%-- 						<td><form action="/dataUploadController/extractDataSheet" --%>
					<%-- 								method="post"> --%>
					<%-- 								<input type="hidden" value="${resource.id}" name="id" /> <input --%>
					<%-- 									type="hidden" name="${_csrf.parameterName}" --%>
					<%-- 									value="${_csrf.token}" /> <input id="process_button" --%>
					<!-- 									type="submit" value="Process" onclick="change()"> -->
					<%-- 							</form></td> --%>
					<%-- 					</c:if> --%>

				</c:if>
				<c:if test="${resource.status eq 'PROCESSED'}">
					<td><widgets:form method="post"
							action="/cms/process/entry/rollbackdatasheet/${resource.id}">
							<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" />
							<button type="submit" class="maia-button rollback-datasheet">
								Rollback</button>
						</widgets:form></td>

				</c:if>
				<c:if test="${resource.status eq 'PROCESSING'}">
					<td><a href="" class="maia-button">Processing</a></td>
				</c:if>
				<c:if test="${resource.status eq 'ROLLINGBACK'}">
					<td><a href="" class="maia-button">Rollingback</a></td>
				</c:if>

			</tr>
		</c:forEach>
	</table>
	<div class="sort">
		<div class="show-items">
			Show items per page
			<form>
				<select class="type-select entries-per-page">
					<option value="20">20</option>
					<option value="15">15</option>
					<option value="10">10</option>
					<option value="5">5</option>
				</select>
			</form>
		</div>

		<div class="sort-numbers">
			<b>${from + 1}</b> - <b>${to}</b> of <b>${total}</b>
		</div>
		<a class="sort-arrows arrow-left"
			href="<c:if test="${from > 0}">?offset=${from-count}&count=${count}&orderBy=${orderBy}&orderDir=${orderDir}</c:if><c:if test="${from <= 0}"></c:if>"
			id="questions-table-previous"><span></span></a> <a
			class="sort-arrows arrow-right"
			href="<c:if test="${to < total}">?offset=${to}&count=${count}&orderBy=${orderBy}&orderDir=${orderDir}</c:if><c:if test="${to == total}"></c:if>"
			id="questions-table-next"><span></span></a>

	</div>
	<%-- 				<p><c:if test="${from == 0 && to != 0}">1</c:if><c:if test="${from > 0 || to == 0}">${from}</c:if>-${to} of ${total}</p> --%>
	<input type="hidden" id="input-from" value="${from}"></input>
	<input type="hidden" id="input-to" value="${to}"></input>
	<input type="hidden" id="input-total" value="${total}"></input>
	<input type="hidden" id="input-orderDir" value="${orderDir}"></input>
	<input type="hidden" id="input-orderBy" value="${orderBy}"></input>
	<input type="hidden" id="input-count" value="${count}"></input>
	</li>
	</ul>
	</article>
	</section>

</templates:cms>
<%@ taglib tagdir="/WEB-INF/tags/cms" prefix="templates"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="widgets"%>

<c:set var="title">
</c:set>
<templates:cms title="${title }" activeTopNavItemId="pagesItem"
	studyId="${studyId }">
	<templates:topNav title="Pages - ${resource.name}"
		topNavFirstItemLink="/cms/page/create" topNavSecondItem="Pages List"
		topNavFirstItem="Add New Page" topNavSecondItemLink="/cms/page"></templates:topNav>

	<widgets:form method="post" action="/cms/page/edit/${resource.id}"
		classes="validate">
		<fieldset>
			<ul>
				<li><label>Name</label> <input name="name" class="required"
					type="text" value='<c:out value="${resource.name }"></c:out>'></li>
				<li><label>Title</label> <input name="title" class="required"
					type="text" value='<c:out value="${resource.title }"></c:out>'></li>
				<li><label>Order</label> <input name="order" type="number"
					min="1" value="${resource.order }"></li>
				<li><label>Key words</label> <input name="keywords" type="text"
					value='<c:out value="${resource.keywords }"></c:out>'></li>
				<li><label>Description</label> <input name="description"
					type="text"
					value='<c:out value="${resource.description }"></c:out>'></li>
				<li><label>Content</label> <textarea name="content"
						class="required"><c:out value="${resource.content}"></c:out> </textarea></li>
			</ul>
		</fieldset>
		<button class="maia-button" type="submit">Save</button>
		<a href="/cms/page" class="maia-button maia-button-secondary"
			type="reset">Cancel</a>
	</widgets:form>
</templates:cms>



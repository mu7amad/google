<%@ taglib tagdir="/WEB-INF/tags/cms" prefix="templates"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="widgets"%>
<c:set var="title">
Google+ Brand Tracker - Pages
</c:set>
<templates:cms title="${title }" activeTopNavItemId="pagesItem"
	studyId="${studyId }">
	<templates:topNav title="Pages List"
		topNavFirstItemLink="/cms/page/create" topNavSecondItem="Pages List"
		topNavSelectedItemIndex="1" topNavFirstItem="Add New Page"
		topNavSecondItemLink="/cms/page"></templates:topNav>
	<div class="sort">
		<div class="sort-numbers">
			<b>${from + 1}</b> - <b>${to}</b> of <b>${total}</b>
		</div>
		<a class="sort-arrows arrow-left"
			href="<c:if test="${from > 0}">?offset=${from-count}&count=${count}</c:if><c:if test="${from <= 0}"></c:if>"
			id="questions-table-previous"><span></span></a> <a
			class="sort-arrows arrow-right"
			href="<c:if test="${to < total}">?offset=${to}&count=${count}</c:if><c:if test="${to == total}"></c:if>"
			id="questions-table-next"><span></span></a>
	</div>
	<table>
		<tr>
			<th scope="col">Name</th>
			<th scope="col">Order</th>
			<th scope="col" class="tiny-raw">Edit</th>
			<th scope="col" class="tiny-raw">Delete</th>
		</tr>

		<c:forEach items="${resource}" var="resources">
			<tr>
				<td><c:out value="${resources.name}"></c:out></td>
				<td>${resources.order}</td>

				<%-- 					<td><c:if --%>
				<%-- 							test="${fn:contains(resource.authorities,'ROLE_ADMIN')}">Administrator</c:if> --%>
				<%-- 						<c:if --%>
				<%-- 							test="${fn:contains(resource.authorities,'ROLE_RESEARCHER')}">Researcher</c:if></td> --%>
				<td><a href="/cms/page/edit/${resources.id }" class="show-icon">Edit</a></td>
				<td><widgets:form action="/cms/page/delete/${resources.id}"
						method="post">
						<button type="submit" class="unstyled-button delete-icon confirm"></button>
					</widgets:form>
			</tr>
		</c:forEach>
	</table>
	<div class="sort">
		<div class="show-items">
			Show items per page
			<form>
				<select class="type-select entries-per-page">
					<option value="20">20</option>
					<option value="15">15</option>
					<option value="10">10</option>
					<option value="5">5</option>
				</select>
			</form>
		</div>

		<div class="sort-numbers">
			<b>${from + 1}</b> - <b>${to}</b> of <b>${total}</b>
		</div>
		<a class="sort-arrows arrow-left"
			href="<c:if test="${from > 0}">?offset=${from-count}&count=${count}</c:if><c:if test="${from <= 0}"></c:if>"
			id="questions-table-previous"><span></span></a> <a
			class="sort-arrows arrow-right"
			href="<c:if test="${to < total}">?offset=${to}&count=${count}</c:if><c:if test="${to == total}"></c:if>"
			id="questions-table-next"><span></span></a> <input type="hidden"
			id="input-from" value="${from}"></input> <input type="hidden"
			id="input-to" value="${to}"></input> <input type="hidden"
			id="input-total" value="${total}"></input> <input type="hidden"
			id="input-orderDir" value="${orderDir}"></input> <input type="hidden"
			id="input-orderBy" value="${orderBy}"></input> <input type="hidden"
			id="input-count" value="${count}"></input>
	</div>
	<%-- 	<a href="<c:if test="${from >= 2}">?offset=${from-2}&count=2&orderBy=${orderBy}&orderDir=${orderDir}</c:if><c:if test="${from < 2}">#</c:if>" id="questions-table-previous">Previous</a> --%>
	<%-- 				<a href="<c:if test="${to < total}">?offset=${from-2}&count=2&orderBy=${orderBy}&orderDir=${orderDir}</c:if><c:if test="${to < total}">#</c:if>" id="questions-table-next">Next</a> --%>
	<%-- 				<p><c:if test="${from == 0 && to != 0}">1</c:if><c:if test="${from > 0 || to == 0}">${from}</c:if>-${to} of ${total}</p> --%>
</templates:cms>



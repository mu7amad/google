<%@ taglib tagdir="/WEB-INF/tags/cms" prefix="templates"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="widgets"%>

<c:set var="title">
Google+ Brand Tracker - Email
</c:set>
<templates:cms title="${title}" activeTopNavItemId="settingsItem"
	studyId="${studyId }">

	<templates:settings showTopNav="false" sideNavActiveItemId="emailsItem"
		title="Email">

		<article class="maia-article" role="article">
			<widgets:form action="/cms/settings/email/edit">
				<fieldset>
					<ul>
						<li><label>E-mails are sent from e-mail address</label> <input
							type="text" name="email" value="${email}"></li>

					</ul>
				</fieldset>
				<button class="maia-button" type="submit">Save</button>
				<button class="maia-button maia-button-secondary" type="reset">Cancel</button>
			</widgets:form>
		</article>

	</templates:settings>
</templates:cms>




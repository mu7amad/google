<%@ taglib tagdir="/WEB-INF/tags/cms" prefix="templates"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="widgets"%>
<c:set var="title">
Google+ Brand Tracker - Locations List
</c:set>
<templates:cms title="${title}" activeTopNavItemId="settingsItem"
	studyId="${studyId }">

	<templates:settings sideNavActiveItemId="locationsItem"
		title="Locations" showTopNav="false">
		<div class="sort">
			<div class="sort-numbers">
				<b>${from + 1}</b> - <b>${to}</b> of <b>${total}</b>
			</div>
			<a class="sort-arrows arrow-left"
				href="<c:if test="${from > 0}">?offset=${from-count}&count=${count}</c:if><c:if test="${from <= 0}"></c:if>"
				id="questions-table-previous"><span></span></a> <a
				class="sort-arrows arrow-right"
				href="<c:if test="${to < total}">?offset=${to}&count=${count}</c:if><c:if test="${to == total}"></c:if>"
				id="questions-table-next"><span></span></a>
		</div>
		<table>
			<tr>
				<th scope="col">Location</th>
				<th scope="col">Define as Country/Region</th>
			</tr>
			<c:forEach items="${resources}" var="resource">
				<tr>
					<td><c:out value="${resource.name}"></c:out></td>
					<c:if test="${resource.isRegion}">
						<td><widgets:form
								action="/cms/defineascountry/${resource.id}" method="post">
								<input type="hidden" name="${_csrf.parameterName}"
									value="${_csrf.token}" />
								<button class="define-region-btn" type="submit">Define
									as Country</button>
							</widgets:form></td>
					</c:if>
					<c:if test="${!resource.isRegion}">
						<td><widgets:form action="/cms/defineasregion/${resource.id}"
								method="post">
								<input type="hidden" name="${_csrf.parameterName}"
									value="${_csrf.token}" />
								<button class="define-region-btn" type="submit">Define
									as Region</button>
							</widgets:form></td>
					</c:if>
				</tr>
			</c:forEach>
		</table>
		<div class="sort">
			<div class="show-items">
				Show items per page
				<form>
					<select class="type-select entries-per-page">
						<option value="20">20</option>
						<option value="15">15</option>
						<option value="10">10</option>
						<option value="5">5</option>
					</select>
				</form>
			</div>

			<div class="sort-numbers">
				<b>${from + 1}</b> - <b>${to}</b> of <b>${total}</b>
			</div>
			<a class="sort-arrows arrow-left"
				href="<c:if test="${from > 0}">?offset=${from-count}&count=${count}</c:if><c:if test="${from <= 0}"></c:if>"
				id="questions-table-previous"><span></span></a> <a
				class="sort-arrows arrow-right"
				href="<c:if test="${to < total}">?offset=${to}&count=${count}</c:if><c:if test="${to == total}"></c:if>"
				id="questions-table-next"><span></span></a> <input type="hidden"
				id="input-from" value="${from}"></input> <input type="hidden"
				id="input-to" value="${to}"></input> <input type="hidden"
				id="input-total" value="${total}"></input> <input type="hidden"
				id="input-orderDir" value="${orderDir}"></input> <input
				type="hidden" id="input-orderBy" value="${orderBy}"></input> <input
				type="hidden" id="input-count" value="${count}"></input>
		</div>
		<%-- 		<a href="<c:if test="${from >= 2}">?offset=${from-2}&count=2</c:if><c:if test="${from < 2}">#</c:if>" id="questions-table-previous">Previous</a> --%>
		<%-- 				<a href="<c:if test="${to < total}">?offset=${to}&count=2</c:if><c:if test="${to < total}">#</c:if>" id="questions-table-next">Next</a> --%>
		<%-- 				<p><c:if test="${from == 0 && to != 0}">1</c:if><c:if test="${from > 0 || to == 0}">${from}</c:if>-${to} of ${total}</p> --%>
	</templates:settings>
</templates:cms>




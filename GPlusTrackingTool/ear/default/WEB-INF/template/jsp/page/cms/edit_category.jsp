<%@ taglib tagdir="/WEB-INF/tags/cms" prefix="templates"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="widgets"%>

<c:set var="title">
Google+ Brand Tracker - Edit Category
</c:set>
<templates:cms title="${title }" activeTopNavItemId="categoriesItem"
	studyId="${studyId }">
	<templates:data topNavSecondItem="Categories List"
		topNavSecondItemLink="/cms/data/category"
		topNavFirstItemLink="/cms/data/category/create"
		topNavFirstItem="Add New Category" topNavThirdItem="Order Categories"
		topNavThirdItemLink="/cms/data/category/order"
		sideNavActiveItemId="categoriesItem"
		title="Categories - Categories List" showTopNav="true">

		<widgets:form method="post"
			action="/cms/data/category/edit/${resource.id }" classes="validate">
			<input type="hidden" value="${study.id}" name="id" />
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
			<fieldset>
				<ul>
					<li><label>Name</label> <input name="name" type="text"
						value='<c:out value="${resource.name}"></c:out>'></li>
					<li><label>Gplus Only</label> <input name="googlePlusOnly"
						type="checkbox"
						<c:if test="${resource.googlePlusOnly}">checked="true"</c:if>></li>
				</ul>
			</fieldset>
			<button class="maia-button" type="submit">Save</button>
			<a href="/cms/data/category"
				class="maia-button maia-button-secondary" type="reset">Cancel</a>
		</widgets:form>
	</templates:data>
</templates:cms>



<%@ taglib tagdir="/WEB-INF/tags/cms" prefix="templates"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="widgets"%>
<c:set var="title">
</c:set>
<templates:cms title="${title}" activeTopNavItemId="settingsItem"
	studyId="${studyId }">

	<templates:settings sideNavActiveItemId="regionsItem"
		showTopNav="false" title="Region">

		<article class="maia-article" role="article">
			<widgets:form method="post" action="/cms/settings/region/edit/${id }"
				classes="validate">
				<fieldset>
					<ul>
						<li><label><c:out value="${resource.name}"></c:out> </label></li>
						<li><label>Country List</label> <select name="locList"
							multiple="multiple">
								<c:forEach items="${resources}" var="resource">
									<option value="${resource.id}"
										<c:if test="${not empty resource.region.id and resource.region.id eq id }" > selected="selected"	</c:if>>
										<c:out value="${resource.name}"></c:out>
									</option>
								</c:forEach>
						</select></li>
					</ul>
				</fieldset>
				<button class="maia-button" type="submit">Save</button>
				<a href="/cms/settings/region"
					class="maia-button maia-button-secondary" type="reset">Cancel</a>
			</widgets:form>
		</article>
	</templates:settings>
</templates:cms>




<%@ taglib tagdir="/WEB-INF/tags/cms" prefix="templates"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="widgets"%>

<c:set var="title">
</c:set>
<templates:cms title="${title }" activeTopNavItemId="adminsItem"
	studyId="${studyId }">
	<templates:topNav title="Admins - Create Admin"
		topNavFirstItemLink="/cms/admin/create" topNavSecondItem="Admins List"
		topNavSelectedItemIndex="0" topNavFirstItem="Add New Admin"
		topNavSecondItemLink="/cms/admin"></templates:topNav>

	<widgets:form method="post" action="/cms/admin/create"
		classes="validate">
		<fieldset>
			<ul>
				<li><label>First Name</label> <input name="firstName"
					type="text"></li>
				<li><label>Last Name</label> <input name="lastName" type="text"></li>
				<li class="email-role"><label>E-mail</label> <input
					class="required emailval" name="email" type="text"></li>
				<li><label>Company</label> <input name="company" type="text"></li>
				<li><label>Type</label> <select name="role" size="1"
					class="type-select roles">
						<option value="ROLE_ADMIN">Administrator</option>
						<option value="ROLE_RESEARCHER">Researcher</option>
				</select></li>
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />

			</ul>
		</fieldset>
		<button class="maia-button" type="submit">Save</button>
		<a href="/cms/admin" class="maia-button maia-button-secondary"
			type="reset">Cancel</a>
	</widgets:form>
</templates:cms>



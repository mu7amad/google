<%@ taglib tagdir="/WEB-INF/tags/cms" prefix="templates"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="widgets"%>

<c:set var="title">
Google+ Brand Tracker - Create Category
</c:set>
<templates:cms title="${title }" activeTopNavItemId="categoriesItem"
	studyId="${studyId }">

	<templates:data title="Categories - Add New Category"
		topNavFirstItemLink="/cms/admin/create"
		topNavSecondItem="Categories List" topNavSelectedItemIndex="0"
		topNavFirstItem="Add New Category"
		topNavSecondItemLink="/cms/data/category"
		topNavThirdItem="Order Categories"
		topNavThirdItemLink="/cms/data/category/order" showTopNav="true"
		sideNavActiveItemId="categoriesItem">

		<widgets:form method="post" action="/cms/data/category/create"
			classes="required">
			<fieldset>
				<ul>
					<li><label>Name</label> <input class="required" name="name"
						type="text"></li>
				</ul>
			</fieldset>
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
			<button class="maia-button" type="submit">Save</button>
			<a href="/cms/data/category"
				class="maia-button maia-button-secondary" type="reset">Cancel</a>
		</widgets:form>
	</templates:data>
</templates:cms>



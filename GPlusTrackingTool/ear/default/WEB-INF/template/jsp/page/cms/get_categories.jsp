<%@ taglib tagdir="/WEB-INF/tags/cms" prefix="templates"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="widgets"%>

<c:set var="title">
Google+ Brand Tracker - Questions List
</c:set>
<templates:cms title="${title }" activeTopNavItemId="dataItem"
	studyId="${studyId }">
	<templates:data topNavSecondItem="Categories List"
		topNavFirstItemLink="/cms/data/category/create"
		topNavFirstItem="Add New Category" topNavSelectedItemIndex="1"
		topNavThirdItem="Order Categories"
		topNavThirdItemLink="/cms/data/category/order"
		sideNavActiveItemId="categoriesItem"
		title="Categories - Categories List" showTopNav="true">

		<article class="maia-article" role="article">

			<table>
				<tr>
					<th scope="col">Categories</th>
					<th scope="col" class="tiny-raw">Edit</th>
					<th scope="col" class="tiny-raw">Delete</th>
				</tr>
				<c:forEach items="${resources}" var="resource">
					<tr>
						<td><c:out value="${resource.name}"></c:out></td>
						<td><a href="/cms/data/category/edit/${resource.id}"
							class="show-icon">Show</a></td>
						<td><widgets:form
								action="/cms/data/category/delete/${resource.id}" method="post">
								<button type="submit"
									class="unstyled-button delete-icon confirm-delete-catgeory">delete</button>
							</widgets:form></td>
					</tr>
				</c:forEach>
			</table>

			<%-- 			<a href="<c:if test="${from >= 2}">?offset=${from-2}&count=2</c:if><c:if test="${from < 2}">#</c:if>" id="questions-table-previous">Previous</a> --%>
			<%-- 				<a href="<c:if test="${to < total}">?offset=${to}&count=2</c:if><c:if test="${to < total}">#</c:if>" id="questions-table-next">Next</a> --%>
			<%-- 				<p><c:if test="${from == 0 && to != 0}">1</c:if><c:if test="${from > 0 || to == 0}">${from}</c:if>-${to} of ${total}</p> --%>
		</article>
	</templates:data>
</templates:cms>



<%@ taglib tagdir="/WEB-INF/tags/cms" prefix="templates"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="widgets"%>

<c:set var="title">
</c:set>
<templates:cms title="${title }" activeTopNavItemId="dataItem"
	studyId="${studyId }">
	<templates:data topNavFirstItemLink="#" topNavSecondItem="Filters List"
		topNavSelectedItemIndex="1" sideNavActiveItemId="filtersItem"
		topNavFirstItem="Edit Filter" title="Filters - Edit Filter"
		topNavSecondItemLink="/cms/data/filter">

		<article class="maia-article" role="article">
			<widgets:form action="/cms/data/filter/edit/${resource.id}"
				method="post">
				<fieldset>
					<ul>
						<li><label>Filter</label> <input type="text" name="name"
							value='<c:out value="${resource.name}"></c:out>'></li>
						<p>Waves</p>
						<table>
							<tr>
								<th scope="col" class="tiny-col">Date</th>
								<th scope="col">Name</th>
								<th scope="col" class="tiny-raw">Show</th>
							</tr>
							<c:forEach items="${dataSets}" var="dataSet">
								<tr>
									<td><fmt:formatDate value="${dataSet.waveDate}"
											pattern="dd/MM/yyyy" /></td>
									<td><c:out value="${dataSet.name}"></c:out></td>
									<td><a href="/cms/data/dataset/edit/${dataSet.id}"
										class="show-icon-eye">show</a></td>
								</tr>
							</c:forEach>
						</table>
						</li>
						<li>
							<p>Divisions</p>
							<ol>
								<c:forEach items="${divisions}" var="division">
									<li><c:out value="${division.name}"></c:out></li>
								</c:forEach>
							</ol>
						</li>
					</ul>
				</fieldset>
				<button class="maia-button" type="submit">Save</button>
				<a href="/cms/data/filter" class="maia-button maia-button-secondary"
					type="reset">Cancel</a>
			</widgets:form>
		</article>
	</templates:data>
</templates:cms>



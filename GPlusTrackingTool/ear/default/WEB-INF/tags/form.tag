<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="action" description="" required="false"%>
<%@ attribute name="method" description="" required="false"%>
<%@ attribute name="classes" description="" required="false"%>
<%@ attribute name="id" description="" required="false"%>
<%@ attribute name="enctype" description="" required="false"%>

<form id="${id}" class="${classes}" method="${method}"
	action="${action }"
	<c:if test="${not empty enctype}">enctype="${enctype}"</c:if>>
	<jsp:doBody />
	<input type="hidden" id="_tk" name="tk" value="${tk }" />
</form>


<%@tag import="com.google.appengine.api.users.User"%>
<%@tag
	import="com.brightcreations.gplus.web.security.ApplicationUserRole"%>
<%@tag import="com.brightcreations.gplus.module.controllers.OfyService"%>
<%@tag import="com.brightcreations.gplus.module.model.ApplicationUser"%>
<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ tag import="com.google.appengine.api.users.UserService"%>
<%@ tag import="com.google.appengine.api.users.UserServiceFactory"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="title" description="" required="true"%>
<%@ attribute name="description" description="" required="false"%>
<%@ attribute name="keywords" description="" required="false"%>
<%@ attribute name="head" description="" required="false"%>
<%@ attribute name="footer" description="" required="false"%>
<%@ attribute name="activeTopNavItemId" description="" required="false"%>



<!DOCTYPE html>
<!--[if IE 7]> <html class="google ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="google ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="google ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="google" lang="en">
<!--<![endif]-->
<head>
<script>
	(function(H) {
		H.className = H.className.replace(/\bgoogle\b/, 'google-js')
	})(document.documentElement)
</script>
<meta charset="utf-8">
<meta content="initial-scale=1, minimum-scale=1, width=device-width"
	name="viewport">
<meta name="description" content="${description}">
<meta name="keywords" content="${keywords}">
<title>${title }</title>
<link rel="stylesheet" media="all" href="/site_ui/css/default.css">
<script src="//www.google.com/js/google.js"></script>
<script>
	new gweb.analytics.AutoTrack({
		profile : "UA-38263893-1"
	});
</script>
<link
	href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;lang=en"
	rel="stylesheet">
<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

${head}
</head>
<body>
	<header>
		<div class="maia-header" id="maia-header" role="banner">
			<div class="maia-aux">
				<h1>
					<a href="../"><img alt="Google"
						src="//www.google.com/images/logos/google_logo_41.png">+
						Brand Tracker</a>
				</h1>
			</div>
		</div>
	</header>
	<!-- /Header -->

	<nav class="maia-nav" id="maia-nav-x" role="navigation">
		<div class="maia-aux"></div>
	</nav>

	<%
		User u = UserServiceFactory.getUserService().getCurrentUser();
	%>
	<nav class="sub-nav clearfix">
		<ul>
			<li class="user"><a href="#"><%=u == null ? "anonymous" : UserServiceFactory.getUserService().getCurrentUser().getEmail()%></a></li>
			<li><a href="/logout">Logout</a></li>
			<li style="display: none" id="UI"><%=u.getUserId()%></li>
		</ul>
	</nav>
	<!-- /Navigtion -->

	<section id="main-content" class="clearfix colums main-hide"
		role="main">
		<jsp:doBody />
	</section>
	<!-- /Content -->



	<div id="Country-popup" class="popup"
		style="display: none; width: 310px; height: 220px;">
		<form>
			<h3>Set default country</h3>
			<div id="popup-content">
				<fieldset>
					<label>Country</label> <select class="type-select"
						id="location-set">
					</select>
				</fieldset>
				<button id="save-location" class="maia-button ">Save</button>
				<!-- 			<button id="close-location" class="maia-button ">Close</button> -->
				<a href="javascript:parent.$.fancybox.close();" class="maia-button ">Close</a>
			</div>
		</form>
	</div>

	<!-- / Popop -->
	<div id="maia-signature"></div>
	<footer class="maia-footer" id="maia-footer">
		<div id="maia-footer-local">
			<div class="maia-aux pages-links">
				<%-- 				<p> 

 					<a href="#">About the research</a> | <a href="#">Terms & 
 						Conditions</a> 
 				</p> --%>
			</div>
		</div>
		<div id="maia-footer-global">
			<div class="maia-aux">
				<ul>
					<li><a href="//www.google.com/">Google</a></li>
					<li><a href="//www.google.com/intl/en/policies/">Privacy
							&amp; Terms</a></li>
					<%--<li><a href="">Terms of Service</a></li>--%>
				</ul>
			</div>
		</div>
	</footer>
	<!-- /Footer -->

	<script src="//www.google.com/js/maia.js"></script>

	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	<script>
		window.jQuery
				|| document
						.write("<script src='/site_ui/js/jquery-1.8.3.min.js'>\x3C/script>")
	</script>
	<!--[if lte IE 9]>
		<script src="/site_ui/js/PIE.js"></script>
		<script src="/site_ui/js/jquery.placeholder.min.js"></script>
	<![endif]-->
	${footer}
</body>
</html>


<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags/cms" prefix="templates"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ attribute name="sideNavActiveItemId" description="" required="true"%>
<%@ attribute name="topNavFirstItem" description="" required="false"%>
<%@ attribute name="topNavFirstItemLink" description="" required="false"%>
<%@ attribute name="topNavSecondItem" description="" required="false"%>
<%@ attribute name="topNavSecondItemLink" description=""
	required="false"%>
<%@ attribute name="topNavSelectedItemIndex" description=""
	required="false"%>
<%@ attribute name="showTopNav" description="" type="Boolean"
	required="false"%>
<%@ attribute name="title" description="" required="true"%>

<templates:topNav title="Settings - ${title }"
	topNavFirstItemLink="${topNavFirstItemLink }"
	topNavSecondItem="${topNavSecondItem}"
	topNavSelectedItemIndex="${topNavSelectedItemIndex}"
	topNavFirstItem="${topNavFirstItem}"
	topNavSecondItemLink="${topNavSecondItemLink}"
	showTopNav="${showTopNav}"></templates:topNav>

<nav class="maia-nav" id="maia-nav-y" role="navigation">
	<ul>
		<!-- 		<li id="dataFileSettingsItem" -->
		<%-- 			<c:if test="${sideNavActiveItemId == 'dataFileSettingsItem'}">class="active"</c:if>><a --%>
		<!-- 			href="#">Data File Settings</a></li> -->
		<li id="locationsItem"
			<c:if test="${sideNavActiveItemId == 'locationsItem'}">class="active"</c:if>><a
			href="/cms/settings/location">Locations</a>
			<ul>
				<li id="regionsItem"
					<c:if test="${sideNavActiveItemId == 'regionsItem'}">class="active"</c:if>><a
					href="/cms/settings/region">Regions</a></li>
				<li id="countriesItem"
					<c:if test="${sideNavActiveItemId == 'countriesItem'}">class="active"</c:if>><a
					href="/cms/settings/country">Countries</a>
			</ul></li>
		<li id="emailsItem"
			<c:if test="${sideNavActiveItemId == 'emailsItem'}">class="active"</c:if>><a
			href="/cms/settings/email">E-mails</a></li>
	</ul>
</nav>
<article class="maia-article" role="article">
	<jsp:doBody />
</article>






<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="title" description="" required="true"%>
<%@ attribute name="topNavFirstItem" description="" required="false"%>
<%@ attribute name="topNavFirstItemLink" description="" required="false"%>
<%@ attribute name="topNavSecondItem" description="" required="false"%>
<%@ attribute name="topNavSecondItemLink" description=""
	required="false"%>
<%@ attribute name="topNavThirdItem" description="" required="false"%>
<%@ attribute name="topNavThirdItemLink" description="" required="false"%>
<%@ attribute name="topNavSelectedItemIndex" description=""
	required="false"%>
<%@ attribute name="showTopNav" description="" required="false"%>

<div class="maia-cols">
	<div class="maia-col-6">
		<h1>${title }</h1>
	</div>
	<c:if test="${empty showTopNav or showTopNav == true}">
		<div class="maia-col-6">
			<nav class="maia-nav-aux">
				<ul>
					<li
						<c:if test="${topNavSelectedItemIndex == 0}">class="active"</c:if>><a
						href="${topNavFirstItemLink }">${topNavFirstItem}</a></li>
					<li
						<c:if test="${topNavSelectedItemIndex == 1}">class="active"</c:if>><a
						href="${topNavSecondItemLink}">${topNavSecondItem}</a></li>
					<c:if test="${not empty topNavThirdItem}">
						<li
							<c:if test="${topNavSelectedItemIndex == 2}">class="active"</c:if>><a
							href="${topNavThirdItemLink}">${topNavThirdItem}</a></li>
					</c:if>
				</ul>
			</nav>
		</div>
	</c:if>
</div>

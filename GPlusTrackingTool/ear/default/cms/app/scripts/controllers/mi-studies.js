// import _ from 'lodash';
class MiStudiesCont {
  constructor($scope, $mdSidenav, $log, $q, $rootScope, $cookies, $http, $mdComponentRegistry, $mdToast, Upload, $timeout, MiStudiesModel, MiAdminModel) {
    // studies listing 
    $scope.mdSidenavOpen = true
    const ToastHideDelay = 2000
    $scope.MiForms = {}

    $scope.StudiesFiltering = function (FilterBy) {
        MiStudiesModel.studiesListing(FilterBy).then((res) => {
          $scope.StudiesListing = res.data.result
          $scope.CopyStudiesListing = angular.copy(res.data.result)
        }) 
      }
      // set default to all studies
    $scope.StudiesFiltering('ListStudies')

    function FormRest() {
      if ($scope.MiForms.AddStudyForm) {
        $scope.MiForms.AddStudyForm.$setUntouched()
        $scope.MiForms.AddStudyForm.$setPristine()
      }
      if ($scope.MiForms.EditStudyForm) {
        $scope.MiForms.EditStudyForm.$setUntouched()
        $scope.MiForms.EditStudyForm.$setPristine()
      }

      // $scope.StudiesListing = angular.copy($scope.CopyStudiesListing)
      $scope.StudyData = {}
    }
    // reset add and edit side nav data in close
    $mdComponentRegistry.when('StudiesSideNav').then(function () {
      $mdSidenav('StudiesSideNav').onClose(function () {
        $scope.StudiesListing = angular.copy($scope.CopyStudiesListing)
        FormRest()
      })
      if ($rootScope.SideNav) {
        $scope.toggleRight();
      }
    })


    $scope.toggleRight = function () {
      $scope.StudyActionTemp = 'cms/app/views/studies/add-study.html'
      $mdSidenav('StudiesSideNav').toggle().then(function () {
        $log.debug('toggle is done')
        $scope.StudyData = {};
        $scope.StudyData.uploaders = [];
        $scope.StudyData.publishers = [];
        $scope.StudyData.sharedWithExternalUsers = [];
        $scope.StudyData.totalInterviewed = '';
        $scope.StudyData.logoUrl = ''
        $scope.StudyData.logoBlobKey = ''
        var StudyPostData = ''
        $scope.mdSidenavOpen = false
      })
    }



    $scope.PublishersAdmins = []
    $scope.UploadersAdmins = []
    $scope.AssingedUsers = []
    $scope.readonly = false
    $scope.selectedItem = null
    $scope.searchText = null
    $scope.querySearch = querySearch
    $scope.autocompleteDemoRequireMatch = true
    $scope.fakeobj = {};

    /**
     * Search for vegetables.
     */
    function querySearch(query, object) {
      var results = query ? object.filter(createFilterFor(query)) : []
      return results
    }

    $scope.AsyncAutocomplete = function (searchText, isAdmin) {
      return MiAdminModel.adminLookUp({ 'searchWord': searchText, 'isAdmin': isAdmin }).then(function (res) {
        return res.data.result;
      })
    }


    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {
      var lowercaseQuery = angular.lowercase(query)
      return function filterFn(FiterlItem) {
        return (FiterlItem._lowername.indexOf(lowercaseQuery) === 0) ||
          (FiterlItem._lowertype.indexOf(lowercaseQuery) === 0)
      }
    }

    $scope.StudyData = {};
    $scope.StudyData.uploaders = [];
    $scope.StudyData.publishers = [];
    $scope.StudyData.sharedWithExternalUsers = [];
    $scope.StudyData.totalInterviewed = '';
    $scope.StudyData.logoUrl = ''
    $scope.StudyData.logoBlobKey = ''
    var StudyPostData = ''

    $scope.mailValidation = (chip, input) => {
      let reg = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;
      if (reg.test(chip)) {
        // input.$setValidity('pattern', true);
        console.log(chip)
        return chip
      } else {
        // input.$setValidity('pattern', false);
        return null
      }
    }

    $scope.StudyValidation = (Firstinput, SecondInput) => {
      if ($scope.StudyData.publishers.length && $scope.StudyData.uploaders.length) {
        var keepGoing = true
        angular.forEach($scope.StudyData.publishers, function (value, key) {
          angular.forEach($scope.StudyData.uploaders, function (params, keys) {
            if (keepGoing) {
              if (value.id == params.id) {
                Firstinput.$setValidity('unique-admin', false);
                keepGoing = false
              } else {
                Firstinput.$setValidity('unique-admin', true)
                if (SecondInput) {
                  SecondInput.$setValidity('unique-admin', true)
                }
              }
            }
          })
        });
      } else {
        Firstinput.$setValidity('unique-admin', true)
        if (SecondInput) {
          SecondInput.$setValidity('unique-admin', true)
        }
      }
    }

    $scope.uploadFiles = (file, errFiles) => {
      // get url to upload logo to it
      //   show the loader
      var loader = true;
      //   request URL to upload image
      MiStudiesModel.createUploadUrl().then((res) => {
        if (file) {
          // start uploading
          file.upload = Upload.upload({
            url: res.data,
            data: { 'images': file }
          })
          file.upload.then((response) => {
            $timeout(() => {
              // remove loader
              loader = false
              file.result = response.data
              $scope.StudyData.logoUrl = response.data.logoUrl
              $scope.StudyData.logoBlobKey = response.data.logoBlobKey.blobKey
            })
          }, (response) => {
            if (response.status > 0) {
              $scope.errorMsg = response.status + ': ' + response.data
            }
          }, (evt) => {
            file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total))
          })
        }
      })
    }

    // add new admin form submmition

    $scope.AddStudy = () => {
      StudyPostData = angular.toJson($scope.StudyData)
      MiStudiesModel.addNewStudy(StudyPostData).then((res) => {
        if (res.data.statusCode == '200') {
          $scope.StudiesListing.push(res.data.result)
          $scope.CopyStudiesListing = angular.copy(res.data.result)
          $scope.StudyData = {}
          $scope.StudyData.uploaders = []
          $scope.StudyData.publishers = []
          $scope.StudyData.sharedWithExternalUsers = []
          $scope.StudyData.totalInterviewed = '';
          $mdSidenav('StudiesSideNav').close()
        } else {
          $mdToast.show($mdToast.simple().textContent(res.data.message).hideDelay(ToastHideDelay))
        }
      })
    }

    //import file
    $scope.ImportFile = (study) => {
      var keepGoing = true;
      if (study.uploaders.length) {
        angular.forEach(study.uploaders, function (val, index) {
          //console.log(val.email);
          //console.log(study.uploaders[val]);
          if (keepGoing) {
            if (val.email == $rootScope.LoginedUserMail) {
              console.log("mm")
              keepGoing = false;
            } else {
              $mdToast.show($mdToast.simple().textContent("You aren't authorized to import files on this study").hideDelay(ToastHideDelay));
            }
          }
        });
      }
    }

    // view Study to be edit
    $scope.ViewStudy = (study) => {
        $scope.StudyData = {};
        $scope.StudyData.uploaders = [];
        $scope.StudyData.publishers = [];
        $scope.StudyData.sharedWithExternalUsers = [];
        $scope.StudyData.totalInterviewed = '';
        $scope.StudyData = study
        console.log(study)
        $mdSidenav('StudiesSideNav').open()
        $scope.StudyActionTemp = 'cms/app/views/studies/edit-study.html'
      }
      // save edited Study 
    $scope.EditStudy = () => {

        StudyPostData = angular.toJson($scope.StudyData);
        MiStudiesModel.updateStudy(StudyPostData).then((res) => {
          if (res.data.statusCode == '200') {
            $scope.CopyStudiesListing = angular.copy($scope.StudyData)
            $mdSidenav('StudiesSideNav').close()
            $mdToast.show($mdToast.simple().textContent(res.data.message).hideDelay(ToastHideDelay))
          } else {
            $mdToast.show($mdToast.simple().textContent(res.data.message).hideDelay(ToastHideDelay))
          }
        })
      }
      // Delete admin user
    $scope.DeleteStudy = (study) => {
        StudyPostData = angular.toJson({ 'id': study.id });
        MiStudiesModel.deleteStudy(StudyPostData).then((res) => {
          if (res.data.statusCode == '200') {
            console.log($scope.StudiesListing.indexOf(study))
            $mdToast.show($mdToast.simple().textContent(res.data.message).hideDelay(ToastHideDelay))
            $scope.StudiesListing.splice($scope.StudiesListing.indexOf(study), 1);
            $mdSidenav('StudiesSideNav').close();
          } else {
            $mdToast.show($mdToast.simple().textContent(res.data.message).hideDelay(ToastHideDelay))
          }
        })
      }
      // Cancel adding new Study
    $scope.CancelNewStudy = (form) => {
      FormRest()
      $mdSidenav('StudiesSideNav').close()
    }


  }

}

let MiStudiesComponent = {
  restrict: 'E',
  templateUrl: '/cms/app/views/studies/studies.html',
  controller: MiStudiesCont
}

export default MiStudiesComponent;
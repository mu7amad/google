// import _ from 'lodash';
class MiStudyFilesCont {
  constructor($scope, $mdToast, Upload, $stateParams, $timeout, MiStudiesModel, MiAdminModel) {

    const ToastHideDelay = 2000

    $scope.selected = []
    $scope.limitOptions = [5, 10]

    $scope.options = {
      rowSelection: false,
      multiSelect: false,
      autoSelect: true,
      decapitate: false,
      largeEditDialog: false,
      boundaryLinks: false,
      limitSelect: true,
      pageSelect: true
    }

    $scope.query = {
      order: 'name',
      limit: 10,
      page: 1
    }

    var StateParams = angular.toJson($stateParams)
    MiStudiesModel.viewStudy(StateParams).then((res) => {
      // console.log(res);
      //$scope.StudyFiles = res.data.result;
    })
  }

}

let MiStudyFilesComponent = {
  restrict: 'E',
  templateUrl: '/cms/app/views/studies/study-files.html',
  controller: MiStudyFilesCont
}

export default MiStudyFilesComponent;
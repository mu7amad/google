class MiDashboardCont {
  constructor($http, $mdEditDialog, $q, $state, $rootScope, $mdToast, $mdComponentRegistry, $scope, $mdSidenav, $log, $timeout, MiDashboardModel) {
    'use strict';
    // 'ngInject';
    var dashboardAbout = {};
    const ToastHideDelay = 2000

    $scope.NavToStudies = (target, sidenavOpen, viewStu) => {
      console.log($rootScope.IsAdmin)
      if ($rootScope.IsAdmin || viewStu) {
        $rootScope.SideNav = sidenavOpen;
        $state.go(target);
      } else {
        $mdToast.show($mdToast.simple().textContent("You aren't authorized to add study").hideDelay(ToastHideDelay))
      }
    }


    MiDashboardModel.pendingFiles().then((res) => {
      $scope.PendingFiles = res.data.result
    });

    MiDashboardModel.aboutDashboard().then((res) => {
      $scope.AboutDashboard = res.data.result
    });
    // $scope.editDashboard = function (text, elem) {
    //   dashboardAbout = angular.toJson({ 'about': text })
    //   MiDashboardModel.editAboutDashboard(dashboardAbout).then((res) => {
    //     console.log(res)
    //   });
    // }


    $scope.editDashboard=function (value){
        console.log($scope.AboutDashboard );
        console.log($rootScope.IsSuperAdmin);
        if ($rootScope.IsSuperAdmin == true) {
        dashboardAbout = angular.toJson({ 'about': value })
        MiDashboardModel.editAboutDashboard(dashboardAbout).then((res) => {
            console.log(res)
            console.log(res.data.result.about);
            $scope.AboutDashboard.about = res.data.result.about;
        });
        }else {
            $mdToast.show($mdToast.simple().textContent("You aren't authorized edit the dashboard").hideDelay(ToastHideDelay))
            console.log("you arent autherized !! ");
        }

    }

  }
}

let MiDashboardComponent = {
  restrict: 'E',
  templateUrl: '/cms/app/views/dashboard/dashboard.html',
  controller: MiDashboardCont
};

export default MiDashboardComponent;
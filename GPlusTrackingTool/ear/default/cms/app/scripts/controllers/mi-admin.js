class MiAdminCont {
  constructor($http, $mdEditDialog, $q, $mdToast, $mdComponentRegistry, $scope, $mdSidenav, $log, $timeout, MiAdminModel) {
    'use strict';
    // 'ngInject';
    $scope.mdSidenavOpen = true

    $scope.MiForms = {}
    const ToastHideDelay = 2000

    // get list of admin users
    MiAdminModel.adminListing().then((res) => {
      $scope.AdminUsers = res.data.result
      $scope.CopyAdminUsers = angular.copy(res.data.result)
    });


    function FormRest() {
      if ($scope.MiForms.AddAdminForm) {
        $scope.MiForms.AddAdminForm.$setUntouched()
        $scope.MiForms.AddAdminForm.$setPristine()
      }
      if ($scope.MiForms.EditAdminForm) {
        $scope.MiForms.EditAdminForm.$setUntouched()
        $scope.MiForms.EditAdminForm.$setPristine()
      }
      $scope.AdminUsers = angular.copy($scope.CopyAdminUsers)
      $scope.NewAdminUser = {}
    };
    // reset add and edit side nav data in close
    $mdComponentRegistry.when('AdminsSideNav').then(function () {
      $mdSidenav('AdminsSideNav').onClose(function () {
        FormRest()
      })
    })

    $scope.selected = []
    $scope.limitOptions = [5, 10]

    $scope.options = {
      rowSelection: false,
      multiSelect: false,
      autoSelect: true,
      decapitate: false,
      largeEditDialog: false,
      boundaryLinks: false,
      limitSelect: true,
      pageSelect: true
    }

    $scope.query = {
      order: 'name',
      limit: 10,
      page: 1
    }

    // $scope.loadStuff = function () {
    //   $scope.promise = $timeout(function () {
    //     // loading
    //   }, 5000);
    // }

    // $scope.logItem = function (item) {
    //   console.log(item.name, 'was selected');
    // };

    // $scope.logOrder = function (order) {
    //   console.log('order: ', order);
    // };

    // $scope.logPagination = function (page, limit) {
    //   console.log('page: ', page);
    //   console.log('limit: ', limit);
    // }

    $scope.toggleRight = function () {
      $scope.AdminActionTemp = 'cms/app/views/admins/add-admin.html';
      $mdSidenav('AdminsSideNav').toggle().then(function () {
        // $log.debug("toggle is done")
        $scope.mdSidenavOpen = false
      })
    }

    // Add Users
    $scope.NewAdminUser = {}
    var AdminPostData
      // add new admin form submmition
    $scope.AddNewAdminUser = function () {
      AdminPostData = JSON.stringify($scope.NewAdminUser)
      MiAdminModel.addAdmin(AdminPostData).then((res) => {
        if (res.data.statusCode == '200') {
          $scope.CopyAdminUsers.push(res.data.result)
          $mdSidenav('AdminsSideNav').close()
          $mdToast.show($mdToast.simple().textContent(res.data.message).hideDelay(ToastHideDelay))
        } else {
          $mdToast.show($mdToast.simple().textContent(res.data.message).hideDelay(ToastHideDelay))
        }
      })
    }


    // Cancel adding new admin
    $scope.CancelNewAdminUser = function (form) {
      FormRest()
      $mdSidenav('AdminsSideNav').close()
    };

    // view admin user to be edit
    $scope.ViewAdminUser = function (user) {

        $scope.NewAdminUser = user
        $scope.AdminActionTemp = 'cms/app/views/admins/edit-admin.html'
        $mdSidenav('AdminsSideNav').open()
      }
      // save edited admin user
    $scope.EditAdminUser = function () {
        AdminPostData = JSON.stringify({ 'firstName': $scope.NewAdminUser.firstName, 'lastName': $scope.NewAdminUser.lastName, 'email': $scope.NewAdminUser.email, 'id': $scope.NewAdminUser.id, 'status': $scope.NewAdminUser.status });
        MiAdminModel.editAdmin(AdminPostData).then((res) => {
          if (res.data.statusCode == '200') {
            $scope.CopyAdminUsers = angular.copy($scope.AdminUsers)
            $mdSidenav('AdminsSideNav').close()
            $mdToast.show($mdToast.simple().textContent(res.data.message).hideDelay(ToastHideDelay))

          } else {
            $mdToast.show($mdToast.simple().textContent(res.data.message).hideDelay(ToastHideDelay))
          }
        })
      }
      // Delete admin user
    $scope.DeleteAdminUser = function (AdminId) {
      AdminPostData = JSON.stringify({ 'id': AdminId.id });
      MiAdminModel.deleteAdmin(AdminPostData).then((res) => {
        if (res.data.statusCode == '200') {
          $scope.AdminUsers.splice($scope.AdminUsers.indexOf(AdminId), 1);
          $scope.CopyAdminUsers = angular.copy($scope.AdminUsers);
          $mdToast.show($mdToast.simple().textContent(res.data.message).hideDelay(ToastHideDelay))
          $mdSidenav('AdminsSideNav').close();
        } else {
          $mdToast.show($mdToast.simple().textContent(res.data.message).hideDelay(ToastHideDelay))
        }
      })
    }
  }
}

let MiAdminComponent = {
  restrict: 'E',
  templateUrl: '/cms/app/views/admins/admin.html',
  // templateUrl: '/views/admin-component.html',
  controller: MiAdminCont
};

export default MiAdminComponent;
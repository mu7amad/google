export default routesConfig;
/** @ngInject */

routesConfig.$inject = ['$urlRouterProvider', '$stateProvider', '$locationProvider', '$mdThemingProvider'];

function routesConfig($urlRouterProvider, $stateProvider, $locationProvider, $mdThemingProvider) {
  // $locationProvider.html5Mode(true);
  $urlRouterProvider.otherwise('/dashboard')
  $stateProvider.state('admins', {
    url: '/admins',
    template: '<mi-admin flex layout="column"></mi-admin>'
  }).state('studies', {
    url: '/studies',
    template: '<mi-studies flex layout="column"></mi-studies>'
  }).state('studies-views', {
    url: '/studies/:studyID',
    template: '<mi-studyfiles flex layout="column"></mi-studyfiles>'
  }).state('dashboard', {
    url: '/dashboard',
    template: '<mi-dashboard flex layout="column"></mi-dashboard>'
  })

  $mdThemingProvider.definePalette('DarkPalette', {
    '50': '#f4f5f8',
    '100': '#c5ccdb',
    '200': '#a2adc5',
    '300': '#fff',
    '400': '#63769e',
    '500': '#57688c',
    '600': '#4b5a79',
    '700': '#404c66',
    '800': '#283338',
    '900': '#283040',
    // '1000': '#000',// uncomment to test
    'A100': '#f4f5f8',
    'A200': '#c5ccdb',
    'A400': '#63769e',
    'A700': '#404c66',
    'contrastDefaultColor': 'light',
    'contrastDarkColors': '50 100 200 300 A100 A200'
  });
  $mdThemingProvider.theme('docs-dark', 'default').primaryPalette('pink').backgroundPalette('DarkPalette').dark();
}
export default class FileReaderServices {
  constructor($apply, $broadcast, $q) {
    'ngInject';
    this._$apply = $apply;
    this._$broadcast = $broadcast;
    this._$q = $q;
  }

  onLoad(reader, deferred, scope) {
    return function() {
      scope.this._$apply(function() {
        deferred.resolve(reader.result);
      });
    };
  }

  onError(reader, deferred, scope) {
    return function() {
      scope.this._$apply(function() {
        deferred.reject(reader.result);
      });
    };
  }

  onProgress(reader, scope) {
    return function(event) {
      scope.this._$broadcast('fileProgress', {
        total: event.total,
        loaded: event.loaded
      });
    };
  }

  getReader(deferred, scope) {
    var reader = new FileReader();
    reader.onload = onLoad(reader, deferred, scope);
    reader.onerror = onError(reader, deferred, scope);
    reader.onprogress = onProgress(reader, scope);
    return reader;
  }

  readAsDataURL(file, scope) {
    var deferred = this._$q.defer();
    var reader = getReader(deferred, scope);
    reader.readAsDataURL(file);
    return deferred.promise;
  }
}

export function FileImport() {
  'ngInject';
  return {
    require: 'ngModel',
    link: function(scope, el, attrs, ngModel) {
      el.bind('change', function(e) {
        ngModel.$render = function() {
          ngModel.$setViewValue(el.val());
        };
        scope.Ufile = (e.srcElement || e.target).files[0];
        scope.getFile(scope.Ufile);
        scope.$apply(function() {
          ngModel.$render();
        });
      });
    }
  };
}


export function ValidOnSelect() {
  'ngInject';
  return {
    require: 'ngModel',
    link: function(scope, el, attrs, ngModel) {
      el.bind('change', function(e) {
        ngModel.$render = function() {
          ngModel.$setViewValue(el.val());
        };
        scope.Ufile = (e.srcElement || e.target).files[0];
        scope.$apply(function() {
          ngModel.$render();
        });
      });
    }
  };
}
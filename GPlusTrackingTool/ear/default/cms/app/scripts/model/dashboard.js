export default class MiDashboardService {
  constructor($http, Networking) {
    'ngInject'
    this._$http = $http
    this._Networking = Networking
      // Apis Urls
  }
  pendingFiles() {
    return this._Networking.Get('UserPendingFiles')
  }
  aboutDashboard() {
    return this._Networking.Get('AboutDashBoard')
  }
  editAboutDashboard(aboutText) {
    return this._Networking.Put('addAboutDashBoard', aboutText)
  }
}
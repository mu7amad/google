export default class MiAdminService {
  constructor($http, Networking) {
    'ngInject';
    this._$http = $http;
    this._Networking = Networking;
    // Apis Urls
  }
  adminListing() {
    return this._Networking.Get('MiAdmins')
  }
  adminLookUp(keyWord) {
    return this._Networking.Get('AdminsLookUp', keyWord)
  }
  addAdmin(NewAdminData) {
    return this._Networking.Put('NewAdmin', NewAdminData)
  }
  editAdmin(AdminData) {
    return this._Networking.Put('EditAdmin', AdminData)
  }
  deleteAdmin(AdminId) {
    return this._Networking.Post('DeleteAdmin', AdminId)
  }
}
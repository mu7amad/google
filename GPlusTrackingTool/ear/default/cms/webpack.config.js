var path = require('path');
var webpack = require('webpack');
var BowerWebpackPlugin = require('bower-webpack-plugin');
// var ngAnnotatePlugin = require('ng-annotate-webpack-plugin');

module.exports = {
  output: {
    // path: __dirname + "/.tmp",
    filename: 'bundle.js',
    publicPath: '/scripts/'
  },

  devtool: 'source-map',
  module: {
    loaders: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'babel',
      query: {
        presets: ['es2015']
      }
    }, {
      test: /\.(css|scss|svg|eot|ttf|woff)$/,
      loader: 'ignore-loader'
    }]
  }
};